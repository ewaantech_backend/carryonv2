$(document).ready(function () {
  // Start PieChart
  function drawPieChart(pieobject) {
    let donut = Morris.Donut({
      element: pieobject.pie_element,
      data: pieobject.data,
      formatter: function (value, data) {
        var formatter = new Intl.NumberFormat('en-US', {
         style: 'currency',
         currency: 'AED',
        });
        if(!isNaN(value)){
         value = formatter.format(value);
        }
        return value;
      }
    });
    donut.options.data.forEach(function (label, i) {
     var formatter = new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: 'AED',
     });
     var tempval = label['value'];
     if(!isNaN(label['value'])){
      label['value'] = formatter.format(label['value']);
     }
      let legendItem = $('<span></span>').text(`${label['label']}`).prepend('<br><span>&nbsp;</span>');
      legendItem.find('span')
        .css('backgroundColor', donut.options.colors[i])
        .css('width', '20px')
        .css('display', 'inline-block')
        .css('margin', '5px');
      $(pieobject.pie_legend_element).append(legendItem);
      label['value'] = tempval;
    });

  }
  // End Piechart


  // Start line graph
  function drawLineGraph(lineObject) {
    let line = Morris.Line({
      // ID of the element in which to draw the chart.
      element: lineObject.element,
      // Chart data records -- each entry in this array corresponds to a point on
      // the chart.
      data: lineObject.data,
      resize: true,
      // The name of the data record attribute that contains x-values.
      xkey: lineObject.xkey,
      // A list of names of data record attributes that contain y-values.
      ykeys: lineObject.ykeys,
      // Labels for the ykeys -- will be displayed when you hover over the
      // chart.
      labels: lineObject.ykeys,
      //lineColors: lineObject.colors
      hideHover: false,
      xLabelAngle: '90',
      padding: 70,
      parseTime: false,
      hoverCallback: function (index, options, content, row) {
        var formatter = new Intl.NumberFormat('en-US', {
          style: 'currency',
          currency: 'AED',
        });
        let hover_string = '';
        lineObject.ykeys.forEach((key, index) => 
        {
          if(!isNaN(row[key])){
            row[key] = formatter.format(row[key]);
          }
          hover_string += `<div class='morris-hover-point' style='color:${options.lineColors[index]} !important;'>${row[key]}</div>`;
        });
        return hover_string;
      },
    });

    line.options.labels.forEach(function (label, i) {
      var legendlabel = $('<span style="display: inline-block;">&nbsp;&nbsp;' + label + '</span>')
      var legendItem = $('<div class="mbox"></div>').css('background-color', line.options.lineColors[i]).append(legendlabel)
      $(lineObject.legend_element).append(legendItem)
    })
  }
  // End line graph

  // Start bar graph
  function drawBarGraph(barObject) {
    console.log(barObject);
    Morris.Bar({
      element: barObject.element,
      resize: true,
      data: barObject.data,
      xkey: 'y',
      xLabelAngle: '90',
      padding: 70,
      ykeys: barObject.ykeys,
      labels: ['Sales'],
      hideHover: false,
      hoverCallback: function (index, options, content, row) {
        var formatter = new Intl.NumberFormat('en-US', {
          style: 'currency',
          currency: 'AED',
        });
        let hover_string = '';
        barObject.ykeys.forEach((key, index) => 
        {
          if(!isNaN(row[key])){
            row[key] = formatter.format(row[key]);
          }
          hover_string += `<div class='morris-hover-point' style='color:${options.barColors[index]} !important;'>${row[key]}</div>`;
        });
        return hover_string;
      },
    });
  }
  // End bar graph

  
  // Bootstrap tab
  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    let target = $(e.target).attr("href") // activated tab
    // start - to remove the selected bug in bootstrap tab
    $("#reports .nav-link").removeClass("active");
    $(this).addClass("active");
    // end - to remove the selected bug in bootstrap tab
    switch (target) {
      case "#sales":
        getData({
          url: `${base_url}/admin/reports/salestab/${$('#daterange-sales').data('daterangepicker').startDate.format('YYYY-MM-DD')}/${$('#daterange-sales').data('daterangepicker').endDate.format('YYYY-MM-DD')}/[]/[]`,
          category: '[]',
          diet: '[]',
          start_date: $('#daterange-sales').data('daterangepicker').startDate.format('YYYY-MM-DD'),
          end_date: $('#daterange-sales').data('daterangepicker').endDate.format('YYYY-MM-DD'),
          spinner_id: 'sales'
        }).
        then((data) => {
            promiseThenSales(data);
          })
          .catch((data) => {
            console.error(data);
            // start - removing spinner object from DOM
            spinner('sales').remove();
            // end - removing spinner object from DOM
          });
        break;
      case "#saleslinechart":
        getData({
          url: `${base_url}/admin/reports/linecharttab/${$('#daterange-sales-linechart').data('daterangepicker').startDate.format('YYYY-MM-DD')}/${$('#daterange-sales-linechart').data('daterangepicker').endDate.format('YYYY-MM-DD')}/[]/[]`,
          category: '[]',
          diet: '[]',
          start_date: $('#daterange-sales-linechart').data('daterangepicker').startDate.format('YYYY-MM-DD'),
          end_date: $('#daterange-sales-linechart').data('daterangepicker').endDate.format('YYYY-MM-DD'),
          spinner_id: 'saleslinechart'
        }).
        then((data) => {
            promiseThenSalesLine(data);
          }).then((data) => {
            $(window).trigger('resize');
          })
          .catch((data) => {
            console.error(data);
            // start - removing spinner object from DOM
            spinner('saleslinechart').remove();
            // end - removing spinner object from DOM
          });
        break;
      case "#destination":
        getData({
          url: `${base_url}/admin/reports/destinationtab/${$('#daterange-destination').data('daterangepicker').startDate.format('YYYY-MM-DD')}/${$('#daterange-destination').data('daterangepicker').endDate.format('YYYY-MM-DD')}/[]/[]`,
          category: '[]',
          diet: '[]',
          start_date: $('#daterange-destination').data('daterangepicker').startDate.format('YYYY-MM-DD'),
          end_date: $('#daterange-destination').data('daterangepicker').endDate.format('YYYY-MM-DD'),
          spinner_id: 'destination'
        }).
        then((data) => {
            promiseThenSalesDestination(data);
          }).then((data) => {
            $(window).trigger('resize');
          })
          .catch((data) => {
            console.error(data);
            // start - removing spinner object from DOM
            spinner('destination').remove();
            // end - removing spinner object from DOM
          });
        break;
      case "#hourlysales":
        getData({
          url: `${base_url}/admin/reports/hourlytab/${$('#daterange-hourlysales').data('daterangepicker').startDate.format('YYYY-MM-DD')}/${$('#daterange-hourlysales').data('daterangepicker').endDate.format('YYYY-MM-DD')}`,
          category: '[]',
          diet: '[]',
          start_date: $('#daterange-hourlysales').data('daterangepicker').startDate.format('YYYY-MM-DD'),
          end_date: $('#daterange-hourlysales').data('daterangepicker').endDate.format('YYYY-MM-DD'),
          spinner_id: 'hourlysales'
        }).
        then((data) => {
            promiseThenSalesHourly(data);
          }).then((data) => {
            $(window).trigger('resize');
          })
          .catch((data) => {
            console.error(data);
            // start - removing spinner object from DOM
            spinner('hourlysales').remove();
            // end - removing spinner object from DOM
          });
        break;
    }
  });
  // End Bootstrap tab

  function spinner(id) {
    // Start Spinner
    var opts = {
      lines: 13, // The number of lines to draw
      length: 38, // The length of each line
      width: 17, // The line thickness
      radius: 45, // The radius of the inner circle
      scale: 1, // Scales overall size of the spinner
      corners: 1, // Corner roundness (0..1)
      color: '#119125', // CSS color or array of colors
      fadeColor: 'transparent', // CSS color or array of colors
      speed: 1, // Rounds per second
      rotate: 0, // The rotation offset
      animation: 'spinner-line-shrink', // The CSS animation name for the lines
      direction: 1, // 1: clockwise, -1: counterclockwise
      zIndex: 2e9, // The z-index (defaults to 2000000000)
      className: 'spinner', // The CSS class to assign to the spinner
      top: '25%', // Top position relative to parent
      left: '50%', // Left position relative to parent
      shadow: '0 0 1px transparent', // Box-shadow for the lines
      position: 'absolute' // Element positioning
    };

    var target = document.getElementById(id);
    var spinner = new Spin.Spinner(opts).spin(target);

    return $(".spinner");
    // End Spinner
  }


  // Start DateRange picker
  function dateRangePicker(params) {
    let start = params.start;

    function callback(start) {
      $(params.elementSpan).html(start.format('MMMM D, YYYY'));
    }
    // single date picker
    if (params.single == true) {
      $(params.element).daterangepicker({
        singleDatePicker: true,
        maxDate: moment(),
        ranges: params.ranges
      }, callback);
      callback(start)

    } else { // date range picker
      let start = params.start;
      let end = params.end;

      function callback(start, end) {
        $(params.elementSpan).html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
      }

      $(params.element).daterangepicker({
        startDate: start,
        endDate: end,
        maxDate: moment(),
        dateLimit: {
          days: params.dateLimit
        },
        ranges: params.ranges
      }, callback);
      callback(start, end)

    }

  }

  // start -  sales date range picker
  dateRangePicker({
    start: moment().subtract(29, 'days'),
    end: moment(),
    elementSpan: '#daterange-sales span',
    element: '#daterange-sales',
    dateLimit: 180,
    single: false,
    ranges: {
      'Today': [moment(), moment()],
      'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      'Last 7 Days': [moment().subtract(6, 'days'), moment()],
      'Last 30 Days': [moment().subtract(29, 'days'), moment()],
      'This Month': [moment().startOf('month'), moment().endOf('month')],
      'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
      'Last 3 Months': [moment().subtract(3, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
      'Last 6 Months': [moment().subtract(6, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    }
  })
  // end -  sales date range picker

  // start -  sales line chart date range picker
  dateRangePicker({
    start: moment().subtract(29, 'days'),
    end: moment(),
    elementSpan: '#daterange-sales-linechart span',
    element: '#daterange-sales-linechart',
    dateLimit: 180,
    single: false,
    ranges: {
      'Today': [moment(), moment()],
      'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      'Last 7 Days': [moment().subtract(6, 'days'), moment()],
      'Last 30 Days': [moment().subtract(29, 'days'), moment()],
      'This Month': [moment().startOf('month'), moment().endOf('month')],
      'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
      'Last 3 Months': [moment().subtract(3, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
      'Last 6 Months': [moment().subtract(6, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
      'Last 12 Months': [moment().subtract(12, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
    }
  })
  // end -  sales line chart date range picker

  // start -  destination date range picker
  dateRangePicker({
    start: moment().subtract(29, 'days'),
    end: moment(),
    elementSpan: '#daterange-destination span',
    element: '#daterange-destination',
    dateLimit: 180,
    single: false,
    ranges: {
      'Today': [moment(), moment()],
      'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      'Last 7 Days': [moment().subtract(6, 'days'), moment()],
      'Last 30 Days': [moment().subtract(29, 'days'), moment()],
      'This Month': [moment().startOf('month'), moment().endOf('month')],
      'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
      'Last 3 Months': [moment().subtract(3, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
      'Last 6 Months': [moment().subtract(6, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
      'Last 12 Months': [moment().subtract(12, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
    }
  })
  // end -  destination date range picker

  // start -  hourly sales date range picker
  dateRangePicker({
    start: moment().subtract(29, 'days'),
    end: moment(),
    elementSpan: '#daterange-hourlysales span',
    element: '#daterange-hourlysales',
    single: false,
    dateLimit: 180,
    ranges: {
      'Today': [moment(), moment()],
      'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      'Last 7 Days': [moment().subtract(6, 'days'), moment()],
      'Last 30 Days': [moment().subtract(29, 'days'), moment()],
      'This Month': [moment().startOf('month'), moment().endOf('month')],
      'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
      'Last 3 Months': [moment().subtract(3, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
      'Last 6 Months': [moment().subtract(6, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    }
  })
  // end -  hourly sales date range picker


  // End Daterange picker

  // Start Select 2 boxes - category and diet
  $(".select-2-filter_category").select2({
    placeholder: "All"
  });
  $(".select-2-filter_diet").select2({
    placeholder: "All"
  });
  $(".select-2-filter_category_sales_linechart").select2({
    placeholder: "All"
  });
  $(".select-2-filter_diet_sales_linechart").select2({
    placeholder: "All"
  });
  $(".select-2-filter_category_destination").select2({
    placeholder: "All"
  });
  $(".select-2-filter_diet_destination").select2({
    placeholder: "All"
  });

  // End Select 2 boxes - category and diet

  // start - API call for sales tab
  function getData(params) {
    spinner(params.spinner_id);
    return new Promise((resolve, reject) => {
      $.ajax({
        url: params.url,
        type: 'GET',
        dataType: 'JSON',
        success: function (data) {
          resolve(data);
        },
        error: function (error) {
          reject(error);
        }
      });
    });
  }
  // end - API call for sales tab

  function promiseThenSales(data) {
    let top5_diet_html = Object.keys(data.top_diets).length === 0 ? '<div class="p-l-10"></br> No Data Available</div>' : '';
    let top5_category_html = Object.keys(data.top_categories).length === 0 ? '<div class="p-l-10"></br> No Data Available</div>' : '';
    let top5_products_html = Object.keys(data.top_products).length === 0 ? '<div class="p-l-10"></br> No Data Available</div>' : '';
    let total_sales = data.total_sale === 0 ? 'AED 0.00' : 'AED ' + data.total_sale;
    let users_count = data.users_count === 0 ? '0' : data.users_count;
    let repeated_users_count = data.repeated_users_count === 0 ? '0' : data.repeated_users_count;

    // start - clearing the pie chart and its legends for regeneration
    $("#category-pie").empty();
    $("#diet-pie").empty();
    $("#category-pie-legend").html('');
    $("#diet-pie-legend").html('');
    // end - clearing the pie chart and its legends for regeneration

    // start - total sales
    total_sales = `<p class="card-text text-left"> ${total_sales}</p>`;
    $("#total_sales").html(total_sales);
    // end - total sales

    // start - users count
    users_count = `<p class="card-text text-left"> ${users_count}</p>`;
    $("#users_count").html(users_count);
    // end - users count

    // start - repeated users count
    repeated_users_count = `<p class="card-text text-left"> ${repeated_users_count}</p>`;
    $("#repeated_users_count").html(repeated_users_count);
    // end - repeated users count

    // start - populating html for top 5 category
    data.top_categories.forEach((item, index) => {
      top5_category_html += `<tr><td>${item.label}</td><td class="color-primary"><span class="cl-currency">AED </span>${item.value}</td></tr>`
    });
    $("#top-categories").html(top5_category_html);
    // end - populating html for top 5 category


    // start - populating html for top 5 diets
    data.top_diets.forEach((item, index) => {
      top5_diet_html += `<tr><td>${item.label}</td><td class="color-primary"><span class="cl-currency">AED </span>${item.value}</td></tr>`
    });
    $("#top-diets").html(top5_diet_html);
    // end - populating html for top 5 diets

    // start - populating html for top 5 products
    data.top_products.forEach((item, index) => {
      top5_products_html += `<tr><td>${item.label}</td><td class="color-primary"><span class="cl-currency">AED </span>${item.value}</td></tr>`
    });
    $("#top-products").html(top5_products_html);
    // end - populating html for top 5 products

    // start - creating pie chart
    Object.keys(data.all_categories).length === 0 ? $("#category-pie").html("No Data Available") : drawPieChart({
      data: data.all_categories,
      pie_element: 'category-pie',
      pie_legend_element: '#category-pie-legend'
    });
    Object.keys(data.all_diets).length === 0 ? $("#diet-pie").html("No Data Available") : drawPieChart({
      data: data.all_diets,
      pie_element: 'diet-pie',
      pie_legend_element: '#diet-pie-legend'
    });
    // end - creating pie chart

    // start - removing spinner object from DOM
    spinner('sales').remove();
    // end - removing spinner object from DOM
  }

  function promiseThenSalesLine(data) {
    let total_sales = data.total_sale === 0 ? 'AED 0.00' : 'AED ' + data.total_sale;
    let users_count = data.users_count === 0 ? '0' : data.users_count;
    let repeated_users_count = data.repeated_users_count === 0 ? '0' : data.repeated_users_count;

    // start - clearing the pie chart and its legends for regeneration
    $("#saleslinegraph").empty();
    $("#sales-linegraph-legend").html('');
    // end - clearing the pie chart and its legends for regeneration

    // start - total sales
    total_sales = `<p class="card-text text-left"> ${total_sales}</p>`;
    $("#total_sales_linechart").html(total_sales);
    // end - total sales

    // start - users count
    users_count = `<p class="card-text text-left"> ${users_count}</p>`;
    $("#users_count_linechart").html(users_count);
    // end - users count

    // start - repeated users count
    repeated_users_count = `<p class="card-text text-left"> ${repeated_users_count}</p>`;
    $("#repeated_users_count_linechart").html(repeated_users_count);
    // end - repeated users count

    // start - creating line graph
    Object.keys(data.linegraph_data).length === 0 ? $("#saleslinegraph").html("No Data Available") : drawLineGraph({
      data: data.linegraph_data,
      element: 'saleslinegraph',
      legend_element: '#sales-linegraph-legend',
      xkey: 'day',
      ykeys: data.linegraph_ykeys,
      labels: data.linegraph_ykeys,
    });
    if($("#saleslinegraph").html() == "No Data Available") {
        $("#saleslinegraph").addClass('p-t-25');
        $("#saleslinegraph").removeClass('p-t-140');
    } else {
        $("#saleslinegraph").addClass('p-t-140');
        $("#saleslinegraph").removeClass('p-t-25');
    }

    // end - creating line graph

    // start - removing spinner object from DOM
    spinner('saleslinechart').remove();
    // end - removing spinner object from DOM
  }

  // start - promise callback for hourlysales tab
  function promiseThenSalesHourly(data) {
    let total_sales = data.total_sale === 0 ? 'AED 0.00' : 'AED ' + data.total_sale;
    let users_count = data.users_count === 0 ? '0' : data.users_count;
    let repeated_users_count = data.repeated_users_count === 0 ? '0' : data.repeated_users_count;

    // start - clearing the pie chart and its legends for regeneration
    $("#hourlysaleslinegraph").empty();
    $("#hourlysaleslinegraph-legend").html('');
    // end - clearing the pie chart and its legends for regeneration

    // start - total sales
    total_sales = `<p class="card-text text-left"> ${total_sales}</p>`;
    $("#total_sales_hourly").html(total_sales);
    // end - total sales

    // start - users count
    users_count = `<p class="card-text text-left"> ${users_count}</p>`;
    $("#users_count_hourly").html(users_count);
    // end - users count

    // start - repeated users count
    repeated_users_count = `<p class="card-text text-left"> ${repeated_users_count}</p>`;
    $("#repeated_users_count_hourly").html(repeated_users_count);
    // end - repeated users count

    // start - creating line graph
    Object.keys(data.hourly_data).length === 0 ? $("#hourlysaleslinegraph").html("No Data Available") : drawLineGraph({
      data: data.hourly_data,
      element: 'hourlysaleslinegraph',
      legend_element: '#hourlysaleslinegraph-legend',
      xkey: 'time',
      ykeys: ['sales'],
      labels: 'sales'
    });

    // end - creating line graph

    // start - removing spinner object from DOM
    spinner('hourlysales').remove();
    // end - removing spinner object from DOM
  }

  // end - promise callback for hourlysales tab

  // start - promise callback for destination tab 
  function promiseThenSalesDestination(data) {
    let total_sales = data.total_sale === 0 ? 'AED 0.00' : 'AED ' + data.total_sale;
    let users_count = data.users_count === 0 ? '0' : data.users_count;
    let repeated_users_count = data.repeated_users_count === 0 ? '0' : data.repeated_users_count;

    // start - clearing the pie chart and its legends for regeneration
    $("#destinationgraph").empty();
    // $("#sales-linegraph-legend").html('');
    // end - clearing the pie chart and its legends for regeneration

    // start - total sales
    total_sales = `<p class="card-text text-left"> ${total_sales}</p>`;
    $("#total_sales_destination").html(total_sales);
    // end - total sales

    // start - users count
    users_count = `<p class="card-text text-left"> ${users_count}</p>`;
    $("#users_count_destination").html(users_count);
    // end - users count

    // start - repeated users count
    repeated_users_count = `<p class="card-text text-left"> ${repeated_users_count}</p>`;
    $("#repeated_users_count_destination").html(repeated_users_count);
    // end - repeated users count

    // start - creating line graph
    Object.keys(data.bargraph_data).length === 0 ? $("#destinationgraph").html("</br> No Data Available") : drawBarGraph({
      data: data.bargraph_data,
      element: 'destinationgraph',
      ykeys: ['a'],
      // legend_element: '#sales-linegraph-legend',
    });
    // end - creating line graph

    // start - removing spinner object from DOM
    spinner('destination').remove();
    // end - removing spinner object from DOM
  }
  // end - promise callback for destination tab 

// start - clear filter for sales chart
  $("#clear_filter").click(function(){
    $('#filter_category').val(null).trigger('change');
    $('#filter_diet').val(null).trigger('change');
    $('#daterange-sales').data('daterangepicker').setStartDate(moment().subtract(29, 'days'));
    $('#daterange-sales').data('daterangepicker').setEndDate(moment());
    $('#daterange-sales span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
    getData({
      url: `${base_url}/admin/reports/salestab/${$('#daterange-sales').data('daterangepicker').startDate.format('YYYY-MM-DD')}/${$('#daterange-sales').data('daterangepicker').endDate.format('YYYY-MM-DD')}/[]/[]`,
      category: '[]',
      diet: '[]',
      start_date: $('#daterange-sales').data('daterangepicker').startDate.format('YYYY-MM-DD'),
      end_date: $('#daterange-sales').data('daterangepicker').endDate.format('YYYY-MM-DD'),
      spinner_id: 'sales'
    }).then((data) => {
      promiseThenSales(data);
    }).then((data) => {
      $(window).trigger('resize');
    }).
    catch((data) => {
      spinner('sales').remove();
    });
  })
  // end - clear filter for sales line chart

  // start - clear filter for sales line chart
  $("#clear_filter_saleslinechart").click(function(){
    $('#filter_category_sales_linechart').val(null).trigger('change');
    $('#filter_diet_sales_linechart').val(null).trigger('change');
    $('#daterange-sales-linechart').data('daterangepicker').setStartDate(moment().subtract(29, 'days'));
    $('#daterange-sales-linechart').data('daterangepicker').setEndDate(moment());
    $('#daterange-sales-linechart span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
    getData({
      url: `${base_url}/admin/reports/linecharttab/${$('#daterange-sales-linechart').data('daterangepicker').startDate.format('YYYY-MM-DD')}/${$('#daterange-sales-linechart').data('daterangepicker').endDate.format('YYYY-MM-DD')}/[]/[]`,
      category: '[]',
      diet: '[]',
      start_date: $('#daterange-sales-linechart').data('daterangepicker').startDate.format('YYYY-MM-DD'),
      end_date: $('#daterange-sales-linechart').data('daterangepicker').endDate.format('YYYY-MM-DD'),
      spinner_id: 'saleslinechart'
    }).then((data) => {
      promiseThenSalesLine(data);
    }).catch((data) => {
      spinner('saleslinechart').remove();
    });
  })
  // end - clear filter for sales line chart

  // start - clear filter for destination
  $("#clear_filter_destination").click(function(){
    $('#filter_category_destination').val(null).trigger('change');
    $('#filter_diet_destination').val(null).trigger('change');
    $('#daterange-destination').data('daterangepicker').setStartDate(moment().subtract(29, 'days'));
    $('#daterange-destination').data('daterangepicker').setEndDate(moment());
    $('#daterange-destination span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
    getData({
      url: `${base_url}/admin/reports/destinationtab/${$('#daterange-destination').data('daterangepicker').startDate.format('YYYY-MM-DD')}/${$('#daterange-destination').data('daterangepicker').endDate.format('YYYY-MM-DD')}/[]/[]`,
      category: '[]',
      diet: '[]',
      start_date: $('#daterange-destination').data('daterangepicker').startDate.format('YYYY-MM-DD'),
      end_date: $('#daterange-destination').data('daterangepicker').endDate.format('YYYY-MM-DD'),
      spinner_id: 'destination'
    }).then((data) => {
      promiseThenSalesDestination(data);
    }).catch((data) => {
      spinner('destination').remove();
    });
  })
  // end - clear filter for destination

  // start - clear filter for hourly sales
  $("#clear_filter_hourlysales").click(function(){
    $('#daterange-hourlysales').data('daterangepicker').setStartDate(moment().subtract(29, 'days'));
    $('#daterange-hourlysales').data('daterangepicker').setEndDate(moment());
    $('#daterange-hourlysales span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
    getData({
      url: `${base_url}/admin/reports/hourlytab/${$('#daterange-hourlysales').data('daterangepicker').startDate.format('YYYY-MM-DD')}/${$('#daterange-hourlysales').data('daterangepicker').endDate.format('YYYY-MM-DD')}`,
      category: '[]',
      diet: '[]',
      start_date: $('#daterange-hourlysales').data('daterangepicker').startDate.format('YYYY-MM-DD'),
      end_date: $('#daterange-hourlysales').data('daterangepicker').endDate.format('YYYY-MM-DD'),
      spinner_id: 'hourlysales'
    }).
    then((data) => {
        promiseThenSalesHourly(data);
      }).then((data) => {
        $(window).trigger('resize');
      })
      .catch((data) => {
        console.error(data);
        // start - removing spinner object from DOM
        spinner('hourlysales').remove();
        // end - removing spinner object from DOM
      });
  })
  // end - clear filter for hourly sales


  $("#filter").click(function () {
    let category = $("#filter_category").val() === null ? '[]' : `[${$("#filter_category").val()}]`;
    let diet = $("#filter_diet").val() === null ? '[]' : `[${$("#filter_diet").val()}]`;
    getData({
      url: `${base_url}/admin/reports/salestab/${$('#daterange-sales').data('daterangepicker').startDate.format('YYYY-MM-DD')}/${$('#daterange-sales').data('daterangepicker').endDate.format('YYYY-MM-DD')}/${category}/${diet}`,
      category: category,
      diet: diet,
      start_date: $('#daterange-sales').data('daterangepicker').startDate.format('YYYY-MM-DD'),
      end_date: $('#daterange-sales').data('daterangepicker').endDate.format('YYYY-MM-DD'),
      spinner_id: 'sales'
    }).then((data) => {
      promiseThenSales(data);
    }).then((data) => {
      $(window).trigger('resize');
    }).
    catch((data) => {
      spinner('sales').remove();
    });
  });

  $("#filter_saleslinechart").click(function () {
    let category = $("#filter_category_sales_linechart").val() === null ? '[]' : `[${$("#filter_category_sales_linechart").val()}]`;
    let diet = $("#filter_diet_sales_linechart").val() === null ? '[]' : `[${$("#filter_diet_sales_linechart").val()}]`;
    getData({
      url: `${base_url}/admin/reports/linecharttab/${$('#daterange-sales-linechart').data('daterangepicker').startDate.format('YYYY-MM-DD')}/${$('#daterange-sales-linechart').data('daterangepicker').endDate.format('YYYY-MM-DD')}/${category}/${diet}`,
      category: category,
      diet: diet,
      start_date: $('#daterange-sales-linechart').data('daterangepicker').startDate.format('YYYY-MM-DD'),
      end_date: $('#daterange-sales-linechart').data('daterangepicker').endDate.format('YYYY-MM-DD'),
      spinner_id: 'saleslinechart'
    }).then((data) => {
      promiseThenSalesLine(data);
    }).catch((data) => {
      spinner('saleslinechart').remove();
    });
  });

  $("#filter_destination").click(function () {
    let category = $("#filter_category_destination").val() === null ? '[]' : `[${$("#filter_category_destination").val()}]`;
    let diet = $("#filter_diet_destination").val() === null ? '[]' : `[${$("#filter_diet_destination").val()}]`;
    getData({
      url: `${base_url}/admin/reports/destinationtab/${$('#daterange-destination').data('daterangepicker').startDate.format('YYYY-MM-DD')}/${$('#daterange-destination').data('daterangepicker').endDate.format('YYYY-MM-DD')}/${category}/${diet}`,
      category: category,
      diet: diet,
      start_date: $('#daterange-destination').data('daterangepicker').startDate.format('YYYY-MM-DD'),
      end_date: $('#daterange-destination').data('daterangepicker').endDate.format('YYYY-MM-DD'),
      spinner_id: 'destination'
    }).then((data) => {
      promiseThenSalesDestination(data);
    }).catch((data) => {
      spinner('destination').remove();
    });
  });

  $("#filter_hourlysales").click(function () {
    getData({
      url: `${base_url}/admin/reports/hourlytab/${$('#daterange-hourlysales').data('daterangepicker').startDate.format('YYYY-MM-DD')}/${$('#daterange-hourlysales').data('daterangepicker').endDate.format('YYYY-MM-DD')}`,
      category: '[]',
      diet: '[]',
      start_date: $('#daterange-hourlysales').data('daterangepicker').startDate.format('YYYY-MM-DD'),
      end_date: $('#daterange-hourlysales').data('daterangepicker').endDate.format('YYYY-MM-DD'),
      spinner_id: 'hourlysales'
    }).
    then((data) => {
        promiseThenSalesHourly(data);
      }).then((data) => {
        $(window).trigger('resize');
      })
      .catch((data) => {
        console.error(data);
        // start - removing spinner object from DOM
        spinner('hourlysales').remove();
        // end - removing spinner object from DOM
      });
  });


  // start - displaying first tab by default'
  $('#reports a[href="#sales"]').tab('show') // Select tab by name
  // end - displaying first tab by default'
});