$(window).on("load", function () {

  if ($(".about").is(":visible")) {

    $.fn.isOnScreen = function () {

      var win = $(window);

      var viewport = {
        top: win.scrollTop(),
        left: win.scrollLeft()
      };
      viewport.right = viewport.left + win.width();
      viewport.bottom = viewport.top + win.height();

      var bounds = this.offset();
      bounds.right = bounds.left + this.outerWidth();
      bounds.bottom = bounds.top + this.outerHeight();

      return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));

    };
  }

$(document).on("click" , function() { 
    $(".menu-page .tabs1 .filter-list").slideUp();
});

$(".menu-page .tabs1 .filter-list a").on("click" , function(e) { 
        e.stopPropagation();
});

  $.fn.slideFadeToggle = function (speed, easing, callback) {
    return this.animate({ opacity: 'toggle', height: 'toggle' }, speed, easing, callback);
  };
  
  $("#header").headroom();
  
  $('#faq h4').click(function () {
    var $this = $(this);
    if ($(this).hasClass("open")) {
      $this.next().slideFadeToggle("500", function () {
        $this.removeClass("open");
      });
    } else {
      $this.addClass("open");
      $this.next().slideFadeToggle("500");
    }
  });


  $('.open-menu-btn').click(function () {
    $('body').toggleClass('open-menu');
    return false;
  });

  $('.close-menu-btn').click(function () {
    $('body').removeClass('open-menu');
    return false;
  });

  $(".figure1").each(function () {
    var jQuerySrc = $(this).find("img").attr("src");
    $(this).find('img').css("visibility", "hidden");
    $(this).css("background-image", "url(" + jQuerySrc + ")");
  });

  // $(".same-height").matchHeight();

  $('[data-toggle="tooltip"]').tooltip();

  $(".scroll-to-top").on("click", function () {
    $("body,html").animate({
      scrollTop: 0
    });
    return false;
  });

  $(document).on("click", ".trigger-order", function () {
    if ($(window).width() >= 768) {
      $("body").addClass("order-popup");
      $("body").addClass("no-transition");
    } else {
      $("body").addClass("popup-open");
    }
    $('#flight_popup').show();
    $(".order .close").show();
    return false;
  });

  $(".order .close").on("click", function () {
    $(".order").removeClass("scrollLeft");
    $("body").removeClass("popup-open order-popup");
    setTimeout(function () {
      $("body").removeClass("no-transition");
    }, 500)
    $(".order .close").hide();
  });

  $('#banner .slides').cycle({
    slideExpr: '.slide',
    fx: 'fade',
    timeout: 5000,
    auto: true,
    speed: 1000,
    next: "#banner .next-btn",
    fit: 1,
    pager: '#banner .controls .container',
    slideResize: 0,
    containerResize: 0,
    height: 'auto',
    before: onBefore,
    after: onAfter,
    width: null
  });

  if ($('.slideshow2 .slides2 .slide').length > 1) {
    $(".slideshow2 .slide-controls").show();
  }

  $('.slideshow2 .slides2').cycle({
    slideExpr: '.slide',
    fx: 'fade',
    timeout: 5000,
    auto: true,
    speed: 1000,
    next: ".slideshow2 .slide-controls .next",
    prev: ".slideshow2 .slide-controls .prev",
    fit: 1,
    pager: '.slideshow2 .controls2',
    slideResize: 0,
    containerResize: 0,
    height: 'auto',
    before: onBefore,
    after: onAfter,
    width: null
  });
  $('.slideshow3 .slides').cycle({
    slideExpr: '.slide',
    fx: 'fade',
    timeout: 5000,
    auto: true,
    speed: 1000,
    fit: 1,
    pager: '.slideshow3 .pager .container',
    slideResize: 0,
    containerResize: 0,
    height: 'auto',
    before: onBefore,
    after: onAfter,
    width: null
  });

  jQuery('.slideshow3 .slides').height(jQuery('.slideshow3 .slide figure img').innerHeight());

  $("#banner .video-slide .play-btn").on("click", function () {
    $('#banner .slides').cycle('pause');
    $("body").addClass("video-playing");
    $("#header.headroom").removeClass("headroom--pinned").addClass("headroom--unpinned");
    $(".order").addClass("scrollRight");
    $(this).parents(".slide").find("h2").addClass("slideLeft");
    $(this).parents(".slide").find(".button1").addClass("slideLeft");
    $(this).parents(".slide").find(".figure1").fadeOut();
    $("#banner .close").show();
    $("#" + $(this).parents(".slide").attr("data-slide"))[0].play();
    $(this).fadeOut();
    return false;
  });

  $("#banner video").on("ended", function () {
    $("#banner .description .display-1, #banner .button1").removeClass("slideLeft");
    $("body").removeClass("video-playing");
    $("#banner .video-slide .play-btn ,#banner .video-slide .figure1").fadeIn();
    $('#banner .slides').cycle('resume');
    $("#banner .close").hide();
    $(".order").removeClass("scrollRight");
    $("#header.headroom").removeClass("headroom--unpinned").addClass("headroom--pinned");
  });

  function onBefore() {
    if ($("#banner .close").is(":visible")) {
      $("#banner .description .display-1, #banner .button1").removeClass("slideLeft");
      $("#banner .video-slide .play-btn ,#banner .video-slide .figure1").fadeIn();
      $('#banner .slides').cycle('resume');
      $(".order").removeClass("scrollRight");
      $("#banner .close").hide();
      $("body").removeClass("video-playing");
      $("#header.headroom").removeClass("headroom--unpinned").addClass("headroom--pinned");
    }
  }
  function onAfter() {
    if ($("#banner .close").is(":visible")) {
      $("#banner .description .display-1, #banner .button1").removeClass("slideLeft");
      $("#banner .video-slide .play-btn ,#banner .video-slide .figure1").fadeIn();
      $('#banner .slides').cycle('resume');
      $(".order").removeClass("scrollRight");
      $("body").removeClass("video-playing");
      $("#banner .close").hide();
      $("#header.headroom").removeClass("headroom--unpinned").addClass("headroom--pinned");
    }
  }

  $("#banner .close").on("click", function () {
    $("#banner .description .display-1, #banner .button1").removeClass("slideLeft");
    $("#banner .video-slide .play-btn ,#banner .video-slide .figure1").fadeIn();
    $('#banner .slides').cycle('resume');
    $(".order").removeClass("scrollRight");
    $("#banner .close").hide();
    $("body").removeClass("video-playing");
    if (typeof $("#slide1")[0] != "undefined") {
      $("#slide1")[0].pause();
    }
    $("#header.headroom").removeClass("headroom--unpinned").addClass("headroom--pinned");
  });

  $(".order .date input").datepicker();

  $('.filter-btn').on('click', function (evt) {
    evt.stopImmediatePropagation();
    $(".filter-section").toggleClass("open");
    $(".filter-list").slideToggle();
  });

  $('.ingredients .header .toggle-btn').on('click', function () {
    if ($(this).hasClass("open")) {
      $(this).removeClass("open");
      $(".widget.ingredients .content").slideUp();
    } else {
      $(this).addClass("open");
      $(".widget.ingredients .content").slideDown();
    }
    return false;
  });

  $('.addons .header .toggle-btn').on('click', function () {
    if ($(this).hasClass("open")) {
      $(this).removeClass("open");
      $(".widget.addons .content").slideUp();
    } else {
      $(this).addClass("open");
      $(".widget.addons .content").slideDown();
    }
    return false;
  });

  $("#addon_close").on('click', function () {
    sessionStorage.removeItem("addonPrice");
  });

  $(".checkout > ul li a").on('click', function () {
    $(".checkout > ul li , #register-form , #login-form").removeClass("active");
    $(this).parent().addClass("active");
    $("div" + $(this).attr("href")).addClass("active");
    return false;
  });

  objectFitImages();

  $(window).on("scroll", function () {
    if ($(".about").is(":visible")) {
      if ($('.about').isOnScreen() == true) {
        $(".about .fuel-for-journey").addClass("show")
      }
    }

    if ($(this).scrollTop() > 1) {
      $('body').addClass("scroll");
    }
    else {
      $('body').removeClass("scroll");
    }

    // $(".order").removeClass("scrollLeft");

    if ($(window).width() >= 768) {
      $("#ui-datepicker-div, .order .close").hide();

      $(".order .date input").blur();
    }

    // if ($(window).scrollTop() >= 300) {
    // $(".order").addClass("scrollRight");
    // } else {
    //  $(".order").removeClass("scrollRight");
    // }

  });

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });


  if (document.getElementById) {
    window.alert = function (txt) {
      createCustomAlert(txt);
    }
  }

  function createCustomAlert(txt) {
    d = document;

    if (d.getElementById("modalContainer")) {
      $('#alertBox p').text(txt);
      $('#modalContainer').fadeIn();
      setTimeout(function () {
        removeCustomAlert();
      }, 1000);

      return;
    }
    mObj = d.getElementsByTagName("body")[0].appendChild(d.createElement("div"));
    mObj.id = "modalContainer";
    // mObj.style.height = d.documentElement.scrollHeight + "px";
    alertObj = mObj.appendChild(d.createElement("div"));
    alertObj.id = "alertBox";
    if (d.all && !window.opera) alertObj.style.top = document.documentElement.scrollTop + "px";
    alertObj.style.left = "35%";
    alertObj.style.visiblity = "visible";
    msg = alertObj.appendChild(d.createElement("p"));
    msg.innerHTML = txt;
    alertObj.style.display = "block";

    setTimeout(function () {
      removeCustomAlert();
    }, 1000);
  }

  function removeCustomAlert() {
    $('#modalContainer').fadeOut(1000);
  }

});


$(function () {
  // $('[data-toggle="tooltip"]').tooltip()
  $('body').tooltip({
    selector: '[data-toggle=tooltip]'
  });
})


