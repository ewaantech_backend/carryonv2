$(window).on("load", function () {

  if ($(".about").is(":visible")) {

    $.fn.isOnScreen = function () {

      var win = $(window);

      var viewport = {
        top: win.scrollTop(),
        left: win.scrollLeft()
      };
      viewport.right = viewport.left + win.width();
      viewport.bottom = viewport.top + win.height();

      var bounds = this.offset();
      bounds.right = bounds.left + this.outerWidth();
      bounds.bottom = bounds.top + this.outerHeight();

      return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));

    };
  }


  $("#header").headroom();

  AOS.init({
    duration: 400,
    easing: 'ease',
    once: true,
    mirror: false
  });

  $('.open-menu-btn').click(function () {
    $('body').toggleClass('open-menu');
    return false;
  });

  $('.close-menu-btn').click(function () {
    $('body').removeClass('open-menu');
    return false;
  });

  $(".figure1").each(function () {
    var jQuerySrc = $(this).find("img").attr("src");
    $(this).find('img').css("visibility", "hidden");
    $(this).css("background-image", "url(" + jQuerySrc + ")");
  });

  $(".same-height").matchHeight();

  $('[data-toggle="tooltip"]').tooltip();

  $(".scroll-to-top").on("click", function () {
    $("body,html").animate({
      scrollTop: 0
    });
    return false;
  });

  $(".trigger-order").on("click", function () {
    if ($(window).width() >= 768) {
      $(".order").addClass("scrollLeft");
      $(".order .close").show();
    } else {
      $("body").addClass("popup-open");
      $(".order .close").show();
    }
    return false;
  });

  $(".order .close").on("click", function () {
    $(".order").removeClass("scrollLeft");
    $("body").removeClass("popup-open");
    $(".order .close").hide();
  });

  $('#banner .slides').cycle({
    slideExpr: '.slide',
    fx: 'fade',
    timeout: 5000,
    auto: true,
    speed: 1000,
    next: "#banner .next-btn",
    fit: 1,
    pager: '#banner .controls .container',
    slideResize: 0,
    containerResize: 0,
    height: 'auto',
    before: onBefore,
    after: onAfter,
    width: null
  });

  $('.slideshow2 .slides2').cycle({
    slideExpr: '.slide',
    fx: 'fade',
    timeout: 5000,
    auto: true,
    speed: 1000,
    next: ".slideshow2 .slide-controls .next",
    prev: ".slideshow2 .slide-controls .prev",
    fit: 1,
    pager: '.slideshow2 .controls2',
    slideResize: 0,
    containerResize: 0,
    height: 'auto',
    before: onBefore,
    after: onAfter,
    width: null
  });

  $("#banner .video-slide .play-btn").on("click", function () {
    $('#banner .slides').cycle('pause');
    $("#header.headroom").removeClass("headroom--pinned").addClass("headroom--unpinned");
    $(".order").addClass("scrollRight");
    $(this).parents(".slide").find("h2").addClass("slideLeft");
    $(this).parents(".slide").find(".button1").addClass("slideLeft");
    $(this).parents(".slide").find(".figure1").fadeOut();
    $("#banner .close").show();
    $("#" + $(this).parents(".slide").attr("data-slide"))[0].play();
    $(this).fadeOut();
    return false;
  });

  $("#banner video").on("ended", function () {
    $("#banner .description .display-1, #banner .button1").removeClass("slideLeft");
    $("#banner .video-slide .play-btn ,#banner .video-slide .figure1").fadeIn();
    $('#banner .slides').cycle('resume');
    $("#banner .close").hide();
    $(".order").removeClass("scrollRight");
    $("#header.headroom").removeClass("headroom--unpinned").addClass("headroom--pinned");
  });

  function onBefore() {
    if ($("#banner .close").is(":visible")) {
      $("#banner .description .display-1, #banner .button1").removeClass("slideLeft");
      $("#banner .video-slide .play-btn ,#banner .video-slide .figure1").fadeIn();
      $('#banner .slides').cycle('resume');
      $(".order").removeClass("scrollRight");
      $("#banner .close").hide();
      $("#header.headroom").removeClass("headroom--unpinned").addClass("headroom--pinned");
    }
  }
  function onAfter() {
    if ($("#banner .close").is(":visible")) {
      $("#banner .description .display-1, #banner .button1").removeClass("slideLeft");
      $("#banner .video-slide .play-btn ,#banner .video-slide .figure1").fadeIn();
      $('#banner .slides').cycle('resume');
      $(".order").removeClass("scrollRight");
      $("#banner .close").hide();
      $("#header.headroom").removeClass("headroom--unpinned").addClass("headroom--pinned");
    }
  }

  $("#banner .close").on("click", function () {
    $("#banner .description .display-1, #banner .button1").removeClass("slideLeft");
    $("#banner .video-slide .play-btn ,#banner .video-slide .figure1").fadeIn();
    $('#banner .slides').cycle('resume');
    $(".order").removeClass("scrollRight");
    $("#banner .close").hide();
    $("#header.headroom").removeClass("headroom--unpinned").addClass("headroom--pinned");
  });

  $(".order .date input").datepicker();

  $('.add').on('click', function () {
    var $qty = $(this).parents('.quantity').find('.input-control');
    var currentVal = parseInt($qty.val());
    if (!isNaN(currentVal)) {
      $qty.val(currentVal + 1);
    }
  });

  $('.less').on('click', function () {
    var $qty = $(this).parents('.quantity').find('.input-control');
    var currentVal = parseInt($qty.val());
    if (!isNaN(currentVal) && currentVal > 0) {
      $qty.val(currentVal - 1);
    }
  });

  $('.filter-btn').on('click', function () {
    $(".filter-section").toggleClass("open");
    $(".filter-list").slideToggle();
  });

  $('.ingredients .header .toggle-btn').on('click', function () {
    if ($(this).hasClass("open")) {
      $(this).removeClass("open");
      $(".widget.ingredients .content").slideUp();
    } else {
      $(this).addClass("open");
      $(".widget.ingredients .content").slideDown();
    }
    return false;
  });

  $("#checkout > ul li a").on('click', function () {
    $("#checkout > ul li , #register , #login").removeClass("active");
    $(this).parent().addClass("active");
    $("div" + $(this).attr("href")).addClass("active");
    return false;
  });

  objectFitImages();

  $(window).on("scroll", function () {
    if ($(".about").is(":visible")) {
      if ($('.about').isOnScreen() == true) {
        $(".about .fuel-for-journey").addClass("show")
      }
    }

    $(".order").removeClass("scrollLeft");

    if ($(window).width() >= 768) {
      $("#ui-datepicker-div, .order .close").hide();

      $(".order .date input").blur();
    }

    if ($(window).scrollTop() >= 300) {
      $(".order").addClass("scrollRight");
    } else {
      $(".order").removeClass("scrollRight");
    }

  });
});

