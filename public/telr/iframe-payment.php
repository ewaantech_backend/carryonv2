<?php

$mode = 0; // 1 for test, 0 for live

$params = array(
        'ivp_method'  => 'create',
        'ivp_store'   => '22999',
        'ivp_authkey' => 'CzR46^fcVJ#C4gRP',
		'ivp_framed'  => 1,
        'ivp_cart'    => 'ORD-' . rand(100000, 10000000),  
        'ivp_test'    => $mode,
        'ivp_amount'  => '98.45',
        'ivp_currency'=> 'AED',
        'ivp_desc'    => '2 Large Pizza + 1 Free',
        'return_auth' => 'https://carryon.ourdemo.online/telr/auth.php',
        'return_can'  => 'https://carryon.ourdemo.online/telr/can.php',
        'return_decl' => 'https://carryon.ourdemo.online/telr/decl.php',
        'bill_fname' => 'Nishad',
        'bill_sname' => 'Aliyar',
        'bill_addr1' => 'Business Bay',
        'bill_city'  => 'Dubai',
        'bill_country' => 'AE',
        'bill_email' => 'nishadaliar@gmail.com'
    );

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://secure.telr.com/gateway/order.json");
curl_setopt($ch, CURLOPT_POST, count($params));
curl_setopt($ch, CURLOPT_POSTFIELDS,$params);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
$results = curl_exec($ch);
curl_close($ch);

$results = json_decode($results,true);
$_SESSION['sess_payment_ref'] = $ref = trim($results['order']['ref']);
$url= trim($results['order']['url']);

//echo '<pre>';print_r($results);echo '</pre>';exit;

if (empty($ref) || empty($url)) {
	exit;
} else {
	echo '<div id="telr-div"><iframe id="telr" src="' . $url . '"></iframe></div>';
}

?>
<style>
#telr {
	min-width: 520px;
    height: 540px;
    border: 0;
}
#telr-div{
	width: 100%;
    text-align: center;
}
</style>
