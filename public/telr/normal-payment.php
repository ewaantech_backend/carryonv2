<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$mode = 1; // 1 for test, 0 for live

$params = array(
        'ivp_method'  => 'create',
        'ivp_store'   => '22999',
        'ivp_authkey' => 'CzR46^fcVJ#C4gRP',
        'ivp_cart'    => rand(100000, 10000000),  
        'ivp_test'    => $mode,
        'ivp_amount'  => '1.00',
        'ivp_currency'=> 'AED',
        'ivp_desc'    => 'Product Description',
        'return_auth' => 'https://carryon.ourdemo.online/telr/auth.php',
        'return_can'  => 'https://carryon.ourdemo.online/telr/can.php',
        'return_decl' => 'https://carryon.ourdemo.online/telr/decl.php',
        'bill_fname' => 'Nishad',
        'bill_sname' => 'Aliyar',
        'bill_addr1' => 'Business Bay',
        'bill_city'  => 'Dubai',
        'bill_country' => 'AE',
        'bill_email' => 'nishadaliar@gmail.com'
    );

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, "https://secure.telr.com/gateway/order.json");
curl_setopt($ch, CURLOPT_POST, count($params));
curl_setopt($ch, CURLOPT_POSTFIELDS,$params);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
$results = curl_exec($ch);
curl_close($ch);

$results = json_decode($results,true);
$ref= trim($results['order']['ref']);
$url= trim($results['order']['url']);

//echo '<pre>';print_r($results);echo '</pre>';exit;

if (empty($ref) || empty($url)) {
	exit;
} else {
	header("Location: " . $url);
}

?>
