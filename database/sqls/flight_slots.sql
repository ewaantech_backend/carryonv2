-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 11, 2020 at 01:35 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `carryon_demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `flight_slots`
--

CREATE TABLE `flight_slots` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `time_from` time NOT NULL,
  `time_to` time NOT NULL,
  `pickup_schedule` time NOT NULL,
  `last_order` time NOT NULL,
  `in_kitchen` time NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `flight_slots`
--

INSERT INTO `flight_slots` (`id`, `time_from`, `time_to`, `pickup_schedule`, `last_order`, `in_kitchen`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '09:00:00', '11:59:00', '06:15:00', '05:00:00', '05:15:00', NULL, '0000-00-00 00:00:00', NULL),
(2, '12:00:00', '14:59:00', '09:15:00', '08:00:00', '08:15:00', NULL, NULL, NULL),
(3, '15:00:00', '17:59:00', '12:15:00', '11:00:00', '11:15:00', NULL, NULL, NULL),
(4, '18:00:00', '20:59:00', '15:15:00', '14:00:00', '14:15:00', NULL, NULL, NULL),
(5, '21:00:00', '23:59:00', '18:15:00', '17:00:00', '17:15:00', NULL, NULL, NULL),
(6, '00:00:00', '02:59:00', '21:15:00', '18:00:00', '18:15:00', NULL, NULL, NULL),
(7, '03:00:00', '05:59:00', '00:15:00', '23:00:00', '23:15:00', NULL, NULL, NULL),
(8, '06:00:00', '08:59:00', '03:15:00', '02:00:00', '02:15:00', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `flight_slots`
--
ALTER TABLE `flight_slots`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `flight_slots`
--
ALTER TABLE `flight_slots`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
