-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 11, 2020 at 01:36 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `carryon_demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `boarding_pickup_map`
--

CREATE TABLE `boarding_pickup_map` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `terminal` int(11) NOT NULL,
  `gate` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pickup_point` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `default` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `map_page_param` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `boarding_pickup_map`
--

INSERT INTO `boarding_pickup_map` (`id`, `terminal`, `gate`, `pickup_point`, `default`, `map_page_param`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 3, 'A1', 'Tree House', 'Grabbit', 'from=XX&to=Tree House', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(2, 3, 'A2', 'Tree House', 'Grabbit', 'from=XX&to=Tree House', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(3, 3, 'A3', 'Tree House', 'Grabbit', 'from=XX&to=Tree House', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(4, 3, 'A4', 'Tree House', 'Grabbit', 'from=XX&to=Tree House', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(5, 3, 'A5', 'Tree House', 'Grabbit', 'from=XX&to=Tree House', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(6, 3, 'A6', 'Tree House', 'Grabbit', 'from=XX&to=Tree House', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(7, 3, 'A7', 'Tree House', 'Grabbit', 'from=XX&to=Tree House', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(8, 3, 'A8', 'Tree House', 'Grabbit', 'from=XX&to=Tree House', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(9, 3, 'A9', 'Tree House', 'Grabbit', 'from=XX&to=Tree House', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(10, 3, 'A10', 'Tree House', 'Grabbit', 'from=XX&to=Tree House', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(11, 3, 'A11', 'Tree House', 'Grabbit', 'from=XX&to=Tree House', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(12, 3, 'A12', 'Tree House', 'Grabbit', 'from=XX&to=Tree House', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(13, 3, 'A13', 'Fix', 'Grabbit', 'from=XX&to=Fix', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(14, 3, 'A14', 'Fix', 'Grabbit', 'from=XX&to=Fix', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(15, 3, 'A15', 'Fix', 'Grabbit', 'from=XX&to=Fix', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(16, 3, 'A16', 'Fix', 'Grabbit', 'from=XX&to=Fix', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(17, 3, 'A17', 'Fix', 'Grabbit', 'from=XX&to=Fix', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(18, 3, 'A18', 'Fix', 'Grabbit', 'from=XX&to=Fix', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(19, 3, 'A19', 'Fix', 'Grabbit', 'from=XX&to=Fix', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(20, 3, 'A20', 'Fix', 'Grabbit', 'from=XX&to=Fix', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(21, 3, 'A21', 'Fix', 'Grabbit', 'from=XX&to=Fix', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(22, 3, 'A22', 'Fix', 'Grabbit', 'from=XX&to=Fix', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(23, 3, 'A23', 'Fix', 'Grabbit', 'from=XX&to=Fix', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(24, 3, 'A24', 'Fix', 'Grabbit', 'from=XX&to=Fix', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(25, 3, 'B1', 'Grabbit', 'Grabbit', 'from=XX&to=Grabbit', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(26, 3, 'B2', 'Grabbit', 'Grabbit', 'from=XX&to=Grabbit', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(27, 3, 'B3', 'Grabbit', 'Grabbit', 'from=XX&to=Grabbit', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(28, 3, 'B4', 'Grabbit', 'Grabbit', 'from=XX&to=Grabbit', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(29, 3, 'B5', 'Grabbit', 'Grabbit', 'from=XX&to=Grabbit', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(30, 3, 'B6', 'Grabbit', 'Grabbit', 'from=XX&to=Grabbit', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(31, 3, 'B7', 'Grabbit', 'Grabbit', 'from=XX&to=Grabbit', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(32, 3, 'B8', 'Grabbit', 'Grabbit', 'from=XX&to=Grabbit', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(33, 3, 'B9', 'Grabbit', 'Grabbit', 'from=XX&to=Grabbit', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(34, 3, 'B10', 'Grabbit', 'Grabbit', 'from=XX&to=Grabbit', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(35, 3, 'B11', 'Grabbit', 'Grabbit', 'from=XX&to=Grabbit', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(36, 3, 'B12', 'Grabbit', 'Grabbit', 'from=XX&to=Grabbit', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(37, 3, 'B13', 'Grabbit', 'Grabbit', 'from=XX&to=Grabbit', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(38, 3, 'B14', 'Grabbit', 'Grabbit', 'from=XX&to=Grabbit', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(39, 3, 'B15', 'Grabbit', 'Grabbit', 'from=XX&to=Grabbit', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(40, 3, 'B16', 'Grabbit', 'Grabbit', 'from=XX&to=Grabbit', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(41, 3, 'B17', 'Nutella', 'Grabbit', 'from=XX&to=Nutella', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(42, 3, 'B18', 'Nutella', 'Grabbit', 'from=XX&to=Nutella', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(43, 3, 'B19', 'Nutella', 'Grabbit', 'from=XX&to=Nutella', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(44, 3, 'B20', 'Nutella', 'Grabbit', 'from=XX&to=Nutella', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(45, 3, 'B21', 'Nutella', 'Grabbit', 'from=XX&to=Nutella', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(46, 3, 'B22', 'Nutella', 'Grabbit', 'from=XX&to=Nutella', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(47, 3, 'B23', 'Nutella', 'Grabbit', 'from=XX&to=Nutella', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(48, 3, 'B24', 'Nutella', 'Grabbit', 'from=XX&to=Nutella', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(49, 3, 'B25', 'Nutella', 'Grabbit', 'from=XX&to=Nutella', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(50, 3, 'B26', 'Nutella', 'Grabbit', 'from=XX&to=Nutella', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(51, 3, 'B27', 'Nutella', 'Grabbit', 'from=XX&to=Nutella', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(52, 3, 'B28', 'Nutella', 'Grabbit', 'from=XX&to=Nutella', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(53, 3, 'B29', 'Nutella', 'Grabbit', 'from=XX&to=Nutella', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(54, 3, 'B30', 'Nutella', 'Grabbit', 'from=XX&to=Nutella', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(55, 3, 'B31', 'Nutella', 'Grabbit', 'from=XX&to=Nutella', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(56, 3, 'B32', 'Nutella', 'Grabbit', 'from=XX&to=Nutella', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(57, 3, 'C1', 'S34', 'Grabbit', 'from=XX&to=S34', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(58, 3, 'C2', 'S34', 'Grabbit', 'from=XX&to=S35', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(59, 3, 'C3', 'S34', 'Grabbit', 'from=XX&to=S36', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(60, 3, 'C4', 'S34', 'Grabbit', 'from=XX&to=S37', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(61, 3, 'C5', 'S34', 'Grabbit', 'from=XX&to=S38', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(62, 3, 'C6', 'S34', 'Grabbit', 'from=XX&to=S39', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(63, 3, 'C7', 'S34', 'Grabbit', 'from=XX&to=S40', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(64, 3, 'C8', 'S34', 'Grabbit', 'from=XX&to=S41', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(65, 3, 'C9', 'S34', 'Grabbit', 'from=XX&to=S42', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(66, 3, 'C10', 'S34', 'Grabbit', 'from=XX&to=S43', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(67, 3, 'C11', 'S34', 'Grabbit', 'from=XX&to=S44', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(68, 3, 'C12', 'S34', 'Grabbit', 'from=XX&to=S45', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(69, 3, 'C13', 'S34', 'Grabbit', 'from=XX&to=S46', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(70, 3, 'C14', 'S34', 'Grabbit', 'from=XX&to=S47', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(71, 3, 'C15', 'S34', 'Grabbit', 'from=XX&to=S48', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(72, 3, 'C16', 'S34', 'Grabbit', 'from=XX&to=S49', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(73, 3, 'C17', 'S34', 'Grabbit', 'from=XX&to=S50', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(74, 3, 'C18', 'S34', 'Grabbit', 'from=XX&to=S51', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(75, 3, 'C19', 'S34', 'Grabbit', 'from=XX&to=S52', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(76, 3, 'C20', 'S34', 'Grabbit', 'from=XX&to=S53', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(77, 3, 'C21', 'S34', 'Grabbit', 'from=XX&to=S54', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(78, 3, 'C22', 'S34', 'Grabbit', 'from=XX&to=S55', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(79, 3, 'C23', 'S34', 'Grabbit', 'from=XX&to=S56', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(80, 3, 'C24', 'S34', 'Grabbit', 'from=XX&to=S57', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(81, 3, 'C25', 'S34', 'Grabbit', 'from=XX&to=S58', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(82, 3, 'C26', 'S34', 'Grabbit', 'from=XX&to=S59', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(83, 3, 'C27', 'S34', 'Grabbit', 'from=XX&to=S60', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(84, 3, 'C28', 'S34', 'Grabbit', 'from=XX&to=S61', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(85, 3, 'C29', 'S34', 'Grabbit', 'from=XX&to=S62', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(86, 3, 'C30', 'S34', 'Grabbit', 'from=XX&to=S63', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(87, 3, 'C31', 'S34', 'Grabbit', 'from=XX&to=S64', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(88, 3, 'C32', 'S34', 'Grabbit', 'from=XX&to=S65', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(89, 3, 'C33', 'S34', 'Grabbit', 'from=XX&to=S66', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(90, 3, 'C34', 'S34', 'Grabbit', 'from=XX&to=S67', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(91, 3, 'C35', 'S34', 'Grabbit', 'from=XX&to=S68', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(92, 3, 'C36', 'S34', 'Grabbit', 'from=XX&to=S69', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(93, 3, 'C37', 'S34', 'Grabbit', 'from=XX&to=S70', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(94, 3, 'C38', 'S34', 'Grabbit', 'from=XX&to=S71', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(95, 3, 'C39', 'S34', 'Grabbit', 'from=XX&to=S72', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(96, 3, 'C40', 'S34', 'Grabbit', 'from=XX&to=S73', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(97, 3, 'C41', 'S34', 'Grabbit', 'from=XX&to=S74', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(98, 3, 'C42', 'S34', 'Grabbit', 'from=XX&to=S75', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(99, 3, 'C43', 'S34', 'Grabbit', 'from=XX&to=S76', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(100, 3, 'C44', 'S34', 'Grabbit', 'from=XX&to=S77', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(101, 3, 'C45', 'S34', 'Grabbit', 'from=XX&to=S78', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(102, 3, 'C46', 'S34', 'Grabbit', 'from=XX&to=S79', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(103, 3, 'C47', 'S34', 'Grabbit', 'from=XX&to=S80', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(104, 3, 'C48', 'S34', 'Grabbit', 'from=XX&to=S81', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(105, 3, 'C49', 'S34', 'Grabbit', 'from=XX&to=S82', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00'),
(106, 3, 'C50', 'S34', 'Grabbit', 'from=XX&to=S83', NULL, '2020-02-05 13:00:00', '2020-02-05 13:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `boarding_pickup_map`
--
ALTER TABLE `boarding_pickup_map`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `boarding_pickup_map`
--
ALTER TABLE `boarding_pickup_map`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
