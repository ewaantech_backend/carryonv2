INSERT INTO `cms` (`id`, `url_key`, `html_content`, `page_title`, `meta_data`, `meta_description`, `meta_keywords`, `visibility`, `deleted_at`, `created_at`, `updated_at`, `short_code`) VALUES (NULL, 'flight_popup_top', 'Fresh, healthy meals for your flight', 'flight_popup_top', NULL, NULL, NULL, 'YES', NULL, NULL, NULL, 'flight_popup_top');
INSERT INTO `cms` (`id`, `url_key`, `html_content`, `page_title`, `meta_data`, `meta_description`, `meta_keywords`, `visibility`, `deleted_at`, `created_at`, `updated_at`, `short_code`) VALUES (NULL, 'flight_popup_bottom', 'By continuing, you agree with our', 'flight_popup_bottom', NULL, NULL, NULL, 'YES', NULL, NULL, NULL, 'flight_popup_bottom');
INSERT INTO `cms` (`id`, `url_key`, `html_content`, `page_title`, `meta_data`, `meta_description`, `meta_keywords`, `visibility`, `deleted_at`, `created_at`, `updated_at`, `short_code`) VALUES (NULL, 'home_page_content', 'Home page content', 'home_page_content', NULL, NULL, NULL, 'YES', NULL, NULL, NULL, 'home_page_content');
INSERT INTO `cms` (`id`, `url_key`, `html_content`, `page_title`, `meta_data`, `meta_description`, `meta_keywords`, `visibility`, `deleted_at`, `created_at`, `updated_at`, `short_code`) VALUES (NULL, 'billing_info', 'Billing Info', 'billing_info', NULL, NULL, NULL, 'YES', NULL, NULL, NULL, 'billing_info');
INSERT INTO `cms` (`id`, `url_key`, `html_content`, `page_title`, `meta_data`, `meta_description`, `meta_keywords`, `visibility`, `deleted_at`, `created_at`, `updated_at`, `short_code`) VALUES (NULL, 'confirm_pay', 'Confirm Pay', 'Please confirm you’ve verified your order and travel details?', NULL, NULL, NULL, 'YES', NULL, NULL, NULL, 'confirm_pay');
INSERT INTO `cms_slider` (`id`, `title`, `description`, `image_path`, `cover_image_path`, `visibility`, `position`, `deleted_at`, `created_at`, `updated_at`, `short_code`) VALUES (NULL, 'Our Menu', 'The good food culture is back', 'images/frontend/banner-img3.jpg', NULL, 'YES', '0', NULL, NULL, NULL, 'our_menu');
INSERT INTO `cms_slider` (`id`, `title`, `description`, `image_path`, `cover_image_path`, `visibility`, `position`, `deleted_at`, `created_at`, `updated_at`, `short_code`) VALUES (NULL, 'Product Detail', 'The good food culture is back', 'images/frontend/banner-img3.jpg', NULL, 'YES', '0', NULL, NULL, NULL, 'product_detail');
INSERT INTO `cms` (`id`, `url_key`, `html_content`, `page_title`, `meta_data`, `meta_description`, `meta_keywords`, `visibility`, `deleted_at`, `created_at`, `updated_at`, `short_code`) VALUES (NULL, 'Footer Address', ' <p>
                      HWH General Trading LLC<br>
                      451 Lounge <br>
                      Airport Terminal 3 <br>
                      Dubai, UAE
                    </p>', 'Footer Address',NULL, NULL, NULL, 'YES', NULL, NULL, NULL, 'footer_address');
INSERT INTO `cms` (`id`, `url_key`, `html_content`, `page_title`, `meta_data`, `meta_description`, `meta_keywords`, `visibility`, `deleted_at`, `created_at`, `updated_at`, `short_code`) VALUES (NULL, 'Footer Content','We understand human bodies go through changes when flying and the high altitude changes not only the
                  palette, but also the needs, therefore we ensure that the taste and nutritional values sit right.', 'Footer Content', NULL, NULL, NULL, 'YES', NULL, NULL, NULL, 'footer_content');
INSERT INTO `cms` (`id`, `url_key`, `html_content`, `page_title`, `meta_data`, `meta_description`, `meta_keywords`, `visibility`, `deleted_at`, `created_at`, `updated_at`, `short_code`) VALUES (NULL, 'Footer Email', 'hello@carryondxb.com',  'Footer Email',NULL, NULL, NULL, 'YES', NULL, NULL, NULL, 'footer_email');
INSERT INTO `cms` (`id`, `url_key`, `html_content`, `page_title`, `meta_data`, `meta_description`, `meta_keywords`, `visibility`, `deleted_at`, `created_at`, `updated_at`, `short_code`) VALUES (NULL, 'Footer Phone', '+971 50 298 7966',  'Footer Phone',NULL, NULL, NULL, 'YES', NULL, NULL, NULL, 'footer_phone');
INSERT INTO `cms` (`id`, `url_key`, `html_content`, `page_title`, `meta_data`, `meta_description`, `meta_keywords`, `visibility`, `deleted_at`, `created_at`, `updated_at`, `short_code`) VALUES (NULL, 'Footer Copyright','Copyright © 2020 HWH General Trading LLC. All rights reserved.',  'Footer Copyright', NULL, NULL, NULL, 'YES', NULL, NULL, NULL, 'footer_copyright');


----Social Media----------
INSERT INTO `cms` (`id`, `url_key`, `html_content`, `page_title`, `meta_data`, `meta_description`, `meta_keywords`, `visibility`, `deleted_at`, `created_at`, `updated_at`, `short_code`, `social_media_name`, `social_media_link`) VALUES (NULL, 'facebook', 'facebook', 'facebook', NULL, NULL, NULL, 'YES', NULL, NULL, NULL, 'socialmedia', 'facebook', 'https://www.facebook.com/');
INSERT INTO `cms` (`id`, `url_key`, `html_content`, `page_title`, `meta_data`, `meta_description`, `meta_keywords`, `visibility`, `deleted_at`, `created_at`, `updated_at`, `short_code`, `social_media_name`, `social_media_link`) VALUES (NULL, 'twitter', 'twitter', 'twitter', NULL, NULL, NULL, 'YES', NULL, NULL, NULL, 'socialmedia', 'twitter', 'https://www.twitter.com/');
INSERT INTO `cms` (`id`, `url_key`, `html_content`, `page_title`, `meta_data`, `meta_description`, `meta_keywords`, `visibility`, `deleted_at`, `created_at`, `updated_at`, `short_code`, `social_media_name`, `social_media_link`) VALUES (NULL, 'linkedin', 'linkedin', 'linkedin', NULL, NULL, NULL, 'YES', NULL, NULL, NULL, 'socialmedia', 'linkedin', 'https://www.linkedin.com/');
INSERT INTO `cms` (`id`, `url_key`, `html_content`, `page_title`, `meta_data`, `meta_description`, `meta_keywords`, `visibility`, `deleted_at`, `created_at`, `updated_at`, `short_code`, `social_media_name`, `social_media_link`) VALUES (NULL, 'instagram', 'instagram', 'instagram', NULL, NULL, NULL, 'YES', NULL, NULL, NULL, 'socialmedia', 'instagram', 'https://www.instagram.com/');


--- Home page ------
INSERT INTO `cms` (`id`, `url_key`, `html_content`, `page_title`, `meta_data`, `meta_description`, `meta_keywords`, `visibility`, `deleted_at`, `created_at`, `updated_at`, `short_code`, `social_media_name`, `social_media_link`, `position`) VALUES (NULL, 'home_page_content_title', '<h2>About CarryOn</h2>\r\n<p>We don’t make it ‘til you order it.</p>', 'home_page_content_title', NULL, NULL, NULL, 'YES', NULL, '2020-02-18 10:46:09', '2020-02-18 10:46:09', 'home_page_content_title', NULL, NULL, '0');
INSERT INTO `cms_slider` (`id`, `title`, `description`, `image_path`, `cover_image_path`, `visibility`, `position`, `deleted_at`, `created_at`, `updated_at`, `short_code`) VALUES (NULL, 'Home page middle image', 'Home page middle image', 'images/frontend/video-image.jpg', NULL, 'YES', '0', NULL, '2020-02-27 00:00:00', '2020-02-27 00:00:00', 'homepage_middle');

-- Login background image --
INSERT INTO `cms_slider` (`id`, `title`, `description`, `image_path`, `cover_image_path`, `visibility`, `position`, `deleted_at`, `created_at`, `updated_at`, `short_code`) 
VALUES (NULL, 'Login Background', 'Login BG Image', 'images/checkout-login-bg.jpg', NULL, 'YES', '0', NULL, '2020-02-27 00:00:00', '2020-02-27 00:00:00', 'login_bg');

-- SignUp BG --
INSERT INTO `cms_slider` (`id`, `title`, `description`, `image_path`, `cover_image_path`, `visibility`, `position`, `deleted_at`, `created_at`, `updated_at`, `short_code`) 
VALUES (NULL, 'Signup Background', 'Signup BG Image', 'images/checkout-login-bg.jpg', NULL, 'YES', '0', NULL, '2020-02-27 00:00:00', '2020-02-27 00:00:00', 'sign_up_bg');
