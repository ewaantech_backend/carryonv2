<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PreferencesOptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preferenceoptions', function (Blueprint $table) { 
            $table->bigIncrements('id');
            $table->string('name', 250);
            $table->timestamps();
            $table->enum('type', ['Intolerence', 'Diet'])->default('Intolerence');
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
