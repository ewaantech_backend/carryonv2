<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserOauth2OriginChange extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::table('users', function ($table) {
//            $table->enum('oauth2_origin', ['FACEBOOK', 'GOOGLE', 'APPLE'])->nullable();
//        }); 
        DB::statement("ALTER TABLE users MODIFY COLUMN oauth2_origin ENUM('FACEBOOK', 'GOOGLE', 'APPLE')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
