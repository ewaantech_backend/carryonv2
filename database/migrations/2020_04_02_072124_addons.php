<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Addons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->string('sku', 200);
            $table->unsignedDecimal('actual_price', 10, 2);
            $table->unsignedDecimal('discount_price', 10, 2)->nullable();
            $table->string('url_key', 100)->nullable();
            $table->text('description')->nullable();
            $table->integer('position')->length(20)->unsigned()->default(0);
            $table->enum('visibility', array('YES', 'NO'))->default('YES');
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
