<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PaymentsOrderField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('payments', function ($table) {
            $table->dropForeign(['order_id']);
        });

        Schema::table('payments', function ($table) {
            $table->dropColumn('order_id');
        });

        Schema::table('payments', function ($table) {
            $table->string('order_id', 512)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
