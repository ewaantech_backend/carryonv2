<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EngineChangeInnodb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tables = [
            'addons',
            'addons_cart_items',
            'addons_order_items',
            'addons_product_mapping',
            'addon_images',
            'allergies',
            'allergies_filter',
            'boarding_pickup_map',
            'boarding_point',
            'cart',
            'cartkey',
            'cart_items',
            'category',
            'cms',
            'cms_slider',
            'failed_jobs',
            'flights',
            'incredience',
            'migrations',
            'model_has_permissions',
            'model_has_roles',
            'notifications',
            'order',
            'orderlog',
            'order_items',
            'password_resets',
            'payments',
            'permissions',
            'pickup_point',
            'preferenceoptions',
            'products',
            'product_allergies',
            'product_allergies_filter',
            'product_category',
            'product_images',
            'product_incredience',
            'product_preferenceoptions',
            'ready_mail',
            'roles',
            'role_has_permissions',
            'sessions',
            'settings',
            'short_links',
            'sms_template',
            'userkey',
            'users',
            'users_preferenceoptions',
        ];
        foreach ($tables as $table) {
            DB::statement('ALTER TABLE `' . $table . '` ENGINE = InnoDB');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
