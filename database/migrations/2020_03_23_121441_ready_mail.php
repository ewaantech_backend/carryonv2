<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ReadyMail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ready_mail', function (Blueprint $table) { 
            $table->bigIncrements('id');
            $table->text('email');
            $table->text('subject');
            $table->text('cust_name');
            $table->text('nearest_gate');
            $table->text('short_link_text');
            $table->text('order_number');
            $table->text('pickup_point');
            $table->text('item_content');
            $table->text('maplink');
            $table->enum('status', ['SENT', 'NOT_SENT'])->default('NOT_SENT');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
