<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('phone_number', 20);
            $table->string('nationality', 100);
            $table->string('pickup_point', 20)->nullable();
            $table->enum('type', ['ADMIN', 'CUSTOMER']);
        });
        Schema::table('allergies', function (Blueprint $table) {
            $table->integer('position')->length(20)->unsigned()->default(0);
        });
        Schema::table('category', function (Blueprint $table) {
            $table->integer('position')->length(20)->unsigned()->default(0);
        });
        Schema::table('incredience', function (Blueprint $table) {
            $table->integer('position')->length(20)->unsigned()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
