<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductDoneness extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_doneness', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->bigInteger('products_id')->unsigned();
            $table->bigInteger('doneness_id')->unsigned();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();

            $table->index(['products_id']);
            $table->index(['doneness_id']);

            
            $table->foreign('products_id')->references('id')->on('products')->onDelete('cascade');
            $table->foreign('doneness_id')->references('id')->on('doneness')->onDelete('cascade');
        });

       
      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_doneness');
    }
}
