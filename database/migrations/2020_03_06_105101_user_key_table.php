<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserKeyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('userkey', function (Blueprint $table) { 
            $table->bigIncrements('id');
            $table->bigInteger('users_id')->unsigned();
            $table->text('key');
            $table->timestamps();
            $table->index(['users_id']);
        });
        Schema::table('userkey', function (Blueprint $table) {
            $table->foreign('users_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
