<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductPreferenceoptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('product_preferenceoptions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('products_id')->unsigned()->nullable();
            $table->bigInteger('preferenceoptions_id')->unsigned()->nullable();
            $table->enum('type', ['Intolerence', 'Diet']);
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();

            $table->index(['products_id', 'preferenceoptions_id']);
        });

        Schema::table('product_preferenceoptions', function (Blueprint $table) {
            $table->foreign('products_id')->references('id')->on('products')->onDelete('set null');
            $table->foreign('preferenceoptions_id')->references('id')->on('preferenceoptions')->onDelete('set null');
        });       //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
