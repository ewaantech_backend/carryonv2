<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeEnumOrderStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order', function ($table) {
            $table->dropColumn('status');
        });

        Schema::table('order', function ($table) {
            $table->enum('status', array('ORDER_CONFIRMED', 'IN_KITCHEN', 'READY_FOR_PICKUP', 'DELIVERED', 'CANCELLED'))->default('ORDER_CONFIRMED');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
