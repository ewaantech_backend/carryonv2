<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UsersPreferenceoptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('users_preferenceoptions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('users_id')->unsigned()->nullable();
            $table->bigInteger('preferenceoptions_id')->unsigned()->nullable();
            $table->enum('type', ['Intolerence', 'Diet']);
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();

            $table->index(['users_id', 'preferenceoptions_id']);
        });

        Schema::table('users_preferenceoptions', function (Blueprint $table) {
            $table->foreign('users_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('preferenceoptions_id')->references('id')->on('preferenceoptions')->onDelete('set null');
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
