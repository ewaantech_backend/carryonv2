<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Userbilling extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table) {
            $table->string('billing_name', 200)->default('');
            $table->string('billing_address', 600)->default(''); 
            $table->string('billing_city', 100)->default('');
            $table->string('billing_zip', 25)->default('');
            $table->string('billing_country', 100)->default('');
        }); 

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
