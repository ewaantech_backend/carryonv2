<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OrderLogOrderid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orderlog', function($table) { 
            $table->bigInteger('order_id')->unsigned()->nullable();
        }); 
        Schema::table('orderlog', function (Blueprint $table) {
            $table->foreign('order_id')->references('id')->on('order')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
