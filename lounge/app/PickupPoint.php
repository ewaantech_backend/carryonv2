<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use App\User;

class PickupPoint extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'boarding_pickup_map';

}