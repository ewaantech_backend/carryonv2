<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
        '/dxblounge/admin/user/data',
        '/dxblounge/admin/user/add',
        '/dxblounge/admin/user/edit',
        '/dxblounge/admin/role/data',
        '/dxblounge/admin/role/add',
        '/dxblounge/admin/role/edit',
        '/dxblounge/admin/product/add',
        '/dxblounge/admin/product/edit',
        '/dxblounge/admin/product/image/delete',
        '/dxblounge/admin/product/delete',
        '/dxblounge/admin/orders/show',
        '/dxblounge/admin/order/edit',
        '/dxblounge/admin/ingredients/add',
        '/dxblounge/admin/ingredients/update',
        '/dxblounge/admin/allergies/add',
        '/dxblounge/admin/allergies/update',
        '/dxblounge/admin/cms/add',
        '/dxblounge/admin/cms/edit',
        '/dxblounge/admin/cms/data',
        '/dxblounge/admin/cms/delete',
        '/dxblounge/admin/category/add',
        '/dxblounge/admin/category/edit',
        '/dxblounge/admin/category/data',
        '/dxblounge/admin/category/delete',
        '/dxblounge/flight/timecheck',
        '/dxblounge/product/menu_dishes',
        '/dxblounge/customer/login',
        '/dxblounge/customer/login',
        '/dxblounge/product/menu_dishes',
        '/dxblounge/cart/add',
        '/dxblounge/cart/remove',
        '/dxblounge/cart/show',
        '/dxblounge/cart/addon/list',
        '/dxblounge/order/add',
        '/dxblounge/home/addbilling',
        '/dxblounge/order/update_cart_user',
        '/dxblounge/order/history',
        '/dxblounge/home/profile',
        '/dxblounge/home/profileupdate',
        '/dxblounge/order/pdfreport',
        '/dxblounge/admin/product/data',
        '/dxblounge/order/cancel',
        '/dxblounge/admin/order/cancel',
        '/dxblounge/admin/orders/pdfreport',
        '/dxblounge/admin/orders/ordercount',
        '/dxblounge/admin/cms/addsmstemplate',
        '/dxblounge/admin/order/pl_change',
        '/dxblounge/admin/order/reimburse',
        '/dxblounge/api/login',
        '/dxblounge/admin/kitchen/data',
        '/dxblounge/home/orderreviewdata',
        '/dxblounge/admin/orders/addon_details',
        '/dxblounge/home/create_guest'
    ];
}

