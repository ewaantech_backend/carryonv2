<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mail\TestEmail;
use App\Notifications;
use App\User;
use App\Userkey;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use AppleSignIn\ASDecoder;

class HomeController extends Controller
{
    /**
     * Method - login 
     * Request - email
     * Request - password
     * Response - usertoken
     * Request content type - JSON
     * Response content type - JSON
     * */
    public function login(Request $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');

        // start - checking the client has passed all the required parameters
        $validator = Validator::make($request->all(), [
            'email' => 'required|email:rfc,dns',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'failed', 'message' => $validator->errors()], 409);
        }
        // end - checking the client has passed all tDB::select("SELECT id FROM `roles` WHERE name = :name ", [":name" => "customer"]);he required parameters

        // start - check for correct email and password
        $user = DB::table('users')
            ->select('*')
            ->where("email", "=", $email)
            ->where("remember_token", "=", $password)
            ->first();
        if ( $user ) {
            $customerRole = $this->getCustomerRole();
            unset( $user->password );
            unset( $user->remember_token );
            // start - check the authenticated user is not a customer role
            if (empty($customerRole[0])) {
                return response()->json(['status' => 'failed', 'message' => 'Access Forbidden'], 403);
            }
            // $user = Auth::user();
            if ($user->role_id != $customerRole[0]->id) {
                return response()->json(['status' => 'failed', 'message' => 'Access Forbidden'], 403);
            }
            // end - check the authenticated user is not a customer role

            if (strpos($user->image, '/') === FALSE) { // fb & google images should have slashes.
                $url = Storage::url("profilepics/" . $user->image);
                $user->image = URL::to("/") . $url;
            }

            if ($request->post('fcm_token')) {
                $fcm_token = $request->post('fcm_token');
                $response = DB::table('users')->where('fcm_token', '=', $fcm_token)->update([
                    'fcm_token' => '',
                    'updated_at' => date("Y-m-d H:i:s")
                ]);
                $response = DB::table('users')->where('id', '=', $user->id)->update([
                    'fcm_token' => $request->post('fcm_token')
                ]);
            }

            $userKey = $this->generateUserToken($user, md5($this->generateCode(10)));
            $pref = $this->getuserPreferenceOptions($user->id);

            return response()->json(
                [
                    'status' => 'success',
                    'user_key' => $userKey->key,
                    'user_details' => $user,
                    'preference' => $pref
                ],
                200
            );
        }

        return response()->json(['status' => 'failed', 'message' => 'Invalid email or password'], 401);
        // end - check for correct email and password
    }

    private function getuserPreferenceOptions( $users_id ) {
        $data = DB::table('users_preferenceoptions')->where('users_id', $users_id)->get();
        $pref = [
            'diets' => [],
            'allergies' => []
        ];
        foreach($data as $values) {
            if($values->type == 'Diet') {
                $pref['diets'][] = $values->id;
            }
            if($values->type == 'Intolerence') {
                $pref['allergies'][] = $values->id;
            }
        }

        return $pref;
    }

    /**
     * Method - Registering user
     * Request - firstname
     * Request - lastname
     * Request - email address
     * Request - phone
     * Request - country
     * Response - email with password
     * Request content type - JSON
     * Response content type - JSON
     */
    public function signUp(Request $request)
    {
        // start - checking the client has passed all the required parameters
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email:rfc,dns|unique:users,email',
            // 'phone' => 'required',
            // 'country' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'failed', 'message' => $validator->errors()], 409);
        }
        // end - checking the client has passed all the required parameters

        $randomPassword = rand(1000, 9999);
        $firstName = $request->input('first_name');
        $lastName = $request->input('last_name');
        $emailAddress = $request->input('email');
        $phoneNumber = null;
        if ($request->filled('phone_number')) {
            $phoneNumber = $request->input('phone_number');
        }
        $country = null;
        if ($request->filled('country')) {
            $country = $request->input('country');
        }
        $timestamp = date('Y-m-d H:i:s');
        $image = $request->input('image');

        try {
            $user = $this->saveUser(array(
                'firstName' => $firstName,
                'lastName' => $lastName,
                'emailAddress' => $emailAddress,
                'phoneNumber' => $phoneNumber,
                'country' => $country,
                'timestamp' => $timestamp,
                'image' => $image,
                'signupType' => 'NORMAL',
                'password' => $randomPassword,
                'fcm_token' => $request->post('fcm_token')
            ));

            $user = User::find($user->id);
            if( $image ) {
                $imageName = $user->id . '.' . 'png';
                Storage::disk('public')->put("profilepics/" . $imageName, base64_decode($image));
                $user->image = $imageName;
            }
            $user->oauth2_origin = null;
            $user->save();

            if ( $image) { // to display in client side.
                $url = Storage::url("profilepics/" . $user->image);
                $user->image = URL::to("/") . $url;
            }

            $mail_message = '<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Hi <b>' . $user->name . '</b>,</p>

                <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Welcome to <b>CarryOn</b>, we’re happy to have you around!</p>
                <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Your system generated password is <b>' . rand(1000, 9999) . '</b></p>';

            $this->sendMail($mail_message, $user->email, $user->name, $randomPassword);

            return response()->json(['status' => 'success','user_details' => $user, 'user_token' => $this->generateUserToken($user, md5($this->generateCode(10)))->key], 201);
        } catch (Exception $e) {
            return response()->json(['status' => 'failed', 'message' => 'An error occured. Please try again'], 500);
        }
    }
    
    

    public function forgotpassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email:rfc,dns',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'failed', 'message' => 'Please enter a valid email'], 409);
        }
        
        $status = array();
        $email = $request->input('email');
        $rand = rand(1000, 9999);

        $usersarr = DB::select('select * from users 
                                        where  email = "' . $email . '" AND  type = "CUSTOMER"');
        //echo '<pre>'; print_r($usersarr); exit;
        $password = Hash::make($rand);

        if (count($usersarr) > 0) {
            DB::table('users')
                ->where('id', $usersarr[0]->id)
                ->update([
                    'password' => $password,
                    'remember_token' => $rand,
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);

            $mail_message = '<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Hi <b>' . $usersarr[0]->name . '</b>,</p>

                            <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Welcome to <b>CarryOn</b>, we’re happy to have you around!</p>
                            <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Your system generated password is <b>' . $rand . '</b></p>';
            $this->sendMail($mail_message, $usersarr[0]->email, $usersarr[0]->name, $rand);
            return response()->json(['status' => 'success','message' => "Your password is updated successfully, and email with password will be sent to your email address."], 201);
        } else {
            return response()->json(['status' => 'failed','message' => "Email address not found in our records."], 401);
        }


        echo json_encode($status);
    }
    

    /**
     * Method - logout the user from the application
     * Request - userkey
     * Request content type - JSON
     * Response content type - JSON
     * Response - flushing out the existing user key from userkeys table
     */
    public function logout(Request $request)
    {
        // start - checking the client has passed all the required parameters
        $validator = Validator::make($request->all(), [
            'user_token' => 'required|exists:userkey,key',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'failed', 'message' => $validator->errors()], 401);
        }
        // end - checking the client has passed all the required parameters

        $userToken = $request->input('user_token');
        try {
            DB::delete("DELETE FROM `userkey`  WHERE `key` = :user_key", [":user_key" => $userToken]);
            return response()->json(['status' => 'success', 'message' => 'Logged out successfully'], 200);
        } catch (Exception $e) {
            return response()->json(['status' => 'failed', 'message' => 'An error occured. Please try again'], 500);
        }
    }

    /**
     * Method - Fetch all the notification for the logged in user
     * Request - usertoken
     * Request content type - JSON
     * Response content type - JSON 
     * Response - notifications json object
     */
    public function getNotifications(Request $request)
    {
        // start - checking the client has passed all the required parameters
        $validator = Validator::make($request->all(), [
            'user_token' => 'required|exists:userkey,key',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'failed', 'message' => $validator->errors()], 401);
        }
        // end - checking the client has passed all the required parameters

        $userToken = $request->input('user_token');
        try {
            $notificationsObject = $this->getNotificationData($userToken);
            return response()->json(['status' => 'success', 'notifications' => $notificationsObject], 200);
        } catch (Exception $e) {
            return response()->json(['status' => 'failed', 'message' => 'An error occured. Please try again'], 500);
        }
    }

    /**
     * Method - To make the notification status as READ
     * Request - usertoken
     * Response - success/failure message
     */
    public function changeNotificationStatus(Request $request)
    {
        // start - checking the client has passed all the required parameters
        $validator = Validator::make($request->all(), [
            'user_token' => 'required|exists:userkey,key',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'failed', 'message' => $validator->errors()], 401);
        }
        // end - checking the client has passed all the required parameters
        $userToken = $request->input('user_token');
        try {

            $userIDObj = Userkey::firstWhere('key', $userToken);
            $userID = $userIDObj->users_id;

            $notificationUpdateQuery = "UPDATE `notifications` AS noti SET noti.status = 'INACTIVE' WHERE noti.users_id = :user_id ";
            DB::update($notificationUpdateQuery, [':user_id' => $userID]);

            return response()->json(['status' => 'success', 'message' => 'Status changed to READ'], 200);
        } catch (Exception $e) {
            return response()->json(['status' => 'failed', 'message' => 'An error occured. Please try again'], 500);
        }
    }

    /**
     * Method - Get the profile details of user 
     * Request - usertoken
     * Request content type - JSON
     * Response content type - JSON 
     * Response - profile json object
     */
    public function getProfile(Request $request)
    {
        // start - checking the client has passed all the required parameters
        $validator = Validator::make($request->all(), [
            'user_token' => 'required|exists:userkey,key',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'failed', 'message' => $validator->errors()], 401);
        }
        // end - checking the client has passed all the required parameters

        $userToken = $request->input('user_token');
        try {
            $userIDObj = Userkey::firstWhere('key', $userToken);
            $userID = $userIDObj->users_id;

            $userDetails = User::find($userID);
            if (strpos($userDetails->image, '/') === FALSE) { // fb & google images should have slashes.
                $url = Storage::url("profilepics/" . $userDetails->image);
                $userDetails->image = URL::to("/") . $url;
            }

            return response()->json(['status' => 'success', 'user_details' => $userDetails, 'notifications_count' => count($this->getNotificationDataCount($userToken))], 200);
        } catch (Exception $e) {
            return response()->json(['status' => 'failed', 'message' => 'An error occured. Please try again', 'm'=> $e], 500);
        }
    }

    /**
     * Method - Oauth2 login for Facebook and Google
     * Request - oauth2 response object from Google and Facebook
     * Response - Access token 
     * Request content type - JSON
     * Response content type - JSON 
     */
    public function oauth2Login(Request $request)
    {
        // file_put_contents("debug.txt", $request->all(), FILE_APPEND);
        // start - checking the client has passed all the required parameters
        $validator = Validator::make($request->all(), [
            'type' => 'required|alpha|in:google,facebook,apple',
            'accessToken' => 'required',
            'id_token' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'failed', 'message' => $validator->errors()], 401);
        }
        // end - checking the client has passed all the required parameters

        $oauthGeneratorType = $request->input('type');
        $accessToken = $request->input('accessToken');
        $firstName = $request->input('displayName');
        $lastName = '';
        $email = !empty($request->input('email')) ? $request->input('email') : ' ';
        $image = $request->input('photoUrl');
        $timestamp = date('Y-m-d H:i:s');
        $phoneNumber = $request->input('phoneNumber');
        $country = '';
        $idToken = $request->input('id_token');

        // start - validating access token with google authorization server
        if ($oauthGeneratorType == 'google') {
            try {
                define("CLIENT_ID", "895641956257-fpgn4tnuj7uf1tsmi2m2ha73kadm0ou8.apps.googleusercontent.com");
                $client = new \Google_Client(['client_id' => CLIENT_ID]);
                $payload = $client->verifyIdToken(trim($idToken));
                if ($payload) {
                    $validator = Validator::make($request->all(), [
                        'email' => 'required|email:rfc,dns|unique:users,email',
                    ]);
                    if ($validator->fails()) {
                        $userQuery = "SELECT id FROM `users` WHERE email = :email";
                        $userID = DB::select($userQuery, [":email" => $email]);
                        $user = User::find($userID[0]->id);
                        $user->oauth2_origin = 'GOOGLE';
                        $user->fcm_token = $request->input('fcm_token');
                        if($user->image == NULL || $user->image == '') {
                            $user->image = $image;
                        }
                        $user->save();
                        if( $user->image != NULL && $user->image != '') {
                            if (strpos($user->image, '/') === FALSE) { // fb & google images should have slashes.
                                $url = Storage::url("profilepics/" . $user->image);
                                $user->image = URL::to("/") . $url;
                            }
                        }
                        $userKey = $this->generateUserToken($user, md5($this->generateCode(10)));
                        $pref = $this->getuserPreferenceOptions($user->id);
                        return response()->json([
                            'status' => 'success', 
                            'signup' => FALSE,
                            'preference' => $pref, 'user_key' => $userKey->key, 'user_details' => $user], 200);
                    } else {
                        $user = $this->saveUser(array(
                            'firstName' => $firstName,
                            'lastName' => $lastName,
                            'emailAddress' => $email,
                            'phoneNumber' => $phoneNumber,
                            'country' => $country,
                            'timestamp' => $timestamp,
                            'image' => $image,
                            'signupType' => 'OAUTH2',
                            'password' => $this->generateCode(10),
                            'fcm_token' => $request->input('fcm_token')
                        ));
                        $user->image = $image;
                        $user->oauth2_origin = 'GOOGLE';
                        $user->save();
                        $userKey = $this->generateUserToken($user, md5($this->generateCode(10)));
                        return response()->json([
                            'status' => 'success',
                            'signup' => TRUE,
                            'preference' => [
                                'diets' => [],
                                'allergies' => []
                            ], 
                            'user_key' => $userKey->key, 'user_token' => $userKey->key, 'user_details' => $user], 
                        201);
                    }
                } else {
                    return response()->json(['status' => 'failed', 'message' => 'Invalid Access Token'], 401);
                }
            } catch (Exception $e) {
                print $e->getMessage();
                return response()->json(['status' => 'failed', 'message' => 'An error occured. Please try again'], 500);
            }
        }
        // end - validating access token with google authorization server

        // start - validating access token with facebook authorization server
        if ($oauthGeneratorType == 'facebook') {
            try {
                define("CLIENT_ID", "1125398287841034");
                define("CLIENT_SECRET", "6b3e3506895a834a6f7557d3e77c6c39");
                $fb = new \Facebook\Facebook([
                    'app_id' => CLIENT_ID,
                    'app_secret' => CLIENT_SECRET,
                    'default_graph_version' => 'v7.0',
                ]);

                $appAccessToken = CLIENT_ID . '|' . CLIENT_SECRET;

                $response = $fb->get(
                    '/debug_token?input_token=' . $accessToken,
                    $appAccessToken
                );

                if ($response->getGraphNode()['is_valid'] === false) {
                    return response()->json(['status' => 'failed', 'message' => 'Invalid Access Token'], 401);
                }

                if (!empty(trim($email))) {
                    $validator = Validator::make($request->all(), [
                        'email' => 'unique:users,email',
                    ]);
                    $userQuery = "SELECT id FROM `users` WHERE email = :email";
                    $dbParams = [":email" => $email];
                } else if (!empty(trim($phoneNumber))) {
                    $validator = Validator::make($request->all(), [
                        'phoneNumber' => 'unique:users,phone_number',
                    ]);
                    $userQuery = "SELECT id FROM `users` WHERE phone_number = :phone_number";
                    $dbParams = [":phone_number" => $phoneNumber];
                } else {
                    return response()->json(['status' => 'failed', 'message' => 'Email and Phone Number is empty. Please try again'], 409);
                }

                if ($validator->fails()) {
                    $userID = DB::select($userQuery, $dbParams);
                    $user = User::find($userID[0]->id);
                    $user->oauth2_origin = 'FACEBOOK';
                    $user->fcm_token = $request->input('fcm_token');
                    if($user->image == NULL || $user->image == '') {
                        $user->image = $image;
                    }
                    $user->save();
                    if( $user->image != NULL && $user->image != '') {
                        if (strpos($user->image, '/') === FALSE) { // fb & google images should have slashes.
                            $url = Storage::url("profilepics/" . $user->image);
                            $user->image = URL::to("/") . $url;
                        }
                    }
                    $userKey = $this->generateUserToken($user, md5($this->generateCode(10)));
                    $pref = $this->getuserPreferenceOptions($user->id);
                    return response()->json([
                        'status' => 'success',
                        'signup' => FALSE,
                        'preference' => $pref, 'user_key' => $userKey->key, 'user_details' => $user], 200);
                } else {
                    $user = $this->saveUser(array(
                        'firstName' => $firstName,
                        'lastName' => $lastName,
                        'emailAddress' => $email,
                        'phoneNumber' => $phoneNumber,
                        'country' => $country,
                        'timestamp' => $timestamp,
                        'image' => $image,
                        'signupType' => 'OAUTH2',
                        'password' => $this->generateCode(10),
                        'fcm_token' => $request->input('fcm_token')
                    ));
                    $user->image = $image;
                    $user->oauth2_origin = 'FACEBOOK';
                    $user->save();
                    $userKey = $this->generateUserToken($user, md5($this->generateCode(10)));
                    return response()->json([
                        'status' => 'success',
                        'signup' => TRUE,
                        'preference' => [
                            'diets' => [],
                            'allergies' => []
                        ], 
                        'user_key' => $userKey->key, 'user_token' => $userKey->key, 'user_details' => $user], 201);
                }
            } catch (\Facebook\Exceptions\FacebookResponseException $e) {
                return response()->json(['status' => 'failed', 'message' => 'Invalid Access Token'], 401);
            } catch (\Facebook\Exceptions\FacebookSDKException $e) {
                print $e->getMessage();
                return response()->json(['status' => 'failed', 'message' => 'An error occured. Please try again'], 500);
            } catch (Exception $e) {
                print $e->getMessage();
                return response()->json(['status' => 'failed', 'message' => 'An error occured. Please try again'], 500);
            }
        }
        
        // start - validating access token with facebook authorization server
        if ($oauthGeneratorType == 'apple') {
            
            $clientUser = $request->input('clientUser');
            $identityToken = $accessToken;
            try {
                $appleSignInPayload = ASDecoder::getAppleSignInPayload($identityToken);
                if ($appleSignInPayload) {
                    $validator = Validator::make($request->all(), [
                        'email' => 'required|email:rfc,dns|unique:users,email',
                    ]);
                    if ($validator->fails()) {
                        if (!empty(trim($email))) {
                            $userQuery = "SELECT id FROM `users` WHERE email = :email";
                            $userID = DB::select($userQuery, [":email" => $email]);
                            $user = User::find($userID[0]->id);
                            $user->oauth2_origin = 'APPLE';
                            $user->fcm_token = $request->input('fcm_token');
                            if($user->image == NULL || $user->image == '') {
                                $user->image = $image;
                            }
                            $user->save();
                            if( $user->image != NULL && $user->image != '') {
                                if (strpos($user->image, '/') === FALSE) { // fb & google images should have slashes.
                                    $url = Storage::url("profilepics/" . $user->image);
                                    $user->image = URL::to("/") . $url;
                                }
                            }
                            $userKey = $this->generateUserToken($user, md5($this->generateCode(10)));
                            $pref = $this->getuserPreferenceOptions($user->id);
                            return response()->json([
                                'status' => 'success',
                                'signup' => FALSE,
                                'preference' => $pref, 'user_key' => $userKey->key, 'user_details' => $user], 200);
                        } else {
                            return response()->json(['status' => 'failed', 'message' => 'Invalid Email or Email not supplied'], 401);
                        }
                    } else {
                        $user = $this->saveUser(array(
                            'firstName' => $firstName,
                            'lastName' => $lastName,
                            'emailAddress' => $email,
                            'phoneNumber' => $phoneNumber,
                            'country' => $country,
                            'timestamp' => $timestamp,
                            'image' => $image,
                            'signupType' => 'OAUTH2',
                            'password' => $this->generateCode(10),
                            'fcm_token' => $request->input('fcm_token')
                        ));
                        $user->image = $image;
                        $user->oauth2_origin = 'APPLE';
                        $user->save();
                        $userKey = $this->generateUserToken($user, md5($this->generateCode(10)));
                        return response()->json([
                            'status' => 'success', 
                            'signup' => TRUE,
                            'preference' => [
                                'diets' => [],
                                'allergies' => []
                            ], 
                            'user_key' => $userKey->key, 'user_token' => $userKey->key, 'user_details' => $user], 201);
                    }
                } else {
                    return response()->json(['status' => 'failed', 'message' => 'Invalid Access Token'], 401);
                }

            } catch (Exception $e) {
                return response()->json(['status' => 'failed', 'message' => 'An error occured. Please try again'], 500);
            }
            
//            echo "<pre>";
//                print_r($appleSignInPayload);

            /**
             * Obtain the Sign In with Apple email and user creds.
             */
//            echo $email = $appleSignInPayload->getEmail();
//            echo "/n";
//            echo $user = $appleSignInPayload->getUser();
            /**
             * Determine whether the client-provided user is valid.
             */
            // $isValid = $appleSignInPayload->verifyUser($clientUser);
//            $isValid = $appleSignInPayload->verifyUser($clientUser);
//            if ($isValid) {
//                echo "verified";
//            } else {
//                echo "not verified";
//            }
//            exit;
            
        }
        // end - validating access token with facebook authorization server

    }
    
    /**
     * Method - Oauth2 login for Facebook and Google
     * Request - oauth2 response object from Google and Facebook
     * Response - Access token 
     * Request content type - JSON
     * Response content type - JSON 
     */
    public function oauth2LoginNew(Request $request)
    {
        // file_put_contents("debug.txt", $request->all(), FILE_APPEND);
        // start - checking the client has passed all the required parameters
        $validator = Validator::make($request->all(), [
            'type' => 'required|alpha|in:google,facebook,apple',
            'accessToken' => 'required',
            'id_token' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'failed', 'message' => $validator->errors()], 401);
        }
        // end - checking the client has passed all the required parameters

        $oauthGeneratorType = $request->input('type');
        $accessToken = $request->input('accessToken');
        $firstName = $request->input('displayName');
        $lastName = '';
        $email = !empty($request->input('email')) ? $request->input('email') : ' ';
        $image = $request->input('photoUrl');
        $timestamp = date('Y-m-d H:i:s');
        $phoneNumber = $request->input('phoneNumber');
        $country = '';
        $idToken = $request->input('id_token');

        // start - validating access token with google authorization server
        if ($oauthGeneratorType == 'google') {
            try {
                define("CLIENT_ID", "895641956257-fpgn4tnuj7uf1tsmi2m2ha73kadm0ou8.apps.googleusercontent.com");
                $client = new \Google_Client(['client_id' => CLIENT_ID]);
                $payload = $client->verifyIdToken(trim($idToken));
                if ($payload) {
                    $validator = Validator::make($request->all(), [
                        'email' => 'required|email:rfc,dns|unique:users,email',
                    ]);
                    if ($validator->fails()) {
                        $userQuery = "SELECT id FROM `users` WHERE email = :email";
                        $userID = DB::select($userQuery, [":email" => $email]);
                        $user = User::find($userID[0]->id);
                        $user->oauth2_origin = 'GOOGLE';
                        $user->fcm_token = $request->input('fcm_token');
                        if($user->image == NULL || $user->image == '') {
                            $user->image = $image;
                        }
                        $user->save();
                        if( $user->image != NULL && $user->image != '') {
                            if (strpos($user->image, '/') === FALSE) { // fb & google images should have slashes.
                                $url = Storage::url("profilepics/" . $user->image);
                                $user->image = URL::to("/") . $url;
                            }
                        }
                        $userKey = $this->generateUserToken($user, md5($this->generateCode(10)));
                        return response()->json(['status' => 'success', 'user_key' => $userKey->key, 'user_details' => $user], 200);
                    } else {
                        $user = $this->saveUser(array(
                            'firstName' => $firstName,
                            'lastName' => $lastName,
                            'emailAddress' => $email,
                            'phoneNumber' => $phoneNumber,
                            'country' => $country,
                            'timestamp' => $timestamp,
                            'image' => $image,
                            'signupType' => 'OAUTH2',
                            'password' => $this->generateCode(10),
                            'fcm_token' => $request->input('fcm_token')
                        ));
                        $user->image = $image;
                        $user->oauth2_origin = 'GOOGLE';
                        $user->save();
                        $userKey = $this->generateUserToken($user, md5($this->generateCode(10)));
                        return response()->json(['status' => 'success', 'user_key' => $userKey->key, 'user_token' => $userKey->key, 'user_details' => $user], 201);
//                        return response()->json(['status' => 'success', 'user_token' => $this->generateUserToken($user, md5($this->generateCode(10)))->key, 'user_details' => $user], 201);
                    }
                } else {
                    return response()->json(['status' => 'failed', 'message' => 'Invalid Access Token'], 401);
                }
            } catch (Exception $e) {
                print $e->getMessage();
                return response()->json(['status' => 'failed', 'message' => 'An error occured. Please try again'], 500);
            }
        }
        // end - validating access token with google authorization server

        // start - validating access token with facebook authorization server
        if ($oauthGeneratorType == 'facebook') {
            try {
                define("CLIENT_ID", "1125398287841034");
                define("CLIENT_SECRET", "6b3e3506895a834a6f7557d3e77c6c39");
                $fb = new \Facebook\Facebook([
                    'app_id' => CLIENT_ID,
                    'app_secret' => CLIENT_SECRET,
                    'default_graph_version' => 'v7.0',
                ]);

                $appAccessToken = CLIENT_ID . '|' . CLIENT_SECRET;

                $response = $fb->get(
                    '/debug_token?input_token=' . $accessToken,
                    $appAccessToken
                );

                if ($response->getGraphNode()['is_valid'] === false) {
                    return response()->json(['status' => 'failed', 'message' => 'Invalid Access Token'], 401);
                }

                if (!empty(trim($email))) {
                    $validator = Validator::make($request->all(), [
                        'email' => 'unique:users,email',
                    ]);
                    $userQuery = "SELECT id FROM `users` WHERE email = :email";
                    $dbParams = [":email" => $email];
                } else if (!empty(trim($phoneNumber))) {
                    $validator = Validator::make($request->all(), [
                        'phoneNumber' => 'unique:users,phone_number',
                    ]);
                    $userQuery = "SELECT id FROM `users` WHERE phone_number = :phone_number";
                    $dbParams = [":phone_number" => $phoneNumber];
                } else {
                    return response()->json(['status' => 'failed', 'message' => 'Email and Phone Number is empty. Please try again'], 409);
                }

                if ($validator->fails()) {
                    $userID = DB::select($userQuery, $dbParams);
                    $user = User::find($userID[0]->id);
                    $user->oauth2_origin = 'FACEBOOK';
                    $user->fcm_token = $request->input('fcm_token');
                    if($user->image == NULL || $user->image == '') {
                        $user->image = $image;
                    }
                    $user->save();
                    if( $user->image != NULL && $user->image != '') {
                        if (strpos($user->image, '/') === FALSE) { // fb & google images should have slashes.
                            $url = Storage::url("profilepics/" . $user->image);
                            $user->image = URL::to("/") . $url;
                        }
                    }
                    $userKey = $this->generateUserToken($user, md5($this->generateCode(10)));
                    return response()->json(['status' => 'success', 'user_key' => $userKey->key, 'user_details' => $user], 200);
                } else {
                    $user = $this->saveUser(array(
                        'firstName' => $firstName,
                        'lastName' => $lastName,
                        'emailAddress' => $email,
                        'phoneNumber' => $phoneNumber,
                        'country' => $country,
                        'timestamp' => $timestamp,
                        'image' => $image,
                        'signupType' => 'OAUTH2',
                        'password' => $this->generateCode(10),
                        'fcm_token' => $request->input('fcm_token')
                    ));
                    $user->image = $image;
                    $user->oauth2_origin = 'FACEBOOK';
                    $user->save();
                    $userKey = $this->generateUserToken($user, md5($this->generateCode(10)));
                    return response()->json(['status' => 'success', 'user_key' => $userKey->key, 'user_token' => $userKey->key, 'user_details' => $user], 201);
//                    return response()->json(['status' => 'success', 'user_token' => $this->generateUserToken($user, md5($this->generateCode(10)))->key, 'user_details' => $user], 201);
                }
            } catch (\Facebook\Exceptions\FacebookResponseException $e) {
                return response()->json(['status' => 'failed', 'message' => 'Invalid Access Token'], 401);
            } catch (\Facebook\Exceptions\FacebookSDKException $e) {
                print $e->getMessage();
                return response()->json(['status' => 'failed', 'message' => 'An error occured. Please try again'], 500);
            } catch (Exception $e) {
                print $e->getMessage();
                return response()->json(['status' => 'failed', 'message' => 'An error occured. Please try again'], 500);
            }
        }
        
        // start - validating access token with facebook authorization server
        if ($oauthGeneratorType == 'apple') {
            
            $clientUser = $request->input('clientUser');
            $identityToken = $accessToken;
            try {
                $appleSignInPayload = ASDecoder::getAppleSignInPayload($identityToken);
                if ($appleSignInPayload) {
                    $validator = Validator::make($request->all(), [
                        'email' => 'required|email:rfc,dns|unique:users,email',
                    ]);
                    if ($validator->fails()) {
                        if (!empty(trim($email))) {
                            $userQuery = "SELECT id FROM `users` WHERE email = :email";
                            $userID = DB::select($userQuery, [":email" => $email]);
                            $user = User::find($userID[0]->id);
                            $user->oauth2_origin = 'APPLE';
                            $user->fcm_token = $request->input('fcm_token');
                            if($user->image == NULL || $user->image == '') {
                                $user->image = $image;
                            }
                            $user->save();
                            if( $user->image != NULL && $user->image != '') {
                                if (strpos($user->image, '/') === FALSE) { // fb & google images should have slashes.
                                    $url = Storage::url("profilepics/" . $user->image);
                                    $user->image = URL::to("/") . $url;
                                }
                            }
                            $userKey = $this->generateUserToken($user, md5($this->generateCode(10)));
                            return response()->json(['status' => 'success', 'user_key' => $userKey->key, 'user_details' => $user], 200);
                        } else {
                            return response()->json(['status' => 'failed', 'message' => 'Invalid Email or Email not supplied'], 401);
                        }
                    } else {
                        $user = $this->saveUser(array(
                            'firstName' => $firstName,
                            'lastName' => $lastName,
                            'emailAddress' => $email,
                            'phoneNumber' => $phoneNumber,
                            'country' => $country,
                            'timestamp' => $timestamp,
                            'image' => $image,
                            'signupType' => 'OAUTH2',
                            'password' => $this->generateCode(10),
                            'fcm_token' => $request->input('fcm_token')
                        ));
                        $user->image = $image;
                        $user->oauth2_origin = 'APPLE';
                        $user->save();
                        $userKey = $this->generateUserToken($user, md5($this->generateCode(10)));
                        return response()->json(['status' => 'success', 'user_key' => $userKey->key, 'user_token' => $userKey->key, 'user_details' => $user], 201);
                    }
                } else {
                    return response()->json(['status' => 'failed', 'message' => 'Invalid Access Token'], 401);
                }

            } catch (Exception $e) {
                return response()->json(['status' => 'failed', 'message' => 'An error occured. Please try again'], 500);
            }
            
//            echo "<pre>";
//                print_r($appleSignInPayload);

            /**
             * Obtain the Sign In with Apple email and user creds.
             */
//            echo $email = $appleSignInPayload->getEmail();
//            echo "/n";
//            echo $user = $appleSignInPayload->getUser();
            /**
             * Determine whether the client-provided user is valid.
             */
            // $isValid = $appleSignInPayload->verifyUser($clientUser);
//            $isValid = $appleSignInPayload->verifyUser($clientUser);
//            if ($isValid) {
//                echo "verified";
//            } else {
//                echo "not verified";
//            }
//            exit;
            
        }
        // end - validating access token with facebook authorization server

    }

    /**
     * Method - Update billing details of user
     * Request - Billing info json object along with user token
     * Response - success or failure with user object
     * Request content type - JSON
     * Response content type - JSON 
     */
    public function updateBillingDetails(Request $request)
    {
        // start - checking the client has passed all the required parameters
        $validator = Validator::make($request->all(), [
            'user_token' => 'required|exists:userkey,key',
            'billing_name' => 'required',
            'city' => 'required',
            'po_box' => 'required',
            'country' => 'required',
            'street_name' => 'required',
            'street_number' => 'required',
            'apartment_number' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'failed', 'message' => $validator->errors()], 401);
        }
        // end - checking the client has passed all the required parameters

        $userToken = $request->input('user_token');
        $billingName = $request->input('billing_name');
        $city = $request->input('city');
        $poBox = $request->input('po_box');
        $country = $request->input('country');
        $streetName = $request->input('street_name');
        $streetNumber = $request->input('street_number');
        $apartmentNumber = $request->input('apartment_number');
        try {
            $userIDObj = Userkey::firstWhere('key', $userToken);
            $userID = $userIDObj->users_id;

            $user = User::find($userID);
            $user->billing_name = $billingName;
            $user->billing_city = $city;
            $user->billing_zip = $poBox;
            $user->billing_country = $country;
            $user->billing_street = $streetName;
            $user->billing_street_no = $streetNumber;
            $user->billing_apartment_no = $apartmentNumber;
            $user->save();

            return response()->json(['status' => 'success', 'user_details' => $user], 200);
        } catch (Exception $e) {
            return response()->json(['status' => 'failed', 'message' => 'An error occured. Please try again'], 500);
        }
    }

    /**
     * Method - Change password
     * Request - password
     * Response - user object
     */
    public function changePassword(Request $request)
    {
        // start - checking the client has passed all the required parameters
        $validator = Validator::make($request->all(), [
            'user_token' => 'required|exists:userkey,key',
            'password' => 'required',
            'current_password' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'failed', 'message' => $validator->errors()], 401);
        }
        // end - checking the client has passed all the required parameters
        $userToken = $request->input('user_token');
        $password = $request->input('password');
        $currentPassword = $request->input('current_password');

        try {
            $userIDObj = Userkey::firstWhere('key', $userToken);
            $userID = $userIDObj->users_id;

            $user = User::find($userID);
            if (Hash::check($currentPassword, $user->password) === false) {
                return response()->json(['status' => 'failed', 'message' => "Incorrect current password "], 401);
            }
            $user->password = Hash::make($password);
            $user->remember_token = $password;
            $user->save();
            return response()->json(['status' => 'success', 'user_details' => $user], 200);
        } catch (Exception $e) {
            return response()->json(['status' => 'failed', 'message' => 'An error occured. Please try again'], 500);
        }
    }

    private function getCustomerRole()
    {
        return DB::select("SELECT id FROM `roles` WHERE name = :name ", [":name" => "customer"]);
    }

    private function sendMail($message, $email, $user, $rand)
    {
        $subject_data = DB::select("select template from sms_template where `type` = :type", ['type' => 'EMAIL_SUBJECT_WELCOMEMAIL']);
        $subject = $subject_data[0]->template;
        $data = [
            'from_name' => 'CARRYON',
            'from_email' => 'noreply@carryon.com',
            // 'subject' => 'Welcome to CarryOn!',
            'subject' => $subject,
            'message1' => $message,
            'message2' => '',
            'user' => $user,
            'rand' => $rand
        ];

        Mail::to($email)->send(new TestEmail($data));
    }

    /**
     * Method - Update profile
     * Request - Profile object - {firstName, lastName, phone, country }
     * Response - User object
     */
    public function updateProfile(Request $request)
    {
        ini_set('post_max_size', '36M');
        ini_set('upload_max_filesize', '30M');
        ini_set('max_execution_time', '120');
        // start - checking the client has passed all the required parameters
        $validator = Validator::make($request->all(), [
            'user_token' => 'required|exists:userkey,key',
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => [
                'required',
                Rule::unique('users')->ignore(
                    Userkey::firstWhere('key', $request->input('user_token'))->users_id
                ),
            ],
//            'phone' => 'required',
//            'country' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'failed', 'message' => $validator->errors()], 401);
        }
        // end - checking the client has passed all the required parameters

        $userToken = $request->input('user_token');
        $firstName = $request->input('first_name');
        $lastName = $request->input('last_name');
        $email = $request->input('email');
        $phone = null;
        if ($request->filled('phone')) {
            $phone = $request->input('phone');
        }
        $country = null;
        if ($request->filled('country')) {
            $country = $request->input('country');
        }
        $image = $request->input('image');

        try {
            $userIDObj = Userkey::firstWhere('key', $userToken);
            $userID = $userIDObj->users_id;

            $user = User::find($userID);

            // start - upload  file if exists if the passed image is base 64 encoded
            if ($image && base64_decode($image, true)) {
                // start - delete existing image
                Storage::disk('public')->delete("profilepics/" . $user->image);
                // end - delete existing image

                // $image = str_replace('data:image/png;base64,', '', $image);
                // $image = str_replace(' ', '+', $image);

                $imageName = $user->id . '.' . 'png';
                Storage::disk('public')->put("profilepics/" . $imageName, base64_decode($image));
                $user->image = $imageName;

                // reduce
                $s_path = Storage::disk('public')->path( "profilepics/" . $user->image );
                $src= $s_path;
                $dest = $s_path;
                $desired_width="200";
                $this->make_thumb($src, $dest, $desired_width);
            }
            // end - upload  file if exists if the passed image is base 64 encoded

            // start - updating profile details

            $user->name = $firstName . ' ' . $lastName;
            $user->first_name = $firstName;
            $user->last_name = $lastName;
            $user->email = $email;
            $user->phone_number = $phone;
            $user->nationality = $country;
            $user->save();
            // end - updating profile details

            // below code to display image in client side.
            if (strpos($user->image, '/') === FALSE) { // fb & google images should have slashes.
                $url = Storage::url("profilepics/" . $user->image);
                $user->image = asset( $url );
            }

            return response()->json(['status' => 'success', 'user_details' => $user], 200);
        } catch (Exception $e) {
            return response()->json(['status' => 'failed', 'message' => 'An error occured. Please try again'], 500);
        }
    }

    private function make_thumb($src, $dest, $desired_width) {

        /* read the source image */
        $source_image = imagecreatefromjpeg($src);
        $width = imagesx($source_image);
        $height = imagesy($source_image);
    
        /* find the "desired height" of this thumbnail, relative to the desired width  */
        $desired_height = floor($height * ($desired_width / $width));
    
        /* create a new, "virtual" image */
        $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
    
        /* copy source image at a resized size */
        imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
    
        /* create the physical thumbnail image to its destination */
        imagejpeg($virtual_image, $dest);
    }

    /**
     * 
     */
    public function updatePreference(Request $request)
    {
        // start - checking the client has passed all the required parameters
        $validator = Validator::make($request->all(), [
            'user_token' => 'required|exists:userkey,key',
            //'gender' => 'required',
            //'age' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'failed', 'message' => "Need a valid token"], 401);
//            return response()->json(['status' => 'failed', 'message' => $validator->errors()], 401);
        }
        // end - checking the client has passed all the required parameters

        $userToken = $request->input('user_token');
        //$age = $request->input('age');
        $age = !empty($request->input('age')) ? $request->input('age') : 0;
        $gender = $request->input('gender');
        $userDiets = $request->input('user_diets');
        $allergies = $request->input('allergies');
        $travelOften = !empty($request->input('travel_often')) ? implode(",", $request->input('travel_often')) : '';
        $travelWhy = !empty($request->input('travel_why')) ? implode(",", $request->input('travel_why')) : '';


        try {
            $userIDObj = Userkey::firstWhere('key', $userToken);
            $userID = $userIDObj->users_id;
            $users_id = $userID;
            $agegroup = $age;
            $gender = $gender;
            $user_diets = $userDiets;
            $user_intolerances = $allergies;

            $data_to_update = [
                'pref_age_group' => $agegroup,
                'pref_gender' => $gender,
                'updated_at' => date('Y-m-d H:i:s'),
                'pref_travel_pattern' => $travelOften,
                'pref_travel_for' => $travelWhy
            ];

            DB::table('users')
                ->where('id', $users_id)
                ->update($data_to_update);
            DB::table('users_preferenceoptions')->where('users_id', $users_id)->delete();
            $diet_data = [];
            if ($user_diets) {
                foreach ($user_diets as $diets_id) {
                    $diet_data[] = [
                        'users_id' => $users_id,
                        'preferenceoptions_id' => $diets_id,
                        'type' => 'Diet'
                    ];
                }
                DB::table('users_preferenceoptions')->insert($diet_data);
            }


            // Below product_intolerances
            $intolerance_data = [];
            if ($user_intolerances) {
                foreach ($user_intolerances as $intolerances_id) {
                    $intolerance_data[] = [
                        'users_id' => $users_id,
                        'preferenceoptions_id' => $intolerances_id,
                        'type' => 'Intolerence'
                    ];
                }
                DB::table('users_preferenceoptions')->insert($intolerance_data);
            }

            return response()->json(['status' => 'success', 'message' => 'User preferences updated'], 200);
        } catch (Exception $e) {
            return response()->json(['status' => 'failed', 'message' => 'An error occured. Please try again'], 500);
        }
    }
    
    private function getNotificationData($userToken)
    {
        /* code before 10th oct 2020 */
//        $notificationsQuery = "SELECT noti.id AS notification_id, noti.users_id, noti.message AS notification_messsage, noti.status AS notification_status,
//                                    noti.order_id AS order_id,ord.status AS order_status, prod.name AS product_name FROM `notifications` AS noti
//                                    LEFT JOIN `order` AS ord ON noti.order_id = ord.id JOIN `order_items` AS oi ON oi.order_id = ord.id
//                                    LEFT JOIN `products` AS prod ON prod.id = oi.products_id JOIN `userkey` AS ukey ON ukey.users_id = noti.users_id
//                                    WHERE ukey.key = :user_token AND noti.delivered = :delivered
//                                    AND noti.created_at BETWEEN DATE_SUB(ADDTIME(NOW(), '5:30'), INTERVAL 2 DAY) AND ADDTIME(NOW(), '5:30')
//                                    ORDER BY noti.order_id DESC, noti.created_at DESC";
//        $notificationsObject = DB::select($notificationsQuery, [":user_token" => $userToken, ":delivered" => "NO"]);
//        return $notificationsObject;
        /* code before 10th oct 2020 */
//        $notificationsQuery = "SELECT noti.id AS notification_id, noti.users_id, noti.message AS notification_messsage, noti.status AS notification_status,
//                                    noti.order_id AS order_id,ord.status AS order_status FROM `notifications` AS noti
//                                    LEFT JOIN `order` AS ord ON noti.order_id = ord.id JOIN `userkey` AS ukey ON ukey.users_id = noti.users_id
//                                    WHERE ukey.key = :user_token AND noti.delivered = :delivered
//                                    AND noti.created_at BETWEEN DATE_SUB(ADDTIME(NOW(), '5:30'), INTERVAL 2 DAY) AND ADDTIME(NOW(), '5:30')
//                                    ORDER BY noti.order_id DESC, noti.created_at DESC";
//        $notificationsObject = DB::select($notificationsQuery, [":user_token" => $userToken, ":delivered" => "NO"]);
//        return $notificationsObject;
        
        $notificationsQuery = "SELECT noti.id AS notification_id, noti.users_id, noti.message AS notification_messsage, noti.status AS notification_status,
                                    noti.order_id AS order_id,ord.status AS order_status, REPLACE(CONCAT(prod.name, ' and ', count(prod.id) - 1, ' more'),' and 0 more','') AS product_name FROM `notifications` AS noti
                                    LEFT JOIN `order` AS ord ON noti.order_id = ord.id JOIN `order_items` AS oi ON oi.order_id = ord.id
                                    LEFT JOIN `products` AS prod ON prod.id = oi.products_id JOIN `userkey` AS ukey ON ukey.users_id = noti.users_id
                                    WHERE ukey.key = :user_token AND noti.delivered = :delivered
                                    AND noti.created_at BETWEEN DATE_SUB(ADDTIME(NOW(), '5:30'), INTERVAL 2 DAY) AND ADDTIME(NOW(), '5:30')
                                    GROUP BY noti.id ORDER BY noti.order_id DESC, noti.created_at DESC";
        $notificationsObject = DB::select($notificationsQuery, [":user_token" => $userToken, ":delivered" => "NO"]);
        return $notificationsObject;
    }

    private function getNotificationDataCount($userToken)
    {
        $notificationsQuery = "SELECT noti.id AS notification_id, noti.users_id, noti.message AS notification_messsage, noti.status AS notification_status,
                                    noti.order_id AS order_id,ord.status AS order_status, prod.name AS product_name FROM `notifications` AS noti
                                    LEFT JOIN `order` AS ord ON noti.order_id = ord.id JOIN `order_items` AS oi ON oi.order_id = ord.id
                                    LEFT JOIN `products` AS prod ON prod.id = oi.products_id JOIN `userkey` AS ukey ON ukey.users_id = noti.users_id
                                    WHERE noti.status = :status AND ukey.key = :user_token AND noti.delivered = :delivered
                                    AND noti.created_at BETWEEN DATE_SUB(ADDTIME(NOW(), '5:30'), INTERVAL 2 DAY) AND ADDTIME(NOW(), '5:30')
                                    ORDER BY noti.order_id DESC, noti.created_at DESC";
        $notificationsObject = DB::select($notificationsQuery, [":status" => 'ACTIVE', ":user_token" => $userToken, ":delivered" => "NO"]);
        return $notificationsObject;        
    }

    private function generateUserToken($user, $key)
    {
        //start - Remove existing tokens for the user
        DB::delete("DELETE FROM `userkey` WHERE users_id = :user_id", [":user_id" => $user->id]);
        //end - Remove existing tokens for the user

        // start - generating token for user
        $timestamp = date('Y-m-d H:i:s');
        $userKey = new Userkey();
        $userKey->users_id = $user->id;
        $userKey->key = $key;
        $userKey->created_at = $timestamp;
        $userKey->updated_at = $timestamp;
        $userKey->save();
        // end - generating token for user

        return $userKey;
    }

    private function generateCode($limit)
    {
        $code = '';
        for ($i = 0; $i < $limit; $i++) {
            $code .= mt_rand(0, 9);
        }
        return substr(str_shuffle('abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789'), 0, 5) . $code;
    }

    private function saveUser($params)
    {
        $user = new User();
        $user->name = $params['firstName'] . ' ' . $params['lastName'];
        $user->first_name = $params['firstName'];
        $user->last_name = $params['lastName'];
        $user->email = $params['emailAddress'];
        $user->password = Hash::make($params['password']);
        $user->remember_token = $params['password'];
        $user->role_id = $this->getCustomerRole()[0]->id;
        $user->signup_type = $params['signupType'];
        $user->phone_number = $params['phoneNumber'];
        $user->nationality = $params['country'];
        $user->billing_name = " ";
        $user->billing_address = " ";
        $user->billing_city = " ";
        $user->billing_zip = " ";
        $user->billing_country = " ";
        $user->created_at = $params['timestamp'];
        $user->updated_at = $params['timestamp'];
        $user->type = 'CUSTOMER';
        $user->fcm_token = $params['fcm_token'];
        $user->save();
        return $user;
    }
}
