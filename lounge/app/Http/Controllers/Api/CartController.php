<?php

namespace App\Http\Controllers\Api;

use App\AddonCartItems;
use App\Addons;
use App\AddonsOrderItems;
use App\Cart;
use App\Cartitems;
use App\Cartkey;
use App\Http\Controllers\Controller;
use App\Order;
use App\Userkey;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Frontend\Checkout;
use App\Orderitems;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $cartKey = $request->input('cart_key');
        $userToken = $request->input('user_token');
        $productID = $request->input('product_id');

        // start - checking the client has passed all the required parameters
        $validator = Validator::make($request->all(), [
            'item_count' => 'required|numeric|min:1',
            'product_id' => 'required|numeric|min:1',
            'addons' => 'array'
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'failed', 'message' => $validator->errors()], 409);
        }
        // end - checking the client has passed all the required parameters

        // start- checking user token exists in the table
        $userID = null;
        if (!empty($userToken)) {
            $validator = Validator::make($request->all(), [
                'user_token' => 'exists:userkey,key',
            ]);
            if ($validator->fails()) {
                return response()->json(['status' => 'failed', 'message' => $validator->errors()], 401);
            }
            $userIDObj = Userkey::firstWhere('key', $userToken);
            $userID = $userIDObj->users_id;
        }
        // end- checking user token exists in the table

        // start- checking product exists in the table
        if (!empty($productID)) {
            $validator = Validator::make($request->all(), [
                'product_id' => 'exists:products,id',
            ]);
            if ($validator->fails()) {
                return response()->json(['status' => 'failed', 'message' => $validator->errors()], 401);
            }

            $priceData = DB::select("select actual_price from products where `id` = :id ", ['id' => $productID]);
            if (empty($priceData)) {
                return response()->json(['status' => 'failed', 'message' => 'No prodct available'], 409);
            }
        }
        // end- checking product exists in the table

        // start- checking cart key exists in the table
        if (!empty($cartKey)) {
            $validator = Validator::make($request->all(), [
                'cart_key' => 'exists:cartkey,key',
            ]);
            if ($validator->fails()) {
                return response()->json(['status' => 'failed', 'message' => $validator->errors()], 401);
            }

            $response = $this->updateCart(
                array(
                    'request' => $request,
                    'actual_price' => $priceData[0]->actual_price,
                    'user_id' => $userID
                )
            );
        } else {
            $response = $this->createCart(
                array(
                    'request' => $request,
                    'actual_price' => $priceData[0]->actual_price,
                    'user_id' => $userID
                )
            );
        }

        switch ($response['status']) {
            case 'success':
                return response()->json(['status' => $response['status'], 'cart_items_id' => $response['cart_items_id'], 'cartkey' => $response['cartkey']], $response['code']);
                break;

            case 'failed':
                return response()->json(['status' => $response['status'], 'message' => $response['message']], $response['code']);
                break;
        }
        // end- checking cart key exists in the table

    }

    private function createCart($params)
    {
        $timestamp = date('Y-m-d H:i:s');
        $item_count = $params['request']->input('item_count');
        $price = $params['actual_price'];
        $productsid =  $params['request']->input('product_id');
        $addons = $params['request']->input('addons');

        // start - database transaction
        DB::beginTransaction();
        try {
            $cart = new Cart();
            $cart->is_guest = is_null($params['user_id']) ? 'YES' : 'NO';
            $cart->item_count = $item_count;
            $tax_percentage = tax_percentage();
            $tax_total = (($price * $item_count) * $tax_percentage);
            $grant_total = ($price * $item_count) + $tax_total;
            $cart->grant_total = $grant_total;
            $cart->tax_total = $tax_total;
            $cart->created_at = $timestamp;
            $cart->updated_at = $timestamp;
            $cart->sub_total = ($price * $item_count);
            $cart->save();
            $cartid = $cart->id;

            $cartitems = new Cartitems();
            $cartitems->cart_id = $cartid;
            $cartitems->products_id = $productsid;
            $cartitems->item_count = $item_count;
            $cartitems->grant_total = $grant_total;
            $cartitems->tax_total = $tax_total;
            $cartitems->sub_total = ($price * $item_count);
            $cartitems->created_at = $timestamp;
            $cartitems->updated_at = $timestamp;
            $cartitems->save();
            $cart_items_id = $cartitems->id;

            $cartkey = new Cartkey();
            $cartkey->cart_id = $cartid;
            $key = md5($this->generateCode(10));
            $cartkey->key = $key;
            $cartkey->created_at = $timestamp;
            $cartkey->updated_at = $timestamp;
            $cartkey->save();
            if (!empty($addons)) {
                if (count($addons) > 0) {

                    $addonsArray = array();
                    foreach ($addons as $addon) {
                        $addonsArray[] = $addon['addons_id'];
                    }

                    $productAddonsQuery = "SELECT addons_id FROM `addons_product_mapping` 
                                WHERE products_id = :products_id AND addons_id NOT IN (" . implode(",", $addonsArray) . ")";
                    $productAddonsResult = DB::select($productAddonsQuery, [':products_id' => $productsid]);
                    foreach ($productAddonsResult as $productsAddon) {
                        $addons[] = array(
                            'item_count' => 0,
                            'addons_id' => $productsAddon->addons_id
                        );
                    }

                    foreach ($addons as $addon) {
                        $addonsCart = new AddonCartItems();
                        $addonsCart->cart_items_id = $cart_items_id;
                        $addonsCart->item_count = $addon['item_count'];
                        $addonsCart->addons_id = $addon['addons_id'];
                        $addonsCart->created_at = $timestamp;
                        $addonsCart->save();
                    }
                }
            }
            DB::commit();

            return array('status' => 'success', 'cartkey' => $key, 'cart_items_id' => $cart_items_id, 'code' => 201);
        } catch (Exception $e) {
            DB::rollBack();
            return array('status' => 'failed', 'message' => 'Databse query exception occured. Please try again', 'code' => 500);
        }
        // end - database transaction
    }

    private function updateCart($params)
    {
        $timestamp = date('Y-m-d H:i:s');
        $item_count = $params['request']->input('item_count');
        $price = $params['actual_price'];
        $productsid = $params['request']->input('product_id');
        $cartKey = $params['request']->input('cart_key');
        $addons = $params['request']->input('addons');

        // start - database transaction
        DB::beginTransaction();
        try {
            $cartKeyObj = Cartkey::firstWhere('key', $cartKey);
            $cartID = $cartKeyObj->cart_id;

            $tax_percentage = tax_percentage();
            $tax_total = (($price * $item_count) * $tax_percentage);
            $grant_total = ($price * $item_count) + $tax_total;

            $sqlQuery = '';
            if (!empty($addons)) {
                $addonsArray = array();
                foreach ($addons as $addon) {
                    $addonsArray[] = $addon['addons_id'];
                }

                $productAddonsQuery = "SELECT addons_id FROM `addons_product_mapping` 
                                WHERE products_id = :products_id AND addons_id NOT IN (" . implode(",", $addonsArray) . ")";
                $productAddonsResult = DB::select($productAddonsQuery, [':products_id' => $productsid]);
                foreach ($productAddonsResult as $productsAddon) {
                    $addons[] = array(
                        'item_count' => 0,
                        'addons_id' => $productsAddon->addons_id
                    );
                }
                $addonsArrayCount = count($addons);
                $addonsQuery = '';
                foreach ($addons as $arrayKey => $addon) {
                    $addonsQuery .= " (addons_id = " . $addon['addons_id'] . " AND item_count = " . $addon['item_count'] . ")";
                    $addonsQuery .= ' OR ';
                }
                $addonsQuery = rtrim($addonsQuery, " OR ");

                if (!empty($addonsQuery)) {
                    $sqlAddonQuery = "SELECT COUNT(aci.cart_items_id), aci.cart_items_id FROM addons_cart_items AS 
                                        aci LEFT JOIN cart_items AS ci ON ci.id = aci.cart_items_id 
                                        WHERE ci.cart_id = $cartID AND ci.products_id = $productsid
                                        AND  aci.cart_items_id IN ( SELECT cart_items_id FROM addons_cart_items
                                        WHERE $addonsQuery GROUP BY cart_items_id HAVING COUNT(cart_items_id) = $addonsArrayCount)
                                        GROUP by aci.cart_items_id HAVING COUNT(cart_items_id) = $addonsArrayCount ";
                    $addonResult = DB::select($sqlAddonQuery);
                } else {
                    $addonResult = [];
                }

                if (empty($addonResult)) {
                    $cart_items_id = $this->insertCartItems($cartID, $productsid, $item_count, $grant_total, $tax_total, $timestamp, $price);
                    $this->insertAddonItems($addons, $cart_items_id, $timestamp, $cartID, $productsid);
                } else {
                    $addonResult = Cartitems::find($addonResult[0]->cart_items_id);
                    $cart_items_id = $this->updateCartItem($addonResult, $item_count, $grant_total, $tax_total, $timestamp, $price);
                    $this->updateAddonCartItem($addonResult, $addons, $cartID, $cart_items_id);
                }
            } else {
                $sqlQuery = "SELECT ci.id, aci.cart_items_id, ci.item_count, ci.grant_total,
                                ci.sub_total, ci.tax_total FROM cart_items AS ci
                                LEFT JOIN addons_cart_items AS aci ON ci.id = aci.cart_items_id
                                WHERE ci.cart_id = $cartID AND ci.products_id = $productsid HAVING aci.cart_items_id IS NULL";

                $result = DB::select($sqlQuery);
                if (empty($result)) {
                    $cart_items_id = $this->insertCartItems($cartID, $productsid, $item_count, $grant_total, $tax_total, $timestamp, $price);
                } else {
                    $cartItem = Cartitems::find($result[0]->id);
                    $cart_items_id = $this->updateCartItem($cartItem, $item_count, $grant_total, $tax_total, $timestamp, $price);
                }
            }

            $carttotaldetails = DB::select("select sum(item_count) as itemcnt,sum(grant_total) as granttotal,sum(sub_total) as subtotal,sum(tax_total) as taxtotal  from cart_items where `cart_id` = :cartid group by  cart_id", ['cartid' => $cartID]);
            if (empty($carttotaldetails)) {
                $item_count_sum = 0;
                $grant_total_sum = 0.00;
                $sub_total_sum = 0.00;
                $tax_total_sum = 0.00;
            } else {
                $item_count_sum = $carttotaldetails[0]->itemcnt;
                $grant_total_sum = $carttotaldetails[0]->granttotal;
                $sub_total_sum = $carttotaldetails[0]->subtotal;
                $tax_total_sum = $carttotaldetails[0]->taxtotal;
            }

            DB::table('cart')
                ->where('id', $cartID)
                ->update([
                    'item_count'       => $item_count_sum,
                    'grant_total' => $grant_total_sum,
                    'sub_total' => $sub_total_sum,
                    'tax_total' => $tax_total_sum,
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
            DB::commit();

            return array('status' => 'success', 'cartkey' => $cartKey, 'cart_items_id' => $cart_items_id, 'code' => 201);
        } catch (Exception $e) {
            DB::rollBack();
            return array('status' => 'failed', 'message' => 'Databse query exception occured. Please try again. ' . $e->getMessage(), 'code' => 500);
        }
        // end - database transaction
    }

    private function insertCartItems($cartID, $productsid, $item_count, $grant_total, $tax_total, $timestamp, $price)
    {
        $cartitems = new Cartitems();
        $cartitems->cart_id = $cartID;
        $cartitems->products_id = $productsid;
        $cartitems->item_count = $item_count;
        $cartitems->grant_total = $grant_total;
        $cartitems->tax_total = $tax_total;
        $cartitems->sub_total = ($price * $item_count);
        $cartitems->created_at = $timestamp;
        $cartitems->updated_at = $timestamp;
        $cartitems->save();
        $cart_items_id = $cartitems->id;
        return $cart_items_id;
    }

    private function updateCartItem($cartItem, $item_count, $grant_total, $tax_total, $timestamp, $price)
    {
        $cartItemObj = $cartItem;
        $cartItemObj->item_count = $cartItem->item_count + $item_count;
        $cartItemObj->grant_total = $cartItem->grant_total + $grant_total;
        $cartItemObj->tax_total = $cartItem->tax_total + $tax_total;
        $cartItemObj->sub_total = $cartItem->sub_total + ($price * $item_count);
        $cartItemObj->updated_at = $timestamp;
        $cartItemObj->save();
        $cart_items_id = $cartItemObj->id;
        return $cart_items_id;
    }

    private function insertAddonItems($addons, $cart_items_id, $timestamp)
    {
        foreach ($addons as $addon) {
            $addonsCart = new AddonCartItems();
            $addonsCart->cart_items_id = $cart_items_id;
            $addonsCart->item_count = $addon['item_count'];
            $addonsCart->addons_id = $addon['addons_id'];
            $addonsCart->created_at = $timestamp;
            $addonsCart->save();
        }
    }

    private function updateAddonCartItem($addonsCart, $addons)
    {
        foreach ($addons as $addon) {
            $addonsCartArray = DB::select('select id,item_count  from addons_cart_items where cart_items_id = :cart_items_id and addons_id = :addons_id', [':cart_items_id' => $addonsCart->id, ':addons_id' => $addon['addons_id']]);
            $addonsCartObj = AddonCartItems::find($addonsCartArray[0]->id);
            $addonsCartObj->item_count = $addonsCartArray[0]->item_count + $addon['item_count'];
            $addonsCartObj->save();
        }
    }

    private function generateCode($limit)
    {
        $code = '';
        for ($i = 0; $i < $limit; $i++) {
            $code .= mt_rand(0, 9);
        }
        return substr(str_shuffle('abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789'), 0, 5) . $code;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function placeOrder(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_token' => 'required|exists:userkey,key',
            'flight_date' => 'required',
            'flight_number' => 'required',
            'flight_duration' => 'required|numeric',
            'cart_id' => 'required|integer|exists:cart,id',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'failed', 'message' => $validator->errors()], 409);
        }
        $cartID = $request->input('cart_id');
        $flightDate = $request->input('flight_date');
        $flightNumber = $request->input('flight_number');
        $flightDuration = $request->input('flight_duration');
        $userToken = $request->input('user_token');
        $userIDObj = Userkey::firstWhere('key', $userToken);
        $userID = $userIDObj->users_id;

        $response = $this->transferToOrder(
            array(
                'cartID' => $cartID,
                'flightDate' => $flightDate,
                'flightNumber' => $flightNumber,
                'flightDuration' => $flightDuration,
                'userID' => $userID,
            )
        );
        switch ($response['status']) {
            case 'success':
                return response()->json(['status' => $response['status'], 'orderID' => $response['order_ID']], $response['code']);
                break;

            case 'failed':
                return response()->json(['status' => $response['status'], 'message' => $response['message']], $response['code']);
                break;
        }
    }

    private function transferToOrder($params)
    {
        $timestamp = date('Y-m-d H:i:s');

        // start - database transaction
        DB::beginTransaction();
        try {

            // getting cart details
            $cart = Cart::find($params['cartID']);
            $order = new Order();
            $order->item_count = $cart->item_count;
            $order->grant_total = $cart->grant_total;
            $order->users_id = $params['userID'];
            $order->tax_total = $cart->tax_total;
            $order->created_at = $timestamp;
            $order->updated_at = $timestamp;
            $order->sub_total = $cart->sub_total;
            $order->status = 'ORDER_CONFIRMED';

            $flight_date = $params['flightDate'];
            $checkout = new Checkout();
            $gateData = $checkout->getFlightDate($params['flightNumber'], $flight_date);
            $apiGate = '-';
            $apiGatePickup = '';
            if ($gateData) {
                $apiGate = $gateData['gate'];
                $apiGatePickup = $gateData['pickup'];
            }

            $order->gate = $apiGate;
            $order->pickup_point = $apiGatePickup;
            $order->duration = $params['flightDuration'];
            $order->flight_number = $params['flightNumber'];
            $order->created_at = $timestamp;
            $order->updated_at = $timestamp;

            $order->order_at = $flight_date;
            $flight_slotes = DB::select("select `flight_slots`.`time_from`,`flight_slots`.`time_to`,`flight_slots`.`pickup_schedule`,`flight_slots`.`last_order`,`flight_slots`.`in_kitchen` from `flight_slots`");
            foreach ($flight_slotes as $key => $value) {
                $date = date('Y-m-d', strtotime($flight_date));
                $time_from = $date . " " . $value->time_from;
                $time_to = $date . " " . $value->time_to;
                $pickup_schedule = $date . " " . $value->pickup_schedule;
                $in_kitchen = $date . " " . $value->in_kitchen;
                $flight_date = date('Y-m-d H:i:s', strtotime($flight_date));
                if ($time_from <= $flight_date && $time_to >= $flight_date) {
                    $order->pickup_time = $pickup_schedule;
                }
            }

            $order->save();
            $orderID = $order->id;

            $cartItems = Cartitems::where('cart_id', $params['cartID'])
                ->get();
            $cartItemsID = array();
            foreach ($cartItems as $cartItem) {
                $orderitems = new Orderitems();
                $orderitems->order_id = $orderID;
                $orderitems->products_id = $cartItem->products_id;
                $orderitems->item_count = $cartItem->item_count;
                $orderitems->grant_total = $cartItem->grant_total;
                $orderitems->tax_total = $cartItem->tax_total;
                $orderitems->sub_total = $cartItem->sub_total;
                $orderitems->created_at = $timestamp;
                $orderitems->updated_at = $timestamp;
                $orderitems->save();
                $cartItemsID[] = $cartItem->id;

                $cartItemsAddons = AddonCartItems::where('cart_items_id', $cartItem->id)
                    ->get();

                foreach ($cartItemsAddons as $addon) {
                    $addonObj = Addons::find($addon->addons_id);
                    $addonsOrder = new AddonsOrderItems();
                    $addonsOrder->order_items_id = $orderitems->id;
                    $addonsOrder->item_count = $addon->item_count;
                    $addonsOrder->addons_id = $addon->addons_id;
                    $addonsOrder->total_amount = $addon->item_count * $addonObj->actual_price;
                    $addonsOrder->created_at = $timestamp;
                    $addonsOrder->save();
                }
            }

            Cart::find($params['cartID'])->delete();
            Cartitems::where('cart_id', $params['cartID'])->delete();
            if (!empty($cartItemsID)) {
                foreach ($cartItemsID as $cartItemID) {
                    AddonCartItems::where('cart_items_id', $cartItemID)->delete();
                }
            }

            DB::commit();
            return array('status' => 'success', 'order_ID' => $orderID, 'code' => 201);
        } catch (Exception $e) {
            DB::rollBack();
            return (array('status' => 'failed', 'message' => 'Database query exception occured. Please try again', 'code' => 500));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    /**
     * Get addon list.
     *
     */
    public function addon_list(Request $request)
    {
        $products_id = $request->post('products_id');
        if (empty($products_id)) {
            return response()->json(['status' => FALSE, 'message' => 'product_id is mandatory'], 401);
        }
        $addons_data = DB::select(
            "select addons_product_mapping.*,addons.name, addons.id as addon_id, addons.actual_price as addon_price from `addons_product_mapping`
                                    join `addons` on addons_product_mapping.addons_id = addons.id where `addons_product_mapping`.`products_id` = :products_id",
            ['products_id' => $products_id]
        );
        return response()->json([
            'status' => TRUE,
            'data' => $addons_data,
            'success' => 'Addons mapped with this product list'
        ], 200);
    }

    /**
     * Delete a cart item.
     *
     */
    public function delete_cart(Request $request)
    {
        $cartkey = $request->post('cartkey');
        $cart_items_id = $request->post('cart_items_id');
        $cartkey_data = DB::select("select `cartkey`.`cart_id` as cart_id  from `cartkey` where `cartkey`.`key` = :key", ['key' => $cartkey]);
        if (empty($cartkey_data)) {
            return response()->json(['status' => FALSE, 'message' => 'Unauthenticated'], 401);
        } else {
            if (empty($cart_items_id)) {
                return response()->json(['status' => FALSE, 'message' => 'cart_items_id is mandatory'], 401);
            }

            $cartitem_data = DB::select("select `cart_items`.`cart_id` as cart_id, `cart_items`.`products_id` as products_id, `cart_items`.`item_count` as item_count from `cart_items` where `cart_items`.`id` = :id", ['id' => $cart_items_id]);

            if ($cartkey_data[0]->cart_id != $cartitem_data[0]->cart_id) {
                return response()->json(['status' => FALSE, 'message' => 'Unauthenticated'], 401);
            }

            DB::table('addons_cart_items')->where('cart_items_id', '=', $cart_items_id)->delete();
            DB::table('cart_items')->where('id', '=', $cart_items_id)->delete();
            $carttotaldetails = DB::select("select sum(item_count) as itemcnt,sum(grant_total) as granttotal,sum(sub_total) as subtotal,sum(tax_total) as taxtotal  from cart_items where `cart_id` = :cartid group by  cart_id", ['cartid' => $cartkey_data[0]->cart_id]);
            if (empty($carttotaldetails)) {
                $item_count_sum = 0;
                $grant_total_sum = 0.00;
                $sub_total_sum = 0.00;
                $tax_total_sum = 0.00;
            } else {
                $item_count_sum = $carttotaldetails[0]->itemcnt;
                $grant_total_sum = $carttotaldetails[0]->granttotal;
                $sub_total_sum = $carttotaldetails[0]->subtotal;
                $tax_total_sum = $carttotaldetails[0]->taxtotal;
            }

            DB::table('cart')
                ->where('id', $cartkey_data[0]->cart_id)
                ->update([
                    'item_count'       => $item_count_sum,
                    'grant_total' => $grant_total_sum,
                    'sub_total' => $sub_total_sum,
                    'tax_total' => $tax_total_sum,
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
            $returnData = array('status' => TRUE, "message" => "Successfully removed cart item");
            return response()->json($returnData, 200);
        }
    }

    /**
     * Update a cart item.
     *
     */
    public function update_cart(Request $request)
    {
        $cartkey = $request->post('cartkey');
        $cart_items_id = $request->post('cart_items_id');
        $qty = $request->post('qty');
        $cartkey_data = DB::select("select `cartkey`.`cart_id` as cart_id  from `cartkey` where `cartkey`.`key` = :key", ['key' => $cartkey]);
        if (empty($cartkey_data)) {
            return response()->json(['status' => FALSE, 'message' => 'Unauthenticated'], 401);
        } else {
            $cartitem_data = DB::select("select `cart_items`.`cart_id` as cart_id, `cart_items`.`products_id` as products_id, `cart_items`.`item_count` as item_count from `cart_items` where `cart_items`.`id` = :id", ['id' => $cart_items_id]);

            if ($cartkey_data[0]->cart_id != $cartitem_data[0]->cart_id) {
                return response()->json(['status' => FALSE, 'message' => 'Unauthenticated'], 401);
            }

            if (empty($cart_items_id) || !isset($qty)) {
                return response()->json(['status' => FALSE, 'message' => 'cart_items_id and qty are mandatory'], 401);
            }
            if ($qty < 1) {
                DB::table('addons_cart_items')->where('cart_items_id', '=', $cart_items_id)->delete();
                DB::table('cart_items')->where('id', '=', $cart_items_id)->delete();
            } else {
                $product_data = DB::select("select `products`.`actual_price` as actual_price from `products` where `products`.`id` = :id", ['id' => $cartitem_data[0]->products_id]);
                $tax_percentage = tax_percentage();
                $price = $product_data[0]->actual_price;
                $tax_total = (($price * $qty) * $tax_percentage);
                $grant_total = ($price * $qty) + $tax_total;
                $data_to_update = [
                    'item_count' => $qty,
                    'grant_total' => $grant_total,
                    'sub_total' => $price * $qty,
                    'tax_total' => $tax_total,
                    'updated_at' => date('Y-m-d H:i:s'),
                ];

                $cartItemObj = Cartitems::find($cart_items_id);
                $addonsCartArray = DB::select('select addons_id AS id,item_count  from addons_cart_items where cart_items_id = :cart_items_id ', [':cart_items_id' => $cart_items_id]);

                if (!empty($addonsCartArray)) {
                    foreach ($addonsCartArray as $addon) {
                        $addonsMaster = Addons::find($addon->id);
                        $cartItemObj->grant_total += ($addonsMaster['actual_price'] * $addon->item_count);
                    }
                }
                $cartItemObj->save();

                DB::table('cart_items')
                    ->where('id', $cart_items_id)
                    ->update($data_to_update);
            }

            $carttotaldetails = DB::select("select sum(item_count) as itemcnt,sum(grant_total) as granttotal,sum(sub_total) as subtotal,sum(tax_total) as taxtotal  from cart_items where `cart_id` = :cartid group by  cart_id", ['cartid' => $cartkey_data[0]->cart_id]);
            if (empty($carttotaldetails)) {
                $item_count_sum = 0;
                $grant_total_sum = 0.00;
                $sub_total_sum = 0.00;
                $tax_total_sum = 0.00;
            } else {
                $item_count_sum = $carttotaldetails[0]->itemcnt;
                $grant_total_sum = $carttotaldetails[0]->granttotal;
                $sub_total_sum = $carttotaldetails[0]->subtotal;
                $tax_total_sum = $carttotaldetails[0]->taxtotal;
            }

            DB::table('cart')
                ->where('id', $cartkey_data[0]->cart_id)
                ->update([
                    'item_count'       => $item_count_sum,
                    'grant_total' => $grant_total_sum,
                    'sub_total' => $sub_total_sum,
                    'tax_total' => $tax_total_sum,
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);

            $this->calculateCartTotal($cartkey);

            $returnData = array('status' => TRUE, "message" => "Successfully updated cart item");
            return response()->json($returnData, 200);
        }
    }

    private function calculateCartTotal($key)
    {
        $cartTotalQuery = "SELECT SUM(ci.grant_total) as grant_total, COUNT(ci.id) as total_count 
                            FROM cart_items AS ci LEFT JOIN cartkey AS c ON ci.cart_id = c.cart_id
                            WHERE c.key = :key";
        $cartTotalResults = DB::select($cartTotalQuery, [":key" => $key]);
        $cartkey = DB::select("select cart_id from cartkey where `key` = :cartkey ", ['cartkey' => $key]);
        $cart = Cart::find($cartkey[0]->cart_id);
        $cart->grant_total = $cartTotalResults[0]->grant_total;
        $cart->item_count = $cartTotalResults[0]->total_count;
        $cart->save();
    }

    /**
     * Update a cart item addon.
     *
     */
    public function update_cart_addon(Request $request)
    {
        $cartkey = $request->post('cart_key');
        $cart_items_id = $request->post('cart_items_id');
        $addons = $request->input('addons');
        $cartkey_data = DB::select("select `cartkey`.`cart_id` as cart_id  from `cartkey` where `cartkey`.`key` = :key", ['key' => $cartkey]);

        if (empty($cartkey_data)) {
            return response()->json(['status' => FALSE, 'message' => 'Unauthenticated'], 401);
        } else {
            if (empty($cart_items_id)) {
                return response()->json(['status' => FALSE, 'message' => 'cart_items_id is mandatory'], 401);
            }

            $validator = Validator::make($request->all(), [
                'addons' => 'array'
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'failed', 'message' => $validator->errors()], 409);
            }

            $cartitem_data = DB::select("select `cart_items`.`cart_id` as cart_id, `cart_items`.`products_id` as products_id, `cart_items`.`item_count` as item_count from `cart_items` where `cart_items`.`id` = :id", ['id' => $cart_items_id]);

            if ($cartkey_data[0]->cart_id != $cartitem_data[0]->cart_id) {
                return response()->json(['status' => FALSE, 'message' => 'Unauthenticated'], 401);
            }

            $timestamp = date('Y-m-d H:i:s');
            $tax_percentage = tax_percentage();
            $product_data = DB::select("select `products`.`actual_price` as actual_price from `products` where `products`.`id` = :id", ['id' => $cartitem_data[0]->products_id]);
            $tax_percentage = tax_percentage();
            $price = $product_data[0]->actual_price;
            $item_count = $cartitem_data[0]->item_count;
            $tax_total = (($price * $item_count) * $tax_percentage);
            $grant_total = ($price * $item_count) + $tax_total;
            $cartID = $cartitem_data[0]->cart_id;
            $productsid = $cartitem_data[0]->products_id;
            $cartItemID = $cart_items_id;

            if (!empty($addons)) {
                $addonsArray = array();
                foreach ($addons as $addon) {
                    $addonsArray[] = $addon['addons_id'];
                }

                $productAddonsQuery = "SELECT addons_id FROM `addons_product_mapping` 
                            WHERE products_id = :products_id AND addons_id NOT IN (" . implode(",", $addonsArray) . ")";
                $productAddonsResult = DB::select($productAddonsQuery, [':products_id' => $productsid]);
                foreach ($productAddonsResult as $productsAddon) {
                    $addons[] = array(
                        'item_count' => 0,
                        'addons_id' => $productsAddon->addons_id
                    );
                }
                $addonsArrayCount = count($addons);
                $addonsQuery = '';
                foreach ($addons as $arrayKey => $addon) {
                    $addonsQuery .= " (addons_id = " . $addon['addons_id'] . " AND item_count = " . $addon['item_count'] . ")";
                    $addonsQuery .= ' OR ';
                }
                $addonsQuery = rtrim($addonsQuery, " OR ");

                if (!empty($addonsQuery)) {
                    $sqlAddonQuery = "SELECT COUNT(aci.cart_items_id), aci.cart_items_id FROM addons_cart_items AS 
                                        aci LEFT JOIN cart_items AS ci ON ci.id = aci.cart_items_id 
                                        WHERE ci.cart_id = $cartID AND ci.products_id = $productsid
                                        AND  aci.cart_items_id IN ( SELECT cart_items_id FROM addons_cart_items
                                        WHERE $addonsQuery GROUP BY cart_items_id HAVING COUNT(cart_items_id) = $addonsArrayCount)
                                        GROUP by aci.cart_items_id HAVING COUNT(cart_items_id) = $addonsArrayCount ";
                    $addonResult = DB::select($sqlAddonQuery);
                } else {
                    $addonResult = [];
                }
                if (!empty($cartItemID)) {
                    $operation = 'insertion';
                    if (empty($addonResult)) {
                        $type = '';
                        $addonResult = Cartitems::find($cartItemID);
                        $this->updateAddonCartItemNew($addonResult, $addons, $type, $cartID, $cartItemID);
                    } else {
                        $type = 'update';
                        $checkUserHasUpdatedQuery = "SELECT * FROM `addons_cart_items` WHERE `cart_items_id` = :cart_items_id
                                                        AND ($addonsQuery) GROUP BY `cart_items_id` HAVING COUNT(`cart_items_id`) = $addonsArrayCount ";
                        $checkUserHasUpdatedQueryResult = DB::select($checkUserHasUpdatedQuery, [':cart_items_id' => $cartItemID]);
                        if (empty($checkUserHasUpdatedQueryResult)) {
                            $addonResult = Cartitems::find($addonResult[0]->cart_items_id);
                            $cart_items_id = $this->updateCartItemNew($addonResult, $item_count, $grant_total, $tax_total, $timestamp, $price, $operation);
                            $this->updateAddonCartItemNew($addonResult, $addons, $type, $cartID, $cart_items_id);

                            $cartItemObj = Cartitems::find($cartItemID);
                            $cart = Cart::find($cartItemObj->cart_id);
                            $cart->grant_total -=  $cartItemObj->grant_total;
                            $cart->sub_total -=  $cartItemObj->grant_total;
                            $cart->item_count -=  $cartItemObj->item_count;

                            // the above code is to club the items together
                            // find addons price and subtract it from cart
                            $addonCartItems = DB::select("SELECT SUM(aci.item_count * a.actual_price) AS total  FROM `addons` AS a JOIN addons_cart_items AS aci 
                                                        ON aci.addons_id = a.id WHERE aci.cart_items_id = :cart_item_id", [':cart_item_id' => $cartItemID]);

                            $cart->grant_total -=  $addonCartItems[0]->total;
                            $cart->sub_total -=  $addonCartItems[0]->total;

                            if ($cart->grant_total <= 0 || $cart->item_count <= 0) {
                                $cart->grant_total =  0;
                                $cart->sub_total =  0;
                                $cart->item_count =  0;
                            }

                            $cart->save();

                            DB::delete('DELETE FROM `cart_items` WHERE id = :cart_item_id', [':cart_item_id' => $cartItemID]);
                            DB::delete('DELETE FROM `addons_cart_items` WHERE  cart_items_id = :cart_item_id', [':cart_item_id' => $cartItemID]);
                        }
                    }
                }
            }

            // $result = DB::select('select cart.id as cartid, '
            //     . ' cart_items.id as cart_items_id,'
            //     . ' addons_cart_items.addons_id as addons_id'

            //     . ' from cart '

            //     . ' join cart_items on cart.id = cart_items.cart_id and cart_items.products_id = :products_id'
            //     . ' left join addons_cart_items on cart_items.id = addons_cart_items.cart_items_id '
            //     . ' where cart.id = :cartid', ['cartid' => $cartitem_data[0]->cart_id, 'products_id' => $cartitem_data[0]->products_id]);
            // $addons = $request->input('addons');
            // $empty_flag = false;
            // $match_cart_item_id = 0;
            // $posted_addons_array = array();
            // if (!empty($addons)) {
            //     foreach ($addons as $key => $value) {
            //         $posted_addons_array[] = $value['addons_id'];
            //     }
            //     if (!empty($result)) {
            //         $addons_array = array();
            //         foreach ($result as $key => $value) {
            //             $addons_array[$value->cart_items_id][] = $value->addons_id;
            //         }
            //         sort($posted_addons_array);
            //         foreach ($addons_array as $key => $value) {
            //             sort($value);
            //             if ($posted_addons_array == $value) {
            //                 $match_cart_item_id = $key;
            //             }
            //         }
            //     }
            // } else {
            //     if (!empty($result)) {
            //         $addons_array = array();
            //         foreach ($result as $key => $value) {
            //             $addons_array[$value->cart_items_id][] = $value->addons_id;
            //         }
            //         foreach ($addons_array as $key => $value) {
            //             if (empty($value[0])) {
            //                 $match_cart_item_id = $key;
            //                 $empty_flag = true;
            //             }
            //         }
            //     }
            // }

            // $timestamp = date('Y-m-d H:i:s');
            // if (($cart_items_id == $match_cart_item_id) || $match_cart_item_id == 0) {
            //     DB::table('addons_cart_items')->where('cart_items_id', '=', $cart_items_id)->delete();
            //     if (!empty($addons)) {
            //         if (count($addons) > 0) {
            //             foreach ($addons as $addon) {
            //                 if ($addon['item_count'] > 0) {
            //                     $addonsCart = new AddonCartItems();
            //                     $addonsCart->cart_items_id = $cart_items_id;
            //                     $addonsCart->item_count = $addon['item_count'];
            //                     $addonsCart->addons_id = $addon['addons_id'];
            //                     $addonsCart->created_at = $timestamp;
            //                     $addonsCart->save();
            //                 }
            //             }
            //         }
            //     }
            // } else if ($empty_flag) {
            //     $matchitem_data = DB::select("select `cart_items`.`cart_id` as cart_id, `cart_items`.`products_id` as products_id, `cart_items`.`item_count` as item_count from `cart_items` where `cart_items`.`id` = :id", ['id' => $match_cart_item_id]);
            //     $product_data = DB::select("select `products`.`actual_price` as actual_price from `products` where `products`.`id` = :id", ['id' => $matchitem_data[0]->products_id]);
            //     $tax_percentage = tax_percentage();
            //     $price = $product_data[0]->actual_price;
            //     // $item_count = $cartitem_data[0]->item_count + $matchitem_data[0]->item_count; 
            //     $item_count = $matchitem_data[0]->item_count;
            //     $tax_total = (($price * $item_count) * $tax_percentage);
            //     $grant_total = ($price * $item_count) + $tax_total;
            //     $addonResult = Cartitems::find($cart_items_id);
            //     $this->updateCartItem($addonResult, $item_count, $grant_total, $tax_total, $timestamp, $price);
            //     DB::table('addons_cart_items')->where('cart_items_id', '=', $match_cart_item_id)->delete();
            //     DB::table('cart_items')->where('id', '=', $match_cart_item_id)->delete();
            //     // $this->updateAddonCartItem($addonResult, $addons);
            // } else {
            //     $matchitem_data = DB::select("select `cart_items`.`cart_id` as cart_id, `cart_items`.`products_id` as products_id, `cart_items`.`item_count` as item_count from `cart_items` where `cart_items`.`id` = :id", ['id' => $match_cart_item_id]);
            //     $product_data = DB::select("select `products`.`actual_price` as actual_price from `products` where `products`.`id` = :id", ['id' => $matchitem_data[0]->products_id]);
            //     $tax_percentage = tax_percentage();
            //     $price = $product_data[0]->actual_price;
            //     // $item_count = $cartitem_data[0]->item_count + $matchitem_data[0]->item_count; 
            //     $item_count = $matchitem_data[0]->item_count;
            //     $tax_total = (($price * $item_count) * $tax_percentage);
            //     $grant_total = ($price * $item_count) + $tax_total;
            //     $addonResult = Cartitems::find($cart_items_id);
            //     $this->updateCartItem($addonResult, $item_count, $grant_total, $tax_total, $timestamp, $price);
            //     $addonMatchResult = Cartitems::find($match_cart_item_id);
            //     $this->updateAddonCartItem($addonMatchResult, $addons);
            //     $data_to_update = [
            //         'cart_items_id' => $cart_items_id,
            //         'updated_at' => $timestamp,
            //     ];

            //     DB::table('addons_cart_items')
            //         ->where('cart_items_id', $match_cart_item_id)
            //         ->update($data_to_update);
            //     // $this->insertAddonItems($addons, $cart_items_id, $timestamp);
            //     // DB::table('addons_cart_items')->where('cart_items_id', '=', $match_cart_item_id)->delete();
            //     DB::table('cart_items')->where('id', '=', $match_cart_item_id)->delete();
            // }

            $carttotaldetails = DB::select("select sum(item_count) as itemcnt,sum(grant_total) as granttotal,sum(sub_total) as subtotal,sum(tax_total) as taxtotal  from cart_items where `cart_id` = :cartid group by  cart_id", ['cartid' => $cartitem_data[0]->cart_id]);
            if (empty($carttotaldetails)) {
                $item_count_sum = 0;
                $grant_total_sum = 0.00;
                $sub_total_sum = 0.00;
                $tax_total_sum = 0.00;
            } else {
                $item_count_sum = $carttotaldetails[0]->itemcnt;
                $grant_total_sum = $carttotaldetails[0]->granttotal;
                $sub_total_sum = $carttotaldetails[0]->subtotal;
                $tax_total_sum = $carttotaldetails[0]->taxtotal;
            }

            DB::table('cart')
                ->where('id', $cartitem_data[0]->cart_id)
                ->update([
                    'item_count'       => $item_count_sum,
                    'grant_total' => $grant_total_sum,
                    'sub_total' => $sub_total_sum,
                    'tax_total' => $tax_total_sum,
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);

            $returnData = array('status' => TRUE, "cart_items_id" => $cart_items_id, "message" => "Successfully updated cart item addon");
            return response()->json($returnData, 200);
        }
    }

    private function updateAddonCartItemNew($addonsCart, $addons, $type, $cartID, $cartItemID)
    {
        $cart = Cart::find($cartID);
        $cartItemObj = Cartitems::find($cartItemID);
        foreach ($addons as $addon) {
            $addonsCartArray = DB::select('select id,item_count  from addons_cart_items where cart_items_id = :cart_items_id and addons_id = :addons_id', [':cart_items_id' => $addonsCart->id, ':addons_id' => $addon['addons_id']]);
            if (count($addonsCartArray) > 0) {
                $addonsCartObj = AddonCartItems::find($addonsCartArray[0]->id);
                $addonsMaster = Addons::find($addon['addons_id']);

                if ($type == 'update') {
                    $addonsCartObj->item_count = $addon['item_count'];
                    $cart->grant_total +=  ($addonsMaster->actual_price * ($addon['item_count']));
                    $cart->sub_total +=  ($addonsMaster->actual_price * ($addon['item_count']));
                    $cart->save();
                } else {
                    if ($addonsCartObj->item_count < $addon['item_count']) {
                        $cart->grant_total +=  ($addonsMaster->actual_price * (($addon['item_count'] - $addonsCartObj->item_count) * $cartItemObj->item_count));
                        $cart->sub_total +=  ($addonsMaster->actual_price * (($addon['item_count'] - $addonsCartObj->item_count) * $cartItemObj->item_count));
                        $cart->save();
                    }
                    if ($addonsCartObj->item_count > $addon['item_count']) {
                        if (($addonsCartObj->item_count - $addon['item_count']) != 0) {
                            $cart->grant_total -=  $addonsMaster->actual_price * (($addonsCartObj->item_count - $addon['item_count']) * $cartItemObj->item_count);
                            $cart->sub_total -=  $addonsMaster->actual_price * (($addonsCartObj->item_count - $addon['item_count']) * $cartItemObj->item_count);
                        } else {
                            $cart->grant_total -=  ($addonsMaster->actual_price * (($addonsCartObj->item_count - $addon['item_count']) * $cartItemObj->item_count));
                            $cart->sub_total -=  ($addonsMaster->actual_price * (($addonsCartObj->item_count - $addon['item_count']) * $cartItemObj->item_count));
                        }
                        $cart->save();
                    }

                    $addonsCartObj->item_count = $addon['item_count'];
                }

                $addonsCartObj->save();
            } else {
                $addonsMaster = Addons::find($addon['addons_id']); 
                $cart->grant_total +=  $addonsMaster->actual_price * $addon['item_count'];
                $cart->sub_total +=  $addonsMaster->actual_price * $addon['item_count'];
                $cart->save();
                $addonsCartObj = new AddonCartItems();
                $addonsCartObj->item_count = $addon['item_count'];
                $addonsCartObj->addons_id = $addon['addons_id'];
                $addonsCartObj->cart_items_id = $cartItemID;
                $addonsCartObj->save();
            }
        }
        if ($cart->grant_total <= 0 || $cart->item_count <= 0) {
            $cart->grant_total =  0;
            $cart->sub_total =  0;
            $cart->item_count =  0;
        }
        $cart->save();
    }

    private function updateCartItemNew($cartItem, $item_count, $grant_total, $tax_total, $timestamp, $price, $operation)
    {
        $cartItemObj = $cartItem;
        $cart = Cart::find($cartItemObj->cart_id);
        if ($operation == 'insertion') {
            $cartItemObj->item_count += $item_count;
            $cartItemObj->grant_total += $grant_total;
            $cartItemObj->tax_total += $tax_total;

            $cart->grant_total +=  $grant_total;
            $cart->sub_total +=  $grant_total;
            $cart->item_count += 1;
            $cart->save();
        }
        if ($operation == 'removal') {
            $cartItemObj->item_count -= $item_count;
            $cartItemObj->grant_total -= $grant_total;
            $cartItemObj->tax_total -= $tax_total;

            $cart->grant_total -=  $grant_total;
            $cart->sub_total -=  $grant_total;
            $cart->item_count -= 1;

            if ($cart->item_count < 0 || $cart->grant_total < 0) {
                $cart->grant_total = 0;
                $cart->sub_total = 0;
                $cart->item_count = 0;
            }
            $cart->save();
        }

        $cartItemObj->updated_at = $timestamp;
        if ($cartItemObj->item_count <= 0) {
            DB::delete("DELETE FROM `cart_items` WHERE id = :id ", [":id" => $cartItemObj->id]);
        } else {
            $cartItemObj->save();
        }

        $cart_items_id = $cartItemObj->id;
        return $cart_items_id;
    }

    /**
     * Cart list.
     *
     */
    public function show(Request $request)
    {
        $cart_key = $request->post('cartkey');
        $cart_item_id = $request->post('cart_item_id');
        
        if (empty($cart_key)) {
            return response()->json(['status' => FALSE, 'message' => 'Key is not passed'], 401);
        }

        if (empty($cart_key)) {
            return response()->json(['status' => FALSE, 'message' => 'Key is not passed'], 401);
        }
        //$cartkey = DB::select("select id from cartkey where `key` = :cartkey ", ['cartkey' => $cart_key]);
        $cartkey = DB::select("select cart_id AS id from cartkey where `key` = :cartkey ", ['cartkey' => $cart_key]);
        if (empty($cartkey)) {
            return response()->json(['status' => FALSE, 'message' => 'Unauthenticated'], 401);
        } else {
            $cartid = $cartkey[0]->id;
            $dyn_qry_part = '';
            $dyn_condn_part = ['cartid' => $cartid];
            if (!empty($cart_item_id)) {
                $dyn_qry_part = ' and cart_items.id = :cart_item_id';
                $dyn_condn_part = ['cartid' => $cartid, 'cart_item_id' => $cart_item_id];
            }
            $result = DB::select('select cart.id as cartid, '
                . ' cart_items.id as cart_items_id,'
                . ' addons.id as addons_id,'
                . ' addons.name as addons_name,'
                . ' addons.actual_price as addons_price,'
                . ' addons_cart_items.id as addons_cart_items_id,'
                . ' addons_cart_items.item_count as addons_item_count,'
                . ' cart.item_count as item_count_total,'
                . ' cart.grant_total as grant_total_total,'
                . ' cart.sub_total as sub_total_total,'
                . ' cart.tax_total as tax_total_total,'
                . ' cart_items.item_count as item_count_item_total,'
                . ' cart_items.grant_total as grant_total_item_total,'
                . ' cart_items.sub_total as sub_total_item_total,'
                . ' cart_items.tax_total as tax_total_item_total,'
                . ' products.id as product_id,'
                . ' products.name as product_name,'
                . 'product_images.path,'
                . 'addon_images.path as addon_image_path'

                . ' from cart '

                . ' join cart_items on cart.id = cart_items.cart_id '
                . ' left join addons_cart_items on cart_items.id = addons_cart_items.cart_items_id and addons_cart_items.item_count != 0'
                . ' left join addons on addons.id = addons_cart_items.addons_id '
                . ' join products on products.id = cart_items.products_id '
                . ' left join product_images on products.id = product_images.products_id and product_images.type="THUMBNAIL"'
                . ' left join addon_images on addons.id = addon_images.addons_id and addon_images.type="THUMBNAIL"'
                . ' where cart.id = :cartid' . $dyn_qry_part . '', $dyn_condn_part);

            $modifiedResult = array(
                'maindata' => array(),
                'eachdata' => array(),
                'status' => TRUE
            );
            foreach ($result as $key => $value) {
                $modifiedResult['maindata'] = array(
                    'id' => $value->cartid,
                    'item_count_total' => $value->item_count_total,
                    'grant_total_total' => $value->grant_total_total,
                    'sub_total_total' => $value->sub_total_total,
                    'tax_total_total' => $value->tax_total_total
                );
                $temp_addons = array();
                if (isset($modifiedResult['eachdata'][$value->cart_items_id])) {
                    if (isset($modifiedResult['eachdata'][$value->cart_items_id]['addons'])) {
                        $temp_addons = $modifiedResult['eachdata'][$value->cart_items_id]['addons'];
                    }
                }
                $modifiedResult['eachdata'][$value->cart_items_id] = array(
                    'id' => $value->cart_items_id,
                    'item_count_item_total' => $value->item_count_item_total,
                    'grant_total_item_total' => $value->grant_total_item_total,
                    'sub_total_item_total' => $value->sub_total_item_total,
                    'tax_total_item_total' => $value->tax_total_item_total,
                    'product_id' => $value->product_id,
                    'product_name' => $value->product_name,
                    'image_path' => url($value->path),
                    'addons' => $temp_addons
                );
                $addons_data = DB::select("select `addons`.`id` as id, `addons`.`name` as name, `addons`.`actual_price` as actual_price from `addons_product_mapping` "
                    . "join `addons` on `addons`.`id` =  `addons_product_mapping`.`addons_id` where `addons_product_mapping`.`products_id` = :products_id", ['products_id' => $value->product_id]);
                $modifiedResult['eachdata'][$value->cart_items_id]['product_addons'] = $addons_data;
                if ($value->addons_cart_items_id != null) {
                    $addon_total_price = $value->addons_item_count * $value->addons_price;
                    $modifiedResult['eachdata'][$value->cart_items_id]['addons'][$value->addons_cart_items_id] = array(
                        'id' => $value->addons_id,
                        'addon_count' => $value->addons_item_count,
                        'addon_total_price' => $addon_total_price,
                        'addon_item_price' => $value->addons_price,
                        'addon_name' => $value->addons_name,
                        'image_path' => url($value->addon_image_path)
                    );
                }
            }

            $modifiedResult['each'] = array();
            if (!empty($modifiedResult['eachdata'])) {
                foreach ($modifiedResult['eachdata'] as $key => $value) {
                    $temp_each = $modifiedResult['eachdata'][$key];
                    array_push($modifiedResult['each'], $temp_each);
                }
            }
            unset($modifiedResult['eachdata']);


            if (!empty($modifiedResult['each'])) {
                foreach ($modifiedResult['each'] as $key => $value) {
                    $addon_total = 0;
                    $grant_product_total = 0;
                    $grant_total = 0;
                    $modifiedResult['each'][$key]['addon'] = array();
                    if (isset($modifiedResult['each'][$key]['addons'])) {
                        foreach ($modifiedResult['each'][$key]['addons'] as $akey => $avalue) {
                            $temp_addon = $modifiedResult['each'][$key]['addons'][$akey];
                            $addon_total += $modifiedResult['each'][$key]['addons'][$akey]['addon_total_price'];
                            array_push($modifiedResult['each'][$key]['addon'], $temp_addon);
                            // $grant_product_total += ($modifiedResult['each'][$key]['addons'][$akey]['addon_item_price'] * $modifiedResult['each'][$key]['addons'][$akey]['addon_count']);
                            $grant_product_total += ($modifiedResult['each'][$key]['addons'][$akey]['addon_item_price'] * ($modifiedResult['each'][$key]['addons'][$akey]['addon_count'] * $modifiedResult['each'][$key]['item_count_item_total'])); // temp fix
                            $grant_total += ($modifiedResult['each'][$key]['addons'][$akey]['addon_item_price'] * ($modifiedResult['each'][$key]['addons'][$akey]['addon_count'] * $modifiedResult['each'][$key]['item_count_item_total'])); //temp fix
                        }
                        $grant_product_total -= $addon_total; // temporary fix
                        unset($modifiedResult['each'][$key]['addons']);
                    } else {
                    }
                    $modifiedResult['each'][$key]['grant_total_item_total'] += $grant_product_total;
                    $modifiedResult['each'][$key]['sub_total_item_total'] += $grant_product_total;
                    $modifiedResult['maindata']['grant_total_total'] += $grant_total; //temporary fix

                }

                if ($addon_total != 0) {
                    $modifiedResult['maindata']['addons_total'] = amount_format($addon_total);
                    $modifiedResult['maindata']['grant_total_total'] -=  $modifiedResult['maindata']['addons_total']; //temp fix
                }
            }
            $modifiedResult['maindata']['item_count_total'] = count($modifiedResult['each']);

            $relatedproducts_allergy_map = array();
            if ($cart_item_id) {
                $category_id = $request->post('category_id');
                $pid = DB::table('cart_items')->where('id' ,$cart_item_id)->value('products_id');
               
                $relatedProductsIdsArray = DB::select('select pr.product_rel_id as prid from product_relation as pr 
                                                where pr.products_id = "' . $pid . '"');
                $prIdArray = array();
                foreach ($relatedProductsIdsArray as $key => $value) {
                    $prIdArray[] = $value->prid;
                }
                
                $inPrIds = implode(',', $prIdArray);
                if (empty($inPrIds)) {
                    $inPrIds = 0;
                }
                    
                $relatedproducts = DB::select('select distinct p.id, p.name, p.actual_price, p.url_key, pi.type, 
                                                pi.path from products as p 
                                                left join  product_category as pc on p.id = pc.products_id
                                                left join product_images as pi on p.id = pi.products_id 
                                                where  p.id in (' . $inPrIds . ') AND 
                                                pi.type = "THUMBNAIL" AND p.id != "' . $pid . '" LIMIT 3');
//                echo "<pre>";
//                    print_r($relatedproducts);
//                    exit;
//                $relatedproducts = DB::select('select distinct p.id, p.name, p.actual_price, p.url_key, pi.type, 
//                                                pi.path from products as p 
//                                                left join  product_category as pc on p.id = pc.products_id
//                                                left join product_images as pi on p.id = pi.products_id 
//                                                where  pc.category_id = "' . $category_id . '" AND 
//                                                pi.type = "THUMBNAIL" AND p.id != "' . $pid . '" LIMIT 3');

                $relateproductsdallergies = DB::select('select distinct pa.products_id, a.name, a.description, 
                                a.image_path from product_category as pc 
                                left join product_allergies as pa on pa.products_id = pc.products_id
                                left join allergies as a on pa.allergies_id = a.id 
                                where  pa.products_id != :product_id and a.visibility = "YES"', ['product_id' => $pid]);

                foreach ($relatedproducts as $key => $relatedproduct) {
                    $relatedproducts_allergy_map[$key]['product_id'] = $relatedproduct->id;
                    $relatedproducts_allergy_map[$key]['name'] = $relatedproduct->name;
                    $relatedproducts_allergy_map[$key]['actual_price'] = $relatedproduct->actual_price;
                    $relatedproducts_allergy_map[$key]['path'] = url($relatedproduct->path);
                    $relatedproducts_allergy_map[$key]['type'] = $relatedproduct->type;
                    $relatedproducts_allergy_map[$key]['url_key'] = 'productdetail/' . $relatedproduct->url_key;
                    foreach ($relateproductsdallergies as  $relateproductsdallergy) {
                        if ($relatedproduct->id == $relateproductsdallergy->products_id) {
                            $relateproductsdallergy->image_path = url($relateproductsdallergy->image_path);
                            $relatedproducts_allergy_map[$key]['allergies_list'][] = $relateproductsdallergy;
                        }
                    }
                }
            }
            return response()->json([
                'status' => TRUE,
                'data' => $modifiedResult,
                'relatedproducts' => $relatedproducts_allergy_map,
                'success' => 'Cart list'
            ], 200);
        }
    }
}
