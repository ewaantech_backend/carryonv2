<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Userkey;
use Auth;
use App\Orderlog;
use App\Notifications;
use App\Readymail;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrderReadyForPickup;
use PDF;
use App\ShortLink;
use Illuminate\Support\Str;

class ApiController extends Controller
{
    private static $API_ACCESS_KEY = 'AAAA0Ihrr6E:APA91bHDWmqOd5813O2xPryWcxkqrUxl9_j6KjwuRDgIg_Elmtl6zKhc4k3gq81vFJDEc3wlV0ClYJc8dFYiKXsmVgPvCESxUPXUD_nsTysb5T7GAS5c710Xtx3eVqhk6Aky6cEg96aE';//'AIzaSyCCxJPno4WLLkGO6LM92weZ9ipY_jyGA5s';
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      
    }
    
    
    // Sends Push notification for Android users
    public function android($data, $reg_id) {
            $url = 'https://fcm.googleapis.com/fcm/send';
//            $message = array(
//                'title' => $data['mtitle'],
//                'message' => $data['mdesc'],
//                'subtitle' => '',
//                'tickerText' => '',
//                'msgcnt' => 1,
//                'vibrate' => 1
//            );
//test
            $message = array(
                'title' => $data['mtitle'],
                'body' => $data['mdesc'],
                'vibrate' => 1
//                'sound' => "default",
//                'icon' => 'ic_notification'
            );

            $headers = array(
                    'Authorization: key=' .self::$API_ACCESS_KEY,
                    'Content-Type: application/json'
            );

            $fields = array(
                'registration_ids' => array($reg_id),
                'notification' => $message,
                "android" => [
                    "notification" => [
                        "sound"  => "default",
                        "icon"  => "ic_notification",
                     ]
                 ],
                'data' => $message
            );

            return $this->useCurl($url, $headers, json_encode($fields));
    }
    
    // Curl 
    private function useCurl($url, $headers, $fields = null) {
            // Open connection
            $ch = curl_init();
            if ($url) {
                // Set the url, number of POST vars, POST data
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                // Disabling SSL Certificate support temporarly
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                if ($fields) {
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                }

                // Execute post
                $result = curl_exec($ch);
                if ($result === FALSE) {
                    die('Curl failed: ' . curl_error($ch));
                }

                // Close connection
                curl_close($ch);
//                echo "<pre>";
//                    print_r($result);
//                    exit;
                return $result;
        }
    }
        
        
    /**
     * Login a Registered Users.
     *
     */
    public function login(Request $request){
        $uname = $request->post('username');
        $password = $request->post('password');
        if (Auth::attempt(array('email' => $uname, 'password' => $password))){
            // user data
            $user_data = DB::table('users')->select('id', 'name' , 'phone_number' , 'email', 'is_default_pickup_person', 'pickup_point', 'fcm_token')
                        ->where('email', '=', $uname)->first();
            $userid = $user_data->id;
            $fcm_token = $user_data->fcm_token;
            $user = \App\User::find($userid);
            $roles = $user->getRoleNames();
            $permissions_data = json_decode(json_encode($roles), true);
            if( in_array('pickup', $permissions_data) ) {
                $timestamp = date('Y-m-d H:i:s');
                $userkey = new Userkey;
                $userkey->users_id = $userid;
                $key = md5($this->generateCode(10));
                $userkey->key = $key;
                $userkey->created_at = $timestamp;
                $userkey->updated_at = $timestamp;
                $userkey->save();
                if ($request->post('fcm_token')) {
                    $fcm_token = $request->post('fcm_token');
                    $response = DB::table('users')->where('fcm_token', '=', $fcm_token)->update([
                        'fcm_token' => '',
                        'updated_at' => date("Y-m-d H:i:s")
                    ]);
                    $response = DB::table('users')->where('id', '=', $userid)->update([
                                'fcm_token' => $request->post('fcm_token')
                                ]);
                }
                date_default_timezone_set('Asia/Dubai');
                $date = date("Y-m-d H:i:s");
                $onedayafter = date("Y-m-d H:i:s", strtotime($date. ' -48 hours'));
                $notification_data = DB::select("select * from `notifications` where `notifications`.`users_id` = :users_id and `notifications`.`delivered` = 'NO' and `notifications`.`created_at` >= :onedayafter",['users_id' => $userid,'onedayafter' => $onedayafter]);
                if (!empty($notification_data)) {
                    foreach ($notification_data as $nkey => $nvalue) {
                        $msg_payload = array (
                                'mtitle' => 'Notification',
                                'mdesc' => $nvalue->message,
                        );

                        // For Android
                        $reg_id = $fcm_token;

                        $result = $this->android($msg_payload, $reg_id);                    
                    }                    
                }

                $response = DB::table('notifications')->where('users_id', '=', $userid)->update([
                    'delivered' => 'YES',
                    'updated_at' => date("Y-m-d H:i:s")
                ]);
                return response()->json([
                    'status' => TRUE, 
                    'success' => 'Successfully Logged in',
                    'key' => $key,
                    'user_data' => $user_data
                ], 200);
            } else {
                return response()->json(['status' => FALSE, 'message' => 'Invalid credentials'], 401);
            }

        }
        else {        
            return response()->json(['status' => FALSE, 'message' => 'Invalid credentials'], 401);
        }
    }
    
    /**
     * Logout a logged in user.
     *
     */
    public function logout(Request $request){
        $key = $request->post('key');
        $userkey_data = DB::select("select `userkey`.`users_id` as user_id  from `userkey` where `userkey`.`key` = :key",['key' => $key]);
        if (empty($userkey_data)) {
             return response()->json(['status' => FALSE, 'message' => 'Unauthenticated'], 401);           
        } else {
            DB::table('userkey')->where('key', '=', $key)->delete();
            $response = DB::table('notifications')->where('users_id', '=', $userkey_data[0]->user_id)->update([
                'delivered' => 'YES',
                'updated_at' => date("Y-m-d H:i:s")
            ]);
            $returnData = Array('status' => TRUE, "message" => "Successfully logged out");
            return response()->json($returnData, 200);
        }
    }
    
    /**
     * Get user profile.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile(Request $request)
    {
        $key = $request->post('key');
        $userkey_data = DB::select("select `userkey`.`users_id` as user_id  from `userkey` where `userkey`.`key` = :key",['key' => $key]);
        if (empty($userkey_data)) {
             return response()->json(['status' => FALSE, 'message' => 'Unauthenticated'], 401);           
        } else {
            $user_id = $userkey_data[0]->user_id;
            $customer_data = DB::table('users')->select('name', 'email', 'phone_number','billing_country','billing_city','billing_zip','billing_address')->where('id', '=', $user_id)->first();
            $returnData = Array('status' => TRUE, "message" => "Successfully fetched user data","data" => $customer_data);
            return response()->json($returnData, 200);
        }
    }
    
    
    /**
     * Profie update.
     *
     * @return \Illuminate\Http\Response
     */
    public function profileupdate(Request $request)
    {
        $key = $request->post('key');
        $userkey_data = DB::select("select `userkey`.`users_id` as user_id  from `userkey` where `userkey`.`key` = :key",['key' => $key]);
        if (empty($userkey_data)) {
             return response()->json(['status' => FALSE, 'message' => 'Unauthenticated'], 401);           
        } else {
            if (!$request->post('name') || !$request->post('email') || !$request->post('phone') || !$request->post('country') || !$request->post('city') || !$request->post('zip') || !$request->post('address')) {
                 return response()->json(['status' => FALSE, 'message' => 'name,email,phone,country,city,zip and address are mandatory'], 401);               
            }
            $users_id = $userkey_data[0]->user_id;
            $name = $request->post('name');
            $email = $request->post('email');
            $phone = $request->post('phone');
            $billing_country = $request->post('country');
            $billing_city = $request->post('city');
            $billing_zip = $request->post('zip');
            $billing_address = $request->post('address');


            $dupe_email = DB::table('users')->select('email')
                ->where('email', '=', $email)
                ->where('id', '!=', $users_id)->first();
            if($dupe_email) {
                return response()->json(['status' => FALSE, 'message' => 'Email already exists'], 401);
            }
            
            $data_to_update = [
                'name' => $name,
                'email' => $email,
                'phone_number' => $phone,
                'billing_city' => $billing_city,
                'billing_zip' => $billing_zip,
                'billing_country' => $billing_country,
                'billing_address' => $billing_address,
                'updated_at' => date('Y-m-d H:i:s'),
            ];

            DB::table('users')
            ->where('id', $users_id)
            ->update($data_to_update);

            $returnData = ['message' => 'successfully updated profile', 'status' => TRUE, 'data' => $data_to_update]; 
            return response()->json($returnData, 200);
        }
    }

    function generateCode($limit){
        $code = '';
        for($i = 0; $i < $limit; $i++) { 
            $code .= mt_rand(0, 9); 
        }
        return substr(str_shuffle('abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789'),0,5).$code;
    }

    function authenticateUserWithkey( $key ) {
        $userkey_data = DB::table('userkey')
            ->select('users_id')
            ->where('key', '=', $key)
            ->first();
        if (empty($userkey_data)) {

             return [ 'message' => 'Unauthenticated' ];           
        }
        $user_id = $userkey_data->users_id;
        $customer_data = DB::table('users')->select('id')->where('id', '=', $user_id)->first();
        if( ! $customer_data) {

            return [ 'message' => 'unauthenticated' ];
        }
        $user = \App\User::find($user_id);
        $roles = $user->getRoleNames();
        return [ 'message' => 'success', 'data' => $customer_data, 'user_id' => $user_id,'roles' => json_decode(json_encode($roles), true) ];
    }
    
    function show(Request $request) {
        $key = $request->post('key');
        $authenticated_user = $this->authenticateUserWithkey( $key );
        if($authenticated_user['message'] != 'success') {

            return response()->json( ['status' => FALSE, 'message' => 'Inavlid login'], 200);
        }
        $pickup_points = DB::table('boarding_pickup_map')->select(DB::raw('pickup_point as name'))->groupBy('pickup_point')->get();
        $permissions_data = $authenticated_user['roles'];
        $aColumns = array(
            0 => 'id',
            1 => 'flight_number',
            2 => 'pickup_time',
            3 => 'pickup_point',
            4 => 'status',
            5 => 'id',
            6 => 'items',
            7 => 'grant_total',
            8 => 'username'
        );
        $sWhere = '';
        if( $request->post('status_type') != '') {
            $sWhere .= $sWhere == '' ? 'WHERE ' : ' AND ';
            if( $request->post('status_type') == 'UPCOMING') {
                date_default_timezone_set('Asia/Dubai');
                // In upcoming orders show items with these statuses.
                $tomorrow = date('Y-m-d', strtotime('+1 day', strtotime(date('Y-m-d')) ) );
                $today = date('Y-m-d');
                $status_to_included = '"IN_KITCHEN", "ORDER_CONFIRMED", "READY_FOR_PICKUP"';
                
//                $sWhere .= ' `order`.status in ('. $status_to_included .') '.
//                ' and (  date(`order`.`order_at`)="' . $tomorrow . '" OR date(`order`.`order_at`)="' . $today . '") ';
                $sWhere .= ' `order`.status in ('. $status_to_included .') '.
                ' and (  date(`order`.`order_at`) >= "' . $today . '") ';
            } else {
                $sWhere .= ' `order`.status = "'.$request->post('status_type').'"';
            }
        }
        
        if( in_array('pickup', $permissions_data) ) {
            $user_id = $authenticated_user['data']->id;
            $pickup_point = DB::table('users')->select('pickup_point', 'is_default_pickup_person')->where('id', '=', $user_id)->first();
            $p_point = $pickup_point->pickup_point ?? '';
            $is_default_pickup_person = $pickup_point->is_default_pickup_person ?? '';
            $sWhere .= $sWhere == '' ? ' WHERE ' : ' AND ';
            if( $is_default_pickup_person == 'YES' ) {
                if( $request->post('status_type') == '' || $request->post('status_type') == 'UPCOMING') {
                    // In all orders tab, display all.
                    $list_all_cond = '';
                } else {
                    $list_all_cond = ' AND ( `order`.pickup_reached_at is null OR `order`.pickup_reached_at="")';
                }
                //$sWhere .= ' `order`.status in ("IN_KITCHEN", "ORDER_CONFIRMED", "READY_FOR_PICKUP", "DELIVERED") '.$list_all_cond;
                $sWhere .= ' 1=1 '.$list_all_cond;
            } else {
                if( $request->post('status_type') == '' || $request->post('status_type') == 'UPCOMING') {
                    $list_all_cond = '';
                } else {
                    $default_grabbit_location = "At ".default_grabbit_location()['location'];
                    $list_all_cond = ' AND ( `order`.pickup_reached_at ="'.$default_grabbit_location.'") ';
                }
                // orders of pickup person's location.
                //$sWhere .= ' `order`.status in ("READY_FOR_PICKUP", "DELIVERED") AND `order`.pickup_point = "'. $p_point .'" '.$list_all_cond;
                $sWhere .= ' `order`.pickup_point = "'. $p_point .'" '.$list_all_cond;
            }
        }

        // $date_from = $request->post('date_from') ?? '';
        // $date_to = $request->post('date_to') ?? '';
        $date_from = date("Y-m-d");
        $date_to = date("Y-m-d", strtotime($date_from. ' +1 days'));
        if( $date_from) {
            $sWhere .= $sWhere == '' ? 'WHERE ' : ' AND ';
            $sWhere .= ' date(`order`.`order_at`) >="' . $date_from . '" ';
        }
        if( $date_to) {
//            $sWhere .= $sWhere == '' ? 'WHERE ' : ' AND ';
//            $sWhere .= ' date(`order`.`order_at`) <="' . $date_to . '" ';
        }
        
        /* Individual column filtering */
        // for ( $i=0 ; $i<count($aColumns) ; $i++ )
        // {
        //     if ( $sWhere == "" )
        //     {
        //         $sWhere = "WHERE ";
        //     }
        //     else
        //     {
        //         $sWhere .= " AND ";
        //     }
        //     $sWhere .= $aColumns[$i]." LIKE '%".$_POST['sSearch_'.$i]."%' ";
        // }
        
        // Get data.

        if( $request->post('searchTerm') != '') {
            $sWhere .= $sWhere == '' ? 'WHERE ' : ' AND ';
            $sWhere .= " `order`.id LIKE '%".$request->post('searchTerm')."%' ";
        }

        $model = DB::select("select 
            `order`.id as serial_no,
            `order`.id,
            `order`.is_reimbursed,
            `order`.pickup_reached_at, 
            `order`.flight_number as flight_number, 
            `order`.order_at,
            `order`.pickup_time, 
            `order`.created_at,
            `order`.status as status, 
            `order`.pickup_point as pickup_point,
            `order`.gate,
            `order`.grant_total,
            users.name as username,
            users.phone_number as phone_number,
            users.id as user_id
        from `order`
        join users on users.id = `order`.users_id
        $sWhere ");
        $output = array(
            "aaData" => array()
        );
        foreach ( $model as $keys => $md)
        {
            $row = array();
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                $key = $aColumns[$i];
                if($i == 3) {
                    $displayPl = $md->gate == '-' ? '-' : $md->$key;
                    $row[] = $displayPl;
                }
                else if($i == 4) {
                    $statusBadge = $md->status;
                    if($md->status == 'READY_FOR_PICKUP' && $md->pickup_reached_at != NULL) {
                        $statusBadge = $md->pickup_reached_at;
                    }
                    $row[] = ucwords( strtolower( str_replace('_', ' ', $statusBadge) ) );
                } else if( $i == 6 ) {
                    $items = DB::table('order_items')->select('item_count', 'name', 'grant_total')
                        ->join('products', 'products.id', '=', 'order_items.products_id')
                        ->where('order_id', '=', $md->id)->get();
                    $ord_items = [];
                    foreach($items as $item) {
                        $ord_items[] = [
                            'item_count' => $item->item_count,
                            'name' => $item->name,
                            'grant_total' => $item->grant_total,
                        ];
                    }
                    $row[] =  $ord_items;
                } else {
                    if($i == 0) {
                        $row[] = "CON".str_pad($md->$key, 5, '0', STR_PAD_LEFT);
                    } else {
                        $row[] = $md->$key;
                    }
                }
            }
            $output['aaData'][] = $row;
        }
        $output['status'] = TRUE;
        $output['pickup_points'] = $pickup_points;
        
        return response()->json( $output, 200);
    }
    
    
     /**
     * Display the specified resources.
     *
     * @return \Illuminate\Http\Response
     */
    public function details(Request $request)
    {
        $key = $request->post('key');
        $authenticated_user = $this->authenticateUserWithkey( $key );
        if($authenticated_user['message'] != 'success') {

            return response()->json(['status' => FALSE, 'message' => 'Unauthenticated'], 401);
        }
        
        $pickup_points = DB::table('boarding_pickup_map')->select(DB::raw('pickup_point as name'))->groupBy('pickup_point')->get();
        
        $id = $request->post('order_id');

        $orders = DB::select("select *, o.flight_number as flight_number, o.order_at as departure, o.qr_code,
        o.freshness,o.taste,o.variety,o.ordering_process,o.comment
        from `order` o
        where o.id = :id ", ['id' => $id]);

        $user_details = DB::table('order')->select('name', 'email', 'phone_number', 'billing_name',
                            'billing_street', 'billing_street_no', 'billing_apartment_no',
                            'billing_city', 'billing_address', 'billing_country')
                        ->join('users', 'users.id', '=', 'order.users_id')
                        ->where('order.id', '=', $id)->first();
        
        $order_items = DB::select("select p.name, oi.item_count,oi.grant_total,oi.sub_total,oi.tax_total
                                    from `order` o
                                    join order_items as oi on o.id = oi.order_id
                                    join products as p on oi.products_id = p.id
                                    where o.id = :id ", ['id' => $id]);
        // $pickup_points = DB::table('boarding_pickup_map')->select(DB::raw('pickup_point as name'))->groupBy('pickup_point')->get();

        // $user_id = $userkey_data->users_id;
        //$user_pickup_point = DB::table('users')->select('pickup_point', 'is_default_pickup_person')->where('id', '=', $user_id)->first();
        //$pickup_location = $user_pickup_point->pickup_point ?? '';
        $invID = str_pad($id, 5, '0', STR_PAD_LEFT);
        $invID = "CON".$invID;
        $pickup_time = '';  
        $flight_date = $orders[0]->departure;
        $flight_slotes = DB::select("select `flight_slots`.`time_from`,`flight_slots`.`time_to`,`flight_slots`.`pickup_schedule`,`flight_slots`.`last_order`,`flight_slots`.`in_kitchen` from `flight_slots`");
        foreach ($flight_slotes as $key => $value) {
            $date = date('Y-m-d', strtotime($flight_date));
            $time_from = $date." ".$value->time_from;
            $time_to = $date." ".$value->time_to;
            $pickup_schedule = $date." ".$value->pickup_schedule;
            $flight_date = date('Y-m-d H:i:s', strtotime($flight_date));
            if ($time_from <= $flight_date && $time_to >= $flight_date) {
                $pickup_time = $pickup_schedule;
            }
        }

        return response()->json([
            'status' => TRUE, 
            'success' => 'Data Fetched Successfull',
            'orders' => $orders, 
            'order_items' => $order_items, 
            'order_id' => $id,
            'customer_details' => $user_details,
            'pickup_time' => $pickup_time,
            'pickup_points' => $pickup_points,
            //'user_pickup_point' =>$pickup_location
        ], 200);

    }
    
   
    function findGate($gateFind) {
        $gate2Find = strtolower( $gateFind );
        if (strpos($gate2Find, 'grabbit') !== false) {
            return $gateFind . ': Gate B7';
        }
        if (strpos($gate2Find, 's34') !== false) {
            return $gateFind . ': Gate C36';
        }
        if (strpos($gate2Find, 'nutella') !== false) {
            return $gateFind . ': Gate B28';
        }
        if (strpos($gate2Find, 'fix') !== false) {
            return $gateFind . ': Gate A17';
        }
        if (strpos($gate2Find, 'tree') !== false) {
            return $gateFind . ': Gate A12';
        }

        return '';
    }
    
    function findGateReminder($gateFind) {
        $gate2Find = strtolower( $gateFind );
        if (strpos($gate2Find, 'grabbit') !== false) {
            return 'B7';
        }
        if (strpos($gate2Find, 's34') !== false) {
            return 'C36';
        }
        if (strpos($gate2Find, 'nutella') !== false) {
            return 'B28';
        }
        if (strpos($gate2Find, 'fix') !== false) {
            return 'A17';
        }
        if (strpos($gate2Find, 'tree') !== false) {
            return 'A12';
        }

        return '';
    }
    
    function sendSMS($message, $phonenumber) {
        $message = urlencode($message);
        preg_match_all('!\d+!', $phonenumber, $matches);
        $phone = implode('',$matches[0]);
        $url = "http://mshastra.com/sendurlcomma.aspx?user=20093644&pwd=Emirates321!&senderid=CarryonDXB&mobileno=".$phone."&msgtext=".$message."&priority=High&CountryCode=ALL";



        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch);
        // echo $curl_scraped_page;
    }
    
    function sendMail($message, $email) {
        try {
            $data = ['from_name' => 'CARRYON', 'from_email' => 'noreply@carryon.com', 'subject' => 'Your CarryOn is ready!', 'message1' => $message, 'message2' => ''];

			Mail::to($email)->send(new TestEmail($data));
        }catch (\Exception $e) {
            print_r($e->getMessage());
        }
    }
    
    public function link_store($long_link)
    {
        $input['link'] = $long_link;
        $input['code'] = Str::random(6);
   
        ShortLink::create($input);
  
        return $input['code'];
    }
    
    /**
     * Order report pdf.
     *
     * @return \Illuminate\Http\Response
     */
    public function pdfreport(Request $request)
    {
        $key = $request->post('key');
        $authenticated_user = $this->authenticateUserWithkey( $key );
        if($authenticated_user['message'] != 'success') {

            return response()->json(['status' => FALSE, 'message' => 'Unauthenticated'], 401);
        }
        $orderid = $request->post('order_id');
        $order = DB::table('order')->where('id', '=', $orderid)->first();
        if ($order === null) {
            return response()->json(['status' => FALSE, 'message' => 'Order not found'], 401);
        }
        // ------ gate update code starts----------|
        /*$order_at_data = DB::select("select DATE_FORMAT(`order`.`order_at`,'%Y/%c/%e') as order_at  from `order` where `order`.`id` = :order_id",['order_id' => $orderid]);
        $fn_prefix = substr($order->flight_number, 0, 2);
        $fn = substr($order->flight_number, 2);    
        $handle = curl_init();
        $url = "https://api.flightstats.com/flex/flightstatus/rest/v2/json/flight/status/".$fn_prefix."/".$fn."/dep/".$order_at_data[0]->order_at."?appId=8a5a55e4&appKey=b3e7538d084234f6f83779bdfbd671ed&utc=false&airport=DXB";
        curl_setopt($handle, CURLOPT_URL, $url);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_HTTPHEADER, array('DY-X-Authorization: 3b75a0bdfc7aa9d383e3f8beef0f2b623a94335c'));
        $output = curl_exec($handle);
        if ( ! curl_errno($handle)) {
            curl_close($handle);
            $output = json_decode($output);
            if (isset($output->flightStatuses[0]->airportResources->departureGate)) {
                $gate = $output->flightStatuses[0]->airportResources->departureGate;
                if($gate) {
                    $p_point = DB::table('boarding_pickup_map')->select('pickup_point')->where('gate', '=', $gate)->first();
                    $response = DB::table('order')->where('id', '=', $orderid)->update([
                        'gate' => $gate,
                        'updated_at' => date("Y-m-d H:i:s"),
                        'gate_updated_at' => date("Y-m-d H:i:s"),
                        'pickup_point' => $p_point->pickup_point
                    ]);
                    $orderlog = new Orderlog;
                    $orderlog->status = 'Gate Updated';
                    $orderlog->order_id = $orderid;
                    $orderlog->created_at = date("Y-m-d H:i:s");
                    $orderlog->updated_at = date("Y-m-d H:i:s");
                    $orderlog->save();
                }
            }
        }*/
        $result = DB::select('select `order`.`id` as orderid, '
                   .'`order_items`.`id` as orderitemid, '
                   . '`order`.`item_count` as item_count_total,'
                   . '`order`.`created_at` as created_at,'
                   . '`order`.`flight_number` as flight_no,'
                   . '`order`.pickup_point as boarding_point,'
                   . '`order`.`status` as order_status,'
                   . ' `order`.`grant_total` as grant_total,'
                   . ' `order`.`sub_total` as sub_total,'
                   . ' `order`.`tax_total` as tax_total,'
                   . ' `order`.`order_at` as order_at,'
                   . ' `order`.`users_id` as users_id,'
                   . ' `order`.`pickup_time` as pickup_time,'
                   . ' `order`.`qr_code` as qr_code,'
                   . ' order_items.item_count as item_count,'
                   . ' order_items.products_id as products_id,'
                   . ' order_items.grant_total as grant_item_price,'
                   . ' order_items.sub_total as sub_item_price,'
                   . ' order_items.tax_total as tax_item_price,'
                   . ' products.name as productname,'
                   . ' products.description as productdescription,'
                   . ' product_images.path as productthumbnail'
                   . ' from `order` join order_items on `order`.`id` = order_items.order_id'
                   . ' join products on `products`.`id` = order_items.products_id '
                   . ' left join product_images on `products`.`id` = product_images.products_id and product_images.type = "THUMBNAIL"'
                   . 'where `order`.`id` = :orderid', ['orderid' => $orderid ]);
            $modifiedResult = Array();
            if (!empty($result)) {
                $i = 0;
                foreach ($result as $key => $value) {
                    if ($i == 0) {
                        $modifiedResult = Array(
                            'qr_code' => $value->qr_code,
                            'order_id' => $value->orderid,
                            'users_id' => $value->users_id,
                            'created_at' => $value->created_at,
                            'order_at' => $value->order_at,
                            'flight_no' => $value->flight_no,
                            'boarding_point' => $value->boarding_point,
                            'order_status' => str_replace("_", " ", $value->order_status),
                            'item_count_total' => $value->item_count_total,
                            'grant_total' => decimal_format_custom($value->grant_total),
                            'sub_total' => decimal_format_custom($value->sub_total),
                            'tax_total' => decimal_format_custom($value->tax_total),
                            'pickup_time' => $value->pickup_time    
                        );
                    }
                    $modifiedResult['items'][$value->orderitemid] = Array(
                        'item_count' => $value->item_count,
                        'unit_item_price' => $value->grant_item_price / $value->item_count,
                        'grant_item_price' => decimal_format_custom($value->grant_item_price),
                        'sub_item_price' => decimal_format_custom($value->sub_item_price),
                        'tax_item_price' => decimal_format_custom($value->tax_item_price),
                        'products_id' => $value->products_id,
                        'productname' => $value->productname,
                        'productthumbnail' => empty($value->productthumbnail)?$value->productthumbnail:url($value->productthumbnail),
                        'productdescription' => $value->productdescription
                    );
                    $i++;
                } 
            }
        $content = '';    
        if (!empty($modifiedResult)) {
            foreach ($modifiedResult['items'] as $key => $value) {
                $content .= '<tr>
                                <td class="description">'.$value["productname"].'</td>
                                <td class="quantity">'.$value["item_count"].'</td>
                                <td class="price" style="text-align:right;">'.$value["grant_item_price"].'</td>
                            </tr>';
            }
        }   
        $user_id = $modifiedResult['users_id'];
            
        $user_data = DB::table('users')->select('phone_number','email','name','billing_address')->where('id', '=', $user_id)->first();
        $invID = str_pad($orderid, 5, '0', STR_PAD_LEFT);
        $invID = "CON".$invID; 
        $order_at = $modifiedResult['order_at']??''; 
        if (!empty($order_at)) {
            $date = date_create($order_at);
            $order_at = date_format($date,"d-M-Y");
        }
        
        $custome_date = date_format_from_month_custom($modifiedResult['pickup_time']);
        $custome_date_array = explode(" ", $custome_date);
        if (!empty($md->$key)) {
            $dispatch_time = $custome_date_array[0]." ".$custome_date_array[1]." <b>".$custome_date_array[2]."</b>";
        } else {
            $dispatch_time = $custome_date;
        }
        
        $data = ['title' => 'Welcome to carryon','invID' => $invID,'customername' => $user_data->name,
            'qr_code' => $modifiedResult['qr_code'],
            'content' => $content, 'total' => $modifiedResult['grant_total'],
            'order_at' => $order_at, 'billing_address' => $user_data->billing_address,
            'pickup_loc' => $modifiedResult['boarding_point'], 'flight_no' => $modifiedResult['flight_no'], 'dispatch_time' => $dispatch_time];
        $customPaper = array(0,0,567.00,283.80);
        $pdf = PDF::loadView('pdf.invoice', $data)->setPaper($customPaper, 'landscape');
  
        // return $pdf->download('carryon' . time() . '.pdf');
        $folder_structure = date('Y')."/".date('m')."/".date('d');
        $target_dir = "./public/upload/".$folder_structure;
        $target_dir_path = "./upload/".$folder_structure;
        if (!file_exists($target_dir)) {
            mkdir($target_dir, 0777, true);
        }
        $file = $target_dir.'/invoice_'.$orderid.'.pdf';
        $filepath = $target_dir_path.'/invoice_'.$orderid.'.pdf';
        $pdf->save($file);
        //$returnData = Array("message" => "Order List","filepath" => url($filepath));
        //echo json_encode($returnData);
        //exit;
        return response()->json([
            'status' => TRUE, 
            'success' => 'Invoice created',
            'filepath' => url($filepath)
        ], 200);
           
    }
    
    /**
     * Password change api.
     *
     */
    public function resetpassword(Request $request){
        $key = $request->post('key');
        $authenticated_user = $this->authenticateUserWithkey( $key );
        if($authenticated_user['message'] != 'success') {

            return response()->json(['status' => FALSE, 'message' => 'Unauthenticated'], 401);
        }
        
        $userkey_data = DB::table('userkey')
            ->select('users_id')
            ->where('key', '=', $key)
            ->first();
        
        $user_id = $userkey_data->users_id;
        $users_data = DB::table('users')->select('email')->where('id', '=', $user_id)->first();
        
        $uname = $users_data->email;
        if (!$request->post('current_password') || !$request->post('new_password')) {
             return response()->json(['status' => FALSE, 'message' => 'current_password and new_password are mandatory'], 401);               
        }
        
        $current_password = $request->post('current_password');
        $new_password = $request->post('new_password');
        if (Auth::attempt(array('email' => $uname, 'password' => $current_password))){
            $password_hashed = Hash::make($new_password);
            DB::table('users')
            ->where('id', $user_id)
            ->update([
                'password' => $password_hashed,
                'remember_token' => $new_password,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
            return response()->json([
                'status' => TRUE, 
                'success' => 'Successfully updated password'
            ], 200);
        }
        else {        
            return response()->json(['status' => FALSE, 'message' => 'Invalid credentials'], 401);
        }
    }
    
    public function change_pl_status(Request $request) {
        $key = $request->post('key');
        $authenticated_user = $this->authenticateUserWithkey( $key );
        if($authenticated_user['message'] != 'success') {
            return response()->json(['status' => FALSE, 'message' => 'Unauthenticated'], 401);
        }
        
        if (!$request->post('order_id') || !$request->post('pickup')) {
             return response()->json(['status' => FALSE, 'message' => 'order_id and pickup are mandatory'], 401);               
        }
        
        $order_id = $request->post('order_id');
        $pickup = $request->post('pickup');
        $pickup_at = 'At '.$pickup;
        
        $order_details = DB::table('order')->select('order_at','users_id','status','pickup_reached_at','pickup_point')->where('id', '=', $order_id)->first();
        if(! $order_details) {
            return response()->json(['status' => FALSE, 'message' => 'Order not found'], 401);
        }
        
        if ($pickup == trim($order_details->pickup_point)) {
            return response()->json(['status' => FALSE, 'message' => 'Trying to update to same pickup point!'], 401);
        } 
        
        $invID = str_pad($order_id, 5, '0', STR_PAD_LEFT);
        $invID = "CON".$invID;
        
        $default_grabbit_location = default_grabbit_location();
        // Notification for selected new pickup location users except the default location users to inform about pickup location change
        if ($default_grabbit_location['location'] != $pickup) {
            $notification_data = DB::select("select id as user_id, fcm_token  from `users` where `users`.`pickup_point` = :pickup_point",['pickup_point' => $pickup]);
            if (!empty($notification_data)) {
                foreach ($notification_data as $key => $value) {
                    $title = 'Notification';
                    $message = 'Pickup point of Order Id '.$invID.' is changed to '.$pickup;
                    $this->notification_add($value->user_id, $message, $title, $value->fcm_token, $order_id);
                }
            }
        }
        
        // Notification for default location usersto inform about pickup location change
        $notification_data = DB::select("select id as user_id, fcm_token  from `users` where `users`.`pickup_point` = :pickup_point",['pickup_point' => $default_grabbit_location['location']]);
        if (!empty($notification_data)) {
            foreach ($notification_data as $key => $value) {
                $title = 'Notification';
                $message = 'Pickup point of Order Id '.$invID.' is changed to '.$pickup;
                $this->notification_add($value->user_id, $message, $title, $value->fcm_token, $order_id);
            }
        }
        
        if ($order_details->status == 'READY_FOR_PICKUP' && trim($order_details->pickup_reached_at) == '') {
            // $notification_data = DB::select("select id as user_id, fcm_token  from `users` where `users`.`pickup_point` = :pickup_point",['pickup_point' => $default_grabbit_location['location']]);
            if (!empty($notification_data)) {
                foreach ($notification_data as $key => $value) {
                    $title = 'Notification';
                    $message = 'Order Id '.$invID.' ready for pickup';
                    $this->notification_add($value->user_id, $message, $title, $value->fcm_token, $order_id);
                }
            }
        }
        
        if ($order_details->status == 'READY_FOR_PICKUP' && trim($order_details->pickup_reached_at) != '' && trim($order_details->pickup_reached_at) == $pickup_at ) {
            $notification_data = DB::select("select id as user_id, fcm_token  from `users` where `users`.`pickup_point` = :pickup_point",['pickup_point' => $pickup]);
            if (!empty($notification_data)) {
                foreach ($notification_data as $key => $value) {
                    $title = 'Notification';
                    $message = 'Order Id '.$invID.' ready for pickup';
                    $this->notification_add($value->user_id, $message, $title, $value->fcm_token, $order_id);
                }
            }
        }
        

        DB::table('order')->where('id', '=', $order_id)->update([
            'pickup_point' => $pickup,
            'gate' => '.' // '.' added to skip update by cron
        ]);

        return response()->json([
            'status' => TRUE, 
            'success' => 'Successfully updated pickup location'
        ], 200);
    }
    
    function notification_add ($user_id, $message, $title, $fcm_token, $order_id) {
        $notifications = new Notifications;
        $notifications->users_id = $user_id;
        $notifications->order_id = $order_id;
        $notifications->message = $message;
        $notifications->created_at = date("Y-m-d H:i:s");
        $notifications->updated_at = date("Y-m-d H:i:s");
        $notifications->save(); 
        // Message payload
        $msg_payload = array (
                'mtitle' => $title,
                'mdesc' => $message,
        );

        // For Android
        $reg_id = $fcm_token;

        $result = $this->android($msg_payload, $reg_id);
    }
    
    function change_status(Request $request) {
        $key = $request->post('key');
        $authenticated_user = $this->authenticateUserWithkey( $key );
        if($authenticated_user['message'] != 'success') {

            return response()->json(['status' => FALSE, 'message' => 'Unauthenticated'], 401);
        }
        
        $user_id = $authenticated_user['user_id'];
        
        $permissions_data = $authenticated_user['roles'];
        if( !in_array('pickup', $permissions_data) ) {
            return response()->json(['status' => FALSE, 'message' => 'Unauthenticated'], 401);
        }
        
        date_default_timezone_set('Asia/Dubai');
        $order_id = $request->post('order_id');
        $invID = str_pad($order_id, 5, '0', STR_PAD_LEFT);
        $invID = "CON".$invID;
        $status = $request->post('order_status');
        $allowed_statuses = Array('DESTINATION','DELIVERED');
        if( !in_array($status, $allowed_statuses)) {
             return response()->json(['status' => FALSE, 'message' => 'Status should be either DESTINATION or DELIVERED'], 401);           
        }
        $order = DB::table('order')->where('id', '=', $order_id)->first();
        if ($order === null) {
            return response()->json(['status' => FALSE, 'message' => 'Order not found'], 401);
        }
        $currentGate = $order->gate;
        $newGate = '';
        $gateToShow = $order->gate;
        $gateFind = $order->pickup_point;
        // ------ gate update logic ends here ------|
        $default_grabbit_location = default_grabbit_location();
        $default_location = 'At '.$default_grabbit_location['location'];
        $order_data = DB::table('order')->select('users_id','flight_number', 'pickup_point','pickup_reached_at','status')->where('id', '=', $order_id)->first();
        if ($order_data->status == 'DELIVERED') {
            return response()->json(['status' => FALSE, 'message' => 'Already delivered'], 401);             
        }
        $user_data = DB::table('users')->select('phone_number','email','name')->where('id', '=', $order_data->users_id)->first();
        $logged_user_data = DB::table('users')->select('id','phone_number','email','name','is_default_pickup_person', 'pickup_point')->where('id', '=', $user_id)->first();
        $order_pickup_point = 'At '.$order_data->pickup_point;
        $text = '';
        $timestamp = date('Y-m-d H:i:s');
        $orderlog = new Orderlog;
        if( $status != 'DESTINATION') {
            if ($status == 'DELIVERED') {
                $response = DB::table('order')->where('id', '=', $order_id)->update([
                    'status' => $status,
                    'pickup_reached_at' => '',
                    'delivered_at' => $timestamp,
                    'updated_at' => $timestamp,
                ]);                
            } else {
                $response = DB::table('order')->where('id', '=', $order_id)->update([
                    'status' => $status,
                    'pickup_reached_at' => '',
                    'updated_at' => $timestamp,
                ]);
            }
            $orderlog->status = modifyText($status);
        } else {
            if ($order_data->status == 'READY_FOR_PICKUP') {

                $text = "At " . $request->post('text');

                if ($logged_user_data->is_default_pickup_person == 'YES' && $text != $default_location && $text != $order_pickup_point) {
                    return response()->json(['status' => FALSE, 'message' => 'You can change status to '.$default_grabbit_location['location'].' or '.$order_data->pickup_point], 401);             
                }



                if ($logged_user_data->is_default_pickup_person == 'NO' && ($text != $order_pickup_point || $order_data->pickup_point != $logged_user_data->pickup_point || empty($order_data->pickup_reached_at) )) {
                    return response()->json(['status' => FALSE, 'message' => 'This order does not belongs to you'], 401);             
                }

                if ($text == $order_data->pickup_reached_at) {
                    return response()->json(['status' => FALSE, 'message' => 'Already changed'], 401);             
                }

                if ($logged_user_data->is_default_pickup_person == 'YES' && $order_pickup_point == $order_data->pickup_reached_at) {
                    return response()->json(['status' => FALSE, 'message' => 'Already changed to the order pickup point, restricted to change to default location(grabbit) again'], 401);             
                }
            } else {
                return response()->json(['status' => FALSE, 'message' => 'Order should be in ready for pickup or '.$default_location.' status to change to this status'], 401);   
            }
            
            $response = DB::table('order')->where('id', '=', $order_id)->update([
                'status' => 'READY_FOR_PICKUP',
                'pickup_reached_at' => $text,
                'updated_at' => date("Y-m-d H:i:s"),
            ]);
            
            
            if ($logged_user_data->is_default_pickup_person == 'NO' || ($logged_user_data->is_default_pickup_person == 'YES' && $text == $order_pickup_point) ) {
                $notification_data = DB::select("select id as user_id, fcm_token  from `users` where `users`.`id` = :users_id",['users_id' => $order_data->users_id]);
                $title = 'Notification';
                $message = 'Your order '.$invID.' is ready!';
                $this->notification_add($order_data->users_id, $message, $title, $notification_data[0]->fcm_token, $order_id);
            } else if ($logged_user_data->is_default_pickup_person == 'YES') {
                $notification_data = DB::select("select id as user_id, fcm_token  from `users` where `users`.`pickup_point` = :pickup_point",['pickup_point' => $order_data->pickup_point]);
                // $notification_data = DB::select("select id as user_id  from `users` where `users`.`pickup_point` = :pickup_point",['pickup_point' => $default_grabbit_location['location']]);
                if (!empty($notification_data)) {
                    foreach ($notification_data as $key => $value) {
                        $title = 'Notification';
                        $message = 'Order Id '.$invID.' ready for pickup at '.$default_grabbit_location['location'];
                        $this->notification_add($value->user_id, $message, $title, $value->fcm_token, $order_id);
                    }
                }
            }

            $orderlog->status = $text;
        }
        $orderlog->order_id = $order_id;
        $orderlog->created_at = $timestamp;
        $orderlog->updated_at = $timestamp;
        $orderlog->save();
        
            $pickup_point = $order_data->pickup_point;

            $pickup_point_text = "At ". $order_data->pickup_point;
            
            if( $status == 'DESTINATION' &&  $text != '' && $text == $pickup_point_text) {
                $order_items = DB::select("select p.name, o.gate as gate, oi.item_count,oi.grant_total,oi.sub_total,oi.tax_total
                            from `order` o
                            join order_items as oi on o.id = oi.order_id
                            join products as p on oi.products_id = p.id
                            where o.id = :id ", ['id' => $order_id]);
                $items = '';
                $sub_total = '0';
                $grant_total = '0';
                $maplink = '';
                if (!empty($order_items)) {
                    foreach ($order_items as $key => $value) {
                        $unit_price = round($value->grant_total / $value->item_count, 2); 
                        $unit_price_modified = decimal_format_custom($unit_price);
                        $sub_total += $value->sub_total;
                        $grant_total += $value->grant_total;

                        if( $value->gate == '-' || $value->gate == '' ) {
                            // Destination reached but no gate. Then update gate to lift.
                            $value->gate = 'Lift';
                            $maplink = url('/maps/concourseb.html?from='.$value->gate.'&to='.$pickup_point);
                        } else {
                            $first_char = strtolower( substr($value->gate, 0, 1) );
                            $file = ($first_char == 'b' || $first_char == 'c') ? "concourse$first_char" : 'index';
                            $maplink = url("/maps/$file.html?from=".$value->gate.'&to='.$pickup_point);
                        }
                    }
                    
                }
                $maplink_short = url('/m/'.$this->link_store($maplink));
                $tax_total = $grant_total - $sub_total;
                $sub_total_modified = decimal_format_custom($sub_total);
                $grant_total_modified = decimal_format_custom($grant_total);
                $tax_total_modified = decimal_format_custom($tax_total);
                
                $template = DB::select("select template from sms_template where `type` = 'READY_FOR_PICKUP'");
                $message = '';
                $findedGate = $this->findGate($gateFind);
                $gateToShow = $this->findGateReminder($gateFind);
                if( ! empty($template)) {
                    $sms_template = $template[0]->template;
                    $message = str_replace("{{ orderId }}", $invID, $sms_template);
                    $message = str_replace("{{ pickupPoint }}", $pickup_point, $message);
                    $message = str_replace("{{ maplinkShort }}", $maplink_short, $message);
                    
                    $message = str_replace("{{ nearest_gate }}", $gateToShow, $message);
                    $message = str_replace("{{ short_link_text }}", $findedGate, $message);
                }
                // $message = 'Your order '.$invID.' is ready for pick up at '.$pickup_point.'! Find your way: '.$maplink_short;
                $this->sendSMS($message, $user_data->phone_number);

          $orderid = $order_id;
                $result = DB::select('select `order`.`id` as orderid, '
                    . '`order_items`.`id` as orderitemid, '
                    . '`order`.`item_count` as item_count_total,'
                    . '`order`.`created_at` as created_at,'
                    . '`order`.`order_at` as order_at,'
                    . '`order`.flight_number as flight_no,'
                    . '`order`.gate as gate,'
                    . '`order`.pickup_reached_at as pickup_reached_at,'
                    . '`order`.pickup_point as boarding_point,'
                    . '`order`.`status` as order_status,'
                    . ' `order`.`grant_total` as grant_total,'
                    . ' `order`.`sub_total` as sub_total,'
                    . ' `order`.`tax_total` as tax_total,'
                    . ' `order`.`qr_code` as qr_code,'
                    . ' order_items.item_count as item_count,'
                    . ' order_items.products_id as products_id,'
                    . ' order_items.grant_total as grant_item_price,'
                    . ' order_items.sub_total as sub_item_price,'
                    . ' order_items.tax_total as tax_item_price,'
                    . ' products.name as productname,'
                    . ' products.actual_price as actual_price,'
                    . ' products.description as productdescription,'
                    . ' product_images.path as productthumbnail,'
                    . ' ado.name as addon_name, ado.actual_price as addon_price, aoi.item_count as addon_item_count'
                    . ' from `order` join order_items on `order`.`id` = order_items.order_id'
                    . ' join products on `products`.`id` = order_items.products_id '
                    . ' left join product_images on `products`.`id` = product_images.products_id and product_images.type = "THUMBNAIL"'
                    . ' left join addons_order_items as aoi on order_items.id = aoi.order_items_id'
                    .  ' left join addons as ado on aoi.addons_id = ado.id'
                    . ' where `order`.`id` = :orderid '
                    . '  order by `order`.`id` desc', ['orderid' => $orderid]);

                $modifiedResult = array();
                $items = '';
                $sub_total = '0.00';
                $grant_total = '0.00';
                if (!empty($result)) {
                    $i = 0;
                    foreach ($result as $key => $value) {
                        if ($i == 0) {
                            $modifiedResult = array(
                                'order_id' => $value->orderid,
                                'created_at' => $value->created_at,
                                'order_at' => $value->order_at,
                                'flight_no' => $value->flight_no,
                                'qr_code' => $value->qr_code,
                                'boarding_point' => $value->boarding_point,
                                'order_status' => str_replace("_", " ", $value->order_status),
                                'item_count_total' => $value->item_count_total,
                                'grant_total' => decimal_format_custom($value->grant_total),
                                'sub_total' => decimal_format_custom($value->sub_total),
                                'tax_total' => decimal_format_custom($value->tax_total)
                            );
                            $grant_total = $value->grant_total;
                            $sub_total = $value->sub_total;
                        }

                        if (!isset($modifiedResult['items'][$value->orderitemid])) {
                            $modifiedResult['items'][$value->orderitemid] = array(
                                'item_count' => $value->item_count,
                                'grant_item_price' => decimal_format_custom($value->grant_item_price),
                                'actual_price' => decimal_format_custom($value->actual_price),
                                'sub_item_price' => decimal_format_custom(($value->sub_item_price * $value->item_count)),
                                'tax_item_price' => decimal_format_custom($value->tax_item_price),
                                'products_id' => $value->products_id,
                                'productname' => $value->productname,
                                'productthumbnail' => empty($value->productthumbnail) ? $value->productthumbnail : url($value->productthumbnail),
                                'productdescription' => $value->productdescription,
                            );
                            if (!empty($value->addon_name)) {
                                $modifiedResult['items'][$value->orderitemid]['addons'][] = array(
                                    'addon_name' => $value->addon_name,
                                    'addon_price' => $value->addon_price,
                                    'addon_item_count' => $value->addon_item_count
                                );
                            } else {
                                $modifiedResult['items'][$value->orderitemid]['addons'] = array();
                            }
                        } else {
                            if (!empty($value->addon_name)) {
                                $modifiedResult['items'][$value->orderitemid]['addons'][] = array(
                                    'addon_name' => $value->addon_name,
                                    'addon_price' => $value->addon_price,
                                    'addon_item_count' => $value->addon_item_count
                                );
                            } else {
                                $modifiedResult['items'][$value->orderitemid]['addons'] = array();
                            }
                        }
                        $i++;
                    }
                }

                $addon_total = "0.00";
                $product_total = "0.00";

                if (!empty($modifiedResult)) {
                    foreach ($modifiedResult['items'] as $key => $value) {
                        $grant_item_price = $value["actual_price"] * $value["item_count"];
                        $product_total += $grant_item_price; 
                        $items .= '<tr>
                        <td style="background-color: #ffffff; color: #3f2d26; text-align: left;">
                        <h4>' . $value["productname"] . '</h4>
                        </td>
                        <td style="background-color: #ffffff; color: #3f2d26; text-align: left;">
                        AED ' . $value["actual_price"] . '
                        </td>
                        <td style="background-color: #ffffff; color: #3f2d26; text-align: left;">
                        ' . $value["item_count"] . '
                        </td>
                        <td style="background-color: #ffffff; color: #3f2d26; text-align: left;">
                        AED ' . $grant_item_price . '
                        </td>
                </tr>';
                        if (count($value['addons']) > 0) {
                            $items .= '<tr>
                            <td style="background-color: #ffffff; color: #3f2d26; text-align: left;"><h5>Addons</h5></td>
                        </tr>';
                            foreach ($value['addons'] as $addons) {
                                if (!empty($addons['addon_item_count'])) {
                                    $addonName = !empty($addons['addon_name']) ? $addons['addon_name'] : 'Nil';
                                    $addonPrice = !empty($addons['addon_price']) ? 'AED ' . $addons['addon_name'] : '0';
                                    $addonItemCount = !empty($addons['addon_item_count']) ?  $addons['addon_item_count'] : '0';
                                    $addon_total += ($addons['addon_item_count'] * $addons['addon_price']);
                                    $addonTotal = 'AED ' . $addons['addon_item_count'] * $addons['addon_price'];
                                    $items .= "<tr>
                                    <td style='background-color: #ffffff; color: #3f2d26; text-align: left;'>
                                        " . $addonName . "
                                    </td>
                                    <td style='background-color: #ffffff; color: #3f2d26; text-align: left;'>
                                       " . $addonPrice . "
                                    </td>
                                    <td style='background-color: #ffffff; color: #3f2d26; text-align: left;'>
                                        " . $addonItemCount . "
                                    </td>
                                    <td style='background-color: #ffffff; color: #3f2d26; text-align: left;'>
                                       " . $addonTotal . "
                                    </td>
                            </tr>";
                                }
                            }
                        }
                    }
                }
                $grant_total = $product_total + $addon_total; 
                $item_content = '<table width="100%" cellspacing="0" cellpadding="10" border="0" style=" font-size:13px;">
									<thead>
										<tr>
										<th style="background-color: #f16521; color: #fff; text-align: left;">
										<font face="arial">PRODUCT NAME</font>
										</th>
										<th style="background-color: #f16521; color: #fff; text-align: left;">
										<font face="arial">UNIT PRICE</font>
										</th>
										<th style="background-color: #f16521; color: #fff; text-align: left;">
										<font face="arial">QUANTITY</font>
										</th>
										<th style="background-color: #f16521; color: #fff; text-align: left;">
										<font face="arial">AMOUNT</font>
                                        </th>
									</tr>
									</thead>
									<tbody>
                                                                         ' . $items . '   
									</tbody>
									<tfoot>
										<tr>
											<td></td>
											<td colspan="2" style="font-size:14px;">
											<font face="arial" style="color: #000000;">Total (Including VAT): </font>
											</td>
											<td style="font-size:15px;color: #000000;">
											<font face="arial"><small style="font-size:11px;">AED</small> <strong>' . $grant_total . '</strong></font>
										</td>
									</tr>
									</tfoot>
                                </table>';
                try {
                    $subject_data = DB::select("select template from sms_template where `type` = :type", ['type' => 'EMAIL_SUBJECT_READYFORPICKUP']);
                    $subject = $subject_data[0]->template;
//                    $data = ['subject' => $subject, 'cust_name' => $user_data->name,
//                    'nearest_gate' => $gateToShow, 'short_link_text' => $findedGate,
//                    'order_id' => $invID, 'pickup_point' => $pickup_point, 'item_content' => $item_content, 'maplink' => $maplink];
                    $readymail = new Readymail();
                    $readymail->email = $user_data->email;
                    $readymail->subject = $subject;
                    $readymail->cust_name = $user_data->name;
                    $readymail->nearest_gate = $gateToShow;
                    $readymail->short_link_text = $findedGate;
                    $readymail->order_number = $invID;
                    $readymail->pickup_point = $pickup_point;
                    $readymail->item_content = $item_content;
                    $readymail->maplink = $maplink;
                    $readymail->save();
                           // Mail::to($user_data->email)->send(new OrderReadyForPickup($data));
                } catch (\Exception $e) {
                    // print_r($e->getMessage());
                    return response()->json(['status' => FALSE, 'message' => $e->getMessage()], 401);
                }
            
            }
            
        //echo json_encode(['message'=> 'Status updated', 'gate_updated' => $gateUpdated, 'status' => TRUE]);

        // return;
        return response()->json([
            'status' => TRUE, 
            'success' => 'Status updated'
        ], 200);
    }
    
    /**
     * Get notification list.
     *
     */
    public function notification_list(Request $request){
        $key = $request->post('key');
        $authenticated_user = $this->authenticateUserWithkey( $key );
        if($authenticated_user['message'] != 'success') {

            return response()->json(['status' => FALSE, 'message' => 'Unauthenticated'], 401);
        }
        
        $userkey_data = DB::table('userkey')
            ->select('users_id')
            ->where('key', '=', $key)
            ->first();
        
        $user_id = $userkey_data->users_id;
        date_default_timezone_set('Asia/Dubai');
        $date = date("Y-m-d H:i:s");
        $onedayafter = date("Y-m-d H:i:s", strtotime($date. ' -48 hours'));
        $notification_data = DB::select("select * from `notifications` where `notifications`.`users_id` = :users_id and `notifications`.`created_at` >= :onedayafter",['users_id' => $user_id,'onedayafter' => $onedayafter]);
        return response()->json([
            'status' => TRUE, 
            'data' => $notification_data,
            'success' => 'Notifications list'
        ], 200);

    }
}