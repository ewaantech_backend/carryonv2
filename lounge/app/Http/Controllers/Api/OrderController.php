<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Userkey;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Mail\CancelEmail;
use Illuminate\Support\Facades\Mail;
use App\Notifications;


class OrderController extends Controller
{

    private static $API_ACCESS_KEY = 'AAAA0Ihrr6E:APA91bHDWmqOd5813O2xPryWcxkqrUxl9_j6KjwuRDgIg_Elmtl6zKhc4k3gq81vFJDEc3wlV0ClYJc8dFYiKXsmVgPvCESxUPXUD_nsTysb5T7GAS5c710Xtx3eVqhk6Aky6cEg96aE'; //'AIzaSyCCxJPno4WLLkGO6LM92weZ9ipY_jyGA5s';

    /**
     * To fetch the selected order details of the cart
     * Request - Order ID
     * Request - User Token
     * Response - Order details JSON object
     */
    public function getOrders(Request $request)
    {
        $orderID = $request->input('order_id');
        $userToken = $request->input('user_token');

        // start - checking the client has passed all the required parameters
        $validator = Validator::make($request->all(), [
            'order_id' => 'required|exists:order,id',
            'user_token' => 'required|exists:userkey,key',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'failed', 'message' => $validator->errors()], 401);
        }
        // end - checking the client has passed all the required parameters

        // start - fetching user id from user token
        $userIDObj = Userkey::firstWhere('key', $userToken);
        $userID = $userIDObj->users_id;
        // end- fetching user id from user token

        // start - getting order details
        try {

            $orderQuery = " SELECT prod.id as product_id, prod.name AS product_name, prod.description AS product_description,
                            oit.item_count AS order_item_count, oit.grant_total AS order_grant_total, oit.id AS order_item_id,
                            ord.*
                            FROM `order` AS ord LEFT JOIN `order_items` AS oit ON ord.id = oit.order_id
                            JOIN  `products` AS prod ON oit.products_id = prod.id
                            WHERE ord.id = :id AND ord.users_id = :user_id 
                            ";
            $orderAddonQuery = " SELECT ado.addons_id, ado.item_count,
                                ado.total_amount, ad.name, ado.order_items_id
                                FROM `addons_order_items` AS ado JOIN `addons` AS ad
                                ON ado.addons_id = ad.id and ado.item_count != 0 
                                JOIN `order_items` AS oi ON ado.order_items_id = oi.id
                                JOIN `order` AS ord ON ord.id = oi.order_id
                                WHERE ord.id = :id AND ord.users_id = :user_id
                                order by ord.id desc";

            $orderResults = DB::select($orderQuery, [':id' => $orderID, ':user_id' => $userID]);
            $orderAddonResults = DB::select($orderAddonQuery, [':id' => $orderID, ':user_id' => $userID]);


            $grant_total = 0;
            foreach ($orderResults as $key => $orderResult) {
                $order_total = 0;
                $orderAddons = array();
                $order_total += (float) $orderResult->order_grant_total;
                if (!empty($orderAddonResults)) {
                    foreach ($orderAddonResults as $orderAddonResult) {
                        if ($orderResult->order_item_id == $orderAddonResult->order_items_id) {
                            $orderAddons[] = $orderAddonResult;
                            $order_total += (float) $orderAddonResult->total_amount;
                        }
                    }
                }
                $orderResults[$key]->addons = $orderAddons;
                $orderResults[$key]->total = $order_total;
                $grant_total += $order_total;

                $orderResults[$key]->thumbnail = '';
                $prd_id = $orderResult->product_id;
                $image = DB::table('product_images')->select('path')
                    ->where('products_id', '=', $prd_id)->where('type', '=', 'THUMBNAIL')->first();
                if ($image) {
                    $orderResults[$key]->thumbnail = url($image->path);
                }
            }
            
            $orders = DB::select("select 'Order Confirmed' as status, o.created_at as created_at
            from `order` o
            where o.id = :id ", ['id' => $orderID]);

            // start - fetching all the order log
            $orderLogQuery = "SELECT DISTINCT status, created_at FROM `orderlog`
                                WHERE order_id = :order_id GROUP BY status, created_at 
                                ORDER BY created_at DESC";
            
            $orderLogQueryResults = DB::select($orderLogQuery, [':order_id' => $orderID]);
            // end -fetching all the order log
            
            $order_logs = array_merge($orders,$orderLogQueryResults);


            return response()->json(
                [
                    'status' => 'success',
                    'orderDetails' => $orderResults,
                    'orderlogs' => $order_logs,
                    'total_order_amount' => $grant_total,
                    200
                ]
            );
        } catch (Exception $e) {
            print $e->getMessage();
            return response()->json(['status' => 'failed', 'message' => 'An error occured. Please try again', 500]);
        }
        // end - getting order details

    }

    /**
     * To fetch all the orders of the user
     * Request - User Token
     * Response - Orders JSON object
     */
    public function getAllOrders(Request $request)
    {
        $userToken = $request->input('user_token');

        // start - checking the client has passed all the required parameters
        $validator = Validator::make($request->all(), [
            'user_token' => 'required|exists:userkey,key',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'failed', 'message' => $validator->errors()], 401);
        }
        // end - checking the client has passed all the required parameters

        // start - fetching user id from user token
        $userIDObj = Userkey::firstWhere('key', $userToken);
        $userID = $userIDObj->users_id;
        // end- fetching user id from user token

        // start - getting all the orders of the user
        try {
            $orderQuery = " SELECT `id` as order_id, `flight_number`, `status`, `pickup_point`, `pickup_reached_at`,
                            DATE_FORMAT(`order_at`, '%d %M %Y') AS order_schedule  
                            FROM `order` 
                            WHERE `users_id` = :user_id
                            ORDER BY updated_at DESC, id DESC
                            ";
            $orderResults = DB::select($orderQuery, [':user_id' => $userID]);
            return response()->json(
                [
                    'status' => 'success',
                    'orders' => $orderResults,
                    200
                ]
            );
        } catch (Exception $e) {
            print $e->getMessage();
            return response()->json(['status' => 'failed', 'message' => 'An error occured. Please try again', 500]);
        }
        // end - getting all the orders of the user
    }

    /**
     * Method - Cancel order
     * Request - order id and user token
     * Response - order object
     */
    public function cancelOrder(Request $request)
    {

        // start - checking the client has passed all the required parameters
        $validator = Validator::make($request->all(), [
            'order_id' => 'required|exists:order,id',
            'user_token' => 'required|exists:userkey,key',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'failed', 'message' => $validator->errors()], 401);
        }
        // end - checking the client has passed all the required parameters

        $orderID = $request->input('order_id');
        $userToken = $request->input('user_token');

        // start - fetching user id from user token
        $userIDObj = Userkey::firstWhere('key', $userToken);
        $userID = $userIDObj->users_id;
        // end- fetching user id from user token

        // start - getting order details
        $order_details = DB::table('order')->select('order_at', 'pickup_point')->where('id', '=', $orderID)->first();
        $cancellable_time = DB::table('settings')->select('value')->where('key', '=', 'ORDER_CANCELLABLE_TIME')->first();
        $order_at = $order_details->order_at;
        date_default_timezone_set('Asia/Dubai');
        $now = date('Y-m-d H:i:s');
        $hrs = explode(':', $cancellable_time->value)[0];
        $mnts = explode(':', $cancellable_time->value)[1] ?? 0;

        if (strtotime(date('Y-m-d H:i:s', strtotime($order_at) - $hrs * 60 * 60 - $mnts * 60)) < strtotime($now)) {
            return response()->json(['status' => 'failed', 'message' => 'Order can be cancelled only before ' . $cancellable_time->value . ' Hrs of departure.'], 401);
        }
        $reason = $request->post('reason') == '' ? '' : $request->post('reason');
        DB::table('order')->where('id', '=', $orderID)->update([
            'status' => 'CANCELLED',
            'cancel_reason' => $reason
        ]);

        $default_grabbit_location = default_grabbit_location();
        $notification_data = DB::select("select id as user_id, fcm_token  from `users` where `users`.`pickup_point` = :pickup_point or `users`.`pickup_point`= :other_pickup_point", ['pickup_point' => $default_grabbit_location['location'], 'other_pickup_point' => $order_details->pickup_point]);
        $invID = str_pad($orderID, 5, '0', STR_PAD_LEFT);
        $invID = "CON" . $invID;
        if (!empty($notification_data)) {
            foreach ($notification_data as $key => $value) {
                $title = 'Notification';
                $message = 'Order Id ' . $invID . ' cancelled';
                $this->notification_add($value->user_id, $message, $title, $value->fcm_token, $orderID);
            }
        }

        $subject_data = DB::select("select template from sms_template where `type` = :type", ['type' => 'EMAIL_SUBJECT_CANCELMAIL']);
        $subject = $subject_data[0]->template;
        $invID = str_pad($orderID, 5, '0', STR_PAD_LEFT);
        $invID = "CON" . $invID;
        $subject = str_replace("{{invID}}", $invID, $subject);
        $data = [
            //'subject' => "Order $invID has been cancelled!",
            'subject' => $subject,
            'order_id' => $invID,
            'url_link' => route('admin_orders')
        ];

        $allAdmins = DB::table('users')->select('email')
            ->join('roles', 'roles.id', '=', 'users.role_id')
            ->where('roles.name', '=', 'admin')
            ->get();
        foreach ($allAdmins as $eachAdmin) {
            Mail::to($eachAdmin->email)->send(new CancelEmail($data));
        }

        return response()->json(['status' => 'success', 'message' => "Order cancelled successfully"], 200);
    }

    public function notification_add($user_id, $message, $title, $fcm_token, $order_id)
    {
        $notifications = new Notifications;
        $notifications->users_id = $user_id;
        $notifications->order_id = $order_id;
        $notifications->message = $message;
        $notifications->created_at = date("Y-m-d H:i:s");
        $notifications->updated_at = date("Y-m-d H:i:s");
        $notifications->save();
        // Message payload
        $msg_payload = array(
            'mtitle' => $title,
            'mdesc' => $message,
        );

        // For Android
        $reg_id = $fcm_token;

        $result = $this->android($msg_payload, $reg_id);
    }

    // Sends Push notification for Android users
    public function android($data, $reg_id)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';
        //            $message = array(
        //                'title' => $data['mtitle'],
        //                'message' => $data['mdesc'],
        //                'subtitle' => '',
        //                'tickerText' => '',
        //                'msgcnt' => 1,
        //                'vibrate' => 1
        //            );
        $message = array(
            'title' => $data['mtitle'],
            'body' => $data['mdesc'],
            'vibrate' => 1
            //                'sound' => "default",
            //                'icon' => 'ic_notification'
        );

        $headers = array(
            'Authorization: key=' . self::$API_ACCESS_KEY,
            'Content-Type: application/json'
        );

        $fields = array(
            'registration_ids' => array($reg_id),
            'notification' => $message,
            "android" => [
                "notification" => [
                    "sound"  => "default",
                    "icon"  => "ic_notification",
                ]
            ],
            'data' => $message,
        );

        return $this->useCurl($url, $headers, json_encode($fields));
    }

    // Curl 
    private function useCurl($url, $headers, $fields = null)
    {
        // Open connection
        $ch = curl_init();
        if ($url) {
            // Set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            // Disabling SSL Certificate support temporarly
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            if ($fields) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            }

            // Execute post
            $result = curl_exec($ch);
            if ($result === FALSE) {
                die('Curl failed: ' . curl_error($ch));
            }

            // Close connection
            curl_close($ch);
            //                echo "<pre>";
            //                    print_r($result);
            //                    exit;
            return $result;
        }
    }
}
