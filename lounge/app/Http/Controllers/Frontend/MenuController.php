<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cart_sess_key = $request->session()->get('session_cart_key');
        return view('frontend.our_menu.our_menu',array('session_cart_key' => $cart_sess_key));
    }

    function menu_dishes(Request $request) {

        $key = $request->post('key');
        $user_id = $request->session()->get('user_id');
        if($user_id) {
            $customer_data = DB::table('users')->select('name', 'email', 'phone_number')->where('id', '=', $user_id)->first();
        }

        $cart_added = [];
        if ( ! empty($key) ) {
            $cartkey = DB::select("select id, cart_id from cartkey where `key` = :cartkey ", ['cartkey' => $key]);
            if (empty($cartkey)) {
                $returnData = Array("message" => "Not a valid key", 'status' => FALSE);
                echo json_encode($returnData);
                return;
            } else {
                $cartid = $cartkey[0]->id;
            }
            $cart_added = DB::table('cart_items')->select('products_id', 'item_count')
                ->where('cart_id', '=', $cartkey[0]->cart_id)->get();
        }
        $products = DB::table('products')->select(
            'products.id as product_id', 'products.url_key', 'products.name as product_name', 'products.sku','actual_price', 'discount_price','url_key',
            'category.name as category_name','category.id as category_id', DB::raw('0 as item_count'))
            ->join('product_category', 'products.id', '=', 'product_category.products_id')
            ->join('category', 'product_category.category_id', '=', 'category.id')
            ->where('category.deleted_at', '=', NULL)
            ->where('products.deleted_at', '=', NULL)
            ->where('products.visibility', '=', "YES")
            ->where('products.is_lounge_product', '=', "Y")
            ->orderBy('category.position', 'ASC')
            ->orderBy('products.position', 'ASC')
            ->get();

        $allergies = DB::table('allergies')->select('name', 'image_path')->where('deleted_at', '=', NULL)
            ->where('visibility', '=', 'YES')->orderBy('position', 'ASC')->get();
        
        $allergiesfilterAllPref = DB::table('preferenceoptions')->select('name', 'original_image as image_path', 'id')
            ->where('type', '=', 'Diet')->where('deleted_at', '=', NULL)->get();

        $allergiesfilterPref = [];
        foreach($allergiesfilterAllPref as $ingred) {
            $ingred->image_path = url($ingred->image_path);
            $allergiesfilterPref[] = $ingred;
        }

        $product_allergies = DB::table('product_allergies')->select('name', 'image_path', 'products_id')
            ->join('allergies', 'allergies.id', '=', 'product_allergies.allergies_id')
            ->where('product_allergies.deleted_at', '=', NULL)
            ->where('allergies.visibility', '=', 'YES')->orderBy('position', 'ASC')->get();

//        $product_ingredients = DB::table('product_incredience')->select('products_id', DB::raw('group_concat(incredience_id) as incredience'))
//                ->groupBy('products_id')->get();
        $product_ingredients = DB::table('product_allergies_filter')->select('products_id', DB::raw('group_concat(allergies_filter_id) as incredience'))
                ->groupBy('products_id')->get();    
//        $product_ingredients = DB::table('product_preferenceoptions')
//            ->select('products_id', DB::raw('group_concat(preferenceoptions_id) as incredience'))
//            ->groupBy('products_id')->get(); 

        $categories = DB::table('category')->select('name')->where('visibility', '=', 'YES')
                ->orderBy('position', 'ASC')->where('deleted_at', '!=', NULL)->get();

        $allergiesfilterAll = DB::table('allergies_filter')->select('name', 'image_path', 'id')
                ->where('visibility', '=', 'YES')->where('deleted_at', '=', NULL)->orderBy('position', 'ASC')->get();

        $allergiesfilter = [];
        foreach($allergiesfilterAll as $ingred) {
            $ingred->image_path = url($ingred->image_path);
            $allergiesfilter[] = $ingred;
        }

        $product_thumbnail = DB::table('product_images')->select('path', 'products_id')
            ->join('products', 'products.id', '=', 'product_images.products_id')
            ->where('products.deleted_at', '=', NULL)
            ->where('type', '=', 'THUMBNAIL')->get();

        $thumbnailMapped = [];
        foreach($product_thumbnail as $thumbs) {
            // mapped thumbnail to product.
            $thumbnailMapped[ $thumbs->products_id ] = url($thumbs->path);
        }

        $prodt_allrgs = [];
        foreach($product_allergies as $allr) {
            $allr->image_path = url($allr->image_path);
            $prodt_allrgs[$allr->products_id][] = $allr;
        }

        $products_menu_filter = DB::table('product_preferenceoptions')
            ->select('products_id', DB::raw('group_concat(preferenceoptions_id) as allergy_filters'))
            ->where('type', 'Diet')
            ->groupBy('products_id')->get(); 

        $products_allergy_menu_filters = [];
        foreach($products_menu_filter as $allergy_menu_filter) {
            $products_allergy_menu_filters[ $allergy_menu_filter->products_id ] = $allergy_menu_filter->allergy_filters;
        }

        $itemsAdded = [];
        foreach($cart_added as $cart) {
            // mapped thumbnail to product.
            $itemsAdded[ $cart->products_id ] = $cart->item_count;
        }

        $product_incredient_mapped = [];
        foreach($product_ingredients as $incredient) {
            $product_incredient_mapped[ $incredient->products_id ] = $incredient->incredience;
        }

        $products_all = [];
        $cat_order = [];
        foreach($products as $prdt) {
            if( ! in_array( $prdt->category_id, $cat_order ) ) {
                $cat_order[] = $prdt->category_id;
            }
            if( ! isset( $thumbnailMapped[ $prdt->product_id ] ) ) {
                continue;
            }
            if( isset( $itemsAdded[ $prdt->product_id ] ) ) {
                $prdt->item_count = $itemsAdded[ $prdt->product_id ];
            }
            $prdt->incredience = '';
            if( isset($product_incredient_mapped[ $prdt->product_id ]) ) {
                $prdt->incredience = $product_incredient_mapped[ $prdt->product_id ];
            }

            $prdt->allergy_menu_filters = '';
            if( isset($products_allergy_menu_filters[ $prdt->product_id ]) ) {
                $prdt->allergy_menu_filters = $products_allergy_menu_filters[ $prdt->product_id ];
            }
            
            $prdt->thumnail = $thumbnailMapped[ $prdt->product_id ];
            
            $prdt->url_key = url('dxblounge/productdetail/'.$prdt->url_key);
            $prdt->allergies = $prodt_allrgs[ $prdt->product_id ] ?? [];
            // Split category wise
            $products_all[$prdt->category_id]['category_name'] = $prdt->category_name;
            $products_all[$prdt->category_id]['category_id'] = $prdt->category_id;
            $products_all[$prdt->category_id]['items'][] = $prdt;
        }

        if($products !== null) {
            echo json_encode([
                    'message'=> 'Products fetched Successfully', 
                    'products' => $products_all,
                    'allergies' => $allergies,
                    'categories' => $categories,
                    'incredients' => $allergiesfilter,
                    'thumbnail' => $product_thumbnail,
                    'category_order' => $cat_order,
                    'allergies_filter' => $allergiesfilterPref,
                    'status' => TRUE
                ]);
        } else {
            echo json_encode(['message'=> 'Wrong product', 'status' => FALSE]);
        }

        return;
    }
}
