<?php

namespace App\Http\Controllers\Frontend;

use App\AddonCartItems;
use App\Addons;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Cartkey;
use App\Cart;
use App\Cartitems;
use Illuminate\Support\Facades\Validator;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = DB::select('select id, name, description from category where visibility = "YES" ');
        return view('frontend.home.index', array('categories' => $categories));
    }

    /**
     * Get addon list.
     *
     */
    public function addon_list(Request $request)
    {
        $products_id = $request->input('products_id');

        if (empty($products_id)) {
            return response()->json(['status' => FALSE, 'message' => 'product_id is mandatory'], 401);
        }
        $addonsQuery = "SELECT p.name AS product_name, pi.path AS product_path, ai.path, ad.name, ad.id AS addon_id, ad.actual_price as addon_price
                        FROM `addons_product_mapping` AS apm join `addons` AS ad ON apm.addons_id = ad.id
                        LEFT JOIN `addon_images` AS ai ON apm.addons_id = ai.addons_id  AND ai.type = 'IMAGE'
                        LEFT JOIN `products` AS p ON p.id = apm.`products_id`
                        LEFT JOIN `product_images` as pi ON p.id = pi.`products_id` AND pi.type = 'IMAGE'
                        WHERE apm.`products_id` = :products_id 
                        GROUP BY addon_id, ai.path, ad.name,addon_price ";

        $addons_data = DB::select($addonsQuery, ['products_id' => $products_id]);
        return response()->json([
            'status' => TRUE,
            'data' => $addons_data,
            'type' => 'create',
            'success' => 'Addons mapped with this product list'
        ], 200);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $key = $request->post('key');
        $addons = $request->input('addons');
        $type = $request->input('type');
        $operation = $request->input('operation');
        $cartItemID = $request->input('cart_item_id');

        $users_id = NULL;
        if ($request->session()->get('user_id')) {
            $users_id = $request->session()->get('user_id');
        }
        $item_count = $request->post('item_count');
        $productsid = $request->post('productsid');
        if ((empty($item_count) && $item_count != 0) || empty($productsid)) {
            $returnData = array("message" => "item count, price and product id is mandatory", 'status' => FALSE);
            echo json_encode($returnData);
            exit;
        }
        $pricedata = DB::select("select actual_price from products where `id` = :id ", ['id' => $productsid]);
        if (empty($pricedata)) {
            $returnData = array("message" => "No product available", 'status' => FALSE);
            echo json_encode($returnData);
            exit;
        }
        $price = $pricedata[0]->actual_price;
        if (empty($key)) {
            if ($item_count == 0) {
                $returnData = array("message" => "Cannot add an item as zero quantity");
                echo json_encode($returnData);
                exit;
            }
            $timestamp = date('Y-m-d H:i:s');
            $cart = new Cart;
            if (is_null($users_id)) {
                $cart->users_id = NULL;
                $cart->is_guest = 'YES';
            } else {
                $cart->users_id = $users_id;
                $cart->is_guest = 'NO';
            }
            $cart->item_count = $item_count;
            $tax_percentage = tax_percentage();
            $tax_total = (($price * $item_count) * $tax_percentage);
            $grant_total = ($price * $item_count) + $tax_total;
            $cart->created_at = $timestamp;
            $cart->updated_at = $timestamp;
            $cart->sub_total = ($price * $item_count);
            $savecart = $cart->save();
            $cartid = $cart->id;

            $cartitems = new Cartitems();
            $cartitems->cart_id = $cartid;
            $cartitems->products_id = $productsid;
            $cartitems->item_count = $item_count;
            $cartitems->grant_total = $grant_total;
            $cartitems->tax_total = $tax_total;
            $cartitems->sub_total = ($price * $item_count);
            $cartitems->created_at = $timestamp;
            $cartitems->updated_at = $timestamp;
            $cartitems->save();
            $cart_items_id = $cartitems->id;

            $cartkey = new Cartkey;
            $cartkey->cart_id = $cartid;
            $key = md5($this->generateCode(10));
            $cartkey->key = $key;
            $cartkey->created_at = $timestamp;
            $cartkey->updated_at = $timestamp;
            // $key = $cartkey->save();
            $cartkey->save();
            $totalAddonsAmount = 0;
            if (!empty($addons)) {
                if (count($addons) > 0) {
                    $addonsArray = array();
                    foreach ($addons as $addon) {
                        $addonsArray[] = $addon['addonID'];
                    }

                    $productAddonsQuery = "SELECT addons_id FROM `addons_product_mapping` 
                                WHERE products_id = :products_id AND addons_id NOT IN (" . implode(",", $addonsArray) . ")";
                    $productAddonsResult = DB::select($productAddonsQuery, [':products_id' => $productsid]);
                    foreach ($productAddonsResult as $productsAddon) {
                        $addons[] = array(
                            'count' => 0,
                            'addonID' => $productsAddon->addons_id
                        );
                    }

                    $addonsArray = array();
                    $totalAddonsAmount = 0;
//                    $cartItemObj = Cartitems::find($cart_items_id);
                    foreach ($addons as $addon) {
                        $addonsCart = new AddonCartItems();
                        $addonsCart->cart_items_id = $cart_items_id;
                        $addonsCart->item_count = $addon['count'];
                        $addonsCart->addons_id = $addon['addonID'];
                        $addonsCart->created_at = $timestamp;
                        $addonsCart->save();

                        $addonsArray[] = $addon['addonID'];
                        $addonsMaster = Addons::find($addon['addonID']);
                        $totalAddonsAmount += ($addonsMaster->actual_price * $addon['count']);

                        // $cartItemObj->grant_total += ($addonsMaster->actual_price * $addon['count']);
                    }
                }
            }

            $cart = Cart::find($cartid);
            // $cart->grant_total = $cartItemObj->grant_total;
            // $cart->sub_total =  $cartItemObj->grant_total;
            $cart->grant_total = $grant_total + $totalAddonsAmount;
            $cart->sub_total =  $grant_total + $totalAddonsAmount;
            $cart->save();
            $cartItemObj = Cartitems::find($cart_items_id);
            $cartItemObj->grant_total = $grant_total + $totalAddonsAmount; 
            $cartItemObj->save();

            $request->session()->put('session_cart_key', $key);
            $request->session()->save();

            $returnData = array('cartkey' => $key, 'message' => 'successfully added', 'status' => TRUE);
            echo json_encode($returnData);
            return;
        } else {
            $timestamp = date('Y-m-d H:i:s');
            $cartkey = DB::select("select cart_id from cartkey where `key` = :cartkey ", ['cartkey' => $key]);
            if (empty($cartkey)) {
                $returnData = array('cartkey' => $key, "message" => "Not a valid cart key", 'status' => FALSE);
                echo json_encode($returnData);
                exit;
            } else {
                $item_count = 1;
                $cartID = $cartkey[0]->cart_id;
                $tax_percentage = tax_percentage();
                $tax_total = (($price * $item_count) * $tax_percentage);
                $grant_total = ($price * $item_count) + $tax_total;

                $sqlQuery = '';
                if (!empty($addons)) {
                    $addonsArray = array();
                    foreach ($addons as $addon) {
                        $addonsArray[] = $addon['addonID'];
                    }

                    $productAddonsQuery = "SELECT addons_id FROM `addons_product_mapping` 
                                WHERE products_id = :products_id AND addons_id NOT IN (" . implode(",", $addonsArray) . ")";
                    $productAddonsResult = DB::select($productAddonsQuery, [':products_id' => $productsid]);
                    foreach ($productAddonsResult as $productsAddon) {
                        $addons[] = array(
                            'count' => 0,
                            'addonID' => $productsAddon->addons_id
                        );
                    }
                    $addonsArrayCount = count($addons);
                    $addonsQuery = '';
                    foreach ($addons as $arrayKey => $addon) {
                        $addonsQuery .= " (addons_id = " . $addon['addonID'] . " AND item_count = " . $addon['count'] . ")";
                        $addonsQuery .= ' OR ';
                    }
                    $addonsQuery = rtrim($addonsQuery, " OR ");

                    if (!empty($addonsQuery)) {
                        $sqlAddonQuery = "SELECT COUNT(aci.cart_items_id), aci.cart_items_id FROM addons_cart_items AS 
                                            aci LEFT JOIN cart_items AS ci ON ci.id = aci.cart_items_id 
                                            WHERE ci.cart_id = $cartID AND ci.products_id = $productsid
                                            AND  aci.cart_items_id IN ( SELECT cart_items_id FROM addons_cart_items
                                            WHERE $addonsQuery GROUP BY cart_items_id HAVING COUNT(cart_items_id) = $addonsArrayCount)
                                            GROUP by aci.cart_items_id HAVING COUNT(cart_items_id) = $addonsArrayCount ";
                        $addonResult = DB::select($sqlAddonQuery);
                    } else {
                        $addonResult = [];
                    }
                    if (!empty($cartItemID)) {
                        $operation = 'insertion';
                        if (empty($addonResult)) {
                            $type = '';
                            $addonResult = Cartitems::find($cartItemID);
                            $this->updateAddonCartItem($addonResult, $addons, $type, $cartID, $cartItemID, 'NO');
                        } else {
                            $type = 'update';
                            $checkUserHasUpdatedQuery = "SELECT * FROM `addons_cart_items` WHERE `cart_items_id` = :cart_items_id
                                                            AND ($addonsQuery) GROUP BY `cart_items_id` HAVING COUNT(`cart_items_id`) = $addonsArrayCount ";
                            $checkUserHasUpdatedQueryResult = DB::select($checkUserHasUpdatedQuery, [':cart_items_id' => $cartItemID]);
                            if (empty($checkUserHasUpdatedQueryResult)) {
                                $addonResult = Cartitems::find($addonResult[0]->cart_items_id);
                                $cart_items_id = $this->updateCartItem($addonResult, $item_count, $grant_total, $tax_total, $timestamp, $price, $operation);
                                $this->updateAddonCartItem($addonResult, $addons, $type, $cartID, $cart_items_id, 'YES');

                                // $cartItemObj = Cartitems::find($cartItemID);
                                // $cart = Cart::find($cartItemObj->cart_id);
                                // $cart->grant_total -=  $cartItemObj->grant_total;
                                // $cart->sub_total -=  $cartItemObj->grant_total;
                                // $cart->item_count -=  $cartItemObj->item_count;

                                // // the above code is to club the items together
                                // // find addons price and subtract it from cart
                                // $addonCartItems = DB::select("SELECT SUM(aci.item_count * a.actual_price) AS total  FROM `addons` AS a JOIN addons_cart_items AS aci 
                                //                             ON aci.addons_id = a.id WHERE aci.cart_items_id = :cart_item_id", [':cart_item_id' => $cartItemID]);

                                // $cart->grant_total -=  $addonCartItems[0]->total;
                                // $cart->sub_total -=  $addonCartItems[0]->total;

                                // if ($cart->grant_total <= 0 || $cart->item_count <= 0) {
                                //     $cart->grant_total =  0;
                                //     $cart->sub_total =  0;
                                //     $cart->item_count =  0;
                                // }

                                // $cart->save();

                                DB::delete('DELETE FROM `cart_items` WHERE id = :cart_item_id', [':cart_item_id' => $cartItemID]);
                                DB::delete('DELETE FROM `addons_cart_items` WHERE  cart_items_id = :cart_item_id', [':cart_item_id' => $cartItemID]);
                            }
                        }
                    } else {
                        if (empty($addonResult)) {
                            $cart_items_id = $this->insertCartItems($cartID, $productsid, $item_count, $grant_total, $tax_total, $timestamp, $price);
                            $this->insertAddonItems($addons, $cart_items_id, $timestamp, $cartID, $productsid);
                        } else {
                            $addonResult = Cartitems::find($addonResult[0]->cart_items_id);
                            $cart_items_id = $this->updateCartItem($addonResult, $item_count, $grant_total, $tax_total, $timestamp, $price, $operation);
                            $this->updateAddonCartItem($addonResult, $addons, $type, $cartID, $cart_items_id, 'YES');
                        }
                    }
                } else {

                    if (!empty($cartItemID)) {
                        $addonsQuery = "SELECT ad.actual_price as addon_price, aci.item_count
                                        FROM  `addons` AS ad 
                                        JOIN `addons_cart_items` AS aci on aci.addons_id = ad.id 
                                        WHERE aci.cart_items_id = :cart_item_id
                                        ";
                        $addonsResult = DB::select($addonsQuery, ['cart_item_id' => $cartItemID]);

                        $cartItemObj = Cartitems::find($cartItemID);
                        $addonsCartArray = DB::select('select addons_id AS id,item_count  from addons_cart_items where cart_items_id = :cart_items_id ', [':cart_items_id' => $cartItemID]);
                        $cart = Cart::find($cartID);

                        if ($operation == 'removal') {

                            if (!empty($addonsCartArray)) {
                                foreach ($addonsCartArray as $addon) {
                                    if ($addon->item_count > 0) {
                                        $addonsMaster = Addons::find($addon->id);
                                        $cartItemObj->grant_total -= ($addonsMaster['actual_price'] * $addon->item_count);
                                    }
                                }
                            }

                            $cartItemObj->item_count -= $item_count;
                            $cartItemObj->grant_total -= $grant_total;
                            $cartItemObj->tax_total -= $tax_total;

                            $cart->item_count -=  1;

                            if ($cartItemObj->item_count <= 0) {
                                DB::delete('DELETE FROM `cart_items` WHERE id = :cart_item_id', ['cart_item_id' => $cartItemID]);
                                if (count($addonsResult) > 0) {

                                    $addonCartItems = DB::select("SELECT SUM(aci.item_count * a.actual_price) AS total  FROM `addons` AS a JOIN addons_cart_items AS aci 
                                                            ON aci.addons_id = a.id WHERE aci.cart_items_id = :cart_item_id", [':cart_item_id' => $cartItemID]);

                                    $cartItemObj->grant_total -=  $addonCartItems[0]->total;
                                    $cartItemObj->tax_total -=  $addonCartItems[0]->total;
                                    DB::delete('DELETE FROM `addons_cart_items` WHERE cart_items_id = :cart_item_id', ['cart_item_id' => $cartItemID]);
                                }
                            }
                            $cartItemObj->save();
                            $cart->save();
                        }
                        if ($operation == 'insertion') {
                            $cartItemObj->item_count += $item_count;
                            $cartItemObj->grant_total += $grant_total;
                            $cartItemObj->tax_total += $tax_total;
                            $cartItemObj->save();

                            $cart->item_count +=  1;

                            if (!empty($addonsCartArray)) {
                                foreach ($addonsCartArray as $addon) {
                                    $addonsMaster = Addons::find($addon->id);
                                    $cartItemObj->grant_total += ($addonsMaster['actual_price'] * $addon->item_count);
                                }
                            }
                            $cartItemObj->save();
                            $cart->save();
                        }
                    } else {
                        $sqlQuery = "SELECT ci.id, aci.cart_items_id, ci.item_count, ci.grant_total,
                                ci.sub_total, ci.tax_total FROM cart_items AS ci
                                LEFT JOIN addons_cart_items AS aci ON ci.id = aci.cart_items_id
                                WHERE ci.cart_id = $cartID AND ci.products_id = $productsid HAVING aci.cart_items_id IS NULL";

                        $result = DB::select($sqlQuery);
                        if (empty($result)) {
                            $cart_items_id = $this->insertCartItems($cartID, $productsid, $item_count, $grant_total, $tax_total, $timestamp, $price);
                        } else {
                            $cartItem = Cartitems::find($result[0]->id);
                            $cart_items_id = $this->updateCartItem($cartItem, $item_count, $grant_total, $tax_total, $timestamp, $price, $operation);
                        }
                    }
                }
                $this->calculateCartTotal($key);
                $returnData = array('cartkey' => $key, 'message' => 'successfully updated', 'status' => TRUE);
                echo json_encode($returnData);
                return;
            }
            $this->calculateCartTotal($key);
            $returnData = array('cartkey' => $key, 'message' => 'successfully updated', 'status' => TRUE);
            echo json_encode($returnData);
            return;
        }
        $this->calculateCartTotal($key);
        $returnData = array('cartkey' => $key);
        echo json_encode($returnData);
        exit;
    }

    private function calculateCartTotal($key)
    {
        $cartTotalQuery = "SELECT SUM(ci.grant_total) as grant_total, COUNT(ci.id) as total_count 
                            FROM cart_items AS ci LEFT JOIN cartkey AS c ON ci.cart_id = c.cart_id
                            WHERE c.key = :key";
        $cartTotalResults = DB::select($cartTotalQuery, [":key" => $key]);
        $cartkey = DB::select("select cart_id from cartkey where `key` = :cartkey ", ['cartkey' => $key]);
        $cart = Cart::find($cartkey[0]->cart_id);
        $cart->grant_total = $cartTotalResults[0]->grant_total;
        $cart->item_count = $cartTotalResults[0]->total_count;
        $cart->save();
    }

    /**
     * 
     */
    public function getAddonsCart(Request $request)
    {
        // start - checking the client has passed all the required parameters
        $validator = Validator::make($request->all(), [
            'cart_item_id' => 'required|exists:cart_items,id',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'failed', 'message' => $validator->errors()], 409);
        }
        // end - checking the client has passed all the required parameters

        $cartItemID = $request->input('cart_item_id');
        $productID = $request->input('product_id');

        $addonsQuery = "SELECT p.name AS product_name, pi.path AS product_path, ai.path, ad.name, ad.id AS addon_id, ad.actual_price as addon_price, aci.item_count
                        FROM `addons_product_mapping` AS apm join `addons` AS ad ON apm.addons_id = ad.id
                        LEFT JOIN `addon_images` AS ai ON apm.addons_id = ai.addons_id  AND ai.type = 'IMAGE'
                        LEFT JOIN `addons_cart_items` AS aci on aci.addons_id = ad.id AND aci.cart_items_id = :cart_item_id
                        LEFT JOIN `products` AS p ON p.`id` = apm.`products_id`
                        LEFT JOIN `product_images` as pi ON p.id = pi.`products_id` AND pi.type = 'IMAGE'
                        WHERE apm.`products_id` = :products_id 
                        GROUP BY addon_id, ai.path, ad.name,addon_price,aci.item_count ";
        $addonsResult = DB::select($addonsQuery, ['products_id' => $productID, 'cart_item_id' => $cartItemID]);
        return response()->json(['status' => 'success', 'addons' => $addonsResult], 200);
    }

    function generateCode($limit)
    {
        $code = '';
        for ($i = 0; $i < $limit; $i++) {
            $code .= mt_rand(0, 9);
        }
        return substr(str_shuffle('abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789'), 0, 5) . $code;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $key = $request->post('key');
        if (empty($key)) {
            $returnData = array("message" => "Key is not passed", 'status' => FALSE);
            echo json_encode($returnData);
            exit;
        }
        $cartkey = DB::select("select cart_id from cartkey where `key` = :cartkey ", ['cartkey' => $key]);
        if (empty($cartkey)) {
            $returnData = array("message" => "Not a valid key", 'status' => FALSE);
            echo json_encode($returnData);
            exit;
        } else {
            $cartid = $cartkey[0]->cart_id;
            $dyn_qry_part = '';
            $dyn_condn_part = ['cartid' => $cartid];

            $result = DB::select('select cart.id as cartid, '
                . ' cart_items.id as cart_items_id,'
                . ' addons.id as addons_id,'
                . ' addons.name as addons_name,'
                . ' addons.actual_price as addons_price,'
                . ' addons_cart_items.id as addons_cart_items_id,'
                . ' addons_cart_items.item_count as addons_item_count,'
                . ' cart.item_count as item_count_total,'
                . ' cart.grant_total as grant_total_total,'
                . ' cart.sub_total as sub_total_total,'
                . ' cart.tax_total as tax_total_total,'
                . ' cart_items.item_count as item_count_item_total,'
                . ' cart_items.grant_total as grant_total_item_total,'
                . ' cart_items.sub_total as sub_total_item_total,'
                . ' cart_items.tax_total as tax_total_item_total,'
                . ' products.id as product_id,'
                . ' products.name as product_name,'
                . ' product_images.path,'
                . ' addon_images.path as addon_image_path'

                . ' from cart '

                . ' join cart_items on cart.id = cart_items.cart_id '
                . ' left join addons_cart_items on cart_items.id = addons_cart_items.cart_items_id and addons_cart_items.item_count !=0 '
                . ' left join addons on addons.id = addons_cart_items.addons_id '
                . ' join products on products.id = cart_items.products_id '
                . ' left join product_images on products.id = product_images.products_id and product_images.type="THUMBNAIL"'
                . ' left join addon_images on addons.id = addon_images.addons_id and addon_images.type="THUMBNAIL"'
                . ' where cart.id = :cartid' . $dyn_qry_part . '', $dyn_condn_part);

            $modifiedResult = array(
                'maindata' => array(),
                'eachdata' => array(),
                'status' => TRUE
            );
            foreach ($result as $key => $value) {
                $modifiedResult['maindata'] = array(
                    'id' => $value->cartid,
                    'item_count_total' => $value->item_count_total,
                    'grant_total_total' => $value->grant_total_total,
                    'sub_total_total' => $value->sub_total_total,
                    'tax_total_total' => $value->tax_total_total
                );
                $temp_addons = array();
                if (isset($modifiedResult['eachdata'][$value->cart_items_id])) {
                    if (isset($modifiedResult['eachdata'][$value->cart_items_id]['addons'])) {
                        $temp_addons = $modifiedResult['eachdata'][$value->cart_items_id]['addons'];
                    }
                }
                $modifiedResult['eachdata'][$value->cart_items_id] = array(
                    'id' => $value->cart_items_id,
                    'item_count_item_total' => $value->item_count_item_total,
                    'grant_total_item_total' => $value->grant_total_item_total,
                    'sub_total_item_total' => $value->sub_total_item_total,
                    'tax_total_item_total' => $value->tax_total_item_total,
                    'product_id' => $value->product_id,
                    'product_name' => $value->product_name,
                    'image_path' => url($value->path),
                    'addons' => $temp_addons
                );
                $addons_data = DB::select("select `addons`.`id` as id, `addons`.`name` as name, `addons`.`actual_price` as actual_price from `addons_product_mapping` "
                    . "join `addons` on `addons`.`id` =  `addons_product_mapping`.`addons_id` where `addons_product_mapping`.`products_id` = :products_id", ['products_id' => $value->product_id]);
                $modifiedResult['eachdata'][$value->cart_items_id]['product_addons'] = $addons_data;
                if ($value->addons_cart_items_id != null) {
                    $addon_total_price = ($value->addons_item_count * $value->item_count_item_total) * $value->addons_price;
                    $modifiedResult['eachdata'][$value->cart_items_id]['addons'][$value->addons_cart_items_id] = array(
                        'id' => $value->addons_id,
                        'addon_count' => $value->addons_item_count,
                        'addon_total_price' => $addon_total_price,
                        'addon_item_price' => $value->addons_price,
                        'addon_name' => $value->addons_name,
                        'image_path' => $value->addon_image_path
                    );
                }
            }

            $productsIDCountResults = DB::select(" SELECT products_id, SUM(item_count) AS products_count FROM `cart_items` WHERE cart_id = :cart_id  GROUP BY products_id", [':cart_id' => $cartid]);
            $modifiedResult['each'] = array();
            if (!empty($modifiedResult['eachdata'])) {
                foreach ($modifiedResult['eachdata'] as $key => $value) {
                    foreach ($productsIDCountResults as $result) {
                        if ($result->products_id == $value['product_id']) {
                            $modifiedResult['eachdata'][$key]['total_products_count'] = $result->products_count;
                        }
                    }
                    $temp_each = $modifiedResult['eachdata'][$key];
                    array_push($modifiedResult['each'], $temp_each);
                }
            }
            unset($modifiedResult['eachdata']);

            $addon_total = 0;
            if (!empty($modifiedResult['each'])) {
                foreach ($modifiedResult['each'] as $key => $value) {
                    $modifiedResult['each'][$key]['addon'] = array();
                    if (isset($modifiedResult['each'][$key]['addons'])) {
                        foreach ($modifiedResult['each'][$key]['addons'] as $akey => $avalue) {
                            $temp_addon = $modifiedResult['each'][$key]['addons'][$akey];
                            $addon_total += $modifiedResult['each'][$key]['addons'][$akey]['addon_total_price'];
                            array_push($modifiedResult['each'][$key]['addon'], $temp_addon);
                        }
                        unset($modifiedResult['each'][$key]['addons']);
                    }
                }
                if ($addon_total != 0) {
                    $modifiedResult['maindata']['addons_total'] = amount_format($addon_total);
                }
            }

            return response()->json([
                'status' => TRUE,
                'eachdata' => $modifiedResult['each'],
                'maindata' => $modifiedResult['maindata'],
                'success' => 'Cart list'
            ], 200);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $key = $request->post('key');
        $productsid = $request->post('productsid');
        if (empty($key) || empty($productsid)) {
            $returnData = array("message" => "cart key and product id is mandatory", 'status' => FALSE);
            echo json_encode($returnData);
            exit;
        }
        $cartkey = DB::select("select id from cartkey where `key` = :cartkey ", ['cartkey' => $key]);
        if (empty($cartkey)) {
            $returnData = array("message" => "Not a valid key", 'status' => FALSE);
            echo json_encode($returnData);
            exit;
        } else {
            $cartid = $cartkey[0]->id;
            DB::table('cart_items')->where('cart_id', $cartid)->where('products_id', $productsid)->delete();
            $carttotaldetails = DB::select("select sum(item_count) as itemcnt,sum(grant_total) as granttotal,sum(sub_total) as subtotal,sum(tax_total) as taxtotal  from cart_items where `cart_id` = :cartid group by  cart_id", ['cartid' => $cartid]);
            $item_count_sum = $carttotaldetails[0]->itemcnt;
            $grant_total_sum = $carttotaldetails[0]->granttotal;
            $sub_total_sum = $carttotaldetails[0]->subtotal;
            $tax_total_sum = $carttotaldetails[0]->taxtotal;

            DB::table('cart')
                ->where('id', $cartid)
                ->update([
                    'item_count'       => $item_count_sum,
                    'grant_total' => $grant_total_sum,
                    'sub_total' => $sub_total_sum,
                    'tax_total' => $tax_total_sum,
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
            $returnData = array('cartkey' => $key, 'message' => 'successfully removed', 'status' => TRUE);
            echo json_encode($returnData);
            exit;
        }
        //
    }

    private function insertCartItems($cartID, $productsid, $item_count, $grant_total, $tax_total, $timestamp, $price)
    {

        $cartitems = new Cartitems();
        $cartitems->cart_id = $cartID;
        $cartitems->products_id = $productsid;
        $cartitems->item_count = $item_count;
        $cartitems->grant_total = $grant_total;
        $cartitems->tax_total = $tax_total;
        $cartitems->sub_total = ($price * $item_count);
        $cartitems->created_at = $timestamp;
        $cartitems->updated_at = $timestamp;
        $cartitems->save();

        $cart = Cart::find($cartID);
        $cart->item_count += 1;
        $cart->save();

        $cart_items_id = $cartitems->id;
        return $cart_items_id;
    }

    private function updateCartItem($cartItem, $item_count, $grant_total, $tax_total, $timestamp, $price, $operation)
    {

        $cartItemObj = $cartItem;
        $cart = Cart::find($cartItemObj->cart_id);

        if ($operation == 'insertion') {
            $cartItemObj->item_count += $item_count;
            $cartItemObj->grant_total += $grant_total;
            $cartItemObj->tax_total += $tax_total;
            $cart->item_count += 1;
            $cart->save();
        }
        if ($operation == 'removal') {
            $cartItemObj->item_count -= $item_count;
            $cartItemObj->grant_total -= $grant_total;
            $cartItemObj->tax_total -= $tax_total;
            $cart->item_count -= 1;
        }

        $cartItemObj->updated_at = $timestamp;
        $cart->save();
        if ($cartItemObj->item_count <= 0) {
            DB::delete("DELETE FROM `cart_items` WHERE id = :id ", [":id" => $cartItemObj->id]);
        } else {
            $cartItemObj->save();
        }

        $cart_items_id = $cartItemObj->id;
        return $cart_items_id;
    }

    private function insertAddonItems($addons, $cart_items_id, $timestamp, $cartID, $productsid)
    {
        $addonsArray = array();
        $totalAddonsAmount = 0;
        $cartItemObj = Cartitems::find($cart_items_id);
        foreach ($addons as $addon) {
            $addonsCart = new AddonCartItems();
            $addonsCart->cart_items_id = $cart_items_id;
            $addonsCart->item_count = $addon['count'];
            $addonsCart->addons_id = $addon['addonID'];
            $addonsCart->created_at = $timestamp;
            $addonsCart->save();

            $addonsArray[] = $addon['addonID'];
            $addonsMaster = Addons::find($addon['addonID']);
            $totalAddonsAmount += ($addonsMaster->actual_price * $addon['count']);

            $cartItemObj->grant_total += ($addonsMaster->actual_price * $addon['count']);
        }

        $cartItemObj->save();
    }

    private function updateAddonCartItem($addonsCart, $addons, $type, $cartID, $cartItemID, $grouped)
    {
        $cart = Cart::find($cartID);
        $cartItemObj = Cartitems::find($cartItemID);
        foreach ($addons as $addon) {
            $addonsCartArray = DB::select('select id,item_count  from addons_cart_items where cart_items_id = :cart_items_id and addons_id = :addons_id', [':cart_items_id' => $addonsCart->id, ':addons_id' => $addon['addonID']]);
            if (count($addonsCartArray) > 0) {
                $addonsCartObj = AddonCartItems::find($addonsCartArray[0]->id);
                $addonsMaster = Addons::find($addon['addonID']);

                if ($type == 'update') {
                    $addonsCartObj->item_count = $addon['count'];
                    $cartItemObj->grant_total += ($addonsMaster->actual_price * ($addon['count']));
                    $cart->save();
                } else {
                    if ($addonsCartObj->item_count < $addon['count']) {
                        $cartItemObj->grant_total += $addonsMaster->actual_price * ($addon['count'] - $addonsCartObj->item_count);
                        $cart->save();
                    }
                    if ($addonsCartObj->item_count > $addon['count']) {
                        if (($addonsCartObj->item_count - $addon['count']) != 0) {
                            $cartItemObj->grant_total -= $addonsMaster->actual_price * ($addonsCartObj->item_count - $addon['count']);
                        } else {
                            $cartItemObj->grant_total -= $addonsMaster->actual_price * ($addonsCartObj->item_count - $addon['count']);
                        }
                        $cart->save();
                    }

                    $addonsCartObj->item_count = $addon['count'];
                }
                // $addonsCartObj->grouped = $grouped;
                $addonsCartObj->save();
            }
            // $addonsCartObj->grouped = $grouped;
        }

        $cartItemObj->save();
    }
}
