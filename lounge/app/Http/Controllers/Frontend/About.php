<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Order;

class About extends Controller
{
    function index()
    {
        $intolerence_data = DB::select("select `preferenceoptions`.`name` as name, `preferenceoptions`.`id` as val   from `preferenceoptions` where `preferenceoptions`.`type` = :type", ['type' => 'intolerence']);
        $diet_data = DB::select("select `preferenceoptions`.`name` as name, `preferenceoptions`.`id` as val  from `preferenceoptions` where `preferenceoptions`.`type` = :type", ['type' => 'Diet']);
        return view('frontend.about.dashboard', ['intolerence_data' => $intolerence_data, 'diet_data' => $diet_data]);
    }

    function order_detail($order_id)
    {
        $order_id = base64_decode($order_id);
        $data = $this->history($order_id);

        $order = Order::where('id', $order_id)
            ->with('user')
            ->first();
        
        $status = $order->status;
        $st = [
            'confirm' => $status != 'CANCELLED',
            'ontheway' => in_array($status, ['READY_FOR_PICKUP']),
            'delivered' => $status == 'DELIVERED'
        ];
        
        $dataTransaction = DB::table('payments')->select('*')
            ->where('order_ref', '=', $order_id)->where('status', '=', 'SUCCESS')->first();

        return view('frontend.about.dashboard', ['order_id' => $order_id, 'data' => $data, 'user' => $order->user, 'status' => $st, 'dataTransaction' => $dataTransaction]);
    }

    private function history($orderId)
    {
        $result = DB::select('select `order`.`id` as orderid, '
                . '`order_items`.`id` as orderitemid, '
                . '`order`.`item_count` as item_count_total,'
                . '`order`.`created_at` as created_at,'
                . '`order`.`order_at` as order_at,'
                . '`order`.flight_number as flight_no,'
                . '`order`.gate as gate,'
                . '`order`.pickup_reached_at as pickup_reached_at,'
                . '`order`.pickup_point as boarding_point,'
                . '`order`.`status` as order_status,'
                . ' `order`.`grant_total` as grant_total,'
                . ' `order`.`sub_total` as sub_total,'
                . ' `order`.`tax_total` as tax_total,'
                . ' `order`.`qr_code` as qr_code,'
                . ' order_items.item_count as item_count,'
                . ' order_items.products_id as products_id,'
                . ' order_items.grant_total as grant_item_price,'
                . ' order_items.sub_total as sub_item_price,'
                . ' order_items.tax_total as tax_item_price,'
                . ' products.name as productname,'
                . ' products.actual_price as actual_price,'
                . ' products.description as productdescription,'
                . ' product_images.path as productthumbnail,'
                . ' ado.name as addon_name, ado.actual_price as addon_price, aoi.item_count as addon_item_count'
                . ' from `order` join order_items on `order`.`id` = order_items.order_id'
                . ' join products on `products`.`id` = order_items.products_id '
                . ' left join product_images on `products`.`id` = product_images.products_id and product_images.type = "THUMBNAIL"'
                . ' left join addons_order_items as aoi on order_items.id = aoi.order_items_id'
                .  ' left join addons as ado on aoi.addons_id = ado.id'
                . ' where `order`.`id` = :orderid '
                . '  order by `order`.`id` desc', ['orderid' => $orderId]);

        $modifiedResult = [];
        if (!empty($result)) {
            $modifiedResult = array();
            foreach ($result as $key => $value) {
                if (!isset($modifiedResult[$value->orderid])) {
                    $order_date = date("d-M-Y H:i", strtotime($value->created_at));
                    $invEncode = str_pad($value->orderid, 5, '0', STR_PAD_LEFT);
                    $invEncode = "CON" . $invEncode;
                    $modifiedResult[$value->orderid] = array(
                        'pickup_reached_at' => $value->pickup_reached_at,
                        'order_id' => $value->orderid,
                        'order_encoded' => $invEncode,
                        'created_at' => $order_date,
                        'flight_no' => $value->flight_no,
                        'boarding_point' => $value->boarding_point,
                        'gate' => $value->gate,
                        'order_status' => str_replace("_", " ", $value->order_status),
                        'item_count_total' => $value->item_count_total,
                        'grant_total' => decimal_format_custom($value->grant_total),
                        'sub_total' => decimal_format_custom($value->sub_total),
                        'tax_total' => decimal_format_custom($value->tax_total),
                        'order_at' => $value->order_at != NULL ? date('d M Y H:i', strtotime($value->order_at)) : 'N/A',
                        'qr_code' => url($value->qr_code)
                    );
                }
                if (!isset($modifiedResult[$value->orderid]['items'][$value->orderitemid])) {
                    $modifiedResult[$value->orderid]['items'][$value->orderitemid] = array(
                        'item_count' => $value->item_count,
                        'grant_item_price' => decimal_format_custom($value->grant_item_price),
                        'actual_price' => decimal_format_custom($value->actual_price),
                        'sub_item_price' => decimal_format_custom(($value->sub_item_price * $value->item_count)),
                        'tax_item_price' => decimal_format_custom($value->tax_item_price),
                        'products_id' => $value->products_id,
                        'productname' => $value->productname,
                        'productthumbnail' => empty($value->productthumbnail) ? $value->productthumbnail : url($value->productthumbnail),
                        'productdescription' => $value->productdescription,
                    );
                    if (!empty($value->addon_name)) {
                        $modifiedResult[$value->orderid]['items'][$value->orderitemid]['addons'][] = array(
                            'addon_name' => $value->addon_name,
                            'addon_price' => $value->addon_price,
                            'addon_item_count' => $value->addon_item_count
                        );
                    } else {
                        $modifiedResult[$value->orderid]['items'][$value->orderitemid]['addons'] = array();
                    }
                } else {
                    if (!empty($value->addon_name)) {
                        $modifiedResult[$value->orderid]['items'][$value->orderitemid]['addons'][] = array(
                            'addon_name' => $value->addon_name,
                            'addon_price' => $value->addon_price,
                            'addon_item_count' => $value->addon_item_count
                        );
                    } else {
                        $modifiedResult[$value->orderid]['items'][$value->orderitemid]['addons'] = array();
                    }
                }
            }
        }
        ksort($modifiedResult);
        
        return $modifiedResult;
    }

    function orderreview($key)
    {
        $key = trim($key);
        if (empty($key)) {
            return redirect()->route('home_page');
        }
        $orderkey_data = DB::select("select `order`.`id` as order_id,`order`.`freshness` as freshness,`order`.`taste` as taste, `order`.`variety` as variety, `order`.`ordering_process` as ordering_process, `order`.`comment` as comment  from `order` where `order`.`key` = :key", ['key' => $key]);
        if (empty($orderkey_data)) {
            return redirect()->route('home_page');
        } else {
            return view('frontend.about.review', ['order_id' => $orderkey_data[0]->order_id, 'order_review_data' => $orderkey_data]);
            //            echo $order_id = $orderkey_data[0]->order_id;
            //            exit;
            //            $customer_data = DB::table('users')->select('name', 'email', 'phone_number','billing_country','billing_city','billing_zip','billing_address')->where('id', '=', $user_id)->first();
            //            $returnData = Array("message" => "Successfully fetched user data","data" => $customer_data);
            //            return response()->json(json_encode($returnData), 200);
        }
    }
}
