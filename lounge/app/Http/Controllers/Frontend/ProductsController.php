<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Products;
use App\Settings;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $product_allergy_map = array();
        $category = $request->get('category');
        $settings = Settings::select('value')->where('key', '=', "HOMEPAGE_PRODUCTS_COUNT")->first();
        $products = DB::select("select distinct p.id, p.name, p.actual_price, p.url_key, pi.path, position from products as p 
                                    left join  product_category as pc on p.id = pc.products_id
                                    left join product_images as pi on p.id = pi.products_id 
                                    where  pc.category_id = '$category' AND pi.type = 'THUMBNAIL' AND p.visibility ='YES' AND 
                                    p.is_lounge_product='Y' AND 
                                    p.deleted_at IS NULL ORDER BY position ASC LIMIT $settings->value");

        $allergies = DB::select('select pa.products_id, a.name, a.description, a.image_path, position from product_category as pc 
                                    left join product_allergies as pa on pa.products_id = pc.products_id
                                    left join allergies as a on pa.allergies_id = a.id 
                                    where  pc.category_id = :category_id and a.visibility = "YES"', ['category_id' => $category]);

        foreach ($products as $key => $product) {
            $product_allergy_map[$key]['product_id'] = $product->id;
            $product_allergy_map[$key]['product_name'] = $product->name;
            $product_allergy_map[$key]['actual_price'] = $product->actual_price;
            $product_allergy_map[$key]['url_key'] = $product->url_key;
            $product_allergy_map[$key]['product_image_path'] = url($product->path);
            foreach ($allergies as  $allergy) {
                if ($product->id == $allergy->products_id) {
                    $product_allergy_map[$key]['allergies'][] = $allergy;
                }
            }
        }
        return json_encode($product_allergy_map);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function productdetail(Request $request, $url_key)
    {

        $ingredients = DB::select('select i.id, i.name, i.description, i.image_path, i.position from incredience as i 
                          left join  product_incredience as pi on i.id = pi.incredience_id
                          left join  products as p on p.id = pi.products_id
                          where  p.url_key = "' . $url_key . '" ORDER BY i.name ASC');

        $addonsResult = array();
        $cart_sess_key = $request->session()->get('session_cart_key');
        $cartid = '';
        if ($cart_sess_key) {
            $cartkey = DB::select("select id from cartkey where `key` = :cartkey ", ['cartkey' => $cart_sess_key]);
            if (!empty($cartkey)) {
                $cartid = $cartkey[0]->id;
            }
        }
        if ($cartid) { // This item may be in cart.
            $product_item = Products::select('id', DB::raw('0 as item_count'))->where('url_key', '=', $url_key)->first();
            $cart_item = DB::table('cart_items')
                ->select('item_count', 'id')
                ->where('cart_id', '=', $cartid)
                ->where('products_id', '=', $product_item->id)->first();

            if ($cart_item) {
                $product_item->item_count = $cart_item->item_count;
                // addons
                $cartItemID = $cartid;
                $productID = $product_item->id;
            }
        } else { // No items in cart.
            $product_item = Products::select('id', DB::raw('0 as item_count'))->where('url_key', '=', $url_key)->first();
        }
        $product_category = DB::table('category')
            ->join('product_category', 'product_category.category_id', '=', 'category.id')
            ->select('category.id', 'category.name')
            ->where('product_category.products_id', '=', $product_item->id)->first();

        $addonsQuery = "SELECT ai.path, ad.name, ad.id AS addon_id, ad.actual_price as addon_price
            FROM `addons_product_mapping` AS apm join `addons` AS ad ON apm.addons_id = ad.id
            LEFT JOIN `addon_images` AS ai ON apm.addons_id = ai.addons_id  AND ai.type = 'IMAGE'
            WHERE apm.`products_id` = :products_id 
            GROUP BY addon_id, ai.path, ad.name,addon_price ";
        $addonsResult = DB::select($addonsQuery, ['products_id' => $product_item->id]);

        //echo '<pre>'; print_r($category); exit;

        $categories = DB::select('select id, name, description from category where visibility = "YES" ');
        // $product = Products::select('id')->where('url_key', '=', $url_key)->first();

        $product_allergy_map = array();
        $products = DB::select('select distinct p.id, p.name, p.actual_price, p.description, pi.type, pi.path from products as p 
                                    left join  product_category as pc on p.id = pc.products_id
                                    left join product_images as pi on p.id = pi.products_id 
                                    where  p.id = :product_id ', ['product_id' => $product_item->id]);

        $allergies = DB::select('select distinct pa.products_id, a.name, a.description, a.image_path from product_category as pc 
                                    left join product_allergies as pa on pa.products_id = pc.products_id
                                    left join allergies as a on pa.allergies_id = a.id 
                                    where  pa.products_id = :product_id and a.visibility = "YES"', ['product_id' => $product_item->id]);

        $category = DB::select('select category.id as id, category.name as name from category join product_category on category.id = product_category.category_id  join products on products.id = product_category.products_id and products.is_lounge_product = "Y" group by category.id');
        foreach ($products as $key => $product) {
            $product_allergy_map[$key]['product_id'] = $product_item->id;
            $product_allergy_map[$key]['item_count'] = $product_item->item_count;
            $product_allergy_map[$key]['product_name'] = $product->name;
            $product_allergy_map[$key]['actual_price'] = $product->actual_price;
            $product_allergy_map[$key]['product_image_path'] = $product->path;
            $product_allergy_map[$key]['description'] = $product->description;
            $product_allergy_map[$key]['type'] = $product->type;
            foreach ($allergies as  $allergy) {
                if ($product->id == $allergy->products_id) {
                    $product_allergy_map[$key]['allergies'][] = $allergy;
                }
            }
        }

        $relateproductsdallergies = array();
        $relatedproducts = DB::select('select distinct p.id, p.name, p.actual_price, p.url_key, pi.type, pi.path from products as p 
                                    left join  product_category as pc on p.id = pc.products_id
                                    left join product_images as pi on p.id = pi.products_id 
                                    where p.is_lounge_product="Y" AND pc.category_id = "' . $product_category->id . '" AND pi.type = "THUMBNAIL" AND p.id != "' . $product->id . '" LIMIT 3');

        $relateproductsdallergies = DB::select('select distinct pa.products_id, a.name, a.description, a.image_path from product_category as pc 
                    left join product_allergies as pa on pa.products_id = pc.products_id
                    left join allergies as a on pa.allergies_id = a.id 
                    where  pa.products_id != :product_id and a.visibility = "YES"', ['product_id' => $product->id]);

        foreach ($relatedproducts as $key => $relatedproduct) {
            $relatedproducts_allergy_map[$key]['product_id'] = $relatedproduct->id;
            $relatedproducts_allergy_map[$key]['name'] = $relatedproduct->name;
            $relatedproducts_allergy_map[$key]['actual_price'] = $relatedproduct->actual_price;
            $relatedproducts_allergy_map[$key]['path'] = $relatedproduct->path;
            $relatedproducts_allergy_map[$key]['type'] = $relatedproduct->type;
            $relatedproducts_allergy_map[$key]['url_key'] = 'productdetail/' . $relatedproduct->url_key;
            foreach ($relateproductsdallergies as  $relateproductsdallergy) {
                if ($relatedproduct->id == $relateproductsdallergy->products_id) {
                    $relatedproducts_allergy_map[$key]['allergies'][] = $relateproductsdallergy;
                }
            }
        }

        //  echo '<pre>'; print_r($relatedproducts_allergy_map); exit;
        return view(
            'frontend.products.detailpage',
            array(
                'ingredients' => $ingredients,
                'productdetails' => $product_allergy_map,
                'productallergies' => $product_allergy_map[0]['allergies'] ?? [],
                'categories' => $category,
                'product_category_id' => $product_category->id,
                'product_category_name' => $product_category->name,
                'relatedproducts' => $relatedproducts_allergy_map ?? [],
                'addons' => $addonsResult
            )
        );
    }
}
