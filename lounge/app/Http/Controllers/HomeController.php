<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $permissions_data = user_permissions(Auth::user());
        $userdetails = DB::table('users')->select(DB::raw('name, email'))->where('id', '=', Auth::user()->id)->first();
        $request->session()->put('user_name', $userdetails->name);
        $request->session()->put('email', $userdetails->email);
        
        if( in_array('admin', $permissions_data['roles']) ) {
            return redirect('admin/reports');
        }
        
        if( in_array('kitchen', $permissions_data['roles']) ) {
            return redirect('admin/kitchen');
        }

        if( in_array('pickup', $permissions_data['roles']) ) {
            return redirect('admin/orders');
        }

        return view('admin.home', [
            'permissions' => $permissions_data['permissions'],
            'user_has_roles' => $permissions_data['roles']
        ]);
    }

    public function logout(Request $request) {
        $request->session()->forget('user_name');
        $request->session()->forget('email');
        Auth::logout();
        return redirect('admin');
    }
    
}
