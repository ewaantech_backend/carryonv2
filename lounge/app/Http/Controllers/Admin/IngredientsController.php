<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Ingredients;
use Illuminate\Support\Facades\Auth;

class IngredientsController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions_data = user_permissions(Auth::user());
        return view('admin.Ingredients.Ingredients', 
                array('ingredients' => $this->fetchIngredients(),
                    'permissions' => $permissions_data['permissions'],
                    'user_has_roles' => $permissions_data['roles']));
      
    }


    // Getting Allergies list
    private function fetchIngredients(){
    $ingredients = DB::table('incredience')
                        ->select('id','name','description','visibility', 'is_filter','created_at','updated_at','deleted_at')
                        ->orderBy('created_at', 'desc')
                        ->get();
         return $ingredients;
    }


    public function create(Request $request)
    {  
       return view('admin.Ingredients.IngredientsCreate');
    }

    public function add(Request $request)
    {
        //echo 'hai';
        $validatedData = $request->validate([
            'name' => 'required',
            'description' => 'required',
            'visibility' => 'required',
            'image' => 'required',
            'fliter' => 'required'
             ]);
        $ingrId = '';    
        $timestamp = date('Y-m-d H:i:s');
        $ingredients = new Ingredients;
        $ingredients->name = $request->name;
        $ingredients->description = $request->description;
        $ingredients->visibility = $request->visibility;
        $ingredients->is_filter = $request->fliter;
        $ingredients->image_path = ''; //$request->image;
        $ingredients->created_at = $timestamp;
        $ingredients->updated_at = $timestamp;
        $save = $ingredients->save();
        $ingrId = $ingredients->id;
        $input['image'] = time().'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('images/frontend/ingredients'), $input['image']);

        DB::table('incredience')
            ->where('id', $ingrId)
            ->update([
                'image_path' => 'images/frontend/ingredients/' . $input['image']
            ]);
        if ($save) $request->session()->flash('status', 'Ingredients created successfully');
       
        return redirect('/admin/ingredients');
    }


    public function edit($id)
        {
            //echo  $id;
            $ingredients = DB::table('incredience')
                    ->select('id', 'name', 'description', 'visibility', 'image_path', 'is_filter' )
                    ->where([
                        ['id', '=', $id],
                    ])
                    ->get();
            return view('admin.Ingredients.IngredientsEdit', array('ingredients' => $ingredients));
            
        }

    public function update(Request $request)
        {
         $id = $request->ing_id;
         if($request->image) {
            $input['image'] = time().'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('images/frontend/ingredients'), $input['image']);
            DB::table('incredience')
            ->where('id', $id)
            ->update([
                'image_path' => 'images/frontend/ingredients/' . $input['image']
            ]);
            }
            $ingredients =  DB::table('incredience')
                ->where('id', $id)
                ->update([
                    'name'       => $request->post('name'),
                    'description'=> $request->post('description'),
                    'visibility' => $request->post('visibility'),
                    'is_filter'  => $request->post('fliter'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
             
                if ( $ingredients) $request->session()->flash('status', 'Ingredients updated successfully');
       
                return redirect('/admin/ingredients');
              
        }

        public function delete ($id)
        {
            //echo $id;
            DB::table('incredience')->where('id', $id)->delete();
            return redirect('/admin/ingredients');
        }
    
}
