<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Category;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     $permissions_data = user_permissions(Auth::user());
        return view('admin.Category.Category', 
                array('categories' => $this->fetchCategories(),
                    'permissions' => $permissions_data['permissions'],
                    'user_has_roles' => $permissions_data['roles']));
      
    }
    public function category_add(Request $request) {
     
        $categoryname = $request->post('category_name');
        $description = $request->post('description_name');
        $visibility = $request->post('visibility');
        $created_at = date('Y-m-d H:i:s');
        $position=$request->post('position');
        $existCheck = Category::where('name','=', $categoryname)->first();
        if (!$categoryname || !$description || !$visibility || !$created_at||!$position) {
            echo json_encode(['message' => 'Provide all fields', 'status' => FALSE]);

            return;
        }

        if ($existCheck !== null) {
            echo json_encode(['message' => 'Category already exists', 'status' => FALSE]);

            return;
        }
        
        $image_path = NULL;
        if( $request->file('single_image') ) {
            $path = '/category_images';
            $imageName = time().'.'.$request->single_image->getClientOriginalExtension();
            $request->single_image->move(public_path($path), $imageName);
            $image_path = $path.'/'.$imageName;
        }
        
        $alternative_image_path = NULL;
        if( $request->file('alternative_image') ) {
            $path = '/category_alt_images';
            $imageName = time().'.'.$request->alternative_image->getClientOriginalExtension();
            $request->alternative_image->move(public_path($path), $imageName);
            $alternative_image_path = $path.'/'.$imageName;
        }

        $category = Category::create(['name' => $categoryname, 'description' => $description,
                     'visibility' => $visibility,'created_at' => $created_at,'position'=> $position, 'image_path' => $image_path, 'alternative_image_path' => $alternative_image_path]);

        if ($category) {
            echo json_encode(['message' => 'Category Added Successfully', 'status' => TRUE]);
        } else {
            echo json_encode(['message' => 'Something went wrong', 'status' => FALSE]);
        }

        return;
    }
    public function category_edit(Request $request) {
        $category_id = $request->post('category-id');
        $categoryname = $request->post('category_name');
        $description = $request->post('description_name');
        $visibility = $request->post('visibility');
        $updated_at = date('Y-m-d H:i:s');
        $position=$request->post('position');
        if (!$categoryname || !$description || !$visibility || !$updated_at||!$position) {
            echo json_encode(['message' => 'Provide all fields', 'status' => FALSE]);

            return;
        }
        $existCheck =Category::where('name', '=', $categoryname)->where('id', '!=', $category_id)->first();
        if ($existCheck !== null) {
            echo json_encode(['message' => 'Category already exists', 'status' => FALSE]);

            return;
        }
        $category =Category::where('id', '=', $category_id)->first();
        $category->name = $categoryname;
        $category->description =  $description;
        $category->visibility = $visibility;
        $category->updated_at = date('Y-m-d H:i:s');
        $category->position = $position;
        if( $request->file('single_image') ) {
            $path = '/category_images';
            $imageName = time().'.'.$request->single_image->getClientOriginalExtension();
            $request->single_image->move(public_path($path), $imageName);
            $image_path = $path.'/'.$imageName;
            $category->image_path = $image_path;
        }
        
        if( $request->file('alternative_image') ) {
            $path = '/category_alt_images';
            $imageName = time().'.'.$request->alternative_image->getClientOriginalExtension();
            $request->alternative_image->move(public_path($path), $imageName);
            $image_path = $path.'/'.$imageName;
            $category->alternative_image_path = $image_path;
        }
        $category->save();
        if ($category) {
            echo json_encode(['message' => 'Category Updated Successfully', 'status' => TRUE]);
        } else {
            echo json_encode(['message' => 'Something went wrong', 'status' => FALSE]);
        }

        return;
    }

    public function category_data(Request $request) {
        $category_id = $request->post('id');
        $category =Category::select('id', 'name','description','visibility','position','image_path','alternative_image_path')->where('id', '=',   $category_id)->first();
        if ( $category !== null) {
            echo json_encode(['message' => 'Category fetched Successfully', 'cms' =>  $category, 'status' => TRUE]);
        } else {
            echo json_encode(['message' => 'Wrong data', 'status' => FALSE]);
        }

        return;
    }
    public function category_destroy(Request $request) {
        $category_id = $request->post('id');
        $category =Category::where('id', '=', $category_id )->first();
        $category->delete();

        if ($category) {
            echo json_encode(['message' => 'Deleted Successfully', 'status' => TRUE]);
        } else {
            echo json_encode(['message' => 'Something went wrong', 'status' => FALSE]);
        }

        return;
    }    
      
     // Getting category details
        private function fetchCategories(){
            $categories = DB::table('category')
                        ->select('id', 'name','description','visibility', 'created_at','updated_at','deleted_at','position')
                        ->orderBy('created_at', 'desc')
                        ->get();
                        //->paginate(10);
            return $categories;
        }
         // For checking duplicate categories
  
}
