<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Cmsslider;
use Validator;
use Illuminate\Support\Facades\Auth;

class CmsSliderController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $permissions_data = user_permissions(Auth::user());
          return view('admin.Cmsslider.Cmsslider', array(
                    'allergies' => $this->fetchCmsSlider(),
                    'permissions' => $permissions_data['permissions'],
                    'user_has_roles' => $permissions_data['roles']));
    }

    public function banners()
    {
          $permissions_data = user_permissions(Auth::user());
          return view('admin.Cmsslider.Cmsslider', array('allergies' => $this->fetchCmsSliderBanners(),
                    'permissions' => $permissions_data['permissions'],
                    'user_has_roles' => $permissions_data['roles']));
    }
    // Getting Allergies list
    private function fetchCmsSlider(){
        $allergies = DB::table('cms_slider')
                        ->select('id','title','description','visibility','created_at','updated_at','deleted_at','position','short_code')
                        ->where('deleted_at','=', NULL)
                        ->where('short_code','=', NULL)
                        ->orderBy('position', 'asc')
                        ->get();
         return $allergies;
    }
    private function fetchCmsSliderBanners(){
        $allergies = DB::table('cms_slider')
                        ->select('id','title','description','visibility','created_at','updated_at','deleted_at','position','short_code')
                        ->where('deleted_at','=', NULL)
                        ->where('short_code','!=', NULL)
                        ->orderBy('position', 'asc')
                        ->get();
         return $allergies;
    }


    public function create(Request $request)
    {  
       return view('admin.Cmsslider.CmssliderCreate');
    }


    public function add(Request $request)
    {
                     
            $validatedData = $request->validate([
                'name' => 'required',
                'visibility' => 'required',
                'image' => 'required',
                 ]);
            $allerId = '';    
            $timestamp = date('Y-m-d H:i:s');
            $slider = new Cmsslider;
            $slider->title = $request->name;
            if( isset($request->cms_description) && $request->cms_description != '') {
                $slider->description = $request->cms_description;
            } else {
                $slider->description = '';
            }
            $link = '';
            if( isset($request->link) && $request->link != '' ) {
                $link = $request->link;
            }
            $link_text = '';
            if( isset($request->link_text) && $request->link_text != '' ) {
                $link_text = '';
            }

            $slider->visibility = $request->visibility;
            $slider->position = $request->position;
            $slider->image_path = ''; //$request->image;
            $slider->link = $link;
            $slider->link_text = $link_text;
            $slider->cover_image_path = '';
            $slider->created_at = $timestamp;
            $slider->updated_at = $timestamp;
            $save = $slider->save();
            $allerId = $slider->id;
            $input['image'] = time().'.'.$request->image->getClientOriginalExtension();
            $input['cimage'] = time().'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('images/frontend/cms_image'), $input['image']);
            if($request->cimage) {
                $request->cimage->move(public_path('images/frontend/cms_cover_image'), $input['cimage']);
            }

            DB::table('cms_slider')
                ->where('id', $allerId)
                ->update([
                    'image_path' => 'images/frontend/cms_image/' . $input['image'],
                    'cover_image_path' => 'images/frontend/cms_cover_image/' . $input['cimage']
                ]);
            if ($save) $request->session()->flash('status', 'CMS Slider created successfully');
            return redirect('/admin/cms_slider');
        }


        public function edit($id)
        {
            // echo  $id;
            // exit;
            $slider = DB::table('cms_slider')
                    ->select('id', 'title', 'description', 'visibility', 'image_path','cover_image_path','position','short_code', 'link', 'link_text' )
                    ->where([
                        ['id', '=', $id],
                    ])
                    ->get();
            return view('admin.Cmsslider.CmssliderEdit', array('allergies' => $slider));
        }

        public function update(Request $request) {
            $id = $request->aller_id;
            if($request->image) {
            $input['image'] = time().'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('images/frontend/cms_image'), $input['image']);
            DB::table('cms_slider')
            ->where('id', $id)
            ->update([
                'image_path' => 'images/frontend/cms_image/' . $input['image']
                ]);
            }
            if($request->cimage) {
                $input['cimage'] = time().'.'.$request->cimage->getClientOriginalExtension();
                $request->cimage->move(public_path('images/frontend/cms_cover_image'), $input['cimage']);
                DB::table('cms_slider')
                ->where('id', $id)
                ->update([
                    'cover_image_path' => 'images/frontend/cms_cover_image/' . $input['cimage']
                ]);
            }
            $description = $request->post('cms_description');
            if( $description ) {
                $description = $request->post('cms_description');
            } else {
                $description = '';
            }
            $link = $request->post('link');
            if( ! $link ) {
                $link = '';
            }
            $link_text = $request->post('link_text');
            if( ! $link_text ) {
                $link_text = '';
            }
            $slider= DB::table('cms_slider')
                ->where('id', $id)
                ->update([
                    'title'       => $request->post('name'),
                    'description'=> $description,
                    'link' => $link,
                    'link_text' => $link_text,
                    'visibility' => $request->post('visibility'),
                    'position'   => $request->post('position'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);
            if ( $slider) $request->session()->flash('status', 'Slider updated successfully');
                    $allergies = DB::table('cms_slider')
                    ->select('short_code')
                    ->where('id','=', $id)
                    ->where('short_code','=', NULL)
                    ->get();
            if(count($allergies) > 0 ){
                return redirect('/admin/cms_slider');
            } else {
                return redirect('/admin/cms_slider/banners');
            }
        }


        public function delete ($id)
        {
            
          $slider= DB::table('cms_slider')
                ->where('id', $id)
                ->update([
                    'deleted_at' => date('Y-m-d H:i:s'),
                ]);            
            return redirect('/admin/cms_slider');

    } 
    
}
   

