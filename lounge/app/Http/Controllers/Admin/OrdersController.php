<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Orders;
use App\Orderlog;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrderReadyForPickup;
use PDF;
use App\ShortLink;
use Illuminate\Support\Str;
use App\Notifications;

class OrdersController extends Controller
{

    private static $API_ACCESS_KEY = 'AAAA0Ihrr6E:APA91bHDWmqOd5813O2xPryWcxkqrUxl9_j6KjwuRDgIg_Elmtl6zKhc4k3gq81vFJDEc3wlV0ClYJc8dFYiKXsmVgPvCESxUPXUD_nsTysb5T7GAS5c710Xtx3eVqhk6Aky6cEg96aE'; //'AIzaSyCCxJPno4WLLkGO6LM92weZ9ipY_jyGA5s';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions_data = user_permissions(Auth::user());
        $pickup_points = DB::table('boarding_pickup_map')->select(DB::raw('pickup_point as name'))->groupBy('pickup_point')->get();
        $type = (isset($_GET['t']) && in_array($_GET['t'], ['DELIVERED', 'CANCELLED'])) ? $_GET['t'] : '';

        $user_id = Auth::user()->id;
        $user_pickup_point = DB::table('users')->select('pickup_point', 'is_default_pickup_person')->where('id', '=', $user_id)->first();
        $pickup_location = '';
        if (in_array('pickup', $permissions_data['roles'])) {
            $pickup_location = $user_pickup_point->pickup_point ?? '';
        }

        return view('admin.Orders.Orders', [
            'permissions' => $permissions_data['permissions'],
            'user_has_roles' => $permissions_data['roles'],
            'pickup_points' => $pickup_points,
            'type' => $type,
            'user_pickup_point' => $pickup_location
        ]);
    }

    // Sends Push notification for Android users
    public function android($data, $reg_id)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';
        
        $message = array(
            'title' => $data['mtitle'],
            'body' => $data['mdesc'],
            'vibrate' => 1,
            'sound' => "default",
            'icon' => 'ic_notification'
        );

        $headers = array(
            'Authorization: key=' . self::$API_ACCESS_KEY,
            'Content-Type: application/json'
        );

        $fields = array(
            'registration_ids' => array($reg_id),
            'notification' => $message,
            "android" => [
                "notification" => [
                    "sound"  => "default",
                    "icon"  => "ic_notification",
                ]
            ],
            'data' => $message
        );

        return $this->useCurl($url, $headers, json_encode($fields));
    }

    // Sends Push notification for Android users
    public function android_n($data, $reg_id)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';
        //            $message = array(
        //                'title' => $data['mtitle'],
        //                'message' => $data['mdesc'],
        //                'subtitle' => '',
        //                'tickerText' => '',
        //                'msgcnt' => 1,
        //                'vibrate' => 1
        //            );
        $message = array(
            'title' => $data['mtitle'],
            'body' => $data['mdesc'],
            'vibrate' => 1,
            // 'sound' => "default",
            'color' => '#D35211',
        );

        $headers = array(
            'Authorization: key=' . self::$API_ACCESS_KEY,
            'Content-Type: application/json'
        );

        $fields = array(
            'registration_ids' => array($reg_id),
            'notification' => $message,
            "android" => [
                "notification" => [
                    "sound"  => "default",
                    "icon"  => "ic_notification",
                ]
            ],
            'data' => $message
        );

        return $this->useCurl($url, $headers, json_encode($fields));
    }

    // Curl 
    private function useCurl($url, $headers, $fields = null)
    {
        // Open connection
        $ch = curl_init();
        if ($url) {
            // Set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            // Disabling SSL Certificate support temporarly
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            if ($fields) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            }

            // Execute post
            $result = curl_exec($ch);
            if ($result === FALSE) {
                die('Curl failed: ' . curl_error($ch));
            }

            // Close connection
            curl_close($ch);
            return $result;
        }
    }

    function show()
    {
        $permissions_data = user_permissions(Auth::user());
        $aColumns = array(
            // datatable column index  => database column name
            // 0 => 'id',
            0 => 'id',
            // 2 => 'username',
            // 3 => 'phone_number',
            1 => 'flight_number',
            2 => 'pickup_time',
            3 => 'pickup_point',
            // 7 => 'status',
            //5 => 'created_at',
            4 => 'status',
            5 => 'status',
        );
        /* 
        * Paging
        */
        $sLimit = "";
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            $sLimit = "LIMIT " . $_GET['iDisplayStart'] . ", " .
                $_GET['iDisplayLength'];
        }


        /*
        * Ordering
        */
        if (isset($_GET['iSortCol_0'])) {
            $sOrder = "ORDER BY  ";
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] . "
                        " . $_GET['sSortDir_' . $i] . ", ";
                }
            }

            $sOrder = substr_replace($sOrder, "", -2);
            if ($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        }


        /* 
        * Filtering
        * NOTE this does not match the built-in DataTables filtering which does it
        * word by word on any field. It's possible to do here, but concerned about efficiency
        * on very large tables, and MySQL's regex functionality is very limited
        */
        $sWhere = "";
        if ($_GET['sSearch'] != "") {
            $sWhere = "WHERE (";
            for ($i = 0; $i < count($aColumns); $i++) {
                $columnItem = $aColumns[$i];
                if ($columnItem == 'serial_no') continue;
                if ($columnItem == 'id') $columnItem = '`order`.id';
                if ($columnItem == 'username') $columnItem = 'users.name';
                if ($columnItem == 'status') $columnItem = '`order`.status';
                if ($columnItem == 'created_at') $columnItem = '`order`.created_at';
                $sWhere .= $columnItem . " LIKE '%" . $_GET['sSearch'] . "%' OR ";
            }
            $sWhere = substr_replace($sWhere, "", -3);
            $sWhere .= ')';
        }

        if (isset($_GET['status_orders']) && $_GET['status_orders'] != '' && in_array($_GET['status_orders'], ['DELIVERED', 'CANCELLED'])) {
            $sWhere .= $sWhere == '' ? 'WHERE ' : ' AND ';
            $sWhere .= ' `order`.status = "' . $_GET['status_orders'] . '"';
        } else {
            $sWhere .= $sWhere == '' ? 'WHERE ' : ' AND ';
            $sWhere .= ' `order`.status not in ("DELIVERED", "CANCELLED") ';
            if (isset($_GET['status_type']) && $_GET['status_type'] != '') {
                $sWhere .= $sWhere == '' ? 'WHERE ' : ' AND ';
                if ($_GET['status_type'] == 'UPCOMING') {
                    date_default_timezone_set('Asia/Dubai');
                    // In upcoming orders show items with these statuses.
                    $tomorrow = date('Y-m-d', strtotime('+1 day', strtotime(date('Y-m-d'))));
                    $today = date('Y-m-d');
                    $status_to_included = '"IN_KITCHEN", "ORDER_CONFIRMED", "READY_FOR_PICKUP"';
                    if (in_array('kitchen', $permissions_data['roles'])) {
                        $status_to_included = '"IN_KITCHEN", "ORDER_CONFIRMED"';
                    }

                    $sWhere .= ' `order`.status in (' . $status_to_included . ') ' .
                        ' and (  date(`order`.`order_at`)="' . $tomorrow . '" OR date(`order`.`order_at`)="' . $today . '") ';
                } else {
                    $sWhere .= ' `order`.status = "' . $_GET['status_type'] . '"';
                }
            }
        }

        if (in_array('kitchen', $permissions_data['roles'])) {
            $sWhere .= $sWhere == '' ? 'WHERE ' : ' AND ';
            $sWhere .= ' (`order`.pickup_reached_at IS NULL OR `order`.pickup_reached_at = "")';
        }

        $enable_pickup_change_option = FALSE;

        if (in_array('pickup', $permissions_data['roles'])) {
            $user_id = Auth::user()->id;
            $pickup_point = DB::table('users')->select('pickup_point', 'is_default_pickup_person')->where('id', '=', $user_id)->first();
            $p_point = $pickup_point->pickup_point ?? '';
            $is_default_pickup_person = $pickup_point->is_default_pickup_person ?? '';
            $sWhere .= $sWhere == '' ? ' WHERE ' : ' AND ';
            if ($is_default_pickup_person == 'YES') {
                $enable_pickup_change_option = TRUE;
                if ($_GET['status_type'] == '' || $_GET['status_type'] == 'UPCOMING') {
                    // In all orders tab, display all.
                    $list_all_cond = '';
                } else {
                    $list_all_cond = ' AND ( `order`.pickup_reached_at is null OR `order`.pickup_reached_at="")';
                }
                //$sWhere .= ' `order`.status in ("IN_KITCHEN", "ORDER_CONFIRMED", "READY_FOR_PICKUP", "DELIVERED") '.$list_all_cond;
                $sWhere .= ' 1=1 ' . $list_all_cond;
            } else {
                if ($_GET['status_type'] == '' || $_GET['status_type'] == 'UPCOMING') {
                    $list_all_cond = '';
                } else {
                    $default_grabbit_location = "At " . default_grabbit_location()['location'];
                    $list_all_cond = ' AND ( `order`.pickup_reached_at ="' . $default_grabbit_location . '") ';
                }
                // orders of pickup person's location.
                //$sWhere .= ' `order`.status in ("READY_FOR_PICKUP", "DELIVERED") AND `order`.pickup_point = "'. $p_point .'" '.$list_all_cond;
                $sWhere .= ' `order`.pickup_point = "' . $p_point . '" ' . $list_all_cond;
            }
        }

        if (!in_array('pickup', $permissions_data['roles'])  && !in_array('kitchen', $permissions_data['roles'])) {
            if ($_GET['status_type'] == 'READY_FOR_PICKUP') {
                $sWhere .= $sWhere == '' ? ' WHERE ' : ' AND ';
                $default_grabbit_location = "At " . default_grabbit_location()['location'];
                $sWhere .=  ' ( `order`.pickup_reached_at ="' . $default_grabbit_location . '") ';
            }
        }

        // is admin??
        if (!$enable_pickup_change_option && !in_array('pickup', $permissions_data['roles'])  && !in_array('kitchen', $permissions_data['roles'])) {
            $enable_pickup_change_option = TRUE;
        }

        $date_from = $_GET['date_from'] ?? '';
        $date_to = $_GET['date_to'] ?? '';
        if ($date_from) {
            $sWhere .= $sWhere == '' ? 'WHERE ' : ' AND ';
            $sWhere .= ' date(`order`.`order_at`) >="' . $date_from . '" ';
        }
        if ($date_to) {
            $sWhere .= $sWhere == '' ? 'WHERE ' : ' AND ';
            $sWhere .= ' date(`order`.`order_at`) <="' . $date_to . '" ';
        }

        /* Individual column filtering */
        for ($i = 0; $i < count($aColumns); $i++) {
            if ($_GET['bSearchable_' . $i] == "true" && $_GET['sSearch_' . $i] != '') {
                if ($sWhere == "") {
                    $sWhere = "WHERE ";
                } else {
                    $sWhere .= " AND ";
                }
                $sWhere .= $aColumns[$i] . " LIKE '%" . $_GET['sSearch_' . $i] . "%' ";
            }
        }


        $database_name = env('DB_DATABASE');
        // Get count.
        $count = count(DB::select("select `order`.id
        from $database_name.order
        join users on users.id = `order`.users_id
        $sWhere "));

        // Get data.
        $model = DB::select("select 
            `order`.id as serial_no,
            `order`.id,
            `order`.is_reimbursed,
            `order`.pickup_reached_at, 
            `order`.flight_number as flight_number, 
            `order`.order_at,
            `order`.pickup_time, 
            `order`.created_at,
            `order`.status as status, 
            `order`.pickup_point as pickup_point,
            `order`.gate,
            users.name as username,
            users.phone_number as phone_number,
            users.id as user_id
        from $database_name.order
        join users on users.id = `order`.users_id
        $sWhere $sOrder $sLimit ");

        $iTotal = $count;
        $output = array(
            "sEcho" => intval($_GET['sEcho']),
            "iTotalRecords" => count($model),
            "iTotalDisplayRecords" => $iTotal,
            "aaData" => array()
        );
        $badgeClr = [
            'ORDER_CONFIRMED' =>  '<button value="ORDER_CONFIRMED" id="order_btn_{{order_btn_id}}" order_id="{{orderid}}" type="button" class="change-stat-modal btn btn-info btn-flat m-b-10 m-l-5">Order confirmed</button>',
            'IN_KITCHEN' =>       '<button value="IN_KITCHEN" id="order_btn_{{order_btn_id}}" order_id="{{orderid}}" type="button" class="change-stat-modal btn btn-warning btn-flat m-b-10 m-l-5">In kitchen</button>',
            'READY_FOR_PICKUP' => '<button value="READY_FOR_PICKUP" id="order_btn_{{order_btn_id}}" order_id="{{orderid}}" type="button" class="change-stat-modal btn btn-pink btn-flat m-b-10 m-l-5">Ready for pickup</button>',
            'DELIVERED' =>        '<button value="DELIVERED" id="order_btn_{{order_btn_id}}" type="button" order_id="{{orderid}}" class="change-stat-modal btn btn-success btn-flat m-b-10 m-l-5">Delivered</button>',
            'CANCELLED' =>        '<button value="CANCELLED" class="btn btn-danger btn-flat m-b-10 m-l-5">Cancelled</button>',
        ];
        foreach ($model as $keys => $md) {
            $row = array();
            for ($i = 0; $i < count($aColumns); $i++) {
                $key = $aColumns[$i];
                if ($i == 2) {
                    $custome_date = date_format_from_month_custom($md->$key);
                    $custome_date_array = explode(" ", $custome_date);
                    if (!empty($md->$key)) {
                        $row[] = $custome_date_array[0] . " " . $custome_date_array[1] . " <b>" . $custome_date_array[2] . "</b>";
                    } else {
                        $row[] = $custome_date;
                    }
                } else if ($i == 3) { // 3 and 4 added here.
                    $enablePlChange = $enable_pickup_change_option == TRUE && !in_array($md->status, ['DELIVERED', 'CANCELLED']);
                    $displayPl = $md->gate == '-' ? '-' : $md->$key;
                    $row[] = ($enablePlChange == TRUE ? (' <a id="pl_order_' . $md->id . '" value="' . $md->$key . '" class="pp_change_btn btn btn-default" order_id="' . $md->id . '">' . $displayPl . '</a>') : $displayPl);
                    // After 3, Items X count inserted.
                    $items = DB::table('order_items')->select('item_count', 'name')
                        ->join('products', 'products.id', '=', 'order_items.products_id')
                        ->where('order_id', '=', $md->id)->get();
                    $string = '';
                    foreach ($items as $item) {
                        $string .= $item->item_count . ' <b>X</b> ' . $item->name . '<br>';
                    }
                    $row[] =  $string;
                } else if ($i == 4) {
                    $statusBadge = $badgeClr[$md->status] ?? '';
                    if ($md->status == 'READY_FOR_PICKUP' && $md->pickup_reached_at != NULL) {
                        $statusBadge = str_replace('Ready for pickup', $md->pickup_reached_at, $statusBadge);
                        $statusBadge = str_replace('READY_FOR_PICKUP', $md->pickup_reached_at, $statusBadge);
                    }
                    $statusBadge = str_replace('{{orderid}}', $md->id, $statusBadge);
                    $statusBadge = str_replace('{{order_btn_id}}', $md->id, $statusBadge);

                    $row[] = $statusBadge;
                } else if ($i == 5) {
                    $canceloption = "";
                    if (in_array('admin', $permissions_data['roles']) && !in_array($md->status, ['CANCELLED']) && $md->status != 'DELIVERED') {
                        $canceloption = "<div><a style='cursor:pointer;color:blue;' order_id='$md->id' class='cancel_order_item'>Cancel</a></div>";
                    }
                    if (in_array('admin', $permissions_data['roles']) && in_array($md->status, ['CANCELLED']) && $md->is_reimbursed == 'NO' && $md->status != 'DELIVERED') {
                        $canceloption = "<div id='reimburse_$md->id'><a style='cursor:pointer;color:blue;' order_id='$md->id' class='reimburse_order_item'>Refund</a></div>";
                    }
                    if (in_array('admin', $permissions_data['roles']) && in_array($md->status, ['CANCELLED']) && $md->is_reimbursed == 'YES' && $md->status != 'DELIVERED') {
                        $canceloption = "<div>Reimbursed</div>";
                    }
                    $row[] = $canceloption
                        . "<div><a style='cursor:pointer;color:blue;' order_id='$md->id' class='invoice_order_item'>Invoice</a></div>" .
                        "<div><a class='view' data-id='$md->id' href='" . url("/admin/orders/details/$md->id") .
                        "' data-toggle='tooltip' title='View' ><i class='fa fa-eye' aria-hidden='true'></i></a></div>";
                } else {
                    if ($i == 0) {
                        $row[] = "CON" . str_pad($md->$key, 5, '0', STR_PAD_LEFT);
                    } else {
                        $row[] = $md->$key;
                    }
                }
            }
            $output['aaData'][] = $row;
        }
        $output['status_type'] =  $_GET['status_type'];

        echo json_encode($output);
    }

    /**
     * Display the specified resources.
     *
     * @return \Illuminate\Http\Response
     */
    public function details($id)
    {
        $permissions_data = user_permissions(Auth::user());

        $orders = DB::select("select *, o.flight_number as flight_number, o.order_at as departure, o.qr_code,
        o.freshness,o.taste,o.variety,o.ordering_process,o.comment
        from `order` o
        where o.id = :id ", ['id' => $id]);

        $orderlog = DB::select("select *
        from `orderlog` o
        where o.order_id = :id ", ['id' => $id]);

        $user_details = DB::table('order')->select(
            'name',
            'email',
            'phone_number',
            'billing_name',
            'billing_street',
            'billing_street_no',
            'billing_apartment_no',
            'billing_city',
            'billing_address',
            'billing_country'
        )
            ->join('users', 'users.id', '=', 'order.users_id')
            ->where('order.id', '=', $id)->first();

        $result = DB::select('select `order`.`id` as orderid, '
            . '`order_items`.`id` as orderitemid, '
            . '`order`.`item_count` as item_count_total,'
            . '`order`.`created_at` as created_at,'
            . '`order`.`order_at` as order_at,'
            . '`order`.flight_number as flight_no,'
            . '`order`.gate as gate,'
            . '`order`.pickup_reached_at as pickup_reached_at,'
            . '`order`.pickup_point as boarding_point,'
            . '`order`.`status` as order_status,'
            . ' `order`.`grant_total` as grant_total,'
            . ' `order`.`sub_total` as sub_total,'
            . ' `order`.`tax_total` as tax_total,'
            . ' `order`.`qr_code` as qr_code,'
            . ' order_items.item_count as item_count,'
            . ' order_items.products_id as products_id,'
            . ' order_items.grant_total as grant_item_price,'
            . ' order_items.sub_total as sub_item_price,'
            . ' order_items.tax_total as tax_item_price,'
            . ' products.name as productname,'
            . ' products.actual_price as actual_price,'
            . ' products.description as productdescription,'
            . ' product_images.path as productthumbnail,'
            . ' ado.name as addon_name, ado.actual_price as addon_price, aoi.item_count as addon_item_count'
            . ' from `order` join order_items on `order`.`id` = order_items.order_id'
            . ' join products on `products`.`id` = order_items.products_id '
            . ' left join product_images on `products`.`id` = product_images.products_id and product_images.type = "THUMBNAIL"'
            . ' left join addons_order_items as aoi on order_items.id = aoi.order_items_id'
            .  ' left join addons as ado on aoi.addons_id = ado.id'
            . ' where `order`.`id` = :orderid '
            . '  order by `order`.`id` desc', ['orderid' => $id]);
        $modifiedResult = array();
        if (!empty($result)) {
            $i = 0;
            foreach ($result as $key => $value) {
                if ($i == 0) {
                    $modifiedResult = array(
                        'order_id' => $value->orderid,
                        'created_at' => $value->created_at,
                        'order_at' => $value->order_at,
                        'flight_no' => $value->flight_no,
                        'qr_code' => $value->qr_code,
                        'boarding_point' => $value->boarding_point,
                        'order_status' => str_replace("_", " ", $value->order_status),
                        'item_count_total' => $value->item_count_total,
                        'grant_total' => decimal_format_custom($value->grant_total),
                        'sub_total' => decimal_format_custom($value->sub_total),
                        'tax_total' => decimal_format_custom($value->tax_total)
                    );
                }

                if (!isset($modifiedResult['items'][$value->orderitemid])) {
                    $modifiedResult['items'][$value->orderitemid] = array(
                        'item_count' => $value->item_count,
                        'grant_item_price' => decimal_format_custom($value->grant_item_price),
                        'actual_price' => decimal_format_custom($value->actual_price),
                        'sub_item_price' => decimal_format_custom(($value->sub_item_price * $value->item_count)),
                        'tax_item_price' => decimal_format_custom($value->tax_item_price),
                        'products_id' => $value->products_id,
                        'productname' => $value->productname,
                        'productthumbnail' => empty($value->productthumbnail) ? $value->productthumbnail : url($value->productthumbnail),
                        'productdescription' => $value->productdescription,
                    );
                    if (!empty($value->addon_name)) {
                        $modifiedResult['items'][$value->orderitemid]['addons'][] = array(
                            'addon_name' => $value->addon_name,
                            'addon_price' => decimal_format_custom($value->addon_price),
                            'addon_item_count' => $value->addon_item_count
                        );
                    } else {
                        $modifiedResult['items'][$value->orderitemid]['addons'] = array();
                    }
                } else {
                    if (!empty($value->addon_name)) {
                        $modifiedResult['items'][$value->orderitemid]['addons'][] = array(
                            'addon_name' => $value->addon_name,
                            'addon_price' => decimal_format_custom($value->addon_price),
                            'addon_item_count' => $value->addon_item_count
                        );
                    } else {
                        $modifiedResult['items'][$value->orderitemid]['addons'] = array();
                    }
                }
                $i++;
            }
        }

        $content = '';
        if (!empty($modifiedResult)) {
            foreach ($modifiedResult['items'] as $key => $value) {
                $content .= '<tr>
                        <td style="padding: 10px;; color: #4d4d4d; text-align: left;">
                        <h4>' . $value["productname"] . '</h4>
                        </td>
                        <td style="padding: 10px;; color: #4d4d4d; text-align: left;">
                        AED ' .  $value["actual_price"] . '
                        </td>
                        <td style="padding: 10px;; color: #4d4d4d; text-align: left;">
                        ' . $value["item_count"] . '
                        </td>
                        <td style="padding: 10px;; color: #4d4d4d; text-align: left;">
                        AED ' . $value["grant_item_price"] . '
                        </td>
                </tr>';
                if (count($value['addons']) > 0) {
                    $content .= "<tr>
                            <td colspan='5'  style='padding: 10px;;  color: #4d4d4d;' ><h5>Addons</h5></td>
                        </tr>";
                    foreach ($value['addons'] as $addons) {
                        if (!empty($addons['addon_item_count'])) {
                            $addonName = !empty($addons['addon_name']) ? $addons['addon_name'] : 'Nil';
                            $addonPrice = !empty($addons['addon_price']) ? 'AED ' . $addons['addon_price'] : '0';
                            $addonItemCount = !empty($addons['addon_item_count']) ?  $addons['addon_item_count'] : '0';
                            $addonTotal = 'AED ' . $addons['addon_item_count'] * $addons['addon_price'];
                            $content .= "<tr>
                                    <td style='padding: 10px;; color: #4d4d4d; text-align: left;'>
                                        " . $addonName . "
                                    </td>
                                    <td style='padding: 10px;; color: #4d4d4d; text-align: left;'>
                                       " . $addonPrice . "
                                    </td>
                                    <td style='padding: 10px;; color: #4d4d4d; text-align: left;'>
                                        " . $addonItemCount . "
                                    </td>
                                    <td style='padding: 10px;; color: #4d4d4d; text-align: left;'>
                                       " . $addonTotal . "
                                    </td>
                            </tr>";
                        }
                    }
                }
            }
        }


        $pickup_points = DB::table('boarding_pickup_map')->select(DB::raw('pickup_point as name'))->groupBy('pickup_point')->get();

        $user_id = Auth::user()->id;
        $user_pickup_point = DB::table('users')->select('pickup_point', 'is_default_pickup_person')->where('id', '=', $user_id)->first();
        $pickup_location = '';
        if (in_array('pickup', $permissions_data['roles'])) {
            $pickup_location = $user_pickup_point->pickup_point ?? '';
        }
        $invID = str_pad($id, 5, '0', STR_PAD_LEFT);
        $invID = "CON" . $invID;
        $redirect_back = 'orders';
        if (in_array('kitchen', $permissions_data['roles'])) {
            $redirect_back = 'kitchen';
        }
        return view(
            'admin.Orders.details',
            array(
                'orders' => $orders,
                'content' => $content,
                'order_id' => $id,
                'orderid_encoded' => $invID,
                'customer_details' => $user_details,
                'pickup_points' => $pickup_points,
                'permissions' => $permissions_data['permissions'],
                'user_has_roles' => $permissions_data['roles'],
                'user_pickup_point' => $pickup_location,
                'redirect_back' => $redirect_back,
                'orderlog' => $orderlog
            )
        );
    }

    
    /**
     * Display the specified resources.
     *
     * @return \Illuminate\Http\Response
     */
    public function addon_details(Request $request)
    {
        $id = $request->post('id');
        $result = DB::select('select `order`.`id` as orderid, '
            . '`order_items`.`id` as orderitemid, '
            . '`order`.`item_count` as item_count_total,'
            . '`order`.`created_at` as created_at,'
            . '`order`.`order_at` as order_at,'
            . '`order`.flight_number as flight_no,'
            . '`order`.gate as gate,'
            . '`order`.pickup_reached_at as pickup_reached_at,'
            . '`order`.pickup_point as boarding_point,'
            . '`order`.`status` as order_status,'
            . ' `order`.`grant_total` as grant_total,'
            . ' `order`.`sub_total` as sub_total,'
            . ' `order`.`tax_total` as tax_total,'
            . ' `order`.`qr_code` as qr_code,'
            . ' order_items.item_count as item_count,'
            . ' order_items.products_id as products_id,'
            . ' order_items.grant_total as grant_item_price,'
            . ' order_items.sub_total as sub_item_price,'
            . ' order_items.tax_total as tax_item_price,'
            . ' products.name as productname,'
            . ' products.actual_price as actual_price,'
            . ' products.description as productdescription,'
            . ' product_images.path as productthumbnail,'
            . ' ado.name as addon_name, ado.actual_price as addon_price, aoi.item_count as addon_item_count'
            . ' from `order` join order_items on `order`.`id` = order_items.order_id'
            . ' join products on `products`.`id` = order_items.products_id '
            . ' left join product_images on `products`.`id` = product_images.products_id and product_images.type = "THUMBNAIL"'
            . ' left join addons_order_items as aoi on order_items.id = aoi.order_items_id'
            .  ' left join addons as ado on aoi.addons_id = ado.id'
            . ' where `order`.`id` = :orderid '
            . '  order by `order`.`id` desc', ['orderid' => $id]);
        $modifiedResult = array();
        if (!empty($result)) {
            $i = 0;
            foreach ($result as $key => $value) {
                if ($i == 0) {
                    $modifiedResult = array(
                        'order_id' => $value->orderid,
                        'created_at' => $value->created_at,
                        'order_at' => $value->order_at,
                        'flight_no' => $value->flight_no,
                        'qr_code' => $value->qr_code,
                        'boarding_point' => $value->boarding_point,
                        'order_status' => str_replace("_", " ", $value->order_status),
                        'item_count_total' => $value->item_count_total,
                        'grant_total' => decimal_format_custom($value->grant_total),
                        'sub_total' => decimal_format_custom($value->sub_total),
                        'tax_total' => decimal_format_custom($value->tax_total)
                    );
                }

                if (!isset($modifiedResult['items'][$value->orderitemid])) {
                    $modifiedResult['items'][$value->orderitemid] = array(
                        'item_count' => $value->item_count,
                        'grant_item_price' => decimal_format_custom($value->grant_item_price),
                        'actual_price' => decimal_format_custom($value->actual_price),
                        'sub_item_price' => decimal_format_custom(($value->sub_item_price * $value->item_count)),
                        'tax_item_price' => decimal_format_custom($value->tax_item_price),
                        'products_id' => $value->products_id,
                        'productname' => $value->productname,
                        'productthumbnail' => empty($value->productthumbnail) ? $value->productthumbnail : url($value->productthumbnail),
                        'productdescription' => $value->productdescription,
                    );
                    if (!empty($value->addon_name)) {
                        $modifiedResult['items'][$value->orderitemid]['addons'][] = array(
                            'addon_name' => $value->addon_name,
                            'addon_price' => decimal_format_custom($value->addon_price),
                            'addon_item_count' => $value->addon_item_count
                        );
                    } else {
                        $modifiedResult['items'][$value->orderitemid]['addons'] = array();
                    }
                } else {
                    if (!empty($value->addon_name)) {
                        $modifiedResult['items'][$value->orderitemid]['addons'][] = array(
                            'addon_name' => $value->addon_name,
                            'addon_price' => decimal_format_custom($value->addon_price),
                            'addon_item_count' => $value->addon_item_count
                        );
                    } else {
                        $modifiedResult['items'][$value->orderitemid]['addons'] = array();
                    }
                }
                $i++;
            }
        }

        $content = '';
        if (!empty($modifiedResult)) {
            foreach ($modifiedResult['items'] as $key => $value) {
                if (count($value['addons']) > 0) {
//                    $content .= "<tr>
//                            <td colspan='5'  style='padding: 10px;;  color: #4d4d4d;' ><h5>Addons</h5></td>
//                        </tr>";
                    $content .= '<li>'
                            . '<div class="card">'
                            . '<header class="card-header">'
                            . '<div class="row align-items-center">Addons</div>'
                            . '</header>'
                            . '<div class="card-body"><table>'
                            . '<tr>'
                            . '<td style="padding: 10px; color: #4d4d4d; text-align: left;"><b>Item</b></td>'
                            . '<td style="padding: 10px; color: #4d4d4d; text-align: left;"><b>Unit Price</b></td>'
                            . '<td style="padding: 10px; color: #4d4d4d; text-align: left;"><b>Quantity</b></td>'
                            . '<td style="padding: 10px; color: #4d4d4d; text-align: left;"><b>Total(Incl. VAT)</b></td>';
                    foreach ($value['addons'] as $addons) {
                        if (!empty($addons['addon_item_count'])) {
                            $addonName = !empty($addons['addon_name']) ? $addons['addon_name'] : 'Nil';
                            $addonPrice = !empty($addons['addon_price']) ? 'AED ' . $addons['addon_price'] : '0';
                            $addonItemCount = !empty($addons['addon_item_count']) ?  $addons['addon_item_count'] : '0';
                            $addonTotal = 'AED ' . $addons['addon_item_count'] * $addons['addon_price'];
                            $content .= "<tr>
                                    <td style='padding: 10px;; color: #4d4d4d; text-align: left;'>
                                        " . $addonName . "
                                    </td>
                                    <td style='padding: 10px;; color: #4d4d4d; text-align: left;'>
                                       " . $addonPrice . "
                                    </td>
                                    <td style='padding: 10px;; color: #4d4d4d; text-align: left;'>
                                        " . $addonItemCount . "
                                    </td>
                                    <td style='padding: 10px;; color: #4d4d4d; text-align: left;'>
                                       " . $addonTotal . "
                                    </td>
                            </tr>";
//                            $content .= '<ul class="list-unstyled item-list">
//                                    <li>
//                                        ' . $addonName . '
//                                    </li>
//                                    <li>
//                                       ' . $addonPrice . '
//                                    </li>
//                                    <li>
//                                        ' . $addonItemCount . '
//                                    </li>
//                                    <li>
//                                       ' . $addonTotal . '
//                                    </li>
//                                    </ul>';
                        }
                    }
                    $content .= '</table></div>'
                            . '</div>'
                            . '</li>';
                } else {
                        $content .= '<li>'
                            . '<div class="card">'
                            . '<header class="card-header">'
                            . '<div class="row align-items-center">No addons added</div>'
                            . '</header>'
                                . '</div>'
                                . '</li>';
                }
            }
        }

        
        echo json_encode( [
            'content' => $content,
            'status' => TRUE ] 
        );
    }

    function sample_notification($type, $id)
    {
        $notification_data = DB::select("select id as user_id, fcm_token  from `users` where `users`.`id` = :id", ['id' => $id]);
        if (!empty($notification_data)) {
            foreach ($notification_data as $key => $value) {
                $title = 'Notification test';
                $message = 'sample nitification';
                $this->notification_add_sample($value->user_id, $message, $title, $value->fcm_token, $type);
            }
        }
    }

    function notification_add_sample($user_id, $message, $title, $fcm_token, $type)
    {
        // Message payload
        $msg_payload = array(
            'mtitle' => $title,
            'mdesc' => $message,
        );

        // For Android
        $reg_id = $fcm_token;
        if ($type == 'n') {
            echo "payload: notification<br>";
            $result = $this->android_n($msg_payload, $reg_id);
        }

        if ($type == 'd') {
            echo "payload: data<br>";
            $result = $this->android($msg_payload, $reg_id);
        }

        echo "<pre>";
        print_r($result);
        exit;
    }
    function notification_add($user_id, $message, $title, $fcm_token, $order_id)
    {
        $notifications = new Notifications;
        $notifications->users_id = $user_id;
        $notifications->order_id = $order_id;
        $notifications->message = $message;
        $notifications->created_at = date("Y-m-d H:i:s");
        $notifications->updated_at = date("Y-m-d H:i:s");
        $notifications->save();
        // Message payload
        $msg_payload = array(
            'mtitle' => $title,
            'mdesc' => $message,
        );

        // For Android
        $reg_id = $fcm_token;

        $result = $this->android($msg_payload, $reg_id);
    }

    function change_status(Request $request)
    {
        date_default_timezone_set('Asia/Dubai');
        $order_id = $request->post('order_id');
        $status = $request->post('order_status');
        $order = DB::table('order')->where('id', '=', $order_id)->first();
        if ($order === null) {
            echo json_encode(['message' => 'Order not found', 'status' => FALSE]);

            return;
        }

        //        if (empty($order->pickup_point) || $order->pickup_point == '.' || $order->pickup_point == '-' ) {
        //            echo json_encode(['message'=> 'Please assign a pickup point before making any status change', 'status' => FALSE]);
        //
        //            return;            
        //        }
        $currentGate = $order->gate;
        $newGate = '';
        $gateToShow = $order->gate;
        // ------ gate update code starts----------|
        if ($status != 'DELIVERED' && (empty($currentGate) || $currentGate == '-' || $currentGate == '.')) {
            $order_at_data = DB::select("select DATE_FORMAT(`order`.`order_at`,'%Y/%c/%e') as order_at  from `order` where `order`.`id` = :order_id", ['order_id' => $order_id]);
            $fn_prefix = substr($order->flight_number, 0, 2);
            $fn = substr($order->flight_number, 2);
            $handle = curl_init();
            $url = "https://api.flightstats.com/flex/flightstatus/rest/v2/json/flight/status/" . $fn_prefix . "/" . $fn . "/dep/" . $order_at_data[0]->order_at . "?appId=8a5a55e4&appKey=b3e7538d084234f6f83779bdfbd671ed&utc=false&airport=DXB";
            curl_setopt($handle, CURLOPT_URL, $url);
            curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($handle, CURLOPT_HTTPHEADER, array('DY-X-Authorization: 3b75a0bdfc7aa9d383e3f8beef0f2b623a94335c'));
            $output = curl_exec($handle);
            $gateFind = $order->pickup_point;
            if (!curl_errno($handle)) {
                curl_close($handle);
                $output = json_decode($output);
                if (isset($output->flightStatuses[0]->airportResources->departureGate)) {
                    $gate = $output->flightStatuses[0]->airportResources->departureGate;
                    $newGate = $gate;
                    if ($gate) {
                        $gateToShow = $gate;
                        $p_point = DB::table('boarding_pickup_map')->select('pickup_point')->where('gate', '=', $gate)->first();
                        $gateFind = $p_point->pickup_point;
                        $response = DB::table('order')->where('id', '=', $order_id)->update([
                            'gate' => $gate,
                            'updated_at' => date("Y-m-d H:i:s"),
                            'gate_updated_at' => date("Y-m-d H:i:s"),
                            'pickup_point' => $p_point->pickup_point
                        ]);
                        $orderlog = new Orderlog;
                        $orderlog->status = 'Gate Updated';
                        $orderlog->order_id = $order_id;
                        $orderlog->created_at = date("Y-m-d H:i:s");
                        $orderlog->updated_at = date("Y-m-d H:i:s");
                        $orderlog->save();
                    }
                }
            }
        }
        $gateUpdated = FALSE;
        if ($newGate && $newGate != $currentGate && $status != 'DELIVERED') {
            $gateUpdated = TRUE;
        }
        // ------ gate update logic ends here ------|
        $text = '';
        $timestamp = date('Y-m-d H:i:s');
        $orderlog = new Orderlog;
        if ($status != 'DESTINATION') {
            if ($status == 'DELIVERED') {
                $response = DB::table('order')->where('id', '=', $order_id)->update([
                    'status' => $status,
                    'pickup_reached_at' => '',
                    'delivered_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s"),
                ]);
                $title = 'Notification';
                $invID = str_pad($order_id, 5, '0', STR_PAD_LEFT);
                $invID = "CON".$invID;
                $message = 'Order Id '.$invID.' has been delivered';
                $userDetails = DB::table('users')->select('fcm_token')->where('id', '=', $order->users_id)->first();
                $this->notification_add($order->users_id, $message, $title, $userDetails->fcm_token, $order_id);  
            } else {
                $order_data_notification = DB::table('order')->where('id', '=', $order_id)->first();
                $response = DB::table('order')->where('id', '=', $order_id)->update([
                    'status' => $status,
                    'pickup_reached_at' => '',
                    'updated_at' => date("Y-m-d H:i:s"),
                ]);
                $default_grabbit_location = default_grabbit_location();
                $notification_data = DB::select("select id as user_id, fcm_token  from `users` where `users`.`pickup_point` = :pickup_point", ['pickup_point' => $default_grabbit_location['location']]);
                $invID = str_pad($order_id, 5, '0', STR_PAD_LEFT);
                $invID = "CON" . $invID;
                if (!empty($notification_data)) {
                    foreach ($notification_data as $key => $value) {
                        $title = 'Notification';
                        $message = 'Order Id ' . $invID . ' ready for pickup';
                        if ($status == 'IN_KITCHEN') {
                            $message = 'Order Id ' . $invID . ' is in kitchen now';
                        }
                        if ($status == 'ORDER_CONFIRMED') {
                            $message = 'Order Id ' . $invID . ' is confirmed now';
                        }
                        $this->notification_add($value->user_id, $message, $title, $value->fcm_token, $order_id);
                    }
                }

                /*$notification_data = DB::select("select id as user_id, fcm_token  from `users` where `users`.`pickup_point` = :pickup_point",['pickup_point' => $order_data_notification->pickup_point]);
                if (!empty($notification_data)) {
                    foreach ($notification_data as $key => $value) {
                        $title = 'Notification';
                        $message = 'Order Id'.$invID.' ready for pickup';
                        if ($status == 'IN_KITCHEN') {
                            $message = 'Order Id '.$invID.' is in kitchen now';
                        }
                        if ($status == 'ORDER_CONFIRMED') {
                            $message = 'Order Id '.$invID.' is confirmed now';                            
                        }
                        $this->notification_add($value->user_id, $message, $title, $value->fcm_token, $order_id);
                    }
                }*/
            }
            $orderlog->status = modifyText($status);
        } else {
            if ($order->status == 'READY_FOR_PICKUP') {
                $text = $request->post('text');
                $response = DB::table('order')->where('id', '=', $order_id)->update([
                    'status' => 'READY_FOR_PICKUP',
                    'pickup_reached_at' => $text,
                    'updated_at' => date("Y-m-d H:i:s"),
                ]);
                $default_grabbit_location = default_grabbit_location();
                $invID = str_pad($order_id, 5, '0', STR_PAD_LEFT);
                $invID = "CON" . $invID;
                //$changed_pickup_point = str_replace("At ", "", $text);
                $order_data_notification = DB::table('order')->where('id', '=', $order_id)->first();
                $notification_data = DB::select("select id as user_id, fcm_token  from `users` where `users`.`pickup_point` = :pickup_point", ['pickup_point' => $order_data_notification->pickup_point]);
                if (!empty($notification_data)) {
                    foreach ($notification_data as $key => $value) {
                        $title = 'Notification';
                        $message = 'Order Id ' . $invID . ' ready for pickup at ' . $default_grabbit_location['location'];
                        $this->notification_add($value->user_id, $message, $title, $value->fcm_token, $order_id);
                    }
                }
                $orderlog->status = $text;
            } else {
                $default_grabbit_location = default_grabbit_location();
                $default_location = 'At ' . $default_grabbit_location['location'];
                echo json_encode(['message' => 'Order should be in ready for pickup or ' . $default_location . ' status to change to this status', 'status' => FALSE]);

                return;
            }
        }
        $orderlog->order_id = $order_id;
        $orderlog->created_at = $timestamp;
        $orderlog->updated_at = $timestamp;
        $orderlog->save();

        $order_data = DB::table('order')->select('users_id', 'flight_number', 'pickup_point')->where('id', '=', $order_id)->first();
        $user_data = DB::table('users')->select('phone_number', 'email', 'name')->where('id', '=', $order_data->users_id)->first();
        $invID = str_pad($order_id, 5, '0', STR_PAD_LEFT);
        $invID = "CON" . $invID;
        $pickup_point = $order_data->pickup_point;

        $pickup_point_text = "At " . $order_data->pickup_point;

        if ($status == 'DESTINATION' &&  $text != '' && $text == $pickup_point_text) {
            $order_items = DB::select("select p.name, o.gate as gate, oi.item_count,oi.grant_total,oi.sub_total,oi.tax_total
                            from `order` o
                            join order_items as oi on o.id = oi.order_id
                            join products as p on oi.products_id = p.id
                            where o.id = :id ", ['id' => $order_id]);
            $items = '';
            $sub_total = '0';
            $grant_total = '0';
            $maplink = '';
            if (!empty($order_items)) {
                foreach ($order_items as $key => $value) {
                    $unit_price = round($value->grant_total / $value->item_count, 2);
                    $unit_price_modified = decimal_format_custom($unit_price);
                    $sub_total += $value->sub_total;
                    $grant_total += $value->grant_total;

                    if ($value->gate == '-' || $value->gate == '') {
                        // Destination reached but no gate. Then update gate to lift.
                        $value->gate = 'Lift';
                        $maplink = url('/maps/concourseb.html?from=' . $value->gate . '&to=' . $pickup_point);
                    } else {
                        $first_char = strtolower(substr($value->gate, 0, 1));
                        $file = ($first_char == 'b' || $first_char == 'c') ? "concourse$first_char" : 'index';
                        $maplink = url("/maps/$file.html?from=" . $value->gate . '&to=' . $pickup_point);
                    }
                }
            }
            $maplink_short = url('/m/' . $this->link_store($maplink));
            $tax_total = $grant_total - $sub_total;
            $sub_total_modified = decimal_format_custom($sub_total);
            $grant_total_modified = decimal_format_custom($grant_total);
            $tax_total_modified = decimal_format_custom($tax_total);

            $template = DB::select("select template from sms_template where `type` = 'READY_FOR_PICKUP'");
            $message = '';
            $findedGate = $this->findGate($gateFind);
            $gateToShow = $this->findGateReminder($gateFind);
            if (!empty($template)) {
                $sms_template = $template[0]->template;
                $message = str_replace("{{ orderId }}", $invID, $sms_template);
                $message = str_replace("{{ pickupPoint }}", $pickup_point, $message);
                $message = str_replace("{{ maplinkShort }}", $maplink_short, $message);

                $message = str_replace("{{ nearest_gate }}", $gateToShow, $message);
                $message = str_replace("{{ short_link_text }}", $findedGate, $message);
            }
            // $message = 'Your order '.$invID.' is ready for pick up at '.$pickup_point.'! Find your way: '.$maplink_short;
            $this->sendSMS($message, $user_data->phone_number);

            $title = 'Notification';
            $message = 'Order Id '.$invID.' is ready for pickup';
            $userDetails = DB::table('users')->select('fcm_token')->where('id', '=', $order->users_id)->first();
            $this->notification_add($order->users_id, $message, $title, $userDetails->fcm_token, $order_id);

          $orderid = $order_id;
                $result = DB::select('select `order`.`id` as orderid, '
                    . '`order_items`.`id` as orderitemid, '
                    . '`order`.`item_count` as item_count_total,'
                    . '`order`.`created_at` as created_at,'
                    . '`order`.`order_at` as order_at,'
                    . '`order`.flight_number as flight_no,'
                    . '`order`.gate as gate,'
                    . '`order`.pickup_reached_at as pickup_reached_at,'
                    . '`order`.pickup_point as boarding_point,'
                    . '`order`.`status` as order_status,'
                    . ' `order`.`grant_total` as grant_total,'
                    . ' `order`.`sub_total` as sub_total,'
                    . ' `order`.`tax_total` as tax_total,'
                    . ' `order`.`qr_code` as qr_code,'
                    . ' order_items.item_count as item_count,'
                    . ' order_items.products_id as products_id,'
                    . ' order_items.grant_total as grant_item_price,'
                    . ' order_items.sub_total as sub_item_price,'
                    . ' order_items.tax_total as tax_item_price,'
                    . ' products.name as productname,'
                    . ' products.actual_price as actual_price,'
                    . ' products.description as productdescription,'
                    . ' product_images.path as productthumbnail,'
                    . ' ado.name as addon_name, ado.actual_price as addon_price, aoi.item_count as addon_item_count'
                    . ' from `order` join order_items on `order`.`id` = order_items.order_id'
                    . ' join products on `products`.`id` = order_items.products_id '
                    . ' left join product_images on `products`.`id` = product_images.products_id and product_images.type = "THUMBNAIL"'
                    . ' left join addons_order_items as aoi on order_items.id = aoi.order_items_id'
                    .  ' left join addons as ado on aoi.addons_id = ado.id'
                    . ' where `order`.`id` = :orderid '
                    . '  order by `order`.`id` desc', ['orderid' => $orderid]);

                $modifiedResult = array();
                $items = '';
                $sub_total = '0.00';
                $grant_total = '0.00';
                if (!empty($result)) {
                    $i = 0;
                    foreach ($result as $key => $value) {
                        if ($i == 0) {
                            $modifiedResult = array(
                                'order_id' => $value->orderid,
                                'created_at' => $value->created_at,
                                'order_at' => $value->order_at,
                                'flight_no' => $value->flight_no,
                                'qr_code' => $value->qr_code,
                                'boarding_point' => $value->boarding_point,
                                'order_status' => str_replace("_", " ", $value->order_status),
                                'item_count_total' => $value->item_count_total,
                                'grant_total' => decimal_format_custom($value->grant_total),
                                'sub_total' => decimal_format_custom($value->sub_total),
                                'tax_total' => decimal_format_custom($value->tax_total)
                            );
                            $grant_total = $value->grant_total;
                            $sub_total = $value->sub_total;
                        }

                        if (!isset($modifiedResult['items'][$value->orderitemid])) {
                            $modifiedResult['items'][$value->orderitemid] = array(
                                'item_count' => $value->item_count,
                                'grant_item_price' => decimal_format_custom($value->grant_item_price),
                                'actual_price' => decimal_format_custom($value->actual_price),
                                'sub_item_price' => decimal_format_custom(($value->sub_item_price * $value->item_count)),
                                'tax_item_price' => decimal_format_custom($value->tax_item_price),
                                'products_id' => $value->products_id,
                                'productname' => $value->productname,
                                'productthumbnail' => empty($value->productthumbnail) ? $value->productthumbnail : url($value->productthumbnail),
                                'productdescription' => $value->productdescription,
                            );
                            if (!empty($value->addon_name)) {
                                $modifiedResult['items'][$value->orderitemid]['addons'][] = array(
                                    'addon_name' => $value->addon_name,
                                    'addon_price' => $value->addon_price,
                                    'addon_item_count' => $value->addon_item_count
                                );
                            } else {
                                $modifiedResult['items'][$value->orderitemid]['addons'] = array();
                            }
                        } else {
                            if (!empty($value->addon_name)) {
                                $modifiedResult['items'][$value->orderitemid]['addons'][] = array(
                                    'addon_name' => $value->addon_name,
                                    'addon_price' => $value->addon_price,
                                    'addon_item_count' => $value->addon_item_count
                                );
                            } else {
                                $modifiedResult['items'][$value->orderitemid]['addons'] = array();
                            }
                        }
                        $i++;
                    }
                }

                $addon_total = "0.00";
                $product_total = "0.00";

                if (!empty($modifiedResult)) {
                    foreach ($modifiedResult['items'] as $key => $value) {
                        $grant_item_price = $value["actual_price"] * $value["item_count"];
                        $product_total += $grant_item_price; 
                        $items .= '<tr>
                        <td style="background-color: #ffffff; color: #3f2d26; text-align: left;">
                        <h4>' . $value["productname"] . '</h4>
                        </td>
                        <td style="background-color: #ffffff; color: #3f2d26; text-align: left;">
                        AED ' . $value["actual_price"] . '
                        </td>
                        <td style="background-color: #ffffff; color: #3f2d26; text-align: left;">
                        ' . $value["item_count"] . '
                        </td>
                        <td style="background-color: #ffffff; color: #3f2d26; text-align: left;">
                        AED ' . $grant_item_price . '
                        </td>
                </tr>';
                        if (count($value['addons']) > 0) {
                            $items .= '<tr>
                            <td style="background-color: #ffffff; color: #3f2d26; text-align: left;"><h5>Addons</h5></td>
                        </tr>';
                            foreach ($value['addons'] as $addons) {
                                if (!empty($addons['addon_item_count'])) {
                                    $addonName = !empty($addons['addon_name']) ? $addons['addon_name'] : 'Nil';
                                    $addonPrice = !empty($addons['addon_price']) ? 'AED ' . $addons['addon_name'] : '0';
                                    $addonItemCount = !empty($addons['addon_item_count']) ?  $addons['addon_item_count'] : '0';
                                    $addon_total += ($addons['addon_item_count'] * $addons['addon_price']);
                                    $addonTotal = 'AED ' . $addons['addon_item_count'] * $addons['addon_price'];
                                    $items .= "<tr>
                                    <td style='background-color: #ffffff; color: #3f2d26; text-align: left;'>
                                        " . $addonName . "
                                    </td>
                                    <td style='background-color: #ffffff; color: #3f2d26; text-align: left;'>
                                       " . $addonPrice . "
                                    </td>
                                    <td style='background-color: #ffffff; color: #3f2d26; text-align: left;'>
                                        " . $addonItemCount . "
                                    </td>
                                    <td style='background-color: #ffffff; color: #3f2d26; text-align: left;'>
                                       " . $addonTotal . "
                                    </td>
                            </tr>";
                                }
                            }
                        }
                    }
                }
                $grant_total = $product_total + $addon_total; 
                $item_content = '<table width="100%" cellspacing="0" cellpadding="10" border="0" style=" font-size:13px;">
									<thead>
										<tr>
										<th style="background-color: #f16521; color: #fff; text-align: left;">
										<font face="arial">PRODUCT NAME</font>
										</th>
										<th style="background-color: #f16521; color: #fff; text-align: left;">
										<font face="arial">UNIT PRICE</font>
										</th>
										<th style="background-color: #f16521; color: #fff; text-align: left;">
										<font face="arial">QUANTITY</font>
										</th>
										<th style="background-color: #f16521; color: #fff; text-align: left;">
										<font face="arial">AMOUNT</font>
                                        </th>
									</tr>
									</thead>
									<tbody>
                                                                         ' . $items . '   
									</tbody>
									<tfoot>
										<tr>
											<td></td>
											<td colspan="2" style="font-size:14px;">
											<font face="arial" style="color: #000000;">Total (Including VAT): </font>
											</td>
											<td style="font-size:15px;color: #000000;">
											<font face="arial"><small style="font-size:11px;">AED</small> <strong>' . $grant_total . '</strong></font>
										</td>
									</tr>
									</tfoot>
                                </table>';
            try {
                //                    $data = ['subject' => 'Thank you for choosing Carry On as your extra baggage!', 'cust_name' => $user_data->name,
                //                    'nearest_gate' => $gateToShow, 'short_link_text' => $findedGate,
                //                    'order_id' => $invID, 'pickup_point' => $pickup_point, 'item_content' => $item_content, 'maplink' => $maplink];
                $subject_data = DB::select("select template from sms_template where `type` = :type", ['type' => 'EMAIL_SUBJECT_READYFORPICKUP']);
                $subject = $subject_data[0]->template;
                $data = [
                    'subject' => $subject, 'cust_name' => $user_data->name,
                    'nearest_gate' => $gateToShow, 'short_link_text' => $findedGate,
                    'order_id' => $invID, 'pickup_point' => $pickup_point, 'item_content' => $item_content, 'maplink' => $maplink
                ];

                Mail::to($user_data->email)->send(new OrderReadyForPickup($data));
            } catch (\Exception $e) {
                print_r($e->getMessage());
            }
        }

        echo json_encode(['message' => 'Status updated', 'gate_updated' => $gateUpdated, 'status' => TRUE]);

        return;
    }

    function findGate($gateFind)
    {
        $gate2Find = strtolower($gateFind);
        if (strpos($gate2Find, 'grabbit') !== false) {
            return $gateFind . ': Gate B7';
        }
        if (strpos($gate2Find, 's34') !== false) {
            return $gateFind . ': Gate C36';
        }
        if (strpos($gate2Find, 'nutella') !== false) {
            return $gateFind . ': Gate B28';
        }
        if (strpos($gate2Find, 'fix') !== false) {
            return $gateFind . ': Gate A17';
        }
        if (strpos($gate2Find, 'tree') !== false) {
            return $gateFind . ': Gate A12';
        }

        return '';
    }

    function findGateReminder($gateFind)
    {
        $gate2Find = strtolower($gateFind);
        if (strpos($gate2Find, 'grabbit') !== false) {
            return 'B7';
        }
        if (strpos($gate2Find, 's34') !== false) {
            return 'C36';
        }
        if (strpos($gate2Find, 'nutella') !== false) {
            return 'B28';
        }
        if (strpos($gate2Find, 'fix') !== false) {
            return 'A17';
        }
        if (strpos($gate2Find, 'tree') !== false) {
            return 'A12';
        }

        return '';
    }

    function sendSMS($message, $phonenumber)
    {
        $message = urlencode($message);
        preg_match_all('!\d+!', $phonenumber, $matches);
        $phone = implode('', $matches[0]);
        $url = "http://mshastra.com/sendurlcomma.aspx?user=20093644&pwd=Emirates321!&senderid=CarryonDXB&mobileno=" . $phone . "&msgtext=" . $message . "&priority=High&CountryCode=ALL";



        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch);
        // echo $curl_scraped_page;
    }

    function sendMail($message, $email)
    {
        try {
            $data = ['from_name' => 'CARRYON', 'from_email' => 'noreply@carryon.com', 'subject' => 'Your CarryOn is ready!', 'message1' => $message, 'message2' => ''];

            Mail::to($email)->send(new TestEmail($data));
        } catch (\Exception $e) {
            print_r($e->getMessage());
        }
    }



    /**
     * Order report pdf.
     *
     * @return \Illuminate\Http\Response
     */
    public function pdfreport(Request $request)
    {
        $orderid = $request->get('orderid');
        $order = DB::table('order')->where('id', '=', $orderid)->first();
        // ------ gate update code starts----------|
        /*$order_at_data = DB::select("select DATE_FORMAT(`order`.`order_at`,'%Y/%c/%e') as order_at  from `order` where `order`.`id` = :order_id",['order_id' => $orderid]);
        $fn_prefix = substr($order->flight_number, 0, 2);
        $fn = substr($order->flight_number, 2);    
        $handle = curl_init();
        $url = "https://api.flightstats.com/flex/flightstatus/rest/v2/json/flight/status/".$fn_prefix."/".$fn."/dep/".$order_at_data[0]->order_at."?appId=8a5a55e4&appKey=b3e7538d084234f6f83779bdfbd671ed&utc=false&airport=DXB";
        curl_setopt($handle, CURLOPT_URL, $url);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_HTTPHEADER, array('DY-X-Authorization: 3b75a0bdfc7aa9d383e3f8beef0f2b623a94335c'));
        $output = curl_exec($handle);
        if ( ! curl_errno($handle)) {
            curl_close($handle);
            $output = json_decode($output);
            if (isset($output->flightStatuses[0]->airportResources->departureGate)) {
                $gate = $output->flightStatuses[0]->airportResources->departureGate;
                if($gate) {
                    $p_point = DB::table('boarding_pickup_map')->select('pickup_point')->where('gate', '=', $gate)->first();
                    $response = DB::table('order')->where('id', '=', $orderid)->update([
                        'gate' => $gate,
                        'updated_at' => date("Y-m-d H:i:s"),
                        'gate_updated_at' => date("Y-m-d H:i:s"),
                        'pickup_point' => $p_point->pickup_point
                    ]);
                    $orderlog = new Orderlog;
                    $orderlog->status = 'Gate Updated';
                    $orderlog->order_id = $orderid;
                    $orderlog->created_at = date("Y-m-d H:i:s");
                    $orderlog->updated_at = date("Y-m-d H:i:s");
                    $orderlog->save();
                }
            }
        }*/

        $result = DB::select('select `order`.`id` as orderid, '
            . '`order_items`.`id` as orderitemid, `order`.`users_id` as users_id,  '
            . '`order`.`item_count` as item_count_total,'
            . '`order`.`created_at` as created_at,'
            . '`order`.`order_at` as order_at,'
            . '`order`.flight_number as flight_no,'
            . '`order`.gate as gate,'
            . '`order`.pickup_reached_at as pickup_reached_at, `order`.pickup_time as pickup_time, '
            . '`order`.pickup_point as boarding_point,'
            . '`order`.`status` as order_status,'
            . ' `order`.`grant_total` as grant_total,'
            . ' `order`.`sub_total` as sub_total,'
            . ' `order`.`tax_total` as tax_total,'
            . ' `order`.`qr_code` as qr_code,'
            . ' order_items.item_count as item_count,'
            . ' order_items.products_id as products_id,'
            . ' order_items.grant_total as grant_item_price,'
            . ' order_items.sub_total as sub_item_price,'
            . ' order_items.tax_total as tax_item_price,'
            . ' products.name as productname,'
            . ' products.actual_price as actual_price,'
            . ' products.description as productdescription,'
            . ' product_images.path as productthumbnail,'
            . ' ado.name as addon_name, ado.actual_price as addon_price, aoi.item_count as addon_item_count'
            . ' from `order` join order_items on `order`.`id` = order_items.order_id'
            . ' join products on `products`.`id` = order_items.products_id '
            . ' left join product_images on `products`.`id` = product_images.products_id and product_images.type = "THUMBNAIL"'
            . ' left join addons_order_items as aoi on order_items.id = aoi.order_items_id'
            .  ' left join addons as ado on aoi.addons_id = ado.id'
            . ' where  `order`.`id` = :orderid '
            . '  order by `order`.`id` desc', ['orderid' => $orderid]);
        $modifiedResult = array();
        if (!empty($result)) {
            $i = 0;
            foreach ($result as $key => $value) {
                if ($i == 0) {
                    $modifiedResult = array(
                        'order_id' => $value->orderid,
                        'users_id' => $value->users_id,
                        'pickup_time' => $value->pickup_time,
                        'created_at' => $value->created_at,
                        'order_at' => $value->order_at,
                        'flight_no' => $value->flight_no,
                        'qr_code' => $value->qr_code,
                        'boarding_point' => $value->boarding_point,
                        'order_status' => str_replace("_", " ", $value->order_status),
                        'item_count_total' => $value->item_count_total,
                        'grant_total' => decimal_format_custom($value->grant_total),
                        'sub_total' => decimal_format_custom($value->sub_total),
                        'tax_total' => decimal_format_custom($value->tax_total)
                    );
                }

                if (!isset($modifiedResult['items'][$value->orderitemid])) {
                    $grant_item_price = $value->actual_price * $value->item_count;
                    $modifiedResult['items'][$value->orderitemid] = array(
                        'item_count' => $value->item_count,
                        'grant_item_price' => decimal_format_custom($grant_item_price),
                        'actual_price' => decimal_format_custom($value->actual_price),
                        'sub_item_price' => decimal_format_custom(($value->sub_item_price * $value->item_count)),
                        'tax_item_price' => decimal_format_custom($value->tax_item_price),
                        'products_id' => $value->products_id,
                        'productname' => $value->productname,
                        'productthumbnail' => empty($value->productthumbnail) ? $value->productthumbnail : url($value->productthumbnail),
                        'productdescription' => $value->productdescription,
                    );
                    if (!empty($value->addon_name)) {
                        $modifiedResult['items'][$value->orderitemid]['addons'][] = array(
                            'addon_name' => $value->addon_name,
                            'addon_price' => decimal_format_custom($value->addon_price),
                            'addon_item_count' => $value->addon_item_count
                        );
                    } else {
                        $modifiedResult['items'][$value->orderitemid]['addons'] = array();
                    }
                } else {
                    if (!empty($value->addon_name)) {
                        $modifiedResult['items'][$value->orderitemid]['addons'][] = array(
                            'addon_name' => $value->addon_name,
                            'addon_price' => decimal_format_custom($value->addon_price),
                            'addon_item_count' => $value->addon_item_count
                        );
                    } else {
                        $modifiedResult['items'][$value->orderitemid]['addons'] = array();
                    }
                }
                $i++;
            }
        }
        $content = '';
        if (!empty($modifiedResult)) {
            foreach ($modifiedResult['items'] as $key => $value) {
                //if ($key % 3 == 0) $content .= "<div style='page-break-after: always;'></div>";
                $content .= '<tr>
                                <td class="description">' . $value["productname"] . '</td>
                                <td class="quantity" style="text-align:center;">' . $value["item_count"] . '</td>
                                <td class="price" style="text-align:left;">' . $value["actual_price"] . '</td>
                                <td class="price" style="text-align:left;">' . $value["grant_item_price"] . '</td>
                            </tr>';
                if (count($value['addons']) > 0) {
                    $content .= '<tr>
                            <td colspan="4" >Addons</td>
                        </tr>';
                    foreach ($value['addons'] as $addons) {
                        if (!empty($addons['addon_item_count'])) {
                            $addonsName = !empty($addons['addon_name']) ? $addons['addon_name'] : 'Nil';
                            $addonsPrice = !empty($addons['addon_price']) ? '' . $addons['addon_price'] : 0;
                            $addonsItemcount = !empty($addons['addon_item_count']) ? $addons['addon_item_count'] : 0;
                            $addonsTotal = '' . $addons['addon_price'] *  $addons['addon_item_count'];
                            $content .= '<tr>
                                <td class="description">
                                    &nbsp;&nbsp;&nbsp;&nbsp;' . $addonsName . '
                                </td>
                                <td class="quantity" style="text-align:center;">
                                    ' . $addonsItemcount  . '
                                </td>
                                <td class="price" style="text-align:left;">
                                    ' . $addonsPrice . '
                                </td>
                                <td class="price" style="text-align:left;">
                                    ' . $addonsTotal . '
                                </td>
                        </tr>';
                        }
                    }
                }
            }
        }
        $user_id = $modifiedResult['users_id'];

        $user_data = DB::table('users')->select('phone_number', 'email', 'name', 'billing_address')->where('id', '=', $user_id)->first();
        $invID = str_pad($orderid, 5, '0', STR_PAD_LEFT);
        $invID = "CON" . $invID;
        $order_at = $modifiedResult['order_at'] ?? '';
        if (!empty($order_at)) {
            $date = date_create($order_at);
            $order_at = date_format($date, "d-M-Y");
        }

        $custome_date = date_format_from_month_custom($modifiedResult['pickup_time']);
        $custome_date_array = explode(" ", $custome_date);
        if (!empty($md->$key)) {
            $dispatch_time = $custome_date_array[0] . " " . $custome_date_array[1] . " <b>" . $custome_date_array[2] . "</b>";
        } else {
            $dispatch_time = $custome_date;
        }

        $data = [
            'title' => 'Welcome to carryon', 'invID' => $invID, 'customername' => $user_data->name,
            'qr_code' => $modifiedResult['qr_code'],
            'content' => $content, 'total' => $modifiedResult['grant_total'],
            'order_at' => $order_at, 'billing_address' => $user_data->billing_address,
            'pickup_loc' => $modifiedResult['boarding_point'], 'flight_no' => $modifiedResult['flight_no'], 'dispatch_time' => $dispatch_time
        ];
        $customPaper = array(0, 0, 567.00, 283.80);
        $pdf = PDF::loadView('pdf.invoice', $data)->setPaper($customPaper, 'landscape');

        // return $pdf->download('carryon' . time() . '.pdf');
        $folder_structure = date('Y') . "/" . date('m') . "/" . date('d');
        // $target_dir = "./public/upload/" . $folder_structure;
        $target_dir = "./upload/" . $folder_structure;
        $target_dir_path = "./upload/" . $folder_structure;
        if (!file_exists($target_dir)) {
            mkdir($target_dir, 0777, true);
        }
        $file = $target_dir . '/invoice_' . $orderid . '.pdf';
        $filepath = $target_dir_path . '/invoice_' . $orderid . '.pdf';
        $pdf->save($file);
        $returnData = array("message" => "Order List", "filepath" => url($filepath));
        echo json_encode($returnData);
        exit;
    }

    public function cancel(Request $request)
    {
        $order_id = $request->post('id');
        $order_details = DB::table('order')->select('order_at', 'pickup_point')->where('id', '=', $order_id)->first();
        if (!$order_details) {
            echo json_encode([
                'status' => FALSE,
                'message' => 'Order not found!'
            ]);
            return;
        }

        DB::table('order')->where('id', '=', $order_id)->update([
            'status' => 'CANCELLED',
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        $default_grabbit_location = default_grabbit_location();
        //$notification_data = DB::select("select id as user_id, fcm_token  from `users` where `users`.`pickup_point` = :pickup_point",['pickup_point' => $default_grabbit_location['location']]);
        $notification_data = DB::select("select id as user_id, fcm_token  from `users` where `users`.`pickup_point` = :pickup_point or `users`.`pickup_point`= :other_pickup_point", ['pickup_point' => $default_grabbit_location['location'], 'other_pickup_point' => $order_details->pickup_point]);
        $invID = str_pad($order_id, 5, '0', STR_PAD_LEFT);
        $invID = "CON" . $invID;
        if (!empty($notification_data)) {
            foreach ($notification_data as $key => $value) {
                $title = 'Notification';
                $message = 'Order Id ' . $invID . ' cancelled';
                $this->notification_add($value->user_id, $message, $title, $value->fcm_token, $order_id);
            }
        }


        echo json_encode([
            'status' => TRUE,
            'message' => 'order cancelled successfully'
        ]);
    }

    public function reimburse(Request $request)
    {
        $order_id = $request->post('id');
        $order_details = DB::table('order')->select('order_at', 'grant_total')->where('id', '=', $order_id)->first();
        if (!$order_details) {
            echo json_encode([
                'status' => FALSE,
                'message' => 'Order not found!'
            ]);
            return;
        }

        $paymentData = DB::table('payments')->select('reference_id')->where('order_ref', '=', $order_id)->first();
        if (!$paymentData) {
            echo json_encode([
                'status' => FALSE,
                'message' => 'Payment data missing!'
            ]);

            return;
        }

        $params = array(
            'ivp_method'  => 'check',
            'ivp_store'   => '22999',
            'ivp_authkey' => 'CzR46^fcVJ#C4gRP',
            'ivp_trantype' => 'refund',
            'ivp_currency' => 'AED',
            'ivp_amount' => $order_details->grant_total,
            'tran_ref' => $paymentData->reference_id,
            'ivp_tranclass' => 'ecom',
            'ivp_test' => 1
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://secure.telr.com/gateway/remote.html");
        curl_setopt($ch, CURLOPT_POST, count($params));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
        $results = curl_exec($ch);
        curl_close($ch);
        $results = json_decode($results, true);

        DB::table('order')->where('id', '=', $order_id)->update([
            'is_reimbursed' => 'YES',
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        echo json_encode([
            'status' => TRUE,
            'message' => 'order reimbursed successfully',
            'reimb' => $results
        ]);
    }

    public function link_store($long_link)
    {
        $input['link'] = $long_link;
        $input['code'] = Str::random(6);

        ShortLink::create($input);

        return $input['code'];
    }

    public function ordercount()
    {
        $user_id = Auth::user()->id;
        $pickup_point = DB::table('users')->select('is_default_pickup_person')->where('id', '=', $user_id)->first();
        $is_default_pickup_person = $pickup_point->is_default_pickup_person ?? '';
        $pickup_condition = '';
        if ($is_default_pickup_person == 'YES') {
            $pickup_condition = " ( `order`.pickup_reached_at is null OR `order`.pickup_reached_at='') ";
        } else {
            $default_grabbit_location = "At " . default_grabbit_location()['location'];
            $pickup_condition = " ( `order`.pickup_reached_at='$default_grabbit_location') ";
        }

        $database_name = env('DB_DATABASE');
        $model = DB::select("select count(*) as cnt from $database_name.order");
        $model_kitchen = DB::select("select count(*) as cnt from $database_name.order where $database_name.order.status='IN_KITCHEN'");
        $model_pickup = DB::select("select count(*) as cnt from $database_name.order where $database_name.order.status='READY_FOR_PICKUP' AND $pickup_condition");
        $status = array('count' => $model[0]->cnt, 'kitchen_count' => $model_kitchen[0]->cnt, 'pickup_count' => $model_pickup[0]->cnt);
        echo json_encode($status);
    }

    public function change_pl_status(Request $request)
    {
        $order_id = $request->post('order_id');
        $pickup = $request->post('pickup');
        $pickup_at = 'At ' . $pickup;
        $order_details = DB::table('order')->select('order_at', 'users_id', 'status', 'pickup_reached_at', 'pickup_point')->where('id', '=', $order_id)->first();
        if (!$order_details) {
            echo json_encode([
                'status' => FALSE,
                'message' => 'Order not found!'
            ]);
            return;
        }
        if ($pickup == trim($order_details->pickup_point)) {
            echo json_encode([
                'status' => FALSE,
                'message' => 'Trying to update to same pickup point!'
            ]);
            return;
        }

        $invID = str_pad($order_id, 5, '0', STR_PAD_LEFT);
        $invID = "CON" . $invID;

        $title = 'Notification';
        $message = 'Pickup point of Order Id '.$invID.' is changed to '.$pickup;
        $userDetails = DB::table('users')->select('fcm_token')->where('id', '=', $order_details->users_id)->first();
        $this->notification_add($order_details->users_id, $message, $title, $userDetails->fcm_token, $order_id);

        $default_grabbit_location = default_grabbit_location();
        if ($default_grabbit_location['location'] != $pickup) {
            $notification_data = DB::select("select id as user_id, fcm_token  from `users` where `users`.`pickup_point` = :pickup_point", ['pickup_point' => $pickup]);
            if (!empty($notification_data)) {
                foreach ($notification_data as $key => $value) {
                    $title = 'Notification';
                    $message = 'Pickup point of Order Id ' . $invID . ' is changed to ' . $pickup;
                    $this->notification_add($value->user_id, $message, $title, $value->fcm_token, $order_id);
                }
            }
        }
        $notification_data = DB::select("select id as user_id, fcm_token  from `users` where `users`.`pickup_point` = :pickup_point", ['pickup_point' => $default_grabbit_location['location']]);
        if (!empty($notification_data)) {
            foreach ($notification_data as $key => $value) {
                $title = 'Notification';
                $message = 'Pickup point of Order Id ' . $invID . ' is changed to ' . $pickup;
                $this->notification_add($value->user_id, $message, $title, $value->fcm_token, $order_id);
            }
        }
        if ($order_details->status == 'READY_FOR_PICKUP' && trim($order_details->pickup_reached_at) == '') {
            if (!empty($notification_data)) {
                foreach ($notification_data as $key => $value) {
                    $title = 'Notification';
                    $message = 'Order Id ' . $invID . ' ready for pickup';
                    $this->notification_add($value->user_id, $message, $title, $value->fcm_token, $order_id);
                }
            }
        }

        if ($order_details->status == 'READY_FOR_PICKUP' && trim($order_details->pickup_reached_at) != '' && trim($order_details->pickup_reached_at) == $pickup_at) {
            $notification_data = DB::select("select id as user_id, fcm_token  from `users` where `users`.`pickup_point` = :pickup_point", ['pickup_point' => $pickup]);
            if (!empty($notification_data)) {
                foreach ($notification_data as $key => $value) {
                    $title = 'Notification';
                    $message = 'Order Id ' . $invID . ' ready for pickup';
                    $this->notification_add($value->user_id, $message, $title, $value->fcm_token, $order_id);
                }
            }
        }

        DB::table('order')->where('id', '=', $order_id)->update([
            'pickup_point' => $pickup,
            'gate' => '.' // '.' added to skip update by cron
        ]);

        echo json_encode([
            'status' => TRUE,
            'message' => 'Pickup location successfully'
        ]);
    }
}
