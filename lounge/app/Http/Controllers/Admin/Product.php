<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Product extends Controller
{

    public function __construct()
    {
        $this->middleware(['permission:products_view']);
    }

    public function test() {
        $permissions_data = user_permissions(Auth::user());
        echo json_encode(['data' => $permissions_data, 'status' => TRUE]);

        return;
    }

    public function products() {
        $permissions_data = user_permissions(Auth::user());
        $incredients = DB::table('incredience')->select('*')->get();
        $allergies = DB::table('allergies')->select('*')->get();
        $doneness = DB::table('doneness')->select('*')->get();
        $addons = DB::table('addons')->select('*')->get();
        $allergies_filter = DB::table('allergies_filter')->select('*')->get();
        $categories = DB::table('category')->select('*')->get();
        $diets = DB::table('preferenceoptions')->select('*')->where('type', '=', 'Diet')->get();
        $intolerences = DB::table('preferenceoptions')->select('*')->where('type', '=', 'Intolerence')->get();
        $products = DB::table('products')->select('id','name','sku', 'actual_price', 'discount_price','description','visibility','position')->where('deleted_at', '=', NULL)->get();
        $allproducts = DB::table('products')->select('*')->get();

        return view('admin.products.products', [ 
            'products' => $products,
            'incredients' => $incredients,
            'allergies' => $allergies,
            'doneness' => $doneness,
            'addons' => $addons,
            'allergies_filter' => $allergies_filter,
            'categories' => $categories,
            'diets' => $diets,
            'allproducts' => $allproducts,
            'intolerences' => $intolerences,
            'permissions' => $permissions_data['permissions'],
            'user_has_roles' => $permissions_data['roles'],
            
        ]);
    }

    public function product_add(Request $request) {
        $product_name = $request->post('product_name');
        $base_content = $request->post('base_content');
        $product_sku = $request->post('product_sku');
        $product_actual_price = $request->post('product_actual_price');
        $product_discount_price = $request->post('product_discount_price');
        $product_description = $request->post('product_description');
        $product_allergies = $request->post('product_allergies');
        $product_doneness = $request->post('product_doneness');
        $product_diets = $request->post('product_diets');
        $product_intolerances = $request->post('product_intolerances');
        $product_addons = $request->post('product_addons');
        $product_allergies_filter = $request->post('product_allergies_filter');
        $product_incredients = $request->post('product_incredients');
        $product_categories = $request->post('product_categories');
        $product_url = $request->post('product_friendly_url');
        $product_visibility = $request->post('product_visibility');
        $product_featured = $request->post('product_featured');
        $product_position= $request->post('product_position');
        $product_related = $request->post('product_related');

        if( ! $product_name || ! $product_sku || ! $product_actual_price || ! $product_discount_price || ! $product_description
                 || ! $product_incredients || ! $product_categories || ! $product_url || !$product_visibility || !$product_featured || !$product_position || !$base_content ) {
            echo json_encode(['message'=> 'Provide all fields', 'status' => FALSE]);

            return;
        }

        $existCheck = DB::table('products')->where('sku', '=', $product_sku)->first();
        if ($existCheck !== null) {
            echo json_encode(['message'=> 'Sku already exists', 'status' => FALSE]);

            return;
        }

        $data = [
            'name' => $product_name, 
            'base_content' => $base_content,
            'sku' => $product_sku, 
            'actual_price' => $product_actual_price,
            'discount_price' => $product_discount_price,
            'url_key' => $product_url,
            'description'=>$product_description,
            'visibility'=>$product_visibility,
            'featured' => $product_featured,
            'position'=>$product_position,
        ];

        $product = DB::table('products')->insertGetId( $data );
        if( $product ) {
            if( $request->file('thumbnail_image') ) {
                $path = '/product_thumbnails';
                $imageName = time().'.'.$request->thumbnail_image->getClientOriginalExtension();
                $request->thumbnail_image->move(public_path($path), $imageName);
                DB::table('product_images')->insert(['path' => $path.'/'.$imageName, 'type' => 'THUMBNAIL', 'products_id' => $product ]);
            }
    
            if( $request->file('product_image') ) {
                $data = [];
                $incr = 0;
                $path = '/product_images';
                foreach( $request->file('product_image') as $img ) {
                    ++$incr;
                    $imageName = time().$incr.'.'.$img->getClientOriginalExtension();
                    $img->move(public_path($path), $imageName);
                    $data[] = [
                        'path' => $path.'/'.$imageName, 
                        'products_id' => $product 
                    ];
                }
                DB::table('product_images')->insert($data);
            }

            // Below product_tags
            $allergy_data = [];
            if($product_allergies) {
                foreach($product_allergies as $allergy_id) {
                    $allergy_data[] = [
                        'products_id' => $product,
                        'allergies_id' => $allergy_id
                    ];
                }
                $allrg = DB::table('product_allergies')->insert( $allergy_data );
            }
            
            // Below related products
            $product_related_data = [];
            if($product_related) {
                foreach($product_related as $product_related_id) {
                    $product_related_data[] = [
                        'products_id' => $product,
                        'product_rel_id' => $product_related_id
                    ];
                }
                $allrg = DB::table('product_relation')->insert( $product_related_data );
            }
            
            // Below product_diet
            $diet_data = [];
            if($product_diets) {
                foreach($product_diets as $diets_id) {
                    $diet_data[] = [
                        'products_id' => $product,
                        'preferenceoptions_id' => $diets_id,
                        'type' => 'Diet'
                    ];
                }
                $allrg = DB::table('product_preferenceoptions')->insert( $diet_data );
            }
            
            
            // Below product_intolerances
            $intolerance_data = [];
            if($product_intolerances) {
                foreach($product_intolerances as $intolerances_id) {
                    $intolerance_data[] = [
                        'products_id' => $product,
                        'preferenceoptions_id' => $intolerances_id,
                        'type' => 'Intolerence'
                    ];
                }
                $allrg = DB::table('product_preferenceoptions')->insert( $intolerance_data );
            }
            
            // Below product_addons
            $addon_data = [];
            if($product_addons) {
                foreach($product_addons as $addon_id) {
                    $addon_data[] = [
                        'products_id' => $product,
                        'addons_id' => $addon_id
                    ];
                }
                $allrg = DB::table('addons_product_mapping')->insert( $addon_data );
            }

             // Below product_doneness
             $doneness_data = [];
             if($product_doneness) {
                 foreach($product_doneness as $doneness_id) {
                     $doneness_data[] = [
                         'products_id' => $product,
                         'doneness_id' => $doneness_id
                     ];
                 }
                 $allrg = DB::table('product_doneness')->insert( $doneness_data );
             }
            
            // Below product_allergies
            $allergy_filter_data = [];
            if($product_allergies_filter) {
                foreach($product_allergies_filter as $allergy_filter_id) {
                    $allergy_filter_data[] = [
                        'products_id' => $product,
                        'allergies_filter_id' => $allergy_filter_id
                    ];
                }
                $allrg_filter = DB::table('product_allergies_filter')->insert( $allergy_filter_data ); 
            }           
            // Below product_categories
            $category_data = [];
            foreach($product_categories as $cat_id) {
                $category_data[] = [
                    'products_id' => $product,
                    'category_id' => $cat_id
                ];
            }
            $cat = DB::table('product_category')->insert( $category_data );
            // Below product_incredients
            $incredient_data = [];
            foreach($product_incredients as $incr_id) {
                $incredient_data[] = [
                    'products_id' => $product,
                    'incredience_id' => $incr_id
                ];
            }
            $incrdnts = DB::table('product_incredience')->insert( $incredient_data );
        }
        if( $product ) {
            echo json_encode(['message'=> 'Product Added Successfully', 'status' => TRUE]);
        } else {
            echo json_encode(['message'=> 'Something went wrong', 'status' => FALSE]);
        }
        
        return;
    }

    public function product_edit(Request $request) {
        $product_id = $request->post('product_id');
        $product_name = $request->post('product_name');
        $base_content = $request->post('base_content');
        $product_sku = $request->post('product_sku');
        $product_actual_price = $request->post('product_actual_price');
        $product_discount_price = $request->post('product_discount_price');
        $product_description = $request->post('product_description');
        $product_allergies = $request->post('product_allergies');
        $product_doneness = $request->post('product_doneness');
        $product_diets = $request->post('product_diets');
        $product_intolerances = $request->post('product_intolerances');
        $product_addons = $request->post('product_addons');
        $product_allergies_filter = $request->post('product_allergies_filter');
        $product_incredients = $request->post('product_incredients');
        $product_categories = $request->post('product_categories');
        $product_url = $request->post('product_friendly_url');
        $product_visibility = $request->post('product_visibility');
        $product_featured = $request->post('product_featured');
        $product_position= $request->post('product_position'); 
        $product_related = $request->post('product_related');

        if( ! $product_name || ! $product_sku || ! $product_actual_price || ! $product_discount_price ||! $product_description||
                 ! $product_incredients || ! $product_categories || ! $product_url || !$product_visibility || !$product_featured || !$product_position  || !$base_content ) {
            echo json_encode(['message'=> 'Provide all fields', 'status' => FALSE]);

            return;
        }

        $existCheck = DB::table('products')->where('id', '!=', $product_id)->where('sku', '=', $product_sku)->first();
        if ($existCheck !== null) {
            echo json_encode(['message'=> 'Sku already exists', 'status' => FALSE]);

            return;
        }

        $data = [
            'name' => $product_name, 
            'base_content' => $base_content,
            'sku' => $product_sku, 
            'actual_price' => $product_actual_price,
            'discount_price' => $product_discount_price,
            'description'=>$product_description,
            'visibility'=>$product_visibility,
            'featured' => $product_featured,
            'position'=>$product_position,
        ];

        $response = DB::table('products')->where('id', '=', $product_id)->update([
            'name' => $product_name,
            'base_content' => $base_content,
            'sku' => $product_sku,
            'actual_price' => $product_actual_price,
            'discount_price' => $product_discount_price,
            'url_key' => $product_url,
            'description'=>$product_description,
            'visibility'=>$product_visibility,
            'featured' => $product_featured,
            'position'=>$product_position,
        ]);

        if( $request->file('thumbnail_image') ) {
            $path = '/product_thumbnails';
            $imageName = time().'.'.$request->thumbnail_image->getClientOriginalExtension();
            $request->thumbnail_image->move(public_path($path), $imageName);
            DB::table('product_images')->where('products_id', '=', $product_id)->where('type', '=', 'THUMBNAIL')->delete();
            DB::table('product_images')->insert(['path' => $path.'/'.$imageName, 'type' => 'THUMBNAIL', 'products_id' => $product_id ]);
        }

        if( $request->file('product_image') ) {
            $data = [];
            $incr = 0;
            $path = '/product_images';
            foreach( $request->file('product_image') as $img ) {
                ++$incr;
                $imageName = time().$incr.'.'.$img->getClientOriginalExtension();
                $img->move(public_path($path), $imageName);
                $data[] = [
                    'path' => $path.'/'.$imageName, 
                    'products_id' => $product_id 
                ];
            }
            DB::table('product_images')->insert($data);
        }

        if( true ) {
            // Below product_allergies
            $allergy_data = [];
            DB::table('product_allergies')->where('products_id', $product_id)->delete();
            if($product_allergies){
                foreach($product_allergies as $allergy_id) {
                    $allergy_data[] = [
                        'products_id' => $product_id,
                        'allergies_id' => $allergy_id
                    ];
                }
                $allrg = DB::table('product_allergies')->insert( $allergy_data );
            }
            
            DB::table('product_relation')->where('products_id', $product_id)->delete();
            // Below related products
            $product_related_data = [];
            if($product_related) {
                foreach($product_related as $product_related_id) {
                    $product_related_data[] = [
                        'products_id' => $product_id,
                        'product_rel_id' => $product_related_id
                    ];
                }
                $allrg = DB::table('product_relation')->insert( $product_related_data );
            }
 
            
            // Below product_diet
            DB::table('product_preferenceoptions')->where('products_id', $product_id)->delete();
            $diet_data = [];
            if($product_diets) {
                foreach($product_diets as $diets_id) {
                    $diet_data[] = [
                        'products_id' => $product_id,
                        'preferenceoptions_id' => $diets_id,
                        'type' => 'Diet'
                    ];
                }
                $allrg = DB::table('product_preferenceoptions')->insert( $diet_data );
            }
            
            
            // Below product_intolerances
            $intolerance_data = [];
            if($product_intolerances) {
                foreach($product_intolerances as $intolerances_id) {
                    $intolerance_data[] = [
                        'products_id' => $product_id,
                        'preferenceoptions_id' => $intolerances_id,
                        'type' => 'Intolerence'
                    ];
                }
                $allrg = DB::table('product_preferenceoptions')->insert( $intolerance_data );
            }
 
            
            // Below product_addons
            $addon_data = [];
            DB::table('addons_product_mapping')->where('products_id', $product_id)->delete();
            if($product_addons) {
                foreach($product_addons as $addon_id) {
                    $addon_data[] = [
                        'products_id' => $product_id,
                        'addons_id' => $addon_id
                    ];
                }
                $allrg = DB::table('addons_product_mapping')->insert( $addon_data );
            }

            // Below product_doneness
            $addon_data = [];
            DB::table('product_doneness')->where('products_id', $product_id)->delete();
            if($product_doneness) {
                foreach($product_doneness as $doneness_id) {
                    $doneness_data[] = [
                        'products_id' => $product_id,
                        'doneness_id' => $doneness_id
                    ];
                }
                $allrg = DB::table('product_doneness')->insert( $doneness_data );
            }
            
            // Below product_allergies filter
            $allergy_filter_data = [];
            DB::table('product_allergies_filter')->where('products_id', $product_id)->delete();
            if($product_allergies_filter){
                foreach($product_allergies_filter as $allergy_filter_id) {
                    $allergy_filter_data[] = [
                        'products_id' => $product_id,
                        'allergies_filter_id' => $allergy_filter_id
                    ];
                }
                $allrg_filter = DB::table('product_allergies_filter')->insert( $allergy_filter_data ); 
            }   
            // Below product_categories
            $category_data = [];
            foreach($product_categories as $cat_id) {
                $category_data[] = [
                    'products_id' => $product_id,
                    'category_id' => $cat_id
                ];
            }
            DB::table('product_category')->where('products_id', $product_id)->delete();
            $cat = DB::table('product_category')->insert( $category_data );
            // Below product_incredients
            $incredient_data = [];
            foreach($product_incredients as $incr_id) {
                $incredient_data[] = [
                    'products_id' => $product_id,
                    'incredience_id' => $incr_id
                ];
            }
            DB::table('product_incredience')->where('products_id', $product_id)->delete();
            $incrdnts = DB::table('product_incredience')->insert( $incredient_data );
        }
        if( true ) {
            echo json_encode(['message'=> 'Product updated Successfully', 'status' => TRUE]);
        } else {
            echo json_encode(['message'=> 'Something went wrong', 'status' => FALSE]);
        }
        
        return;
    }

    public function product_data(Request $request) {
        $product_id = $request->post('id');
        $product = DB::table('products')->select('*')->where('id', '=', $product_id)->first();
        $allergies = DB::table('product_allergies')->select('allergies_id')->where('products_id', '=', $product->id)->get();
        $doneness = DB::table('product_doneness')->select('doneness_id')->where('products_id', '=', $product->id)->get();
        $diets = DB::table('product_preferenceoptions')->select('preferenceoptions_id')->where('products_id', '=', $product->id)->where('type', '=', 'Diet')->get();
        $intolerences = DB::table('product_preferenceoptions')->select('preferenceoptions_id')->where('products_id', '=', $product->id)->where('type', '=', 'Intolerence')->get();
        $addons = DB::table('addons_product_mapping')->select('addons_id')->where('products_id', '=', $product->id)->get();
        $allergies_filter = DB::table('product_allergies_filter')->select('allergies_filter_id')->where('products_id', '=', $product->id)->get();
        $categories = DB::table('product_category')->select('category_id')->where('products_id', '=', $product->id)->get();
        $incredients = DB::table('product_incredience')->select('incredience_id')->where('products_id', '=', $product->id)->get();
        $relations = DB::table('product_relation')->select('product_rel_id')->where('products_id', '=', $product->id)->get();
        $product_thumbnail = DB::table('product_images')->select('path', 'id')
            ->where('products_id', '=', $product_id)
            ->where('type', '=', 'THUMBNAIL')->first();
        $product_images = DB::table('product_images')->select('path', 'id')
            ->where('products_id', '=', $product_id)
            ->where('type', '=', 'IMAGE')->get();

        if($product !== null) {
            echo json_encode([
                    'message'=> 'Product fetched Successfully', 
                    'product' => $product,
                    'incredients' => $incredients,
                    'relations' => $relations,
                    'allergies' => $allergies,
                    'doneness' => $doneness,
                    'diets' => $diets,
                    'intolerences' => $intolerences,
                    'addons' => $addons,
                    'allergies_filter' => $allergies_filter,
                    'categories' => $categories,
                    'thumbnail' => $product_thumbnail,
                    'images' => $product_images,
                    'status' => TRUE
                ]);
        } else {
            echo json_encode(['message'=> 'Wrong product', 'status' => FALSE]);
        }

        return;
    }
    
    public function addon_data(Request $request) {
        $product_id = $request->post('id');
        $addons = DB::table('addons_product_mapping')->select('addons_id','name','actual_price')
                ->join('addons', 'addons.id', '=', 'addons_product_mapping.addons_id')
                ->where('products_id', '=', $product_id)->get();
        echo json_encode([
                'message'=> 'Addons fetched Successfully', 
                'addons' => $addons,
                'status' => TRUE
            ]);

        return;
    }
    
    public function product_destroy(Request $request) {
        $product_id = $request->post('id');
//        $allergies = DB::table('product_allergies')->where('products_id', '=', $product_id)->delete();
//        $categories = DB::table('product_category')->where('products_id', '=', $product_id)->delete();
//        $incredients = DB::table('product_incredience')->where('products_id', '=', $product_id)->delete();
//        $product_images =DB::table('product_images')->where('products_id', '=', $product_id)->delete();
//        $product = DB::table('products')->where('id', '=', $product_id)->delete();
        $response = DB::table('products')->where('id', '=', $product_id)->update([
            'deleted_at' => date('Y-m-d H:i:s')
        ]);        
        if ( $response ) {
            echo json_encode(['message' => 'Deleted Successfully', 'status' => TRUE]);
        } else {
            echo json_encode(['message' => 'Something went wrong', 'status' => FALSE]);
        }

        return;
    }    
      

    function product_image_delete(Request $request) {
        $image_id = $request->post('img_id');
        DB::table('product_images')->where('id', '=', $image_id)->delete();

        echo json_encode(['message'=> 'Image deleted', 'status' => TRUE]);
    }

}
