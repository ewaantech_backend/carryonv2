<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Orderitems extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'order_items';

    protected $fillable = [
        'order_id', 'products_id','item_count', 'grant_total','sub_total','tax_total','created_at' , 'updated_at'
    ];
}