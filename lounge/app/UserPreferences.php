<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPreferences extends Model
{
    protected $table = 'users_preferenceoptions';
}
