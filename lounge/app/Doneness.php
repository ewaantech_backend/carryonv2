<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doneness extends Model
{
    protected $table = 'doneness';
  
    
    /**
 * The attributes that are mass assignable.
 *
 * @var array
 */
protected $fillable = [
    'name', 'visibility'
];
}