<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Cartkey extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'cartkey';

    protected $fillable = [
        'cart_id', 'key', 'created_at' , 'updated_at'
    ];
}