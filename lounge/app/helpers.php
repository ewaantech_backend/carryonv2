<?php

function user_permissions($user)
{
    $permissionuser = [];
    $role = [];
    if( $user !== null) {
        $permissions = $user->getAllPermissions();
        foreach($permissions as $perm) {
            $permissionuser[$perm->name] = TRUE;
        }
        $roles = $user->getRoleNames();
        foreach($roles as $r) {
            $role[] = $r;
        }
    }
    return ['permissions' => $permissionuser, 'roles' => $role];
}

function tax_percentage()
{
    /*
     total = actual_price * this_value + actual_price * qty 
    */
    return 0.00;
}

function date_format_from_month_custom($date, $origin = '', $format = 'M d H:i') {
    if( ! $date) return '';
    return date($format, strtotime($date) );
}

function date_format_custom($date, $origin = '', $format = 'Y-m-d H:i') {
    if( ! $date) return '';
    return date($format, strtotime($date) );
}

function time_format_custom($time, $tail_unit = '', $origin = '', $format = 'H:i') {
    if( ! $time) return '';
    return date($format, strtotime($time) ).$tail_unit;
}

function decimal_format_custom($amount=0) {
    return (float) $amount;
}

function default_grabbit_location() {
    return ['location' => "Grabbit"];
}

function amount_format($amount=0) {
    return number_format($amount,2);
}

function modifyText($str) {
    return ucwords(strtolower(str_replace("_", " ", $str)));
}