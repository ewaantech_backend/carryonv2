<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cmsslider extends Model
{
    protected $table = 'cms_slider';
    
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'title','description','image_path','cover_image_path','visibility','position','short_code'
    ];
}