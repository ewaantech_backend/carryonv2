<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use App\PickupPoint;

class BoardingPoint extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'boarding_point';

    public function pickup_point()
    {
        return $this->hasOne(PickupPoint::class, 'gate', 'name');
    }
}