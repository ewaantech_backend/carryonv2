<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Allergies extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'allergies';

    protected $fillable = [
        'name', 'description', 'visibility' , 'image_path'
    ];
}