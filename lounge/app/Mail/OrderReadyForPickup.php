<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderReadyForPickup extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function build()
    {        
        return $this->view('emails.orderreadyforpickup')
                    ->from('noreply@carryon.com', 'CARRYON')
                    ->subject($this->data['subject'])
                    ->with([
                        'cust_name' => $this->data['cust_name'], 
                        'order_id' => $this->data['order_id'], 
                        'pickup_point' => $this->data['pickup_point'], 
                        'item_content' => $this->data['item_content'],
                        'maplink' => $this->data['maplink'],
                        'nearest_gate' => $this->data['nearest_gate'],
                        'short_link_text' => $this->data['short_link_text']
                    ]);
    }
}