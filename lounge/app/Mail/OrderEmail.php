<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function build()
    {        
        return $this->view('emails.ordermail')
                    ->from('noreply@carryon.com', 'CARRYON')
                    ->subject($this->data['subject'])
                    ->with([
                        'cust_name' => $this->data['cust_name'], 
                        'order_id' => $this->data['order_id'], 
                        'item_content' => $this->data['item_content'],
                        'qr_url' => $this->data['qr_url'],
                        'status_link' => $this->data['status_link']
                        ]);
    }
}