<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use App\User;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'order';

    protected $fillable = [
        'users_id', 'item_count', 'grant_total','sub_total','tax_total','flight_number','status','created_at' , 'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'users_id', 'id');
    }
}