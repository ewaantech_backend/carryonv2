<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use App\BoardingPoint;

class Lounge extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'lounge';

    public function gate() {
        return $this->belongsTo(BoardingPoint::class, 'boarding_point_id', 'id');
    }

}