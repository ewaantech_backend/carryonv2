<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('pickup_login', 'Api\ApiController@login')->name('api_login');
Route::post('profile', 'Api\ApiController@profile')->name('api_profile');
Route::post('orders', 'Api\ApiController@show')->name('api_orders');
Route::post('details', 'Api\ApiController@details')->name('api_details');
Route::post('logout', 'Api\ApiController@logout')->name('api_logout');
Route::post('change_status', 'Api\ApiController@change_status')->name('api_change_status');
Route::post('change_status_by_pickup_users', 'Api\ApiController@change_status_by_pickup_users')->name('change_status_by_pickup_users');
Route::post('pdfreport', 'Api\ApiController@pdfreport')->name('api_pdfreport');
Route::post('profileupdate', 'Api\ApiController@profileupdate')->name('api_profileupdate');
Route::post('resetpassword', 'Api\ApiController@resetpassword')->name('api_resetpassword');
Route::post('change_pl_status', 'Api\ApiController@change_pl_status')->name('api_change_pl_status');
Route::post('notification_list', 'Api\ApiController@notification_list')->name('api_notification_list');


Route::post('/flightdata', 'Api\CustApiController@flightshow');
Route::get('/categories', 'Api\CustApiController@categories');
Route::get('/preferenceoptions', 'Api\CustApiController@preferenceoptions');
Route::post('/featured_products', 'Api\CustApiController@featured_products');
Route::post('/preferenceupdate', 'Api\CustApiController@preferenceupdate');
Route::post('/product_detail', 'Api\CustApiController@productdetail');
Route::post('/suggested', 'Api\CustApiController@suggested_for_you_products');
Route::post('/menu_dishes', 'Api\CustApiController@menu_dishes');

// cart
Route::post('/cart/add', 'Api\CartController@create');
Route::post('/cart/placeorder', 'Api\CartController@placeOrder');
Route::post('addon_list', 'Api\CartController@addon_list')->name('api_addon_list');
Route::post('delete_cart', 'Api\CartController@delete_cart')->name('api_delete_cart');
Route::post('update_cart', 'Api\CartController@update_cart')->name('api_update_cart');
Route::post('update_cart_addon', 'Api\CartController@update_cart_addon')->name('api_update_cart_addon');
Route::post('/cart/show', 'Api\CartController@show')->name('api_cart_show');

// start - orders 
Route::post('/cart/order', 'Api\OrderController@getOrders');
Route::post('/cart/orders', 'Api\OrderController@getAllOrders');
Route::post('/order/invoice_pdf', 'Frontend\OrderController@pdfCustomer');
// end - orders

// login
Route::post('/login', 'Api\HomeController@login');
Route::post('/oauthsignin', 'Api\HomeController@oauth2Login');
Route::get('/oauthsigninnew', 'Api\HomeController@oauth2LoginNew');
// sign up
Route::post('/signup', 'Api\HomeController@signUp');
// logout
Route::post('/logout', 'Api\HomeController@logout');
// notifications list
Route::post('/notifications', 'Api\HomeController@getNotifications');
Route::post('/notifications/update', 'Api\HomeController@changeNotificationStatus');
// profile
Route::post('/profiledetails', 'Api\HomeController@getProfile');
Route::post('/profiledetailsUpdate', 'Api\HomeController@updateProfile');
Route::post('/cart/billing', 'Api\HomeController@updateBillingDetails');
Route::post('/changepassword', 'Api\HomeController@changePassword');
Route::post("/preference", "Api\HomeController@updatePreference");

Route::get('/cart/gateway_initialize/{userToken}/{cart_sess_key}', 'Api\OrderController@pre_order_checkout');
Route::get('/payment_success', 'Api\OrderController@payment_success');

//cancel order 
Route::post("/order/cancel", "Api\OrderController@cancelOrder");
Route::post('/forgot_password', 'Api\HomeController@forgotpassword')->name('forgot_password');

