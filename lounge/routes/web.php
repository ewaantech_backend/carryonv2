<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/dxblounge/', 'Frontend\HomeController@index')->name('home_page');
Route::get('/dxblounge/l/{id?}', 'Frontend\HomeController@set_lounge')->name('set_lounge');

Route::get('/dxblounge/productdetail/{url_key}', 'Frontend\ProductsController@productdetail');
Route::get('/dxblounge/flight/show', 'Frontend\HomeController@show');
Route::get('/dxblounge/flight/timecheck', 'Frontend\HomeController@timecheck');

Route::get('/dxblounge/customer/registration', 'Frontend\HomeController@registration');
Route::get('/dxblounge/customer/logout', 'Frontend\HomeController@logout');
Route::post('/dxblounge/customer/login', 'Frontend\HomeController@login');

Route::post('/dxblounge/customer/forgotpassword', 'Frontend\HomeController@forgotpassword')->name('forgotpassword');
Route::post('/dxblounge/customer/resetpassword', 'Frontend\HomeController@resetpassword')->name('resetpassword');
Route::get('/dxblounge/customer/checkout', 'Frontend\HomeController@checkout')->name('checkout');
Route::get('/dxblounge/customer/billingaddress', 'Frontend\HomeController@billingaddress')->name('billingaddress');
Route::get('/dxblounge/product/show', 'Frontend\ProductsController@show');
Route::get('/dxblounge/product/menu', 'Frontend\MenuController@index')->name('our_menu');
Route::post('/dxblounge/product/menu_dishes', 'Frontend\MenuController@menu_dishes')->name('menu_dishes');

Route::get('/dxblounge/customer/orders', 'Frontend\About@index')->name('about');
Route::get('/dxblounge/customer/orders/{order_id}', 'Frontend\About@order_detail')->name('order_detail');

Route::post('/dxblounge/cart/add', 'Frontend\CartController@create')->name('cart_add');
Route::post('/dxblounge/cart/remove', 'Frontend\CartController@destroy');
Route::post('/dxblounge/cart/show', 'Frontend\CartController@show')->name('cart_list');
Route::post('/dxblounge/cart/addon', 'Frontend\CartController@getAddonsCart')->name('cart_addon');
Route::post('/dxblounge/order/add', 'Frontend\OrderController@create')->name('order_add');
Route::post('/dxblounge/home/addbilling', 'Frontend\HomeController@addbilling')->name('add_billing');

Route::post('/dxblounge/home/create_guest', 'Frontend\Checkout@pre_order_checkout')->name('create_lounge');

// Mobile checkoout
Route::get('/dxblounge/cart/gateway_initialize/{userToken}/{cart_sess_key}/{flight}/{date}/{duration}', 'Frontend\Checkout@mobile_pre_order_checkout')->name('mobile_payment_page');
Route::get('/dxblounge/mobile/success/{data}', 'Frontend\Checkout@post_mobile_pay')->name('post_mobile_payment');

Route::get('/dxblounge/order/payment', 'Frontend\Checkout@pre_order_checkout')->name('payment_page');
Route::get('/dxblounge/order/payment/success', 'Frontend\Checkout@payment_success')->name('payment_success');
Route::get('/dxblounge/order/payment/cancelled', 'Frontend\Checkout@payment_cancelled')->name('payment_cancelled');
Route::get('/dxblounge/order/payment/declined', 'Frontend\Checkout@payment_declined')->name('payment_declined');
Route::post('/dxblounge/order/cancel', 'Frontend\OrderController@cancel')->name('cancel_order');

Route::get('/dxblounge/order/cod/success', 'Frontend\Checkout@cod_success')->name('cod_success');

Route::post('/dxblounge/order/update_cart_user', 'Frontend\Checkout@update_cart_user')->name('update_cart_user');
Route::post('/dxblounge/order/history', 'Frontend\OrderController@history')->name('order_history');
Route::post('/dxblounge/home/profile', 'Frontend\HomeController@profile')->name('profile');
Route::post('/dxblounge/home/profileupdate', 'Frontend\HomeController@profileupdate')->name('profileupdate');

Route::post('/dxblounge/home/preference', 'Frontend\HomeController@preference')->name('preference');
Route::post('/dxblounge/home/preferenceupdate', 'Frontend\HomeController@preferenceupdate')->name('preferenceupdate');
Route::post('/dxblounge/home/orderreviewdata', 'Frontend\HomeController@orderreviewdata')->name('orderreviewdata');
Route::post('/dxblounge/home/orderreviewupdate', 'Frontend\HomeController@orderreviewupdate')->name('orderreviewupdate');
Route::post('/dxblounge/order/pdfreport', 'Frontend\OrderController@pdfReportWeb')->name('pdfreport');

Route::get('/dxblounge/checkout/samplesms', 'Frontend\Checkout@samplesms')->name('samplesms');
Route::post('/dxblounge/cart/addon/list', 'Frontend\CartController@addon_list')->name('addon_list');

Route::any('sendemail', function () {
        $data = array(
                'bodyMessage' => "Hi"
        );
        Mail::send('email', $data, function ($message) {
                $message->from('kamarudduja@gmail.com', 'Just Laravel');
                $message->to('kamarudduja@gmail.com')->subject('Just Laravel demo email using SendGrid');
        });
        return Redirect::back()->withErrors([
                'Your email has been sent successfully'
        ]);
});

Auth::routes();
Route::get('/dxblounge/m/{code}', 'Frontend\Checkout@shortenLink')->name('shorten.link');
Route::get('/dxblounge/automatedstatuschange', 'Frontend\CroneController@statuschange')->name('crone_statuschange');
Route::get('/dxblounge/flightgateupdate', 'Frontend\CroneController@flightgateupdate')->name('crone_flightgateupdate');
Route::get('/dxblounge/orderstatusupdate', 'Frontend\CroneController@orderstatusupdate')->name('crone_orderstatusupdate');
Route::get('/dxblounge/remaindermail', 'Frontend\CroneController@remaindermail')->name('crone_remaindermail');
Route::get('/dxblounge/reviewmail', 'Frontend\CroneController@reviewmail')->name('crone_reviewmail');
Route::get('/dxblounge/readymail', 'Frontend\CroneController@readymail')->name('crone_readymail');
Route::get('/dxblounge/orderreview/{key}', 'Frontend\About@orderreview')->name('orderreview');
Route::get('/dxblounge/sample_notification/{type}/{id}', 'Admin\OrdersController@sample_notification')->name('sample_notification');
Route::get('/dxblounge/{url_key}', 'Frontend\HomeController@cms')->name('cms_page');
