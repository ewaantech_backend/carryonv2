<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>CARRYON ADMIN</title>

    <!-- Styles -->
    <link href="{{ asset('dxblounge/css/lib/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dxblounge/css/lib/themify-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('dxblounge/css/lib/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dxblounge/css/lib/helper.css') }}" rel="stylesheet">
    <link href="{{ asset('dxblounge/css/style.css') }}" rel="stylesheet">
</head>

<body class="bg-primary" style="background-color: #4E834B!important;">

    <div class="unix-login">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-4">
                    <div class="login-content">
                        <div class="login-logo">
                            <div class="logo"><a href="index.html"><img src="{{ asset('dxblounge/images/frontend/logo.svg') }}" width="120%" height="120%"/></a></div>
                        </div>
                        <div class="login-form">
                            <!-- <h4>Administratior Login</h4> -->
                            <form method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="form-group">
                                    <!-- <label>Email address</label> -->
                                    <input placeholder="Email" id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                    
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <!-- <label>Password</label> -->
                                    <input placeholder="Password" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                    
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                
                                <!-- m-b-30 m-t-30 -->
                                <button type="submit" class="btn btn-primary btn-flat ">Sign in</button>

                                <div class="checkbox forgot-checkbox">
                                    <label class="pull-right">
                                    @if (Route::has('password.request'))
                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                            Forgot your password?
                                        </a>
                                    @endif
									</label>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>

</html>