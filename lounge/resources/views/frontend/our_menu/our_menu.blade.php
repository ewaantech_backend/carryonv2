<!DOCTYPE html>
<html js no-mobile desktop no-ie chrome chrome83 root-section gradient rgba opacity textshadow multiplebgs boxshadow borderimage borderradius cssreflections csstransforms csstransitions no-touch no-retina fontface webkit chrome win js domloaded w-1920 gt-240 gt-320 gt-480 gt-640 gt-768 gt-800 gt-1024 gt-1280 gt-1440 gt-1680 no-portrait landscape" id="index-page">


<head>
  <!-- Meta Tags -->
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width" />
  <meta name="description" content="" />
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <!-- Page Title & Favicon -->
  <title>Carry On.</title>
  <!-- Stylesheets -->
  <link rel="stylesheet" href="{{ asset('dxblounge/css/frontend/bootstrap.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('dxblounge/css/frontend/font-awesome.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('dxblounge/css/frontend/aos-min.css') }}" />
  <link rel="stylesheet" href="{{ asset('dxblounge/css/frontend/jquery-ui.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('dxblounge/css/frontend/jquery-ui.theme.min.css') }}" />
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">


  <link rel="stylesheet" href="{{asset('dxblounge/css/frontend/cocogoose.css')}}" /> <!-- new -->
  <link rel="stylesheet" href="{{ asset('dxblounge/css/frontend/main.css') }}" />
  <link rel="stylesheet" href="{{asset('dxblounge/css/frontend/main2.css')}}" /> <!-- new -->
  <link rel="stylesheet" href="{{ asset('dxblounge/css/frontend/custom.css') }}" />
  <!-- Javascript -->
  <script src="{{ asset('dxblounge/js/vendor/head.core.js') }}"></script>
  <link rel="stylesheet" href="{{asset('dxblounge/css/phone_plugin/intlTelInput.css')}}" />
</head>

<body class="home menu-page">
  @php
  $our_menu = DB::select('select title, description, image_path from cms_slider where visibility = "YES" and short_code = "our_menu" and deleted_at IS NULL');
  @endphp
  <!--[if lt IE 10]>
        <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please upgrade your browser to improve your experience.</p>
  <![endif]-->
  <div id="container">
    <!-- header -->
    @include('frontend.layouts.headermenu')

    @include('frontend.layouts.footer')
    <!-- /header -->

    @include('frontend.layouts.popup')

    <!-- banner -->

    <div id="banner">
      <figure class="figure1">
        <img class="w-100" src="{{ asset($our_menu[0]->image_path) }}" alt="img">
      </figure>
      <div class="description">
        <div class="container">
          <div class="row">
            <div class="col-lg-2 col-md-7">
              <h2 class="display-1">{!! $our_menu[0]->title !!}</h2>
              <p>{!! $our_menu[0]->description !!}</p>
            </div>
          </div>
        </div>
      </div>
    </div>


    <!-- banner -->

    <!-- banner -->
    <div id="content">


      <div class="section">
        <div class="container">
          {{-- <div class="row tab-content" id="menu_tabContent"> --}}
          <div class="row">
            <div class="col-lg-8 column place-holder tab-content" id="menu_tabContent">
              <div class="tabs1">
                <ul class="nav nav-tabs" id="menu_tab" role="tablist">
                </ul>
                <p class="text-right"><span>Filter out meals with: </span><button type="button" class="btn button2 btn-lg filter-btn"><i class="fa fa-sliders" aria-hidden="true"></i>
                        Filter <i class="fa fa-caret-down" aria-hidden="true"></i></button>
                        <span class="clear-filter-btn d-none"><i class="fa fa-times fa-2x" aria-hidden="true"></i></span>
                </p>
                <ul class="filter-list">
                </ul>
              </div>
              <span id="render_content"></span>
              <!-- Items prepend here -->
            </div>
            
            <div class="col-lg-4 column">
              <div class="widgets">
                <div class="widget flight-block">
                  <div class="header">
                    <h3>Your CarryOn Details</h3>
                    <!-- <a href="#" class="toggle-btn"></a> -->
                  </div>
                  @include('frontend.layouts.flightinfo_order_side')
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>

    <!-- footer -->
    @include('frontend.layouts.footermenu')

    @include('frontend.our_menu.menu_script')
    <!-- /footer -->

    <button class="btn button1 btn-block view-basket" data-toggle="modal" data-target="#basket">view basket</button>
    @include('frontend.layouts.basket_popup')



  </div>

</body>

</html>