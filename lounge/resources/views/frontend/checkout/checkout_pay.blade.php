<div class="payment-information">
  @if( isset($status) && $status == TRUE)
    <div id="telr-div" style="padding-top: 25px;background: #fff;">
        <iframe frameBorder="0" id="telr" src={{ $url }} style="width: 100%;height: 600px;"></iframe>
    </div>
  @else
    <div>
      <span>Payment gateway unavailable. <font id="retry_pg">Retry in 10  sec...</font></span>
    </div>
  @endif
</div>