<!DOCTYPE html>
<html>

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <!-- Meta Tags -->
  
  <meta name="viewport" content="width=device-width" />
  <meta name="description" content="" />
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <!-- Page Title & Favicon -->
  <title>Carry On</title>
  <!-- Stylesheets -->
  <link rel="stylesheet" href="{{ asset('dxblounge/css/frontend/bootstrap.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('dxblounge/css/frontend/font-awesome.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('dxblounge/css/frontend/aos-min.css') }}" />
  <link rel="stylesheet" href="{{ asset('dxblounge/css/frontend/jquery-ui.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('dxblounge/css/frontend/jquery-ui.theme.min.css') }}" />  
    <link rel="stylesheet" href="{{ asset('dxblounge/css/frontend/main.css') }}" />
    <link rel="stylesheet" href="{{asset('dxblounge/css/frontend/main2.css')}}" /> <!-- new -->
    <link rel="stylesheet" href="{{ asset('dxblounge/css/frontend/custom.css') }}" />

    <link rel="stylesheet" href="{{asset('dxblounge/css/phone_plugin/intlTelInput.css')}}" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('dxblounge/css/lib/sweetalert/sweetalert.css') }}">
    <link href="{{ asset('dxblounge/css/lib/multiselect/multiselect-styles.css') }}" rel="stylesheet">
<!--    <link href="{{ asset('dxblounge/css/lib/multiselect/demo-styles.css') }}" rel="stylesheet">-->
  <!-- Javascript -->
  <script src="{{ asset('dxblounge/js/vendor/head.core.js') }}"></script>

  <style>
          #errcode,#errphone
            {
            color: red !important;
             font-size: 16px !important; 
            text-align: center;
            }
</style>
</head>

<body>
  <!--[if lt IE 10]>
        <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please upgrade your browser to improve your experience.</p>
  <![endif]-->
  <div id="container">
    <!-- header -->
    @include('frontend.layouts.headermenu')

    @include('frontend.layouts.footer')
    <!-- /header -->
    
    <!-- /header -->
    <div id="content">
      <!-- section -->
      <div class="section dashboard">
        <div class="container no-bg">
          <div class="row">
            <div class="col-sm-12">
              <div id="order-successful">
                <main>
                  <header>
                    <h2><i class="fa fa-check-circle" aria-hidden="true"></i> Order Received </h2>
                    <?php
                        $invID = str_pad($order_id, 5, '0', STR_PAD_LEFT);
                        $invID = "CON" . $invID;
                    ?>
                    <p>Order no : <strong>{{ $invID }}</strong></p>
                    <p class="call d-none"><i class="fa fa-phone-square" aria-hidden="true"></i> Reachout: <strong>+971 4
                        1234567</strong></p>
                  </header>
                  <?php
                    if ($status['confirm'] == true) {
                        $style = "step1";                        
                    }
                    if ($status['ontheway'] == true) {
                        $style = "step2";                        
                    }
                    if ($status['delivered'] == true) {
                        $style = "step3";                        
                    }
                  ?>
                  <ul class="progressbar <?php echo $style; ?>">
                    <li class="@if($status['confirm'] == true || $status['ontheway'] == true || $status['delivered'] == true) active @endif"><span>Order Confirmed</span></li>
                    <li class="@if($status['ontheway'] == true || $status['delivered'] == true) active @endif"><span>On the way</span></li>
                    <li class="@if($status['delivered'] == true) active @endif"><span>Order delivered</span></li>
                  </ul>
                  <h3 class="dash-h2-ites-head">Order summary</h3>
                  <div class="table-responsive">
                    <table class="table table2 item-table">
                      <thead>
                        <tr>
                          <th scope="col" class="product-thumbnail">Product Name</th>
                          <th scope="col" class="price">Price</th>
                          <th scope="col" class="qty">QTY</th>
                          <th scope="col" class="total">Total</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($data as $order)
                          @foreach($order['items'] as $prdt)
                            <tr>
                            <td class="product-thumbnail" data-title="Product Name">
                              <a href="https://con.ourdemo.online/customer/orders#" class="image">
                                <img src="{{ url($prdt['productthumbnail']) }}" alt="img">
                              </a>
                              <div class="content">
                                <h5><a href="https://con.ourdemo.online/customer/orders#">{{ $prdt['productname'] }}</a></h5>
                                <!-- <p>2 Large Pizza + 1 Free</p> -->
                                @if(isset($prdt['addons']) && count($prdt['addons']) > 0 )
                                  <p>Extra Items</p>
                                @endif
                                <p>
                                  <small>
                                    @php
                                      $addonsAll = '';
                                      foreach($prdt['addons'] as $addon) {
                                        $addonsAll .= $addon['addon_item_count'] . ' X ' .$addon['addon_name'] .'('. $addon['addon_price'].') ';
                                      }
                                    @endphp
                                    {{ $addonsAll }}</small>
                                  </p>
                              </div>
                            </td>
                            <td class="price" data-title="Price">AED {{ $prdt['actual_price'] }}</td>
                            <td class="qty" data-title="QTY">{{ $prdt['item_count'] }}</td>
                            <td class="total" style="text-align:right" data-title="Total">AED {{ $prdt['grant_item_price'] }}</td>
                          </tr>
                          @endforeach
                        @endforeach
                      </tbody>
                      <tfoot class="price-total">
                        <tr>
                          <td colspan="4">AED <span id="item_total">{{ $order['grant_total'] }}</span></td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>

                  <h3>Customer Details</h3>
                  <div class="customer-information mb-0">
                    <ul>
                      <li>
                        <span>Customer Name:</span>{{ $user->name }}
                      </li>
                      <li>
                        <span>Customer Email:</span>{{ $user->lounge_email }}
                      </li>
                      <li>
                        <span>Mobile Number:</span>{{ $user->phone_number }}
                      </li>
                      <li>
                        <span>Country:</span>{{ $user->nationality }}
                      </li>
                      <li>
                        <span>City:</span>{{ $user->billing_city }}
                      </li>
                      <li>
                        <span>Address:</span>{{ $user->billing_address }}
                      </li>
                    </ul>
                  </div>
                  <h3>Payment Details</h3>
                  <?php
                    $type = $dataTransaction->payment_mode ?? '';
                    $auth_code = $dataTransaction->auth_code ?? '';
                    $card = '***********' . $dataTransaction->note ?? '';
                    $time = date('d-m-Y H:i', strtotime($dataTransaction->created_at)) ?? '';
                  ?>
                  <div class="customer-information mb-0">
                    <ul>
                      <li>
                        <span>Transaction Type:</span>{{ $type }}
                      </li>
                      <li>
                        <span>Time:</span>{{ $time }}
                      </li>
                      <?php
                        if ($type != 'COD') {
                      ?>
                      <li>
                        <span>Authorization Code:</span>{{ $auth_code }}
                      </li>
                      <li>
                        <span>Card:</span>{{ $card }}
                      </li>                      
                      <?php 
                        }
                      ?>    
                    </ul>
                  </div>
                </main>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /section -->
    </div>

    <!-- footer -->
    @include('frontend.layouts.footermenu')

    
  </div>
</body>

</html>