<!DOCTYPE html>
<html>

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <!-- Meta Tags -->
  
  <meta name="viewport" content="width=device-width" />
  <meta name="description" content="" />
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <!-- Page Title & Favicon -->
  <title>Carry On</title>
  <!-- Stylesheets -->
  <link rel="stylesheet" href="{{ asset('dxblounge/css/frontend/bootstrap.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('dxblounge/css/frontend/font-awesome.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('dxblounge/css/frontend/aos-min.css') }}" />
  <link rel="stylesheet" href="{{ asset('dxblounge/css/frontend/jquery-ui.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('dxblounge/css/frontend/jquery-ui.theme.min.css') }}" />  
    <link rel="stylesheet" href="{{ asset('dxblounge/css/frontend/main.css') }}" />
    <link rel="stylesheet" href="{{asset('dxblounge/css/frontend/main2.css')}}" /> <!-- new -->
    <link rel="stylesheet" href="{{ asset('dxblounge/css/frontend/custom.css') }}" />

    <link rel="stylesheet" href="{{asset('dxblounge/css/phone_plugin/intlTelInput.css')}}" />
  
  <link rel="stylesheet" href="{{ asset('dxblounge/css/lib/sweetalert/sweetalert.css') }}">
    <link href="{{ asset('dxblounge/css/lib/multiselect/multiselect-styles.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
    <link rel="stylesheet" href="{{asset('dxblounge/css/frontend/custom.css')}}" />
<!--    <link href="{{ asset('dxblounge/css/lib/multiselect/demo-styles.css') }}" rel="stylesheet">-->
  <!-- Javascript -->
  <script src="{{ asset('dxblounge/js/vendor/head.core.js') }}"></script>

  <style>
          #errcode,#errphone
            {
            color: red !important;
             font-size: 16px !important; 
            text-align: center;
            }
</style>
</head>

<body>
  <!--[if lt IE 10]>
        <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please upgrade your browser to improve your experience.</p>
  <![endif]-->
  <div id="container">
    <!-- header -->
    @include('frontend.layouts.headerwithoutmenu')

    @include('frontend.layouts.footerreview')
    <!-- /header -->
    
    <!-- /header -->

    <!-- content -->
    <div id="content">
      <!-- section -->
      <div class="section dashboard revieworder">
        <div class="container">
          <div class="row">

            <div class="tab-content col-xl-9 column">

            <div class="tab-pane  active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
            <main id="main">
                <div class="reviewformarea">
                    <h3>Customer Satisfaction Survey</h3>
                    <h4>Listening to customers has always been important to us. Your feedback will help us better serve people like you!</h4>
                </div>
                <div id="alreadydone" style="display:none;">
                <h3>You have already filled your feedback!</h3>
                </div>
                <div class="form reviewformarea">
                  <ul>
                    <li>
                      <label>1. Freshness</label>
                      <div id="freshness_star"></div>
                      <input type="hidden" readonly id="freshness" name="freshness" class="form-control form-control-sm" value="0">
                    </li>
                    <li>
                      <label>2. Taste</label>
                      <div id="taste_star"></div>
                      <input type="hidden" readonly id="taste" name="freshness" class="form-control form-control-sm" value="0">
                    </li>
                    <li>
                      <label>3. Variety</label>
                      <div id="variety_star"></div>
                      <input type="hidden" readonly id="variety" name="freshness" class="form-control form-control-sm" value="0">
                    </li>
                    <li>
                      <label>4. Ordering Process</label>
                      <div id="ordering_process_star"></div>
                      <input type="hidden" readonly id="ordering_process" name="freshness" class="form-control form-control-sm" value="0">
                    </li>   
                    <li class="staraligned">
                      <label>5. Do you have any other comments or how can we do better?</label>
                      <textarea id="comment" name="comment" class="textarea"></textarea>
                    </li>
                    <li>
                      <input id="reviewsubmit" type="submit" value="Save" class="btn button1">
                    </li>
                  </ul>
                  </ul>
                </div>
              </main>
            </div>
                
              
            </div>

          </div>
        </div>
      </div>
      <!-- /section -->
    </div>
    <!-- /content -->

    <!-- footer -->
    @include('frontend.layouts.footermenu')
    
  </div>
  <script>
  
  $(document).ready(function() {
    @if( isset($order_id) )
      var order_confirmed_id = {{ $order_id }}
    @else
      var order_confirmed_id = "";
    @endif
    
    loadData("<?php echo route('orderreviewdata'); ?>", 'orderreviewdata', {"orderid":order_confirmed_id});
    
    $('#reviewsubmit').on('click', function() {
        var freshness = $('#freshness').val().trim();
        var taste =  $('#taste').val().trim();
        var variety =  $('#variety').val().trim();
        var ordering_process =  $('#ordering_process').val();
        var comment =  $('#comment').val().trim();
        console.log('comment');
        console.log(comment);
        var flag = true; 

        if(comment == ''){
            $("#comment").addClass('error').focus();
            flag = false;
        }

        if (flag) {
            var dataToSubmit = {"freshness":freshness,"taste":taste,
              "variety": variety, "ordering_process": ordering_process, 
              "comment": comment,
              "order_id":order_confirmed_id};    
            loadData("<?php echo route('orderreviewupdate'); ?>", 'orderreviewupdate', dataToSubmit);
        }
    });
    
    function loadData(url, tab, data) {
      $('#comment').removeClass('error')
      $("#overlay").show();
      var order_id = order_confirmed_id;
      $.ajax({
        url: url,
        type: 'post',
        data: data,
        success: function(data) {
          data = JSON.parse(data);
         
          if( tab == 'orderreviewdata') {
            orderreviewdata(data);
          }
          
          if (tab == 'orderreviewupdate') {
              alert("Thank you for your feedback");
              location. href = "<?php echo route('home_page'); ?>";
          }
          
          $("#overlay").hide();
        },
        complete: function() {
            $("#overlay").hide();
        }
      });
    }
    
    function orderreviewdata(data) {
        if (data.data.freshness.trim() == '') {
            $('.reviewformarea').show();
            $('#alreadydone').hide();
        } else {
            $('.reviewformarea').hide();
            $('#alreadydone').show();
        }    
        $("#freshness_star").rating({
            "value": data.data.freshness,
            "click": function (e) {
                $("#freshness").val(e.stars);
            }
        });
        $("#taste_star").rating({
            "value": data.data.taste,
            "click": function (e) {
                $("#taste").val(e.stars);
            }
        });
        $("#variety_star").rating({
            "value": data.data.variety,
            "click": function (e) {
                $("#variety").val(e.stars);
            }
        });
        $("#ordering_process_star").rating({
            "value": data.data.ordering_process,
            "click": function (e) {
                $("#ordering_process").val(e.stars);
            }
        });
        $('#comment').val(data.data.comment);
       
    }
  });
</script>
</body>

</html>