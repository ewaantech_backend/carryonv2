@extends('frontend.layouts.appdetail')
@section('content')
<style>
  input[type=search] {

    background-color: #f1f1f1;
    border-color: #2e2e2e;
    border-style: solid;
    border-width: 2px 2px 2px 2px;
    outline: none;
    padding: 10px 20px 10px 20px;
    width: 250px;
  }

  ul.ui-autocomplete {
    color: #000000 !important;
    -moz-border-radius: 15px;
    border-radius: 1px;
    max-height: 180px;
    overflow-y: auto;
    overflow-x: hidden;
    z-index: 99;
    font-size: 83% !important;
    font-weight: normal !important;
  }
</style>
  <style>
  #errcode,#errphone
    {
    color: red !important;
     font-size: 16px !important; 
    text-align: center;
    }
</style>
<script>
  $(document).ready(function() {
    $('.pay_proceed').hide();

    setTimeout(() => {
      $('#flight_popup button.close').trigger('click');
    }, 200);
    //$('.order').hide();

    $(function() {
      var availableTags = ["Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Anguilla", "Antigua & Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia & Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central Arfrican Republic", "Chad", "Chile", "China", "Colombia", "Congo", "Cook Islands", "Costa Rica", "Cote D Ivoire", "Croatia", "Cuba", "Curacao", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "French West Indies", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Kosovo", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania", "Mauritius", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauro", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "North Korea", "Norway", "Oman", "Pakistan", "Palau", "Palestine", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Pierre & Miquelon", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Korea", "South Sudan", "Spain", "Sri Lanka", "St Kitts & Nevis", "St Lucia", "St Vincent", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Timor L'Este", "Togo", "Tonga", "Trinidad & Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks & Caicos", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States of America", "Uruguay", "Uzbekistan", "Vanuatu", "Vatican City", "Venezuela", "Vietnam", "Virgin Islands (US)", "Yemen", "Zambia", "Zimbabwe"];
      $("#billing_country").autocomplete({
        source: availableTags
      });
    });
  });
</script>
<div id="content">
  <!-- section -->
  <div class="wrapper">
    <div class="container">
      <nav aria-label="breadcrumb">
        <!--            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item"><a href="#">Our Menu </a></li>
              <li class="breadcrumb-item"><a href="#">Breakfast</a></li>
              <li class="breadcrumb-item active" aria-current="page">Gluten free omlet</li>
            </ol>-->
      </nav>
      <div class="row">
        <div class="col-lg-8 column">
          <div class="col-lg-12 column">

            <h2>Your Information</h2>
            <div class="billing-information">
              <p id="validation_messages" style="color:red;font-weight: bold;"></p>
              <h1 id="success_message" style="color: white!important;"></h1>
              <div class="form">
                <ul>
                  <li class="half">
                    <input type="text" id="first_name" name="first_name" class="textbox" placeholder="First Name">
                  </li>

                  <li class="half">
                    <input type="text" id="last_name" name="last_name" class="textbox" placeholder="Last Name">
                  </li>

                  <li class="half phone-field">
                    <!--<input type="text" id="phone" name="phone" class="textbox" placeholder="Phone">-->
                      <input id="phone" name="phone" type="text" class="textbox" placeholder="Phone">
                      <!--<span id="errphone" style="display:none;"></span>-->
                      <!--<span id="phone_error" style="color:red;font-size: 17px;display:none;">Invalid Mobile Number</span>-->
                  </li>

                  <li class="half">
                    <input type="text" id="email" name="email" class="textbox" placeholder="Email">
                  </li>

                  <li class="half">
                    <input type="text" id="city" name="city" class="textbox" placeholder="City">
                  </li>

                  <li class="half">
                    <span class="dropdown_custom" style="display:block;">
                      <select id="billing_country" name="billing_country" class="textbox" placeholder="Select Country">
                      </select>
                    </span>
                  </li>

                  <li class="half">
                    <textarea style="height: 90px;" type="text" id="address" name="sddress" class="textbox" placeholder="Enter Address"></textarea>
                  </li>

                  <li>
                    <input type="submit" value="Continue" class="btn button1" id="billing_submits">
                  </li>
                </ul>
              </div>
            </div>



          </div>

          <div class="accordion" id="accordionExample" style="display: none;">
            <div class="card">
              <div class="card-header" id="headingOne">
                <h2 class="mb-0">
                  <input type="radio" name="type-payment" data-toggle="collapse" data-target="#cash-pay" aria-expanded="true" aria-controls="cash-pay">
                  <label>Online Payment</label>
                </h2>
              </div>

              <div id="cash-pay" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="card-body">
                  <div class="col-lg-12 column payment-info">
                  </div>
                </div>
              </div>
            </div>

            <div class="card">
              <div class="card-header" id="headingTwo">
                <h2 class="mb-0">
                  <input type="radio" name="type-payment" data-toggle="collapse" data-target="#cod-pay" aria-expanded="true" aria-controls="cod-pay">
                  <label>COD</label>
                </h2>
              </div>
              <div id="cod-pay" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                <div class="card-body">
                    <input type="button" onclick="gotoCheckout();" value="Continue" class="btn button1" id="submit-cod">
                </div>
              </div>
            </div>
          </div>

        </div>

        <div class="col-lg-4 column pt-5">
          <div class="widgets">
            <div class="widget flight-block">
              <div class="header">
                <h3>Your CarryOn Details</h3>
              </div>
              @include('frontend.layouts.flightinfo_order_side')
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /section -->
</div>
@php
$confirm_pay = DB::select('select html_content from cms where visibility = "YES" and short_code = "confirm_pay" and deleted_at IS NULL');

@endphp
<!-- <button class="btn button1 btn-block " data-toggle="modal" data-target="#confirm">View Confirm Popup</button> -->

<div class="modal" id="confirm" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h2>Please Confirm</h2>
        <p>{!! $confirm_pay[0]->html_content !!}</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary order-reviewed">Yes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>
<script>
  function cod_confirm() {
    var r = confirm("Please confirm your order and continue?");
    if (r == true) {
      location.href='{{ route('cod_success') }}';
    }
  }

  $(document).ready(function() {
     $('#phone').on('focusout', function() {
      var phone = $(this).val();
      if( ! phonevalidation(phone) ) {
        $('#phone_error').show();
        $("#phone").addClass('error');
      } else {
        $('#phone_error').hide();
        $("#phone").removeClass('error');
      }
    });

    function phonevalidation(phone) {
      reg = /\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)-\d{5,10}$/
      return reg.test(phone);
    }     
    $("#phone").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
          $("#errcode,#errphone").html("Digits only").show().fadeOut("slow");
          return false;
        }
    });
    var input = document.querySelector("#phone");
    window.intlTelInput(input, {
    preferredCountries: ["ae"],
    initialCountry:"ae",
    utilsScript: "{{asset('js/phone_plugin/utils.js')}}",
    });
    $('.iti__country').click(function(){
      $('#phone').val($(this).children("span:nth-child(3)").html()+'-');
    })

    // populate country name in country feild //
    //alert("{{ Session::get('billing_country')}}");
    var countryNames = ["United Arab Emirates", "Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Anguilla", "Antigua & Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia & Herzegovina", "Botswana", "Brazil", "British Virgin Islands", "Brunei", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central Arfrican Republic", "Chad", "Chile", "China", "Colombia", "Congo", "Cook Islands", "Costa Rica", "Cote D Ivoire", "Croatia", "Cuba", "Curacao", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands", "Faroe Islands", "Fiji", "Finland", "France", "French Polynesia", "French West Indies", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Kosovo", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania", "Mauritius", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauro", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "North Korea", "Norway", "Oman", "Pakistan", "Palau", "Palestine", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russia", "Rwanda", "Saint Pierre & Miquelon", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Korea", "South Sudan", "Spain", "Sri Lanka", "St Kitts & Nevis", "St Lucia", "St Vincent", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Timor L'Este", "Togo", "Tonga", "Trinidad & Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks & Caicos", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States of America", "Uruguay", "Uzbekistan", "Vanuatu", "Vatican City", "Venezuela", "Vietnam", "Virgin Islands (US)", "Yemen", "Zambia", "Zimbabwe"];
    var billing_country = $("#billing_country");
    $(countryNames).each(function(index, element) {
      var option = $("<option />");

      //Set Customer Name in Text part.
      option.html(element);

      //Set Customer CustomerId in Value part.
      option.val(element);

      //Add the Option element to DropDownList.
      billing_country.append(option);
    });
    @if(Session::has('billing_country'))
    $("#billing_country").val("{{ Session::get('billing_country')}}").change();
    @endif
    // populate country name in country feild  ends //

  });

  function gotoCheckout() {
    swal({
            title: "Confirm billing",
            text: "Are you sure to continue?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: true,
            showLoaderOnConfirm: true,
        },
        function(confirm){
            if(confirm) {
              location.href = "{{ route('cod_success') }}";
            }
        });
  }
</script>
@endsection