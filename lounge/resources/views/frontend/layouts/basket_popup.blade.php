<div class="modal" id="basket" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="widgets">
            <div class="widget flight-block">
              <div class="header">
                <h3>Your CarryOn Details</h3>
                <button type="button" class="toggle-btn btn btn-secondary" data-dismiss="modal"></button>
              </div>
              @include('frontend.layouts.flightinfo_order_side')
            </div>
          </div>
        </div>
      </div>
    </div>
    <script>
        $(document).ready(function() {
            $(document).on('click', '.cart', function() {
                $('.view-basket').trigger('click');
            });
        });
    </script>