<!DOCTYPE html>
<html>

    @extends('frontend.layouts.header')

<body>
  <!--[if lt IE 10]>
        <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please upgrade your browser to improve your experience.</p>
  <![endif]-->
  <div id="container">
    <!-- header -->
    @extends('frontend.layouts.headermenu')
    <!-- /header -->
    <div class="fixed-footer">
      <a class="btn button1 trigger-order">Order now</a>
    </div>
    <!-- Order -->
    @extends('frontend.layouts.popup')
    <!-- Order -->
    <!-- banner -->
    <div id="banner">
      <button class="close"><i class="fa fa-times" aria-hidden="true"></i> </button>
      <div class="slides">
        <div class="slide video-slide" data-slide="slide1">
          <figure class="figure1">
            <img class="w-100" src="{{asset('dxblounge/images/frontend/banner-img1.jpg')}}" alt="img">
          </figure>
          <a href="#" class="play-btn"><i class="fa fa-play" aria-hidden="true"></i></a>
          <video id="slide1" muted controls>
            <source src="{{asset('dxblounge/videos/movie.mp4')}}" type="video/mp4">
          </video>
          <div class="description">
            <div class="container">
              <div class="row">
                <div class="col-lg-4 col-md-7">
                  <h2 class="display-1">Your restaurant’s own
                    online food ordering portal</h2>
                  <a href="#" class="btn button1">Learn more</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="slide" data-slide="slide2">
          <figure class="figure1">
            <img class="w-100" src="{{asset('dxblounge/images/frontend/banner-img1.jpg')}}" alt="img">
          </figure>
          <div class="description">
            <div class="container">
              <div class="row">
                <div class="col-lg-4 col-md-7">
                  <h2 class="display-1">Your online food ordering portal</h2>
                  <a href="#" class="btn button1">Learn more</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="slide" data-slide="slide3">
          <figure class="figure1">
            <img class="w-100" src="{{asset('dxblounge/images/frontend/banner-img1.jpg')}}" alt="img">
          </figure>
          <div class="description">
            <div class="container">
              <div class="row">
                <div class="col-lg-4 col-md-7">
                  <h2 class="display-1">Restaurant’s ordering portal</h2>
                  <a href="#" class="btn button1">Learn more</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="slide video-slide" data-slide="slide4">
          <figure class="figure1">
            <img class="w-100" src="{{asset('dxblounge/images/frontend/banner-img1.jpg')}}" alt="img">
          </figure>
          <a href="#" class="play-btn"><i class="fa fa-play" aria-hidden="true"></i></a>
          <video id="slide4" muted controls>
            <source src="{{asset('dxblounge/videos/movie1.mp4')}}" type="video/mp4">
          </video>
          <div class="description">
            <div class="container">
              <div class="row">
                <div class="col-lg-4 col-md-7">
                  <h2 class="display-1">Your own
                    online food ordering</h2>
                  <a href="#" class="btn button1">Learn more</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="controls">
        <div class="container"></div>
      </div>
    </div>
    <!-- banner -->
    <div id="content">
      <!-- section -->
      <div class="section">
        @yield('content')
      </div>
      <!-- /section -->
      <!-- section -->
      <div class="section about">
        <div class="fuel-for-journey">
          <img src="{{asset('dxblounge/images/frontend/fuel-for-journey.png')}}" alt="Fuel For Journey" />
        </div>
        <div class="container">
          <header>
            <h2>About CarryOn</h2>
            <p>We don’t make it ‘til you order it.</p>
          </header>
        </div>
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-7 order-md-2">
              <div class="video">
                <a href="#">
                  <img class="w-100" src="{{asset('dxblounge/images/frontend/video-image.jpg')}}" alt="img">
                  <span class="play-btn">
                    <i class="fa fa-play" aria-hidden="true"></i>
                  </span>
                </a>
              </div>
            </div>
            <div class="col-md-5 d-flex">
              <div class="box1 align-self-center">
                <h2>We believe
                  eating right should be easy for
                  everyone.</h2>
                <p>We started CarryOn because we believe eating fresh, nutritious dishes every day should not only be
                  easy for everyone, but it is also important when traveling too.</p>
                <footer>
                  <a href="#" class="btn btn-link">Learn more about CarryOn</a>
                </footer>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /section -->
    </div>





    <!-- footer -->
     @extends('frontend.layouts.footermenu')
     @extends('frontend.layouts.footer')
     <!-- footer -->
  </div>
</body>

</html>


























