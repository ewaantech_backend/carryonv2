<div class="content">
    <!-- <hr class="divider1"> -->
    <div class="table-responsive">
        <table class="table table1">
            <tbody class="sideon-cart-list">
                <!-- side cart list -->
            </tbody>
        </table>
    </div>
    <ul class="list1 mb-0">
        <!-- <li>
        <span class="data">Subtotal</span>
        <span class="value sub_total">AED 00.00</span>
        </li>
        <li>
        <span class="data">Tax (5%)</span>
        <span class="value tax_total">AED 00.00</span>
        </li> -->
        <li class="cart-total">
            <span class="data">Total<span style="font-size: 15px !important;padding: 3px;font-weight: 500;">(Including VAT)</span></span>
            <span class="value grand_total"><span>AED</span> 0</span>
        </li>
    </ul>
    <footer>
        <a class="pay_proceed btn button1 btn-block" style="color: #fff;cursor:pointer;">Go to Checkout</a>
    </footer>
</div>
<script>
    $(document).ready(function() {

        $(document).on('click', '.flight-update', function() {
            $('#basket').find('.toggle-btn').trigger('click');
            $('#flight_popup').show();
            $('.trigger-order').trigger('click');
        });

        $(document).on('click', '.pay_proceed', function(event) {
            location.href = "<?php echo route('billingaddress') ?>";
        });


        function update_cart() {
            $('#overlay').show();
            $.ajax({
                url: "<?php echo route('update_cart_user') ?>",
                method: "post",
                data: {
                    key: localStorage.getItem('cart_key')
                },
                success: function(data) {
                    data = JSON.parse(data);
                    if (data.status == true) {
                        location.href = "<?php echo route('billingaddress') ?>";
                    }
                },
                complete: function() {
                    $('#overlay').hide();
                }
            });
        }

    });
</script>