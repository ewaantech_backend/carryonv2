@php
$flight_popup_top = DB::select('select html_content from cms where visibility = "YES" and short_code = "flight_popup_top" and deleted_at IS NULL');
$flight_popup_bottom = DB::select('select html_content from cms where visibility = "YES" and short_code = "flight_popup_bottom" and deleted_at IS NULL');
@endphp
<div class="order" id="flight_popup">
  <button style="display:none;" class="trigger-order">trigger</button>
  <button class="close"><i class="fa fa-times" aria-hidden="true"></i> </button>
  {!! $flight_popup_top[0]->html_content !!}
  <!--    <h1>Fresh, healthy meals for your flight</h1>-->
  <div class="form">
    <span class="flight ui-widget">
      <input type="password" style="display:none" /><!-- to override prompt -->
      <input id='flight' type="text" placeholder="Enter your flight number" class="flight" />
    </span>
    <input type="password" style="display:none" /><!-- to override prompt -->
    <span class="date"><input id="flight_date_dt" type="text" placeholder="Select your flight date" class="flight_date" readonly onfocus="this.removeAttribute('readonly');" /></span>

    <span id="flight_data" style="font-size: 16px;border-radius: 7px;color: #4a8046;"></span>
    <!-- <span class="flight-info ">No flight added yet.</span> -->

    <span id="timecheck_error" style="color:red;font-weight: bold;font-size: 18px"></span>
    <span class="submit"><input id='fligh_submit' type="button" value="Let's go!" /></span>
  </div>
  <p>{!! $flight_popup_bottom[0]->html_content !!}
    <!--        <a href="{{asset('dxblounge/terms-and-conditions')}}" target="_blank">Terms & Conditions</a> and <a href="{{asset('dxblounge/privacy-policy')}}" target="_blank">Privacy Policy.</a>-->
  </p>
</div>

<!-- Addons Modal -->
{{-- <div class="modal fade" id="addon" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addonTitle">Add ons</h5>
        <button type="button" id="addon_close" class="close" style="color:#387635 !important" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer" style="color:#387635 !important">
        <footer class="clearfix">
          Total <span id="addon_price" class="price" data-price="0">0 <span>AED</span></span>
          <button id="addon_save" type="button" class="btn button1" data-dismiss="modal">Save</button>
        </footer>
      </div>
    </div>
  </div>
</div> --}}

<div class="modal fade" id="addon" tabindex="-1" role="dialog"
 {{-- style="padding-right: 17px; display: block;" --}}
 >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <button
      type="button"
      class="btn btn-secondary"
      data-dismiss="modal"
      id="close_addon"
    >
      <i class="fa fa-times" aria-hidden="true"></i>
    </button>
      <div class="content">
        <h3>Add ons</h3> 
          <div class="row">
            <div class="col-sm-6" id="popupimage">
<!--                <img src="{{asset('dxblounge/images/banner-img1.jpg')}}" alt="" />-->
            </div>
            <div class="col-sm-6">
              <h4 id="addons_product_name"></h4>
              <p></p>
            </div>
        </div>
        <ul class="addon-list">

        </ul>
        <footer class="clearfix">
          Total <span id="addon_price" class="price" data-price="0">0 <span>AED</span></span>
          <a id="addon_save" href="#" class="btn button1" data-dismiss="modal">Add to cart </a>
          {{-- <button id="addon_save" type="button" class="btn button1" data-dismiss="modal">Add to cart</button> --}}
        </footer>
      </div>
    </div>
  </div>
</div> 
<!-- /footer -->
{{-- <div class="modal-backdrop show"></div> --}}
  

<script type="text/Javascript">
  $(document).ready(function(){

  // start - addon 

  $('#addon').on('hidden.bs.modal', function (e) {
    $("#addon_save").off("click");
  })
    // end - addon 


    localStorage.setItem("flight", 'LOUNGE');
    localStorage.setItem("flight_date", '2020-01-01');
    localStorage.setItem("can_order", 1);

  $('.addonsubmit').click(function(){
    var flight = $("#flight_new").val();
    flight = flight.toUpperCase();
    var flight_date = $(".flight_date_new1").val();
    var flight_date_original = flight_date;
    if(flight == ''){
        $("#flight_new").addClass('textbox error').focus();
        return false;
    }
    if(flight_date == ''){
        $(".flight_date_new1").addClass('textbox error');
        return false;
    }
    $("#flight_new").removeClass('textbox error');
    $(".flight_date_new1").removeClass('textbox error');

    $('#overlay').show();
    var n = flight_date.includes("/");
//    if (n == 0) {
//        var res = flight_date.split("-");
//        flight_date = res[1]+'/'+res[0]+'/'+res[2]; 
//    }
    var res = flight_date.split("-");
    flight_date = res[1]+'/'+res[0]+'/'+res[2]; 
    console.log(flight_date);
    $.ajax( {
      url: "{{url('/dxbdxblounge/flight/show')}}",
      dataType: "json",
      data: {
        fl_no: flight,
        date: flight_date,
        fl_rnd: Math.random()
      },
      success: function( data ) {
        if(data.status == true) {
          localStorage.setItem("can_order", 1);
          $("#timecheck_error_new").text("");
          localStorage.setItem("flight",flight);
          localStorage.setItem("flight_date", flight_date_original);
          localStorage.setItem("flight_updated_at", new Date());

          $('.flight_number_sidebar').html('<span>Flight number:</span>'+ flight );
          $('.flight_deliver_date_sidebar').html('<span>Flight date:</span>' + flight_date );
          alert('Flight data updated');
          $('#flight_popup button.close').trigger('click');
          $("body").removeClass("popup-open");
          var url_part = location.href.split('/')[location.href.split('/').length - 1].trim();
          if( $.inArray(url_part, ['terms-and-conditions', 'menu'] ) == -1 && location.href.indexOf('productdetail') == -1 ) {
            location.href = "<?php echo route('our_menu') ?>";
          }
        } else {
          localStorage.setItem("can_order", 0);
          localStorage.setItem("flight", 'N/A');
          localStorage.setItem("flight_date", 'N/A');
          if(data.message == 'time elapsed') {
            $("#timecheck_error").html(`We can only accept orders 6 hours before departure time. So sorry!`);
          } else {
            $("#timecheck_error_new").html(data.message);
          }
          $(".flight-info").html("No flight added.");
          $('#flight_data_new').hide();
        }
      },
      complete: function() {
        $('#overlay').hide();
      }
    });
}); 


$('.submit').click(function(){
    var flight = $("#flight").val();
    flight = flight.toUpperCase();
    var flight_date = $(".flight_date").val();
    var flight_date_original = flight_date;
    if(flight == ''){
        $("#flight").addClass('textbox error').focus();
        return false;
    }
    if(flight_date == ''){
        $(".flight_date").addClass('textbox error');
        return false;
    }
    $("#flight").removeClass('textbox error');
    $(".flight_date").removeClass('textbox error');

    $('#overlay').show();
    var n = flight_date.includes("/");
//    if (n == 0) {
//        var res = flight_date.split("-");
//        flight_date = res[1]+'/'+res[0]+'/'+res[2];
//    }
    var res = flight_date.split("-");
    flight_date = res[1]+'/'+res[0]+'/'+res[2]; 
    $.ajax( {
      url: "{{url('/dxbdxblounge/flight/show')}}",
      dataType: "json",
      data: {
        fl_no: flight,
        date: flight_date,
        fl_rnd: Math.random()
      },
      success: function( data ) {
        if(data.status == true) {
          localStorage.setItem("can_order", 1);
          $("#timecheck_error").text("");
          localStorage.setItem("flight",flight);
          localStorage.setItem("flight_date", flight_date_original);
          localStorage.setItem("flight_updated_at", new Date());

          $('.flight_number_sidebar').html('<span>Flight number:</span>'+ flight );
          $('.flight_deliver_date_sidebar').html('<span>Flight date:</span>' + flight_date );
          alert('Flight data updated');
          $('#flight_popup button.close').trigger('click');
          $("body").removeClass("popup-open");
          var url_part = location.href.split('/')[location.href.split('/').length - 1].trim();
          if( $.inArray(url_part, ['terms-and-conditions', 'menu'] ) == -1 && location.href.indexOf('productdetail') == -1 ) {
            location.href = "<?php echo route('our_menu') ?>";
          }
        } else {
          localStorage.setItem("can_order", 0);
          localStorage.setItem("flight", 'N/A');
          localStorage.setItem("flight_date", 'N/A');
          if(data.message == 'time elapsed') {
            $("#timecheck_error").html(`We can only accept orders 6 hours before departure time. So sorry!`);
          } else {
            $("#timecheck_error").html(data.message);
          }
          $(".flight-info").html("No flight added.");
          $('#flight_data').hide();
        }
      },
      complete: function() {
        $('#overlay').hide();
      }
    });
});    
   
//$('#flight_popup').hide();
//Order button in mobile view click
  $('.trigger-order').click(function(){
      $('#flight_popup').show();
  });
       
//Close pop on clicking close icon in pop
$(".close").click(function(){
   $('#flight_popup').hide();
});
    
$(".food").click(function(){
    $('#flight_popup').show();
})

});


</script>