<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<p><span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">Your order ready</span></p>

<table bgcolor="#525659" cellpadding="0" cellspacing="0" width="100%">
	<tbody>
		<tr>
			<td>
			<table align="center" bgcolor="#ffffff" cellpadding="0" cellspacing="0" width="720">
				<tbody>
					<tr>
						<td align="center" valign="top">
						<table border="0" cellpadding="20" cellspacing="0" width="100%">
							<tbody>
								<tr>
									<td align="center" bgcolor="#f5f7ec"><a href="#" target="_blank"><img src="https://con.ourdemo.online/public/images/logo-email.png" style="width: 220px;" /> </a></td>
								</tr>
								<tr>
									<td align="center" bgcolor="#387635"><font face="arial" style="font-size: 32px;color: #ffffff;">Order Confirmation</font></td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td align="center" valign="top">
						<table style="font-size:14px;color:#404041;" width="620">
							<tbody>
								<tr>
									<td height="40"> </td>
								</tr>
								<tr>
									<td align="center"><font face="arial" style="font-size: 22px;color: #f16521;"><b>Hi {{ $cust_name }}</b></font></td>
								</tr>
								<tr>
									<td height="20"> </td>
								</tr>
								<tr>
									<td align="center"><font face="arial" style="font-size: 18px;color: #f16521;">Your order is ready!</font></td>
								</tr>
								<tr>
									<td height="30"> </td>
								</tr>
								<tr>
									<td align="center" valign="top">
									<table border="0" cellpadding="5" cellspacing="2" style="font-size: 10px;text-transform: uppercase;font-weight: bold;" width="80%">
										<tbody>
											<tr>
												<td align="center" bgcolor="#387635" width="33%"><font face="arial" style="color: #ffffff;padding: 10px 0;">Order Placed</font></td>
												<td align="center" bgcolor="#387635" width="33%"><font face="arial" style="color: #ffffff;padding: 10px 0;">Ready for pick up</font></td>
												<td align="center" bgcolor="#e2e2e2" width="33%"><font face="arial" style="color: #4a494c;padding: 10px 0;">Delivered</font></td>
											</tr>
										</tbody>
									</table>
									</td>
								</tr>
								<tr>
									<td height="30" style="border-bottom:1px dashed #bbca85;"> </td>
								</tr>
								<tr>
									<td height="20"> </td>
								</tr>
								<tr>
									<td style="text-align:justify;font-size: 13px;color: #404041;">
									<p><span style="line-height:24px"><font face="arial">Your CarryOn order <b>{{ $order_id }}</b> is waiting for you at <b>{{ $pickup_point }}, </b>next&nbsp;to<b>&nbsp;</b>gate <b>{{ $nearest_gate }}.</b></font></span></p>
									</td>
								</tr>
								<tr>
									<td height="10"> </td>
								</tr>
								<tr>
									<td style="text-align:justify;font-size: 13px;color: #404041;"><span style="line-height:24px"><font face="arial">Find your way:&nbsp;<a href="{{ $maplink }}" style="text-decoration: none;color: #799b00;cursor: pointer;" target="_blank">Click to See the Location in Map</a></font> </span></td>
								</tr>
								<tr>
									<td height="10"> </td>
								</tr>
								<tr>
									<td style="text-align:justify;font-size: 13px;color: #404041;"><span style="line-height:24px"><font face="arial">If you cannot find the pick-up location inside the terminal, please call the number on your receipt and one of our team members will be happy to assist you.</font> </span></td>
								</tr>
								<tr>
									<td height="60"> </td>
								</tr>
								<tr>
									<td style="font-size:20px;color:#f16521"><font face="arial"><strong>Order Details</strong></font></td>
								</tr>
								<tr>
									<td height="10"> </td>
								</tr>
								<tr>
									<td>{!! $item_content !!}</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td height="50"> </td>
					</tr>
					<tr>
						<td align="center"><font face="arial" style="font-size: 20px;color: #f16521;">Have a safe journey!</font></td>
					</tr>
					<tr>
						<td height="40"> </td>
					</tr>
					<tr>
						<td bgcolor="#f5f7ec">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tbody>
								<tr>
									<td height="40"> </td>
								</tr>
								<tr>
									<td align="center">
									<table border="0" cellpadding="0" cellspacing="5" width="75%">
										<tbody>
											<tr>
												<td style="text-align:center;font-size: 13px;color: grey;"><span style="line-height:24px"><font face="arial"><strong>Customer Support:</strong> Email us your questions at <a href="mailto:hello@carryondxb.com" style="text-decoration: none;color: #799b00;cursor: pointer;" target="_blank">hello@carryondxb.com</a></font> </span></td>
											</tr>
											<tr>
												<td style="text-align:center;font-size: 13px;color: grey;"><font face="arial">Here are our <a href="#" style="text-decoration: none;color: #799b00;cursor: pointer;" target="_blank">Terms & Conditions</a></font></td>
											</tr>
											<tr>
												<td height="20" style="border-top:0;border-right:0;border-bottom:2px solid #7ca16f;border-left:0;"> </td>
											</tr>
											<tr>
												<td height="20"> </td>
											</tr>
											<tr>
												<td style="text-align:center;font-size: 14px;color: grey;"><font face="arial">CarryOnDXB LLC</font></td>
											</tr>
											<tr>
												<td style="text-align:center;font-size: 13px;color: grey;"><font face="arial">Dubai International Airport, Terminal 3, Dubai, United Arab Emirates</font></td>
											</tr>
											<tr>
												<td style="text-align:center;font-size: 13px;color: grey;"><font face="arial"><strong>Telephone:</strong> +971 50 298 7966 </font></td>
											</tr>
										</tbody>
									</table>
									</td>
								</tr>
								<tr>
									<td height="30"> </td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>