<!-- <p><span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">Welcome Mail</span></p>

<table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;">
	<tbody>
		<tr>
			<td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"> </td>
			<td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; Margin: 0 auto; max-width: 580px; padding: 10px; width: 580px;">
			<div class="content" style="box-sizing: border-box; display: block; Margin: 0 auto; max-width: 580px; padding: 10px;">
			<table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #ffffff; border-radius: 3px;">
				<tbody>
					<tr>
						<td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">
						<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
							<tbody>
								<tr>
									<td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">
									<p><img alt="Useful alt text" border="0" src="https://www.carryondxb.com/images/logo-email.png" style="border:0; outline:none; text-decoration:none; display:block;" /></p>
									{!! $body_text1 !!}</td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>

			<div class="footer" style="clear: both; Margin-top: 10px; text-align: center; width: 100%;">
			<table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
				<tbody>
					<tr>
						<td class="content-block" style="font-family: sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; font-size: 12px; color: #999999; text-align: center;"><span class="apple-link" style="color: #999999; font-size: 12px; text-align: center;">hello@carryondxb.com</span></td>
					</tr>
					<tr>
						<td class="content-block powered-by" style="font-family: sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; font-size: 12px; color: #999999; text-align: center;">+971 50 298 7966</td>
					</tr>
				</tbody>
			</table>
			</div>
			</div>
			</td>
			<td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"> </td>
		</tr>
	</tbody>
</table> --><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<p><span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">Welcome Mail</span></p>

<table bgcolor="#525659" cellpadding="0" cellspacing="0" width="100%">
	<tbody>
		<tr>
			<td>
			<table align="center" bgcolor="#ffffff" cellpadding="0" cellspacing="0" width="720">
				<tbody>
					<tr>
						<td align="center" valign="top">
						<table border="0" cellpadding="20" cellspacing="0" width="100%">
							<tbody>
								<tr>
									<td align="center" bgcolor="#f5f7ec" style="border-bottom:2pt solid #f16521;"><a href="#" target="_blank"><img src="https://www.carryondxb.com/images/logo-email.png" style="width: 220px;" /> </a></td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td height="20"> </td>
					</tr>
					<tr>
						<td align="center" valign="top">
						<div style="font-size: 36px; color:#000; font-weight: bold; text-align: center;">Hi {!! $user !!},</div>
						 

						<div style="font-size: 18px; color:#000; text-align: center;">Welcome to <b>CarryOn</b>, we’re happy to have you around!</div>
						</td>
					</tr>
					<tr>
						<td bgcolor="#f16521" height="50">
						<div style="font-size: 18px; color:#fff; text-align: center;">Your system generated password is <b>{{ $rand }}</b></div>
						</td>
					</tr>
					<tr>
						<td height="40"> </td>
					</tr>
					<tr>
						<td bgcolor="#f5f7ec">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tbody>
								<tr>
									<td height="40"> </td>
								</tr>
								<tr>
									<td align="center">
									<table border="0" cellpadding="0" cellspacing="5" width="75%">
										<tbody>
											<tr>
												<td style="text-align:center;font-size: 13px;color: grey;"><span style="line-height:24px"><font face="arial"><strong>Customer Support:</strong> Email us all your questions at <a href="mailto:hello@carryondxb.com" style="text-decoration: none;color: #f19a21;cursor: pointer;" target="_blank">hello@carryondxb.com</a></font> </span></td>
											</tr>
											<tr>
												<td style="text-align:center;font-size: 13px;color: grey;"><font face="arial">Here are our <a href="#" style="text-decoration: none;color: #f19a21;cursor: pointer;" target="_blank">Terms & Conditions</a></font></td>
											</tr>
											<tr>
												<td height="20" style="border-top:0;border-right:0;border-bottom:2px solid #f16521;border-left:0;"> </td>
											</tr>
											<tr>
												<td height="20"> </td>
											</tr>
											<tr>
												<td style="text-align:center;font-size: 14px;color: grey;"><font face="arial">CarryOnDXB LLC</font></td>
											</tr>
											<tr>
												<td style="text-align:center;font-size: 13px;color: grey;"><font face="arial">451 Lounge, Airport Terminal 3, Dubai, United Arab Emirates</font></td>
											</tr>
											<tr>
												<td style="text-align:center;font-size: 13px;color: grey;"><font face="arial"><strong>Telephone:</strong> +971 4 12345678</font></td>
											</tr>
										</tbody>
									</table>
									</td>
								</tr>
								<tr>
									<td height="30"> </td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>