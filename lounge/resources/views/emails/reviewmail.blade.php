<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<p><span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">We hope you enjoyed the food </span></p>

<table bgcolor="#525659" cellpadding="0" cellspacing="0" width="100%">
	<tbody>
		<tr>
			<td>
			<table align="center" bgcolor="#ffffff" cellpadding="0" cellspacing="0" width="720">
				<tbody>
					<tr>
						<td align="center" valign="top">
						<table border="0" cellpadding="20" cellspacing="0" width="100%">
							<tbody>
								<tr>
									<td align="center" bgcolor="#f5f7ec"><a href="#" target="_blank"><img src="https://www.carryondxb.com/images/logo-email.png" style="width: 220px;" /> </a></td>
								</tr>
								<tr>
									<td align="center" bgcolor="#f16521"><font face="arial" style="font-size: 32px;color: #ffffff;">Share your feedback!</font></td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td align="center" valign="top">
						<table style="font-size:14px;color:#404041;" width="620">
							<tbody>
								<tr>
									<td height="40"> </td>
								</tr>
								<tr>
									<td align="center"><font face="arial" style="font-size: 22px;color: #f16521;"><b>Hi {{ $cust_name }}</b></font></td>
								</tr>
								<tr>
									<td height="20"> </td>
								</tr>
								<tr>
									<td align="center"><font face="arial" style="font-size: 18px;color: #f16521;">We hope you had a safe journey and enjoyed your CarryOn meal!</font></td>
								</tr>
								<tr>
									<td align="center"><font face="arial" style="font-size: 18px;color: #f16521;">How did we do? </font><font face="arial"><font style="font-size: 18px;color: #f16521;"><a href="{{ $reviewlink }}">Click here</a> to review the order!</font></font></td>
								</tr>
								<tr>
									<td align="center"> </td>
								</tr>
								<tr>
									<td align="center">To make future orders easier and faster, take a moment to update your profile. </td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td height="40"> </td>
					</tr>
					<tr>
						<td bgcolor="#f5f7ec">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tbody>
								<tr>
									<td height="40"> </td>
								</tr>
								<tr>
									<td align="center">
									<table border="0" cellpadding="0" cellspacing="5" width="75%">
										<tbody>
											<tr>
												<td style="text-align:center;font-size: 13px;color: grey;"><span style="line-height:24px"><font face="arial"><strong>Customer Support:</strong> Email us all your questions at <a href="mailto:hello@carryondxb.com" style="text-decoration: none;color: #f19a21;cursor: pointer;" target="_blank">hello@carryondxb.com</a></font> </span></td>
											</tr>
											<tr>
												<td style="text-align:center;font-size: 13px;color: grey;"><font face="arial">Here are our <a href="#" style="text-decoration: none;color: #f19a21;cursor: pointer;" target="_blank">Terms & Conditions</a></font></td>
											</tr>
											<tr>
												<td height="20" style="border-top:0;border-right:0;border-bottom:2px solid #f16521;border-left:0;"> </td>
											</tr>
											<tr>
												<td height="20"> </td>
											</tr>
											<tr>
												<td style="text-align:center;font-size: 14px;color: grey;"><font face="arial">CarryOnDXB LLC</font></td>
											</tr>
											<tr>
												<td style="text-align:center;font-size: 13px;color: grey;"><font face="arial">Dubai International Airport, Terminal 3, Dubai, United Arab Emirates</font></td>
											</tr>
											<tr>
												<td style="text-align:center;font-size: 13px;color: grey;"><font face="arial"><strong>Telephone:</strong> +971 50 298 7966</font></td>
											</tr>
										</tbody>
									</table>
									</td>
								</tr>
								<tr>
									<td height="30"> </td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>