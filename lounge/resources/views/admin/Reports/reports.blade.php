@extends('admin.layouts.app')

@section('content')

<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">

            <!-- /# row -->
            <section id="main-content">
                <div class="row">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-title p-b-10">
                                <h4>Reports</h4>
                            </div>
                            <div class="card-body">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs nav-fill" id="reports" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="sales-tab" data-toggle="tab" href="#sales"
                                            role="tab"><span class="hidden-sm-up"><i class="ti-money"></i></span> <span
                                                class="hidden-xs-down">Sales</span></a> </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="saleslinechart-tab" data-toggle="tab"
                                            href="#saleslinechart" role="tab"><span class="hidden-sm-up"><i
                                                    class="ti-stats-up"></i></span> <span class="hidden-xs-down">Sales (Line
                                                Chart)</span></a> </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="destination-tab" data-toggle="tab" href="#destination"
                                            role="tab"><span class="hidden-sm-up"><i class="ti-bar-chart-alt"></i></span> <span
                                                class="hidden-xs-down">Destination Report</span></a> </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="hourlysales-tab" data-toggle="tab" href="#hourlysales"
                                            role="tab"><span class="hidden-sm-up"><i class="ti-timer"></i></span> <span
                                                class="hidden-xs-down">Hourly Sales</span></a> </li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content tabcontent-border">
                                    <!-- start - Sales -->
                                    <div class="tab-pane active" id="sales" role="tabpanel" aria-labelledby="sales-tab">
                                        <div class="row p-t-30 p-l-30 p-r-30">
                                            <div class="col-lg-12">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div class="horizontal-form">
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                            <label
                                                                                for="filter_category">Category</label>
                                                                            <select
                                                                                class="form-control select-2-filter_category"
                                                                                id="filter_category"
                                                                                name="filter_category[]"
                                                                                multiple="multiple">
                                                                                @foreach ($categories as $category)
                                                                                <option value="{{ $category->id }}">
                                                                                    {{ $category->name }}
                                                                                </option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                            <label for="filter_diet">Diet</label>
                                                                            <select
                                                                                class="form-control select-2-filter_diet"
                                                                                id="filter_diet" name="filter_diet[]"
                                                                                multiple="multiple">
                                                                                @foreach ($diets as $diet)
                                                                                <option value="{{ $diet->id }}">
                                                                                    {{ $diet->name }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                            <label for="daterange-sales">Date</label>
                                                                            <div id="daterange-sales"
                                                                                style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100% ">
                                                                                <i class="fa fa-calendar"></i>&nbsp;
                                                                                <i class="fa fa-caret-down"></i>
                                                                                <span></span> 
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-1 m-t-23">
                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                            <button style="cursor: pointer;" type="button" id='filter'
                                                                                class="btn btn-success">Filter</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-5 m-t-23">
                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                            <button style="cursor: pointer;" type="button" id='clear_filter'
                                                                                class="btn btn-danger">Clear Filter</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /# column -->
                                        </div>
                                        <div class="row p-t-30 p-l-30 p-r-30">
                                            <div class="col-lg-4">
                                                <div class="card">
                                                    <div class="stat-widget-one">
                                                        <div class="stat-icon dib"><i
                                                                class="color-success border-success aed-ti-money">AED</i></div>
                                                        <div class="stat-content dib">
                                                            <div class="stat-text">Total Sales</div>
                                                            <div class="stat-digit" id="total_sales"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="card">
                                                    <div class="stat-widget-one">
                                                        <div class="stat-icon dib"><i
                                                                class="ti-user color-primary border-primary"></i></div>
                                                        <div class="stat-content dib">
                                                            <div class="stat-text">Total Customers</div>
                                                            <div class="stat-digit" id="users_count"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="card">
                                                    <div class="stat-widget-one">
                                                        <div class="stat-icon dib"><i
                                                                class="ti-user color-primary border-primary"></i></div>
                                                        <div class="stat-content dib">
                                                            <div class="stat-text">Total Repeated Customers</div>
                                                            <div class="stat-digit" id="repeated_users_count"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row p-t-30 p-l-30 p-r-30">
                                            <div class="col-lg-6">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <h4 class="card-title p-b-25">Category </h4>
                                                        <div id='category-pie'></div>
                                                        <div id="category-pie-legend" class="pietopcorner"></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-6">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <h4 class="card-title p-b-25">Diet</h4>
                                                        <div id='diet-pie'></div>
                                                        <div id="diet-pie-legend" class="pietopcorner"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row p-t-30 p-l-30 p-r-30 p-b-30">
                                            <div class="col-lg-6">
                                                <div class="card">
                                                    <div class="card-title">
                                                        <h4>Top 5 Categories </h4>
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="table-responsive">
                                                            <table class="table table-striped">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Category</th>
                                                                        <th>Price</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="top-categories"></tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="card">
                                                    <div class="card-title">
                                                        <h4>Top 5 Diets </h4>
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="table-responsive">
                                                            <table class="table table-striped">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Diet</th>
                                                                        <th>Price</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="top-diets"></tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 p-t-30">
                                                <div class="card">
                                                    <div class="card-title">
                                                        <h4>Top 5 Products </h4>
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="table-responsive">
                                                            <table class="table table-striped">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Product</th>
                                                                        <th>Price</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="top-products"></tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end - Sales -->
                                    <!-- start - Sales(Line Chart) -->
                                    <div class="tab-pane active" id="saleslinechart" role="tabpanel"
                                        aria-labelledby="saleslinechart-tab">
                                        <div class="row p-t-30 p-l-30 p-r-30">
                                            <div class="col-lg-12">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div class="horizontal-form">
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                            <label
                                                                                for="filter_category_sales_linechart">Category</label>
                                                                            <select
                                                                                class="form-control select-2-filter_category_sales_linechart"
                                                                                id="filter_category_sales_linechart"
                                                                                name="filter_category_sales_linechart[]"
                                                                                multiple="multiple">
                                                                                @foreach ($categories as $category)
                                                                                <option value="{{ $category->id }}">
                                                                                    {{ $category->name }}
                                                                                </option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                            <label
                                                                                for="filter_diet_sales_linechart">Diet</label>
                                                                            <select
                                                                                class="form-control select-2-filter_diet_sales_linechart"
                                                                                id="filter_diet_sales_linechart"
                                                                                name="filter_diet_sales_linechart[]"
                                                                                multiple="multiple">
                                                                                @foreach ($diets as $diet)
                                                                                <option value="{{ $diet->id }}">
                                                                                    {{ $diet->name }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                            <label
                                                                                for="daterange-sales-linechart">Date</label>
                                                                            <div id="daterange-sales-linechart"
                                                                                style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100% ">
                                                                                <i class="fa fa-calendar"></i>&nbsp;
                                                                                <i class="fa fa-caret-down"></i>
                                                                                <span></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-1 m-t-23">
                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                            <button style="cursor: pointer;" type="button"
                                                                                id='filter_saleslinechart'
                                                                                class="btn btn-success">Filter</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-5 m-t-23">
                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                            <button style="cursor: pointer;" type="button" id='clear_filter_saleslinechart'
                                                                                class="btn btn-danger">Clear Filter</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /# column -->
                                        </div>
                                        <div class="row p-t-30 p-l-30 p-r-30">
                                            <div class="col-lg-4">
                                                <div class="card">
                                                    <div class="stat-widget-one">
                                                        <div class="stat-icon dib"><i
                                                                class="color-success border-success aed-ti-money">AED</i></div>
                                                        <div class="stat-content dib">
                                                            <div class="stat-text">Total Sales</div>
                                                            <div class="stat-digit" id="total_sales_linechart"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="card">
                                                    <div class="stat-widget-one">
                                                        <div class="stat-icon dib"><i
                                                                class="ti-user color-primary border-primary"></i></div>
                                                        <div class="stat-content dib">
                                                            <div class="stat-text">Total Customers</div>
                                                            <div class="stat-digit" id="users_count_linechart"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="card">
                                                    <div class="stat-widget-one">
                                                        <div class="stat-icon dib"><i
                                                                class="ti-user color-primary border-primary"></i></div>
                                                        <div class="stat-content dib">
                                                            <div class="stat-text">Total Repeated Customers</div>
                                                            <div class="stat-digit" id="repeated_users_count_linechart">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row p-t-30 p-l-30 p-r-30 p-b-30">
                                            <div class="card w-100 h-100">
                                                <div id="sales-linegraph-legend" class="topcorner"></div>
                                                <h5 class="card-header">Sales Line Graph</h5>
                                                <div class="card-body p-l-22 p-t-140" id='saleslinegraph'
                                                    style="height: 600px">
                                                    
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end - Sales(Line Chart) -->
                                    <!-- start - Destination Report-->
                                    <div class="tab-pane active" id="destination" role="tabpanel"
                                        aria-labelledby="destination-tab">
                                        <div class="row p-t-30 p-l-30 p-r-30">
                                            <div class="col-lg-12">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div class="horizontal-form">
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                            <label
                                                                                for="filter_category_destination">Category</label>
                                                                            <select
                                                                                class="form-control select-2-filter_category_destination"
                                                                                id="filter_category_destination"
                                                                                name="filter_category_destination[]"
                                                                                multiple="multiple">
                                                                                @foreach ($categories as $category)
                                                                                <option value="{{ $category->id }}">
                                                                                    {{ $category->name }}
                                                                                </option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                            <label
                                                                                for="filter_diet_destination">Diet</label>
                                                                            <select
                                                                                class="form-control select-2-filter_diet_destination"
                                                                                id="filter_diet_destination"
                                                                                name="filter_diet_destination[]"
                                                                                multiple="multiple">
                                                                                @foreach ($diets as $diet)
                                                                                <option value="{{ $diet->id }}">
                                                                                    {{ $diet->name }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                            <label
                                                                                for="daterange-destination">Date</label>
                                                                            <div id="daterange-destination"
                                                                                style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100% ">
                                                                                <i class="fa fa-calendar"></i>&nbsp;
                                                                                <i class="fa fa-caret-down"></i>
                                                                                <span></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-1 m-t-23">
                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                            <button style="cursor: pointer;" type="button"
                                                                                id='filter_destination'
                                                                                class="btn btn-success">Filter</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-5 m-t-23">
                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                            <button style="cursor: pointer;" type="button" id='clear_filter_destination'
                                                                                class="btn btn-danger">Clear Filter</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /# column -->
                                        </div>
                                        <div class="row p-t-30 p-l-30 p-r-30">
                                            <div class="col-lg-4">
                                                <div class="card">
                                                    <div class="stat-widget-one">
                                                        <div class="stat-icon dib"><i
                                                                class="color-success border-success aed-ti-money">AED</i></div>
                                                        <div class="stat-content dib">
                                                            <div class="stat-text">Total Sales</div>
                                                            <div class="stat-digit" id="total_sales_destination"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="card">
                                                    <div class="stat-widget-one">
                                                        <div class="stat-icon dib"><i
                                                                class="ti-user color-primary border-primary"></i></div>
                                                        <div class="stat-content dib">
                                                            <div class="stat-text">Total Customers</div>
                                                            <div class="stat-digit" id="users_count_destination"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="card">
                                                    <div class="stat-widget-one">
                                                        <div class="stat-icon dib"><i
                                                                class="ti-user color-primary border-primary"></i></div>
                                                        <div class="stat-content dib">
                                                            <div class="stat-text">Total Repeated Customers</div>
                                                            <div class="stat-digit"
                                                                id="repeated_users_count_destination"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row p-t-30 p-l-30 p-r-30 p-b-30">
                                            <div class="card w-100 h-100">
                                                <h5 class="card-header">Destination Bar Graph</h5>
                                                <div class="card-body p-l-22" id='destinationgraph' style="height:450px;">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end - Destination Report-->
                                    <!-- start - Hourly Sales-->
                                    <div class="tab-pane active" id="hourlysales" role="tabpanel"
                                        aria-labelledby="hourlysales-tab">
                                        <div class="row p-t-30 p-l-30 p-r-30">
                                            <div class="col-lg-12">
                                                <div class="card">
                                                    <div class="card-body">
                                                        <div class="horizontal-form">
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                            <label
                                                                                for="daterange-hourlysales">Date</label>
                                                                            <div id="daterange-hourlysales"
                                                                                style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100% ">
                                                                                <i class="fa fa-calendar"></i>&nbsp;
                                                                                <i class="fa fa-caret-down"></i>
                                                                                <span></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-1 m-t-23">
                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                            <button style="cursor: pointer;" type="button"
                                                                                id='filter_hourlysales'
                                                                                class="btn btn-success">Filter</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-5 m-t-23">
                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                            <button style="cursor: pointer;" type="button" id='clear_filter_hourlysales'
                                                                                class="btn btn-danger">Clear Filter</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /# column -->
                                        </div>
                                        <div class="row p-t-30 p-l-30 p-r-30">
                                            <div class="col-lg-4">
                                                <div class="card">
                                                    <div class="stat-widget-one">
                                                        <div class="stat-icon dib"><i
                                                                class="color-success border-success aed-ti-money">AED</i></div>
                                                        <div class="stat-content dib">
                                                            <div class="stat-text">Total Sales</div>
                                                            <div class="stat-digit" id="total_sales_hourly"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="card">
                                                    <div class="stat-widget-one">
                                                        <div class="stat-icon dib"><i
                                                                class="ti-user color-primary border-primary"></i></div>
                                                        <div class="stat-content dib">
                                                            <div class="stat-text">Total Customers</div>
                                                            <div class="stat-digit" id="users_count_hourly"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="card">
                                                    <div class="stat-widget-one">
                                                        <div class="stat-icon dib"><i
                                                                class="ti-user color-primary border-primary"></i></div>
                                                        <div class="stat-content dib">
                                                            <div class="stat-text">Total Repeated Customers</div>
                                                            <div class="stat-digit" id="repeated_users_count_hourly"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row p-t-30 p-l-30 p-r-30 p-b-30">
                                            <div class="card w-100 h-100">
                                                <h5 class="card-header">Hourly Sales Line Graph</h5>
                                                <div class="card-body p-l-22">
                                                    <div class="row">&nbsp;</div>
                                                    <div id='hourlysaleslinegraph' style="height:450px"></div>
                                                    <!--<div id="hourlysaleslinegraph-legend" class="line-legend"></div> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end - Hourly Sales -->
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- /# row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer">
                            <p>2020 © carryOn DXB Admin.</p>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

@endsection


@push('scripts')
<script type="text/javascript">
    var base_url = "{{ URL::to('/') }}";
</script>
<script src="{{ asset('dxblounge/js/lib/Morris/raphael.min.js') }}"></script>
<script src="{{ asset('dxblounge/js/lib/Morris/morris.min.js') }}"></script>
<script src="{{ asset('dxblounge/js/lib/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('dxblounge/js/lib/spinner/spin.js') }}"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="{{ asset('dxblounge/js/reports/categories.js') }}"></script>
@endpush