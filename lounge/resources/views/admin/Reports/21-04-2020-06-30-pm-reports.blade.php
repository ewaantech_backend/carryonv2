@extends('admin.layouts.app')

@section('content')

<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">

            <!-- /# row -->
            <section id="main-content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card list-block">
                            <div class="card-title">
                                <h4>Reports</h4>
                            </div>
                            <div class="invoice-edit">
                                <!--.row-->
                                <div class="invoicelist-body">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-pills nav-fill" id="reports" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="sales-tab" data-toggle="pill" href="#sales"
                                                role="tab" aria-controls="sales" aria-selected="true">Sales</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="saleslinechart-tab" data-toggle="pill"
                                                href="#saleslinechart" role="tab" aria-controls="saleslinechart"
                                                aria-selected="false">Sales (line chart)</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="destination-tab" data-toggle="pill"
                                                href="#destination" role="tab" aria-controls="destination"
                                                aria-selected="false">Destination</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="hourlysales-tab" data-toggle="pill"
                                                href="#hourlysales" role="tab" aria-controls="hourlysales"
                                                aria-selected="false">Hourly Sales</a>
                                        </li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="sales" role="tabpanel"
                                            aria-labelledby="sales-tab">
                                            <div class="row">
                                                &nbsp;
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-sm-3">
                                                    Category
                                                    <select class="form-control select-2-filter_category"
                                                        id="filter_category" name="filter_category[]"
                                                        multiple="multiple">
                                                        @foreach ($categories as $category)
                                                        <option value="{{ $category->id }}">{{ $category->name }}
                                                        </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-sm-3">
                                                    Diet
                                                    <select class="form-control select-2-filter_diet" id="filter_diet"
                                                        name="filter_diet[]" multiple="multiple">
                                                        @foreach ($diets as $diet)
                                                        <option value="{{ $diet->id }}">{{ $diet->name }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-sm-3">
                                                    Date
                                                    <div id="daterange-sales"
                                                        style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100% ">
                                                        <i class="fa fa-calendar"></i>&nbsp;
                                                        <span></span> <i class="fa fa-caret-down"></i>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <br>
                                                    <button type="button" id='filter'
                                                        class="btn btn-success">Filter</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="card">
                                                        <h5 class="card-header">Top 5 Category</h5>
                                                        <div class="card-body">
                                                            <ul class="list-group list-group-flush" id='top-categories'>

                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="card">
                                                        <h5 class="card-header">Top 5 Diet</h5>
                                                        <div class="card-body">
                                                            <ul class="list-group list-group-flush" id='top-diets'>

                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="card h-75">
                                                        <h5 class="card-header">Total Sales</h5>
                                                        <div class="card-body" id="total_sales">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="row-sm-6">
                                                        <div class="card">
                                                            <h5 class="card-header">Customers</h5>
                                                            <div class="card-body">
                                                                <div class="row-sm-6">
                                                                    <div class="card">
                                                                        <h5 class="card-header">Total no: of customers
                                                                        </h5>
                                                                        <div class="card-body" id="users_count">
                                                                            &nbsp;

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row-sm-6">
                                                                    <div class="card">
                                                                        <h5 class="card-header">Total no: of repeated
                                                                            customers</h5>
                                                                        <div class="card-body"
                                                                            id="repeated_users_count">
                                                                            &nbsp;

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="card">
                                                        <h5 class="card-header">Category Piechart</h5>
                                                        <div class="card-body">
                                                            &nbsp;
                                                            <div id='category-pie'></div>
                                                            <div id="category-pie-legend" class="donut-legend"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="card">
                                                        <h5 class="card-header">Diet Piechart</h5>
                                                        <div class="card-body">
                                                            &nbsp;
                                                            <div id='diet-pie'></div>
                                                            <div id="diet-pie-legend" class="donut-legend"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="card">
                                                        <h5 class="card-header">Top 5 Products</h5>
                                                        <div class="card-body">
                                                            <ul class="list-group list-group-flush" id="top-products">

                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="saleslinechart" role="tabpanel"
                                            aria-labelledby="saleslinechart-tab">
                                            <div class="row">&nbsp;</div>
                                            <div class="form-group row">
                                                <div class="col-sm-2">
                                                    <label for="filter_category">
                                                        Category
                                                        <select class="form-control select-2" id="filter_category"
                                                            name="filter_category">
                                                            <option value="0">--Select Category--</option>
                                                            <option value="1">Category 1</option>
                                                            <option value="2">Category 2</option>
                                                            <option value="3">Category 3</option>
                                                        </select>
                                                </div>
                                                <div class="col-sm-2">
                                                    <label for="filter_category">
                                                        Diet
                                                        <select class="form-control select-2" id="filter_category"
                                                            name="filter_category">
                                                            <option value="0">--Select Diet--</option>
                                                            <option value="1">Diet 1</option>
                                                            <option value="2">Diet 2</option>
                                                            <option value="3">Diet 3</option>
                                                        </select>
                                                </div>
                                                <div class="col-sm-2">
                                                    <label for="filter_category">
                                                        Date
                                                        <select class="form-control select-2" id="filter_category"
                                                            name="filter_category">
                                                            <option value="0">--Select Diet--</option>
                                                            <option value="1">Today</option>
                                                            <option value="2">Yesterday</option>
                                                            <option value="3">Last 7 days</option>
                                                            <option value="4">Last 30 days</option>
                                                            <option value="4">This month</option>
                                                            <option value="4">Last month</option>
                                                            <option value="4">Custom Range</option>
                                                        </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="card w-100 h-100">
                                                    <h5 class="card-header">Sales Line Graph</h5>
                                                    <div class="card-body" id='saleslinegraph'
                                                        style="height: 450px; width:650px">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="destination" role="tabpanel"
                                            aria-labelledby="destination-tab">
                                            <div class="row">&nbsp;</div>
                                            <div class="form-group row">
                                                <div class="col-sm-2">
                                                    <label for="filter_category">
                                                        Category
                                                        <select class="form-control select-2" id="filter_category"
                                                            name="filter_category">
                                                            <option value="0">--Select Category--</option>
                                                            <option value="1">Category 1</option>
                                                            <option value="2">Category 2</option>
                                                            <option value="3">Category 3</option>
                                                        </select>
                                                </div>
                                                <div class="col-sm-2">
                                                    <label for="filter_category">
                                                        Diet
                                                        <select class="form-control select-2" id="filter_category"
                                                            name="filter_category">
                                                            <option value="0">--Select Diet--</option>
                                                            <option value="1">Diet 1</option>
                                                            <option value="2">Diet 2</option>
                                                            <option value="3">Diet 3</option>
                                                        </select>
                                                </div>
                                                <div class="col-sm-2">
                                                    <label for="filter_category">
                                                        Date
                                                        <select class="form-control select-2" id="filter_category"
                                                            name="filter_category">
                                                            <option value="0">--Select Date--</option>
                                                            <option value="1">Today</option>
                                                            <option value="2">Yesterday</option>
                                                            <option value="3">Last 7 days</option>
                                                            <option value="4">Last 30 days</option>
                                                            <option value="4">This month</option>
                                                            <option value="4">Last month</option>
                                                            <option value="4">Custom Range</option>
                                                        </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="card w-100 h-100">
                                                    <h5 class="card-header">Destination Bar Graph</h5>
                                                    <div class="card-body" id='destinationgraph'>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="hourlysales" role="tabpanel"
                                            aria-labelledby="hourlysales-tab">
                                            <div class="row">&nbsp;</div>
                                            <div class="form-group row">
                                                <div class="col-sm-2">
                                                    <label for="filter_category">
                                                        Date
                                                        <select class="form-control select-2" id="filter_category"
                                                            name="filter_category">
                                                            <option value="0">--Select Date--</option>
                                                            <option value="1">Today</option>
                                                            <option value="2">Yesterday</option>
                                                            <option value="4">Custom Range</option>
                                                        </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="card w-100 h-100">
                                                    <h5 class="card-header">Hourly Sales Line Graph</h5>
                                                    <div class="card-body" id='hourlysaleslinegraph'
                                                        style="height: 450px; width:950px">

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="card w-100 h-100">
                                                    <h5 class="card-header">Top 5 time period</h5>
                                                    <div class="card-body">
                                                        <ul class="list-group list-group-flush">
                                                            <li class="list-group-item">04 PM - 100 Sales</li>
                                                            <li class="list-group-item">06 PM - 200 Sales</li>
                                                            <li class="list-group-item">08 PM - 240 Sales</li>
                                                            <li class="list-group-item">09 PM - 250 Sales</li>
                                                            <li class="list-group-item">10 PM - 300 Sales</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- /# card -->
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer">
                            <p>2020 © carryOn DXB Admin.</p>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

@endsection


@push('scripts')
<script type="text/javascript">
    var base_url = "{{ URL::to('/') }}";
</script>
<script src="{{ asset('dxblounge/js/lib/Morris/raphael.min.js') }}"></script>
<script src="{{ asset('dxblounge/js/lib/Morris/morris.min.js') }}"></script>
<script src="{{ asset('dxblounge/js/lib/select2/select2.full.min.js') }}"></script>
<script src="{{ asset('dxblounge/js/lib/spinner/spin.js') }}"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script src="{{ asset('dxblounge/js/reports/categories.js') }}"></script>
@endpush