
@extends('admin.layouts.app')

@section('content')

    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                
                <!-- /# row -->
                <section id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card list-block">
                                <div class="card-title">
                                    <h4>Category List</h4>
                                </div>
                                <div class="invoice-edit">
                                    
                                    <!--.row-->
                                    <div class="invoicelist-body">
                                        <div class="bootstrap-data-table-panel">
                                            <div class="table-responsive">
                                                <table  id="bootstrap-data-table" class="table table-striped table-bordered">
                                                    <thead>
                                                        <tr>
                                                        <th>No</th>
                                                          <th>Name</th>
                                                            <th>Description</th>
                                                            <th>visibility</th>
                                                            <th>created</th>
                                                            <th>updated</th>
                                                            <th>postion</th>
                                                            <th>Action</th>
                                                           
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if (count($categories) > 0)
                                                        @php
                                                        $count = 1;
                                                        @endphp
                                                        @foreach ($categories as $category)
                                                                    <tr>
                                                                    <td>{{$count++}}</td>
                                                                    <td>{{$category->name}}</td>
                                                                    <td>{{$category->description}}</td>
                                                                    <td>{{$category->visibility}}</td>
                                                                    <td>{{ date_format_custom($category->created_at) }}</td>
                                                                    <td>{{ date_format_custom($category->updated_at) }}</td>
                                                                    <td>{{$category->position}}</td>
                                                                    <td><a class="delete-item"  data-item={{$category->id}} ><i style='margin-left: 10px;color:red;' class="ti-trash pull-right"></i></a><a class="edit-item" data-item={{$category->id}}><span><i style='color:blue;' class="ti-pencil-alt pull-right"></span></i> </td></a>
                                                                    </tr>
                                                    @endforeach
                                                    @else
                                                        <tr>
                                                            <td colspan="4" class="text-center"> No Categories Available</td>
                                                        </tr>
                                                    @endif  
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <a class="control newRow add-new-item" href="#">+ Add Category</a>
                                    </div>
                                </div>
                            </div>

                            <div class="edit-add-block">
                                <div class="card">
                                    <div class="card-title">
                                        <h4>Category Add/Edit</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="form-validation">
                                        <form id="category-add" class="form-valide add-form" method="post">
                                                <input type="hidden" id="category-id" name="category-id">
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Category Name <span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control" id="category_name" name="category_name" placeholder="Enter a Category Name">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Description <span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control" id="description_name" name="description_name" placeholder="Enter description">
                                                    </div>
                                                </div>
                                                                                              
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Visibility<span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                        <select class="form-control" id="visibility" name="visibility">
                                                            <option value="YES">YES</option>
                                                            <option value="NO">NO</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Position <span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control" id="position" name="position" >
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Image<span class="text-danger">*</span></label>
                                                    <div class="col-lg-8" id="thumb-img">
                                                        <button style="cursor:pointer;background-color: transparent;border-color:#aaa;" class="btn btn-default" type="button" 
                                                            onclick="onButtonClicked('single_image', 'content')">
                                                            <div class="stat-icon">
                                                                <i style="color:blue;font-size:25px;" class="ti-cloud-up">
                                                                    <span style="color:#495057;font-size:17px;top:-6px;position: relative;font-family: 'Roboto', sans-serif;"> Upload image</span>
                                                                </i>
                                                            </div>
                                                        </button>
                                                        <div id="content"></div>
                                                        <div id="content-hidden" style="display:none;"></div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Alternative Image<span class="text-danger">*</span></label>
                                                    <div class="col-lg-8" id="thumb-img">
                                                        <button style="cursor:pointer;background-color: transparent;border-color:#aaa;" class="btn btn-default" type="button" 
                                                            onclick="onButtonClicked('alternative_image', 'altcontent')">
                                                            <div class="stat-icon">
                                                                <i style="color:blue;font-size:25px;" class="ti-cloud-up">
                                                                    <span style="color:#495057;font-size:17px;top:-6px;position: relative;font-family: 'Roboto', sans-serif;"> Upload image</span>
                                                                </i>
                                                            </div>
                                                        </button>
                                                        <div id="altcontent"></div>
                                                        <div id="altcontent-hidden" style="display:none;"></div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-8 ml-auto">
                                                        <button type="button" class="btn btn-default cancel-form">Cancel</button>   
                                                        <button type="submit" class="btn btn-primary">Submit</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /# card -->
                        </div>
                        <!-- /# column -->
                    </div>
                    <!-- /# row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>2020 © carryOn DXB Admin.</p>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    @endsection
    @push('scripts')

<script src="{{ asset('dxblounge/js/lib/select2/select2.full.min.js') }}"></script>
<!-- Form validation -->
<script src="{{ asset('dxblounge/js/lib/form-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('dxblounge/js/lib/form-validation/jquery.validate-init.js') }}"></script>

<script>
  $(document).ready(function(){
    // CKEDITOR.replace('html_content');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var form = $( "#category-add" );
    form.validate();
    $( "#category-add" ).on("submit", function() {
        if(form.valid() == true) {
            // var data = form.serialize();
            let data = new FormData( form[0] );
            $('#overlay').show();
            $.ajax({
                url: form.attr('action'),
                method: "post",
                data: data,
                processData: false,
                contentType: false,
                success: function(data) {
                    data = JSON.parse(data);
                    // $('.list-block').show();
                    // $('.edit-add-block').hide();
                    if(data.status == true) {
                        swal({
                            title: "Success",
                            text: "Saved successfully",
                            timer: 2000,
                            showConfirmButton: false
                        });
                        setTimeout(() => {
                            location.reload();
                        }, 2000);
                    } else {
                        swal({
                            title: "Error",
                            text: data.message,
                            timer: 2000,
                            showConfirmButton: false
                        });
                    }
                },
                complete: function() {
                    $('#overlay').hide();
                }
            });
        }
    });
    
    $('.add-new-item').on('click', function() {
        $( "#category-add" ).attr('action', "<?php echo route('category_add')?>");
        $('#cms').val('');
        $('#content').html('');
        $('#category-id').val('');
        $('#category_name').val('');
        $('#description_name').val('');
        $('#visibility').val('');
        $('#position').val('');
    });

    $('.edit-item').on("click", function() {
        var id = $(this).data('item');
       // alert(id);
        $( "#category-add" ).attr('action', "<?php echo route('category_edit')?>");
        $('#category-id').val(id);
        $('#content').html('');
        $('#overlay').show();
        $.ajax({
            url: "<?php echo route('category_data')?>",
            method: "post",
            data: {
                id: id
            },
            success: function(data) {
                data = JSON.parse(data);
                if(data.status == true) {
                    $('#category_name').val(data.cms.name);
                    $('#description_name').val(data.cms.description);
                    $('#category-id').val(id);
                    $('#visibility').val(data.cms.visibility);
                    $('#position').val(data.cms.position);
                    var appUrl = "{{ env('APP_URL') }}/public";
                    if( data.cms.image_path != null) {
                        $('#content').html(`<img src="${appUrl}${data.cms.image_path}" 
                        style="width: 200px; height: 200px; margin-top: 25px;margin-right: 10px;">`);
                    }
                    if( data.cms.alternative_image_path != null) {
                        $('#altcontent').html(`<img src="${appUrl}${data.cms.alternative_image_path}" 
                        style="width: 200px; height: 200px; margin-top: 25px;margin-right: 10px;">`);
                    }
                    $('.list-block').hide();
                    $('.edit-add-block').show();
                }
            },
            complete: function() {
                $('#overlay').hide();
            }
        });
    });
});

    async function onButtonClicked(name, container){
        let files = await selectFile("image/*", false, name, container);
        $('#' + container).html(`<img src="${URL.createObjectURL(files)}" style="width: 200px; height: 200px; margin-top: 25px;margin-right: 10px;">`);
    }

    function selectFile (contentType, multiple, name, container){
        return new Promise(resolve => {
            let input = document.createElement('input');
            input.type = 'file';
            input.multiple = multiple;
            input.accept = contentType;
            input.name=name;

            input.onchange = _ => {
                let files = Array.from(input.files);
                if (multiple)
                    resolve(files);
                else
                    resolve(files[0]);
            };
            if(name == 'single_image' || name == 'alternative_image') {
                $('#'+container+'-hidden').html(input);
            } else {
                $('#'+container+'-hidden').append(input);
            }
            input.click();
        });
    }
    
    $('.delete-item').on("click", function() {
            var id = $(this).data('item');
              //alert(id);
            $('#overlay').show();
            $.ajax({
                url: "<?php echo route('category_destroy')?>",
                method: "post",
                data: {
                    id: id
                },
                success: function(data) {
                    data = JSON.parse(data);

                        if(data.status == true) {
                            swal({
                                title: "Success",
                                text: "Deleted successfully",
                                timer: 2000,
                                showConfirmButton: false
                            });
                            setTimeout(() => {
                                location.reload();
                            }, 1000);
                        } else {
                            swal({
                                title: "Error",
                                text: data.message,
                                timer: 2000,
                                showConfirmButton: false
                            });
                        }
                },
                complete: function() {
                    $('#overlay').hide();
                }
            });
        });           
</script>
@endpush