@extends('admin.layouts.app')
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
      $(document).ready(function(){
         setTimeout(function(){
          $("#success_message").remove();
             }, 2000);
      });
    </script>
    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>CMS Slider</h1>
                            </div>
                        </div>
                    </div>
                 </div>
                 @if (session('status'))
                      <div class="alert alert-success" id="success_message" role="alert">
                        {{ session('status') }}
                      </div>
                    @endif
                    <section id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-title">
                                    <h4>CMS Sliders List </h4>
                                    
                                </div>
                                @php
                                if($allergies[0]->short_code  == NULL){
                                @endphp                                
                                                                <div class="text-right">
                               <a href="{{asset('dxblounge//admin/cms_slider/create')}}"> <button type="button" class="btn btn-info btn-sm m-b-10 m-l-5"> +Add New</button> </a>
                               </div>
                                 @php
                                 }
                                 @endphp
                                <div class="bootstrap-data-table-panel">
                                    <div class="table-responsive">
                                        <table id="row-select" class="display table table-borderd table-hover ">
                                            <thead>
                                                <tr>
                                                <th>No</th>
                                                <th>Title</th>
                                                <th>Visibility</th>
                                               @php
                                                if($allergies[0]->short_code  == NULL){
                                                @endphp                                
                                                <th>Position</th>
                                                 @php
                                                 }
                                                 @endphp
                                                <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                    @if (count($allergies) > 0)
                                    @php
                                    $count = 1;
                                    @endphp
                                    @foreach ($allergies as $allergiey)
                                                <tr>
                                                <td>{{$count++}}</td>
                                                <td>{{$allergiey->title}}</td>
                                                <td>{{$allergiey->visibility}}</td>
                                                
                                                @php
                                                if($allergiey->short_code == NULL){
                                                @endphp
                                                <td>{{$allergiey->position}}</td>
                                                <td><a href="{{url('admin/cms_slider/edit',$allergiey->id)}}"><i style='color:blue;' class="ti-pencil-alt "></span></i></a>
                                                <a href="{{url('admin/cms_slider/delete',$allergiey->id)}}"><i style='margin-left: 10px;color:red;' class="ti-trash "></a></td>
                                                 @php
                                                 }else{
                                                 @endphp
                                                <td><a href="{{url('admin/cms_slider/edit',$allergiey->id)}}"><i style='color:blue;' class="ti-pencil-alt "></span></i></a></td>
                                                 @php
                                                 }
                                                 @endphp
                                                </tr>
                                  @endforeach
                                  @else
                                    <tr>
                                        <td colspan="4" class="text-center"> No slider Available</td>
                                    </tr>
                                  @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                         </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

</div>
 @endsection