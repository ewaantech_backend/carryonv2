
@extends('admin.layouts.app')
@section('content')
<style>
#errmsg
{
color: red;
font-size: 15px;
}
</style>

<div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Update CMS Slider</h1>
                            </div>
                        </div>
                    </div>
                   
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb">
                                    <!-- <li class="breadcrumb-item"><a href="#">Create Allergies</a></li> -->
                                </ol>
                            </div>
                        </div>
                    </div>
                    </div>
                                              
                                    @php
                                    $count = 1;
                                    @endphp
                                    @foreach ($allergies as $allergiey)
                                    @php
                                    $aller_id= $allergiey->id; 
                                    $cms_name= $allergiey->title;
                                    $cms_description= $allergiey->description;

                                    $link= $allergiey->link;
                                    $link_text= $allergiey->link_text;

                                    $cms_visibility= $allergiey->visibility;
                                    $cms_position= $allergiey->position;
                                    $cms_image= $allergiey->image_path;
                                    $cover_image= $allergiey->cover_image_path;

                                    @endphp
                                  
                <section id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="form-validation">
                                        <form class="form-valide direct_submit" action="{{ route('cmsslider_update') }}"  method="POST" id="allergies-add" enctype="multipart/form-data">
                                        <div class="alert alert-danger print-error-msg" style="display:none">
                                        <ul></ul>
                                        </div>
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" value="{{ $aller_id }}" name="aller_id"> 
                                        <div class="form-group row">
                                                <label class="col-lg-4 col-form-label" for="val-currency">Name  <span class="text-danger">*</span></label>
                                                <div class="col-lg-6">
                                                <input type="text" class="form-control" id="name" name="name"  value="{{$cms_name}}" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label" for="val-website">Description</label>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <textarea  class="form-control" rows="50" cols="50" id="cms_description" name="cms_description" value="">{{ $cms_description }}</textarea>
                                                    </div>
                                                </div>
                                            </div>

                                        @if( $allergies[0]->short_code == NULL )
                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label" for="val-currency">Link</label>
                                                <div class="col-lg-6">
                                                    <input type="text" value="{{ $link }}" class="form-control" id="link" name="link" >
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label" for="val-currency">Link Text</label>
                                                <div class="col-lg-6">
                                                    <input type="text" value="{{ $link_text }}" class="form-control" id="link_text" name="link_text" >
                                                </div>
                                            </div>
                                        @endif

                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label" for="val-skill">visibility <span class="text-danger">*</span></label>
                                                <div class="col-lg-6">
                                                    <select class="form-control" id="visibility" name="visibility" required>
                                                        <option @if($cms_visibility=='YES') selected @endif value="YES">YES</option>
                                                        <option @if($cms_visibility=='NO') selected @endif value="NO">NO</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                                <label class="col-lg-4 col-form-label" for="val-currency">Position  <span class="text-danger">*</span></label>
                                                <div class="col-lg-6">
                                                    <input type="text" class="form-control" id="position" name="position"  value="{{$cms_position}}" required>
                                                    <span id="errmsg"></span>
                                                </div>
                                            </div>                                        
                                        <!-- <div class="form-group row">
                                                <label class="col-lg-4 col-form-label" for="val-currency">Position  <span class="text-danger">*</span></label>
                                                <div class="col-lg-6">
                                                    <input type="text" class="form-control" id="aller_position" name="position" required>
                                                </div>
                                        </div> -->
                                        <div class="form-group row">
                                                <label class="col-lg-4 col-form-label" for="val-website"> Image/Video <span class="text-danger">*</span></label>
                                                <div class="col-lg-6">
                                                    
                                                @php
                                                $contains = strpos($cms_image, 'mp4');
                                                if($contains){
                                                @endphp
                                                    <a target="_blank" href="{{asset("$cms_image")}}" style="font-weight:bolder;color:blue;">Click here for Video</a><br><br> 
                                                @php
                                                } else {
                                                @endphp
                                                    <img src="{{asset("$cms_image")}}" style="width:75px;height:75" class="img-thumbnail"><br><br>
                                                @php
                                                }
                                                @endphp
                                                <input type="file" class="form-control-file" id="image" name="image">
                                        </div>
                                        </div>
                                        <div class="form-group row">
                                                <label class="col-lg-4 col-form-label" for="val-website">Cover Image for Video <span class="text-danger">*</span></label>
                                                <div class="col-lg-6">
                                                @php
                                                $contains = strpos($cms_image, 'mp4');
                                                if($contains){
                                                @endphp
                                                    <img src="{{asset("$cover_image")}}" style="width:75px;height:75" class="img-thumbnail"><br><br>
                                                @php
                                                }
                                                @endphp
                                                <input type="file" class="form-control-file" id="cimage" name="cimage">
                                        </div>
                                        </div>
                                        <div class="form-group row">
                                                <div class="col-lg-8 ml-auto">
                                                    <button type="submit" class="btn btn-primary btn-sm m-b-10 m-l-5">Submit</button>
                                                    @php
                                                    if($allergies[0]->short_code != NULL){
                                                    @endphp
                                                    <button type="button" id="cancel_button_banners" class="btn btn-default btn-sm m-b-10 m-l-5">Cancel</button>
                                                    @php
                                                    } else {
                                                    @endphp
                                                    <button type="button" id="cancel_button" class="btn btn-default btn-sm m-b-10 m-l-5">Cancel</button>
                                                    @php
                                                    }
                                                    @endphp
                                                </div>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                </section>
                @endforeach
            </div>
        </div>
    </div>
@endsection



@push('scripts')

<script src="{{ asset('dxblounge/js/lib/select2/select2.full.min.js') }}"></script>
<!-- Form validation -->
<script src="{{ asset('dxblounge/js/lib/form-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('dxblounge/js/lib/form-validation/jquery.validate-init.js') }}"></script>

<!-- ajax validation -->
<!-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="http://malsup.github.com/jquery.form.js"></script>      

    <script>
        $(document).ready(function(){
          $("#cancel_button").click(function(){
            location.href = "{{url('admin/cms_slider')}}";
          });
          $("#cancel_button_banners").click(function(){
            location.href = "{{url('admin/cms_slider/banners')}}";
          });
        });
        $("#position").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg").html("Accept Number Only").show().fadeOut("slow");
               return false;
    }
   });
    </script>

@endpush