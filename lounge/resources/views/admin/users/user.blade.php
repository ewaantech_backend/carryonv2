@extends('admin.layouts.app')

@section('content')

    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                
                <!-- /# row -->
                <section id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card list-block">
                                <div class="card-title">
                                    <h4>Users</h4>
                                </div>
                                <div class="invoice-edit">
                                    
                                    <!--.row-->
                                    <div class="invoicelist-body">
                                        <div class="bootstrap-data-table-panel">
                                            <div class="table-responsive">
                                                <table  id="bootstrap-data-table" class="table table-striped table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>User #</th>
                                                            <th>Name</th>
                                                            <th style="width:100px !important;">Email</th>
                                                            <th>Password</th>
                                                            <th>Role</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                    @if (count($users) > 0)
                                                        
                                                        @foreach ($users as $user)
                                                            <tr>
                                                                <td><a class="edit-item user_{{$user->id}}" data-item={{$user->id}}>{{$user->id}}</a></td>
                                                                <td>{{$user->name}}</td>
                                                                <td>{{$user->email}}</td>
                                                                <td>{{$user->remember_token}}</td>
                                                                <td>{{$user->role}}</td>
                                                                <td></td>
                                                            </tr>    
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td colspan="4" class="text-center"> No Users Available</td>
                                                        </tr>
                                                    @endif

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        @if ( isset($permissions['users_add_edit']) && $permissions['users_add_edit'] == true )
                                        <a class="control newRow add-new-item" href="#">+ New User</a>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="edit-add-block">
                                <div class="card">
                                @if ( ! isset($permissions['users_add_edit']))
                                    <div class="alert alert-danger">
                                        You dont have permission to edit.
                                    </div>
                                    @endif
                                    <div class="card-title">
                                        <h4>User Add/Edit</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="form-validation">
                                            <form id="user-add" class="form-valide add-form" method="post">
                                                <input type="hidden" id="user_id" name="user_id">
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Name <span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter a name..">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Email <span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control" id="email" name="email" placeholder="Enter a valid email">
                                                    </div>
                                                </div>
                                                <div class="form-group row password">
                                                    <label class="col-lg-4 col-form-label">Password <span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                        <input type="password" class="form-control" id="password" name="password" placeholder="Enter a password">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Role <span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                        <select class="form-control" id="role_id" name="role_id">
                                                            <option value="">Please select</option>
                                                            @foreach ($roles as $role)
                                                                <option value="{{ $role->id }}">{{ $role->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row" id="pickup-person" style="display:none;">
                                                    <label class="col-lg-4 col-form-label">Pickup Point <span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                        <select class="form-control" id="pickup" name="pickup_point" disabled="disabled">
                                                            <option value="">Please select</option>
                                                            @foreach ($pickup_points as $pts)
                                                                <option value="{{ $pts->name }}">{{ $pts->name }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-lg-8 ml-auto">
                                                        <button type="button" class="btn btn-default cancel-form">Cancel</button>
                                                        @if ( isset($permissions['users_add_edit']) && $permissions['users_add_edit'] == true )
                                                        <button type="submit" class="btn btn-primary">Submit</button>
                                                        @endif
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /# card -->
                        </div>
                        <!-- /# column -->
                    </div>
                    <!-- /# row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>2020 © carryOn DXB Admin.</p>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    @endsection


    @push('scripts')

    <script src="{{ asset('dxblounge/js/lib/select2/select2.full.min.js') }}"></script>
    <!-- Form validation -->
    <script src="{{ asset('dxblounge/js/lib/form-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('dxblounge/js/lib/form-validation/jquery.validate-init.js') }}"></script>

    <script>
      $(document).ready(function(){

        var user_id = "{{ $user_id }}";
        console.log(user_id);
        if( user_id != "") {
            setTimeout(() => {
               // $('.user_' + user_id).trigger('click');
            }, 10);
            
        }

        $('#role_id').on('change', function() {
            if( this.options[this.selectedIndex].innerHTML == 'pickup' ) {
                $('#pickup-person').show();
                $('#pickup').removeAttr('disabled');
            } else {
                $('#pickup-person').hide();
                $('#pickup').attr('disabled', true);
            }
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var form = $( "#user-add" );
        form.validate();
        $( "#user-add" ).on("submit", function() {
            if(form.valid() == true) {
                var data = form.serialize();
                $('#overlay').show();
                $.ajax({
                    url: form.attr('action'),
                    method: "post",
                    data: data,
                    success: function(data) {
                        data = JSON.parse(data);
                        // $('.list-block').show();
                        // $('.edit-add-block').hide();
                        if(data.status == true) {
                            swal({
                                title: "Success",
                                text: "Saved successfully",
                                timer: 2000,
                                showConfirmButton: false
                            });
                            setTimeout(() => {
                                location.reload();
                            }, 2000);
                        } else {
                            swal({
                                title: "Error",
                                text: data.message,
                                timer: 2000,
                                showConfirmButton: false
                            });
                        }
                    },
                    complete: function() {
                        $('#overlay').hide();
                    }
                });
            }
        });

        $('.add-new-item').on('click', function() {
            $( "#user-add" ).attr('action', "<?php echo route('user_add')?>");
            $('#user').val('');
            $('#user_id').val('');
            $('.password').show();
            $('#password').removeAttr('disabled');
            $('input:checkbox').prop('checked', false);
            $( "#user-add" ).trigger("reset");
            $('#pickup-person').hide();
            $('#pickup').attr('disabled', true);
        });

        $(document).on("click", '.edit-item', function() {
            var id = $(this).data('item');
            $( "#user-add" ).attr('action', "<?php echo route('user_edit')?>");
            $('#user_id').val(id);
            $('#overlay').show();
            $.ajax({
                url: "<?php echo route('user_data')?>",
                method: "post",
                data: {
                    id: id
                },
                success: function(data) {
                    data = JSON.parse(data);
                    if(data.status == true) {
                        $('#name').val(data.user.name);
                        $('#email').val(data.user.email);
                        $('.password').hide();
                        $('#password').attr('disabled', true);
                        $('#role_id').val(data.user.role_id);
                        if( $('#role_id')[0].options[$('#role_id')[0].selectedIndex].innerHTML == 'pickup' ) {
                            $('#pickup-person').show();
                            $('#pickup').removeAttr('disabled');
                        }
                        $('#pickup').val(data.user.pickup_point);
                        $('.list-block').hide();
                        $('.edit-add-block').show();
                    }
                },
                complete: function() {
                    $('#overlay').hide();
                }
            });
        });


      });
            
    </script>
@endpush