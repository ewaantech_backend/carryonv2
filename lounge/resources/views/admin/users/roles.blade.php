@extends('admin.layouts.app')

@section('content')

    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                
                <!-- /# row -->
                <section id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card list-block">
                                <div class="card-title">
                                    <h4>Roles</h4>
                                </div>
                                <div class="invoice-edit">
                                    
                                    <!--.row-->
                                    <div class="invoicelist-body">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead contenteditable>
                                                    <tr>
                                                        <th>Role #</th>
                                                        <th>Name</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                @if (count($roles) > 0)
                                                    
                                                    @foreach ($roles as $role)
                                                        <tr>
                                                            <td><a class="edit-item" data-name="{{ $role->name }}" data-item={{$role->id}}>{{$role->id}}</a></td>
                                                            <td>{{$role->name}}</td>
                                                            <td></td>
                                                        </tr>    
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="4" class="text-center"> No Roles Available</td>
                                                    </tr>
                                                @endif

                                                </tbody>
                                            </table>
                                        </div>
                                        <a class="control newRow add-new-item" href="#">+ New ROLE</a>
                                    </div>
                                </div>
                            </div>

                            <div class="edit-add-block">
                                <div class="card">
                                    <div class="card-title">
                                        <h4>Role Add/Edit</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="form-validation">
                                            <form id="role-add" class="form-valide add-form" method="post">
                                                <input type="hidden" id="role_id" name="role_id" value="0">
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Role <span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control" id="role" name="role" placeholder="Enter a role name..">
                                                    </div>
                                                </div>

                                                @foreach ($user_permissions as $role)
                                                
                                                <div class="card">
                                                    <div class="card-title">
                                                        <h4>{{ ucfirst($role['key']) }}</h4>
                                                    </div>
                                                    <div class="todo-list">
                                                        <div class="tdl-holder">
                                                            <div class="tdl-content">
                                                                <ul>
                                                                    <div class="row">
                                                                    @foreach ($role['items'] as $item)
                                                                    <div class="col-md-4">
                                                                        <!-- <li> -->
                                                                            <label>
                                                                            <div class="col-md-6">
                                                                                <input type="checkbox" name="{{ $item['permission_key'] }}">
                                                                                <i></i>
                                                                            </div>
                                                                                <div class="col-md-6">
                                                                                <span>{{ ucfirst($item['display']) }}</span>
                                                                                </div>
                                                                            </label>
                                                                        <!-- </li> -->
                                                                    </div>
                                                                    @endforeach
                                                                    </div>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                @endforeach

                                                <div class="form-group row">
                                                    <div class="col-lg-8 ml-auto">
                                                        <button type="button" class="btn btn-default cancel-form">Cancel</button>   
                                                        <button type="submit" class="btn btn-primary">Submit</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /# card -->
                        </div>
                        <!-- /# column -->
                    </div>
                    <!-- /# row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>2020 © carryOn DXB Admin.</p>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    @endsection


    @push('scripts')

    <script src="{{ asset('dxblounge/js/lib/select2/select2.full.min.js') }}"></script>
    <!-- Form validation -->
    <script src="{{ asset('dxblounge/js/lib/form-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('dxblounge/js/lib/form-validation/jquery.validate-init.js') }}"></script>

    <script>
      $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var form = $( "#role-add" );
        form.validate();
        $( "#role-add" ).on("submit", function() {
            console.log(form.valid());
            if(form.valid() == true) {
                var data = form.serialize();
                $('#overlay').show();
                $.ajax({
                    url: form.attr('action'),
                    method: "post",
                    data: data,
                    success: function(data) {
                        data = JSON.parse(data);
                        //$('.list-block').show();
                        //$('.edit-add-block').hide();
                        if(data.status == true) {
                            swal({
                                title: "Success",
                                text: "Saved successfully",
                                timer: 2000,
                                showConfirmButton: false
                            });
                            setTimeout(() => {
                                location.reload();
                            }, 2000);
                        } else {
                            swal({
                                title: "Error",
                                text: data.message,
                                timer: 2000,
                                showConfirmButton: false
                            });
                        }
                    },
                    complete: function() {
                        $('#overlay').hide();
                    }
                });
            }
        });

        $('.add-new-item').on('click', function() {
            $( "#role-add" ).attr('action', "<?php echo route('role_add')?>");
            $('#role').val('');
            $('#role_id').val('0');
            $('input:checkbox').prop('checked', false);
            $( "#role-add" ).trigger("reset");
        });

        $(document).on("click", '.edit-item', function() {
            var id = $(this).data('item');
            var name = $(this).data('name');
            $( "#role-add" ).attr('action', "<?php echo route('role_edit')?>");
            $('#role_id').val(id);
            $('#role').val(name);
            $('#overlay').show();
            $.ajax({
                url: "<?php echo route('role_data')?>",
                method: "post",
                data: {
                    id: id
                },
                success: function(data) {
                    data = JSON.parse(data);
                    if(data.status == true) {
                        $('input:checkbox').removeAttr('checked');
                        for(var inc = 0; inc < data.permissions.length; inc++) {
                            $('input:checkbox[name=' + data.permissions[inc].name +']').prop('checked', true);
                        }
                        $('.list-block').hide();
                        $('.edit-add-block').show();
                        
                    }
                },
                complete: function() {
                    $('#overlay').hide();
                }
            });
        });


      });
            
    </script>
@endpush