<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>CARRYON ADMIN</title>

    <!-- Styles -->
    <link href="{{ asset('dxblounge/css/lib/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dxblounge/css/lib/themify-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('dxblounge/css/lib/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('dxblounge/css/lib/helper.css') }}" rel="stylesheet">
    <link href="{{ asset('dxblounge/css/style.css') }}" rel="stylesheet">
</head>

<body class="bg-primary">

    <div class="unix-login">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="login-content">
                        <div class="login-logo">
                            <a href="index.html"><span>Carryon</span></a>
                        </div>
                        <div class="login-form">
                            <h4>Admin Login</h4>
                            <form>
                                <div class="form-group">
                                    <label>Email address</label>
                                    <input type="email" class="form-control" placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input type="password" class="form-control" placeholder="Password">
                                </div>
                                <div class="checkbox">
                                    <label class="pull-right">
										<a href="#">Forgotten Password?</a>
									</label>

                                </div>
                                <button type="submit" class="btn btn-primary btn-flat m-b-30 m-t-30">Sign in</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>

</html>