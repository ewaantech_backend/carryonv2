
@extends('admin.layouts.app')
@section('content')
<div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Update  Ingredients</h1>
                            </div>
                        </div>
                    </div>
                    @if (session('status'))
                      <div class="alert alert-success" id="success_message" role="alert">
                        {{session('status')}}
                      </div>
                    @endif
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb">
                                    <!-- <li class="breadcrumb-item"><a href="#">Create Allergies</a></li> -->
                                </ol>
                            </div>
                        </div>
                    </div>
                    </div>
                                    @php
                                    $count = 1;
                                    @endphp
                                    @foreach ($ingredients as $ingredient)
                                    @php
                                    $ing_id= $ingredient->id; 
                                    $ing_name= $ingredient->name;
                                    $ing_description= $ingredient->description;
                                    $ing_visibility= $ingredient->visibility;
                                    $ing_image= $ingredient->image_path;
                                    $ing_filter= $ingredient->is_filter;
                                    @endphp
                <section id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="form-validation">
                                        <form class="form-valide direct_submit" action="{{ route('ing_update') }}" method="POST" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
                                        <input type="hidden" value="{{ $ing_id }}" name="ing_id">    
                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label" for="val-currency">Name  <span class="text-danger">*</span></label>
                                                <div class="col-lg-6">
                                                    <input type="text" class="form-control" id="name" name="name" required value="{{$ing_name}}" >
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label" for="val-website">Description <span class="text-danger">*</span></label>
                                                <div class="col-lg-6">
                                                <div class="form-group">
 
                                        <textarea  class="form-control" rows="50" cols="50" id="description" name="description" required >{{ $ing_description }}</textarea>
                                        </div>
                                          </div>
                                            </div>

                                           

                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label" for="val-skill">Visibility <span class="text-danger">*</span></label>
                                                <div class="col-lg-6">
                                                    <select class="form-control" id="visibility" name="visibility" required>
                                                        <option @if($ing_visibility=='YES') selected @endif value="YES">YES</option>
                                                        <option @if($ing_visibility=='NO') selected @endif value="NO">NO</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label" for="val-skill">Fliter <span class="text-danger">*</span></label>
                                                <div class="col-lg-6">
                                                    <select class="form-control" id="fliter" name="fliter" required>
                                                        <!-- <option value="">Please select</option>
                                                        <option value="YES">YES</option>
                                                        <option value="NO">NO</option> -->
                                                        <option @if($ing_filter=='YES') selected @endif value="YES">YES</option>
                                                        <option @if($ing_filter=='NO') selected @endif value="NO">NO</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label" for="val-website">Upload Image <span class="text-danger">*</span></label>
                                                <div class="col-lg-6">
                                                <img src="{{asset("$ing_image")}}" style="width:75px;height:75" class="img-thumbnail"><br><br>
                                                <input type="file" class="form-control-file" id="image" name="image" >
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-lg-8 ml-auto">
                                                    <button type="submit" class="btn btn-primary btn-sm m-b-10 m-l-5">Submit</button>
                                                    <button type="button" id="cancel_button" class="btn btn-default btn-sm m-b-10 m-l-5">Cancel</button>
                                                </div>
                                            </div>
                                                               
                                           
                                            
                                            
                                           
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                </section>
                @endforeach
            </div>
        </div>
    </div>
@endsection


@push('scripts')

<script src="{{ asset('dxblounge/js/lib/select2/select2.full.min.js') }}"></script>
<!-- Form validation -->
<!-- <script src="{{ asset('dxblounge/js/lib/form-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('dxblounge/js/lib/form-validation/jquery.validate-init.js') }}"></script> -->
<script>
        $(document).ready(function(){
          $("#cancel_button").click(function(){
            location.href = "{{url('admin/ingredients')}}";
          });
        });
    </script>

@endpush