@extends('admin.layouts.app')

@section('content')

    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                
                <!-- /# row -->
                <section id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card list-block">
                                <div class="card-title">
                                    <h4>Addons</h4>
                                </div>
                                <div class="invoice-edit">
                                    
                                    <!--.row-->
                                    <div class="invoicelist-body">
                                        <div class="bootstrap-data-table-panel">
                                            <div class="table-responsive">
                                                <table  id="bootstrap-data-table" class="table table-striped table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Addon #</th>
                                                            <th>Name</th>
<!--                                                            <th>Sku</th>-->
                                                            <th>Price</th>
<!--                                                            <th>Discount price</th>-->
                                                            <th>Visibility</th>
                                                            <th>Position</th>
                                                            <!-- <th>Description</th> -->
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                    @if (count($addons) > 0)
                                                        
                                                        @foreach ($addons as $addon)
                                                            <tr>
                                                                <td><a class="edit-item" data-item={{$addon->id}}>{{$addon->id}}</a></td>
                                                                <td>{{$addon->name}}</td>
<!--                                                                <td>{{$addon->sku}}</td>-->
                                                                <td>{{ decimal_format_custom($addon->actual_price) }}</td>
<!--                                                                <td>{{ decimal_format_custom($addon->discount_price) }}</td>-->
                                                                <td>{{$addon->visibility}}</td>
                                                                <td>{{$addon->position}}</td>
                                                                <!-- <td>{{$addon->description}}</td> -->
                                                                <td><a class="edit-item" data-item={{$addon->id}}><span style='color:blue;' class="ti-pencil-alt"></span></a><a class="delete-item" data-item={{$addon->id}}><span style='color:red;' class="ti-trash"></span></a></td>
                                                            </tr>
                                                        @endforeach
                                                    @endif

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        @if ( isset($permissions['addons_add_edit']) && $permissions['addons_add_edit'] == true )
                                            <a class="control newRow add-new-item" href="#">+ New Addon</a>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="edit-add-block">
                                <div class="card">
                                    @if ( ! isset($permissions['addons_add_edit']))
                                    <div class="alert alert-danger">
                                        You dont have permission to edit.
                                    </div>
                                    @endif
                                    <div class="card-title">
                                        <h4>Addon Add/Edit</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="form-validation">
                                            <form id="addon-add" class="form-valide add-form" method="post" enctype="multipart/form-data">
                                                <input type="hidden" id="addon_id" name="addon_id">
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Name <span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control" id="addon_name" name="addon_name" placeholder="Enter a name..">
                                                    </div>
                                                </div>
<!--                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Sku <span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control" id="addon_sku" name="addon_sku" placeholder="Enter sku..">
                                                    </div>
                                                </div>-->
                                                <div class="form-group row password">
                                                    <label class="col-lg-4 col-form-label">Price <span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                        <input type="number" class="form-control" id="addon_actual_price" name="addon_actual_price" placeholder="Enter actual price">
                                                    </div>
                                                </div>
<!--                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Discount price <span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                    <input type="number" class="form-control" id="addon_discount_price" name="addon_discount_price" placeholder="Enter discount price">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Description <span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                    <textarea class="form-control" id="addon_description" name="addon_description" placeholder="Enter addon description" ></textarea>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Friendly Url <span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                    <input type="text" class="form-control" id="addon_friendly_url" name="addon_friendly_url" placeholder="Enter friendly url">
                                                    </div>
                                                </div>-->


                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Visibility<span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                        <select class="form-control" id="addon_visibility" name="addon_visibility">
                                                            <option value="YES">YES</option>
                                                            <option value="NO">NO</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Position <span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control" id="addon_position" name="addon_position" >
                                                    </div>
                                                </div>                                                

                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Thumbnail Image<span class="text-danger">*</span></label>
                                                    <div class="col-lg-8" id="thumb-img">
                                                        <button style="cursor:pointer;background-color: transparent;border-color:#aaa;" class="btn btn-default" type="button" 
                                                            onclick="onButtonClicked('thumbnail_image', 'content')">
                                                            <div class="stat-icon">
                                                                <i style="color:blue;font-size:25px;" class="ti-cloud-up">
                                                                    <span style="color:#495057;font-size:17px;top:-6px;position: relative;font-family: 'Roboto', sans-serif;"> Upload thumbnail image</span>
                                                                </i>
                                                            </div>
                                                        </button>
                                                        <div id="content"></div>
                                                        <div id="content-hidden" style="display:none;"></div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">addon Images<span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                        <button style="cursor:pointer;background-color: transparent;border-color:#aaa;" class="btn btn-default" type="button" 
                                                            onclick="onButtonClicked('addon_image[]', 'image-content')">
                                                            <div class="stat-icon">
                                                                <i style="color:blue;font-size:25px;" class="ti-cloud-up">
                                                                    <span style="color:#495057;font-size:17px;top:-6px;position: relative;font-family: 'Roboto', sans-serif;"> Upload addon images</span>
                                                                </i>
                                                            </div>
                                                        </button>
                                                        <div id="image-content"></div>
                                                        <div id="image-content-hidden" style="display:none;"></div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-lg-8 ml-auto">
                                                        <button type="button" class="btn btn-default cancel-form">Cancel</button>
                                                        @if ( isset($permissions['addons_add_edit']) && $permissions['addons_add_edit'] == true )  
                                                            <button type="submit" class="btn btn-primary">Submit</button>
                                                        @endif
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /# card -->
                        </div>
                        <!-- /# column -->
                    </div>
                    <!-- /# row -->
                <div class="modal" id="confirm_status" tabindex="-1" role="dialog">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-body">
                            <h4>Do you want to delete the addon? </h4>
                            <input type="hidden" value="" id="delete_id">
                            <div id="status_modal_contents" class="row">
                                <!-- contents -->
                            </div>
                          </div>
                          <div class="modal-footer">
                            <span id="confirm-text" style="color:red;display:none;">Do you want to delete the addon? </span>
                            <button type="button" class="btn btn-primary delete-item-confirm">Proceed</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>2020 © carryOn DXB Admin.</p>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    @endsection


    @push('scripts')

    <script src="{{ asset('dxblounge/js/lib/select2/select2.full.min.js') }}"></script>
    <!-- Form validation -->
    <script src="{{ asset('dxblounge/js/lib/form-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('dxblounge/js/lib/form-validation/jquery.validate-init.js') }}"></script>

    <script>
      $(document).ready(function(){
//        CKEDITOR.replace('addon_description', {
//            on: {
//                  change: function() {
//                      this.updateElement();    
//                  }
//            }
//        })
        $('.select-2').select2({
            placeholder: 'Select options'
        });
        
    $(document).on('click', '.change-stat-modal', function() {
        var order_id = $(this).attr('order_id');
        $('#order_id').val(order_id);
        var currentItem = $(this).attr('value');
        $('button.change-pickup').removeClass('btn-success');
        if( $.inArray(currentItem, ['ORDER_CONFIRMED', 'IN_KITCHEN', 'READY_FOR_PICKUP', 'DELIVERED']) != -1 ) {
            $('button.change-pickup[value="'+ currentItem +'"]').addClass('btn-success');
        } else {
            $('button.change-pickup[itemval="'+ currentItem +'"]').addClass('btn-success');
        }
        $('#confirm-text').hide();
        $('#confirm_status').modal('show');
    });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var form = $( "#addon-add" );
        form.validate();
        $( "#addon-add" ).on("submit", function() {
            if(form.valid() == true) {
                // var data = form.serialize();
//                $('#addon_description').val(CKEDITOR.instances.addon_description.getData());
                let data = new FormData( form[0] );
                //console.log(data);
                $('#overlay').show();
                $.ajax({
                    url: form.attr('action'),
                    method: "post",
                    data: data,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        data = JSON.parse(data);
                        if(data.status == true) {
                            swal({
                                title: "Success",
                                text: "Saved successfully",
                                timer: 2000,
                                showConfirmButton: false
                            });
                            setTimeout(() => {
                               location.reload();
                            }, 2000);
                        } else {
                            swal({
                                title: "Error",
                                text: data.message,
                                timer: 2000,
                                showConfirmButton: false
                            });
                        }
                    },
                    complete: function() {
                        $('#overlay').hide();
                    }
                });
            }
        });
        $('.add-new-item').on('click', function() {
            $( "#addon-add" ).attr('action', "<?php echo route('addon_add')?>");
            $('#addon_id').val('');
            $('#addon_allergies').val([]).change();
            $('#addon_allergies_filter').val([]).change();
            $('#addon_categories').val([]).change();
            $('#addon_incredients').val([]).change();
            $( "#addon-add" ).trigger("reset");
        });

        $(document).on("click", '.edit-item', function() {
            var id = $(this).data('item');
            $( "#addon-add" ).attr('action', "<?php echo route('addon_edit')?>");
            $('#addon_id').val(id);
            $('#overlay').show();
            $.ajax({
                url: "<?php echo route('addon_data')?>",
                method: "post",
                data: {
                    id: id
                },
                success: function(data) {
                    data = JSON.parse(data);
                    if(data.status == true) {
                        $('#addon_name').val(data.addon.name);
                        //$('#addon_sku').val(data.addon.sku);
                        $('#addon_actual_price').val(data.addon.actual_price);
                        //$('#addon_discount_price').val(data.addon.discount_price);
                        //CKEDITOR.instances.addon_description.setData(data.addon.description);
                        //$('#addon_description').val(data.addon.description);
                        //$('#addon_friendly_url').val(data.addon.url_key);
                        $('#addon_visibility').val(data.addon.visibility);
                        $('#addon_position').val(data.addon.position);
                        
                        var appUrl = "{{ env('APP_URL') }}/public";
                        if( data.thumbnail != null) {
                            $('#content').html(`<img src="${appUrl}${data.thumbnail.path}" 
                            style="width: 100px; height: 100px; margin-top: 25px;">`);
                        }
                        if( data.images.length != 0 ) {
                            $('#image-content').empty();
                            for(var incr_img =0; incr_img < data.images.length; incr_img++ ){
                                $('#image-content').append(`<img id="prdt_img_${data.images[incr_img].id}" src="${appUrl}${data.images[incr_img].path}" 
                                style="width: 200px; height: 200px; margin-top: 25px;margin-right: 10px;">
                                <button class="dlt-img" img_id="${data.images[incr_img].id}" style="position: relative; top: -71px; left: -37px;" type="button">x</button>`);
                            }
                        }
                        $('.list-block').hide();
                        $('.edit-add-block').show();
                    }
                },
                complete: function() {
                    $('#overlay').hide();
                }
            });
        });

        $(document).on('click', '.dlt-img', function() {
            var img_id = $(this).attr('img_id');
            var that = this;
            $('#overlay').show();
            $.ajax({
                url: "<?php echo route('addon_image_delete')?>",
                method: "post",
                data: {
                    img_id: img_id
                },
                success: function(data) {
                    $('#prdt_img_'+img_id).remove();
                    $(that).remove();
                },
                complete: function() {
                    $('#overlay').hide();
                }
            });


        });

      });
      $('.delete-item').on("click", function() {
            var id = $(this).data('item');
            $("#delete_id").val(id);
            $('#confirm_status').modal().show();
     });
        
  $('.delete-item-confirm').on("click", function() {
      $('#confirm_status').modal().hide();
        $('#overlay').show();
        var id = $("#delete_id").val();
        $.ajax({
                url: "<?php echo route('addon_destroy')?>",
                method: "post",
                data: {
                    id: id
                },
                success: function(data) {
                    data = JSON.parse(data);

                        if(data.status == true) {
                            swal({
                                title: "Success",
                                text: "Deleted successfully",
                                timer: 2000,
                                showConfirmButton: false
                            });
                            setTimeout(() => {
                                location.reload();
                            }, 1000);
                        } else {
                            swal({
                                title: "Error",
                                text: data.message,
                                timer: 2000,
                                showConfirmButton: false
                            });
                        }
                },
                complete: function() {
                    $('#overlay').hide();
                }
            });
      
  });      
        
        // Button callback
        async function onButtonClicked(name, container){
            let files = await selectFile("image/*", false, name, container);
            if(name == 'thumbnail_image') {
                $('#' + container).html(`<img src="${URL.createObjectURL(files)}" style="width: 100px; height: 100px; margin-top: 25px;">`);
            } else {
                $('#' + container).append(`<img src="${URL.createObjectURL(files)}" style="width: 200px; height: 200px; margin-top: 25px;margin-right: 10px;">`);
            }
        }

        // ---- function definition ----
        function selectFile (contentType, multiple, name, container){
            return new Promise(resolve => {
                let input = document.createElement('input');
                input.type = 'file';
                input.multiple = multiple;
                input.accept = contentType;
                input.name=name;

                input.onchange = _ => {
                    let files = Array.from(input.files);
                    if (multiple)
                        resolve(files);
                    else
                        resolve(files[0]);
                };
                if(name == 'thumbnail_image') {
                    $('#'+container+'-hidden').html(input);
                } else {
                    $('#'+container+'-hidden').append(input);
                }
                input.click();
            });
        }
            
    </script>
@endpush