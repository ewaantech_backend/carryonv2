<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        <title>Carryon</title>

        <!-- Styles -->
        <link href="{{ asset('dxblounge/css/lib/weather-icons.css') }}" rel="stylesheet" />
        <link href="{{ asset('dxblounge/css/lib/owl.carousel.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('dxblounge/css/lib/owl.theme.default.min.css') }}" rel="stylesheet" />
        <link href="{{ asset('dxblounge/css/lib/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('dxblounge/css/lib/themify-icons.css') }}" rel="stylesheet">
        <link href="{{ asset('dxblounge/css/lib/menubar/sidebar.css') }}" rel="stylesheet">
        <link href="{{ asset('dxblounge/css/lib/bootstrap.min.css') }}" rel="stylesheet">

        <link href="{{ asset('dxblounge/css/lib/select2/select2.min.css') }}" rel="stylesheet">
        <link href="{{ asset('dxblounge/css/lib/sweetalert/sweetalert.css') }}" rel="stylesheet">

        <link href="{{ asset('dxblounge/css/lib/helper.css') }}" rel="stylesheet">
        <link href="{{ asset('dxblounge/css/style.css') }}" rel="stylesheet">
        <script src="//cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
        <link href="{{ asset('dxblounge/css/admin/jquery-ui/jquery-ui.css') }}" rel = "stylesheet">

        <!-- Kitchen -->
        <link rel="stylesheet" href="{{ asset('dxblounge/kitchen/assets/stylesheets/main.css') }}" />
        
      
        <!-- Morris JS -->
        <link rel="stylesheet" href="{{ asset('dxblounge/js/lib/Morris/morris.css') }}">

        <!-- Spinner JS -->
        <link rel="stylesheet" href="{{ asset('dxblounge/js/lib/spinner/spin.css') }}">

        <!-- DateRange Picker -->
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

        <!-- Reports blade custom css -->
        <link rel="stylesheet" type="text/css" href="{{ asset('dxblounge/js/reports/reports.css') }}" />
    </head>
    
    <body class="orders-status">

        <div>
            <header id="header">
                <div class="container-fluid">
                    <div class="row align-items-center">
                    <div class="col-xl-3 col-sm-6 ">
                        <div class="logo">
                        <a href="/admin">
                            <img src="{{ asset('dxblounge/images/logo.png') }}" alt="logo">
                        </a>
                        </div>
                    </div>
                    <div class="order-visible-item col-xl-3 col-sm-6 text-center text-md-left">
                        <p class="date-time"></p>
                        <a href="#" class="alarm" id="alarm_check">
                        <i><img src="{{ asset('dxblounge/kitchen/assets/images/bell.png') }}" alt="icon"></i>
                        Ring me
                        </a>
                    </div>
                    <div class="col-xl-6">
                        <div class="row align-items-center">
                        <div class="order-visible-item col-xl-9 col-12">
                            <nav id="dashboard-navigation">
                            <ul class="tab-order-type">
                                <li class="active">
                                <input type="hidden" value="UPCOMING">
                                <a href="#">
                                    <i><img class="injectable" src="{{ asset('dxblounge/kitchen/assets/images/nav-icon1.svg') }}" alt="icon"></i>
                                    <span>upcoming orders</span>
                                </a>
                                </li>
                                <li>
                                <input type="hidden" value="READY_FOR_PICKUP">
                                <a href="#">
                                    <i><img class="injectable" src="{{ asset('dxblounge/kitchen/assets/images/nav-icon2.svg') }}" alt="icon"></i>
                                    <span>ready for pickup</span>
                                </a>
                                </li>
                                <li>
                                <input type="hidden" value="">
                                <a href="#">
                                    <i><img class="injectable" src="{{ asset('dxblounge/kitchen/assets/images/nav-icon3.svg') }}" alt="icon"></i>
                                    <span>all orders</span>
                                </a>
                                </li>
                            </ul>
                            </nav>
                        </div>
                        <div class="col-xl-3 col-12 actions">
                            <div class="action">
                                <p>Welcome 
                                    @if(Session::has('user_name'))
                                        {{ Session::get('user_name') }}!
                                    @endif    
                                    <a href="<?php echo route('user_logout')?>">Logout</a>
                                </p>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
            </header>
        </div>

        <div @if(isset($kitchen_login) && $kitchen_login ) id="sidebar" @endif class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
            <div class="nano">
                <div class="nano-content">
                    <ul>
                    @if( (isset($user_has_roles) && in_array('kitchen', $user_has_roles)) ||  (isset($user_has_roles) && in_array('pickup', $user_has_roles)))
                        @if( (isset($user_has_roles) && in_array('kitchen', $user_has_roles)) )
                        <li><a href="{{ asset('dxblounge//admin/kitchen') }}" class=""><i class="ti-files"></i>Orders</a></li>
                        @else
                        <li><a href="{{ asset('dxblounge//admin/orders') }}" class=""><i class="ti-files"></i>Orders</a></li>
                        <li><a href="<?php echo route('delivered_orders') ?>" class=""><i class="ti-stamp"></i>Delivered Orders</a></li>
                        <li><a href="<?php echo route('cancelled_orders') ?>" class=""><i class="ti-close"></i>Cancelled Orders</a></li>
                        @endif
                    @else
                        <li><a href="{{ asset('dxblounge//admin/users') }}" class=""><i class="ti-bar-chart-alt"></i>User management</a></li>
                        <li><a href="{{ asset('dxblounge//admin/orders') }}" class=""><i class="ti-files"></i>Orders</a></li>
                        <li><a href="<?php echo route('delivered_orders') ?>" class=""><i class="ti-stamp"></i>Delivered Orders</a></li>
                        <li><a href="<?php echo route('cancelled_orders') ?>" class=""><i class="ti-close"></i>Cancelled Orders</a></li>
                        <li><a href="<?php echo route('products')?>" class=""><i class="ti-package"></i>Products</a></li>
                        <li><a href="<?php echo route('addons')?>" class=""><i class="ti-package"></i>Addons</a></li>
                        <li><a href="{{ asset('dxblounge//admin/doneness') }}" class=""><i class="ti-package"></i>Doneness</a></li>
                              
                         <li><a class="sidebar-sub-toggle"><i class="ti-pencil-alt"></i>CMS<span class="sidebar-collapse-icon ti-angle-down"></span></a>
                            <ul>
                                <li><a href="<?php echo route('cms')?>" class=""><i class="ti-pencil-alt"></i>Pages</a></li>
                                <li><a href="<?php echo route('cms_section')?>" class=""><i class="ti-pencil-alt"></i>Sections</a></li>
                                <li><a href="{{ asset('dxblounge//admin/cms_slider') }}"><i class="ti-pencil-alt"></i>Home Slider</a></li>
                                <li><a href="{{ asset('dxblounge//admin/cms_slider/banners') }}"><i class="ti-pencil-alt"></i>Banners</a></li>
                                <li><a href="<?php echo route('socialmedia')?>"><i class="ti-pencil-alt"></i>Social Media</a></li>
                            </ul>
                        </li>                         
                         
                        <!-- Reports -->
                        <!--<li><a href="{{route('render_reports')}}" class=""><i class="ti-clipboard"></i>Reports</a>
                        </li> -->
                        
                         <li><a class="sidebar-sub-toggle"><i class="ti-settings"></i>Settings<span class="sidebar-collapse-icon ti-angle-down"></span></a>
                            <ul>
                                <li><a href="{{ asset('dxblounge//admin/cms_slider') }}">Cms Slider</a></li>
                                <li><a href="{{ asset('dxblounge//admin/category') }}">Product Categories</a></li>
                                <li><a href="{{ asset('dxblounge//admin/ingredients') }}">Product Ingredients</a></li>
                                <li><a href="{{ asset('dxblounge//admin/allergies') }}">Product Tags</a></li>
                                <li><a href="{{ asset('dxblounge//admin/allergies_fliter') }}">Product Menu Filter</a></li>
                                <li><a href="<?php echo route('preferences')?>">Preferences</a></li>
                                <li><a href="<?php echo route('settings_data')?>">General</a></li>
                            </ul>
                        </li>
                         <li><a class="sidebar-sub-toggle"><i class="ti-settings"></i>Email Settings<span class="sidebar-collapse-icon ti-angle-down"></span></a>
                            <ul>
                                <li><a href="{{ asset('dxblounge//admin/email/readyforpickup') }}">Ready For Pickup</a></li>
                                <li><a href="{{ asset('dxblounge//admin/email/welcomemail') }}">Welcome Mail</a></li>
                                <li><a href="{{ asset('dxblounge//admin/email/cancelmail') }}">Cancel Mail</a></li>
                                <li><a href="{{ asset('dxblounge//admin/email/orderplace') }}">Order Place</a></li>
                                <li><a href="{{ asset('dxblounge//admin/email/review') }}">Review after 24 hours</a></li>
                                <li><a href="{{ asset('dxblounge//admin/email/reminder') }}">Reminder</a></li>
                            </ul>
                        </li>
                         <li><a class="sidebar-sub-toggle"><i class="ti-settings"></i>SMS Settings<span class="sidebar-collapse-icon ti-angle-down"></span></a>
                            <ul>
                                <li><a href="{{ asset('dxblounge//admin/sms/READY_FOR_PICKUP') }}">Ready For Pickup</a></li>
                                <li><a href="{{ asset('dxblounge//admin/sms/ORDER_PLACE') }}">Order Place</a></li>
                                <li><a href="{{ asset('dxblounge//admin/sms/REMINDER') }}">Reminder</a></li>
                            </ul>
                        </li>
                        @endif
                        <li><a href="<?php echo route('user_logout')?>"><i class="ti-close"></i> Logout</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /# sidebar -->


        <!-- <div class="header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="float-left">
                            <div class="hamburger sidebar-toggle">
                                <span class="line"></span>
                                <span class="line"></span>
                                <span class="line"></span>
                            </div>
                        </div>
                        <div class="float-right">
                            <ul>

                                <li class="header-icon dib">
                                    <i >&nbsp;</i>
                                    <div class="drop-down">
                                        <div class="dropdown-content-heading">
                                            <span class="text-left">Recent Notifications</span>
                                        </div>
                                        <div class="dropdown-content-body">
                                            <ul>
                                                <li>
                                                    <a href="#">
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Mr. John</div>
                                                    <div class="notification-text">5 members joined today </div>
                                                </div>
                                            </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Mariam</div>
                                                    <div class="notification-text">likes a photo of you</div>
                                                </div>
                                            </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Tasnim</div>
                                                    <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                                </div>
                                            </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                <div class="notification-content">
                                                    <small class="notification-timestamp pull-right">02:34 PM</small>
                                                    <div class="notification-heading">Mr. John</div>
                                                    <div class="notification-text">Hi Teddy, Just wanted to let you ...</div>
                                                </div>
                                            </a>
                                                </li>
                                                <li class="text-center">
                                                    <a href="#" class="more-link">See All</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                               
                               
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <div id="overlay">
            <div class="lds-dual-ring"></div>
        </div>
        

        @yield('content')

        <!-- jquery vendor -->
        <script src="{{ asset('dxblounge/js/lib/jquery.min.js') }}"></script>
        <script src="{{ asset('dxblounge/js/lib/jquery.nanoscroller.min.js')}} "></script>
        <!-- nano scroller -->
        <script src="{{ asset('dxblounge/js/lib/menubar/sidebar.js')}}"></script>
        <script src="{{ asset('dxblounge/js/lib/preloader/pace.min.js')}}"></script>
        <!-- sidebar -->
        <script src="{{ asset('dxblounge/js/lib/bootstrap.min.js')}}"></script>

        <script src="{{ asset('dxblounge/js/lib/sweetalert/sweetalert.min.js') }}"></script>
        <!-- <script src="{{ asset('dxblounge/js/lib/sweetalert/sweetalert.init.js') }}"></script> -->

        <script src="{{ asset('dxblounge/js/scripts.js')}}"></script>

        <script src="{{ asset('dxblounge/js/lib/data-table/datatables.min.js') }}"></script>
        <!-- <script src="{{ asset('dxblounge/js/lib/data-table/buttons.dataTables.min.js') }}"></script> -->
        <script src="{{ asset('dxblounge/js/lib/data-table/dataTables.buttons.min.js') }}"></script>
        <script src="{{ asset('dxblounge/js/lib/data-table/buttons.flash.min.js') }}"></script>
        <script src="{{ asset('dxblounge/js/lib/data-table/jszip.min.js') }}"></script>
        <script src="{{ asset('dxblounge/js/lib/data-table/pdfmake.min.js') }}"></script>
        <script src="{{ asset('dxblounge/js/lib/data-table/vfs_fonts.js') }}"></script>
        <script src="{{ asset('dxblounge/js/lib/data-table/buttons.html5.min.js') }}"></script>
        <script src="{{ asset('dxblounge/js/lib/data-table/buttons.print.min.js') }}"></script>
        <script src="{{ asset('dxblounge/js/lib/data-table/datatables-init.js') }} "></script>

        <script src = "{{ asset('dxblounge/js/lib/jquery-ui/jquery-ui.min.js') }}"></script>
        <script>
            $(document).ready(function() {
                $('#alarm_check').on('click', function() {
                  var context = new AudioContext()
                  context.resume()
                  var o = context.createOscillator()
                  var  g = context.createGain()
                  var frequency = 830.6
                  o.frequency.value = frequency
                  o.connect(g)
                  g.connect(context.destination)
                  g.gain.exponentialRampToValueAtTime(0.00001, context.currentTime + 5)
                  o.start();
                  setTimeout(function(){
                      o.stop();
                  }, 2000); 
                });                
            });
        </script>
        @stack('scripts')
        
        
    </body>

</html>