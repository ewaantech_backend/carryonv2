@extends('admin.layouts.app')

@section('content')
    <script>
      $(document).ready(function(){
         setTimeout(function(){
          $("#success_message").remove();
             }, 2000);
      });
    </script>

    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                @if (session('status'))
                   <div class="alert alert-success" id="success_message" role="alert">
                     {{ session('status') }}
                   </div>
                 @endif                
                <!-- /# row -->
                <section id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div>
                                <div class="card">
                                    <div class="card-title">
                                        <h4>Email template edit - {{$title}}</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="form-validation">
                                            <form id="cms-add" class="form-valide add-form" action="<?php echo route('template_add')?>" method="post">
                                                <input type="hidden" id="emailtype" name="emailtype" value="{{$emailtype}}">
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Subject <span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control" id="subject" name="subject"  value="{{$subject}}" placeholder="Enter Subject">
                                                    </div>
                                                </div> 
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Content <span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                        <textarea type="text" class="form-control" id="html_content" name="html_content" placeholder="Enter content">{{$content}}</textarea>
                                                    </div>
                                                </div>                                                
                                                <div class="form-group row">
                                                    <div class="col-lg-8 ml-auto">
                                                        <button type="button" class="btn btn-default cancel-form">Cancel</button>   
                                                        <button type="submit" class="btn btn-primary">Submit</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /# card -->
                        </div>
                        <!-- /# column -->
                    </div>
                    <!-- /# row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>2020 © carryOn DXB Admin.</p>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    @endsection


    @push('scripts')

    <script src="{{ asset('dxblounge/js/lib/select2/select2.full.min.js') }}"></script>
    <!-- Form validation -->
    <script src="{{ asset('dxblounge/js/lib/form-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('dxblounge/js/lib/form-validation/jquery.validate-init.js') }}"></script>

    <script>
      $(document).ready(function(){       
        CKEDITOR.replace('html_content', {
            allowedContent: true,
            on: {
                  change: function() {
                      this.updateElement();    
                  }
            }
        })
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var form = $( "#cms-add" );
        form.validate();
        $( "#cms-add" ).on("submit", function() {
            $('#html_content').html(CKEDITOR.instances.html_content.getData());
            if(form.valid() == true) {
                //console.log(CKEDITOR.instances.html_content.getData());
                var data = form.serialize();
                // console.log(data);
                $('#overlay').show();
                $.ajax({
                    url: form.attr('action'),
                    method: "post",
                    data: data,
                    success: function(data) {
                        data = JSON.parse(data);
                        // $('.list-block').show();
                        // $('.edit-add-block').hide();
                        if(data.status == true) {
                            swal({
                                title: "Success",
                                text: "Saved successfully",
                                timer: 2000,
                                showConfirmButton: false
                            });
                            setTimeout(() => {
                                location.reload();
                            }, 2000);
                        } else {
                            swal({
                                title: "Error",
                                text: data.message,
                                timer: 2000,
                                showConfirmButton: false
                            });
                        }
                    },
                    complete: function() {
                        $('#overlay').hide();
                    }
                });
            }
        });

        $('.add-new-item').on('click', function() {
            $( "#cms-add" ).attr('action', "<?php echo route('cms_add')?>");
            $('#cms').val('');
            $('#cms_id').val('');
        });

        $('.edit-item').on("click", function() {
            var id = $(this).data('item');
            //alert(id);
            $( "#cms-add" ).attr('action', "<?php echo route('cms_edit')?>");
            $('#cms_id').val(id);
            $('#overlay').show();
            $.ajax({
                url: "<?php echo route('cms_data')?>",
                method: "post",
                data: {
                    id: id
                },
                success: function(data) {
                    data = JSON.parse(data);
                    console.log(data)
                    if(data.status == true) {
                        $('#url_key').val(data.cms.url_key);
                        $('#page_title').val(data.cms.page_title);
                        $('#cms_id').val(id);
                        CKEDITOR.instances.html_content.setData(data.cms.html_content);
                        $('#html_content').val(data.cms.html_content);
                        $('#visibility').val(data.cms.visibility);
                        $('.list-block').hide();
                        $('.edit-add-block').show();
                    }
                },
                complete: function() {
                    $('#overlay').hide();
                }
            });
        });
      });
        $('.delete-item').on("click", function() {
            var id = $(this).data('item');
            $('#overlay').show();
            $.ajax({
                url: "<?php echo route('cms_delete')?>",
                method: "post",
                data: {
                    id: id
                },
                success: function(data) {
                    data = JSON.parse(data);

                        if(data.status == true) {
                            swal({
                                title: "Success",
                                text: "Deleted successfully",
                                timer: 2000,
                                showConfirmButton: false
                            });
                            setTimeout(() => {
                                location.reload();
                            }, 1000);
                        } else {
                            swal({
                                title: "Error",
                                text: data.message,
                                timer: 2000,
                                showConfirmButton: false
                            });
                        }
                },
                complete: function() {
                    $('#overlay').hide();
                }
            });
        });          
    </script>
@endpush