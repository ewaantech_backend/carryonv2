
@extends('admin.layouts.app')
@section('content')
<div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Update  Settings</h1>
                            </div>
                        </div>
                    </div>
                    </div>
                                    @if (session('status'))
                      <div class="alert alert-success" id="success_message" role="alert">
                        {{session('status')}}
                      </div>
                    @endif
                <section id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="form-validation">
                                        <form class="form-valide direct_submit" action="{{ route('settings_edit') }}" method="POST" enctype="multipart/form-data">
<!--                                            <input type="hidden" name="_token" value="{{ csrf_token() }}"> -->
                                            @csrf
                                                    @if (count($settings) > 0)
                                                        @foreach ($settings as $setting)
                                                            <div class="form-group row">
                                                                <label class="col-lg-4 col-form-label" for="val-currency">{{$setting->display_name}}  <span class="text-danger">*</span></label>
                                                                <div class="col-lg-6">
                                                                    <input type="text" class="form-control" id="{{$setting->key}}" name="{{$setting->key}}" required value="{{$setting->value}}" >
                                                                </div>
                                                            </div>  
                                                        @endforeach
                                                    @endif                                        
                                            <div class="form-group row">
                                                <div class="col-lg-8 ml-auto">
                                                    <button type="submit" class="btn btn-primary btn-sm m-b-10 m-l-5">Update</button>
<!--                                                    <button type="button" id="cancel_button" class="btn btn-default btn-sm m-b-10 m-l-5">Cancel</button>-->
                                                </div>
                                            </div>
                                                               
                                           
                                            
                                            
                                           
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                </section>
            </div>
        </div>
    </div>
@endsection


@push('scripts')

<script src="{{ asset('dxblounge/js/lib/select2/select2.full.min.js') }}"></script>
<!-- Form validation -->
<!-- <script src="{{ asset('dxblounge/js/lib/form-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('dxblounge/js/lib/form-validation/jquery.validate-init.js') }}"></script> -->
<script>
        $(document).ready(function(){
          $("#cancel_button").click(function(){
            location.href = "{{url('admin/ingredients')}}";
          });
        });
    </script>

@endpush