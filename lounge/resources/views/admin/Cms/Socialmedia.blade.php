@extends('admin.layouts.app')

@section('content')

    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                
                <!-- /# row -->
                <section id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card list-block">
                                <div class="card-title">
                                    <h4>Social Media Management</h4>
                                </div>
                                <div class="invoice-edit">
                                    
                                    <!--.row-->
                                    <div class="invoicelist-body">
                                        <div class="bootstrap-data-table-panel">
                                            <div class="table-responsive">
                                                <table  id="bootstrap-data-table" class="table table-striped table-bordered">
<!--                                                    <table class="table table-striped table-bordered">-->
                                                    <thead>
                                                        <tr>
                                                            <th>Sm #</th>
                                                            <th>Name</th>
                                                            <th>link</th>
                                                            <th>visibility</th>
                                                            <th>Position</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if (count($cmsdetails) > 0)
                                                        @php
                                                          $count = 1;
                                                        @endphp
                                                        @foreach ($cmsdetails as $cms)
                                                                    <tr>
<!--                                                                    <td><td><a class="edit-item" data-item={{$cms->id}}>{{$cms->id}}</a></td></td>-->
                                                                    <td>{{$count++}}</td>
                                                                    <td>{{$cms->social_media_name}}</td>
                                                                    <td>{{$cms->social_media_link}}</td>
                                                                    <td>{{$cms->visibility}}</td>
                                                                    <td>{{$cms->position}}</td>
                                                                    <td><a class="edit-item" data-item={{$cms->id}}><span><i style='color:blue;' class="ti-pencil-alt pull-right"></span></i> </td></a>
<!--                                                                    <td><a class="delete-item" data-item={{$cms->id}} ><i style='margin-left: 10px;color:red;' class="ti-trash pull-right"></i></a><a class="edit-item" data-item={{$cms->id}}><span><i style='color:blue;' class="ti-pencil-alt pull-right"></span></i> </td></a>-->
                                                                    </tr>
                                                      @endforeach
                                                    @else
                                                        <tr>
                                                            <td colspan="7" class="text-center"> No Cms Available</td>
                                                        </tr>
                                                    @endif
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                                    @php
                                                    if($cmsdetails[0]->short_code == NULL){
                                                    @endphp
                                                    <a class="control newRow add-new-item" href="#">+ Add CMS</a>
                                                    @php
                                                    } 
                                                    @endphp                       
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="edit-add-block">
                                <div class="card">
                                    <div class="card-title">
                                        <h4>CMS Add/Edit</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="form-validation">
                                            <form id="cms-add" class="form-valide add-form" method="post">
                                                <input type="hidden" id="cms_id" name="cms_id">
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Social Media Name <span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control" id="social_media_name" name="social_media_name" placeholder="Enter a url key.." disabled="disabled" />
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Social Media Link <span class="text-danger"></span></label>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control" id="social_media_link" name="social_media_link" placeholder="Enter a page title">
                                                    </div>
                                                </div>  
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Position <span class="text-danger"></span></label>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control" id="position" name="position" placeholder="Enter a page title">
                                                    </div>
                                                </div>                                                
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Visibility<span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                        <select class="form-control" id="visibility" name="visibility">
                                                            <option value="YES">YES</option>
                                                            <option value="NO">NO</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-lg-8 ml-auto">
                                                        <button type="button" class="btn btn-default cancel-form">Cancel</button>   
                                                        <button type="submit" class="btn btn-primary">Submit</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /# card -->
                        </div>
                        <!-- /# column -->
                    </div>
                    <!-- /# row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>2020 © carryOn DXB Admin.</p>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    @endsection


    @push('scripts')

    <script src="{{ asset('dxblounge/js/lib/select2/select2.full.min.js') }}"></script>
    <!-- Form validation -->
    <script src="{{ asset('dxblounge/js/lib/form-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('dxblounge/js/lib/form-validation/jquery.validate-init.js') }}"></script>

    <script>
      $(document).ready(function(){       
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
            var form = $( "#cms-add" );
        form.validate();
        $( "#cms-add" ).on("submit", function() {
            if(form.valid() == true) {
                var data = form.serialize();
                $('#overlay').show();
                $.ajax({
                    url: form.attr('action'),
                    method: "post",
                    data: data,
                    success: function(data) {
                        data = JSON.parse(data);
                        // $('.list-block').show();
                        // $('.edit-add-block').hide();
                        if(data.status == true) {
                            swal({
                                title: "Success",
                                text: "Saved successfully",
                                timer: 2000,
                                showConfirmButton: false
                            });
                            setTimeout(() => {
                                location.reload();
                            }, 2000);
                        } else {
                            swal({
                                title: "Error",
                                text: data.message,
                                timer: 2000,
                                showConfirmButton: false
                            });
                        }
                    },
                    complete: function() {
                        $('#overlay').hide();
                    }
                });
            }
        });    
        
        
        $('.edit-item').on("click", function() {
            var id = $(this).data('item');
            //alert(id);
            $("#cms-add" ).attr('action', "<?php echo route('socialmedia_edit')?>");
            $('#cms_id').val(id);
            $('#overlay').show();
            $.ajax({
                url: "<?php echo route('socialmedia_data')?>",
                method: "post",
                data: {
                    id: id
                },
                success: function(data) {
                    data = JSON.parse(data);
                    console.log(data)
                    if(data.status == true) {
                        $('#social_media_name').val(data.cms.social_media_name);
                        $('#social_media_link').val(data.cms.social_media_link);
                        $('#position').val(data.cms.position);
                        $('#cms_id').val(id);
                        $('#visibility').val(data.cms.visibility);
                        $('.list-block').hide();
                        $('.edit-add-block').show();
                    }
                },
                complete: function() {
                    $('#overlay').hide();
                }
            });
        });
      });         
    </script>
@endpush