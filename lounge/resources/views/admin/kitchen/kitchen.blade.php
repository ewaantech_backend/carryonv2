@extends('admin.layouts.app')

@section('content')

  <div id="container">
    

    <div id="content">
      <!-- <aside id="sidebar">
        <ul>
          <li><a href="#"><i class="fa fa-files-o" aria-hidden="true"></i> Orders</a> </li>
          <li><a href="#"><i class="fa fa-times" aria-hidden="true"></i> Logout</a></li>
        </ul>
      </aside> -->
      <div class="status-wrapper">
        <button class="navicon"><i class="fa fa-bars" aria-hidden="true"></i> </button>
        <div id="deligate" style="display:none;"></div>
        <!-- Modal -->
        <div class="modal" id="statusChangeMoal" tabindex="-1" role="dialog" aria-labelledby="statusChangeMoalLabel"
          aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <div class="row text-center">
                  <div class="col-lg-12">
                    <h4 id="enc_order">BUMP : COD00001231</h4>
                    <input id="change_st_order" type="hidden" value="">
                    <a class="button rfp-st">Print and Ready for pickup</a>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-12">
                    <h4 class="text-center">Change Status</h4>
                    <a value="ORDER_CONFIRMED" class="o-st ORDER_CONFIRMED button button-success">Order Confirmed</a>
                    <a value="IN_KITCHEN" class="o-st IN_KITCHEN button">In Kitchen</a>
                    <a value="READY_FOR_PICKUP" class="o-st READY_FOR_PICKUP button">Ready for Pickup</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div class="modal" id="addonModal" tabindex="-1" role="dialog" aria-labelledby="addonModalLabel"
          aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                  <ul class="list-undtyled" id="addon_content">
                      
                  </ul>  
              </div>
            </div>
          </div>
        </div>

        <div class="container-fluid">
        <iframe style="display:none;"
            type="application/pdf"
            src=""
            id="pdfDocument"
            width="100%"
            height="100%"></iframe>
          <ul class="list-undtyled order-list">

          </ul>
          <p>2020 © carryOn DXB Admin.</p>
        </div>
      </div>
    </div>

    </div>

</html>

@endsection

@push('scripts')
<script>
    $(document).ready(function() {
      $('.order-visible-item').css('visibility', 'visible');

      @if( in_array('kitchen', $user_has_roles) )
        localStorage.setItem("ordercount", '0');
        localStorage.setItem("kitchenordercount", '1000000000000000000');
      @endif

      $.ajax({
        url: "<?php echo route('ordercount') ?>",
        type: 'post',
        data: {},
        success: function(data) {
            data = JSON.parse(data);
            localStorage.setItem("ordercount", data.count);
            localStorage.setItem("kitchenordercount", data.kitchen_count);
            localStorage.setItem("pickupordercount", data.pickup_count);
            return true;
        }
      });

      setInterval(() => {
        var active_tab = $('.tab-order-type li.active input').val();
        if (active_tab == 'UPCOMING') {
          $.ajax({
            url: "<?php echo route('ordercount') ?>",
            type: 'post',
            data: {},
            success: function(data) {
              data = JSON.parse(data);
              var older_order_count = localStorage.getItem("ordercount");
              var older_kitchen_order_count = localStorage.getItem("kitchenordercount");
              if ((data.count != older_order_count) || (parseInt(data.kitchen_count) > parseInt(older_kitchen_order_count))) {
                $('#deligate').click(); 
                for(i = 1;i<4; i++) {
                  setTimeout(function(){
                    $('#deligate').click();
                  }, 2500 * i); 
                }
              }
              localStorage.setItem("ordercount", data.count);
              localStorage.setItem("kitchenordercount", parseInt(data.kitchen_count));
              return true;
            }
          });
        }
        loadData(false);
      }, 10000);

      $('#deligate').on('click', function() {
        var context = new AudioContext()
        context.resume()
        var o = context.createOscillator()
        var  g = context.createGain()
        var frequency = 830.6
        o.frequency.value = frequency
        o.connect(g)
        g.connect(context.destination)
        g.gain.exponentialRampToValueAtTime(0.00001, context.currentTime + 5)
        o.start();
        setTimeout(function(){
            o.stop();
        }, 2000); 
      });

      // $(".same-height").matchHeight();
      // objectFitImages();
      // SVGInject(document.querySelector("img.injectable"));
      $("#content .navicon").on("click", function() { 
        $("body").toggleClass("sidebar-open")
      });

      $('.tab-order-type li').on('click', function() {
          $('.tab-order-type li').removeClass('active');
          $(this).addClass('active');
          loadData();
      });

      loadData();
      function loadData(no_loader = true) {
        if( no_loader == true)
          $('#overlay').show();
        $.ajax({
          url: "<?php echo route('kitchen_view'); ?>",
          data: {
            status_type: $('.tab-order-type li.active input').val()
          },
          type: "post",
          success: function(data) {
            data = JSON.parse(data);
            var string = '';
            if( data.orders.aaData.length == 0) {
              $('.order-list').empty().append('<i class="no-kitchen-orders">No orders to show!</i>');
              return false;
            }
            for(var i=0; i< data.orders.aaData.length; i++) {
              order = data.orders.aaData[i];
              col_f = order[9] == '' ? 12 : 8;
              col_s = order[9] == '' ? 0 : 4;
              card_header_color = order[7] == 'violet' ? '' : order[7]; 
              var display_content = order[4].split("#%^_");
              string += `<li>
                <div class="card ${card_header_color}">
                  <header class="card-header">
                    <div class="row align-items-center">
                      <div class="col-${col_f}">
                        <h3>${order[0]} - ${order[5]}</h3>
                        ${order[2]}
                      </div>
                      <div class="col-${col_s} text-right">
                        ${order[9]}
                      </div>
                    </div>
                  </header>
                  <div class="card-body">
                    <ul class="list-unstyled item-list">
                    ${display_content[0]}
                    </ul>
                  </div>
                  <footer class="card-footer">
                    <a class="linktexts" href="/admin/orders/details/${order[6]}">View details<i> <img src="{{ asset('dxblounge/kitchen/assets/images/eye.png') }}" alt="icon"></i></a>
                    <a class="invoice_order_item linktexts" order_id="${order[6]}">Print invoice <i> <img src="{{ asset('dxblounge/kitchen/assets/images/print.png') }}" alt="icon"></i></a>`;
                    if (display_content[1] == 'true') {
                        //console.log(order[5]);
                        //  string += `<a class="order_addons" id="${order[6]}">Addons <i> <img src="{{ asset('dxblounge/kitchen/assets/images/addon.png') }}" alt="icon"></i></a>`;
                    }
                    string += `<a enc_order="${order[0]}" order_id="${order[6]}" status="${order[8]}" class="button bump">bump</a>
                  </footer>
                </div>
              </li>`;
            }
            $('.order-list').empty().append(string);
          },
          complete: function(){
            $('#overlay').hide();
          }
        });
      }

      formatDate();
      setInterval(() => {
          formatDate();
      }, 60000);

      function formatDate() {
        // 19 FEB 2020, 4:23 PM
            var d = new Date();
            var localTime = d.getTime();
            var localOffset = d.getTimezoneOffset() * 60000;
            var utc = localTime + localOffset;
            var offset = 4;    //UTC of Dubai is +04.00
            var dubai = utc + (3600000*offset);
            var date = new Date(dubai); 
            
            var hours = date.getHours();
            var minutes = date.getMinutes();
            var ampm = hours >= 12 ? 'PM' : 'AM';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0'+minutes : minutes;
            var strTime = hours + ':' + minutes + ' ' + ampm;
            var montNames = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
            var datef = date.getDate() + " " + montNames[date.getMonth()] + " " + date.getFullYear() + ",  " + strTime;

            $('.date-time').text(datef);
        }

        $(document).on('click', '.invoice_order_item', function() {
          var order_id = $(this).attr('order_id');
          var dataToSubmit = {"orderid":order_id};   
          loadInvData("<?php echo route('adminpdfreport'); ?>", 'pdfreport', dataToSubmit);
        });

        function loadInvData(url, tab, data) {
          $("#overlay").show();
          $.ajax({
            url: url,
            type: 'post',
            data: data,
            success: function(data) {
              data = JSON.parse(data);
              if( tab == 'pdfreport') {
                  $('#pdfDocument').attr("src", data.filepath);
                  printDocument('pdfDocument');
              }
            $("#overlay").hide();
            },
            complete: function() {
                $("#overlay").hide();
            }
          });
        }

        function printDocument(documentId) {
          var doc = document.getElementById(documentId);
          setTimeout(function(){doc.contentWindow.print();}, 1000);
        }

        $('.order-list').on('click', '.bump', function() {
          var order_id = $(this).attr('order_id');
          var status = $(this).attr('status');
          var enc_order = $(this).attr('enc_order');
          $('#enc_order').text('BUMP : ' + enc_order);
          $('#change_st_order').val(order_id);
          $('.o-st').removeClass('button-success');
          $('.' + status).addClass('button-success');
          $('#statusChangeMoal').modal('show');
        });
        
        $('.order-list').on('click', '.order_addons', function(no_loader = true) {
            if( no_loader == true)
              $('#overlay').show();
            $.ajax({
              url: "<?php echo route('kitchen_addon_view'); ?>",
              data: {
                id: $(this).attr('id')
              },
              type: "post",
              success: function(data) {
                data = JSON.parse(data);
                $('#addon_content').empty().append(data.content);
              },
              complete: function(){
                $('#overlay').hide();
              }
            });
          $('#addonModal').modal('show');
        });

        $('#statusChangeMoal').on('click', '.rfp-st', function() {
          var order_id = $('#change_st_order').val();
          $('.o-st[value="READY_FOR_PICKUP"]').trigger('click');
          $('.invoice_order_item[order_id="'+ order_id +'"]').trigger('click');
        });

        $('#statusChangeMoal').on('click', '.o-st', function() {
          var status = $(this).attr('value');
          var order_id = $('#change_st_order').val();
          $('#overlay').show();
          $.ajax({
            url: "<?php echo route('change_status')?>",
            type: "post",
            data: {
                order_id: order_id,
                order_status: status
            },
            success: function(data) {
              data = JSON.parse(data);
              if(data.status == true) {
                swal({
                    title: "Success",
                    text: "Status updated successfully",
                    timer: 2000,
                    showConfirmButton: false
                });
                loadData();
                $('#statusChangeMoal').modal('hide');
              } else {
                 swal({
                    title: "Failed",
                    text: data.message,
                    timer: 2000,
                    showConfirmButton: false
                });
                loadData();
                $('#statusChangeMoal').modal('hide');                 
              }
            }
          });
        });

    });
</script>
@endpush