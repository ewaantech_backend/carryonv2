
@extends('admin.layouts.app')
@section('content')
<div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Update Tags</h1>
                            </div>
                        </div>
                    </div>
                   
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb">
                                    <!-- <li class="breadcrumb-item"><a href="#">Create Allergies</a></li> -->
                                </ol>
                            </div>
                        </div>
                    </div>
                    </div>
                                              
                                    @php
                                    $count = 1;
                                    @endphp
                                    @foreach ($allergies as $allergiey)
                                    @php
                                    $aller_id= $allergiey->id; 
                                    $aller_name= $allergiey->name;
                                    $aller_description= $allergiey->description;
                                    $aller_visibility= $allergiey->visibility;
                                    $aller_image= $allergiey->image_path;
                                    @endphp
                                  
                <section id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="form-validation">
                                        <form class="form-valide direct_submit" action="{{ route('update') }}"  method="POST" id="allergies-add" enctype="multipart/form-data">
                                        <div class="alert alert-danger print-error-msg" style="display:none">
                                        <ul></ul>
                                        </div>
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" value="{{ $aller_id }}" name="aller_id"> 
                                        <div class="form-group row">
                                                <label class="col-lg-4 col-form-label" for="val-currency">Name  <span class="text-danger">*</span></label>
                                                <div class="col-lg-6">
                                                    <input type="text" class="form-control" id="name" name="name"  value="{{$aller_name}}" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-lg-4 col-form-label" for="val-website">Description <span class="text-danger">*</span></label>
                                                <div class="col-lg-6">
                                                <div class="form-group">
                                         <textarea  class="form-control" rows="50" cols="50" id="description" name="description" value="" required>{{ $aller_description }}</textarea>
                                        </div>
                                          </div>
                                            </div>
                                        <div class="form-group row">
                                                <label class="col-lg-4 col-form-label" for="val-skill">visibility <span class="text-danger">*</span></label>
                                                <div class="col-lg-6">
                                                    <select class="form-control" id="visibility" name="visibility" required>
                                                        <option @if($aller_visibility=='YES') selected @endif value="YES">YES</option>
                                                        <option @if($aller_visibility=='NO') selected @endif value="NO">NO</option>
                                                </select>
                                                </div>
                                        </div>
                                        <!-- <div class="form-group row">
                                                <label class="col-lg-4 col-form-label" for="val-currency">Position  <span class="text-danger">*</span></label>
                                                <div class="col-lg-6">
                                                    <input type="text" class="form-control" id="aller_position" name="position" required>
                                                </div>
                                        </div> -->
                                        <div class="form-group row">
                                                <label class="col-lg-4 col-form-label" for="val-website">Upload Image <span class="text-danger">*</span></label>
                                                <div class="col-lg-6">
                                                <!-- <img src="/images/frontend/allergy/{{ $aller_image }}"> -->
                                                <img src="{{asset("$aller_image")}}" style="width:75px;height:75" class="img-thumbnail"><br><br>
                                                <input type="file" class="form-control-file" id="image" name="image">
                                        </div>
                                        </div>
                                        <div class="form-group row">
                                                <div class="col-lg-8 ml-auto">
                                                    <button type="submit" class="btn btn-primary btn-sm m-b-10 m-l-5">Submit</button>
                                                    <button type="button" id="cancel_button" class="btn btn-default btn-sm m-b-10 m-l-5">Cancel</button>
                                                </div>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                </section>
                @endforeach
            </div>
        </div>
    </div>
@endsection



@push('scripts')

<script src="{{ asset('dxblounge/js/lib/select2/select2.full.min.js') }}"></script>
<!-- Form validation -->
<script src="{{ asset('dxblounge/js/lib/form-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('dxblounge/js/lib/form-validation/jquery.validate-init.js') }}"></script>

<!-- ajax validation -->
<!-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="http://malsup.github.com/jquery.form.js"></script>      

    <script>
        $(document).ready(function(){
          $("#cancel_button").click(function(){
            location.href = "{{url('admin/allergies')}}";
          });
        });
    </script>

@endpush