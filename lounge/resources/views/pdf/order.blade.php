<!DOCTYPE html
PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<!-- Meta Tags -->
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<title>Invoice | Carry On</title>
	</head>
	<body style="margin: 0; padding: 0; font-family: Arial, Helvetica, sans-serif;">
		<table width="100%" border="0" style=" border-collapse: collapse; border-spacing: 0;">
			<tr>
			<td bgcolor="#f16521" align="center" style="vertical-align: top; padding: 0px;">
				<table width="95%" border="0" align="center" style=" border-collapse: collapse; border-spacing: 0;">
					<tr>
					<td bgcolor="#f16521" align="center" style=" vertical-align: top; padding: 0px;">
						<table width="100%" style=" border-collapse: collapse; border-spacing: 0;">
							<tr>
								<td height="20px"></td>
							</tr>
							<tr>
								<td style=" vertical-align: top; padding: 0px;">
									<table width="100%" style=" border-collapse: collapse; border-spacing: 0;">
									<tr>
										<td style="vertical-align: top; padding: 0px;">
											<div style="font-size: 36px; color: #fff; font-weight: bold;">
											TAX INVOICE
											</div>
											<br />
											<div style="font-size: 18px; color: #fff; line-height: 1.5;">
											TRN# <br />
											<b>1002 4427 9400 003</b>
											</div>
										</td>
										<td style="text-align: right; vertical-align: middle; padding: 0px;">
										<a href="#" style="color: #000; text-decoration: none;"><img style="max-width: 300px;"
										src="images/pdf-logo.png" alt="Logo" /></a>
										</td>
									</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td height="20px"></td>
							</tr>
						</table>
					</td>
					</tr>
				</table>
			</td>
			</tr>
			<tr>
			<td height="20px"></td>
			</tr>
			<tr>
			<td align="center" style=" vertical-align: top; padding: 0px;">
				<table width="100%" align="center" style=" border-collapse: collapse; border-spacing: 0;">
					<tr>
						<td style=" vertical-align: top; padding: 0px;">
							<div style="font-size: 36px; color:#000; font-weight: bold; text-align: center;">
							Hi {!! $customername !!},
							</div>
							<br />
							<div style="font-size: 18px; color:#000; text-align: center;">
							 thank you for your order!
							</div>
							<div style="font-size: 18px; color:#000; text-align: center;">
								<img src="{{ $qr_code }}" />
							</div>
						</td>
						</tr>
						<tr>
						<td height="20px"></td>
					</tr>
					<tr>
					<td>
						<table width="100%" align="center" style=" border-collapse: collapse; border-spacing: 0;font-size: 14px">
						<thead>
							<tr>
								<th
								style="background-color: #f16521; padding: 10px;; color: #fff; text-transform: uppercase; text-align: left;">
								PRODUCT NAME
								</th>
								<th
								style="background-color: #f16521; padding: 10px;; color: #fff; text-transform: uppercase; text-align: left;">
								UNIT PRICE
								</th>
								<th
								style="background-color: #f16521; padding: 10px;; color: #fff; text-transform: uppercase; text-align: right;">
								QUANTITY
								</th>
								<th
								style="background-color: #f16521; padding: 10px;; color: #fff; text-transform: uppercase; text-align: right;">
								AMOUNT
								</th>
							</tr>
							</thead>
							<tbody>
							{!! $content !!}
							</tbody>
							<tfoot>
							<tr>
								<td></td>
								<td  colspan="2" style="text-align:right; padding: 10px 7px; background-color:#f16521; color: #fff;">
								SUB TOTAL
								</td>
								<td style="text-align:right; padding: 10px 7px; background-color:#f16521; color: #fff;">
								AED {!! $sub_total !!}
								</td>
							</tr>
							<tr>
								<td></td>
								<td  colspan="2" style="text-align:right; padding: 10px 7px; color: #fff;">
								VAT
								</td>
								<td style="text-align:right; padding: 10px 7px; color: #fff;">
								AED {!! $tax_total !!}
								</td>
							</tr>                                                            
							<tr>
								<td></td>
								<td  colspan="2" style="text-align:right; padding: 10px 7px; background-color:#f16521; color: #fff;">
								TOTAL(Including VAT)
								</td>
								<td style="text-align:right; padding: 10px 7px; background-color:#f16521; color: #fff;">
								AED {!! $total !!}
								</td>
							</tr>
							</tfoot>
						</table>
					</td>
					</tr>
				</table>
			</td>
			</tr>
			<tr>
			<td height="70px"></td>
			</tr>
<!--			<tr>
			<td bgcolor="#f16521" align="center" style=" vertical-align: top; padding: 0px;">
				
			</td>
			</tr>-->
		</table>

	<table width="100%" align="center" border="0" style="position: absolute;bottom:170px;border-collapse: collapse; border-spacing: 0;">
					<tr>
					<td bgcolor="#f16521" align="center" style=" vertical-align: top; padding-left: 10px;">
						<table width="100%" border="0" align="center" style=" border-collapse: collapse; border-spacing: 0;">
							<tr>
								<td height="30px"></td>
							</tr>
							<tr>
								<td width="50%" align="left">
									<table style=" border-collapse: collapse; border-spacing: 0;">
										<tr>
										<td style=" vertical-align: top; padding: 0px;">
											<div style="color:#fff; font-weight:bold; font-size: 16px; margin-bottom: 5px;">
											Company Address
											</div>
											<div style="color:#fff; font-size: 14px; line-height: 1.4;">
											451 Lounge, <br />
											Airport Terminal 3, <br />
											Dubai, United Arab Emirates
											</div>
										</td>
										<td width="5%"></td>
										<td style="vertical-align: top; padding: 0px;">
											<div style="color:#fff; font-weight:bold; font-size: 16px;">
											Telephone
											</div>
											<div style="color:#fff; font-size: 14px; line-height: 1.4;">
											+971&nbsp;(0)&nbsp;50&nbsp;298&nbsp;7966
											</div><br />
											<div style="color:#fff; font-weight:bold; font-size: 16px;">
											Email
											</div>
											<div style="color:#fff; font-size: 14px; line-height: 1.4;">
											<a href="mailto:hello@carryondxb.com"
											style="color: #fff; text-decoration: none;">hello@carryondxb.com</a>
											</div>
										</td>
										</tr>
									</table>
								</td>
								<td width="50%" align="right">
									<table style=" border-collapse: collapse; border-spacing: 0;">
										<tr>
										<td style=" vertical-align: top; padding: 0px;">
											<div style="color:#fff; font-weight:bold; font-size: 16px;">
											Order Number
											</div>
											<div style="color:#fff; font-size: 14px; line-height: 1.4;">
											{!! $invID !!}
											</div><br />
											<div style="color:#f16521; font-weight:bold; font-size: 16px;">
											Order Date
											</div>
											<div style="color:#fff; font-size: 14px; line-height: 1.4;">
											{!! $order_at !!}
											</div>
										</td>
										<td width="5%"></td>
										<td style=" vertical-align: top; padding: 0px;">
											<div style="color:#fff; font-weight:bold; font-size: 16px; margin-bottom: 5px;">
											Billing Address
											</div>
											<div style="color:#fff; font-size: 14px; line-height: 1.4;">
											{!! $billing_address !!}
											</div>
										</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td height="30px"></td>
							</tr>
						</table>
					</td>
					</tr>
				</table>


	</body>
</html>