<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CancelEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function build()
    {        
        return $this->view('emails.cancelmail')
                    ->from('noreply@carryon.com', 'CARRYON')
                    ->subject($this->data['subject'])
                    ->with(['order_id' => $this->data['order_id'], 'url_link' =>  $this->data['url_link'] ]);
    }
}