<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReviewEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function build()
    {        
        return $this->view('emails.reviewmail')
                    ->from('noreply@carryon.com', 'CARRYON')
                    ->subject($this->data['subject'])
                    ->with(['cust_name' => $this->data['cust_name'],'reviewlink' =>$this->data['reviewlink']]);
    }
}