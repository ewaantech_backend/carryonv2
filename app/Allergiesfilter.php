<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Allergiesfilter extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'allergies_filter';

    protected $fillable = [
        'name', 'description', 'visibility' , 'image_path'
    ];
}