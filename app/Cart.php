<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'cart';

    protected $fillable = [
        'users_id', 'item_count', 'grant_total','sub_total','tax_total','is_guest','created_at' , 'updated_at'
    ];
}