<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Readymail extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'ready_mail';

    protected $fillable = [
        'subject','cust_name','nearest_gate' , 'short_link_text','order_number','pickup_point','item_content','maplink','created_at','updated_at'
    ];
}