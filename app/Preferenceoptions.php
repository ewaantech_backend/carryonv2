<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Preferenceoptions extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'preferenceoptions';

    protected $fillable = [
        'name', 'type'
    ];
}