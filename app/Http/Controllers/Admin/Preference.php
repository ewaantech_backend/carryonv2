<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Preferenceoptions;
use Exception;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class Preference extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions_data = user_permissions(Auth::user());
        return view('admin.preference.Preference', array(
            'preferences' => $this->fetchAllergies(),
            'permissions' => $permissions_data['permissions'],
            'user_has_roles' => $permissions_data['roles']
        ));
    }


    // Getting Allergies list
    private function fetchAllergies()
    {
        $preferences = DB::table('preferenceoptions')
            ->select('id', 'name', 'type', 'created_at', 'updated_at', 'deleted_at')
            ->orderBy('created_at', 'desc')
            ->get();
        return $preferences;
    }


    public function create(Request $request)
    {
        return view('admin.preference.PreferenceCreate');
    }

    public function add(Request $request)
    {

        $request->validate([
            'name' => 'required',
        ]);
        try {

            $timestamp = date('Y-m-d H:i:s');
            $preferences = new Preferenceoptions;
            $preferences->name = $request->name;
            $preferences->type = $request->pref_type;
            $preferences->created_at = $timestamp;
            $preferences->updated_at = $timestamp;
            $preferences->save();
            $preferenceId = $preferences->id;

            $preferences = Preferenceoptions::find($preferenceId);
            if (!empty($request->file('original_image'))) {
                $request->validate([
                    'original_image' => 'required|image|max:2048',
                ]);

                $request->file('original_image')->storeAs(
                    'preference',
                    'original_image_' . $preferenceId . '.jpeg',
                    'public'
                );
                $preferences->original_image = 'preference/' . 'original_image_' . $preferenceId . '.jpeg';
            }

            if (!empty($request->file('alternative_image'))) {
                $request->validate([
                    'alternative_image' => 'required|image|max:2048',
                ]);

                $request->file('alternative_image')->storeAs(
                    'preference',
                    'alternative_image_' . $preferenceId . '.jpeg',
                    'public'
                );
                $preferences->alternative_image = 'preference/' . 'alternative_image_' . $preferenceId . '.jpeg';
            }
            
            $preferences->save();

            if ($preferenceId) $request->session()->flash('status', 'Preference created successfully');
        } catch (Exception $e) {
            print $e->getMessage();
            exit;
            $request->session()->flash('status', 'An error occured. Please try again');
        }
        return redirect('/admin/preferences');
    }

    public function edit($id)
    {
        //echo  $id;
        $preference = DB::table('preferenceoptions')
            ->select('*')
            ->where([
                ['id', '=', $id],
            ])
            ->get();
        return view('admin.preference.PreferenceEdit', array('preference' => $preference));
    }

    public function update(Request $request)
    {
        try {
            $id = $request->pref_id;
            if (!empty($request->file('original_image'))) {
                $request->validate([
                    'original_image' => 'required|image|max:2048',
                ]);

                Storage::disk('public')->delete('preference/original_image_' . $id . '.jpeg');

                $request->file('original_image')->storeAs(
                    'preference',
                    'original_image_' . $id . '.jpeg',
                    'public'
                );
            }

            if (!empty($request->file('alternative_image'))) {
                $request->validate([
                    'alternative_image' => 'required|image|max:2048',
                ]);

                Storage::disk('public')->delete('preference/alternative_image_' . $id . '.jpeg');

                $request->file('alternative_image')->storeAs(
                    'preference',
                    'alternative_image_' . $id . '.jpeg',
                    'public'
                );
            }


            $pref =  DB::table('preferenceoptions')
                ->where('id', $id)
                ->update([
                    'name'       => $request->post('name'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);



            if ($pref) $request->session()->flash('status', 'Preference updated successfully');
        } catch (Exception $e) {
            print $e->getMessage(); exit;
            $request->session()->flash('status', 'An error occured. Please try again');
        }

        return redirect('/admin/preferences');
    }


    public function delete($id)
    {
        //echo $id;
        DB::table('preferenceoptions')->where('id', $id)->delete();
        return redirect('/admin/preferences');
    }
}
