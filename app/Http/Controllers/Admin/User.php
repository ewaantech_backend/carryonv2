<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\User as Users;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class User extends Controller
{
    public function __construct()
    {
        $this->middleware(['permission:users_view']);
    }

    public function user_add(Request $request) {
        $user_name = $request->post('name');
        $user_email = $request->post('email');
        $user_password = $request->post('password');
        $user_role = $request->post('role_id');
        $user_pickup = $request->post('pickup_point');
        $existCheck = Users::where('email', '=', $user_email)->first();
        if( ! $user_name || ! $user_email || ! $user_role || ! $user_password) {
            echo json_encode(['message'=> 'Provide all fields', 'status' => FALSE]);

            return;
        }
        if ($existCheck !== null) {
            echo json_encode(['message'=> 'Email already exists', 'status' => FALSE]);

            return;
        }

        $user = Users::create( ['name' => $user_name, 'email' => $user_email, 'pickup_point' => $user_pickup,
                                'role_id' => $user_role, 'password' => Hash::make( $user_password ) ] );
        $role = Role::where('id', '=', $user_role)->first();
        $user->assignRole($role->name);

        if( $user ) {
            echo json_encode(['message'=> 'User Added Successfully', 'status' => TRUE]);
        } else {
            echo json_encode(['message'=> 'Something went wrong', 'status' => FALSE]);
        }
        
        return;
    }

    public function user_edit(Request $request) {
        $user_id = $request->post('user_id');
        $user_name = $request->post('name');
        $user_email = $request->post('email');
        $user_password = $request->post('password');
        $user_role = $request->post('role_id');
        $user_pickup = $request->post('pickup_point');
        if( ! $user_name || ! $user_email || ! $user_role) {
            echo json_encode(['message'=> 'Provide all fields', 'status' => FALSE]);

            return;
        }

        $existCheck = Users::where('email', '=', $user_email)->where('id', '!=', $user_id)->first();
        if ($existCheck !== null) {
            echo json_encode(['message'=> 'Email already exists', 'status' => FALSE]);

            return;
        }

        $user = Users::where('id', '=', $user_id)->first();
        $user->name = $user_name;
        $user->email = $user_email;
        if($user_password){
            $user->password = Hash::make( $user_password );
        }
        $user->role_id = $user_role;
        $user->pickup_point = $user_pickup;
        $user->save();

        if($user->roles->first() !== null) {
            $user->removeRole($user->roles->first());
        }
        $role = Role::where('id', '=', $user_role)->first();
        $user->assignRole($role->name);

        if( $user ) {
            echo json_encode(['message'=> 'User Added Successfully', 'status' => TRUE]);
        } else {
            echo json_encode(['message'=> 'Something went wrong', 'status' => FALSE]);
        }
        
        return;
    }

    public function users() {
        $data = $this->user_list_data();

        return view('admin.users.user', $data);
    }

    public function user_details($user_id) {
        $data = $this->user_list_data($user_id);

        return view('admin.users.user', $data);
    }

    function user_list_data($user_id = '') {
        $permissions_data = user_permissions(Auth::user());
        $roles = Role::all();
        if( ! $user_id) {
            $users = DB::table('users')->select('users.id','users.name','users.email',
                        DB::raw('case when(type="ADMIN") then ("******") else(users.remember_token) end as remember_token'),'roles.name as role')
                ->join('roles', 'roles.id', '=', 'users.role_id')->get();
        } else {
            $users = DB::table('users')->select('users.id','users.name','users.email',
                DB::raw('case when(type="ADMIN") then ("******") else(users.remember_token) end as remember_token'),'roles.name as role')
            ->join('roles', 'roles.id', '=', 'users.role_id')
            ->where('users.id', '=', $user_id)->get();
        }
        
        $pickup_points = DB::table('boarding_pickup_map')->select(DB::raw('pickup_point as name'))->groupBy('pickup_point')->get();

        return [
            'user_id' => $user_id,
            'roles' => $roles, 
            'users' => $users,
            'pickup_points' => $pickup_points,
            'permissions' => $permissions_data['permissions'],
            'user_has_roles' => $permissions_data['roles'] ];
    }

    public function user_data(Request $request) {
        $user_id = $request->post('id');
        $user = Users::select('name', 'email', 'role_id', 'pickup_point')->where('id', '=', $user_id)->first();
        if($user !== null) {
            echo json_encode(['message'=> 'User fetched Successfully', 'user' => $user, 'status' => TRUE]);
        } else {
            echo json_encode(['message'=> 'Wrong user', 'status' => FALSE]);
        }

        return;
    }

}
