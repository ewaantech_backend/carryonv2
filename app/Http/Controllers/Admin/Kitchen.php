<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Kitchen extends Controller
{


    function display() {
        $permissions_data = user_permissions(Auth::user());

        return view('admin.kitchen.kitchen', array(
            'permissions' => $permissions_data['permissions'], 
            'user_has_roles' => $permissions_data['roles'],
            'kitchen_login' => TRUE
            )
        );
    }

    function show() {
        date_default_timezone_set('Asia/Dubai');

        $permissions_data = user_permissions(Auth::user());
        $aColumns = array(
            0 => 'id',
            1 => 'flight_number',
            2 => 'pickup_time',
            3 => 'pickup_point',
            4 => 'status',
            5 => 'id',
            6 => 'color',
            7 => 'status',
            8 => 'time_elapsed'
        );

        $sWhere = '';

        $orderBy = " order by pickup_time asc,`order`.id asc ";

        if(isset($_POST['status_orders']) && $_POST['status_orders'] != '' && in_array($_POST['status_orders'], ['DELIVERED', 'CANCELLED']) ) {
            $sWhere .= $sWhere == '' ? 'WHERE ' : ' AND ';
            $sWhere .= ' `order`.status = "'.$_POST['status_orders'].'"';
        } else {
            $sWhere .= $sWhere == '' ? 'WHERE ' : ' AND ';
            $sWhere .= ' `order`.status not in ("DELIVERED", "CANCELLED") ';
            if(isset($_POST['status_type']) && $_POST['status_type'] != '') {
                $sWhere .= $sWhere == '' ? 'WHERE ' : ' AND ';
                if( $_POST['status_type'] == 'UPCOMING') {
                    
                    // In upcoming orders show items with these statuses.
                    $tomorrow = date('Y-m-d', strtotime('+1 day', strtotime(date('Y-m-d')) ) );
                    $today = date('Y-m-d');
                    $status_to_included = '"IN_KITCHEN", "ORDER_CONFIRMED", "READY_FOR_PICKUP"';
                    if( in_array('kitchen', $permissions_data['roles']) ) {
                      $status_to_included = '"IN_KITCHEN", "ORDER_CONFIRMED"';  
                    }
                   
                    $sWhere .= ' `order`.status in ('. $status_to_included .') '.
                    ' and (  date(`order`.`order_at`)="' . $tomorrow . '" OR date(`order`.`order_at`)="' . $today . '") ';
                } else {
                    $sWhere .= ' `order`.status = "'.$_POST['status_type'].'"';
                }
            }
        }

        if( in_array('kitchen', $permissions_data['roles']) ) {
            $sWhere .= $sWhere == '' ? 'WHERE ' : ' AND ';
            $sWhere .= ' (`order`.pickup_reached_at IS NULL OR `order`.pickup_reached_at = "")';
        }

        $date_from = $_POST['date_from'] ?? '';
        $date_to = $_POST['date_to'] ?? '';
        if( $date_from) {
            $sWhere .= $sWhere == '' ? 'WHERE ' : ' AND ';
            $sWhere .= ' date(`order`.`order_at`) >="' . $date_from . '" ';
        }
        if( $date_to) {
            $sWhere .= $sWhere == '' ? 'WHERE ' : ' AND ';
            $sWhere .= ' date(`order`.`order_at`) <="' . $date_to . '" ';
        }
        
        $database_name = env('DB_DATABASE');

        // Get data.
        $model = DB::select("select 
            `order`.id as serial_no,
            `order`.id,
            `order`.is_reimbursed,
            `order`.pickup_reached_at, 
            `order`.flight_number as flight_number, 
            `order`.order_at,
            `order`.pickup_time, 
            `order`.updated_at, 
            `order`.created_at,
            `order`.status as status, 
            `order`.pickup_point as pickup_point,
            `order`.gate,
            `order`.is_lounge as is_lounge,
            users.name as username,
            users.phone_number as phone_number,
            users.id as user_id
        from $database_name.order
        join users on users.id = `order`.users_id
        $sWhere $orderBy");

        $output = array(
            "aaData" => array()
        );
        $date_before30min = strtotime( date('Y-m-d H:i:s') ) - 30*60;
        foreach ( $model as $keys => $md)
        {
            $row = array();
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                $key = $aColumns[$i];
                if($i == 2) {
                    $custome_date = date_format_from_month_custom($md->$key);
                    $custome_date_array = explode(" ", $custome_date);
                    if (!empty($md->$key)) {
                        $day_mnth = date('M d', strtotime($md->$key));
                        $hr_min = date('h.i', strtotime($md->$key));
                        $am_pm = date('A', strtotime($md->$key));
                        $row[] = "<p>$day_mnth, <strong>$hr_min</strong> $am_pm</p>";
                    } else {
                        $row[] = $custome_date;
                    }
                } else if($i == 3) { // 3 and 4 added here.
                    $displayPl = $md->gate == '-' ? '-' : $md->$key;
                    $row[] = $displayPl;
                    // After 3, Items X count inserted.
                    $items = DB::table('order_items')->select('item_count', 'name', 'order_items.id as orderitemid')
                        ->join('products', 'products.id', '=', 'order_items.products_id')
                        ->where('order_id', '=', $md->id)->get();
                    $string = '';
                    $addon_flag = 'false';
                    foreach($items as $item) {
                        $string .= '<li>' . $item->item_count.' <b>X</b> '.$item->name.'</li>';
                        $addon_items = DB::table('order_items')->select('addons_order_items.order_items_id as order_items_id','addons.name as name','addons_order_items.item_count as item_count')
                        ->join('addons_order_items', 'order_items.id', '=', 'addons_order_items.order_items_id')
                        ->join('addons', 'addons.id', '=', 'addons_order_items.addons_id')
                        ->where('order_items.id', '=', $item->orderitemid)->get()->toArray();
                        if (!empty($addon_items)) {
                            $addon_flag = 'true';
                            $string .= '<ul><li style="color: #FF4500;">Addons</li>';
                            foreach($addon_items as $addon_item) {
                                $string .= '<li>' . $addon_item->item_count.' <b>X</b> '.$addon_item->name.'</li>';                               
                            }
                            $string .= '</ul>';
                        }
                    }
                    $row[] =  $string."#%^_".$addon_flag;
//                    $row[] = $addon_flag;
                }
                else if($i == 4) {
                    $row[] = ucwords( strtolower(  str_replace('_', ' ', $md->status) ) );
                } else if($i == 6) {
                    $color = '';
                    $time_greater_than30 = '';
                    $last_updated_time = $md->updated_at;
                    
                    if( $date_before30min - strtotime($last_updated_time) > 0 ) {
                        $time_greater_than30 = TRUE;
                    }

                    if( $time_greater_than30 && $md->status == 'IN_KITCHEN' ) {
                      $color = 'red';
                    }
                    if($color == '' && $md->status == 'IN_KITCHEN' ) {
                      $color = 'orange';
                    }
                    
                    if ($md->is_lounge == 'Y') {
                      $color = $color.' violet';                        
                    }

                    $row[] = $color;
                } else if($i == 8) {
                    $secondsElapsed = strtotime(date('Y-m-d H:i:s')) - strtotime($last_updated_time);
                    $row[] = $this->seconds2hr($secondsElapsed, $md->status);
                } else {
                    if($i == 0) {
                        $row[] = "CON".str_pad($md->$key, 5, '0', STR_PAD_LEFT);
                    } else {
                        $row[] = $md->$key;
                    }
                    
                }
                
            }
            $output['aaData'][] = $row;
        }
         echo json_encode( [
            'orders' => $output, 
            'status' => TRUE ] 
        );
    }

    function seconds2hr($ss, $st) {
        if($st != 'IN_KITCHEN') {
            return '';
        }
        $s = $ss%60;
        $m = floor(($ss%3600)/60);
        $h = floor(($ss%86400)/3600);
        $d = floor(($ss%2592000)/86400);
        $M = floor($ss/2592000);
        
        if( $M > 0 || $d > 0 || $h > 0 || $m > 30) {
            return '<span class="duration"><strong>30</strong> min +</span>';
        }

        return '<span class="duration"><strong>' . $m. '</strong> min</span>';
    }
}