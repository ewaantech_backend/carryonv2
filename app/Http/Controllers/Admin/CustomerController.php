<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Customer;

class CustomerController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */ public function index()
    {
     
        return view('admin.Customer.Customer', array('customer_details' => $this->fetchcustomer()));
      
    }


// public function users() {
//         $permissions_data = user_permissions(Auth::user());
//         $roles = Role::all();
//         $users = DB::table('users')->select('users.id','users.name','users.email', 'roles.name as role')
//                 ->join('roles', 'roles.id', '=', 'users.role_id')->get();

//         return view('admin.users.user', [
//             'roles' => $roles, 
//             'users' => $users,
//             'permissions' => $permissions_data ]);
//     }
     // Getting customer details
     private function fetchcustomer(){
        $customer_details = DB::table('users')
                    ->select('id', 'name','email','phone', 'nationality','type')
                    ->where('type', '=','CUSTOMER' )
                    ->orderBy('created_at', 'desc')
                    ->get();
                    //->paginate(10);
        return $customer_details;
    }
     // For checking duplicate categories

}