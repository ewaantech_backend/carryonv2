<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Doneness;
use Illuminate\Support\Facades\Auth;

class DonenessController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     $permissions_data = user_permissions(Auth::user());
        return view('admin.Doneness.Doneness', 
                array('doneness' => $this->fetchDoneness(),
                    'permissions' => $permissions_data['permissions'],
                    'user_has_roles' => $permissions_data['roles']));
      
    }
    public function doneness_add(Request $request) {
   
        $donenessname = $request->post('doneness_name');
        $visibility = $request->post('visibility');
        $created_at = date('Y-m-d H:i:s');
        
        $existCheck = Doneness::where('name','=', $donenessname)->first();
        if (!$donenessname  || !$visibility || !$created_at) {
            echo json_encode(['message' => 'Provide all fields', 'status' => FALSE]);

            return;
        }

        if ($existCheck !== null) {
            echo json_encode(['message' => 'Doneness already exists', 'status' => FALSE]);

            return;
        }
     
        $doneness = Doneness::create(['name' => $donenessname, 
                     'visibility' => $visibility,'created_at' => $created_at]);

        if ($doneness) {
            echo json_encode(['message' => 'Doneness Added Successfully', 'status' => TRUE]);
        } else {
            echo json_encode(['message' => 'Something went wrong', 'status' => FALSE]);
        }

        return;
    }
    public function doneness_edit(Request $request) {
        $doneness_id = $request->post('doneness-id');
        $doneness_name = $request->post('doneness_name');
        $visibility = $request->post('visibility');
        $updated_at = date('Y-m-d H:i:s');
        if (!$doneness_name ||  !$visibility || !$updated_at) {
            echo json_encode(['message' => 'Provide all fields', 'status' => FALSE]);

            return;
        }
        $existCheck =Doneness::where('name', '=', $doneness_name)->where('id', '!=', $doneness_id)->first();
        if ($existCheck !== null) {
            echo json_encode(['message' => 'Doneness already exists', 'status' => FALSE]);

            return;
        }
        $doneness =Doneness::where('id', '=', $doneness_id)->first();
        $doneness->name = $doneness_name;
        $doneness->visibility = $visibility;
        $doneness->updated_at = date('Y-m-d H:i:s');
        
        
        $doneness->save();
        if ($doneness) {
            echo json_encode(['message' => 'Doneness Updated Successfully', 'status' => TRUE]);
        } else {
            echo json_encode(['message' => 'Something went wrong', 'status' => FALSE]);
        }

        return;
    }

    public function doneness_data(Request $request) {
        $doneness_id = $request->post('id');
        $doneness =Doneness::select('id', 'name','visibility')->where('id', '=',   $doneness_id)->first();
        if ( $doneness !== null) {
            echo json_encode(['message' => 'Doneness fetched Successfully', 'cms' =>  $doneness, 'status' => TRUE]);
        } else {
            echo json_encode(['message' => 'Wrong data', 'status' => FALSE]);
        }

        return;
    }
    public function doneness_destroy(Request $request) {
        $doneness_id = $request->post('id');
        $doneness =Doneness::where('id', '=', $doneness_id )->first();
        $doneness->delete();

        if ($doneness) {
            echo json_encode(['message' => 'Deleted Successfully', 'status' => TRUE]);
        } else {
            echo json_encode(['message' => 'Something went wrong', 'status' => FALSE]);
        }

        return;
    }    
      
     // Getting category details
        private function fetchDoneness(){
            $doneness = DB::table('doneness')
                        ->select('id', 'name','visibility', 'created_at','updated_at','deleted_at')
                        ->orderBy('created_at', 'desc')
                        ->get();
                        //->paginate(10);
            return $doneness;
        }
         // For checking duplicate categories
  
}
