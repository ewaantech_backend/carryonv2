<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Addons extends Controller
{

    public function __construct()
    {
        $this->middleware(['permission:addons_view']);
    }

    public function addons() {
        $permissions_data = user_permissions(Auth::user());
        $addons = DB::table('addons')->select('id','name','sku', 'actual_price', 'discount_price','description','visibility','position')->where('deleted_at', '=', NULL)->get();

        return view('admin.addons.addons', [ 
            'addons' => $addons,
            'permissions' => $permissions_data['permissions'],
            'user_has_roles' => $permissions_data['roles'],
            
        ]);
    }

    public function addons_add(Request $request) {
        $addon_name = $request->post('addon_name');
        // $addon_sku = $request->post('addon_sku');
        $addon_sku = '';
        $addon_actual_price = $request->post('addon_actual_price');
        //$addon_discount_price = $request->post('addon_discount_price');
        $addon_discount_price = $request->post('addon_actual_price');
        //$addon_description = $request->post('addon_description');
        $addon_description = '';
        //$addon_url = $request->post('addon_friendly_url');
        $addon_url = '';
        $addon_visibility = $request->post('addon_visibility');
        $addon_position= $request->post('addon_position');

        if( ! $addon_name || ! $addon_actual_price || !$addon_visibility || !$addon_position ) {
            echo json_encode(['message'=> 'Provide all fields', 'status' => FALSE]);

            return;
        }

//        $existCheck = DB::table('addons')->where('sku', '=', $addon_sku)->first();
//        if ($existCheck !== null) {
//            echo json_encode(['message'=> 'Sku already exists', 'status' => FALSE]);
//
//            return;
//        }

        $data = [
            'name' => $addon_name, 
            'sku' => $addon_sku, 
            'actual_price' => $addon_actual_price,
            'discount_price' => $addon_discount_price,
            'url_key' => $addon_url,
            'description'=>$addon_description,
            'visibility'=>$addon_visibility,
            'position'=>$addon_position,
        ];

        $addon = DB::table('addons')->insertGetId( $data );
        if( $addon ) {
            if( $request->file('thumbnail_image') ) {
                $path = 'addon_thumbnails';
                $imageName = time().'.'.$request->thumbnail_image->getClientOriginalExtension();
                $request->thumbnail_image->move(public_path($path), $imageName);
                DB::table('addon_images')->insert(['path' => $path.'/'.$imageName, 'type' => 'THUMBNAIL', 'addons_id' => $addon ]);
            }
    
            if( $request->file('addon_image') ) {
                $data = [];
                $incr = 0;
                $path = 'addon_images';
                foreach( $request->file('addon_image') as $img ) {
                    ++$incr;
                    $imageName = time().$incr.'.'.$img->getClientOriginalExtension();
                    $img->move(public_path($path), $imageName);
                    $data[] = [
                        'path' => $path.'/'.$imageName, 
                        'addons_id' => $addon 
                    ];
                }
                DB::table('addon_images')->insert($data);
            }
        }
        if( $addon ) {
            echo json_encode(['message'=> 'Product Added Successfully', 'status' => TRUE]);
        } else {
            echo json_encode(['message'=> 'Something went wrong', 'status' => FALSE]);
        }
        
        return;
    }

    public function addons_edit(Request $request) {
        $addon_id = $request->post('addon_id');
        $addon_name = $request->post('addon_name');
        //$addon_sku = $request->post('addon_sku');
        $addon_sku = '';
        $addon_actual_price = $request->post('addon_actual_price');
        //$addon_discount_price = $request->post('addon_discount_price');
        $addon_discount_price = $request->post('addon_actual_price');
        //$addon_description = $request->post('addon_description');
        $addon_description = '';
        //$addon_url = $request->post('addon_friendly_url');
        $addon_url = '';
        $addon_visibility = $request->post('addon_visibility');
        $addon_position= $request->post('addon_position');        

        if( ! $addon_name || ! $addon_actual_price || !$addon_visibility || !$addon_position ) {
            echo json_encode(['message'=> 'Provide all fields', 'status' => FALSE]);

            return;
        }

//        $existCheck = DB::table('addons')->where('id', '!=', $addon_id)->where('sku', '=', $addon_sku)->first();
//        if ($existCheck !== null) {
//            echo json_encode(['message'=> 'Sku already exists', 'status' => FALSE]);
//
//            return;
//        }

        $data = [
            'name' => $addon_name, 
            'sku' => $addon_sku, 
            'actual_price' => $addon_actual_price,
            'discount_price' => $addon_discount_price,
            'description'=>$addon_description,
            'visibility'=>$addon_visibility,
            'position'=>$addon_position,
        ];

        $response = DB::table('addons')->where('id', '=', $addon_id)->update([
            'name' => $addon_name,
            'sku' => $addon_sku,
            'actual_price' => $addon_actual_price,
            'discount_price' => $addon_discount_price,
            'url_key' => $addon_url,
            'description'=>$addon_description,
            'visibility'=>$addon_visibility,
            'position'=>$addon_position,
        ]);

        if( $request->file('thumbnail_image') ) {
            $path = 'addon_thumbnails';
            $imageName = time().'.'.$request->thumbnail_image->getClientOriginalExtension();
            $request->thumbnail_image->move(public_path($path), $imageName);
            DB::table('addon_images')->where('addons_id', '=', $addon_id)->where('type', '=', 'THUMBNAIL')->delete();
            DB::table('addon_images')->insert(['path' => $path.'/'.$imageName, 'type' => 'THUMBNAIL', 'addons_id' => $addon_id ]);
        }

        if( $request->file('addon_image') ) {
            $data = [];
            $incr = 0;
            $path = 'addon_images';
            foreach( $request->file('addon_image') as $img ) {
                ++$incr;
                $imageName = time().$incr.'.'.$img->getClientOriginalExtension();
                $img->move(public_path($path), $imageName);
                $data[] = [
                    'path' => $path.'/'.$imageName, 
                    'addons_id' => $addon_id 
                ];
            }
            DB::table('addon_images')->insert($data);
        }

        if( true ) {
            echo json_encode(['message'=> 'Product updated Successfully', 'status' => TRUE]);
        } else {
            echo json_encode(['message'=> 'Something went wrong', 'status' => FALSE]);
        }
        
        return;
    }

    public function addons_data(Request $request) {
        $addon_id = $request->post('id');
        $addon = DB::table('addons')->select('*')->where('id', '=', $addon_id)->first();
        $addon_thumbnail = DB::table('addon_images')->select('path', 'id')
            ->where('addons_id', '=', $addon_id)
            ->where('type', '=', 'THUMBNAIL')->first();
        $addon_images = DB::table('addon_images')->select('path', 'id')
            ->where('addons_id', '=', $addon_id)
            ->where('type', '=', 'IMAGE')->get();

        if($addon !== null) {
            echo json_encode([
                    'message'=> 'Product fetched Successfully', 
                    'addon' => $addon,
                    'thumbnail' => $addon_thumbnail,
                    'images' => $addon_images,
                    'status' => TRUE
                ]);
        } else {
            echo json_encode(['message'=> 'Wrong addon', 'status' => FALSE]);
        }

        return;
    }
    public function addons_destroy(Request $request) {
        $addon_id = $request->post('id');
        $response = DB::table('addons')->where('id', '=', $addon_id)->update([
            'deleted_at' => date('Y-m-d H:i:s')
        ]);        
        if ( $response ) {
            echo json_encode(['message' => 'Deleted Successfully', 'status' => TRUE]);
        } else {
            echo json_encode(['message' => 'Something went wrong', 'status' => FALSE]);
        }

        return;
    }    
      

    function addons_image_delete(Request $request) {
        $image_id = $request->post('img_id');
        DB::table('addon_images')->where('id', '=', $image_id)->delete();

        echo json_encode(['message'=> 'Image deleted', 'status' => TRUE]);
    }

}
