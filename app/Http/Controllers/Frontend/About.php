<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class About extends Controller
{
    function index()
    {
        $intolerence_data = DB::select("select `preferenceoptions`.`name` as name, `preferenceoptions`.`id` as val   from `preferenceoptions` where `preferenceoptions`.`type` = :type", ['type' => 'intolerence']);
        $diet_data = DB::select("select `preferenceoptions`.`name` as name, `preferenceoptions`.`id` as val  from `preferenceoptions` where `preferenceoptions`.`type` = :type", ['type' => 'Diet']);
        return view('frontend.about.dashboard', ['intolerence_data' => $intolerence_data, 'diet_data' => $diet_data]);
    }

    function order_detail($order_id)
    {

        $intolerence_data = DB::select("select `preferenceoptions`.`name` as name, `preferenceoptions`.`id` as val   from `preferenceoptions` where `preferenceoptions`.`type` = :type", ['type' => 'intolerence']);
        $diet_data = DB::select("select `preferenceoptions`.`name` as name, `preferenceoptions`.`id` as val  from `preferenceoptions` where `preferenceoptions`.`type` = :type", ['type' => 'Diet']);
        return view('frontend.about.dashboard', ['order_id' => $order_id, 'intolerence_data' => $intolerence_data, 'diet_data' => $diet_data]);
    }

    function orderreview($key)
    {
        $key = trim($key);
        if (empty($key)) {
            return redirect()->route('home_page');
        }
        $orderkey_data = DB::select("select `order`.`id` as order_id,`order`.`freshness` as freshness,`order`.`taste` as taste, `order`.`variety` as variety, `order`.`ordering_process` as ordering_process, `order`.`comment` as comment  from `order` where `order`.`key` = :key", ['key' => $key]);
        if (empty($orderkey_data)) {
            return redirect()->route('home_page');
        } else {
            return view('frontend.about.review', ['order_id' => $orderkey_data[0]->order_id, 'order_review_data' => $orderkey_data]);
            //            echo $order_id = $orderkey_data[0]->order_id;
            //            exit;
            //            $customer_data = DB::table('users')->select('name', 'email', 'phone_number','billing_country','billing_city','billing_zip','billing_address')->where('id', '=', $user_id)->first();
            //            $returnData = Array("message" => "Successfully fetched user data","data" => $customer_data);
            //            return response()->json(json_encode($returnData), 200);
        }
    }
}
