<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Cartkey;
use App\Cart;
use App\Cartitems;
use App\Order;
use App\Orderitems;
use App\Notifications;
use PDF;
use Codedge\Fpdf\Facades\Fpdf;

use Illuminate\Support\Facades\Mail;
use App\Mail\CancelEmail;

use App\Userkey;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{

    private static $API_ACCESS_KEY = 'AAAA0Ihrr6E:APA91bHDWmqOd5813O2xPryWcxkqrUxl9_j6KjwuRDgIg_Elmtl6zKhc4k3gq81vFJDEc3wlV0ClYJc8dFYiKXsmVgPvCESxUPXUD_nsTysb5T7GAS5c710Xtx3eVqhk6Aky6cEg96aE'; //'AIzaSyCCxJPno4WLLkGO6LM92weZ9ipY_jyGA5s';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    // Sends Push notification for Android users
    public function android($data, $reg_id)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';
        //            $message = array(
        //                'title' => $data['mtitle'],
        //                'message' => $data['mdesc'],
        //                'subtitle' => '',
        //                'tickerText' => '',
        //                'msgcnt' => 1,
        //                'vibrate' => 1
        //            );
        $message = array(
            'title' => $data['mtitle'],
            'body' => $data['mdesc'],
            'vibrate' => 1
            //                'sound' => "default",
            //                'icon' => 'ic_notification'
        );

        $headers = array(
            'Authorization: key=' . self::$API_ACCESS_KEY,
            'Content-Type: application/json'
        );

        $fields = array(
            'registration_ids' => array($reg_id),
            'notification' => $message,
            "android" => [
                "notification" => [
                    "sound"  => "default",
                    "icon"  => "ic_notification",
                ]
            ],
            'data' => $message,
        );

        return $this->useCurl($url, $headers, json_encode($fields));
    }

    // Curl 
    private function useCurl($url, $headers, $fields = null)
    {
        // Open connection
        $ch = curl_init();
        if ($url) {
            // Set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            // Disabling SSL Certificate support temporarly
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            if ($fields) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            }

            // Execute post
            $result = curl_exec($ch);
            if ($result === FALSE) {
                die('Curl failed: ' . curl_error($ch));
            }

            // Close connection
            curl_close($ch);
            //                echo "<pre>";
            //                    print_r($result);
            //                    exit;
            return $result;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    function generateCode($limit)
    {
        $code = '';
        for ($i = 0; $i < $limit; $i++) {
            $code .= mt_rand(0, 9);
        }
        return $code;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
    }

    /**
     * Order history.
     *
     * @return \Illuminate\Http\Response
     */
    public function history(Request $request)
    {
        if ($request->session()->get('user_id')) {
            $user_id = $request->session()->get('user_id');
        } else {
            $returnData = array("message" => "User is not logged in");
            echo json_encode($returnData);
            return;
        }

        $orderid = $request->post('orderid');
        if (empty($orderid)) {
            $result = DB::select('select `order`.`id` as orderid, '
                . '`order_items`.`id` as orderitemid, '
                . '`order`.`item_count` as item_count_total,'
                . '`order`.`created_at` as created_at,'
                . '`order`.`order_at` as order_at,'
                . '`order`.flight_number as flight_no,'
                . '`order`.gate as gate,'
                . '`order`.pickup_reached_at as pickup_reached_at,'
                . '`order`.pickup_point as boarding_point,'
                . '`order`.`status` as order_status,'
                . ' `order`.`grant_total` as grant_total,'
                . ' `order`.`sub_total` as sub_total,'
                . ' `order`.`tax_total` as tax_total,'
                . ' `order`.`qr_code` as qr_code,'
                . ' order_items.item_count as item_count,'
                . ' order_items.products_id as products_id,'
                . ' order_items.grant_total as grant_item_price,'
                . ' order_items.sub_total as sub_item_price,'
                . ' order_items.tax_total as tax_item_price,'
                . ' products.name as productname,'
                . ' products.actual_price as actual_price,'
                . ' products.description as productdescription,'
                . ' product_images.path as productthumbnail,'
                . ' ado.name as addon_name, ado.actual_price as addon_price, aoi.item_count as addon_item_count'
                . ' from `order` join order_items on `order`.`id` = order_items.order_id'
                . ' join products on `products`.`id` = order_items.products_id '
                . ' left join product_images on `products`.`id` = product_images.products_id and product_images.type = "THUMBNAIL"'
                . ' left join addons_order_items as aoi on order_items.id = aoi.order_items_id'
                .  ' left join addons as ado on aoi.addons_id = ado.id'
                . ' where `order`.`users_id` = :userid '
                . '  order by `order`.`id` desc ', ['userid' => $user_id]);
        } else {
            $result = DB::select('select `order`.`id` as orderid, '
                . '`order_items`.`id` as orderitemid, '
                . '`order`.`item_count` as item_count_total,'
                . '`order`.`created_at` as created_at,'
                . '`order`.`order_at` as order_at,'
                . '`order`.flight_number as flight_no,'
                . '`order`.gate as gate,'
                . '`order`.pickup_reached_at as pickup_reached_at,'
                . '`order`.pickup_point as boarding_point,'
                . '`order`.`status` as order_status,'
                . ' `order`.`grant_total` as grant_total,'
                . ' `order`.`sub_total` as sub_total,'
                . ' `order`.`tax_total` as tax_total,'
                . ' `order`.`qr_code` as qr_code,'
                . ' order_items.item_count as item_count,'
                . ' order_items.products_id as products_id,'
                . ' order_items.grant_total as grant_item_price,'
                . ' order_items.sub_total as sub_item_price,'
                . ' order_items.tax_total as tax_item_price,'
                . ' products.name as productname,'
                . ' products.actual_price as actual_price,'
                . ' products.description as productdescription,'
                . ' product_images.path as productthumbnail,'
                . ' ado.name as addon_name, ado.actual_price as addon_price, aoi.item_count as addon_item_count'
                . ' from `order` join order_items on `order`.`id` = order_items.order_id'
                . ' join products on `products`.`id` = order_items.products_id '
                . ' left join product_images on `products`.`id` = product_images.products_id and product_images.type = "THUMBNAIL"'
                . ' left join addons_order_items as aoi on order_items.id = aoi.order_items_id'
                .  ' left join addons as ado on aoi.addons_id = ado.id'
                . ' where `order`.`users_id` = :userid and `order`.`id` = :orderid '
                . '  order by `order`.`id` desc', ['userid' => $user_id, 'orderid' => $orderid]);
        }
        $modifiedResult = [];
        $orderItemsIDArray = [];
        if (!empty($result)) {
            $modifiedResult = array();
            foreach ($result as $key => $value) {
                if (!isset($modifiedResult[$value->orderid])) {
                    $order_date = date("d-M-Y H:i", strtotime($value->created_at));
                    $invEncode = str_pad($value->orderid, 5, '0', STR_PAD_LEFT);
                    $invEncode = "CON" . $invEncode;
                    $modifiedResult[$value->orderid] = array(
                        'pickup_reached_at' => $value->pickup_reached_at,
                        'order_id' => $value->orderid,
                        'order_encoded' => $invEncode,
                        'created_at' => $order_date,
                        'flight_no' => $value->flight_no,
                        'boarding_point' => $value->boarding_point,
                        'gate' => $value->gate,
                        'order_status' => str_replace("_", " ", $value->order_status),
                        'item_count_total' => $value->item_count_total,
                        'grant_total' => decimal_format_custom($value->grant_total),
                        'sub_total' => decimal_format_custom($value->sub_total),
                        'tax_total' => decimal_format_custom($value->tax_total),
                        'order_at' => $value->order_at != NULL ? date('d M Y H:i', strtotime($value->order_at)) : 'N/A',
                        'qr_code' => url($value->qr_code)
                    );
                }
                if (!isset($modifiedResult[$value->orderid]['items'][$value->orderitemid])) {
                    $modifiedResult[$value->orderid]['items'][$value->orderitemid] = array(
                        'item_count' => $value->item_count,
                        'grant_item_price' => decimal_format_custom($value->grant_item_price),
                        'actual_price' => decimal_format_custom($value->actual_price),
                        'sub_item_price' => decimal_format_custom(($value->sub_item_price * $value->item_count)),
                        'tax_item_price' => decimal_format_custom($value->tax_item_price),
                        'products_id' => $value->products_id,
                        'productname' => $value->productname,
                        'productthumbnail' => empty($value->productthumbnail) ? $value->productthumbnail : url($value->productthumbnail),
                        'productdescription' => $value->productdescription,
                    );
                    if (!empty($value->addon_name)) {
                        $modifiedResult[$value->orderid]['items'][$value->orderitemid]['addons'][] = array(
                            'addon_name' => $value->addon_name,
                            'addon_price' => $value->addon_price,
                            'addon_item_count' => $value->addon_item_count
                        );
                    } else {
                        $modifiedResult[$value->orderid]['items'][$value->orderitemid]['addons'] = array();
                    }
                } else {
                    if (!empty($value->addon_name)) {
                        $modifiedResult[$value->orderid]['items'][$value->orderitemid]['addons'][] = array(
                            'addon_name' => $value->addon_name,
                            'addon_price' => $value->addon_price,
                            'addon_item_count' => $value->addon_item_count
                        );
                    } else {
                        $modifiedResult[$value->orderid]['items'][$value->orderitemid]['addons'] = array();
                    }
                }
            }
        }

        ksort($modifiedResult);
        $returnData = array("message" => "Order List", "data" => $modifiedResult);
        echo json_encode($returnData);
    }

    public function pdfCustomer(Request $request)
    {
        $orderID = $request->input('order_id');
        $userToken = $request->input('user_token');
        $validator = Validator::make($request->all(), [
            'order_id' => 'required|exists:order,id',
            'user_token' => 'required|exists:userkey,key',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'failed', 'message' => $validator->errors()], 401);
        }
        $userIDObj = Userkey::firstWhere('key', $userToken);
        $userID = $userIDObj->users_id;
        $this->pdfreport($userID, $orderID);
    }

    public function pdfReportWeb(Request $request)
    {
        if ($request->session()->get('user_id')) {
            $user_id = $request->session()->get('user_id');
        } else {
            $returnData = array("message" => "User is not logged in");
            echo json_encode($returnData);
            return;
        }
        $orderid = $request->input('orderid');
        $this->pdfreport($user_id, $orderid);
    }

    /**
     * Order report pdf.
     *
     * @return \Illuminate\Http\Response
     */
    private function pdfreport($user_id, $orderid)
    {
        $result = DB::select('select `order`.`id` as orderid, '
            . '`order_items`.`id` as orderitemid, '
            . '`order`.`item_count` as item_count_total,'
            . '`order`.`created_at` as created_at,'
            . '`order`.`order_at` as order_at,'
            . '`order`.flight_number as flight_no,'
            . '`order`.gate as gate,'
            . '`order`.pickup_reached_at as pickup_reached_at,'
            . '`order`.pickup_point as boarding_point,'
            . '`order`.`status` as order_status,'
            . ' `order`.`grant_total` as grant_total,'
            . ' `order`.`sub_total` as sub_total,'
            . ' `order`.`tax_total` as tax_total,'
            . ' `order`.`qr_code` as qr_code,'
            . ' order_items.item_count as item_count,'
            . ' order_items.products_id as products_id,'
            . ' order_items.grant_total as grant_item_price,'
            . ' order_items.sub_total as sub_item_price,'
            . ' order_items.tax_total as tax_item_price,'
            . ' products.name as productname,'
            . ' products.actual_price as actual_price,'
            . ' products.description as productdescription,'
            . ' product_images.path as productthumbnail,'
            . ' ado.name as addon_name, ado.actual_price as addon_price, aoi.item_count as addon_item_count'
            . ' from `order` join order_items on `order`.`id` = order_items.order_id'
            . ' join products on `products`.`id` = order_items.products_id '
            . ' left join product_images on `products`.`id` = product_images.products_id and product_images.type = "THUMBNAIL"'
            . ' left join addons_order_items as aoi on order_items.id = aoi.order_items_id'
            .  ' left join addons as ado on aoi.addons_id = ado.id'
            . ' where `order`.`users_id` = :userid and `order`.`id` = :orderid '
            . '  order by `order`.`id` desc', ['userid' => $user_id, 'orderid' => $orderid]);
        $modifiedResult = array();
        if (!empty($result)) {
            $i = 0;
            foreach ($result as $key => $value) {
                if ($i == 0) {
                    $modifiedResult = array(
                        'order_id' => $value->orderid,
                        'created_at' => $value->created_at,
                        'order_at' => $value->order_at,
                        'flight_no' => $value->flight_no,
                        'qr_code' => $value->qr_code,
                        'boarding_point' => $value->boarding_point,
                        'order_status' => str_replace("_", " ", $value->order_status),
                        'item_count_total' => $value->item_count_total,
                        'grant_total' => decimal_format_custom($value->grant_total),
                        'sub_total' => decimal_format_custom($value->sub_total),
                        'tax_total' => decimal_format_custom($value->tax_total)
                    );
                }

                if (!isset($modifiedResult['items'][$value->orderitemid])) {
                    $modifiedResult['items'][$value->orderitemid] = array(
                        'item_count' => $value->item_count,
                        'grant_item_price' => decimal_format_custom($value->grant_item_price),
                        'actual_price' => decimal_format_custom($value->actual_price),
                        'sub_item_price' => decimal_format_custom(($value->sub_item_price * $value->item_count)),
                        'tax_item_price' => decimal_format_custom($value->tax_item_price),
                        'products_id' => $value->products_id,
                        'productname' => $value->productname,
                        'productthumbnail' => empty($value->productthumbnail) ? $value->productthumbnail : url($value->productthumbnail),
                        'productdescription' => $value->productdescription,
                    );
                    if (!empty($value->addon_name)) {
                        $modifiedResult['items'][$value->orderitemid]['addons'][] = array(
                            'addon_name' => $value->addon_name,
                            'addon_price' => $value->addon_price,
                            'addon_item_count' => $value->addon_item_count
                        );
                    } else {
                        $modifiedResult['items'][$value->orderitemid]['addons'] = array();
                    }
                } else {
                    if (!empty($value->addon_name)) {
                        $modifiedResult['items'][$value->orderitemid]['addons'][] = array(
                            'addon_name' => $value->addon_name,
                            'addon_price' => $value->addon_price,
                            'addon_item_count' => $value->addon_item_count
                        );
                    } else {
                        $modifiedResult['items'][$value->orderitemid]['addons'] = array();
                    }
                }
                $i++;
            }
        }

        $content = '';
        $addon_total = "0.00";
        $product_total = "0.00";
        if (!empty($modifiedResult)) {
            foreach ($modifiedResult['items'] as $key => $value) {
                $grant_item_price = $value["actual_price"] * $value["item_count"];
                $product_total += $grant_item_price; 
                $content .= '<tr>
                        <td style="padding: 10px;; color: #4d4d4d; text-align: right;">
                        ' . $value["productname"] . '
                        </td>
                        <td style="padding: 10px;; color: #4d4d4d; text-align: right;">
                        AED ' . $value["actual_price"] . '
                        </td>
                        <td style="padding: 10px;; color: #4d4d4d; text-align: right;">
                        ' . $value["item_count"] . '
                        </td>
                        <td style="padding: 10px;; color: #4d4d4d; text-align: right;">
                        AED ' . $grant_item_price . '
                        </td>
                </tr>';
                if (count($value['addons']) > 0) {
                    $content .= "<tr>
                            <td colspan='4' style='padding: 10px;; color: #4d4d4d; text-align: left;'>Addons</td>
                        </tr>";
                    foreach ($value['addons'] as $addons) {
                        if (!empty($addons['addon_item_count'])) {
                            $addonsName = !empty($addons['addon_name']) ? $addons['addon_name'] : 'Nil';
                            $addonsPrice = !empty($addons['addon_price']) ? 'AED ' . $addons['addon_price'] : 0;
                            $addonsItemcount = !empty($addons['addon_item_count']) ? $addons['addon_item_count'] : 0;
                            $addon_total += ($addons['addon_item_count'] * $addons['addon_price']);
                            $addonsTotal = 'AED ' . $addons['addon_price'] *  $addons['addon_item_count'];
                            $content .= "<tr>
                                    <td style='padding: 10px;; color: #4d4d4d; text-align: right;'>
                                        " . $addonsName . "
                                    </td>
                                    <td style='padding: 10px;; color: #4d4d4d; text-align: right;'>
                                        " . $addonsPrice . "
                                    </td>
                                    <td style='padding: 10px;; color: #4d4d4d; text-align: right;'>
                                        " . $addonsItemcount . "
                                    </td>
                                    <td style='padding: 10px;; color: #4d4d4d; text-align: right;'>
                                        " . $addonsTotal . "
                                    </td>
                            </tr>";
                        }
                    }
                }
            }
        }
        
        $grant_total = $product_total + $addon_total;

        $user_data = DB::table('users')->select('phone_number', 'email', 'name', 'billing_address')->where('id', '=', $user_id)->first();
        $invID = str_pad($orderid, 5, '0', STR_PAD_LEFT);
        $invID = "CON" . $invID;
        $order_at = $modifiedResult['order_at'] ?? '';
        if (!empty($order_at)) {
            $date = date_create($order_at);
            $order_at = date_format($date, "d-M-Y");
        }
        $modifiedResult['tax_total'] = $modifiedResult['grant_total'] * 0.05;
        $modifiedResult['sub_total'] = $modifiedResult['grant_total'] - $modifiedResult['tax_total'];
        $data = [
            'title' => 'Welcome to carryon',
            'invID' => $invID,
            'customername' => $user_data->name,
            'content' => $content,
            'total' => $grant_total,
            'sub_total' => $modifiedResult['sub_total'],
            'tax_total' => $modifiedResult['tax_total'],
            'qr_code' => $modifiedResult['qr_code'],
            'order_at' => $order_at,
            'billing_address' => $user_data->billing_address
        ];
        $pdf = PDF::loadView('pdf.order', $data);

        // return $pdf->download('carryon' . time() . '.pdf');
        $folder_structure = date('Y') . "/" . date('m') . "/" . date('d');
        $target_dir = "./public/upload/" . $folder_structure;
        //$target_dir = "./upload/" . $folder_structure;
        $target_dir_path = "./public/upload/" . $folder_structure;
        if (!file_exists($target_dir)) {
            mkdir($target_dir, 0777, true);
        }
        $file = $target_dir . '/order_' . $orderid . '.pdf';
        $filepath = $target_dir_path . '/order_' . $orderid . '.pdf';
        $pdf->save($file);
        $returnData = array("message" => "Order List", "filepath" => url($filepath));
        echo json_encode($returnData);
        exit;

        /*Fpdf::AddPage('P','A4');
            Fpdf::SetFont('Arial', 'B', 12);
            Fpdf::Cell(60, 8, "productname", 1, 0, 'C');
            Fpdf::Cell(35, 8, "Qty", 1, 0, 'C');
            Fpdf::Cell(40, 8, "Price", 1, 0, 'C');
            Fpdf::Cell(45, 8, "Total", 1, 0, 'C');
            Fpdf::Ln();
            if (!empty($modifiedResult)) {
                foreach ($modifiedResult as $mkey => $mvalue) {
                    foreach ($mvalue['items'] as $key => $value) {
                        Fpdf::Cell(60, 8, $value['productname'], 1, 0, 'LR');
                        Fpdf::Cell(35, 8, $value['item_count'], 1,0,'C');
                        Fpdf::Cell(40, 8, $value['sub_item_price'], 1,0,'C');
                        Fpdf::Cell(45, 8, $value['grant_item_price'], 1,0,'C');
                        Fpdf::Ln();                    
                    }
                    Fpdf::Cell(100, 15, "", 0, 0, 'L');
                    Fpdf::Cell(50, 8, "Total Item Count", 0, 0, 'L');
                    Fpdf::Cell(50, 8, $mvalue['item_count_total'], 0, 1, 'L');

                    Fpdf::Cell(100, 8, "", 0, 0, 'L');
                    Fpdf::Cell(50, 8, "Sub Total", 0, 0, 'L');
                    Fpdf::Cell(50, 8, $mvalue['sub_total'], 0, 1, 'L');

                    Fpdf::Cell(100, 8, "", 0, 0, 'L');
                    Fpdf::Cell(50, 8, "Grant total", 0, 0, 'L');
                    Fpdf::Cell(50, 8, $mvalue['grant_total'], 0, 1, 'L');
                }
            }
            $folder_structure = date('Y')."/".date('m')."/".date('d');
            $target_dir = "./upload/".$folder_structure;
            if (!file_exists($target_dir)) {
                mkdir($target_dir, 0777, true);
            }
            $file = $target_dir.'/order_'.$orderid.'.pdf';
            Fpdf::Output($file, 'F');
            $returnData = Array("message" => "Order List","filepath" => url($file));
            echo json_encode($returnData);
            exit;  
            // Fpdf::Output();
            exit;  */
    }

    public function cancel(Request $request)
    {
        if (!$request->session()->get('user_id')) {
            $returnData = array("message" => "User is not logged in", 'status' => FALSE);
            echo json_encode($returnData);
            return;
        }
        $order_id = $request->post('id');
        $order_details = DB::table('order')->select('order_at', 'pickup_point')->where('id', '=', $order_id)->first();
        if (!$order_details) {
            echo json_encode([
                'status' => FALSE,
                'message' => 'Order not found!'
            ]);
            return;
        }
        $cancellable_time = DB::table('settings')->select('value')->where('key', '=', 'ORDER_CANCELLABLE_TIME')->first();
        $order_at = $order_details->order_at;
        date_default_timezone_set('Asia/Dubai');
        $now = date('Y-m-d H:i:s');
        $hrs = explode(':', $cancellable_time->value)[0];
        $mnts = explode(':', $cancellable_time->value)[1] ?? 0;
        if (strtotime(date('Y-m-d H:i:s', strtotime($order_at) - $hrs * 60 * 60 - $mnts * 60)) < strtotime($now)) {
            echo json_encode([
                'status' => FALSE,
                'message' => 'Order can be cancelled only before ' . $cancellable_time->value . ' Hrs of departure.'
            ]);
            return;
        }
        $reason = $request->post('reason') == '' ? '' : $request->post('reason');
        DB::table('order')->where('id', '=', $order_id)->update([
            'status' => 'CANCELLED',
            'cancel_reason' => $reason
        ]);

        $default_grabbit_location = default_grabbit_location();
        $notification_data = DB::select("select id as user_id, fcm_token  from `users` where `users`.`pickup_point` = :pickup_point or `users`.`pickup_point`= :other_pickup_point", ['pickup_point' => $default_grabbit_location['location'], 'other_pickup_point' => $order_details->pickup_point]);
        $invID = str_pad($order_id, 5, '0', STR_PAD_LEFT);
        $invID = "CON" . $invID;
        if (!empty($notification_data)) {
            foreach ($notification_data as $key => $value) {
                $title = 'Notification';
                $message = 'Order Id ' . $invID . ' cancelled';
                $this->notification_add($value->user_id, $message, $title, $value->fcm_token, $order_id);
            }
        }

        $subject_data = DB::select("select template from sms_template where `type` = :type", ['type' => 'EMAIL_SUBJECT_CANCELMAIL']);
        $subject = $subject_data[0]->template;
        $invID = str_pad($order_id, 5, '0', STR_PAD_LEFT);
        $invID = "CON" . $invID;
        $subject = str_replace("{{invID}}", $invID, $subject);
        $data = [
            //'subject' => "Order $invID has been cancelled!",
            'subject' => $subject,
            'order_id' => $invID,
            'url_link' => route('admin_orders')
        ];

        $allAdmins = DB::table('users')->select('email')
            ->join('roles', 'roles.id', '=', 'users.role_id')
            ->where('roles.name', '=', 'admin')
            ->get();
        foreach ($allAdmins as $eachAdmin) {
            Mail::to($eachAdmin->email)->send(new CancelEmail($data));
        }



        echo json_encode([
            'status' => TRUE,
            'message' => 'order cancelled successfully'
        ]);
    }

    function notification_add($user_id, $message, $title, $fcm_token, $order_id)
    {
        $notifications = new Notifications;
        $notifications->users_id = $user_id;
        $notifications->order_id = $order_id;
        $notifications->message = $message;
        $notifications->created_at = date("Y-m-d H:i:s");
        $notifications->updated_at = date("Y-m-d H:i:s");
        $notifications->save();
        // Message payload
        $msg_payload = array(
            'mtitle' => $title,
            'mdesc' => $message,
        );

        // For Android
        $reg_id = $fcm_token;

        $result = $this->android($msg_payload, $reg_id);
    }
}
