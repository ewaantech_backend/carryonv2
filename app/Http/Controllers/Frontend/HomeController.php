<?php

namespace App\Http\Controllers\Frontend;

use DateTime;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Support\Facades\Mail;
use App\Mail\TestEmail;
use App\UserPreferences;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $home_sliders = DB::select('select id, title, link, link_text, description,image_path,cover_image_path from cms_slider where visibility = "YES" and deleted_at IS NULL and short_code IS NULL ORDER BY position ASC');
        $home_page_content = DB::select('select html_content from cms where visibility = "YES" and short_code = "home_page_content" and deleted_at IS NULL');
        $home_page_content_title = DB::select('select html_content from cms where visibility = "YES" and short_code = "home_page_content_title" and deleted_at IS NULL');
        $categories = DB::select('select id, name, description from category where visibility = "YES" ORDER BY position ASC');
        return view('frontend.home.index', array('categories' => $categories, 'home_sliders' => $home_sliders, 'home_page_content' => $home_page_content, 'home_page_content_title' => $home_page_content_title));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cms($url_key)
    {
        $cms = DB::select('select html_content from cms where url_key = :urlkey ', ['urlkey' => $url_key]);
        if (empty($cms)) {
            return redirect()->route('home_page');
        } else {
            return view('frontend.home.cms', array('content' => $cms[0]->html_content));
            exit;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
        public function show(Request $request)
        {
            $request->session()->put('flight_number', '');
            $request->session()->put('flight_date', '');
            $request->session()->put('flight_id', '');
            $request->session()->put('flight_duration', '');
            $request->session()->put('flight_terminal', '');
            $request->session()->save();
    
            $flight = $request->get('fl_no');
            $date = $request->get('date');
            if( ! $flight || ! $date) {
                echo json_encode( [ 'status' => FALSE, 'message' => 'Please provide date and flight number' ] );
                return;
            }
            $dateSplitted = explode('/', $date);
            $day = $dateSplitted[1];
            $month = $dateSplitted[0];
            $year = $dateSplitted[2];
            $flight_iata = substr($flight, 0, 2);
            $flight_code = substr($flight, 2, 4);
            $url = "https://api.flightstats.com/flex/schedules/rest/v1/json/flight/$flight_iata/$flight_code/departing/$year/$month/$day?appId=8a5a55e4&appKey=b3e7538d084234f6f83779bdfbd671ed";
    
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
            $results = curl_exec($ch);
    
            $results = json_decode($results, true);
            // Error from API side.
            if( isset( $results['error']['errorMessage'] ) ) {
                // echo json_encode( [ 'status' => FALSE, 'message' => explode('.', $results['error']['errorMessage'])[0] ]);
                echo json_encode( [ 'status' => FALSE, 'message' => 'Flight details not found']);
                return;
            } 
            else if( isset($results['scheduledFlights']) && $results['scheduledFlights'] ) {
                // Check departure from DXB.
                if( ! isset( $results['scheduledFlights'][0]['departureAirportFsCode'] ) || $results['scheduledFlights'][0]['departureAirportFsCode'] != 'DXB'  ) {
                    
                    echo json_encode( [ 'status' => FALSE, 'message' => 'We’re currently operating only from DXB, Terminal 3. Stay tuned for updates!' ] );
                    return;
                }
                // Check departure from Terminal 3.
                if( ! isset( $results['scheduledFlights'][0]['departureTerminal'] ) || $results['scheduledFlights'][0]['departureTerminal'] != 3) {
                    echo json_encode( [ 'status' => FALSE, 'message' => 'We’re currently operating only from DXB, Terminal 3. Stay tuned for updates!' ] );
                    return;
                }
                $diffOfRegions = 0;
                $diffVariantTimeRegionDiff = 0;
                /*
                 Below code to find journey duration.
                 Local current time of arrvl - local current time of departure + diff of arrvl and dep.
                 */
                if( isset( $results['appendix']['airports'] ) ) {
                    $dataAirports = $results['appendix']['airports'];
                    if( isset($dataAirports[1]['localTime']) && isset($dataAirports[0]['localTime']) ) {
                        $diffOfRegions = strtotime($dataAirports[0]['localTime']) - strtotime($dataAirports[1]['localTime']);
                    }
                    $diffVariantTimeRegionDiff = strtotime($results['scheduledFlights'][0]['arrivalTime']) - strtotime($results['scheduledFlights'][0]['departureTime']);
                }
                $actualDiff = $diffVariantTimeRegionDiff + $diffOfRegions;
                $duration = $this->formatSeconds($actualDiff);
                // Duration find code ends.
                $departureTime = date('H:i', strtotime($results['scheduledFlights'][0]['departureTime']) );
                $slot = DB::table('flight_slots')->select('last_order')
                        ->where('time_from', '<=', $departureTime)
                        ->where('time_to', '>=', $departureTime)
                        ->first();
                date_default_timezone_set('Asia/Dubai');
                $localDXB_time = date('Y-m-d H:i');
                // Find date of departure
                $dateOfDeparture = date('Y-m-d', strtotime($results['scheduledFlights'][0]['departureTime']) );
                // Find possible last order time. (Removed H:i:s of departure and appended last order time from slot to dep time).
                if(strtotime($slot->last_order) >= strtotime('18:00:00')) {
                    // Last possible order date is prevoius day for order time upto 05:59 AM.(its last_order time wll b [18:00:00, 23:00:00])
                    $dateOfDeparture = date('Y-m-d', strtotime('-1 day', strtotime($dateOfDeparture)));
                }
                $possibleMaxOrderTime = date('Y-m-d H:i', strtotime( $dateOfDeparture . ' ' . $slot->last_order ) );
                // Check order possibility with time slot.
                if(strtotime($localDXB_time) > strtotime($possibleMaxOrderTime)) {
                    $lt_time = explode(':', str_replace(':00', '', $slot->last_order));
                    $lt_time = $lt_time[0] > 12 ? ($lt_time[0] -12 . ' PM') : ($lt_time[0] . ' AM');
                    $departed = FALSE;
                    if( strtotime($localDXB_time) > strtotime($results['scheduledFlights'][0]['departureTime']) ) {
                        $departed = TRUE;
                    }
                    echo json_encode( [ 
                        'status' => FALSE, 
                        'message' => 'time elapsed',
                        'departed' => $departed,
                        'last_order' => '<i>'.date('d M Y', strtotime($dateOfDeparture)). ' ' .str_replace(':00', '', $lt_time).'</i>',
                        'test' => $slot->last_order
                    ] );
                    return;
                }
                // Success
                $request->session()->put('flight_number', $flight);
                $request->session()->put('flight_date', $results['scheduledFlights'][0]['departureTime']);
                $request->session()->put('flight_id', $flight);
                $request->session()->put('flight_duration', $duration);
                $request->session()->put('flight_terminal', 3);
                $request->session()->save();
                if($results['appendix']['airports'][0]['iata'] == 'DXB') {
                    $destin_city = $results['appendix']['airports'][1]['city'] ?? $results['scheduledFlights'][1]['arrivalAirportFsCode'];
                } else {
                    $destin_city = $results['appendix']['airports'][0]['city'] ?? $results['scheduledFlights'][0]['arrivalAirportFsCode'];
                }
                echo json_encode( [ 
                        'status' => TRUE, 
                        'message' => 'Flight data fetched',
                        'flight' => $flight,
                        'departure' => date('h:i A', strtotime($results['scheduledFlights'][0]['departureTime']) ),
                        'duration' => $duration,
                        'destination' => $destin_city,
                    ] );
                    return;
            } else {
                echo json_encode( [ 
                    'status' => FALSE, 
                    'message' => 'Flight details not found'
                ] );
                return;
            }
            echo json_encode( $results );
    
        }

//    public function show(Request $request)
//    {
//        $request->session()->put('flight_number', '');
//        $request->session()->put('flight_date', '');
//        $request->session()->put('flight_id', '');
//        $request->session()->put('flight_duration', '');
//        $request->session()->put('flight_terminal', '');
//        $request->session()->save();
//
//        $flight = $request->get('fl_no');
//        //$date = $request->get('date');
//        $date = '03/24/2020';
//        if (!$flight || !$date) {
//            echo json_encode(['status' => FALSE, 'message' => 'Please provide date and flight number']);
//            return;
//        }
//        $date = date("Y-m-d H:i:s");
//        $onedayafter = date("Y-m-d");
//        // $onedayafter = date("Y-m-d", strtotime($date. ' +24 hours'));
//        //$results['scheduledFlights'][0]['departureTime'] = '2020-03-26T07:45:00.000';
//        //$departureTime = $onedayafter.'T07:45:00.000';
//        $departureTime = $onedayafter . 'T23:00:00.000';
//        $duration = $this->formatSeconds(7 * 60 * 60);
//
//        // Success
//        $request->session()->put('flight_number', $flight);
//        $request->session()->put('flight_date', $departureTime);
//        $request->session()->put('flight_id', $flight);
//        $request->session()->put('flight_duration', $duration);
//        $request->session()->put('flight_terminal', 3);
//        $request->session()->save();
//
//        $destin_city = 'London';
//        echo json_encode([
//            'status' => TRUE,
//            'message' => 'Flight data fetched',
//            'flight' => $flight,
//            'departure' => date('h:i A', strtotime($departureTime)),
//            'duration' => $duration,
//            'destination' => $destin_city,
//        ]);
//        return;
//    }

    function formatSeconds($seconds)
    {
        // if($milliseconds > 0) {
        //     $seconds = $milliseconds / 1000;
        // }
        $hours = 0;
        $milliseconds = str_replace("0.", '', $seconds - floor($seconds));

        if ($seconds > 3600) {
            $hours = floor($seconds / 3600);
        }
        $seconds = $seconds % 3600;


        return str_pad($hours, 2, '0', STR_PAD_LEFT)
            . gmdate(':i', $seconds);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function timecheck(Request $request)
    {
        $status = array();
        $flight = $request->get('flight');
        $flight_date = $request->get('flight_date');

        $flightdetails = DB::select('select id,departure from flights 
                                    where  flight_no = "' . $flight . '"');
        $flight_time = $flightdetails[0]->departure;
        $flight_id = $flightdetails[0]->id;
        $todays_date_time = date('Y-m-d H:i');
        $flight_date_time = date("Y-m-d H:i", strtotime($flight_date . " " . $flight_time));
        $flight_date = date("Y-m-d H:i:s", strtotime($flight_date));

        $t1 = strtotime("$todays_date_time");
        $t2 = strtotime("$flight_date_time");
        $diff =  $t2 - $t1;
        $hours = $diff / (60 * 60);

        if ($hours <= 4) {
            $status['can_order'] = 0;
            $request->session()->put('flight_number', "");
            $request->session()->put('flight_date', "");
            $request->session()->put('flight_id', "");
            $request->session()->save();
        } else {
            $request->session()->put('flight_number', $flight);
            $request->session()->put('flight_date', $flight_date);
            $request->session()->put('flight_id', $flight_id);
            $request->session()->save();
            $status['can_order'] = 1;
        }
        if ($request->session()->has('user_id')) {
            $status['is_logged_in'] = 1;
        } else {
            $status['is_logged_in'] = 0;
        }
        $status['time'] = $hours;
        echo json_encode($status);
    }

    function sendMail($message, $email, $user, $rand)
    {
        try {
            $subject_data = DB::select("select template from sms_template where `type` = :type", ['type' => 'EMAIL_SUBJECT_WELCOMEMAIL']);
            $subject = $subject_data[0]->template;
            $data = [
                'from_name' => 'CARRYON',
                'from_email' => 'noreply@carryon.com',
                // 'subject' => 'Welcome to CarryOn!',
                'subject' => $subject,
                'message1' => $message,
                'message2' => '',
                'user' => $user,
                'rand' => $rand
            ];

            Mail::to($email)->send(new TestEmail($data));
        } catch (\Exception $e) {
            print_r($e->getMessage());
        }
    }

    public function registration(Request $request)
    {

        $status = array();
        $usersarr = array();

        $first_name = $request->get('first_name');
        $last_name = $request->get('last_name');
        $email = $request->get('email');
        //$code = $request->get('code');
        $phone = $request->get('phone');
        $country = $request->get('country');
        $timestamp = date('Y-m-d H:i:s');
        $rand = rand(1000, 9999);

        $usersarr = DB::select('select * from users 
                                        where  email = "' . $email . '"');
        $userrole = DB::select('select id as role_id from roles 
                                        where  name = "customer"');

        $flight_number = $request->session()->get('flight_number');
        /*$flights = DB::select('select departure from flights 
                                        where  flight_no = "'.$flight_number.'"');
        $request->session()->put('flight_time', $flights[0]->departure);*/

        $password = Hash::make($rand);

        if (count($usersarr) < 1) {
            $user = new User;
            $user->name = $first_name . ' ' . $last_name;
            $user->first_name = $first_name;
            $user->last_name = $last_name;
            $user->email = $email;
            $user->password = $password;
            $user->remember_token = $rand;
            //$user->phone_number = $code.'-'.$phone;
            $user->phone_number = $phone;
            $user->nationality = $country;
            $user->role_id = $userrole[0]->role_id;
            $user->type = 'CUSTOMER';
            $user->created_at = $timestamp;
            $user->updated_at = $timestamp;
            $user->status = 'ACTIVE';
            $saveuser = $user->save();

            $request->session()->put('user_id', $user->id);
            $request->session()->put('user_name', $user->name);
            $request->session()->put('user_email', $user->email);
            $request->session()->put('user_phone', $user->phone_number);
            $request->session()->put('user_nationality', $user->nationality);
            $request->session()->save();

            $mail_message = '<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Hi <b>' . $user->name . '</b>,</p>

                <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Welcome to <b>CarryOn</b>, we’re happy to have you around!</p>
                <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Your system generated password is <b>' . $rand . '</b></p>';
            $this->sendMail($mail_message, $user->email, $user->name, $rand);

            $status['error'] = 0;
            $status['message'] = "You have registered successfully, and email with password will be sent to your email address.";
        } else {
            $status['error'] = 1;
            $status['message'] = "Email address is already registered.";
        }


        echo json_encode($status);
    }
    /**
     * Add billing.
     *
     * @return \Illuminate\Http\Response
     */
    public function addbilling(Request $request)
    {
        $users_id = $request->session()->get('user_id');
        $billing_name = $request->post('billing_name');
        // $billing_address = $request->post('billing_address');
        $billing_city = $request->post('billing_city');
        $billing_zip = $request->post('billing_zip');
        $billing_country = $request->post('billing_country');

        $billing_street = $request->post('billing_street');
        $billing_street_no = $request->post('billing_street_number');
        $billing_apartment_no = $request->post('billing_apartment_number');

        DB::table('users')
            ->where('id', $users_id)
            ->update([
                'billing_name'       => $billing_name,
                'billing_street' => $billing_street,
                'billing_street_no' => $billing_street_no,
                'billing_apartment_no' => $billing_apartment_no,
                'billing_city' => $billing_city,
                'billing_zip' => $billing_zip,
                'billing_country' => $billing_country,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        $request->session()->put('billing_name', $billing_name);
        $request->session()->put('billing_street', $billing_street);
        $request->session()->put('billing_street_number', $billing_street_no);
        $request->session()->put('billing_apartment_number', $billing_apartment_no);
        $request->session()->put('billing_city', $billing_city);
        $request->session()->put('billing_zip', $billing_zip);
        $request->session()->put('billing_country', $billing_country);
        $request->session()->save();

        $returnData = array('message' => 'Successfully updated billing address', 'status' => TRUE, 'error' => 0);
        echo json_encode($returnData);
    }

    /**
     * User profile view.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile(Request $request)
    {
        if ($request->session()->get('user_id')) {
            $user_id = $request->session()->get('user_id');
        } else {
            $returnData = array("message" => "User is not logged in");
            echo json_encode($returnData);
            exit;
        }

        $customer_data = DB::table('users')->select('name', 'email', 'phone_number', 'billing_country', 'billing_city', 'billing_zip', 'billing_street', 'billing_street_no', 'billing_apartment_no', 'first_name', 'last_name')->where('id', '=', $user_id)->first();
        $returnData = array("message" => "Successfully fetched user data", "data" => $customer_data);
        echo json_encode($returnData);
        exit;
    }


    /**
     * Profie update.
     *
     * @return \Illuminate\Http\Response
     */
    public function profileupdate(Request $request)
    {
        $users_id = $request->session()->get('user_id');
        $first_name = trim($request->post('first_name'));
        $last_name = trim($request->post('last_name'));
        $name_arr = array($first_name,$last_name);
        $name = implode(" ",$name_arr);
        $email = $request->post('email');
        //$phoneprefix = $request->post('phoneprefix');
        $phone = $request->post('phone');
        $billing_city = $request->post('city');
        $billing_zip = $request->post('zip');
        $billing_country = $request->post('country');
        $billing_street = $request->post('billing_street');
        $billing_street_no = $request->post('billing_street_number');
        $billing_apartment_no = $request->post('billing_apartment_number');

        $dupe_email = DB::table('users')->select('email')
            ->where('email', '=', $email)
            ->where('id', '!=', $users_id)->first();
        if ($dupe_email) {
            $returnData = array('message' => 'Email already exists', 'status' => FALSE, 'error' => 1);
            echo json_encode($returnData);
            return;
        }

        DB::table('users')
            ->where('id', $users_id)
            ->update([
                'name' => $name,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'email' => $email,
                'phone_number' => $phone,
                'billing_city' => $billing_city,
                'billing_zip' => $billing_zip,
                'billing_country' => $billing_country,
                'billing_street' => $billing_street,
                'billing_street_no' => $billing_street_no,
                'billing_apartment_no' => $billing_apartment_no,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        $request->session()->put('name', $name);
        $request->session()->put('user_name', $name);
        $request->session()->put('billing_name', $name);
        $request->session()->put('email', $email);
        $request->session()->put('user_email', $email);
        $request->session()->put('phone_number', $phone);
        $request->session()->put('user_phone', $phone);
        $request->session()->put('billing_city', $billing_city);
        $request->session()->put('billing_zip', $billing_zip);
        $request->session()->put('billing_country', $billing_country);
        $request->session()->put('billing_street', $billing_street);
        $request->session()->put('billing_street_number', $billing_street_no);
        $request->session()->put('billing_apartment_number', $billing_apartment_no);
        $request->session()->save();

        $returnData = array('message' => 'successfully updated profile', 'status' => TRUE, 'error' => 0);
        echo json_encode($returnData);
    }


    /**
     * User preference view.
     *
     * @return \Illuminate\Http\Response
     */
    public function preference(Request $request)
    {
        if ($request->session()->get('user_id')) {
            $user_id = $request->session()->get('user_id');
        } else {
            $returnData = array("message" => "User is not logged in");
            echo json_encode($returnData);
            exit;
        }

        $preferences = DB::select("SELECT GROUP_CONCAT(preferenceoptions_id) AS preference FROM users_preferenceoptions WHERE users_id = :users_id AND type = 'Intolerence' ", [":users_id" => $user_id]);
        $diets = DB::select("SELECT GROUP_CONCAT(preferenceoptions_id) AS diet FROM users_preferenceoptions WHERE users_id = :users_id AND type = 'Diet' ", [":users_id" => $user_id]);

        $customer_data = DB::table('users')->select('pref_age_group', 'pref_gender', 'pref_country', 'pref_intolerances', 'pref_diet', 'pref_lifestyle', 'pref_travel_pattern', 'pref_travel_for')->where('id', '=', $user_id)->first();

        foreach ($preferences as $preference) {
            $customer_data->pref_intolerances = $preference->preference;
        }

        foreach ($diets as $diet) {
            $customer_data->pref_diet = $diet->diet;
        }


        $returnData = array("message" => "Successfully fetched user data", "data" => $customer_data);
        echo json_encode($returnData);
        exit;
    }


    /**
     * Preference update.
     *
     * @return \Illuminate\Http\Response
     */
    public function preferenceupdate(Request $request)
    {
        $users_id = $request->session()->get('user_id');
        $agegroup = $request->post('agegroup');
        $gender = $request->post('gender');
        $pref_country = $request->post('pref_country');
        $intolerances = $request->post('intolerances');
        $preferred_diets = $request->post('preferred_diet');
        $lifestyle = $request->post('lifestyle');
        $travel_pattern = $request->post('travel_pattern');
        $travelfor = $request->post('travelfor');

        DB::table('users')
            ->where('id', $users_id)
            ->update([
                'pref_age_group' => $agegroup,
                'pref_gender' => $gender,
                'pref_country' => $pref_country,
                'pref_lifestyle' => $lifestyle,
                'pref_travel_pattern' => $travel_pattern,
                'pref_travel_for' => $travelfor,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);

        if (count($intolerances) > 0) {
            DB::delete("DELETE FROM users_preferenceoptions WHERE users_id =:users_id AND type='Intolerence'", [':users_id' => $users_id]);
            foreach ($intolerances as $intolerance) {
                $userPreferences = new UserPreferences();
                $userPreferences->users_id = $users_id;
                $userPreferences->preferenceoptions_id = $intolerance;
                $userPreferences->type = 'Intolerence';
                $userPreferences->save();
            }
        }

        if (count($preferred_diets) > 0) {
            DB::delete("DELETE FROM users_preferenceoptions WHERE users_id =:users_id AND type='Diet'", [':users_id' => $users_id]);
            foreach ($preferred_diets as $preferred_diet) {
                $diet = new UserPreferences();
                $diet->users_id = $users_id;
                $diet->preferenceoptions_id = $preferred_diet;
                $diet->type = 'Diet';
                $diet->save();
            }
        }

        $returnData = array('message' => 'successfully updated profile', 'status' => TRUE, 'error' => 0);
        echo json_encode($returnData);
    }


    /**
     * User order review view.
     *
     * @return \Illuminate\Http\Response
     */
    public function orderreviewdata(Request $request)
    {
        $order_id = $request->post('orderid');
        $order_review_data = DB::table('order')->select('freshness', 'taste', 'variety', 'ordering_process', 'comment')->where('id', '=', $order_id)->first();
        $returnData = array("message" => "Successfully fetched user data", "data" => $order_review_data);
        echo json_encode($returnData);
        exit;
    }


    /**
     * Preference update.
     *
     * @return \Illuminate\Http\Response
     */
    public function orderreviewupdate(Request $request)
    {
        $order_id = $request->post('order_id');
        $freshness = $request->post('freshness');
        $taste = $request->post('taste');
        $variety = $request->post('variety');
        $ordering_process = $request->post('ordering_process');
        $comment = $request->post('comment');
        DB::table('order')
            ->where('id', $order_id)
            ->update([
                'freshness' => $freshness,
                'taste' => $taste,
                'variety' => $variety,
                'ordering_process' => $ordering_process,
                'comment' => $comment,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);

        $returnData = array('message' => 'successfully submitted your feedback', 'status' => TRUE, 'error' => 0);
        echo json_encode($returnData);
    }

    /**
     * Add logout.
     *
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        //$status['error'] = 1;
        $status['message'] = "Logout Successfully.";
        $request->session()->forget('user_id');
        $request->session()->forget('user_name');
        $request->session()->forget('user_email');
        $request->session()->forget('user_phone');
        $request->session()->forget('user_nationality');
        $request->session()->forget('billing_name');
        $request->session()->forget('billing_street');
        $request->session()->forget('billing_street_number');
        $request->session()->forget('billing_apartment_number');
        $request->session()->forget('billing_city');
        $request->session()->forget('billing_zip');
        $request->session()->forget('billing_country');
        $request->session()->forget('session_cart_key');

        $request->session()->save();
        echo json_encode($status);
    }

    /**
     * Add login.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $usersarr = array();

        $email = $request->get('email');
        $password = $request->get('password');
        $flight_number = $request->session()->get('flight_number');

        //        echo Hash::make($password); exit;
        //        $usersarr = DB::select('select * from users 
        //                                        where  email = "'.$email.'" AND password = "'.Hash::make($password).'"');
        $usersarr = DB::select('select * from users 
                                        where  email = "' . $email . '" AND remember_token = "' . $password . '"');
        //        $flights = DB::select('select departure from flights 
        //                                        where  flight_no = "'.$flight_number.'"');
        //        $request->session()->put('flight_time', $flights[0]->departure);
        if (count($usersarr) > 0) {
            $request->session()->put('user_id', $usersarr[0]->id);
            $request->session()->put('user_name', $usersarr[0]->name);
            $request->session()->put('user_email', $usersarr[0]->email);
            $request->session()->put('user_phone', $usersarr[0]->phone_number);
            $request->session()->put('user_nationality', $usersarr[0]->nationality);
            $request->session()->put('billing_name', $usersarr[0]->billing_name);
            //$request->session()->put('billing_phone', $usersarr[0]->billing_phone);
            $request->session()->put('billing_street', $usersarr[0]->billing_street);
            $request->session()->put('billing_street_number', $usersarr[0]->billing_street_no);
            $request->session()->put('billing_apartment_number', $usersarr[0]->billing_apartment_no);
            $request->session()->put('billing_city', $usersarr[0]->billing_city);
            $request->session()->put('billing_zip', $usersarr[0]->billing_zip);
            $request->session()->put('billing_country', $usersarr[0]->billing_country);
            $request->session()->save();


            $status['error'] = 0;
            $status['message'] = "You have logged in successfully.";
        } else {
            $status['error'] = 1;
            $status['message'] = "Email or password is incorrect.";
        }


        echo json_encode($status);
    }

    /**
     * checkout login.
     *
     * @return \Illuminate\Http\Response
     */
    public function checkout(Request $request)
    {
        if ($request->session()->has('user_id')) {
            return view('frontend.products.billingaddress');
        } else {
            return view('frontend.products.checkoutlogin');
        }
    }

    /**
     * bliing address.
     *
     * @return \Illuminate\Http\Response
     */
    public function billingaddress(Request $request)
    {
        if ($request->session()->has('user_id')) {
            return view('frontend.products.billingaddress');
        }
        //$request->session()->get('user_id');
    }

    public function forgotpassword(Request $request)
    {

        $status = array();
        $usersarr = array();
        $email = $request->get('email');
        $rand = rand(1000, 9999);

        $usersarr = DB::select('select * from users 
                                        where  email = "' . $email . '" AND  type = "CUSTOMER"');
        //echo '<pre>'; print_r($usersarr); exit;
        $password = Hash::make($rand);

        if (count($usersarr) > 0) {
            DB::table('users')
                ->where('id', $usersarr[0]->id)
                ->update([
                    'password' => $password,
                    'remember_token' => $rand,
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);

            $mail_message = '<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Hi <b>' . $usersarr[0]->name . '</b>,</p>

                            <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Welcome to <b>CarryOn</b>, we’re happy to have you around!</p>
                            <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Your system generated password is <b>' . $rand . '</b></p>';
            $this->sendMail($mail_message, $usersarr[0]->email, $usersarr[0]->name, $rand);

            $status['error'] = 0;
            $status['message'] = "You password is updated successfully, and email with password will be sent to your email address.";
        } else {
            $status['error'] = 1;
            $status['message'] = "Email address not found in our records.";
        }


        echo json_encode($status);
    }
    public function resetpassword(Request $request)
    {

        $status = array();
        $usersarr = array();
        $user_id = $request->session()->get('user_id');
        $current_password = $request->post('current_password');
        $new_password = $request->post('new_password');

        $usersarr = DB::select('select * from users 
                                        where  id = "' . $user_id . '" AND  remember_token = "' . $current_password . '"');
        //echo '<pre>'; print_r($usersarr); exit;
        $password_hashed = Hash::make($new_password);

        if (count($usersarr) > 0) {
            DB::table('users')
                ->where('id', $user_id)
                ->update([
                    'password' => $password_hashed,
                    'remember_token' => $new_password,
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);

            $status['error'] = 0;
            $status['message'] = "You password is updated successfully.";
        } else {
            $status['error'] = 1;
            $status['message'] = "Provided current password is incorrect.";
        }

        echo json_encode($status);
    }
}
