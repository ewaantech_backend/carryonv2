<?php

namespace App\Http\Controllers\Frontend;

use App\AddonCartItems;
use App\Addons;
use App\AddonsOrderItems;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Support\Facades\Session;

use App\Order;
use App\Orderitems;
use App\Userkey;

use Illuminate\Support\Facades\Mail;
use App\Mail\OrderEmail;
use App\ShortLink;
use Illuminate\Support\Str;
use App\Notifications;

use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Support\Facades\File;

class Checkout extends Controller
{

    private static $API_ACCESS_KEY = 'AAAA0Ihrr6E:APA91bHDWmqOd5813O2xPryWcxkqrUxl9_j6KjwuRDgIg_Elmtl6zKhc4k3gq81vFJDEc3wlV0ClYJc8dFYiKXsmVgPvCESxUPXUD_nsTysb5T7GAS5c710Xtx3eVqhk6Aky6cEg96aE'; //'AIzaSyCCxJPno4WLLkGO6LM92weZ9ipY_jyGA5s';

    // Sends Push notification for Android users
    public function android($data, $reg_id)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';
        
        $message = array(
            'title' => $data['mtitle'],
            'body' => $data['mdesc'],
            'vibrate' => 1,
            'sound' => "default",
            'icon' => 'ic_notification'
        );

        $headers = array(
            'Authorization: key=' . self::$API_ACCESS_KEY,
            'Content-Type: application/json'
        );

        $fields = array(
            'registration_ids' => array($reg_id),
            'notification' => $message,
            "android" => [
                "notification" => [
                    "sound"  => "default",
                    "icon"  => "ic_notification",
                ]
            ],
            'data' => $message
        );

        return $this->useCurl($url, $headers, json_encode($fields));
    }

    // Curl 
    private function useCurl($url, $headers, $fields = null)
    {
        // Open connection
        $ch = curl_init();
        if ($url) {
            // Set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            // Disabling SSL Certificate support temporarly
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            if ($fields) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
            }

            // Execute post
            $result = curl_exec($ch);
            if ($result === FALSE) {
                die('Curl failed: ' . curl_error($ch));
            }

            // Close connection
            curl_close($ch);
            //                echo "<pre>";
            //                    print_r($result);
            //                    exit;
            return $result;
        }
    }


    function pre_order_checkout(Request $request)
    {

        $mode = 1; // 1 for test, 0 for live

        $user_id = $request->session()->get('user_id');
        $customer_data = DB::table('users')->select('name', 'email', 'phone_number')->where('id', '=', $user_id)->first();
        $cart_data =  [];
        $cart_sess_key = $request->session()->get('session_cart_key');
        if ($cart_sess_key) {
            $cartkey = DB::select("select cart_id from cartkey where `key` = :cartkey ", ['cartkey' => $cart_sess_key]);
            if (!empty($cartkey) && $user_id) {
                $cartid = $cartkey[0]->cart_id;
                $response = DB::table('cart')->where('id', '=', $cartid)->update([
                    'users_id' => $user_id
                ]);
            }
            if (!empty($cartkey)) {
                $cart_data = $result = DB::table('cart')->select('id', 'grant_total')->where('id', '=', $cartid)->first();
            }
        }
        if (!$cart_data) {
            $cart_data = $result = DB::table('cart')->select('id', 'grant_total')
                ->where('users_id', '=', $user_id)->orderBy('id', 'desc')->first();
        }
        if (!$cart_data) {
            // Something went wrong.
            echo '<script>window.top.location.href="' . route('menu_dishes') . '";</script>';
        }

        $params = array(
            'ivp_method'  => 'create',
            'ivp_store'   => '22999',
            'ivp_authkey' => 'CzR46^fcVJ#C4gRP',
            'ivp_framed'  => 1,
            'ivp_cart'    => $cart_data->id . '-' . time(),
            'ivp_test'    => $mode,
            'ivp_amount'  => $cart_data->grant_total,
            'ivp_currency' => 'AED',
            'ivp_desc'    => 'Carryon | The good food culture | Payment',
            'return_auth' => route('payment_success'),
            'return_can'  => route('payment_cancelled'),
            'return_decl' => route('payment_declined'),
            'bill_fname' => '',
            'bill_sname' => '',
            'bill_addr1' => 'Dubai',
            'bill_city'  => 'Dubai',
            'bill_country' => 'AE',
            'bill_email' => $customer_data->email,
            'bill_phone' => $customer_data->phone_number
        );

        //echo '<pre>';print_r($params);echo '</pre>';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://secure.telr.com/gateway/order.json");
        curl_setopt($ch, CURLOPT_POST, count($params));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
        $results = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $results = json_decode($results, true);

        $response = DB::table('cart')->where('id', '=', $cart_data->id)->update([
            'payment_ref' => trim($results['order']['ref'])
        ]);

        if ($httpcode == 200 && isset($results['order']['url'])) {
            $url = trim($results['order']['url']);
            $status = TRUE;
        } else {
            $status = FALSE;
            $url = '';
        }
        // $status = FALSE; to check payment gateway failure.

        return view('frontend.checkout.checkout_pay', array(
            'url' => $url,
            'customer' => $customer_data,
            'status' => $status
        ));
    }

    function payment_success(Request $request)
    {

        $user_id = $request->session()->get('user_id');
        $is_mobile = $request->session()->get('mobile_pay');
        $cart_sess_key = $request->session()->get('session_cart_key');
        $cart_data = [];
        if ($cart_sess_key) {
            $cartkey = DB::select("select cart_id as id from cartkey where `key` = :cartkey ", ['cartkey' => $cart_sess_key]);
            if (!empty($cartkey)) {
                $cartid = $cartkey[0]->id;
                $cart_data = $result = DB::table('cart')->select('id', 'payment_ref')->where('id', '=', $cartid)->first();
            }
        }
        if (!$cart_data) {
            $cart_data = $result = DB::table('cart')->select('id', 'payment_ref')
                ->where('users_id', '=', $user_id)->orderBy('id', 'desc')->first();
        }
        $params = array(
            'ivp_method'  => 'check',
            'ivp_store'   => '22999',
            'ivp_authkey' => 'CzR46^fcVJ#C4gRP',
            'order_ref' => $cart_data->payment_ref
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://secure.telr.com/gateway/order.json");
        curl_setopt($ch, CURLOPT_POST, count($params));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
        $results = curl_exec($ch);
        curl_close($ch);
        $results = json_decode($results, true);
        
        if (!isset($results['order'])) {
            Session::flash('message', 'Payment failed');

            if ($is_mobile == TRUE) {
                echo '<script>window.top.location.href="' . route('post_mobile_payment', ['data' => 'failed']) . '";</script>';
            } else {
                echo '<script>window.top.location.href="' . route('payment_page') . '";</script>';
            }     
            return;
        }

        if ($results['order']['status']['code'] == -1) {
            $text = $results['order']['status']['text'] ?? 'Expired';
            Session::flash('message', $text);

            if ($is_mobile == TRUE) {
                echo '<script>window.top.location.href="' . route('post_mobile_payment', ['data' => 'failed']) . '";</script>';
            } else {
                echo '<script>window.top.location.href="' . route('payment_page') . '";</script>';
            }

            return;
        }

        $order_id = $results['order']['cartid'];
        $order_users_id = $user_id;
        $status = 'SUCCESS';
        $amount = $results['order']['amount'];
        $signature = $results['order']['ref'];
        $reference_id = $results['order']['transaction']['ref'] ?? '';
        $authCode = $results['order']['transaction']['code'] ?? '';
        $payment_mode = $results['order']['paymethod'];
        $type = 'ONLINE';
        //$note = 'Card last 4 :'.$results['order']['card']['last4'] ?? '';
        $note = $results['order']['card']['last4'] ?? NULL;
        date_default_timezone_set('Asia/Dubai');
        $pay_id = DB::table('payments')->insertGetId([
            'order_id' => $order_id,
            'order_ref' => explode('-', $order_id)[0] ?? 0,
            'auth_code' => $authCode,
            'order_users_id' => $order_users_id,
            'status' => $status,
            'amount' => $amount,
            'signature' => $signature,
            'reference_id' => $reference_id,
            'payment_mode' => $payment_mode,
            'type' => $type,
            'note' => $note,
            'receipt' => '',
            'created_at' => date('Y-m-d H:i:s')
        ]);

        $this->create($user_id, $request->session()->get('flight_id'), $request, $pay_id);
    }

    function payment_cancelled(Request $request)
    {

        $is_mobile = $request->session()->get('mobile_pay');
        $user_id = $request->session()->get('user_id');
        $cart_data = $result = DB::table('cart')->select('id', 'payment_ref')->where('users_id', '=', $user_id)->first();
        $params = array(
            'ivp_method'  => 'check',
            'ivp_store'   => '22999',
            'ivp_authkey' => 'CzR46^fcVJ#C4gRP',
            'order_ref' => $cart_data->payment_ref
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://secure.telr.com/gateway/order.json");
        curl_setopt($ch, CURLOPT_POST, count($params));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
        $results = curl_exec($ch);
        curl_close($ch);

        $results = json_decode($results, true);
        Session::flash('message', 'payment cancelled!');

        if ($is_mobile == TRUE) {
            echo '<script>window.top.location.href="' . route('post_mobile_payment', ['data' => 'cancelled']) . '";</script>';
            // echo '<html><body><h1>Hi!</h1><script type="text/javascript">setInterval(function() { document.write(" hi"); window.postMessage("hi");}, 1000);</script></body></html>';
        } else {
            echo '<script>window.top.location.href="' . route('payment_page') . '";</script>';
        }
        //return redirect()->route('payment_page');
    }

    function post_mobile_pay(Request $request, $data)
    {
        return view('frontend.checkout.checkout_mobile_success');
    }

    function payment_declined(Request $request)
    {

        $is_mobile = $request->session()->get('mobile_pay');
        $user_id = $request->session()->get('user_id');
        $cart_data = $result = DB::table('cart')->select('id', 'payment_ref')->where('users_id', '=', $user_id)->first();
        $params = array(
            'ivp_method'  => 'check',
            'ivp_store'   => '22999',
            'ivp_authkey' => 'CzR46^fcVJ#C4gRP',
            'order_ref' => $cart_data->payment_ref
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://secure.telr.com/gateway/order.json");
        curl_setopt($ch, CURLOPT_POST, count($params));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
        $results = curl_exec($ch);
        curl_close($ch);

        $results = json_decode($results, true);
        Session::flash('message', 'payment declined!');

        if ($is_mobile == TRUE) {
            echo '<script>window.top.location.href="' . route('post_mobile_payment', ['data' => 'declined']) . '";</script>';
        } else {
            echo '<script>window.top.location.href="' . route('our_menu') . '";</script>';
        }
    }

    public function create($user_id, $flight_number, $request, $pay_id)
    {

        $is_mobile = $request->session()->get('mobile_pay');
        if (!$user_id) {
            $returnData = array("message" => "User is not logged in");
            echo json_encode($returnData);
            exit;
        }

        $cart_sess_key = $request->session()->get('session_cart_key');
        $cart = [];
        if ($cart_sess_key) {
            $cartkey = DB::select("select id from cartkey where `key` = :cartkey ", ['cartkey' => $cart_sess_key]);
            if (!empty($cartkey)) {
                $cartid = $cartkey[0]->id;
                $cart = DB::select("select id from cart where `id` = :cartid ", ['cartid' => $cartid]);
            }
        }
        if (!$cart) {
            $cart = DB::select("select id from cart where `users_id` = :users_id order by id desc", ['users_id' => $user_id]);
        }

        if (empty($cart)) {
            $returnData = array('users_id' => $user_id, "message" => "Not a valid user");
            echo json_encode($returnData);
            exit;
        } else {
            $cartid = $cart[0]->id;
            $result = DB::select('select cart.id as cartid, '
                . 'cart.item_count as item_count_total,'
                . ' cart.grant_total as grant_total_total,'
                . ' cart.sub_total as sub_total_total,'
                . ' cart.tax_total as tax_total_total,'
                . ' cart_items.item_count as item_count_item_total,'
                . ' cart_items.products_id as products_id,'
                . ' cart_items.id as cart_item_id,'
                . ' cart_items.grant_total as grant_total_item_total,'
                . ' cart_items.sub_total as sub_total_item_total,'
                . ' cart_items.tax_total as tax_total_item_total'
                . ' from cart join cart_items on cart.id = cart_items.cart_id where cart.id = :cartid', ['cartid' => $cartid]);
            $modifiedResult = array(
                'maindata' => array(),
                'eachdata' => array()
            );
            foreach ($result as $key => $value) {
                $modifiedResult['maindata'] = array(
                    'item_count_total' => $value->item_count_total,
                    'grant_total_total' => $value->grant_total_total,
                    'sub_total_total' => $value->sub_total_total,
                    'tax_total_total' => $value->tax_total_total
                );
                $modifiedResult['eachdata'][$key] = array(
                    'item_count_item_total' => $value->item_count_item_total,
                    'grant_total_item_total' => $value->grant_total_item_total,
                    'sub_total_item_total' => $value->sub_total_item_total,
                    'tax_total_item_total' => $value->tax_total_item_total,
                    'products_id' => $value->products_id,
                    'cart_item_id' => $value->cart_item_id
                );
            }

            if (!empty($modifiedResult['maindata']) && !empty($modifiedResult['eachdata'])) {
                $maindata = $modifiedResult['maindata'];
                $timestamp = date('Y-m-d H:i:s');
                $order = new Order;
                $order->users_id = $user_id;
                $order->item_count = $maindata['item_count_total'];
                $order->grant_total = $maindata['grant_total_total'];
                $order->sub_total = $maindata['sub_total_total'];
                $order->tax_total = $maindata['tax_total_total'];
                $order->flight_number = $flight_number;
                $order->status = 'ORDER_CONFIRMED';

                $flight_date = $request->session()->get('flight_date');
                $gateData = $this->getFlightDate($flight_number, $flight_date);
                $apiGate = '-';
                $apiGatePickup = '';
                if ($gateData) {
                    $apiGate = $gateData['gate'];
                    $apiGatePickup = $gateData['pickup'];
                }

                $order->gate = $apiGate;
                $order->pickup_point = $apiGatePickup;
                $order->duration = $request->session()->get('flight_duration');
                $order->created_at = $timestamp;
                $order->updated_at = $timestamp;

                $order->order_at = $flight_date;
                $flight_slotes = DB::select("select `flight_slots`.`time_from`,`flight_slots`.`time_to`,`flight_slots`.`pickup_schedule`,`flight_slots`.`last_order`,`flight_slots`.`in_kitchen` from `flight_slots`");
                foreach ($flight_slotes as $key => $value) {
                    $date = date('Y-m-d', strtotime($flight_date));
                    $time_from = $date . " " . $value->time_from;
                    $time_to = $date . " " . $value->time_to;
                    $pickup_schedule = $date . " " . $value->pickup_schedule;
                    $in_kitchen = $date . " " . $value->in_kitchen;
                    $flight_date = date('Y-m-d H:i:s', strtotime($flight_date));
                    if ($time_from <= $flight_date && $time_to >= $flight_date) {
                        $order->pickup_time = $pickup_schedule;
                    }
                }
                $saveorder = $order->save();
                $orderid = $order->id;

                $folder_structure = date('Y') . "/" . date('m') . "/" . date('d');
		$target_dir_path = "public/upload/qrcodes/" . $folder_structure;
		$target_orgin_dir_path = "upload/qrcodes/" . $folder_structure;
                if ($request->getHttpHost() == 'localhost') {
                    $target_dir_path = "public/upload/qrcodes/" . $folder_structure;
                }
                //$create_dir_path = "public/upload/qrcodes/" . $folder_structure;
                $create_dir_path = "public/upload/qrcodes/" . $folder_structure;
                if (!File::isDirectory($create_dir_path)) {
                    File::makeDirectory($create_dir_path, 0777, true, true);
                }
                $createfilepath = $target_dir_path . '/qr_' . $orderid . '.png'; 
                $filepath = $target_orgin_dir_path . '/qr_' . $orderid . '.png';
                $invEncode = str_pad($orderid, 5, '0', STR_PAD_LEFT);
                $invEncode = "CON" . $invEncode;
                QrCode::backgroundColor(255, 255, 0)
                    ->format('png')
                    ->size(100)
                    ->generate($invEncode, $createfilepath);
                DB::table('order')
                    ->where('id', '=', $orderid)
                    ->update([
                        'qr_code' => $filepath
                    ]);

                DB::table('payments')
                    ->where('id', '=', $pay_id)
                    ->update([
                        'order_ref' => $orderid
                    ]);

                $cartItemsIDArray = array();
                foreach ($modifiedResult['eachdata'] as $key => $value) {
                    $orderitems = new Orderitems();
                    $orderitems->order_id = $orderid;
                    $orderitems->products_id = $value['products_id'];
                    $orderitems->item_count = $value['item_count_item_total'];
                    $orderitems->grant_total = $value['grant_total_item_total'];
                    $orderitems->sub_total = $value['sub_total_item_total'];
                    $orderitems->tax_total = $value['tax_total_item_total'];
                    $orderitems->created_at = $timestamp;
                    $orderitems->updated_at = $timestamp;
                    $saveorderitems = $orderitems->save();
                    $cartItemsAddons = AddonCartItems::where('cart_items_id', $value['cart_item_id'])
                        ->get();

                    $cartItemsIDArray[] = $value['cart_item_id'];

                    foreach ($cartItemsAddons as $addon) {
                        $addonObj = Addons::find($addon->addons_id);
                        $addonsOrder = new AddonsOrderItems();
                        $addonsOrder->order_items_id = $orderitems->id;
                        $addonsOrder->item_count = $addon->item_count;
                        $addonsOrder->addons_id = $addon->addons_id;
                        $addonsOrder->total_amount = $addon->item_count * $addonObj->actual_price;
                        $addonsOrder->created_at = $timestamp;
                        $addonsOrder->save();
                    }
                }

                $cartItemsID = implode(",", $cartItemsIDArray);
                DB::table('cart_items')->where('cart_id', $cartid)->delete();
                DB::table('cart')->where('id', $cartid)->delete();
                DB::delete("DELETE FROM `addons_cart_items` WHERE cart_items_id IN ($cartItemsID)");

                $returnData = array("message" => "Order Placed");
                $user_data = DB::table('users')->select('id', 'fcm_token', 'phone_number', 'email', 'name')->where('id', '=', $user_id)->first();
                $invID = str_pad($orderid, 5, '0', STR_PAD_LEFT);
                $invID = "CON" . $invID;
                $template = DB::select("select template from sms_template where `type` = 'ORDER_PLACE'");
                $message = '';
                if (!empty($template)) {
                    $sms_template = $template[0]->template;
                    $message = str_replace("{{ orderId }}", $invID, $sms_template);
                }
                $this->sendSMS($message, $user_data->phone_number);

                $result = DB::select('select `order`.`id` as orderid, '
                    . '`order_items`.`id` as orderitemid, '
                    . '`order`.`item_count` as item_count_total,'
                    . '`order`.`created_at` as created_at,'
                    . '`order`.`order_at` as order_at,'
                    . '`order`.flight_number as flight_no,'
                    . '`order`.gate as gate,'
                    . '`order`.pickup_reached_at as pickup_reached_at,'
                    . '`order`.pickup_point as boarding_point,'
                    . '`order`.`status` as order_status,'
                    . ' `order`.`grant_total` as grant_total,'
                    . ' `order`.`sub_total` as sub_total,'
                    . ' `order`.`tax_total` as tax_total,'
                    . ' `order`.`qr_code` as qr_code,'
                    . ' order_items.item_count as item_count,'
                    . ' order_items.products_id as products_id,'
                    . ' order_items.grant_total as grant_item_price,'
                    . ' order_items.sub_total as sub_item_price,'
                    . ' order_items.tax_total as tax_item_price,'
                    . ' products.name as productname,'
                    . ' products.actual_price as actual_price,'
                    . ' products.description as productdescription,'
                    . ' product_images.path as productthumbnail,'
                    . ' ado.name as addon_name, ado.actual_price as addon_price, aoi.item_count as addon_item_count'
                    . ' from `order` join order_items on `order`.`id` = order_items.order_id'
                    . ' join products on `products`.`id` = order_items.products_id '
                    . ' left join product_images on `products`.`id` = product_images.products_id and product_images.type = "THUMBNAIL"'
                    . ' left join addons_order_items as aoi on order_items.id = aoi.order_items_id'
                    .  ' left join addons as ado on aoi.addons_id = ado.id'
                    . ' where `order`.`id` = :orderid '
                    . '  order by `order`.`id` desc', ['orderid' => $orderid]);

                $modifiedResult = array();
                $items = '';
                $sub_total = '0.00';
                $grant_total = '0.00';
                if (!empty($result)) {
                    $i = 0;
                    foreach ($result as $key => $value) {
                        if ($i == 0) {
                            $modifiedResult = array(
                                'order_id' => $value->orderid,
                                'created_at' => $value->created_at,
                                'order_at' => $value->order_at,
                                'flight_no' => $value->flight_no,
                                'qr_code' => $value->qr_code,
                                'boarding_point' => $value->boarding_point,
                                'order_status' => str_replace("_", " ", $value->order_status),
                                'item_count_total' => $value->item_count_total,
                                'grant_total' => decimal_format_custom($value->grant_total),
                                'sub_total' => decimal_format_custom($value->sub_total),
                                'tax_total' => decimal_format_custom($value->tax_total)
                            );
                            $grant_total = $value->grant_total;
                            $sub_total = $value->sub_total;
                        }

                        if (!isset($modifiedResult['items'][$value->orderitemid])) {
                            $modifiedResult['items'][$value->orderitemid] = array(
                                'item_count' => $value->item_count,
                                'grant_item_price' => decimal_format_custom($value->grant_item_price),
                                'actual_price' => decimal_format_custom($value->actual_price),
                                'sub_item_price' => decimal_format_custom(($value->sub_item_price * $value->item_count)),
                                'tax_item_price' => decimal_format_custom($value->tax_item_price),
                                'products_id' => $value->products_id,
                                'productname' => $value->productname,
                                'productthumbnail' => empty($value->productthumbnail) ? $value->productthumbnail : url($value->productthumbnail),
                                'productdescription' => $value->productdescription,
                            );
                            if (!empty($value->addon_name)) {
                                $modifiedResult['items'][$value->orderitemid]['addons'][] = array(
                                    'addon_name' => $value->addon_name,
                                    'addon_price' => $value->addon_price,
                                    'addon_item_count' => $value->addon_item_count
                                );
                            } else {
                                $modifiedResult['items'][$value->orderitemid]['addons'] = array();
                            }
                        } else {
                            if (!empty($value->addon_name)) {
                                $modifiedResult['items'][$value->orderitemid]['addons'][] = array(
                                    'addon_name' => $value->addon_name,
                                    'addon_price' => $value->addon_price,
                                    'addon_item_count' => $value->addon_item_count
                                );
                            } else {
                                $modifiedResult['items'][$value->orderitemid]['addons'] = array();
                            }
                        }
                        $i++;
                    }
                }

                $addon_total = "0.00";
                $product_total = "0.00";

                if (!empty($modifiedResult)) {
                    foreach ($modifiedResult['items'] as $key => $value) {
                        $grant_item_price = $value["actual_price"] * $value["item_count"];
                        $product_total += $grant_item_price; 
                        $items .= '<tr>
                        <td style="background-color: #ffffff; color: #3f2d26; text-align: left;">
                        <h4>' . $value["productname"] . '</h4>
                        </td>
                        <td style="background-color: #ffffff; color: #3f2d26; text-align: left;">
                        AED ' . $value["actual_price"] . '
                        </td>
                        <td style="background-color: #ffffff; color: #3f2d26; text-align: left;">
                        ' . $value["item_count"] . '
                        </td>
                        <td style="background-color: #ffffff; color: #3f2d26; text-align: left;">
                        AED ' . $grant_item_price . '
                        </td>
                </tr>';
                        if (count($value['addons']) > 0) {
                            $items .= '<tr>
                            <td style="background-color: #ffffff; color: #3f2d26; text-align: left;"><h5>Addons</h5></td>
                        </tr>';
                            foreach ($value['addons'] as $addons) {
                                if (!empty($addons['addon_item_count'])) {
                                    $addonName = !empty($addons['addon_name']) ? $addons['addon_name'] : 'Nil';
                                    $addonPrice = !empty($addons['addon_price']) ? 'AED ' . $addons['addon_name'] : '0';
                                    $addonItemCount = !empty($addons['addon_item_count']) ?  $addons['addon_item_count'] : '0';
                                    $addon_total += ($addons['addon_item_count'] * $addons['addon_price']);
                                    $addonTotal = 'AED ' . $addons['addon_item_count'] * $addons['addon_price'];
                                    $items .= "<tr>
                                    <td style='background-color: #ffffff; color: #3f2d26; text-align: left;'>
                                        " . $addonName . "
                                    </td>
                                    <td style='background-color: #ffffff; color: #3f2d26; text-align: left;'>
                                       " . $addonPrice . "
                                    </td>
                                    <td style='background-color: #ffffff; color: #3f2d26; text-align: left;'>
                                        " . $addonItemCount . "
                                    </td>
                                    <td style='background-color: #ffffff; color: #3f2d26; text-align: left;'>
                                       " . $addonTotal . "
                                    </td>
                            </tr>";
                                }
                            }
                        }
                    }
                }
                $grant_total = $product_total + $addon_total; 
                $item_content = '<table width="100%" cellspacing="0" cellpadding="10" border="0" style=" font-size:13px;">
									<thead>
										<tr>
										<th style="background-color: #f16521; color: #fff; text-align: left;">
										<font face="arial">PRODUCT NAME</font>
										</th>
										<th style="background-color: #f16521; color: #fff; text-align: left;">
										<font face="arial">UNIT PRICE</font>
										</th>
										<th style="background-color: #f16521; color: #fff; text-align: left;">
										<font face="arial">QUANTITY</font>
										</th>
										<th style="background-color: #f16521; color: #fff; text-align: left;">
										<font face="arial">AMOUNT</font>
                                        </th>
									</tr>
									</thead>
									<tbody>
                                                                         ' . $items . '   
									</tbody>
									<tfoot>
										<tr>
											<td></td>
											<td colspan="2" style="font-size:14px;">
											<font face="arial" style="color: #000000;">Total (Including VAT): </font>
											</td>
											<td style="font-size:15px;color: #000000;">
											<font face="arial"><small style="font-size:11px;">AED</small> <strong>' . $grant_total . '</strong></font>
										</td>
									</tr>
									</tfoot>
                                </table>';
                $dataTransaction = DB::table('payments')->select('*')
                    ->where('id', '=', $pay_id)->where('status', '=', 'SUCCESS')->first();
                $ref = $dataTransaction->reference_id ?? '';
                $type = $dataTransaction->payment_mode ?? '';
                $time = date('d-m-Y H:i', strtotime($dataTransaction->created_at)) ?? '';
                $auth_code = $dataTransaction->auth_code ?? '';
                $card = '***********' . $dataTransaction->note ?? '';
                $item_content .= '<table width="100%" cellspacing="0" cellpadding="10" border="0" style=" font-size:13px;">
                                <thead>
                                    <tr>
                                    <th colspan=2 style="background-color: #f16521; color: #fff; text-align: left;">
                                        <font face="arial">Payment Details</font>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                      <tr>
                                        <td style="background-color: #ffffff; color: #3f2d26; text-align: left;">Transaction Reference</td>
                                        <td style="background-color: #ffffff; color: #3f2d26; text-align: left;">
                                            <font face="arial" style="color: #000000;">' . $ref . '</font>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td style="background-color: #ffffff; color: #3f2d26; text-align: left;">Transaction Type</td>
                                        <td style="background-color: #ffffff; color: #3f2d26; text-align: left;">
                                            <font face="arial" style="color: #000000;">' . $type . '</font>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td style="background-color: #ffffff; color: #3f2d26; text-align: left;">Time</td>
                                        <td style="background-color: #ffffff; color: #3f2d26; text-align: left;">
                                            <font face="arial" style="color: #000000;">' . $time . '</font>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td style="background-color: #ffffff; color: #3f2d26; text-align: left;">Authorization Code</td>
                                        <td style="background-color: #ffffff; color: #3f2d26; text-align: left;">
                                            <font face="arial" style="color: #000000;">' . $auth_code . '</font>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td style="background-color: #ffffff; color: #3f2d26; text-align: left;">Card</td>
                                        <td style="background-color: #ffffff; color: #3f2d26; text-align: left;">
                                            <font face="arial" style="color: #000000;">' . $card . '</font>
                                        </td>
                                      </tr>
                                </tbody>
                                </table>';
                $qr_url = url($filepath);
                $default_grabbit_location = default_grabbit_location();
                $notification_data = DB::select("select id as user_id, fcm_token  from `users` where `users`.`pickup_point` = :pickup_point", ['pickup_point' => $default_grabbit_location['location']]);
                $title = 'Notification';
                $message = 'Order Id ' . $invID . ' is placed';
                if (!empty($notification_data)) {
                    foreach ($notification_data as $key => $value) {
                        $this->notification_add($value->user_id, $message, $title, $value->fcm_token, $orderid);
                    }
                }
                $this->notification_add($user_data->id, $message, $title, $user_data->fcm_token, $orderid);
                try {
                    //                        $data = ['subject' => 'Thank you for choosing Carry On as your extra baggage!', 'cust_name' => $user_data->name,
                    //                            'order_id' => $invID, 'item_content' => $item_content, 'qr_url'=> $qr_url ];
                    $subject_data = DB::select("select template from sms_template where `type` = :type", ['type' => 'EMAIL_SUBJECT_ORDERPLACE']);
                    $subject = $subject_data[0]->template;
                    $data = [
                        'subject' => $subject, 'cust_name' => $user_data->name,
                        'order_id' => $invID, 'item_content' => $item_content, 'qr_url' => $qr_url
                    ];

                    Mail::to($user_data->email)->send(new OrderEmail($data));
                } catch (\Exception $e) {
                    print_r($e->getMessage());
                }

                $request->session()->forget('session_cart_key');
                $request->session()->save();

                Session::flash('message', 'Order placed successfully!');
                Session::flash('code', 'PAYED_OK');

                if ($is_mobile == TRUE) {
                    echo '<script>window.top.location.href="' . route('post_mobile_payment', ['data' => 'passed_' . $orderid]) . '";</script>';
                } else {
                    echo '<script>window.top.location.href="' . route('order_detail', ['order_id' => $orderid]) . '";</script>';
                }
            }
        }
    }

    function notification_add($user_id, $message, $title, $fcm_token, $order_id)
    {
        $notifications = new Notifications;
        $notifications->users_id = $user_id;
        $notifications->order_id = $order_id;
        $notifications->message = $message;
        $notifications->created_at = date("Y-m-d H:i:s");
        $notifications->updated_at = date("Y-m-d H:i:s");
        $notifications->save();
        // Message payload
        $msg_payload = array(
            'mtitle' => $title,
            'mdesc' => $message,
        );

        // For Android
        $reg_id = $fcm_token;

        $result = $this->android($msg_payload, $reg_id);
    }

    function getFlightDate($flight_no, $date)
    {
        $date = date('Y/m/d', strtotime($date));
        $fn_prefix = substr($flight_no, 0, 2);
        $fn = substr($flight_no, 2);
        $handle = curl_init();

        $url = "https://api.flightstats.com/flex/flightstatus/rest/v2/json/flight/status/" . $fn_prefix . "/" . $fn . "/dep/" . $date . "?appId=8a5a55e4&appKey=b3e7538d084234f6f83779bdfbd671ed&utc=false&airport=DXB";

        curl_setopt($handle, CURLOPT_URL, $url);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_HTTPHEADER, array('DY-X-Authorization: 3b75a0bdfc7aa9d383e3f8beef0f2b623a94335c'));
        $output = curl_exec($handle);
        if (curl_errno($handle)) {
            print "Error: " . curl_error($handle);
            exit();
        }

        curl_close($handle);
        $output = json_decode($output);
        if (isset($output->flightStatuses[0]->airportResources->departureGate)) {
            $gate = $output->flightStatuses[0]->airportResources->departureGate;
            if ($gate) {
                $p_point = DB::table('boarding_pickup_map')->select('pickup_point')->where('gate', '=', $gate)->first();
                return [
                    'gate' => $gate,
                    'pickup' => $p_point->pickup_point ?? ''
                ];
            }
        } else {
            $handle = curl_init();
            $url = "http://aviation-edge.com/v2/public/timetable?key=3818d1-36c1c9&iataCode=DXB&type=departure&flight_iata=$flight_no";
            curl_setopt($handle, CURLOPT_URL, $url);
            curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec($handle);
            if (curl_errno($handle)) {
                print "Error: " . curl_error($handle);
            }

            curl_close($handle);
            $output = json_decode($output);
            if (!isset($output->error) && isset($output[0]->departure->gate) && $output[0]->departure->gate != '') {
                $gate = $output[0]->departure->gate;
                $p_point = DB::table('boarding_pickup_map')->select('pickup_point')->where('gate', '=', $gate)->first();
                return [
                    'gate' => $gate,
                    'pickup' => $p_point->pickup_point ?? ''
                ];
            }
        }

        return FALSE;
    }

    function samplesms()
    {
        $message = 'Your CarryOn order is confirmed! Before you fly, don�t forget to pass by - pick up your meals on your way to your gate.';
        $this->sendSMS($message, '+971 55 691 4524');
    }

    function sendSMS($message, $phonenumber)
    {
        $message = urlencode($message);
        preg_match_all('!\d+!', $phonenumber, $matches);
        $phone = implode('', $matches[0]);
        $url = "http://mshastra.com/sendurlcomma.aspx?user=20093644&pwd=Emirates321!&senderid=CarryonDXB&mobileno=" . $phone . "&msgtext=" . $message . "&priority=High&CountryCode=ALL";



        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch);
        echo $curl_scraped_page;
    }

    function update_cart_user(Request $request)
    {
        $key = $request->post('key');
        $user_id = $request->session()->get('user_id');
        $cartkey = DB::select("select id from cartkey where `key` = :cartkey ", ['cartkey' => $key]);
        if (empty($cartkey)) {
            $returnData = array("message" => "Not a valid key", 'status' => FALSE);
            echo json_encode($returnData);
            return;
        } else {
            $cartid = $cartkey[0]->id;
        }
        $response = DB::table('cart')->where('id', '=', $cartid)->update([
            'users_id' => $user_id
        ]);

        echo json_encode(['status' => TRUE]);
        return;
    }

    // Create short link.
    public function link_store($long_link)
    {
        $input['link'] = $long_link;
        $input['code'] = Str::random(6);

        ShortLink::create($input);

        return $input['code'];
    }

    // Redirect to actual link.
    public function shortenLink($code)
    {
        $find = ShortLink::where('code', $code)->first();

        return redirect($find->link);
    }
    
    
    /**
     * Display the specified resource.
     *
     */
    public function flightdata($request, $flight = '',$date = '')
    {

        if( ! $flight || ! $date) {
            return response()->json([ 'status' => FALSE, 'message' => 'Please provide date and flight number' ], 401);
        }
//        $dateSplitted = explode('/', $date);
        $dateSplitted = explode('-', $date);
//        $day = $dateSplitted[1];
//        $month = $dateSplitted[0];
//        $year = $dateSplitted[2];
        $day = $dateSplitted[0];
        $month = $dateSplitted[1];
        $year = $dateSplitted[2];
        $flight_iata = substr($flight, 0, 2);
        $flight_code = substr($flight, 2, 4);
        $url = "https://api.flightstats.com/flex/schedules/rest/v1/json/flight/$flight_iata/$flight_code/departing/$year/$month/$day?appId=8a5a55e4&appKey=b3e7538d084234f6f83779bdfbd671ed";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
        $results = curl_exec($ch);

        $results = json_decode($results, true);
        // Error from API side.
        if( isset( $results['error']['errorMessage'] ) ) {
            return response()->json(['status' => FALSE, 'message' => explode('.', $results['error']['errorMessage'])[0]], 401);
        } 
        else if( isset($results['scheduledFlights']) && $results['scheduledFlights'] ) {
            // Check departure from DXB.
            if( ! isset( $results['scheduledFlights'][0]['departureAirportFsCode'] ) || $results['scheduledFlights'][0]['departureAirportFsCode'] != 'DXB'  ) {
//                return response()->json(['status' => FALSE, 'message' => 'We’re currently operating only from DXB, Terminal 3. Stay tuned for updates!'], 401);
                echo '<script>window.top.location.href="' . route('post_mobile_payment', ['data' => 'failed']) . '";</script>';
                return;
            }
            // Check departure from Terminal 3.
            if( ! isset( $results['scheduledFlights'][0]['departureTerminal'] ) || $results['scheduledFlights'][0]['departureTerminal'] != 3) {
//                return response()->json(['status' => FALSE, 'message' => 'We’re currently operating only from DXB, Terminal 3. Stay tuned for updates!'], 401);
                echo '<script>window.top.location.href="' . route('post_mobile_payment', ['data' => 'failed']) . '";</script>';
                return;
            }
            $diffOfRegions = 0;
            $diffVariantTimeRegionDiff = 0;
            /*
             Below code to find journey duration.
             Local current time of arrvl - local current time of departure + diff of arrvl and dep.
             */
            if( isset( $results['appendix']['airports'] ) ) {
                $dataAirports = $results['appendix']['airports'];
                if( isset($dataAirports[1]['localTime']) && isset($dataAirports[0]['localTime']) ) {
                    $diffOfRegions = strtotime($dataAirports[0]['localTime']) - strtotime($dataAirports[1]['localTime']);
                }
                $diffVariantTimeRegionDiff = strtotime($results['scheduledFlights'][0]['arrivalTime']) - strtotime($results['scheduledFlights'][0]['departureTime']);
            }
            $actualDiff = $diffVariantTimeRegionDiff + $diffOfRegions;
            $duration = $this->formatSeconds($actualDiff);
            // Duration find code ends.
            $departureTime = date('H:i', strtotime($results['scheduledFlights'][0]['departureTime']) );
            $slot = DB::table('flight_slots')->select('last_order')
                    ->where('time_from', '<=', $departureTime)
                    ->where('time_to', '>=', $departureTime)
                    ->first();
            date_default_timezone_set('Asia/Dubai');
            $localDXB_time = date('Y-m-d H:i');
            // Find date of departure
            $dateOfDeparture = date('Y-m-d', strtotime($results['scheduledFlights'][0]['departureTime']) );
            // Find possible last order time. (Removed H:i:s of departure and appended last order time from slot to dep time).
            if(strtotime($slot->last_order) >= strtotime('18:00:00')) {
                // Last possible order date is prevoius day for order time upto 05:59 AM.(its last_order time wll b [18:00:00, 23:00:00])
                $dateOfDeparture = date('Y-m-d', strtotime('-1 day', strtotime($dateOfDeparture)));
            }
            $possibleMaxOrderTime = date('Y-m-d H:i', strtotime( $dateOfDeparture . ' ' . $slot->last_order ) );
            // Check order possibility with time slot.
            if(strtotime($localDXB_time) > strtotime($possibleMaxOrderTime)) {
                $lt_time = explode(':', str_replace(':00', '', $slot->last_order));
                $lt_time = $lt_time[0] > 12 ? ($lt_time[0] -12 . ' PM') : ($lt_time[0] . ' AM');
                $departed = FALSE;
                if( strtotime($localDXB_time) > strtotime($results['scheduledFlights'][0]['departureTime']) ) {
                    $departed = TRUE;
                }
//                return response()->json( [ 
//                    'status' => FALSE, 
//                    'message' => 'time elapsed',
//                    'departed' => $departed,
//                    'last_order' => '<i>'.date('d M Y', strtotime($dateOfDeparture)). ' ' .str_replace(':00', '', $lt_time).'</i>',
//                    'test' => $slot->last_order
//                ] , 401);
                echo '<script>window.top.location.href="' . route('post_mobile_payment', ['data' => 'failed']) . '";</script>';
                return;
            }
            // Success
            $request->session()->put('flight_number', $flight);
            $request->session()->put('flight_date', $results['scheduledFlights'][0]['departureTime']);
            $request->session()->put('flight_id', $flight);
            $request->session()->put('flight_duration', $duration);
            $request->session()->put('flight_terminal', 3);
            $request->session()->save();
            if($results['appendix']['airports'][0]['iata'] == 'DXB') {
                $destin_city = $results['appendix']['airports'][1]['city'] ?? $results['scheduledFlights'][1]['arrivalAirportFsCode'];
            } else {
                $destin_city = $results['appendix']['airports'][0]['city'] ?? $results['scheduledFlights'][0]['arrivalAirportFsCode'];
            }

                return $returnArray = [ 
                    'status' => TRUE, 
                    'message' => 'Flight data fetched',
                    'flight' => $flight,
                    'departure' => date('h:i A', strtotime($results['scheduledFlights'][0]['departureTime']) ),
                    'duration' => $duration,
                    'destination' => $destin_city,
                ]; 
//            return response()->json([ 
//                    'status' => TRUE, 
//                    'message' => 'Flight data fetched',
//                    'flight' => $flight,
//                    'departure' => date('h:i A', strtotime($results['scheduledFlights'][0]['departureTime']) ),
//                    'duration' => $duration,
//                    'destination' => $destin_city,
//                ], 200);
        } else {
            echo '<script>window.top.location.href="' . route('post_mobile_payment', ['data' => 'failed']) . '";</script>';
            return;
        }
    }
    
    function formatSeconds($seconds)
    {
        // if($milliseconds > 0) {
        //     $seconds = $milliseconds / 1000;
        // }
        $hours = 0;
        $milliseconds = str_replace("0.", '', $seconds - floor($seconds));

        if ($seconds > 3600) {
            $hours = floor($seconds / 3600);
        }
        $seconds = $seconds % 3600;


        return str_pad($hours, 2, '0', STR_PAD_LEFT)
            . gmdate(':i', $seconds);
    }

    function mobile_pre_order_checkout(Request $request, $userToken, $cart_sess_key, $flight, $date, $duration)
    {

        $mode = 1; // 1 for test, 0 for live

        if (!$userToken) {
            echo '<script>window.top.location.href="' . route('post_mobile_payment', ['data' => 'token']) . '";</script>';
            return;
        }
        $userIDObj = Userkey::firstWhere('key', $userToken);
        $user_id = $userIDObj->users_id;
        if (!$user_id) {
            echo '<script>window.top.location.href="' . route('post_mobile_payment', ['data' => 'token']) . '";</script>';
            return;
        }

        $flight_data = $this->flightdata($request,$flight,$date);
        $request->session()->put('user_id', $user_id);
        $request->session()->put('cart_sess_key', $cart_sess_key);
        $request->session()->put('flight_id', $flight);
        if (empty($flight_data)) {
            $date = $date." 23:00:00";// temporary adjustment, remove it once flight api result available 
        } else {
            $date = $date.''.$flight_data['departure'];
        }
        $date = date('Y-m-d H:i:s', strtotime($date));
        $request->session()->put('flight_date', $date);
        $duration = str_replace('_', ':', $duration);
        $request->session()->put('flight_duration', $duration);
        $request->session()->put('mobile_pay', TRUE);
        $request->session()->save();

        $customer_data = DB::table('users')->select('name', 'email', 'phone_number')->where('id', '=', $user_id)->first();
        $cart_data =  [];
        if ($cart_sess_key) {
            $cartkey = DB::select("select cart_id as id from cartkey where `key` = :cartkey ", ['cartkey' => $cart_sess_key]);
            if (!empty($cartkey) && $user_id) {
                $cartid = $cartkey[0]->id;
                $response = DB::table('cart')->where('id', '=', $cartid)->update([
                    'users_id' => $user_id
                ]);
            }
            if (!empty($cartkey)) {
                $cart_data = $result = DB::table('cart')->select('id', 'grant_total')->where('id', '=', $cartid)->first();
            }
        }
        if (!$cart_data) {
            $cart_data = $result = DB::table('cart')->select('id', 'grant_total')
                ->where('users_id', '=', $user_id)->orderBy('id', 'desc')->first();
        }
        if (!$cart_data) {
            // Something went wrong.
            echo '<script>window.top.location.href="' . route('post_mobile_payment', ['data' => 'nocart']) . '";</script>';
            return;
        }

        $params = array(
            'ivp_method'  => 'create',
            'ivp_store'   => '22999',
            'ivp_authkey' => 'CzR46^fcVJ#C4gRP',
            'ivp_framed'  => 1,
            'ivp_cart'    => $cart_data->id . '-' . time(),
            'ivp_test'    => $mode,
            'ivp_amount'  => $cart_data->grant_total,
            'ivp_currency' => 'AED',
            'ivp_desc'    => 'Carryon | The good food culture | Payment',
            'return_auth' => route('payment_success'),
            'return_can'  => route('payment_cancelled'),
            'return_decl' => route('payment_declined'),
            'bill_fname' => '',
            'bill_sname' => '',
            'bill_addr1' => 'Dubai',
            'bill_city'  => 'Dubai',
            'bill_country' => 'AE',
            'bill_email' => $customer_data->email,
            'bill_phone' => $customer_data->phone_number
        );

        //echo '<pre>';print_r($params);echo '</pre>';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://secure.telr.com/gateway/order.json");
        curl_setopt($ch, CURLOPT_POST, count($params));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
        $results = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $results = json_decode($results, true);

        if (isset($results['order'])) {
            if ($httpcode == 200 && isset($results['order']['url'])) {
                $response = DB::table('cart')->where('id', '=', $cart_data->id)->update([
                    'payment_ref' => trim($results['order']['ref'])
                ]);
                $url = trim($results['order']['url']);
                $status = TRUE;
            } else {
                $status = FALSE;
                $url = '';
                echo '<script>window.top.location.href="' . route('post_mobile_payment', ['data' => 'failed']) . '";</script>';
                return;
            }
        } else {
                echo '<script>window.top.location.href="' . route('post_mobile_payment', ['data' => 'failed']) . '";</script>';
                return;
        }
        // $status = FALSE; to check payment gateway failure.

        return view('frontend.checkout.checkout_mobile_app', array(
            'url' => $url,
            // 'customer' => $customer_data,
            // 'status' => $status
        ));
    }
}
