<?php
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Userkey;
use Auth;
use App\Orderlog;
use App\Notifications;
use App\Readymail;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrderReadyForPickup;
use PDF;
use App\ShortLink;
use Illuminate\Support\Str;
use App\Products;
use Illuminate\Support\Facades\Storage;

class CustApiController extends Controller
{
    private static $API_ACCESS_KEY = 'AAAA0Ihrr6E:APA91bHDWmqOd5813O2xPryWcxkqrUxl9_j6KjwuRDgIg_Elmtl6zKhc4k3gq81vFJDEc3wlV0ClYJc8dFYiKXsmVgPvCESxUPXUD_nsTysb5T7GAS5c710Xtx3eVqhk6Aky6cEg96aE';//'AIzaSyCCxJPno4WLLkGO6LM92weZ9ipY_jyGA5s';
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      
    }
    
    
    // Sends Push notification for Android users
    public function android($data, $reg_id) {
            $url = 'https://fcm.googleapis.com/fcm/send';
            $message = array(
                'title' => $data['mtitle'],
                'body' => $data['mdesc'],
                'vibrate' => 1
            );

            $headers = array(
                    'Authorization: key=' .self::$API_ACCESS_KEY,
                    'Content-Type: application/json'
            );

            $fields = array(
                'registration_ids' => array($reg_id),
                'notification' => $message,
                "android" => [
                    "notification" => [
                        "sound"  => "default",
                        "icon"  => "ic_notification",
                     ]
                 ],
                'data' => $message
            );

            return $this->useCurl($url, $headers, json_encode($fields));
    }
    
    // Curl 
    private function useCurl($url, $headers, $fields = null) {
            // Open connection
            $ch = curl_init();
            if ($url) {
                // Set the url, number of POST vars, POST data
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                // Disabling SSL Certificate support temporarly
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                if ($fields) {
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                }

                // Execute post
                $result = curl_exec($ch);
                if ($result === FALSE) {
                    die('Curl failed: ' . curl_error($ch));
                }

                // Close connection
                curl_close($ch);
//                echo "<pre>";
//                    print_r($result);
//                    exit;
                return $result;
        }
    }
        
        
    /**
     * Login a Registered Users.
     *
     */
    public function login(Request $request){
        $uname = $request->post('username');
        $password = $request->post('password');
        if (Auth::attempt(array('email' => $uname, 'password' => $password))){
            // user data
            $user_data = DB::table('users')->select('id', 'name' , 'phone_number' , 'email', 'is_default_pickup_person', 'pickup_point', 'fcm_token')
                        ->where('email', '=', $uname)->first();
            $userid = $user_data->id;
            $fcm_token = $user_data->fcm_token;
            $user = \App\User::find($userid);
            $roles = $user->getRoleNames();
            $permissions_data = json_decode(json_encode($roles), true);
            if( in_array('pickup', $permissions_data) ) {
                $timestamp = date('Y-m-d H:i:s');
                $userkey = new Userkey;
                $userkey->users_id = $userid;
                $key = md5($this->generateCode(10));
                $userkey->key = $key;
                $userkey->created_at = $timestamp;
                $userkey->updated_at = $timestamp;
                $userkey->save();
                if ($request->post('fcm_token')) {
                    $fcm_token = $request->post('fcm_token');
                    $response = DB::table('users')->where('fcm_token', '=', $fcm_token)->update([
                        'fcm_token' => '',
                        'updated_at' => date("Y-m-d H:i:s")
                    ]);
                    $response = DB::table('users')->where('id', '=', $userid)->update([
                                'fcm_token' => $request->post('fcm_token')
                                ]);
                }
                date_default_timezone_set('Asia/Dubai');
                $date = date("Y-m-d H:i:s");
                $onedayafter = date("Y-m-d H:i:s", strtotime($date. ' -48 hours'));
                $notification_data = DB::select("select * from `notifications` where `notifications`.`users_id` = :users_id and `notifications`.`delivered` = 'NO' and `notifications`.`created_at` >= :onedayafter",['users_id' => $userid,'onedayafter' => $onedayafter]);
                if (!empty($notification_data)) {
                    foreach ($notification_data as $nkey => $nvalue) {
                        $msg_payload = array (
                                'mtitle' => 'Notification',
                                'mdesc' => $nvalue->message,
                        );

                        // For Android
                        $reg_id = $fcm_token;

                        $result = $this->android($msg_payload, $reg_id);                    
                    }                    
                }

                $response = DB::table('notifications')->where('users_id', '=', $userid)->update([
                    'delivered' => 'YES',
                    'updated_at' => date("Y-m-d H:i:s")
                ]);
                return response()->json([
                    'status' => TRUE, 
                    'success' => 'Successfully Logged in',
                    'key' => $key,
                    'user_data' => $user_data
                ], 200);
            } else {
                return response()->json(['status' => FALSE, 'message' => 'Invalid credentials'], 401);
            }

        }
        else {        
            return response()->json(['status' => FALSE, 'message' => 'Invalid credentials'], 401);
        }
    }
    
    /**
     * Logout a logged in user.
     *
     */
    public function logout(Request $request){
        $key = $request->post('key');
        $userkey_data = DB::select("select `userkey`.`users_id` as user_id  from `userkey` where `userkey`.`key` = :key",['key' => $key]);
        if (empty($userkey_data)) {
             return response()->json(['status' => FALSE, 'message' => 'Unauthenticated'], 401);           
        } else {
            DB::table('userkey')->where('key', '=', $key)->delete();
            $response = DB::table('notifications')->where('users_id', '=', $userkey_data[0]->user_id)->update([
                'delivered' => 'YES',
                'updated_at' => date("Y-m-d H:i:s")
            ]);
            $returnData = Array('status' => TRUE, "message" => "Successfully logged out");
            return response()->json($returnData, 200);
        }
    }
    
    /**
     * Get user profile.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile(Request $request)
    {
        $key = $request->post('key');
        $userkey_data = DB::select("select `userkey`.`users_id` as user_id  from `userkey` where `userkey`.`key` = :key",['key' => $key]);
        if (empty($userkey_data)) {
             return response()->json(['status' => FALSE, 'message' => 'Unauthenticated'], 401);           
        } else {
            $user_id = $userkey_data[0]->user_id;
            $customer_data = DB::table('users')->select('name', 'email', 'phone_number','billing_country','billing_city','billing_zip','billing_address')->where('id', '=', $user_id)->first();
            $returnData = Array('status' => TRUE, "message" => "Successfully fetched user data","data" => $customer_data);
            return response()->json($returnData, 200);
        }
    }
    
    
    /**
     * Profie update.
     *
     * @return \Illuminate\Http\Response
     */
    public function profileupdate(Request $request)
    {
        $key = $request->post('key');
        $userkey_data = DB::select("select `userkey`.`users_id` as user_id  from `userkey` where `userkey`.`key` = :key",['key' => $key]);
        if (empty($userkey_data)) {
             return response()->json(['status' => FALSE, 'message' => 'Unauthenticated'], 401);           
        } else {
            if (!$request->post('name') || !$request->post('email') || !$request->post('phone') || !$request->post('country') || !$request->post('city') || !$request->post('zip') || !$request->post('address')) {
                 return response()->json(['status' => FALSE, 'message' => 'name,email,phone,country,city,zip and address are mandatory'], 401);               
            }
            $users_id = $userkey_data[0]->user_id;
            $name = $request->post('name');
            $email = $request->post('email');
            $phone = $request->post('phone');
            $billing_country = $request->post('country');
            $billing_city = $request->post('city');
            $billing_zip = $request->post('zip');
            $billing_address = $request->post('address');


            $dupe_email = DB::table('users')->select('email')
                ->where('email', '=', $email)
                ->where('id', '!=', $users_id)->first();
            if($dupe_email) {
                return response()->json(['status' => FALSE, 'message' => 'Email already exists'], 401);
            }
            
            $data_to_update = [
                'name' => $name,
                'email' => $email,
                'phone_number' => $phone,
                'billing_city' => $billing_city,
                'billing_zip' => $billing_zip,
                'billing_country' => $billing_country,
                'billing_address' => $billing_address,
                'updated_at' => date('Y-m-d H:i:s'),
            ];

            DB::table('users')
            ->where('id', $users_id)
            ->update($data_to_update);

            $returnData = ['message' => 'successfully updated profile', 'status' => TRUE, 'data' => $data_to_update]; 
            return response()->json($returnData, 200);
        }
    }

    function generateCode($limit){
        $code = '';
        for($i = 0; $i < $limit; $i++) { 
            $code .= mt_rand(0, 9); 
        }
        return substr(str_shuffle('abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789'),0,5).$code;
    }

    function authenticateUserWithkey( $key ) {
        $userkey_data = DB::table('userkey')
            ->select('users_id')
            ->where('key', '=', $key)
            ->first();
        if (empty($userkey_data)) {

             return [ 'message' => 'Unauthenticated' ];           
        }
        $user_id = $userkey_data->users_id;
        $customer_data = DB::table('users')->select('id')->where('id', '=', $user_id)->first();
        if( ! $customer_data) {

            return [ 'message' => 'unauthenticated' ];
        }
        $user = \App\User::find($user_id);
        $roles = $user->getRoleNames();
        return [ 'message' => 'success', 'data' => $customer_data, 'user_id' => $user_id,'roles' => json_decode(json_encode($roles), true) ];
    }
    
    function show(Request $request) {
        $key = $request->post('key');
        $authenticated_user = $this->authenticateUserWithkey( $key );
        if($authenticated_user['message'] != 'success') {

            return response()->json( ['status' => FALSE, 'message' => 'Inavlid login'], 200);
        }
        $pickup_points = DB::table('boarding_pickup_map')->select(DB::raw('pickup_point as name'))->groupBy('pickup_point')->get();
        $permissions_data = $authenticated_user['roles'];
        $aColumns = array(
            0 => 'id',
            1 => 'flight_number',
            2 => 'pickup_time',
            3 => 'pickup_point',
            4 => 'status',
            5 => 'id',
            6 => 'items',
            7 => 'grant_total',
            8 => 'username'
        );
        $sWhere = '';
        if( $request->post('status_type') != '') {
            $sWhere .= $sWhere == '' ? 'WHERE ' : ' AND ';
            if( $request->post('status_type') == 'UPCOMING') {
                date_default_timezone_set('Asia/Dubai');
                // In upcoming orders show items with these statuses.
                $tomorrow = date('Y-m-d', strtotime('+1 day', strtotime(date('Y-m-d')) ) );
                $today = date('Y-m-d');
                $status_to_included = '"IN_KITCHEN", "ORDER_CONFIRMED", "READY_FOR_PICKUP"';
                
                $sWhere .= ' `order`.status in ('. $status_to_included .') '.
                ' and (  date(`order`.`order_at`)="' . $tomorrow . '" OR date(`order`.`order_at`)="' . $today . '") ';
            } else {
                $sWhere .= ' `order`.status = "'.$request->post('status_type').'"';
            }
        }
        
        if( in_array('pickup', $permissions_data) ) {
            $user_id = $authenticated_user['data']->id;
            $pickup_point = DB::table('users')->select('pickup_point', 'is_default_pickup_person')->where('id', '=', $user_id)->first();
            $p_point = $pickup_point->pickup_point ?? '';
            $is_default_pickup_person = $pickup_point->is_default_pickup_person ?? '';
            $sWhere .= $sWhere == '' ? ' WHERE ' : ' AND ';
            if( $is_default_pickup_person == 'YES' ) {
                if( $request->post('status_type') == '' || $request->post('status_type') == 'UPCOMING') {
                    // In all orders tab, display all.
                    $list_all_cond = '';
                } else {
                    $list_all_cond = ' AND ( `order`.pickup_reached_at is null OR `order`.pickup_reached_at="")';
                }
                //$sWhere .= ' `order`.status in ("IN_KITCHEN", "ORDER_CONFIRMED", "READY_FOR_PICKUP", "DELIVERED") '.$list_all_cond;
                $sWhere .= ' 1=1 '.$list_all_cond;
            } else {
                if( $request->post('status_type') == '' || $request->post('status_type') == 'UPCOMING') {
                    $list_all_cond = '';
                } else {
                    $default_grabbit_location = "At ".default_grabbit_location()['location'];
                    $list_all_cond = ' AND ( `order`.pickup_reached_at ="'.$default_grabbit_location.'") ';
                }
                // orders of pickup person's location.
                //$sWhere .= ' `order`.status in ("READY_FOR_PICKUP", "DELIVERED") AND `order`.pickup_point = "'. $p_point .'" '.$list_all_cond;
                $sWhere .= ' `order`.pickup_point = "'. $p_point .'" '.$list_all_cond;
            }
        }

        // $date_from = $request->post('date_from') ?? '';
        // $date_to = $request->post('date_to') ?? '';
        $date_from = date("Y-m-d");
        $date_to = date("Y-m-d", strtotime($date_from. ' +1 days'));
        if( $date_from) {
            $sWhere .= $sWhere == '' ? 'WHERE ' : ' AND ';
            $sWhere .= ' date(`order`.`order_at`) >="' . $date_from . '" ';
        }
        if( $date_to) {
            $sWhere .= $sWhere == '' ? 'WHERE ' : ' AND ';
            $sWhere .= ' date(`order`.`order_at`) <="' . $date_to . '" ';
        }
        
        if( $request->post('searchTerm') != '') {
            $sWhere .= $sWhere == '' ? 'WHERE ' : ' AND ';
            $sWhere .= " `order`.id LIKE '%".$request->post('searchTerm')."%' ";
        }

        $model = DB::select("select 
            `order`.id as serial_no,
            `order`.id,
            `order`.is_reimbursed,
            `order`.pickup_reached_at, 
            `order`.flight_number as flight_number, 
            `order`.order_at,
            `order`.pickup_time, 
            `order`.created_at,
            `order`.status as status, 
            `order`.pickup_point as pickup_point,
            `order`.gate,
            `order`.grant_total,
            users.name as username,
            users.phone_number as phone_number,
            users.id as user_id
        from `order`
        join users on users.id = `order`.users_id
        $sWhere ");
        $output = array(
            "aaData" => array()
        );
        foreach ( $model as $keys => $md)
        {
            $row = array();
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
                $key = $aColumns[$i];
                if($i == 3) {
                    $displayPl = $md->gate == '-' ? '-' : $md->$key;
                    $row[] = $displayPl;
                }
                else if($i == 4) {
                    $statusBadge = $md->status;
                    if($md->status == 'READY_FOR_PICKUP' && $md->pickup_reached_at != NULL) {
                        $statusBadge = $md->pickup_reached_at;
                    }
                    $row[] = ucwords( strtolower( str_replace('_', ' ', $statusBadge) ) );
                } else if( $i == 6 ) {
                    $items = DB::table('order_items')->select('item_count', 'name', 'grant_total')
                        ->join('products', 'products.id', '=', 'order_items.products_id')
                        ->where('order_id', '=', $md->id)->get();
                    $ord_items = [];
                    foreach($items as $item) {
                        $ord_items[] = [
                            'item_count' => $item->item_count,
                            'name' => $item->name,
                            'total' => $item->grant_total,
                        ];
                    }
                    $row[] =  $ord_items;
                } else {
                    if($i == 0) {
                        $row[] = "CON".str_pad($md->$key, 5, '0', STR_PAD_LEFT);
                    } else {
                        $row[] = $md->$key;
                    }
                }
            }
            $output['aaData'][] = $row;
        }
        $output['status'] = TRUE;
        $output['pickup_points'] = $pickup_points;
        
        return response()->json( $output, 200);
    }
    
    
     /**
     * Display the specified resources.
     *
     * @return \Illuminate\Http\Response
     */
    public function details(Request $request)
    {
        $key = $request->post('key');
        $authenticated_user = $this->authenticateUserWithkey( $key );
        if($authenticated_user['message'] != 'success') {

            return response()->json(['status' => FALSE, 'message' => 'Unauthenticated'], 401);
        }
        
        $pickup_points = DB::table('boarding_pickup_map')->select(DB::raw('pickup_point as name'))->groupBy('pickup_point')->get();
        
        $id = $request->post('order_id');

        $orders = DB::select("select *, o.flight_number as flight_number, o.order_at as departure, o.qr_code,
        o.freshness,o.taste,o.variety,o.ordering_process,o.comment
        from `order` o
        where o.id = :id ", ['id' => $id]);

        $user_details = DB::table('order')->select('name', 'email', 'phone_number', 'billing_name',
                            'billing_street', 'billing_street_no', 'billing_apartment_no',
                            'billing_city', 'billing_address', 'billing_country')
                        ->join('users', 'users.id', '=', 'order.users_id')
                        ->where('order.id', '=', $id)->first();
        
        $order_items = DB::select("select p.name, oi.item_count,oi.grant_total,oi.sub_total,oi.tax_total
                                    from `order` o
                                    join order_items as oi on o.id = oi.order_id
                                    join products as p on oi.products_id = p.id
                                    where o.id = :id ", ['id' => $id]);
        $invID = str_pad($id, 5, '0', STR_PAD_LEFT);
        $invID = "CON".$invID;
        $pickup_time = '';  
        $flight_date = $orders[0]->departure;
        $flight_slotes = DB::select("select `flight_slots`.`time_from`,`flight_slots`.`time_to`,`flight_slots`.`pickup_schedule`,`flight_slots`.`last_order`,`flight_slots`.`in_kitchen` from `flight_slots`");
        foreach ($flight_slotes as $key => $value) {
            $date = date('Y-m-d', strtotime($flight_date));
            $time_from = $date." ".$value->time_from;
            $time_to = $date." ".$value->time_to;
            $pickup_schedule = $date." ".$value->pickup_schedule;
            $flight_date = date('Y-m-d H:i:s', strtotime($flight_date));
            if ($time_from <= $flight_date && $time_to >= $flight_date) {
                $pickup_time = $pickup_schedule;
            }
        }

        return response()->json([
            'status' => TRUE, 
            'success' => 'Data Fetched Successfull',
            'orders' => $orders, 
            'order_items' => $order_items, 
            'order_id' => $id,
            'customer_details' => $user_details,
            'pickup_time' => $pickup_time,
            'pickup_points' => $pickup_points,
            //'user_pickup_point' =>$pickup_location
        ], 200);

    }
    
   
    function findGate($gateFind) {
        $gate2Find = strtolower( $gateFind );
        if (strpos($gate2Find, 'grabbit') !== false) {
            return $gateFind . ': Gate B7';
        }
        if (strpos($gate2Find, 's34') !== false) {
            return $gateFind . ': Gate C36';
        }
        if (strpos($gate2Find, 'nutella') !== false) {
            return $gateFind . ': Gate B28';
        }
        if (strpos($gate2Find, 'fix') !== false) {
            return $gateFind . ': Gate A17';
        }
        if (strpos($gate2Find, 'tree') !== false) {
            return $gateFind . ': Gate A12';
        }

        return '';
    }
    
    function findGateReminder($gateFind) {
        $gate2Find = strtolower( $gateFind );
        if (strpos($gate2Find, 'grabbit') !== false) {
            return 'B7';
        }
        if (strpos($gate2Find, 's34') !== false) {
            return 'C36';
        }
        if (strpos($gate2Find, 'nutella') !== false) {
            return 'B28';
        }
        if (strpos($gate2Find, 'fix') !== false) {
            return 'A17';
        }
        if (strpos($gate2Find, 'tree') !== false) {
            return 'A12';
        }

        return '';
    }
    
    function sendSMS($message, $phonenumber) {
        $message = urlencode($message);
        preg_match_all('!\d+!', $phonenumber, $matches);
        $phone = implode('',$matches[0]);
        $url = "http://mshastra.com/sendurlcomma.aspx?user=20093644&pwd=Emirates321!&senderid=CarryonDXB&mobileno=".$phone."&msgtext=".$message."&priority=High&CountryCode=ALL";



        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $curl_scraped_page = curl_exec($ch);
        curl_close($ch);
        // echo $curl_scraped_page;
    }
    
    function sendMail($message, $email) {
        try {
            $data = ['from_name' => 'CARRYON', 'from_email' => 'noreply@carryon.com', 'subject' => 'Your CarryOn is ready!', 'message1' => $message, 'message2' => ''];

			Mail::to($email)->send(new TestEmail($data));
        }catch (\Exception $e) {
            print_r($e->getMessage());
        }
    }
    
    public function link_store($long_link)
    {
        $input['link'] = $long_link;
        $input['code'] = Str::random(6);
   
        ShortLink::create($input);
  
        return $input['code'];
    }
    
    /**
     * Order report pdf.
     *
     * @return \Illuminate\Http\Response
     */
    public function pdfreport(Request $request)
    {
        $key = $request->post('key');
        $authenticated_user = $this->authenticateUserWithkey( $key );
        if($authenticated_user['message'] != 'success') {

            return response()->json(['status' => FALSE, 'message' => 'Unauthenticated'], 401);
        }
        $orderid = $request->post('order_id');
        $order = DB::table('order')->where('id', '=', $orderid)->first();
        if ($order === null) {
            return response()->json(['status' => FALSE, 'message' => 'Order not found'], 401);
        }

        $result = DB::select('select `order`.`id` as orderid, '
                   .'`order_items`.`id` as orderitemid, '
                   . '`order`.`item_count` as item_count_total,'
                   . '`order`.`created_at` as created_at,'
                   . '`order`.`flight_number` as flight_no,'
                   . '`order`.pickup_point as boarding_point,'
                   . '`order`.`status` as order_status,'
                   . ' `order`.`grant_total` as grant_total,'
                   . ' `order`.`sub_total` as sub_total,'
                   . ' `order`.`tax_total` as tax_total,'
                   . ' `order`.`order_at` as order_at,'
                   . ' `order`.`users_id` as users_id,'
                   . ' `order`.`pickup_time` as pickup_time,'
                   . ' `order`.`qr_code` as qr_code,'
                   . ' order_items.item_count as item_count,'
                   . ' order_items.products_id as products_id,'
                   . ' order_items.grant_total as grant_item_price,'
                   . ' order_items.sub_total as sub_item_price,'
                   . ' order_items.tax_total as tax_item_price,'
                   . ' products.name as productname,'
                   . ' products.description as productdescription,'
                   . ' product_images.path as productthumbnail'
                   . ' from `order` join order_items on `order`.`id` = order_items.order_id'
                   . ' join products on `products`.`id` = order_items.products_id '
                   . ' left join product_images on `products`.`id` = product_images.products_id and product_images.type = "THUMBNAIL"'
                   . 'where `order`.`id` = :orderid', ['orderid' => $orderid ]);
            $modifiedResult = Array();
            if (!empty($result)) {
                $i = 0;
                foreach ($result as $key => $value) {
                    if ($i == 0) {
                        $modifiedResult = Array(
                            'qr_code' => $value->qr_code,
                            'order_id' => $value->orderid,
                            'users_id' => $value->users_id,
                            'created_at' => $value->created_at,
                            'order_at' => $value->order_at,
                            'flight_no' => $value->flight_no,
                            'boarding_point' => $value->boarding_point,
                            'order_status' => str_replace("_", " ", $value->order_status),
                            'item_count_total' => $value->item_count_total,
                            'grant_total' => decimal_format_custom($value->grant_total),
                            'sub_total' => decimal_format_custom($value->sub_total),
                            'tax_total' => decimal_format_custom($value->tax_total),
                            'pickup_time' => $value->pickup_time    
                        );
                    }
                    $modifiedResult['items'][$value->orderitemid] = Array(
                        'item_count' => $value->item_count,
                        'unit_item_price' => $value->grant_item_price / $value->item_count,
                        'grant_item_price' => decimal_format_custom($value->grant_item_price),
                        'sub_item_price' => decimal_format_custom($value->sub_item_price),
                        'tax_item_price' => decimal_format_custom($value->tax_item_price),
                        'products_id' => $value->products_id,
                        'productname' => $value->productname,
                        'productthumbnail' => empty($value->productthumbnail)?$value->productthumbnail:url($value->productthumbnail),
                        'productdescription' => $value->productdescription
                    );
                    $i++;
                } 
            }
        $content = '';    
        if (!empty($modifiedResult)) {
            foreach ($modifiedResult['items'] as $key => $value) {
                $content .= '<tr>
                                <td class="description">'.$value["productname"].'</td>
                                <td class="quantity">'.$value["item_count"].'</td>
                                <td class="price" style="text-align:right;">'.$value["grant_item_price"].'</td>
                            </tr>';
            }
        }   
        $user_id = $modifiedResult['users_id'];
            
        $user_data = DB::table('users')->select('phone_number','email','name','billing_address')->where('id', '=', $user_id)->first();
        $invID = str_pad($orderid, 5, '0', STR_PAD_LEFT);
        $invID = "CON".$invID; 
        $order_at = $modifiedResult['order_at']??''; 
        if (!empty($order_at)) {
            $date = date_create($order_at);
            $order_at = date_format($date,"d-M-Y");
        }
        
        $custome_date = date_format_from_month_custom($modifiedResult['pickup_time']);
        $custome_date_array = explode(" ", $custome_date);
        if (!empty($md->$key)) {
            $dispatch_time = $custome_date_array[0]." ".$custome_date_array[1]." <b>".$custome_date_array[2]."</b>";
        } else {
            $dispatch_time = $custome_date;
        }
        
        $data = ['title' => 'Welcome to carryon','invID' => $invID,'customername' => $user_data->name,
            'qr_code' => $modifiedResult['qr_code'],
            'content' => $content, 'total' => $modifiedResult['grant_total'],
            'order_at' => $order_at, 'billing_address' => $user_data->billing_address,
            'pickup_loc' => $modifiedResult['boarding_point'], 'flight_no' => $modifiedResult['flight_no'], 'dispatch_time' => $dispatch_time];
        $customPaper = array(0,0,567.00,283.80);
        $pdf = PDF::loadView('pdf.invoice', $data)->setPaper($customPaper, 'landscape');
  
        // return $pdf->download('carryon' . time() . '.pdf');
        $folder_structure = date('Y')."/".date('m')."/".date('d');
        $target_dir = "./public/upload/".$folder_structure;
        $target_dir_path = "./upload/".$folder_structure;
        if (!file_exists($target_dir)) {
            mkdir($target_dir, 0777, true);
        }
        $file = $target_dir.'/invoice_'.$orderid.'.pdf';
        $filepath = $target_dir_path.'/invoice_'.$orderid.'.pdf';
        $pdf->save($file);
        return response()->json([
            'status' => TRUE, 
            'success' => 'Invoice created',
            'filepath' => url($filepath)
        ], 200);
           
    }
    
    /**
     * Password change api.
     *
     */
    public function resetpassword(Request $request){
        $key = $request->post('key');
        $authenticated_user = $this->authenticateUserWithkey( $key );
        if($authenticated_user['message'] != 'success') {

            return response()->json(['status' => FALSE, 'message' => 'Unauthenticated'], 401);
        }
        
        $userkey_data = DB::table('userkey')
            ->select('users_id')
            ->where('key', '=', $key)
            ->first();
        
        $user_id = $userkey_data->users_id;
        $users_data = DB::table('users')->select('email')->where('id', '=', $user_id)->first();
        
        $uname = $users_data->email;
        if (!$request->post('current_password') || !$request->post('new_password')) {
             return response()->json(['status' => FALSE, 'message' => 'current_password and new_password are mandatory'], 401);               
        }
        
        $current_password = $request->post('current_password');
        $new_password = $request->post('new_password');
        if (Auth::attempt(array('email' => $uname, 'password' => $current_password))){
            $password_hashed = Hash::make($new_password);
            DB::table('users')
            ->where('id', $user_id)
            ->update([
                'password' => $password_hashed,
                'remember_token' => $new_password,
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
            return response()->json([
                'status' => TRUE, 
                'success' => 'Successfully updated password'
            ], 200);
        }
        else {        
            return response()->json(['status' => FALSE, 'message' => 'Invalid credentials'], 401);
        }
    }
    
   
    function notification_add ($user_id, $message, $title, $fcm_token, $order_id) {
        $notifications = new Notifications;
        $notifications->users_id = $user_id;
        $notifications->order_id = $order_id;
        $notifications->message = $message;
        $notifications->created_at = date("Y-m-d H:i:s");
        $notifications->updated_at = date("Y-m-d H:i:s");
        $notifications->save(); 
        // Message payload
        $msg_payload = array (
                'mtitle' => $title,
                'mdesc' => $message,
        );

        // For Android
        $reg_id = $fcm_token;

        $result = $this->android($msg_payload, $reg_id);
    }
    
    /**
     * Get notification list.
     *
     */
    public function notification_list(Request $request){
        $key = $request->post('key');
        $authenticated_user = $this->authenticateUserWithkey( $key );
        if($authenticated_user['message'] != 'success') {

            return response()->json(['status' => FALSE, 'message' => 'Unauthenticated'], 401);
        }
        
        $userkey_data = DB::table('userkey')
            ->select('users_id')
            ->where('key', '=', $key)
            ->first();
        
        $user_id = $userkey_data->users_id;
        date_default_timezone_set('Asia/Dubai');
        $date = date("Y-m-d H:i:s");
        $onedayafter = date("Y-m-d H:i:s", strtotime($date. ' -48 hours'));
        $notification_data = DB::select("select * from `notifications` where `notifications`.`users_id` = :users_id and `notifications`.`created_at` >= :onedayafter",['users_id' => $user_id,'onedayafter' => $onedayafter]);
        return response()->json([
            'status' => TRUE, 
            'data' => $notification_data,
            'success' => 'Notifications list'
        ], 200);

    }
    
    function formatSeconds( $seconds )
    {
        // if($milliseconds > 0) {
        //     $seconds = $milliseconds / 1000;
        // }
        $hours = 0;
        $milliseconds = str_replace( "0.", '', $seconds - floor( $seconds ) );

        if ( $seconds > 3600 )
        {
            $hours = floor( $seconds / 3600 );
        }
        $seconds = $seconds % 3600;


        return str_pad( $hours, 2, '0', STR_PAD_LEFT )
            . gmdate( ':i', $seconds )
        ;
    }
    
    public function categories(){
        $categories = DB::table('category')
                    ->select('id', 'name','description','visibility', 'created_at','updated_at','deleted_at','position','image_path','alternative_image_path')
                    ->where('deleted_at', NULL)
                    ->orderBy('position', 'asc')
                    ->get();
                    //->paginate(10);
        if (!empty($categories)) {
            foreach ($categories as $key => $value) {
                $value->image_path = url($value->image_path);
                $value->alternative_image_path = url($value->alternative_image_path);
                $categories[$key] = $value;
            }
        }
        
        return response()->json([
                'status' => TRUE, 
                'message' => 'Categories list fetched',
                'categories' => $categories
        ], 200);
    }
    
    public function preferenceoptions(){
        $diets = DB::table('preferenceoptions')
                    ->select('id', 'name', 'original_image', 'alternative_image')
                    ->where('deleted_at', NULL)
                    ->orderBy('created_at', 'desc')
                    ->where('type', 'Diet')
                    ->get();
                    //->paginate(10);
        $allergies = DB::table('preferenceoptions')
                    ->select('id', 'name', 'original_image','alternative_image')
                    ->where('deleted_at', NULL)
                    ->orderBy('created_at', 'desc')
                    ->where('type', 'Intolerence')
                    ->get();
        
        foreach($diets as $key => $diet){
            if (!empty($diet->original_image)) $diets[$key]->original_image = url('storage/'.$diet->original_image);
            else $diets[$key]->original_image = null;
            if (!empty($diet->alternative_image)) $diets[$key]->alternative_image = url('storage/'.$diet->alternative_image);
            else $diets[$key]->alternative_image = null;
        }

        foreach($allergies as $key => $allergy){
            if (!empty($allergy->original_image)) $allergies[$key]->original_image = url('storage/'.$allergy->original_image);
            else $allergies[$key]->original_image = null;
            if (!empty($allergy->alternative_image)) $allergies[$key]->alternative_image = url('storage/'.$allergy->alternative_image);
            else $allergies[$key]->alternative_image = null;
        }
        
        return response()->json([
                'status' => TRUE, 
                'message' => 'Preference options fetched',
                'diets' => $diets,
                'allergies' => $allergies
        ], 200);
    }
    
    
    public function featured_products(Request $request){
        $offset = $request->post('offset');
        $products = DB::table('products')
                    ->select('products.id as id','products.name as name','products.sku as sku','products.actual_price as actual_price'
                            ,'products.discount_price as discount_price','products.created_at as created_at','products.url_key as url_key'
                            ,'products.description as description','products.position as position','products.visibility as visibility'
                            ,'product_images.id as image_id','product_images.path as image_path','product_images.type as image_type'
                            ,'allergies.id as allergies_id','allergies.name as allergies_name','allergies.description as allergies_description','allergies.image_path as allergies_image_path')
                    ->leftjoin('product_images', 'products.id', '=', 'product_images.products_id')
                    ->leftjoin('product_allergies', 'products.id', '=', 'product_allergies.products_id')
                    ->leftjoin('allergies', 'allergies.id', '=', 'product_allergies.allergies_id')
                    ->where('featured', 'YES')
                    ->where('products.deleted_at', NULL)
                    ->where('products.visibility', '=', "YES")
                    ->orderBy('products.position', 'asc')
//                    ->offset($offset)
//                    ->limit(3)
                    ->get();
                    //->paginate(10);
        $productArray = array();
        if (!empty($products)) {
            foreach ($products as $key => $value) {
                $productArray[$value->id]['id'] = $value->id;
                $name = $value->name;
                $productArray[$value->id]['name'] = $name;
                $productArray[$value->id]['sku'] = $value->sku;
                $productArray[$value->id]['actual_price'] = $value->actual_price;
                $productArray[$value->id]['discount_price'] = $value->discount_price;
                $productArray[$value->id]['created_at'] = $value->created_at;
                $productArray[$value->id]['url_key'] = $value->url_key;
                $productArray[$value->id]['description'] = $value->description;
                $productArray[$value->id]['position'] = $value->position;
                $productArray[$value->id]['visibility'] = $value->visibility;
                
                if (!empty($value->image_id)) {
                    $productArray[$value->id]['images'][$value->image_type][$value->image_id]['image_id'] = $value->image_id;
                    $productArray[$value->id]['images'][$value->image_type][$value->image_id]['image_path'] = url($value->image_path);
                }
                
           
                if (!empty($value->allergies_id)) {                                                                                                                                             
                    $productArray[$value->id]['allergies'][$value->allergies_id]['allergies_id'] = $value->allergies_id;
                    $productArray[$value->id]['allergies'][$value->allergies_id]['name'] = $value->allergies_name;
                    $productArray[$value->id]['allergies'][$value->allergies_id]['description'] = $value->allergies_description;
                    $productArray[$value->id]['allergies'][$value->allergies_id]['image_path'] = url($value->allergies_image_path);
                }
            }
            
            $productArray = array_slice($productArray,$offset,3);
            $finalProductArray = array();
            foreach ($productArray as $key => $value) {
                if (isset($value['images'])) {
                    foreach ($value['images'] as $vkey => $vvalue) {
                        foreach ($vvalue as $vvkey => $vvvalue) {
                            $value['images_list'][$vkey][] = $vvvalue;
                        }
                    }
                    unset($value['images']);
                }
                if( ! isset($value['images_list']['THUMBNAIL']) ) {
                    $value['images_list']['THUMBNAIL'][0]['image_id'] = 0;
                    $value['images_list']['THUMBNAIL'][0]['image_path'] = url('default_image/thumbnail.png');
                }
                if (isset($value['allergies'])) {
                    foreach ($value['allergies'] as $vkey => $vvalue) {
                        $value['allergies_list'][] = $vvalue;
                    }
                    unset($value['allergies']);
                }
                $finalProductArray[] = $value;
            }
        }
        
        return response()->json([
                'status' => TRUE, 
                'message' => 'Categories list fetched',
                'products' => $finalProductArray,
                // 'products_before' => $products
        ], 200);
    }
    
    public function flightshow_static(Request $request)
    {
        $flight = $request->post('fl_no');
        //$date = $request->get('date');
        $date = '03/24/2020';
        if( ! $flight || ! $date) {
            return response()->json([ 'status' => FALSE, 'message' => 'Please provide date and flight number' ], 401);
        }
            $date = date("Y-m-d H:i:s");
            $onedayafter = date("Y-m-d", strtotime($date. ' +24 hours'));
            //$results['scheduledFlights'][0]['departureTime'] = '2020-03-26T07:45:00.000';
            $departureTime = $onedayafter.'T07:45:00.000';
            $duration = $this->formatSeconds(7*60*60);

            $destin_city = 'London';
            return response()->json([
                    'status' => TRUE, 
                    'message' => 'Flight data fetched',
                    'flight' => $flight,
                    'departure' => date('h:i A', strtotime($departureTime) ),
                    'duration' => $duration,
                    'destination' => $destin_city
            ], 200);

    }
    
    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function flightshow(Request $request)
    {

        $flight = $request->post('fl_no');
        $date = $request->post('date');
        if( ! $flight || ! $date) {
            return response()->json([ 'status' => FALSE, 'message' => 'Please provide date and flight number' ], 401);
        }
//        $dateSplitted = explode('/', $date);
        $dateSplitted = explode('-', $date);
//        $day = $dateSplitted[1];
//        $month = $dateSplitted[0];
//        $year = $dateSplitted[2];
        $day = $dateSplitted[0];
        $month = $dateSplitted[1];
        $year = $dateSplitted[2];
        $flight_iata = substr($flight, 0, 2);
        $flight_code = substr($flight, 2, 4);
        $url = "https://api.flightstats.com/flex/schedules/rest/v1/json/flight/$flight_iata/$flight_code/departing/$year/$month/$day?appId=8a5a55e4&appKey=b3e7538d084234f6f83779bdfbd671ed";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
        $results = curl_exec($ch);

        $results = json_decode($results, true);
        // Error from API side.
        if( isset( $results['error']['errorMessage'] ) ) {
//            return response()->json(['status' => FALSE, 'message' => explode('.', $results['error']['errorMessage'])[0]], 401);
            return response()->json(['status' => FALSE, 'message' => 'Flight details not found'], 401);
        } 
        else if( isset($results['scheduledFlights']) && $results['scheduledFlights'] ) {
            // Check departure from DXB.
            if( ! isset( $results['scheduledFlights'][0]['departureAirportFsCode'] ) || $results['scheduledFlights'][0]['departureAirportFsCode'] != 'DXB'  ) {
                return response()->json(['status' => FALSE, 'message' => 'We’re currently operating only from DXB, Terminal 3. Stay tuned for updates!'], 401);
            }
            // Check departure from Terminal 3.
            if( ! isset( $results['scheduledFlights'][0]['departureTerminal'] ) || $results['scheduledFlights'][0]['departureTerminal'] != 3) {
                return response()->json(['status' => FALSE, 'message' => 'We’re currently operating only from DXB, Terminal 3. Stay tuned for updates!'], 401);
            }
            $diffOfRegions = 0;
            $diffVariantTimeRegionDiff = 0;
            /*
             Below code to find journey duration.
             Local current time of arrvl - local current time of departure + diff of arrvl and dep.
             */
            if( isset( $results['appendix']['airports'] ) ) {
                $dataAirports = $results['appendix']['airports'];
                if( isset($dataAirports[1]['localTime']) && isset($dataAirports[0]['localTime']) ) {
                    $diffOfRegions = strtotime($dataAirports[0]['localTime']) - strtotime($dataAirports[1]['localTime']);
                }
                $diffVariantTimeRegionDiff = strtotime($results['scheduledFlights'][0]['arrivalTime']) - strtotime($results['scheduledFlights'][0]['departureTime']);
            }
            $actualDiff = $diffVariantTimeRegionDiff + $diffOfRegions;
            $duration = $this->formatSeconds($actualDiff);
            // Duration find code ends.
            $departureTime = date('H:i', strtotime($results['scheduledFlights'][0]['departureTime']) );
            $slot = DB::table('flight_slots')->select('last_order')
                    ->where('time_from', '<=', $departureTime)
                    ->where('time_to', '>=', $departureTime)
                    ->first();
            date_default_timezone_set('Asia/Dubai');
            $localDXB_time = date('Y-m-d H:i');
            // Find date of departure
            $dateOfDeparture = date('Y-m-d', strtotime($results['scheduledFlights'][0]['departureTime']) );
            // Find possible last order time. (Removed H:i:s of departure and appended last order time from slot to dep time).
            if(strtotime($slot->last_order) >= strtotime('18:00:00')) {
                // Last possible order date is prevoius day for order time upto 05:59 AM.(its last_order time wll b [18:00:00, 23:00:00])
                $dateOfDeparture = date('Y-m-d', strtotime('-1 day', strtotime($dateOfDeparture)));
            }
            $possibleMaxOrderTime = date('Y-m-d H:i', strtotime( $dateOfDeparture . ' ' . $slot->last_order ) );
            // Check order possibility with time slot.
            if(strtotime($localDXB_time) > strtotime($possibleMaxOrderTime)) {
                $lt_time = explode(':', str_replace(':00', '', $slot->last_order));
                $lt_time = $lt_time[0] > 12 ? ($lt_time[0] -12 . ' PM') : ($lt_time[0] . ' AM');
                $departed = FALSE;
                if( strtotime($localDXB_time) > strtotime($results['scheduledFlights'][0]['departureTime']) ) {
                    $departed = TRUE;
                }
                return response()->json( [ 
                    'status' => FALSE, 
                    'message' => 'time elapsed',
                    'departed' => $departed,
                    'last_order' => '<i>'.date('d M Y', strtotime($dateOfDeparture)). ' ' .str_replace(':00', '', $lt_time).'</i>',
                    'test' => $slot->last_order
                ] , 401);
            }
            // Success
//            $request->session()->put('flight_number', $flight);
//            $request->session()->put('flight_date', $results['scheduledFlights'][0]['departureTime']);
//            $request->session()->put('flight_id', $flight);
//            $request->session()->put('flight_duration', $duration);
//            $request->session()->put('flight_terminal', 3);
//            $request->session()->save();
            if($results['appendix']['airports'][0]['iata'] == 'DXB') {
                $destin_city = $results['appendix']['airports'][1]['city'] ?? $results['scheduledFlights'][1]['arrivalAirportFsCode'];
            } else {
                $destin_city = $results['appendix']['airports'][0]['city'] ?? $results['scheduledFlights'][0]['arrivalAirportFsCode'];
            }

            return response()->json([ 
                    'status' => TRUE, 
                    'message' => 'Flight data fetched',
                    'flight' => $flight,
                    'departure' => date('h:i A', strtotime($results['scheduledFlights'][0]['departureTime']) ),
                    'duration' => $duration,
                    'destination' => $destin_city,
                ], 200);
        } else {
            return response()->json( [ 
                'status' => FALSE, 
                'message' => 'Flight details not found'
            ] , 401);
        }
        return response()->json($results, 200);
    }
    
    /**
     * Profie update.
     *
     * @return \Illuminate\Http\Response
     */
    public function preferenceupdate(Request $request)
    {
        $key = $request->post('key');
        $userkey_data = DB::select("select `userkey`.`users_id` as user_id  from `userkey` where `userkey`.`key` = :key",['key' => $key]);
        if (empty($userkey_data)) {
             return response()->json(['status' => FALSE, 'message' => 'Unauthenticated'], 401);           
        } else {
            if (!$request->post('agegroup') || !$request->post('gender')) {
                 return response()->json(['status' => FALSE, 'message' => 'agegroup and gender are mandatory'], 401);               
            }
            $users_id = $userkey_data[0]->user_id;
            $agegroup = $request->post('agegroup');
            $gender = $request->post('gender');
            $user_diets = json_decode($request->post('user_diets'), true);
            $user_intolerances = json_decode($request->post('user_intolerances'), true);

           
            $data_to_update = [
                'pref_age_group' => $agegroup,
                'pref_gender' => $gender,
                'updated_at' => date('Y-m-d H:i:s'),
            ];

            DB::table('users')
            ->where('id', $users_id)
            ->update($data_to_update);
            DB::table('users_preferenceoptions')->where('users_id', $users_id)->delete();
            $diet_data = [];
            if($user_diets) {
                foreach($user_diets as $diets_id) {
                    $diet_data[] = [
                        'users_id' => $users_id,
                        'preferenceoptions_id' => $diets_id,
                        'type' => 'Diet'
                    ];
                }
                $allrg = DB::table('users_preferenceoptions')->insert( $diet_data );
            }
            
            
            // Below product_intolerances
            $intolerance_data = [];
            if($user_intolerances) {
                foreach($user_intolerances as $intolerances_id) {
                    $intolerance_data[] = [
                        'users_id' => $users_id,
                        'preferenceoptions_id' => $intolerances_id,
                        'type' => 'Intolerence'
                    ];
                }
                $allrg = DB::table('users_preferenceoptions')->insert( $intolerance_data );
            }

            $returnData = ['message' => 'successfully updated user preferences', 'status' => TRUE, 'data' => $data_to_update]; 
            return response()->json($returnData, 200);
        }
    }


    public function productdetail(Request $request){
        $product_id = $request->post('prdt_itm_id');
        $ingredientsAll = DB::select('select i.id, i.name, i.description, i.image_path, i.position from incredience as i 
                          left join  product_incredience as pi on i.id = pi.incredience_id
                          left join  products as p on p.id = pi.products_id
                          where  p.id = "'.$product_id.'" ORDER BY i.name ASC');
        $ingredients = [];
        foreach( $ingredientsAll as $key => $incr) {
            $incr->image_path = url( $incr->image_path );
            $ingredients[] = $incr;
        }

        $cart_sess_key = $request->post('session_cart_key');
        $cartid = '';
        if($cart_sess_key) {
            $cartkey = DB::select("select id from cartkey where `key` = :cartkey ", ['cartkey' => $cart_sess_key]);
            if ( ! empty($cartkey) ) {
                $cartid = $cartkey[0]->id;
            }
        }
        if( $cartid) { // This item may be in cart.
            $product_item = Products::select('id', DB::raw('0 as item_count'))->where('id', '=', $product_id)->first();
            $cart_item = DB::table('cart_items')
            ->select('item_count')
            ->where('cart_id', '=', $cartid)
            ->where('products_id', '=', $product_item->id)->first();
            if($cart_item) {
                $product_item->item_count = $cart_item->item_count;
            }
        } else { // No items in cart.
            $product_item = Products::select('id', DB::raw('0 as item_count'))->where('id', '=', $product_id)->first();
        }
        $product_category = DB::table('category')
            ->join('product_category', 'product_category.category_id', '=', 'category.id')
            ->select('category.id', 'category.name')
            ->where('product_category.products_id', '=', $product_item->id)->first();   
            
        $categories = DB::select('select id, name, description from category where visibility = "YES" ');
        $product_allergy_map = array();
        $products = DB::select('select distinct p.id, p.name, p.actual_price, p.description, 
                                    pi.type, pi.path, pc.category_id as category_id, p.base_content 
                                    from products as p
                                    left join  product_category as pc on p.id = pc.products_id
                                    left join product_images as pi on p.id = pi.products_id 
                                    where pi.type = :img_type and p.id = :product_id ', [
                                        'product_id' => $product_item->id, 
                                        'img_type' => 'IMAGE'
                                    ]);
 
        $allergies = DB::select('select distinct pa.products_id, a.name, a.description, a.image_path from product_category as pc 
                                    left join product_allergies as pa on pa.products_id = pc.products_id
                                    left join allergies as a on pa.allergies_id = a.id 
                                    where  pa.products_id = :product_id and a.visibility = "YES"', ['product_id' => $product_item->id]);
        
        $category = DB::select('select id, name from category');
        $base_content_obj = DB::select('select page_title, html_content from cms where visibility = "YES" and short_code = "app_confirmpage_title" and deleted_at IS NULL');
        $base_content = ( ! empty($base_content_obj[0]->page_title) )?$base_content_obj[0]->page_title:"Goes well with..";
        // ini_set("allow_url_fopen", 1);
        foreach ($products as $key => $product){
            $product_allergy_map[$key]['product_id'] = $product_item->id;
            $product_allergy_map[$key]['item_count'] = $product_item->item_count;
            $product_allergy_map[$key]['product_name'] = $product->name;
            $product_allergy_map[$key]['actual_price'] = $product->actual_price;
            if (empty($product->path)) {
                $product_allergy_map[$key]['product_image_path'] = url('default_image/thumbnail.png');                
            } else {
                $product_allergy_map[$key]['product_image_path'] = url($product->path);
            }
            $product_allergy_map[$key]['base_content'] = $base_content;

            $path = "public".$product->path;
            if (!file_exists($path)) {   
                $path = public_path($product->path); // to work with localhost.
            }

            list($width, $height) = getimagesize($path);
            $product_allergy_map[$key]['height'] = $height;
            $product_allergy_map[$key]['width'] = $width;

            $src = $path;
            $target_dir = public_path("/app_product_thump/product_images");
            if (!file_exists($target_dir)) {
                mkdir($target_dir, 0777, true);
            }
            $dest = public_path("/app_product_thump" . $product->path);
            $desired_width="200";
            $this->make_thumb($src, $dest, $desired_width);
            $product_allergy_map[$key]['thump_generated_image_path'] = url("/app_product_thump" . $product->path);

            $product_allergy_map[$key]['description'] = $product->description;
            $product_allergy_map[$key]['type'] = $product->type;
            $product_allergy_map[$key]['category_id'] = $product->category_id;
            foreach($allergies as  $allergy){
                if ($product->id == $allergy->products_id) {
                    $allergy->image_path = url( $allergy->image_path );
                    $product_allergy_map[$key]['allergies'][] = $allergy;
                }
            }
        }
        
        $relatedProductsIdsArray = DB::select('select pr.product_rel_id as prid from product_relation as pr 
                                        where pr.products_id = "' . $product_id . '"');
        $prIdArray = array();
        foreach ($relatedProductsIdsArray as $key => $value) {
            $prIdArray[] = $value->prid;
        }

        $inPrIds = implode(',', $prIdArray);
        if (empty($inPrIds)) {
            $inPrIds = 0;
        }
        
        $relatedproducts_allergy_map = array();
        $relatedproducts = DB::select('select distinct p.id, p.name, p.actual_price, p.url_key, pi.type, pi.path from products as p 
                                    left join  product_category as pc on p.id = pc.products_id
                                    left join product_images as pi on p.id = pi.products_id 
                                    where  p.id in (' . $inPrIds . ') AND pi.type = "THUMBNAIL" AND p.id != "'.$product->id.'" LIMIT 3');
        
//        $relatedproducts = DB::select('select distinct p.id, p.name, p.actual_price, p.url_key, pi.type, pi.path from products as p 
//                                    left join  product_category as pc on p.id = pc.products_id
//                                    left join product_images as pi on p.id = pi.products_id 
//                                    where  pc.category_id = "'.$product_category->id.'" AND pi.type = "THUMBNAIL" AND p.id != "'.$product->id.'" LIMIT 3');
        
        $relateproductsdallergies = DB::select('select distinct pa.products_id, a.name, a.description, a.image_path from product_category as pc 
                    left join product_allergies as pa on pa.products_id = pc.products_id
                    left join allergies as a on pa.allergies_id = a.id 
                    where  pa.products_id != :product_id and a.visibility = "YES"', ['product_id' => $product->id]);
    
        foreach ($relatedproducts as $key => $relatedproduct){
            $relatedproducts_allergy_map[$key]['product_id'] = $relatedproduct->id;
            $relatedproducts_allergy_map[$key]['name'] = $relatedproduct->name;
            $relatedproducts_allergy_map[$key]['actual_price'] = $relatedproduct->actual_price;
            $relatedproducts_allergy_map[$key]['path'] = url($relatedproduct->path);
            $relatedproducts_allergy_map[$key]['type'] = $relatedproduct->type;
            $relatedproducts_allergy_map[$key]['url_key'] = 'productdetail/'.$relatedproduct->url_key;
            foreach($relateproductsdallergies as  $relateproductsdallergy){
                if ($relatedproduct->id == $relateproductsdallergy->products_id){
                    $relateproductsdallergy->image_path = url($relateproductsdallergy->image_path);
                    $relatedproducts_allergy_map[$key]['allergies_list'][] = $relateproductsdallergy;
                }
            }
        }

        return response()->json(
            array(
                'status' => TRUE,
                'ingredients' => $ingredients, 
                'productdetails' => $product_allergy_map, 
                'productallergies' => $product_allergy_map[0]['allergies'] ?? [],
                'categories' => $category, 
                'product_category_id' => $product_category->id,
                'product_category_name' => $product_category->name,
                'relatedproducts' => $relatedproducts_allergy_map,
                'addons' => $this->product_addon_list( $product_id )
            ) , 200);
    }

    private function make_thumb($src, $dest, $desired_width) {

        /* read the source image */
        $source_image = imagecreatefromjpeg($src);
        $width = imagesx($source_image);
        $height = imagesy($source_image);
    
        /* find the "desired height" of this thumbnail, relative to the desired width  */
        $desired_height = floor($height * ($desired_width / $width));
    
        /* create a new, "virtual" image */
        $virtual_image = imagecreatetruecolor($desired_width, $desired_height);
    
        /* copy source image at a resized size */
        imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);
    
        /* create the physical thumbnail image to its destination */
        imagejpeg($virtual_image, $dest);
    }

    private function product_addon_list( $products_id )
    {
        if (empty($products_id)) {
            return [];
        }
        $addons_data = DB::select("select a.id, apm.products_id, a.name, a.actual_price, a.position " .
                                " from `addons_product_mapping` apm".
                                " join addons a on a.id = apm.addons_id " .
                                " where `apm`.`products_id` = :products_id and a.visibility='YES' ",
                                ['products_id' => $products_id]);
        return $addons_data;
    }

    public function suggested_for_you_products(Request $request){

        $age_pref = $request->post('age');
        $diets = $request->post('diets');
        $allergies = $request->post('allergies');

        $offset = $request->post('offset');
        $products = DB::table('products')
                    ->select('products.id as id', 
                    'product_preferenceoptions.type as pref_type', 'product_preferenceoptions.preferenceoptions_id',
                    'products.name as name','products.sku as sku','products.actual_price as actual_price'
                            ,'products.discount_price as discount_price','products.created_at as created_at','products.url_key as url_key'
                            ,'products.description as description','products.position as position','products.visibility as visibility'
                            ,'product_images.id as image_id','product_images.path as image_path','product_images.type as image_type'
                            ,'allergies.id as allergies_id','allergies.name as allergies_name','allergies.description as allergies_description','allergies.image_path as allergies_image_path')
                    ->leftjoin('product_images', 'products.id', '=', 'product_images.products_id')
                    ->leftjoin('product_allergies', 'products.id', '=', 'product_allergies.products_id')
                    ->leftjoin('allergies', 'allergies.id', '=', 'product_allergies.allergies_id')

                    ->leftjoin('product_preferenceoptions', 'products.id', '=', 'product_preferenceoptions.products_id')

                    // ->where('featured', 'YES')
                    ->where('products.deleted_at', NULL)
                    ->orderBy('products.position', 'asc')
                    ->get();
        $productArray = array();


        $dietsPreferred = empty( $diets ) ? [] : $diets;
        $allergiesPreferred = empty( $allergies ) ? [] : $allergies;
        $user_allergy_product = [];
        $user_diet_product = [];
        $processed = [];
        foreach($products as $prdt) {
            if( $prdt->pref_type == 'Intolerence' && in_array( $prdt->preferenceoptions_id,  $allergiesPreferred ) ) {
                $user_allergy_product[] = $prdt->id;
            }
        }
        foreach($products as $prdt) {
            if( $prdt->pref_type == 'Diet' && in_array( $prdt->preferenceoptions_id,  $dietsPreferred ) ) {
                $user_diet_product[] = $prdt->id;
            }
        }

        if (!empty($products)) {
            foreach ($products as $key => $value) {
                if( in_array($value->id, $processed) ) {
                    continue;
                }
                // Include user selected diet products.
                if( $dietsPreferred  && ! in_array( $value->id,  $user_diet_product ) ) {
                    continue;
                }
                // Exclude user selected allergy (intolerence) products.
                if( $allergiesPreferred && in_array( $value->id, $user_allergy_product ) ) {
                    continue;
                }
                $processed[] = $value->id;

                $productArray[$value->id]['id'] = $value->id;
                $productArray[$value->id]['product_id'] = $value->id;
                $productArray[$value->id]['name'] = $value->name;
                $productArray[$value->id]['sku'] = $value->sku;
                $productArray[$value->id]['actual_price'] = $value->actual_price;
                $productArray[$value->id]['discount_price'] = $value->discount_price;
                $productArray[$value->id]['created_at'] = $value->created_at;
                $productArray[$value->id]['url_key'] = $value->url_key;
                $productArray[$value->id]['description'] = $value->description;
                $productArray[$value->id]['position'] = $value->position;
                $productArray[$value->id]['visibility'] = $value->visibility;
                
                if (!empty($value->image_id)) {
                    $productArray[$value->id]['images'][$value->image_type][$value->image_id]['image_id'] = $value->image_id;
                    $productArray[$value->id]['images'][$value->image_type][$value->image_id]['image_path'] = url($value->image_path);
                }
                
           
                if (!empty($value->allergies_id)) {                                                                                                                                             
                    $productArray[$value->id]['allergies'][$value->allergies_id]['allergies_id'] = $value->allergies_id;
                    $productArray[$value->id]['allergies'][$value->allergies_id]['name'] = $value->allergies_name;
                    $productArray[$value->id]['allergies'][$value->allergies_id]['description'] = $value->allergies_description;
                    $productArray[$value->id]['allergies'][$value->allergies_id]['image_path'] = url($value->allergies_image_path);
                }
            }
            
            $productArray = array_slice($productArray,$offset,3);
            $finalProductArray = array();
            foreach ($productArray as $key => $value) {
                if (isset($value['images'])) {
                    foreach ($value['images'] as $vkey => $vvalue) {
                        foreach ($vvalue as $vvkey => $vvvalue) {
                            $value['images_list'][$vkey][] = $vvvalue;
                        }
                    }
                    unset($value['images']);
                }
                if(! isset($value['images_list']['THUMBNAIL']) ) {
                    $value['images_list']['THUMBNAIL'][0]['image_id'] = 0;
                    $value['images_list']['THUMBNAIL'][0]['image_path'] = url('default_image/thumbnail.png');
                }
                if (isset($value['allergies'])) {
                    foreach ($value['allergies'] as $vkey => $vvalue) {
                        $value['allergies_list'][] = $vvalue;
                    }
                    unset($value['allergies']);
                }
                $finalProductArray[] = $value;
            }
        }
        
        return response()->json([
                'status' => TRUE, 
                'message' => 'Categories list fetched',
                'products' => $finalProductArray,
                // 'products_before' => $products
        ], 200);
    }

    function menu_dishes(Request $request) {

        $key = $request->post('key');
        $cat_id = $request->post('cat_id');
        $is_for_u = $request->post('is_for_u');
        $dietsPreferred = $request->post('diets');
        $allergiesPreferred = $request->post('allergies');
        $cart_added = [];
        if ( ! empty($key) ) {
            $cartkey = DB::select("select id from cartkey where `key` = :cartkey ", ['cartkey' => $key]);
            if (empty($cartkey)) {
                $returnData = Array("message" => "Not a valid key", 'status' => FALSE);
                return response()->json($returnData, 200);
            } else {
                $cartid = $cartkey[0]->id;
            }
            $cart_added = DB::table('cart_items')->select('products_id', 'item_count')
                ->where('cart_id', '=', $cartid)->get();
        }
        if( $is_for_u) {
            $products = DB::table('products')->select(
                'product_preferenceoptions.type as pref_type', 'product_preferenceoptions.preferenceoptions_id as pref_id',
                'products.id as product_id', 'products.url_key', 'products.name as product_name', 'products.sku','actual_price', 'discount_price','url_key',
                'category.name as category_name','category.id as category_id', DB::raw('0 as item_count'))
                ->join('product_category', 'products.id', '=', 'product_category.products_id')
                ->join('category', 'product_category.category_id', '=', 'category.id')

                ->leftjoin('product_preferenceoptions', 'products.id', '=', 'product_preferenceoptions.products_id')

                //->where('category.id', '=', $cat_id)
                ->where('category.deleted_at', '=', NULL)
                ->where('products.deleted_at', '=', NULL)
                ->where('products.visibility', '=', "YES")
                ->orderBy('category.position', 'ASC')
                ->orderBy('products.position', 'ASC')
                ->get();
        } else {
            $products = DB::table('products')->select(
                'products.id as product_id', 'products.url_key', 'products.name as product_name', 'products.sku','actual_price', 'discount_price','url_key',
                'category.name as category_name','category.id as category_id', DB::raw('0 as item_count'))
                ->join('product_category', 'products.id', '=', 'product_category.products_id')
                ->join('category', 'product_category.category_id', '=', 'category.id')
                ->where('category.id', '=', $cat_id)
                ->where('category.deleted_at', '=', NULL)
                ->where('products.deleted_at', '=', NULL)
                ->where('products.visibility', '=', "YES")
                ->orderBy('category.position', 'ASC')
                ->orderBy('products.position', 'ASC')
                ->get();
            }

        $allergies = DB::table('allergies')->select('name', 'image_path')->where('deleted_at', '=', NULL)
            ->where('visibility', '=', 'YES')->orderBy('position', 'ASC')->get();

        $product_allergies = DB::table('product_allergies')->select('name', 'image_path', 'products_id')
            ->join('allergies', 'allergies.id', '=', 'product_allergies.allergies_id')
            ->where('product_allergies.deleted_at', '=', NULL)
            ->where('allergies.visibility', '=', 'YES')->orderBy('position', 'ASC')->get();

        // $products_menu_filter = DB::table('product_allergies_filter')->select('products_id', DB::raw('group_concat(allergies_filter_id) as allergy_filters'))
        //         ->groupBy('products_id')->get();    
                
        // Used for diet filter. Our menu page top filter.
        $products_menu_filter = DB::table('product_preferenceoptions')
            ->select('products_id', DB::raw('group_concat(preferenceoptions_id) as allergy_filters'))
            ->where('type', 'Diet')
            ->groupBy('products_id')->get(); 

        $categories = DB::table('category')->select('name')->where('visibility', '=', 'YES')
                ->orderBy('position', 'ASC')->where('deleted_at', '!=', NULL)->get();

        // $allergiesfilterAll = DB::table('allergies_filter')->select('name', 'image_path', 'id')
        //         ->where('visibility', '=', 'YES')->where('deleted_at', '=', NULL)->orderBy('position', 'ASC')->get();

        $allergiesfilterAll = DB::table('preferenceoptions')->select('name', 'original_image as image_path', 'id')
            ->where('type', '=', 'Diet')->where('deleted_at', '=', NULL)->get();

        $allergiesfilter = [];
        foreach($allergiesfilterAll as $ingred) {
            $ingred->image_path = url($ingred->image_path);
            $allergiesfilter[] = $ingred;
        }

        $product_thumbnail = DB::table('product_images')->select('path', 'products_id')
            ->join('products', 'products.id', '=', 'product_images.products_id')
            ->where('products.deleted_at', '=', NULL)
            ->where('type', '=', 'THUMBNAIL')->get();

        $thumbnailMapped = [];
        foreach($product_thumbnail as $thumbs) {
            // mapped thumbnail to product.
            $thumbnailMapped[ $thumbs->products_id ] = url($thumbs->path);
        }

        $prodt_allrgs = [];
        foreach($product_allergies as $allr) {
            $allr->image_path = url($allr->image_path);
            $prodt_allrgs[$allr->products_id][] = $allr;
        }

        $itemsAdded = [];
        foreach($cart_added as $cart) {
            // mapped thumbnail to product.
            $itemsAdded[ $cart->products_id ] = $cart->item_count;
        }

        $products_allergy_menu_filters = [];
        foreach($products_menu_filter as $allergy_menu_filter) {
            $products_allergy_menu_filters[ $allergy_menu_filter->products_id ] = $allergy_menu_filter->allergy_filters;
        }

        $products_all = [];
        $cat_order = [];

        $dietsPreferred = empty( $dietsPreferred ) ? [] : $dietsPreferred;
        $allergiesPreferred = empty( $allergiesPreferred ) ? [] : $allergiesPreferred;

        $processed = [];
        $user_allergy_product = [];
        $user_diet_product = [];
        if( $is_for_u) {
            foreach($products as $prdt) {
                if( $prdt->pref_type == 'Intolerence' && in_array( $prdt->pref_id,  $allergiesPreferred ) ) {
                    $user_allergy_product[] = $prdt->product_id;
                }
            }
            foreach($products as $prdt) {
                if( $prdt->pref_type == 'Diet' && in_array( $prdt->pref_id,  $dietsPreferred ) ) {
                    $user_diet_product[] = $prdt->product_id;
                }
            }
        }
        foreach($products as $prdt) {

            if( $is_for_u) {
                if( in_array($prdt->product_id, $processed) ) {
                    continue;
                }
                // Include user selected diet products.
                if( $dietsPreferred  && ! in_array( $prdt->product_id,  $user_diet_product ) ) {
                    continue;
                }
                // Exclude user selected allergy (intolerence) products.
                if( $allergiesPreferred && in_array( $prdt->product_id, $user_allergy_product ) ) {
                    continue;
                }
                $processed[] = $prdt->product_id;
            }

            if( ! in_array( $prdt->category_id, $cat_order ) ) {
                $cat_order[] = $prdt->category_id;
            }
            if( ! isset( $thumbnailMapped[ $prdt->product_id ] ) ) {
                continue;
            }
            if( isset( $itemsAdded[ $prdt->product_id ] ) ) {
                $prdt->item_count = $itemsAdded[ $prdt->product_id ];
            }
            $prdt->allergy_menu_filters = '';
            if( isset($products_allergy_menu_filters[ $prdt->product_id ]) ) {
                $prdt->allergy_menu_filters = $products_allergy_menu_filters[ $prdt->product_id ];
            }
            $prdt->thumnail = $thumbnailMapped[ $prdt->product_id ];
            
            $prdt->url_key = url('productdetail/'.$prdt->url_key);
            $prdt->allergies = $prodt_allrgs[ $prdt->product_id ] ?? [];
            
            // $products_all[$prdt->category_id]['category_name'] = $prdt->category_name;
            // $products_all[$prdt->category_id]['category_id'] = $prdt->category_id;
            // $products_all[$prdt->category_id]['items'][] = $prdt;
            $products_all[] = $prdt;
        }

        if($products !== null) {
            return response()->json([
                'message'=> 'Products fetched Successfully', 
                'products' => $products_all,
                'allergies' => $allergies,
                'categories' => $categories,
                'allergies_filter' => $allergiesfilter,
                'thumbnail' => $product_thumbnail,
                'category_order' => $cat_order,
                'status' => TRUE
            ], 200);
        } else {
            return response()->json(['message'=> 'Wrong product', 'status' => FALSE], 200);
        }
    }
}