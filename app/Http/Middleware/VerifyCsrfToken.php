<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
        '/admin/user/data',
        '/admin/user/add',
        '/admin/user/edit',
        '/admin/role/data',
        '/admin/role/add',
        '/admin/role/edit',
        '/admin/product/add',
        '/admin/product/edit',
        '/admin/product/image/delete',
        '/admin/product/delete',
        '/admin/orders/show',
        '/admin/order/edit',
        '/admin/ingredients/add',
        '/admin/ingredients/update',
        '/admin/allergies/add',
        '/admin/allergies/update',
        '/admin/cms/add',
        '/admin/cms/edit',
        '/admin/cms/data',
        '/admin/cms/delete',
        '/admin/category/add',
        '/admin/category/edit',
        '/admin/category/data',
        '/admin/category/delete',
        '/flight/timecheck',
        '/product/menu_dishes',
        '/customer/login',
        '/customer/login',
        '/product/menu_dishes',
        '/cart/add',
        '/cart/remove',
        '/cart/show',
        '/cart/show',
        '/order/add',
        '/home/addbilling',
        '/order/update_cart_user',
        '/order/history',
        '/home/profile',
        '/home/profileupdate',
        '/order/pdfreport',
        '/admin/product/data',
        '/order/cancel',
        '/admin/order/cancel',
        '/admin/orders/pdfreport',
        '/admin/orders/ordercount',
        '/admin/cms/addsmstemplate',
        '/admin/order/pl_change',
        '/admin/order/reimburse',
        '/api/login',
        '/admin/kitchen/data',
        '/home/orderreviewdata',
        '/admin/orders/addon_details'
    ];
}

