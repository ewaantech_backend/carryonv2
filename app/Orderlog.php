<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Orderlog extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'orderlog';

    protected $fillable = [
        'status','created_at' , 'updated_at'
    ];
}