<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cms extends Model
{
    protected $table = 'cms';
    
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'url_key', 'page_title', 'html_content', 'visibility', 'short_code','social_media_link','social_media_name'
    ];
}