<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/admin/test', 'Admin\Product@test');

Route::get('/admin/users', 'Admin\User@users')->name('users')->middleware('auth');
Route::post('/admin/user/data', 'Admin\User@user_data')->name('user_data')->middleware('auth');
Route::post('/admin/user/add', 'Admin\User@user_add')->name('user_add')->middleware('auth');
Route::post('/admin/user/edit', 'Admin\User@user_edit')->name('user_edit')->middleware('auth');
Route::get('/admin/user/details/{user_id}', 'Admin\User@user_details')->name('user_details')->middleware('auth');

Route::get('/admin/user/logout', 'HomeController@logout')->name('user_logout')->middleware('auth');

Route::get('/admin/roles', 'Admin\Roles@roles')->name('roles')->middleware('auth');
Route::post('/admin/role/data', 'Admin\Roles@role_data')->name('role_data')->middleware('auth');
Route::post('/admin/role/add', 'Admin\Roles@role_add')->name('role_add')->middleware('auth');
Route::post('/admin/role/edit', 'Admin\Roles@role_edit')->name('role_edit')->middleware('auth');

Route::get('/admin/products', 'Admin\Product@products')->name('products')->middleware('auth');
Route::post('/admin/product/data', 'Admin\Product@product_data')->name('product_data')->middleware('auth');
Route::post('/admin/product/add', 'Admin\Product@product_add')->name('product_add')->middleware('auth');
Route::post('/admin/product/edit', 'Admin\Product@product_edit')->name('product_edit')->middleware('auth');
Route::post('/admin/product/image/delete', 'Admin\Product@product_image_delete')->name('product_image_delete')->middleware('auth');
Route::post('/admin/product/delete', 'Admin\Product@product_destroy')->name('product_destroy')->middleware('auth');
Route::post('/admin/product/addon_data', 'Admin\Product@addon_data')->name('addon_data_to_display')->middleware('auth');


Route::get('/admin/addons', 'Admin\Addons@addons')->name('addons')->middleware('auth');
Route::post('/admin/addons/data', 'Admin\Addons@addons_data')->name('addon_data')->middleware('auth');
Route::post('/admin/addons/add', 'Admin\Addons@addons_add')->name('addon_add')->middleware('auth');
Route::post('/admin/addons/edit', 'Admin\Addons@addons_edit')->name('addon_edit')->middleware('auth');
Route::post('/admin/addons/image/delete', 'Admin\Addons@addons_image_delete')->name('addon_image_delete')->middleware('auth');
Route::post('/admin/addons/delete', 'Admin\Addons@addons_destroy')->name('addon_destroy')->middleware('auth');

Route::post('/admin/order/edit', 'Admin\OrdersController@change_status')->name('change_status')->middleware('auth');
Route::post('/admin/order/pl_change', 'Admin\OrdersController@change_pl_status')->name('change_pickup')->middleware('auth');

//Reports
Route::get('/admin/reports', 'Admin\ReportsController@index')->name('render_reports')->middleware('auth');

Route::get('/', 'Frontend\HomeController@index')->name('home_page');

Route::get('/l/{id?}', 'Frontend\HomeController@set_lounge')->name('set_lounge');

Route::get('/productdetail/{url_key}', 'Frontend\ProductsController@productdetail');
Route::get('/flight/show', 'Frontend\HomeController@show');
Route::get('/flight/timecheck', 'Frontend\HomeController@timecheck');
Route::get('/customer/registration', 'Frontend\HomeController@registration');
Route::get('/customer/logout', 'Frontend\HomeController@logout');
Route::post('/customer/login', 'Frontend\HomeController@login');
Route::post('/customer/forgotpassword', 'Frontend\HomeController@forgotpassword')->name('forgotpassword');
Route::post('/customer/resetpassword', 'Frontend\HomeController@resetpassword')->name('resetpassword');
Route::get('/customer/checkout', 'Frontend\HomeController@checkout')->name('checkout');
Route::get('/customer/billingaddress', 'Frontend\HomeController@billingaddress')->name('billingaddress');
Route::get('/product/show', 'Frontend\ProductsController@show');
Route::get('/product/menu', 'Frontend\MenuController@index')->name('our_menu');
Route::post('/product/menu_dishes', 'Frontend\MenuController@menu_dishes')->name('menu_dishes');

Route::get('/customer/orders', 'Frontend\About@index')->name('about');
Route::get('/customer/orders/{order_id}', 'Frontend\About@order_detail')->name('order_detail');

Route::post('/cart/add', 'Frontend\CartController@create')->name('cart_add');
Route::post('/cart/remove', 'Frontend\CartController@destroy');
Route::post('/cart/show', 'Frontend\CartController@show')->name('cart_list');
Route::post('/cart/addon', 'Frontend\CartController@getAddonsCart')->name('cart_addon');
Route::post('/order/add', 'Frontend\OrderController@create')->name('order_add');
Route::post('/home/addbilling', 'Frontend\HomeController@addbilling')->name('add_billing');

// Mobile checkoout
Route::get('/cart/gateway_initialize/{userToken}/{cart_sess_key}/{flight}/{date}/{duration}', 'Frontend\Checkout@mobile_pre_order_checkout')->name('mobile_payment_page');
Route::get('/mobile/success/{data}', 'Frontend\Checkout@post_mobile_pay')->name('post_mobile_payment');

Route::get('/order/payment', 'Frontend\Checkout@pre_order_checkout')->name('payment_page');
Route::get('/order/payment/success', 'Frontend\Checkout@payment_success')->name('payment_success');
Route::get('/order/payment/cancelled', 'Frontend\Checkout@payment_cancelled')->name('payment_cancelled');
Route::get('/order/payment/declined', 'Frontend\Checkout@payment_declined')->name('payment_declined');
Route::post('/order/cancel', 'Frontend\OrderController@cancel')->name('cancel_order');

Route::post('/order/update_cart_user', 'Frontend\Checkout@update_cart_user')->name('update_cart_user');

Route::post('/order/history', 'Frontend\OrderController@history')->name('order_history');
Route::post('/home/profile', 'Frontend\HomeController@profile')->name('profile');
Route::post('/home/profileupdate', 'Frontend\HomeController@profileupdate')->name('profileupdate');
Route::post('/home/preference', 'Frontend\HomeController@preference')->name('preference');
Route::post('/home/preferenceupdate', 'Frontend\HomeController@preferenceupdate')->name('preferenceupdate');
Route::post('/home/orderreviewdata', 'Frontend\HomeController@orderreviewdata')->name('orderreviewdata');
Route::post('/home/orderreviewupdate', 'Frontend\HomeController@orderreviewupdate')->name('orderreviewupdate');
Route::post('/order/pdfreport', 'Frontend\OrderController@pdfReportWeb')->name('pdfreport');

Route::get('/checkout/samplesms', 'Frontend\Checkout@samplesms')->name('samplesms');

Route::post('/cart/addon/list', 'Frontend\CartController@addon_list')->name('addon_list');

Route::any('sendemail', function () {
        $data = array(
                'bodyMessage' => "Hi"
        );
        Mail::send('email', $data, function ($message) {

                $message->from('kamarudduja@gmail.com', 'Just Laravel');

                $message->to('kamarudduja@gmail.com')->subject('Just Laravel demo email using SendGrid');
        });
        return Redirect::back()->withErrors([
                'Your email has been sent successfully'
        ]);
});

Route::get('/testmail', 'Frontend\TestMailController@index')->name('testMail');
Route::get('/testpdf', 'Frontend\TestPdfController@index')->name('TestPdf');

Auth::routes();




//Route::get('/admin', 'Admin\ReportsController@index')->name('admin')->middleware('auth');
Route::get('/admin', 'HomeController@index')->name('admin');
Route::get('/admin/allergies', 'Admin\AllergiesController@index')->middleware('auth');
Route::get('/admin/ingredients', 'Admin\IngredientsController@index')->middleware('auth');
Route::get('/admin/ingredients/create', 'Admin\IngredientsController@create')->middleware('auth');
Route::post('/admin/ingredients/add', 'Admin\IngredientsController@add')->name('ing_add')->middleware('auth');
Route::get('/admin/ingredients/edit/{id}', 'Admin\IngredientsController@edit')->name('ing_edit')->middleware('auth');
Route::post('/admin/ingredients/update', 'Admin\IngredientsController@update')->name('ing_update')->middleware('auth');
Route::get('/admin/ingredients/delete/{id}', 'Admin\IngredientsController@delete')->name('ing_delete')->middleware('auth');

// kitchen
Route::post('/admin/kitchen/data', 'Admin\Kitchen@show')->name('kitchen_view')->middleware('auth');
Route::get('/admin/kitchen', 'Admin\Kitchen@display')->name('kitchen_page')->middleware('auth');

Route::get('/admin/allergies_fliter', 'Admin\AllergiesFliterController@index')->middleware('auth');
Route::get('/admin/allergies_fliter/create', 'Admin\AllergiesFliterController@create')->name('alfliter_create')->middleware('auth');
Route::post('/admin/allergies_fliter/add', 'Admin\AllergiesFliterController@add')->name('alfliter_add')->middleware('auth');
Route::get('/admin/allergies_fliter/edit/{id}', 'Admin\AllergiesFliterController@edit')->name('alfliter_edit')->middleware('auth');
Route::post('/admin/allergies_fliter/update', 'Admin\AllergiesFliterController@update')->name('alfliter_update')->middleware('auth');
Route::get('/admin/allergies_fliter/delete/{id}', 'Admin\AllergiesFliterController@delete')->name('alfliter_delete')->middleware('auth');

Route::get('/admin/cms_slider', 'Admin\CmsSliderController@index')->middleware('auth');
Route::get('/admin/cms_slider/banners', 'Admin\CmsSliderController@banners')->middleware('auth');
Route::get('/admin/cms_slider/create', 'Admin\CmsSliderController@create')->name('cmsslider_create')->middleware('auth');
Route::post('/admin/cms_slider/add', 'Admin\CmsSliderController@add')->name('cmsslider_add')->middleware('auth');
Route::get('/admin/cms_slider/edit/{id}', 'Admin\CmsSliderController@edit')->name('cmsslider_edit')->middleware('auth');
Route::post('/admin/cms_slider/update', 'Admin\CmsSliderController@update')->name('cmsslider_update')->middleware('auth');
Route::get('/admin/cms_slider/delete/{id}', 'Admin\CmsSliderController@delete')->name('cmsslider_delete')->middleware('auth');

Route::get('/admin/allergies/create', 'Admin\AllergiesController@create')->name('create')->middleware('auth');
Route::post('/admin/allergies/add', 'Admin\AllergiesController@add')->name('add')->middleware('auth');
Route::get('/admin/allergies/edit/{id}', 'Admin\AllergiesController@edit')->name('edit')->middleware('auth');
Route::post('/admin/allergies/update', 'Admin\AllergiesController@update')->name('update')->middleware('auth');
Route::get('/admin/allergies/delete/{id}', 'Admin\AllergiesController@delete')->name('delete')->middleware('auth');

// Route::get('/admin/allergies/create', 'Admin\AllergiesController@create')->middleware('auth');
// Route::post('/admin/allergies/add',   'Admin\AllergiesController@add')->name('add')->middleware('auth');

Route::get('/admin/preferences', 'Admin\Preference@index')->name('preferences')->middleware('auth');
Route::get('/admin/preferences/create', 'Admin\Preference@create')->name('preference_create')->middleware('auth');
Route::post('/admin/preferences/add', 'Admin\Preference@add')->name('preference_add')->middleware('auth');
Route::get('/admin/preferences/edit/{id}', 'Admin\Preference@edit')->name('preference_edit')->middleware('auth');
Route::post('/admin/preferences/update', 'Admin\Preference@update')->name('preference_update')->middleware('auth');
Route::get('/admin/preferences/delete/{id}', 'Admin\Preference@delete')->name('preference_delete')->middleware('auth');

Route::get('/admin/orders', 'Admin\OrdersController@index')->name('admin_orders')->middleware('auth');
Route::get('/admin/orders?t=DELIVERED', 'Admin\OrdersController@index')->name('delivered_orders')->middleware('auth');
Route::get('/admin/orders?t=CANCELLED', 'Admin\OrdersController@index')->name('cancelled_orders')->middleware('auth');
Route::get('/admin/cms', 'Admin\CmsController@index')->name('cms')->middleware('auth');
Route::get('/admin/cms/section', 'Admin\CmsController@section')->name('cms_section')->middleware('auth');
Route::get('/admin/cms/socialmedia', 'Admin\CmsController@socialmedia')->name('socialmedia')->middleware('auth');
Route::get('/admin/orders/show', 'Admin\OrdersController@show')->middleware('auth');
Route::get('/admin/orders/details/{id}', 'Admin\OrdersController@details')->middleware('auth');
Route::post('/admin/orders/addon_details', 'Admin\OrdersController@addon_details')->name('kitchen_addon_view')->middleware('auth');
Route::post('/admin/orders/ordercount', 'Admin\OrdersController@ordercount')->name('ordercount')->middleware('auth');
Route::post('/admin/orders/pdfreport', 'Admin\OrdersController@pdfreport')->name('adminpdfreport');
Route::post('/admin/order/cancel', 'Admin\OrdersController@cancel')->name('admin_cancel_order')->middleware('auth');
Route::post('/admin/order/reimburse', 'Admin\OrdersController@reimburse')->name('admin_reimburse_order')->middleware('auth');
Route::get('/admin/email/{type}', 'Admin\CmsController@emailtemplate')->name('emailtemplate')->middleware('auth');
Route::get('/admin/sms/{type}', 'Admin\CmsController@smstemplate')->name('smstemplate')->middleware('auth');
Route::post('/admin/cms/addtemplate', 'Admin\CmsController@template_add')->name('template_add')->middleware('auth');
Route::post('/admin/cms/addsmstemplate', 'Admin\CmsController@smstemplate_add')->name('smstemplate_add')->middleware('auth');

Route::post('/admin/cms/add', 'Admin\CmsController@cms_add')->name('cms_add')->middleware('auth');
Route::post('/admin/cms/edit', 'Admin\CmsController@cms_edit')->name('cms_edit')->middleware('auth');
Route::post('/admin/cms/socialmedia/data', 'Admin\CmsController@socialmedia_data')->name('socialmedia_data')->middleware('auth');
Route::post('/admin/cms/socialmedia/edit', 'Admin\CmsController@socialmedia_edit')->name('socialmedia_edit')->middleware('auth');
Route::post('/admin/cms/data', 'Admin\CmsController@cms_data')->name('cms_data')->middleware('auth');
Route::post('/admin/cms/delete', 'Admin\CmsController@cms_delete')->name('cms_delete')->middleware('auth');
Route::get('/admin/settings/data', 'Admin\CmsController@settings_data')->name('settings_data');
Route::post('/admin/settings/edit', 'Admin\CmsController@settings_edit')->name('settings_edit');

Route::get('/admin/category', 'Admin\CategoryController@index')->middleware('auth');
Route::post('/admin/category/add', 'Admin\CategoryController@category_add')->name('category_add')->middleware('auth');
Route::post('/admin/category/edit', 'Admin\CategoryController@category_edit')->name('category_edit')->middleware('auth');
Route::post('/admin/category/data', 'Admin\CategoryController@category_data')->name('category_data')->middleware('auth');
Route::post('/admin/category/delete', 'Admin\CategoryController@category_destroy')->name('category_destroy')->middleware('auth');

Route::get('/admin/doneness', 'Admin\DonenessController@index')->middleware('auth');
Route::post('/admin/doneness/add', 'Admin\DonenessController@doneness_add')->name('doneness_add')->middleware('auth');
Route::post('/admin/doneness/edit', 'Admin\DonenessController@doneness_edit')->name('doneness_edit')->middleware('auth');
Route::post('/admin/doneness/data', 'Admin\DonenessController@doneness_data')->name('doneness_data')->middleware('auth');
Route::post('/admin/doneness/delete', 'Admin\DonenessController@doneness_destroy')->name('doneness_destroy')->middleware('auth');


Route::get('/admin/customer', 'Admin\CustomerController@index')->middleware('auth');
Route::get('/admin/reports/categories', 'Admin\ReportsController@categories');
Route::get('/admin/reports/diets', 'Admin\ReportsController@diets');
Route::get('/admin/reports/salestab/{date_from}/{date_to}/{cat}/{diet}', 'Admin\ReportsController@salestab');
Route::get('/admin/reports/linecharttab/{date_from}/{date_to}/{cat}/{diet}', 'Admin\ReportsController@linecharttab');
Route::get('/admin/reports/destinationtab/{date_from}/{date_to}/{cat}/{diet}', 'Admin\ReportsController@destinationtab');

Route::get('/admin/reports/hourlytab/{date_from}/{date_to}', 'Admin\ReportsController@hourlytab');

// Route::post('/api/login', 'Api\ApiController@login')->name('api_login');

// Short link redirect to main link.
Route::get('/m/{code}', 'Frontend\Checkout@shortenLink')->name('shorten.link');

Route::get('/automatedstatuschange', 'Frontend\CroneController@statuschange')->name('crone_statuschange');

Route::get('/flightgateupdate', 'Frontend\CroneController@flightgateupdate')->name('crone_flightgateupdate');

Route::get('/orderstatusupdate', 'Frontend\CroneController@orderstatusupdate')->name('crone_orderstatusupdate');

Route::get('/remaindermail', 'Frontend\CroneController@remaindermail')->name('crone_remaindermail');

Route::get('/reviewmail', 'Frontend\CroneController@reviewmail')->name('crone_reviewmail');
Route::get('/readymail', 'Frontend\CroneController@readymail')->name('crone_readymail');

Route::get('/orderreview/{key}', 'Frontend\About@orderreview')->name('orderreview');
Route::get('/sample_notification/{type}/{id}', 'Admin\OrdersController@sample_notification')->name('sample_notification');

Route::get('/{url_key}', 'Frontend\HomeController@cms')->name('cms_page');
