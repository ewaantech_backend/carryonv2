@extends('admin.layouts.app')

@section('content')

    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                
                <!-- /# row -->
                <section id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card list-block">
                                <div class="card-title">
                                    <h4>Products</h4>
                                </div>
                                <div class="invoice-edit">
                                    
                                    <!--.row-->
                                    <div class="invoicelist-body">
                                        <div class="bootstrap-data-table-panel">
                                            <div class="table-responsive">
                                                <table  id="bootstrap-data-table" class="table table-striped table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Product #</th>
                                                            <th>Name</th>
                                                            <th>Sku</th>
                                                            <th>Actual price</th>
                                                            <th>Discount price</th>
                                                            <th>Visibility</th>
                                                            <th>Position</th>
                                                            <!-- <th>Description</th> -->
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                    @if (count($products) > 0)
                                                        
                                                        @foreach ($products as $product)
                                                            <tr>
                                                                <td><a class="edit-item" data-item={{$product->id}}>{{$product->id}}</a></td>
                                                                <td>{{$product->name}}</td>
                                                                <td>{{$product->sku}}</td>
                                                                <td>{{ decimal_format_custom($product->actual_price) }}</td>
                                                                <td>{{ decimal_format_custom($product->discount_price) }}</td>
                                                                <td>{{$product->visibility}}</td>
                                                                <td>{{$product->position}}</td>
                                                                <!-- <td>{{$product->description}}</td> -->
                                                                <td><a class="addon-item" data-item={{$product->id}}><span style='color:green;' class="ti-plus pull-left"></span></a><a class="delete-item" data-item={{$product->id}}><span style='color:red;' class="ti-trash pull-right"></span></a><a class="edit-item" data-item={{$product->id}}><span style='color:blue;' class="ti-pencil-alt pull-right"></span></a></td>
                                                            </tr>
                                                        @endforeach
                                                    @endif

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        @if ( isset($permissions['products_add_edit']) && $permissions['products_add_edit'] == true )
                                            <a class="control newRow add-new-item" href="#">+ New Product</a>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="edit-add-block">
                                <div class="card">
                                    @if ( ! isset($permissions['products_add_edit']))
                                    <div class="alert alert-danger">
                                        You dont have permission to edit.
                                    </div>
                                    @endif
                                    <div class="card-title">
                                        <h4>Product Add/Edit</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="form-validation">
                                            <form id="product-add" class="form-valide add-form" method="post" enctype="multipart/form-data">
                                                <input type="hidden" id="product_id" name="product_id">
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Name <span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control" id="product_name" name="product_name" placeholder="Enter a name..">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Sku <span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control" id="product_sku" name="product_sku" placeholder="Enter sku..">
                                                    </div>
                                                </div>
                                                <div class="form-group row password">
                                                    <label class="col-lg-4 col-form-label">Actual Price <span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                        <input type="number" class="form-control" id="product_actual_price" name="product_actual_price" placeholder="Enter actual price">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Discount Price <span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                    <input type="number" class="form-control" id="product_discount_price" name="product_discount_price" placeholder="Enter discount price">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Description <span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                    <textarea class="form-control" id="product_description" name="product_description" placeholder="Enter product description" ></textarea>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Base Content <span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control" id="base_content" name="base_content" placeholder="Enter base content">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Friendly Url <span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                    <input type="text" class="form-control" id="product_friendly_url" name="product_friendly_url" placeholder="Enter friendly url">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Categories <span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                        <select style="width:100%" class=" form-control select-2" id="product_categories" name="product_categories[]" multiple="multiple">
                                                        @foreach ($categories as $category)
                                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                        @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Addons</label>
                                                    <div class="col-lg-8">
                                                    <select style="width:100%" class="form-control select-2" id="product_addons" name="product_addons[]" multiple="multiple">
                                                        @foreach ($addons as $addon)
                                                            <option value="{{ $addon->id }}">{{ $addon->name }}</option>
                                                        @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Doneness </label>
                                                    <div class="col-lg-8">
                                                        <select style="width:100%" class=" form-control select-2" id="product_doneness" name="product_doneness[]" multiple="multiple">
                                                        @foreach ($doneness as $donenes)
                                                            <option value="{{ $donenes->id }}">{{ $donenes->name }}</option>
                                                        @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Related Products</label>
                                                    <div class="col-lg-8">
                                                    <select style="width:100%" class="form-control select-2" id="product_related" name="product_related[]" multiple="multiple">
                                                        @foreach ($allproducts as $allproduct)
                                                            <option value="{{ $allproduct->id }}">{{ $allproduct->name }}</option>
                                                        @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Tags</label>
                                                    <div class="col-lg-8">
                                                    <select style="width:100%" class="form-control select-2" id="product_allergies" name="product_allergies[]" multiple="multiple">
                                                        @foreach ($allergies as $allergy)
                                                            <option value="{{ $allergy->id }}">{{ $allergy->name }}</option>
                                                        @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Menu Filters</label>
                                                    <div class="col-lg-8">
                                                    <select style="width:100%" class="form-control select-2" id="product_allergies_filter" name="product_allergies_filter[]" multiple="multiple">
                                                        @foreach ($allergies_filter as $allergy_filter)
                                                            <option value="{{ $allergy_filter->id }}">{{ $allergy_filter->name }}</option>
                                                        @endforeach
                                                        </select>
                                                    </div>
                                                </div>   
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Diet</label>
                                                    <div class="col-lg-8">
                                                    <select style="width:100%" class="form-control select-2" id="product_diets" name="product_diets[]" multiple="multiple">
                                                        @foreach ($diets as $diet)
                                                            <option value="{{ $diet->id }}">{{ $diet->name }}</option>
                                                        @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Allergies</label>
                                                    <div class="col-lg-8">
                                                    <select style="width:100%" class="form-control select-2" id="product_intolerances" name="product_intolerances[]" multiple="multiple">
                                                        @foreach ($intolerences as $intolerance)
                                                            <option value="{{ $intolerance->id }}">{{ $intolerance->name }}</option>
                                                        @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Ingredients<span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                    <select style="width:100%" class="form-control select-2" id="product_incredients" name="product_incredients[]" multiple="multiple">
                                                        @foreach ($incredients as $incredient)
                                                            <option value="{{ $incredient->id }}">{{ $incredient->name }}</option>
                                                        @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Visibility<span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                        <select class="form-control" id="product_visibility" name="product_visibility">
                                                            <option value="YES">YES</option>
                                                            <option value="NO">NO</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Featured<span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                        <select class="form-control" id="product_featured" name="product_featured">
                                                            <option value="YES">YES</option>
                                                            <option value="NO">NO</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Position <span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control" id="product_position" name="product_position" >
                                                    </div>
                                                </div>                                                

                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Thumbnail Image<span class="text-danger">*</span></label>
                                                    <div class="col-lg-8" id="thumb-img">
                                                        <button style="cursor:pointer;background-color: transparent;border-color:#aaa;" class="btn btn-default" type="button" 
                                                            onclick="onButtonClicked('thumbnail_image', 'content')">
                                                            <div class="stat-icon">
                                                                <i style="color:blue;font-size:25px;" class="ti-cloud-up">
                                                                    <span style="color:#495057;font-size:17px;top:-6px;position: relative;font-family: 'Roboto', sans-serif;"> Upload thumbnail image</span>
                                                                </i>
                                                            </div>
                                                        </button>
                                                        <div id="content"></div>
                                                        <div id="content-hidden" style="display:none;"></div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">product Images<span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                        <button style="cursor:pointer;background-color: transparent;border-color:#aaa;" class="btn btn-default" type="button" 
                                                            onclick="onButtonClicked('product_image[]', 'image-content')">
                                                            <div class="stat-icon">
                                                                <i style="color:blue;font-size:25px;" class="ti-cloud-up">
                                                                    <span style="color:#495057;font-size:17px;top:-6px;position: relative;font-family: 'Roboto', sans-serif;"> Upload product images</span>
                                                                </i>
                                                            </div>
                                                        </button>
                                                        <div id="image-content"></div>
                                                        <div id="image-content-hidden" style="display:none;"></div>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Is Carryon Product <span class="text-danger">*</span></label>
                                                    <div class="col-lg-1">
                                                    <input style="height: 15px;" type="checkbox" value='Y' class="form-control" id="is_carryon" name="is_carryon">
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Is Lounge Product <span class="text-danger">*</span></label>
                                                    <div class="col-lg-1">
                                                    <input style="height: 15px;" value="Y" type="checkbox" class="form-control" id="is_lounge" name="is_lounge">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-lg-8 ml-auto">
                                                        <button type="button" class="btn btn-default cancel-form">Cancel</button>
                                                        @if ( isset($permissions['products_add_edit']) && $permissions['products_add_edit'] == true )  
                                                            <button type="submit" class="btn btn-primary">Submit</button>
                                                        @endif
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /# card -->
                        </div>
                        <!-- /# column -->
                    </div>
                    <!-- /# row -->
                <div class="modal" id="confirm_status" tabindex="-1" role="dialog">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-body">
                            <h4>Do you want to delete the product? </h4>
                            <input type="hidden" value="" id="delete_id">
                            <div id="status_modal_contents" class="row">
                                <!-- contents -->
                            </div>
                          </div>
                          <div class="modal-footer">
                            <span id="confirm-text" style="color:red;display:none;">Do you want to delete the product? </span>
                            <button type="button" class="btn btn-primary delete-item-confirm">Proceed</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                          </div>
                        </div>
                      </div>
                    </div>
                <div class="modal" id="addon_data" tabindex="-1" role="dialog">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-body">
                            <div id="addon_modal_title">
<!--                                <h4>Addons associated with this product</h4>-->
                            </div>
                            <div id="addon_modal_contents">
                                <!-- contents -->
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>2020 © carryOn DXB Admin.</p>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    @endsection


    @push('scripts')

    <script src="{{ asset('js/lib/select2/select2.full.min.js') }}"></script>
    <!-- Form validation -->
    <script src="{{ asset('js/lib/form-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/lib/form-validation/jquery.validate-init.js') }}"></script>

    <script>
      $(document).ready(function(){
        CKEDITOR.replace('product_description', {
            on: {
                  change: function() {
                      this.updateElement();    
                  }
            }
        })
        $('.select-2').select2({
            placeholder: 'Select options'
        });
        
    $(document).on('click', '.change-stat-modal', function() {
        var order_id = $(this).attr('order_id');
        $('#order_id').val(order_id);
        var currentItem = $(this).attr('value');
        $('button.change-pickup').removeClass('btn-success');
        if( $.inArray(currentItem, ['ORDER_CONFIRMED', 'IN_KITCHEN', 'READY_FOR_PICKUP', 'DELIVERED']) != -1 ) {
            $('button.change-pickup[value="'+ currentItem +'"]').addClass('btn-success');
        } else {
            $('button.change-pickup[itemval="'+ currentItem +'"]').addClass('btn-success');
        }
        $('#confirm-text').hide();
        $('#confirm_status').modal('show');
    });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var form = $( "#product-add" );
        form.validate();
        $( "#product-add" ).on("submit", function() {
            if(form.valid() == true) {
                // var data = form.serialize();
                $('#product_description').val(CKEDITOR.instances.product_description.getData());
                let data = new FormData( form[0] );
                console.log(data);
                $('#overlay').show();
                $.ajax({
                    url: form.attr('action'),
                    method: "post",
                    data: data,
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        data = JSON.parse(data);
                        if(data.status == true) {
                            swal({
                                title: "Success",
                                text: "Saved successfully",
                                timer: 2000,
                                showConfirmButton: false
                            });
                            setTimeout(() => {
                               location.reload();
                            }, 2000);
                        } else {
                            swal({
                                title: "Error",
                                text: data.message,
                                timer: 2000,
                                showConfirmButton: false
                            });
                        }
                    },
                    complete: function() {
                        $('#overlay').hide();
                    }
                });
            }
        });
        $('.add-new-item').on('click', function() {
            $( "#product-add" ).attr('action', "<?php echo route('product_add')?>");
            $('#product_id').val('');
            $('#product_description').empty();
            CKEDITOR.instances.product_description.setData('')
            $('#image-content').empty();
            $('#content').empty();
            $('#product_allergies').val([]).change();
            $('#product_allergies_filter').val([]).change();
            $('#product_diets').val([]).change();
            $('#product_intolerances').val([]).change();
            $('#product_categories').val([]).change();
            $('#product_doneness').val([]).change();
            $('#product_related').val([]).change();
            $('#product_incredients').val([]).change();
            $( "#product-add" ).trigger("reset");

            $('#is_lounge').removeAttr('checked').change();
            $('#is_carryon').removeAttr('checked').change();
        });

        $(document).on("click", '.edit-item', function() {
            var id = $(this).data('item');
            $( "#product-add" ).attr('action', "<?php echo route('product_edit')?>");
            $('#product_id').val(id);
            $('#overlay').show();
            $.ajax({
                url: "<?php echo route('product_data')?>",
                method: "post",
                data: {
                    id: id
                },
                success: function(data) {
                    data = JSON.parse(data);
                    if(data.status == true) {
                        $('#product_name').val(data.product.name);
                        $('#base_content').val(data.product.base_content);
                        $('#product_sku').val(data.product.sku);
                        $('#product_actual_price').val(data.product.actual_price);
                        $('#product_discount_price').val(data.product.discount_price);
                        CKEDITOR.instances.product_description.setData(data.product.description);
                        $('#product_description').val(data.product.description);
                        $('#product_friendly_url').val(data.product.url_key);
                        $('#product_visibility').val(data.product.visibility);
                        $('#product_featured').val(data.product.featured);
                        $('#product_position').val(data.product.position);

                        if(data.product.is_lounge_product == 'Y') {
                            $('#is_lounge').attr('checked', 'checked');
                        } else {
                            $('#is_lounge').removeAttr('checked');
                        }
                        if(data.product.is_carryon_product == 'Y') {
                            $('#is_carryon').attr('checked', 'checked');
                        } else {
                            $('#is_carryon').removeAttr('checked');
                        }

                        var incrdnts_selected = [];
                        for(var incr_indnts =0; incr_indnts < data.incredients.length; incr_indnts++ ){
                            incrdnts_selected.push(data.incredients[incr_indnts].incredience_id);
                        }
                        $('#product_incredients').val(incrdnts_selected).change();
                        
                        var relations_selected = [];
                        for(var incr_relations =0; incr_relations < data.relations.length; incr_relations++ ){
                            relations_selected.push(data.relations[incr_relations].product_rel_id);
                        }
                        $('#product_related').val(relations_selected).change();
                        

                        var categories_selected = [];
                        for(var incr_cats =0; incr_cats < data.categories.length; incr_cats++ ){
                            categories_selected.push(data.categories[incr_cats].category_id);
                        }
                        $('#product_categories').val(categories_selected).change();

                        var doneness_selected = [];
                        for(var incr_done =0; incr_done < data.doneness.length; incr_done++ ){
                            doneness_selected.push(data.doneness[incr_done].doneness_id);
                        }
                        $('#product_doneness').val(doneness_selected).change();

                        var allergies_selected = [];
                        for(var incr_all =0; incr_all < data.allergies.length; incr_all++ ){
                            allergies_selected.push(data.allergies[incr_all].allergies_id);
                        }
                        $('#product_allergies').val(allergies_selected).change();
                        
                        var diets_selected = [];
                        for(var incr_all =0; incr_all < data.diets.length; incr_all++ ){
                            diets_selected.push(data.diets[incr_all].preferenceoptions_id);
                        }
                        $('#product_diets').val(diets_selected).change();
                        
                        var intolerences_selected = [];
                        for(var incr_all =0; incr_all < data.intolerences.length; incr_all++ ){
                            intolerences_selected.push(data.intolerences[incr_all].preferenceoptions_id);
                        }
                        $('#product_intolerances').val(intolerences_selected).change();
                        
                        var addons_selected = [];
                        for(var incr_addons =0; incr_addons < data.addons.length; incr_addons++ ){
                            addons_selected.push(data.addons[incr_addons].addons_id);
                        }
                        $('#product_addons').val(addons_selected).change();

                        var allergies_filter_selected = [];
                        for(var incr_all =0; incr_all < data.allergies_filter.length; incr_all++ ){
                            allergies_filter_selected.push(data.allergies_filter[incr_all].allergies_filter_id);
                        }
                        $('#product_allergies_filter').val(allergies_filter_selected).change();
                        
                        var appUrl = "{{ env('APP_URL') }}/public";
                        if( data.thumbnail != null) {
                            $('#content').html(`<img src="${appUrl}${data.thumbnail.path}" 
                            style="width: 100px; height: 100px; margin-top: 25px;">`);
                        }
                        if( data.images.length != 0 ) {
                            $('#image-content').empty();
                            for(var incr_img =0; incr_img < data.images.length; incr_img++ ){
                                $('#image-content').append(`<img id="prdt_img_${data.images[incr_img].id}" src="${appUrl}${data.images[incr_img].path}" 
                                style="width: 200px; height: 200px; margin-top: 25px;margin-right: 10px;">
                                <button class="dlt-img" img_id="${data.images[incr_img].id}" style="position: relative; top: -71px; left: -37px;" type="button">x</button>`);
                            }
                        }
                        $('.list-block').hide();
                        $('.edit-add-block').show();
                    }
                },
                complete: function() {
                    $('#overlay').hide();
                }
            });
        });

        $(document).on('click', '.dlt-img', function() {
            var img_id = $(this).attr('img_id');
            var that = this;
            $('#overlay').show();
            $.ajax({
                url: "<?php echo route('product_image_delete')?>",
                method: "post",
                data: {
                    img_id: img_id
                },
                success: function(data) {
                    $('#prdt_img_'+img_id).remove();
                    $(that).remove();
                },
                complete: function() {
                    $('#overlay').hide();
                }
            });


        });

      });
      //$('.delete-item').on("click", function() {
    $(document).on( "click", ".delete-item", function() {
            var id = $(this).data('item');
            $("#delete_id").val(id);
            $('#confirm_status').modal().show();
     });
     
    $(document).on( "click", ".addon-item", function() {
            var id = $(this).data('item');
            $('#addon_data').modal().show();
            $.ajax({
                url: "<?php echo route('addon_data_to_display')?>",
                method: "post",
                data: {
                    id: id
                },
                success: function(data) {
                    $('#overlay').show();
                    $('#addon_modal_contents').empty();
                    $('#addon_modal_title').empty();
                    data = JSON.parse(data);
                    if(data.status == true) {
                        if( data.addons.length != 0 ) {
                            var listing = ``;
                            //$('#addon_modal_contents').append(`<div class="row"><span style="width:50px;">Name<span><span>Price<span></div>`);
                            listing += `<div class="bootstrap-data-table-panel">
                                                                    <div class="table-responsive">
                                                                        <table  id="bootstrap-data-table" class="table table-striped table-bordered">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th>Name</th>
                                                                                    <th>Price</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>`;
                            for(var incr_addon =0; incr_addon < data.addons.length; incr_addon++ ){
                                $('#addon_modal_title').html('<h4>Addons</h4>');
                                listing += `<tr>
                                                                       <td>${data.addons[incr_addon].name}</td>
                                                                       <td>${data.addons[incr_addon].actual_price}</td>    
                                                                    </tr>`;
                                //$('#addon_modal_contents').append(`<div class="row"><span>${data.addons[incr_addon].name}</span><span>${data.addons[incr_addon].actual_price}</span></div>`;
                            }
                                listing += `</tbody>
                                                                </table>
                                                                </div>
                                                                </div>`;
                                 $('#addon_modal_contents').html(listing);
                        } else {
                            $('#addon_modal_title').html('<h4 style="color:red;">No addon associated with this product</h4>');
                        }
                    }
                },
                complete: function() {
                    $('#overlay').hide();
                }
            });
            //$('#addon_modal_contents').html('<div class="row">sample1</div><div class="row">sample2</div>');
     });
        
  $('.delete-item-confirm').on("click", function() {
      $('#confirm_status').modal().hide();
        $('#overlay').show();
        var id = $("#delete_id").val();
        $.ajax({
                url: "<?php echo route('product_destroy')?>",
                method: "post",
                data: {
                    id: id
                },
                success: function(data) {
                    data = JSON.parse(data);

                        if(data.status == true) {
                            swal({
                                title: "Success",
                                text: "Deleted successfully",
                                timer: 2000,
                                showConfirmButton: false
                            });
                            setTimeout(() => {
                                location.reload();
                            }, 1000);
                        } else {
                            swal({
                                title: "Error",
                                text: data.message,
                                timer: 2000,
                                showConfirmButton: false
                            });
                        }
                },
                complete: function() {
                    $('#overlay').hide();
                }
            });
      
  });      
        
        // Button callback
        async function onButtonClicked(name, container){
            let files = await selectFile("image/*", false, name, container);
            if(name == 'thumbnail_image') {
                $('#' + container).html(`<img src="${URL.createObjectURL(files)}" style="width: 100px; height: 100px; margin-top: 25px;">`);
            } else {
                $('#' + container).append(`<img src="${URL.createObjectURL(files)}" style="width: 200px; height: 200px; margin-top: 25px;margin-right: 10px;">`);
            }
        }

        // ---- function definition ----
        function selectFile (contentType, multiple, name, container){
            return new Promise(resolve => {
                let input = document.createElement('input');
                input.type = 'file';
                input.multiple = multiple;
                input.accept = contentType;
                input.name=name;

                input.onchange = _ => {
                    let files = Array.from(input.files);
                    if (multiple)
                        resolve(files);
                    else
                        resolve(files[0]);
                };
                if(name == 'thumbnail_image') {
                    $('#'+container+'-hidden').html(input);
                } else {
                    $('#'+container+'-hidden').append(input);
                }
                input.click();
            });
        }
            
    </script>
@endpush