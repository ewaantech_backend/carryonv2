
@extends('admin.layouts.app')
@section('content')
<div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Create Preference</h1>
                            </div>
                        </div>
                    </div>
                   
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb">
                                    <!-- <li class="breadcrumb-item"><a href="#">Create Allergies</a></li> -->
                                </ol>
                            </div>
                        </div>
                    </div>
                    </div>
                
                <section id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="form-validation">
                                        <form class="form-valide direct_submit" action="{{ route('preference_add') }}"  method="POST" enctype="multipart/form-data" id="preference-add">
                                        <div class="alert alert-danger print-error-msg" style="display:none">
                                        <ul></ul>
                                        </div>
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="val-currency">Name  <span class="text-danger">*</span></label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control" id="name" name="name" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="val-skill">visibility <span class="text-danger">*</span></label>
                                            <div class="col-lg-6">
                                                <select class="form-control" id="pref_type" name="pref_type" required>
                                                    <option value="">Please select</option>
                                                    <option value="Intolerence">Intolerence</option>
                                                    <option value="Diet">Diet</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="val-currency">Original Image  <span class="text-danger"></span></label>
                                            <div class="col-lg-6">
                                                <input id="original_image" type="file" class="form-control" name="original_image" >
                                                @error('original_image')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" for="val-currency">Alternative Image  <span class="text-danger"></span></label>
                                            <div class="col-lg-6">
                                                <input id="alternative_image" type="file" class="form-control" name="alternative_image" >
                                                @error('alternative_image')
                                                    <div class="alert alert-danger">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-lg-8 ml-auto">
                                                <button type="submit" class="btn btn-primary btn-sm m-b-10 m-l-5">Submit</button>
                                                <button type="button" id="cancel_button" class="btn btn-default btn-sm m-b-10 m-l-5">Cancel</button>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                </section>
            </div>
        </div>
    </div>
@endsection



@push('scripts')

<script src="{{ asset('js/lib/select2/select2.full.min.js') }}"></script>
<!-- Form validation -->
<script src="{{ asset('js/lib/form-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/lib/form-validation/jquery.validate-init.js') }}"></script>

<!-- ajax validation -->
<!-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="http://malsup.github.com/jquery.form.js"></script>      

<script>
        $(document).ready(function(){
          $("#cancel_button").click(function(){
            location.href = "{{url('/admin/preferences')}}";
          });
        });
    </script>

@endpush