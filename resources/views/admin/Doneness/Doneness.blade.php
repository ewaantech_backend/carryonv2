
@extends('admin.layouts.app')

@section('content')

    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                
                <!-- /# row -->
                <section id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card list-block">
                                <div class="card-title">
                                    <h4>Doneness List</h4>
                                </div>
                                <div class="invoice-edit">
                                    
                                    <!--.row-->
                                    <div class="invoicelist-body">
                                        <div class="bootstrap-data-table-panel">
                                            <div class="table-responsive">
                                                <table  id="bootstrap-data-table" class="table table-striped table-bordered">
                                                    <thead>
                                                        <tr>
                                                        <th>No</th>
                                                          <th>Name</th>
                                                            <th>visibility</th>
                                                            <th>created</th>
                                                            <th>updated</th>
                                                            <th>Action</th>
                                                           
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    @if (count($doneness) > 0)
                                                        @php
                                                        $count = 1;
                                                        @endphp
                                                        @foreach ($doneness as $donenes)
                                                                    <tr>
                                                                    <td>{{$count++}}</td>
                                                                    <td>{{$donenes->name}}</td>
                                                                   
                                                                    <td>{{$donenes->visibility}}</td>
                                                                    <td>{{ date_format_custom($donenes->created_at) }}</td>
                                                                    <td>{{ date_format_custom($donenes->updated_at) }}</td>
                                                                   
                                                                    <td><a class="delete-item"  data-item={{$donenes->id}} ><i style='margin-left: 10px;color:red;' class="ti-trash pull-right"></i></a><a class="edit-item" data-item={{$donenes->id}}><span><i style='color:blue;' class="ti-pencil-alt pull-right"></span></i> </td></a>
                                                                    </tr>
                                                    @endforeach
                                                    @endif  
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <a class="control newRow add-new-item" href="#">+ Add Doneness</a>
                                    </div>
                                </div>
                            </div>

                            <div class="edit-add-block">
                                <div class="card">
                                    <div class="card-title">
                                        <h4>Doneness Add/Edit</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="form-validation">
                                        <form id="doneness-add" class="form-valide add-form" method="post">
                                                <input type="hidden" id="doneness-id" name="doneness-id">
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Doneness Name <span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control" id="doneness_name" name="doneness_name" placeholder="Enter a Doneness Name">
                                                    </div>
                                                </div>
                                               
                                                                                              
                                                <div class="form-group row">
                                                    <label class="col-lg-4 col-form-label">Visibility<span class="text-danger">*</span></label>
                                                    <div class="col-lg-8">
                                                        <select class="form-control" id="visibility" name="visibility">
                                                            <option value="YES">YES</option>
                                                            <option value="NO">NO</option>
                                                        </select>
                                                    </div>
                                                </div>
                                              
                                                
                                                
                                                <div class="form-group row">
                                                    <div class="col-lg-8 ml-auto">
                                                        <button type="button" class="btn btn-default cancel-form">Cancel</button>   
                                                        <button type="submit" class="btn btn-primary">Submit</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /# card -->
                        </div>
                        <!-- /# column -->
                    </div>
                    <!-- /# row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>2020 © carryOn DXB Admin.</p>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    @endsection
    @push('scripts')

<script src="{{ asset('js/lib/select2/select2.full.min.js') }}"></script>
<!-- Form validation -->
<script src="{{ asset('js/lib/form-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/lib/form-validation/jquery.validate-init.js') }}"></script>

<script>
  $(document).ready(function(){
    // CKEDITOR.replace('html_content');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var form = $( "#doneness-add" );
    form.validate();
    $( "#doneness-add" ).on("submit", function() {
        if(form.valid() == true) {
            // var data = form.serialize();
            let data = new FormData( form[0] );
            $('#overlay').show();
            $.ajax({
                url: form.attr('action'),
                method: "post",
                data: data,
                processData: false,
                contentType: false,
                success: function(data) {
                    data = JSON.parse(data);
                    // $('.list-block').show();
                    // $('.edit-add-block').hide();
                    if(data.status == true) {
                        swal({
                            title: "Success",
                            text: "Saved successfully",
                            timer: 2000,
                            showConfirmButton: false
                        });
                        setTimeout(() => {
                            location.reload();
                        }, 2000);
                    } else {
                        swal({
                            title: "Error",
                            text: data.message,
                            timer: 2000,
                            showConfirmButton: false
                        });
                    }
                },
                complete: function() {
                    $('#overlay').hide();
                }
            });
        }
    });
    
    $('.add-new-item').on('click', function() {
        $( "#doneness-add" ).attr('action', "<?php echo route('doneness_add')?>");
        $('#cms').val('');
        $('#content').html('');
        $('#doneness-id').val('');
        $('#doneness_name').val('');
        $('#visibility').val('');
    });

    $('.edit-item').on("click", function() {
        var id = $(this).data('item');
       // alert(id);
        $( "#doneness-add" ).attr('action', "<?php echo route('doneness_edit')?>");
        $('#doneness-id').val(id);
        $('#content').html('');
        $('#overlay').show();
        $.ajax({
            url: "<?php echo route('doneness_data')?>",
            method: "post",
            data: {
                id: id
            },
            success: function(data) {
                data = JSON.parse(data);
                if(data.status == true) {
                    $('#doneness_name').val(data.cms.name);
                    $('#doneness-id').val(id);
                    $('#visibility').val(data.cms.visibility);
                    $('.list-block').hide();
                    $('.edit-add-block').show();
                }
            },
            complete: function() {
                $('#overlay').hide();
            }
        });
    });
});

   
    
    $('.delete-item').on("click", function() {
            var id = $(this).data('item');
              //alert(id);
            $('#overlay').show();
            $.ajax({
                url: "<?php echo route('doneness_destroy')?>",
                method: "post",
                data: {
                    id: id
                },
                success: function(data) {
                    data = JSON.parse(data);

                        if(data.status == true) {
                            swal({
                                title: "Success",
                                text: "Deleted successfully",
                                timer: 2000,
                                showConfirmButton: false
                            });
                            setTimeout(() => {
                                location.reload();
                            }, 1000);
                        } else {
                            swal({
                                title: "Error",
                                text: data.message,
                                timer: 2000,
                                showConfirmButton: false
                            });
                        }
                },
                complete: function() {
                    $('#overlay').hide();
                }
            });
        });           
</script>
@endpush