<div class="modal" id="pickup_change" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <h2>Change Pickup Location</h2>
            <input type="hidden" value="" id="order_pp_id">
            <div id="pickup_change_contents" class="row">
                <!-- contents -->
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary order-pl-change">Proceed</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </div>
    </div>

@push('scripts')
<script>
    $(document).ready(function() {
        var pickup_points = [];
        pickup_points = {!! $pickup_points !!};
        var user_pickup_point = "{{ $user_pickup_point }}";

        for(var i=0; i < pickup_points.length; i++) {
                $('#pickup_change_contents').append(
                    `<div class="col-sm-6"><button value="${pickup_points[i].name}" 
                    class="btn btn-default change-pl-pickup" 
                    style="margin-left:2px">${pickup_points[i].name}</button></div>`
                );
        }  
    });

    $(document).on('click', '.pp_change_btn', function() {
        var order_id = $(this).attr('order_id');
        $('#order_pp_id').val(order_id);
        var currentItem = $(this).attr('value');
        $('button.change-pl-pickup').removeClass('btn-success');
        $('button.change-pl-pickup[value="'+ currentItem +'"]').addClass('btn-success');
        $('#pickup_change').modal('show');
    });

    $(document).on('click', '.change-pl-pickup',function() {
        $('button.change-pl-pickup').removeClass('btn-success');
        $(this).addClass('btn-success');
    })

    $(document).on('click', '.order-pl-change', function() {
        var order_id = $('#order_pp_id').val();
        $('#overlay').show();
        var pickup = $('.change-pl-pickup.btn-success').attr('value');
        $.ajax({
            url: "<?php echo route('change_pickup')?>",
            type: "post",
            data: {
                order_id: order_id,
                pickup: pickup
            },
            success: function(data) {
                data = JSON.parse(data);
                if(data.status == true) {
                    $('#confirm_status').modal('hide');
                    
                    $('#pl_order_' + order_id).attr('value', pickup);
                    $('.card.active').trigger('click');
                    swal({
                        title: "Success",
                        text: "Status updated successfully",
                        timer: 2000,
                        showConfirmButton: false
                    });
                    $('#pickup_change').modal('hide');
                } else {
                    $('#order_' + order_id).val($('#order_' + order_id).attr('data-selected'));
                    swal({
                        title: "Success",
                        text: data.message,
                        timer: 2000,
                        showConfirmButton: false
                    });
                }
                
            },
            complete: function() {
                $('#overlay').hide();
            }
        });
    });
</script>
@endpush