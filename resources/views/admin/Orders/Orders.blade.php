@extends('admin.layouts.app')
@section('content')
@include('admin.Orders.pickup_status_modal')
@include('admin.Orders.pickup_change')

<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">

            <!-- /# row -->
            <section id="main-content">
                        <iframe style="display:none;"
            type="application/pdf"
            src=""
            id="pdfDocument"
            width="100%"
            height="100%"></iframe>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card list-block">
                            <div class="card-title">
                                <h4 id='title-text-order'>Orders</h4>
                                <div id="deligate" style="display:none;"></div>
                                <h4 class="pull-right asia-dubai" style="margin-right: 3.5%;font-size:25px;"></h4>
                            </div>
                            <div class="invoice-edit">

                                <!--.row-->
                                <div class="invoicelist-body">


                                    <div class="row" id="order-list-tab">
                                        <div class="col-md-4">
                                            <div class="card active">
                                                <div class="stat-widget-one">
                                                    <div class="stat-icon dib"><i class="ti-layout-list-thumb-alt"></i></div>
                                                    <div class="stat-content dib">
                                                        <div class="stat-text">Upcoming Orders</div>
                                                        <input type="hidden" value="UPCOMING">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="card">
                                                <div class="stat-widget-one">
                                                    <div class="stat-icon dib"><i class="ti-shopping-cart-full"></i></div>
                                                    <div class="stat-content dib">
                                                        <div class="stat-text">Ready for Pickup</div>
                                                        <input type="hidden" value="READY_FOR_PICKUP">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="card">
                                                <div class="stat-widget-one">
                                                    <div class="stat-icon dib"><i class="ti-menu-alt"></i></div>
                                                    <div class="stat-content dib">
                                                        <div class="stat-text">All Orders</div>
                                                        <input type="hidden" value="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="bootstrap-data-table-panel">
                                        <div class="table-responsive">
                                            <table class="table table-bordered filter-orders-table">
                                                <tr>
                                                    <td width="30%"></td>
                                                    <td>Date&nbsp;From</td>
                                                    <td><input id="date_from" class="form-control"></td>
                                                    <td>Date&nbsp;To</td>
                                                    <td><input id="date_to" class="form-control"></td>
                                                    <td>
                                                        <button style="cursor:pointer" id="date-filter" class="btn btn-primary">Filter</button>
                                                    </td>
                                                    <td>
                                                    <button style="cursor:pointer" id="clear-filter" class="btn btn-danger">Clear Filter</button>
                                                    </td>
                                                </tr>
                                            </table>
                                            <table id="tbl_orders" class="table table-striped table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Order ID</th>
                                                        <th width="12%">Flight Number</th>
                                                        @if( ! in_array('pickup', $user_has_roles) )
                                                        <th width="12%">Dispatch time</th>
                                                        @else
                                                        <th width="12%">Pickup time</th>
                                                        @endif
                                                        <th width="13%">Pickup Location</th>
                                                        <th width="35%">Order Items</th>
                                                        <th>Change Status</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer">
                            <p>2020 © carryOn DXB Admin.</p>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>


@endsection

@push('scripts')
<script>
    jQuery(document).ready(function () {

        $('#date_to').datepicker({
            dateFormat:"yy-mm-dd",
        });
        $('#date_from').datepicker({
            dateFormat:"yy-mm-dd",
        });

        var table;
        var table_id;

        $('#date-filter').on('click', function() {
            table.ajax.reload();
        });

        $('#clear-filter').on('click', function() {
            $('#date_to').val('');
            $('#date_from').val('');
            table.ajax.reload();
        });
        
        @if( in_array('kitchen', $user_has_roles) )
            localStorage.setItem("ordercount", '0');
            localStorage.setItem("kitchenordercount", '1000000000000000000');
        @endif
        
        @if( in_array('pickup', $user_has_roles) )
            localStorage.setItem("pickupordercount", '1000000000000000000');
        @endif
        
        formatDate();
        setInterval(() => {
            formatDate();
        }, 60000);
        $('body').click();
        
        $('#deligate').on('click', function() {
            var context = new AudioContext()
            context.resume()
            var o = context.createOscillator()
            var  g = context.createGain()
            //o.type = "sine"
            var frequency = 830.6
            o.frequency.value = frequency
            //o.connect(context.destination)
            o.connect(g)
            g.connect(context.destination)
            g.gain.exponentialRampToValueAtTime(0.00001, context.currentTime + 5)
            o.start();
            setTimeout(function(){
                o.stop();
            }, 2000); 
        });


        function formatDate() {
            var d = new Date();
            var localTime = d.getTime();
            var localOffset = d.getTimezoneOffset() * 60000;
            var utc = localTime + localOffset;
            var offset = 4;    //UTC of Dubai is +04.00
            var dubai = utc + (3600000*offset);
            var date = new Date(dubai); 
            
            var hours = date.getHours();
            var minutes = date.getMinutes();
            var ampm = hours >= 12 ? 'PM' : 'AM';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0'+minutes : minutes;
            var strTime = hours + ':' + minutes + ' ' + ampm;
            var montNames = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
            var datef = date.getDate() + " " + montNames[date.getMonth()] + " " + date.getFullYear() + "  " + strTime;

            $('.asia-dubai').text(datef);
        }

        
        $('#order-list-tab .card').on('click', function() {
            $('#order-list-tab .card').removeClass('active');
            $(this).addClass('active');
            table.ajax.reload();
        });

        var typeOrder = 'all';
        var orders_list_type = "<?php echo $type ?>";
        if( orders_list_type != "" ) {
            $('#order-list-tab').hide();
            var titleTextDisplay = orders_list_type == 'DELIVERED' ? 'Delivered ' : 'Cancelled '
            $('.card-title h4#title-text-order').text('Orders ' + titleTextDisplay);
        }

        function buildAjaxData() {
            return {
                    type: typeOrder
                };
        }

        $.ajax({
                url: "<?php echo route('ordercount') ?>",
                type: 'post',
                data: {},
                success: function(data) {
                    //$('#overlay').hide();
                    data = JSON.parse(data);
                    localStorage.setItem("ordercount", data.count);
                    localStorage.setItem("kitchenordercount", data.kitchen_count);
                    localStorage.setItem("pickupordercount", data.pickup_count);
                    return true;
            }
            
        });
        
        showAjaxData();
        
        @if( in_array('kitchen', $user_has_roles) )
        setInterval(() => {
            var active_tab = $('#order-list-tab .card.active input').val(); 
            if (active_tab == 'UPCOMING') {
                $.ajax({
                        url: "<?php echo route('ordercount') ?>",
                        type: 'post',
                        data: {},
                        success: function(data) {
                            //$('#overlay').hide();
                            data = JSON.parse(data);
                            var older_order_count = localStorage.getItem("ordercount");
                            var older_kitchen_order_count = localStorage.getItem("kitchenordercount");
                            if ((data.count != older_order_count) || (parseInt(data.kitchen_count) > parseInt(older_kitchen_order_count))) {
                                $('#deligate').click(); 
                                for(i = 1;i<4; i++) {
                                    setTimeout(function(){
                                        $('#deligate').click();
                                    }, 2500 * i); 
                                }
                             }
                            localStorage.setItem("ordercount", data.count);
                            localStorage.setItem("kitchenordercount", parseInt(data.kitchen_count));
                            return true;
                    }

                });
            }
            table.ajax.reload();
        }, 10000);
        @endif
        
        @if( in_array('pickup', $user_has_roles) )
        setInterval(() => {
            var active_tab = $('#order-list-tab .card.active input').val(); 
            if (active_tab == 'READY_FOR_PICKUP') {
                $.ajax({
                        url: "<?php echo route('ordercount') ?>",
                        type: 'post',
                        data: {},
                        success: function(data) {
                            //$('#overlay').hide();
                            data = JSON.parse(data);
                            var older_pickup_order_count = localStorage.getItem("pickupordercount");
                            if (parseInt(data.pickup_count) > parseInt(older_pickup_order_count)) {
                                $('#deligate').click(); 
                                for(i = 1;i<4; i++) {
                                    setTimeout(function(){
                                        $('#deligate').click();
                                    }, 2500 * i); 
                                }
                             }
                            localStorage.setItem("pickupordercount", parseInt(data.pickup_count));
                            return true;
                    }

                });
            }
            table.ajax.reload();
        }, 10000);
        @endif
        
        @if( !in_array('pickup', $user_has_roles) && !in_array('kitchen', $user_has_roles))
            setInterval(() => {
                if (orders_list_type != 'DELIVERED') {
                    table.ajax.reload();
                }
            }, 10000);
        @endif
        
        function showAjaxData() {
            table_id = "#tbl_orders";
            table = jQuery(table_id).on("preXhr.dt", function (e, settings, data) {
                data.display_type = jQuery("#display_type").val();
                return data;
            }).on('processing.dt', function (e, settings, processing) {
                // ( processing == true ) ? jQuery('.page-loader-wrapper').show() : jQuery('.page-loader-wrapper').hide()
            }).DataTable({
                pageLength: 50,
                processing: true,
                order: [
                    [2, "asc"]
                ],
                // ajax: {
                //     url: '{{ url("admin/orders/show")}}',
                //     type: "POST",
                //     cache: false,
                // },
                serverSide: true,
                fixedColumns: true,

                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": '{{ url("admin/orders/show")}}',
                "fnServerData" : function ( sSource, aoData, fnCallback, oSettings ) {
                    aoData.push(
                            {
                                "name": "status_type", 
                                "value": $('#order-list-tab .card.active input').val() 
                            },
                            {
                                "name": "status_orders",
                                "value": orders_list_type
                            }, {
                                "name": "date_from",
                                "value": $('#date_from').val()
                            },{
                                "name": "date_to",
                                "value": $('#date_to').val()
                            }
                        );
                    oSettings.jqXHR = $.ajax( {
                        "dataType": 'json',
                        "type": "GET",
                        "url": sSource,
                        "data": aoData,
                        "success": fnCallback,
                        "error": function (e) {
                            console.log(e.message);
                        }
                    });
                },

            });
        }

    });
    
    function loadData(url, tab, data) {
      $("#overlay").show();
      @if( isset($order_id) )
        var order_id = {{ $order_id }}
      @else
        var order_id = "";
      @endif
      $.ajax({
        url: url,
        type: 'post',
        data: data,
        success: function(data) {
          data = JSON.parse(data);
          if( tab == 'pdfreport') {
              //data.filepath = data.filepath.replace("./", "public/");
              $('#pdfDocument').attr("src", data.filepath);
              printDocument('pdfDocument');
//                window.open(
//                  data.filepath,
//                  '_blank'
//                );
          }
        $("#overlay").hide();
        },
        complete: function() {
            $("#overlay").hide();
        }
      });
    }
    
    function printDocument(documentId) {
        var doc = document.getElementById(documentId);
//        console.log(doc.content);
        //Wait until PDF is ready to print   
//        console.log(typeof doc.contentWindow.print);
//        if (typeof doc.contentWindow.print === 'undefined') { 
//            //console.log('undefined');
//            setTimeout(function(){printDocument(documentId);}, 1000);
//        } else {
            //console.log('print');
            //doc.contentWindow.print();
            setTimeout(function(){doc.contentWindow.print();}, 1000);
       // }
    }
    $(document).on('click', '.invoice_order_item', function() {
      var order_id = $(this).attr('order_id');
      var dataToSubmit = {"orderid":order_id};   
      loadData("<?php echo route('adminpdfreport'); ?>", 'pdfreport', dataToSubmit);
    });

    $(document).on('click', '.cancel_order_item', function() {
      var order_id = $(this).attr('order_id');
      swal({
            title: "Cacel order",
            text: "Are you sure to cancel this order?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: true,
            showLoaderOnConfirm: true,
        },
        function(confirm){
            if(confirm) {
              $('#overlay').show();
              $.ajax({
                url: "<?php echo route('admin_cancel_order') ?>",
                type: 'post',
                data: {
                  id: order_id
                },
                success: function(data) {
                  data = JSON.parse(data);
                  if(data.status == true) {
                    $('#status_item_' + order_id).text('Cancelled');
                    $('#view_item_' + order_id).text('');
                    setTimeout(() => {
                        swal({
                            title: "Success",
                            text: 'Order cancelled successfully',
                            timer: 3000,
                            showConfirmButton: false
                        });
                    }, 200);
                    $('#order_btn_' + order_id).removeClass('btn-info btn-warning btn-pink btn-success change-stat-modal').addClass('btn-danger').text('Cancelled');
                  } else {
                    show_error(data.message);
                  }
                },
                complete: function() {
                  $('#overlay').hide();
                }
              });
            }
          }
      );
      
    });

    function show_error(message) {
      setTimeout(() => {
        swal({
          title: "CAN'T BE CANCELLED",
          text: message,
          timer: 3000,
          showConfirmButton: false
      });
      }, 200);
    }

    function camelize(text) {
        text = text.replace(/_/g,  ' ').toLowerCase();
        return text.substr(0, 1).toUpperCase() + text.substr(1);
    }

    function change_status(order_id) {
        swal({
            title: "Change status",
            text: "Are you sure to change status to " + camelize($('#order_' + order_id).val()) + "?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: true,
            showLoaderOnConfirm: true,
        },
        function(confirm){
            if(confirm) {
                var status = $('#order_' + order_id).val();
                $('#overlay').show();
                $.ajax({
                    url: "<?php echo route('change_status')?>",
                    type: "post",
                    data: {
                        order_id: order_id,
                        order_status: status
                    },
                    success: function(data) {
                        data = JSON.parse(data);
                        if(data.status == true) {
                            if(data.gate_updated == true) {
                                swal({
                                    title: "Success",
                                    text: "Order gate changed",
                                    timer: 2000,
                                    showConfirmButton: false
                                });
                                setTimeout(() => {
                                    location.reload();
                                }, 2000);
                                return true;
                            }
                            $('#order_' + order_id).attr('data-selected', status);
                            var allStatuses = {
                                    "ORDER_CONFIRMED": "info",
                                    "IN_KITCHEN": "warning",
                                    "READY_FOR_PICKUP": "pink",
                                    "DELIVERED": "success",
                                };
                            var classs = allStatuses[status];
                            $('#order_stat_' + order_id).removeClass('btn-warning btn-info btn-success btn-pink')
                                .addClass('btn-' + classs).text( camelize(status) );
                            swal({
                                title: "Success",
                                text: "Status updated successfully",
                                timer: 2000,
                                showConfirmButton: false
                            });
                        } else {
                            $('#order_' + order_id).val($('#order_' + order_id).attr('data-selected'));
                            swal({
                                title: "Failed",
                                text: data.message,
                                timer: 2000,
                                showConfirmButton: false
                            });
                        }
                        
                    },
                    complete: function() {
                        $('#overlay').hide();
                    }
                });
            } else {
                $('#order_' + order_id).val($('#order_' + order_id).attr('data-selected'));
            }
            
        });
    }

    $(document).on('click', '.reimburse_order_item', function() {
      var order_id = $(this).attr('order_id');
      swal({
            title: "Reimbursement",
            text: "Are you sure to reimburse for this order #"+ order_id +"?",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: true,
            showLoaderOnConfirm: true,
        },
        function(confirm){
            if(confirm) {
              $('#overlay').show();
              $.ajax({
                url: "<?php echo route('admin_reimburse_order') ?>",
                type: 'post',
                data: {
                  id: order_id
                },
                success: function(data) {
                  data = JSON.parse(data);
                  if(data.status == true) {
                    $('#reimburse_' + order_id).text('Reimbursed');
                    setTimeout(() => {
                        swal({
                            title: "Success",
                            text: 'Reimbursed successfully',
                            timer: 3000,
                            showConfirmButton: false
                        });
                    }, 200);
                    $('#order_btn_' + order_id).removeClass('btn-info btn-warning btn-pink btn-success change-stat-modal').addClass('btn-danger').text('Cancelled');
                  } else {
                    show_error(data.message);
                  }
                },
                complete: function() {
                  $('#overlay').hide();
                }
              });
            }
          }
      );
      
    });

</script>
@endpush
