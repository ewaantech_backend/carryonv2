<div class="modal" id="confirm_status" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <h2>Change Status</h2>
            <input type="hidden" value="" id="order_id">
            <div id="status_modal_contents" class="row">
                <!-- contents -->
            </div>
          </div>
          <div class="modal-footer">
            <!-- <span id="confirm-text" style="color:red;display:none;">Are you sure to change? </span> -->
            <!-- <button type="button" class="btn btn-primary order-change">Proceed</button> -->
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </div>
    </div>

@push('scripts')
<script>
    $(document).ready(function() {
        var pickup_points = [];
        pickup_points = {!! $pickup_points !!};
        var user_pickup_point = "{{ $user_pickup_point }}";
        @if( ! in_array('pickup', $user_has_roles) )
        $('#status_modal_contents').append(`<div class="col-sm-6"><button value="ORDER_CONFIRMED" class="btn btn-default change-pickup" style="margin-left:2px">Order Confirmed</button></div>`);
        $('#status_modal_contents').append(`<div class="col-sm-6"><button value="IN_KITCHEN" class="btn btn-default change-pickup" style="margin-left:2px">In Kitchen</button></div>`);
        $('#status_modal_contents').append(`<div class="col-sm-6"><button value="READY_FOR_PICKUP" class="btn btn-default change-pickup" style="margin-left:2px">Ready for Pickup</button></div>`);
        @endif

        for(var i=0; i < pickup_points.length; i++) {
            if( user_pickup_point == '') {
                $('#status_modal_contents').append(`<div class="col-sm-6"><button value="DESTINATION" itemval="At ${pickup_points[i].name}" class="btn btn-default change-pickup" style="margin-left:2px">At ${pickup_points[i].name}</button></div>`);
            } else {
                if( user_pickup_point == pickup_points[i].name ) {
                    // Non default pickup persons can change order to his own location only.
                    $('#status_modal_contents').append(`<div class="col-sm-6"><button value="DESTINATION" itemval="At ${pickup_points[i].name}" class="btn btn-default change-pickup" style="margin-left:2px">At ${pickup_points[i].name}</button></div>`);
                }
            }
        }
        
        @if( ! in_array('kitchen', $user_has_roles) )
        $('#status_modal_contents').append(`<div class="col-sm-6"><button value="DELIVERED" class="btn btn-default change-pickup" style="margin-left:2px">Delivered</button>`);
        @endif
        
      
    });
    //Hide pickup points if not ready for pickup status//
    $(document).on('click', "button[id^='order_btn_']", function() {
        if($(this).attr('value') != "READY_FOR_PICKUP" && $(this).attr('value').indexOf('At ') == -1 ){
          $('[value="DESTINATION"]').parent('div').hide();
        } else {
          $('[value="DESTINATION"]').parent('div').show();
          setTimeout(() => {
            $('[value="DESTINATION"]').parent('div').show();
          }, 200);
        }
        @if( in_array('kitchen', $user_has_roles) )
            $('[value="DESTINATION"]').parent('div').hide();
            setTimeout(() => {
                $('[value="DESTINATION"]').parent('div').hide();
            }, 200);
        @endif
    });
    //Hide pickup points if not ready for pickup status//
    
    // $(document).on('click', '.change-pickup', function() {
    //     $('.change-pickup').removeClass('btn-success').addClass('btn-default');
    //     $(this).addClass('btn-success');
    //     $('#confirm-text').show();
    // });

    function camelize(text) {
            text = text.replace(/_/g,  ' ').toLowerCase();
            return text.substr(0, 1).toUpperCase() + text.substr(1);
        }

    $(document).on('click', '.change-stat-modal', function() {
        var order_id = $(this).attr('order_id');
        $('#order_id').val(order_id);
        var currentItem = $(this).attr('value');
        $('button.change-pickup').removeClass('btn-success');
        if( $.inArray(currentItem, ['ORDER_CONFIRMED', 'IN_KITCHEN', 'READY_FOR_PICKUP', 'DELIVERED']) != -1 ) {
            $('button.change-pickup[value="'+ currentItem +'"]').addClass('btn-success');
        } else {
            $('button.change-pickup[itemval="'+ currentItem +'"]').addClass('btn-success');
        }
        $('#confirm-text').hide();
        $('#confirm_status').modal('show');
    });

    $(document).on('click', '.change-pickup', function() { // order-change
        var order_id = $('#order_id').val();
        @if( in_array('pickup', $user_has_roles) )
            var pickup = true;
        @else
            var pickup = false;
        @endif
        var change_possible = $('#order_btn_' + order_id).attr('value') == 'READY_FOR_PICKUP' ||
                                $('#order_btn_' + order_id).attr('value').indexOf('At ') != -1;
        if( pickup == true && change_possible == false ) {
            swal({
                title: "Error",
                text: "You can only change orders in 'Ready for pickup'",
                timer: 2000,
                showConfirmButton: false
            });
            return;
        }
        $('.change-pickup').removeClass('btn-success').addClass('btn-default');
        $(this).addClass('btn-success');
        $('#overlay').show();
        
        var status = $('.change-pickup.btn-success').attr('value');
        var text = $('.change-pickup.btn-success').text();
        $.ajax({
            url: "<?php echo route('change_status')?>",
            type: "post",
            data: {
                order_id: order_id,
                order_status: status,
                text: text
            },
            success: function(data) {
                data = JSON.parse(data);
                if(data.status == true) {
                    $('#confirm_status').modal('hide');
                    $('#order_' + order_id).attr('data-selected', status);
                    var allStatuses = {
                            "ORDER_CONFIRMED": "info",
                            "IN_KITCHEN": "warning",
                            "READY_FOR_PICKUP": "pink",
                            "DELIVERED": "success",
                        };
                    var classs = allStatuses[status];
                    status = status == 'DESTINATION' ? text : status;
                    $('#order_btn_' + order_id).removeClass('btn-warning btn-info btn-success btn-pink')
                        .addClass('btn-' + classs).text( camelize(status) ).attr('value', status);
                    $('.card.active').trigger('click');
                    swal({
                        title: "Success",
                        text: "Status updated successfully",
                        timer: 2000,
                        showConfirmButton: false
                    });
                } else {
                    $('#order_' + order_id).val($('#order_' + order_id).attr('data-selected'));
                    swal({
                        title: "Failed",
                        text: data.message,
                        timer: 2000,
                        showConfirmButton: false
                    });
                }
                
            },
            complete: function() {
                $('#overlay').hide();
            }
        });
    });
</script>
@endpush