@extends('admin.layouts.app')

@section('content')
@include('admin.Orders.pickup_status_modal')
<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
            <!-- /# row -->
            <section id="main-content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="">
                            <div class="card">
                                <div class="card-title">
                                    <h2>
                                        <button style="cursor:pointer" onclick="location.href='{{ asset('/admin/'.$redirect_back) }}'" 
                                        class="btn btn-success"><< Back </button> {{ $orderid_encoded }}

                                        @foreach ($orders as $order)
                                        <input type="hidden" id="order_id" value="{{ $order_id }}">
                                        <span class="pull-right" style="font-size:18px;">Change Status:
                                    @php
                                    $st = $order->status;
                                    
                                    if($st == 'READY_FOR_PICKUP' && $order->pickup_reached_at != NULL) {
                                        $st = $order->pickup_reached_at;
                                    }
                                    @endphp
                                    <button id="order_btn_{{$order_id}}" order_id="{{$order_id}}" type="button" value="{{ $st }}"
                                        class="change-stat-modal btn btn-info btn-flat m-b-10 m-l-5">{{ ucwords(strtolower(str_replace('_', ' ', $st) ) ) }}</button>
                                    <button order_id="{{$order_id}}" type="button"
                                        class="cancel_order_item btn btn-primary btn-flat m-b-10 m-l-5">Cancel order</button>
                                    </span>
                                    @endforeach
                                    </h2>
                                    
                                </div>


                                <div class="tab-content">

                                    <div role="tabpanel" class="tab-pane active" id="1">
                                        <div class="contact-information">
                                            <h4 style="border-top: 1px solid #cac5c5;">Customer information</h4>
                                            <div class="phone-content">
                                                <span class="contact-title">Name:</span>
                                                <span class="cust-name">{{ $customer_details->name }}</span>
                                            </div>
                                            <div class="phone-content">
                                                <span class="contact-title">Phone:</span>
                                                <span class="cust-number">{{ $customer_details->phone_number }}</span>
                                            </div>
                                            <div class="email-content">
                                                <span class="contact-title">Email:</span>
                                                <span class="cust-contact_email">{{ $customer_details->email }}</span>
                                            </div>
                                            <div class="phone-content">
                                                <span class="contact-title">Billing Name:</span>
                                                <span class="cust-billing_name">{{ $customer_details->billing_name }}</span>
                                            </div>
                                            <div class="phone-content">
                                                <span class="contact-title">Billing Street, Street No:</span>
                                                <span class="cust-billing_name">{{ $customer_details->billing_street }}, {{ $customer_details->billing_street_no }}</span>
                                            </div>
                                            <div class="phone-content">
                                                <span class="contact-title">Billing Apartment No:</span>
                                                <span class="cust-billing_name">{{ $customer_details->billing_apartment_no }}</span>
                                            </div>
                                            <div class="address-content">
                                                <span class="contact-title">Billing City, Country:</span>
                                                <span class="cust-mail_address">
                                                {{ $customer_details->billing_city }}, {{ $customer_details->billing_country }}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <div class="card-body">

                                    <div class="card">
                                        <div class="card-title">
                                            <h4>Order Details</h4>
                                        </div>
                                        <div class="todo-list">
                                            <div class="tdl-holder">
                                                <div class="tdl-content">


                                                @foreach ($orders as $order)
                                                <div class="row">
                                                    <div class="col-lg-3">
                                                        <div class="card bg-primary">
                                                        <div class="stat-widget-six">
                                                            <div class="stat-icon">
                                                            <i class="ti-stats-up"></i>
                                                            </div>
                                                            <div class="stat-content">
                                                            <div class="text-left dib">
                                                                <div class="stat-heading">Flight&nbsp;#</div>
                                                                <div class="stat-text">{{ $order->flight_number }}</div>
                                                            </div>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="card bg-primary">
                                                        <div class="stat-widget-six">
                                                            <div class="stat-icon">
                                                            <i class="ti-time"></i>
                                                            </div>
                                                            <div class="stat-content">
                                                            <div class="text-left dib">
                                                                <div class="stat-heading"> Departure</div>
                                                                <div class="stat-text">{{ date_format_custom($order->departure) }}</div>
                                                            </div>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="card bg-primary">
                                                        <div class="stat-widget-six">
                                                            <div class="stat-icon">
                                                            <i class="ti-shift-right"></i>
                                                            </div>
                                                            <div class="stat-content">
                                                            <div class="text-left dib">
                                                                <div class="stat-heading">Duration</div>
                                                                <div class="stat-text">{{time_format_custom( $order->duration, ' Hrs' ) }}</div>
                                                            </div>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <div class="card bg-primary">
                                                        <div class="stat-widget-six">
                                                            <div class="stat-icon">
                                                            <i class="ti-bolt-alt"></i>
                                                            </div>
                                                            <div class="stat-content">
                                                                <div class="text-left dib">
                                                                    <div class="stat-heading">Pickup&nbsp;Point</div>
                                                                    <div class="stat-text">{{ $order->pickup_point }}</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </div>
                                                    </div>
                                                    @endforeach

                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card">
                                        <div class="card-title">
                                            <h4>Order History</h4>
                                        </div>
                                        <div class="todo-list">
                                            <div class="tdl-holder">
                                                <div class="tdl-content">
                                                    <div class="table-responsive">
                                                        <table class="table">
                                                            <thead contenteditable style="background: #a4b3fe;">
                                                                <tr>
                                                                    <th>Order Changes</th>
                                                                    <th>Date and Time</th>
                                                                    <th></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                            @foreach ($orders as $order)
                                                                <tr>
                                                                    <td> Order Created</td>
                                                                    <td>{{ $order->created_at }}</td>
                                                                    <td></td>
                                                                </tr>    
                                                            @endforeach
                                                            @foreach ($orderlog as $orderl)
                                                                <tr>
                                                                    <td> {{ $orderl->status }}</td>
                                                                    <td>{{ $orderl->created_at }}</td>
                                                                    <td></td>
                                                                </tr>    
                                                            @endforeach

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="card">
                                        <div class="card-title">
                                            <h4>Order Items</h4>
                                        </div>
                                        <div class="todo-list">
                                            <div class="tdl-holder">
                                                <div class="tdl-content">
                                                    <div class="table-responsive">
                                                        <table class="table">
                                                            <thead contenteditable style="background: #a4b3fe;">
                                                                <tr>
                                                                    <th>Item</th>
                                                                    <th>Unit Price</th>
                                                                    <th>Quantity</th>
                                                                    <th>Total(Including VAT)</th>
                                                                    <!-- <th>Sub total</th> -->
                                                                    <!-- <th>tax amount</th> -->
                                                                    <th></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            {!! $content !!}
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    

                                    @php
                                        $freshness = $orders[0]->freshness;
                                        $taste = $orders[0]->taste;
                                        $veriety = $orders[0]->variety;
                                        $ordering_process = $orders[0]->ordering_process;
                                        $comment = $orders[0]->comment;
                                        $hide_review = ! $freshness && ! $veriety && ! $ordering_process && ! $taste;
                                    @endphp

                                    <div class="card-body" @if($hide_review) style="display:none;" @endif>
                                        <div class="card">
                                            <div class="card-title">
                                                <h4>Customer review</h4>
                                            </div>
                                            <div class="todo-list">
                                                <div class="tdl-holder">
                                                    <div class="tdl-content">
                                                    <div class="row review-order">


                                                        <div class="col-lg-3">
                                                            <div class="card p-0">
                                                                <div class="stat-widget-three">
                                                                    <div class="stat-icon bg-success">
                                                                        <i class="ti-wand"></i>
                                                                    </div>
                                                                    <div class="stat-content">
                                                                        <div class="stat-digit">Freshness</div>
                                                                        <div class="stat-text">
                                                                            @foreach([1,2,3,4,5] as $inc)
                                                                            <i class="ti-star @if($freshness==0) no-review @endif @if($freshness>=$inc) active @endif"></i>
                                                                            @endforeach
                                                                            @if( ! $freshness) NA @else {{ $freshness }}/5 @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            </div>

                                                            <div class="col-lg-3">
                                                            <div class="card p-0">
                                                                <div class="stat-widget-three">
                                                                    <div class="stat-icon bg-success">
                                                                        <i class="ti-heart"></i>
                                                                    </div>
                                                                    <div class="stat-content">
                                                                        <div class="stat-digit">Taste</div>
                                                                        <div class="stat-text">
                                                                            @foreach([1,2,3,4,5] as $inc)
                                                                            <i class="ti-star @if($taste==0) no-review @endif @if($taste>=$inc) active @endif"></i>
                                                                            @endforeach
                                                                            @if( ! $taste) NA @else {{ $taste }}/5 @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            </div>

                                                            <div class="col-lg-3">
                                                            <div class="card p-0">
                                                                <div class="stat-widget-three">
                                                                    <div class="stat-icon bg-success">
                                                                        <i class="ti-crown"></i>
                                                                    </div>
                                                                    <div class="stat-content">
                                                                        <div class="stat-digit">Veriety</div>
                                                                        <div class="stat-text">
                                                                            @foreach([1,2,3,4,5] as $inc)
                                                                            <i class="ti-star @if($veriety==0) no-review @endif @if($veriety>=$inc) active @endif"></i>
                                                                            @endforeach
                                                                            @if( ! $veriety) N/A @else {{ $veriety }}/5 @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            </div>

                                                            
                                                            <div class="col-lg-3">
                                                            <div class="card p-0">
                                                                <div class="stat-widget-three">
                                                                    <div class="stat-icon bg-success">
                                                                        <i class="ti-write"></i>
                                                                    </div>
                                                                    <div class="stat-content">
                                                                        <div class="stat-digit">Ordering Process</div>
                                                                        <div class="stat-text">
                                                                            @foreach([1,2,3,4,5] as $inc)
                                                                            <i class="ti-star @if($ordering_process==0) no-review @endif @if($ordering_process>=$inc) active @endif"></i>
                                                                            @endforeach
                                                                            @if( ! $ordering_process) NA @else {{ $ordering_process }}/5 @endif
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-12" @if( ! $comment) style="display:none;" @endif>
                                                        <div class="card p-0">
                                                                <div class="stat-widget-three">
                                                                    <div class="stat-icon bg-success">
                                                                        <i class="ti-comment"></i>
                                                                    </div>
                                                                    <div class="stat-content">
                                                                        <div class="stat-digit">Comment:</div>
                                                                        <div class="stat-text">
                                                                            {{ $comment }}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>



                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <div class="col-lg-12 ml-auto">
                                        <button style="cursor:pointer" onclick="location.href='{{ asset('/admin/'.$redirect_back) }}'" class="pull-left btn btn-success"><< Back</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- /# card -->
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->

                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer">
                            <p>2020 © carryOn DXB Admin.</p>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

@endsection


@push('scripts')


<script>
    $(document).ready(function() {
        function camelize(text) {
            text = text.replace(/_/g,  ' ').toLowerCase();
            return text.substr(0, 1).toUpperCase() + text.substr(1);
        }

        $('#order_status').on('change', function(e) {
            e.preventDefault();
            order_id = $('#order_id').val();
            var status = $('#order_status').val();
            swal({
                title: "Change status",
                text: "Are you sure to change status to " + camelize(status) + "?",
                type: "info",
                showCancelButton: true,
                closeOnConfirm: true,
                showLoaderOnConfirm: true,
            },
            function(confirm){
                if(confirm) {
                    $('#overlay').show();
                    $.ajax({
                        url: "<?php echo route('change_status')?>",
                        type: "post",
                        data: {
                            order_id: order_id,
                            order_status: status
                        },
                        success: function(data) {
                            data = JSON.parse(data);
                            if(data.status == true) {

                                if(data.gate_updated == true) {
                                    swal({
                                        title: "Success",
                                        text: "Order gate changed",
                                        timer: 2000,
                                        showConfirmButton: false
                                    });
                                    setTimeout(() => {
                                        location.reload();
                                    }, 2000);
                                    return true;
                                }
                                
                                $('#order_status').attr('data-selected', status);
                                setTimeout(() => {
                                    swal({
                                        title: "Success",
                                        text: "Status updated successfully",
                                        timer: 2000,
                                        showConfirmButton: false
                                    });
                                }, 200);
                            } else {
                                $('#order_status').val($('#order_status').data('selected'));
                                swal({
                                    title: "Failed",
                                    text: data.message,
                                    timer: 2000,
                                    showConfirmButton: false
                                });
                            }
                            
                        },
                        complete: function() {
                            $('#overlay').hide();
                        }
                    });
                } else {
                    $('#order_status').val($('#order_status').attr('data-selected'));
                }
                
            });
        });

        $(document).on('click', '.cancel_order_item', function() {
            var order_id = $(this).attr('order_id');
            swal({
                    title: "Cacel order",
                    text: "Are you sure to cancel this order?",
                    type: "info",
                    showCancelButton: true,
                    closeOnConfirm: true,
                    showLoaderOnConfirm: true,
                },
                function(confirm){
                    if(confirm) {
                    $('#overlay').show();
                    $.ajax({
                        url: "<?php echo route('admin_cancel_order') ?>",
                        type: 'post',
                        data: {
                        id: order_id
                        },
                        success: function(data) {
                        data = JSON.parse(data);
                        if(data.status == true) {
                            $('#status_item_' + order_id).text('Cancelled');
                            $('#view_item_' + order_id).text('');
                            setTimeout(() => {
                                swal({
                                    title: "SUCCESS",
                                    text: 'Order cancelled successfully',
                                    timer: 3000,
                                    showConfirmButton: false
                                });
                            }, 200);
                            $('#order_btn_' + order_id).removeClass('btn-info btn-warning btn-pink btn-success change-stat-modal').addClass('btn-danger').text('Cancelled');
                        } else {
                            show_error(data.message);
                        }
                        },
                        complete: function() {
                        $('#overlay').hide();
                        }
                    });
                    }
                }
            );
            
        });

        function show_error(message) {
            setTimeout(() => {
                swal({
                title: "CAN'T BE CANCELLED",
                text: message,
                timer: 3000,
                showConfirmButton: false
            });
            }, 200);
        }

    });
</script>
@endpush
