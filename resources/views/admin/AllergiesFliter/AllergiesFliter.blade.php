@extends('admin.layouts.app')
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
      $(document).ready(function(){
         setTimeout(function(){
          $("#success_message").remove();
             }, 2000);
      });
    </script>

    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Menu</h1>
                            </div>
                        </div>
                    </div>
                 </div>
                   @if (session('status'))
                      <div class="alert alert-success" id="success_message" role="alert">
                        {{ session('status') }}
                      </div>
                    @endif
                    <section id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-title">
                                    <h4>Menu Fliter List </h4>
                                </div>
                             
                                <div class="text-right">
                               <a href="{{asset('/admin/allergies_fliter/create')}}"> <button type="button" class="btn btn-info btn-sm m-b-10 m-l-5">+Add New</button></a>
                               </div>
                                <div class="bootstrap-data-table-panel">
                                    <div class="table-responsive">
                                        <table id="row-select" class="display table table-borderd table-hover ">
                                            <thead>
                                                <tr>
                                                <th>No</th>
                                                <th>Name</th>
                                                <th>Description</th>   
                                                <th>Visibility</th>  
                                                <th>Position</th> 
                                                <th>Created Date</th>
                                                <th>Action</th>
                                               
                                                </tr>
                                            </thead>
                                            <tbody>
                                    @if (count($allergiesfliter) > 0)
                                    @php
                                    $count = 1;
                                    @endphp
                                    @foreach ($allergiesfliter as $allergiey)
                                                <tr>
                                                <td>{{$count++}}</td>
                                                <td>{{$allergiey->name}}</td>
                                                <td>{{$allergiey->description}}</td>
                                                <td>{{$allergiey->visibility}}</td>
                                                <td>{{$allergiey->position}}</td>
                                                <td>{{ date_format_custom($allergiey->created_at) }}</td>
                                                <td><a href="{{url('admin/allergies_fliter/edit',$allergiey->id)}}"><i style='color:blue;' class="ti-pencil-alt "></span></i></a>
                                                <a href="{{url('admin/allergies_fliter/delete',$allergiey->id)}}"><i style='margin-left: 10px;color:red;' class="ti-trash "></a></td>
                                                </tr>
                                  @endforeach
                                  @else
                                    <tr>
                                        <td colspan="4" class="text-center"> No Allergies Available</td>
                                    </tr>
                                  @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                         </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

</div>
 @endsection

 <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
      $(document).ready(function(){
         setTimeout(function(){
          $("#success_message").remove();
            }, 3000);
      });
    </script> -->