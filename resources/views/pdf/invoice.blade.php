<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <style>
        * {
            font-size: 12px;
            font-family: 'Helvetica';
        }
        td,
        th,
        tr,
        table {
            border-top: 1px solid black;
            border-collapse: collapse;
        }

        td.description,
        th.description {
            width: 110px;
            max-width: 110px;
        }
        
        td.total,
        th.total {
            width: 75px;
            max-width: 75px;
        }

        td.quantity,
        th.quantity {
            width: 30px;
            max-width: 30px;
            word-break: break-all;
        }
        
        td.empty,
        th.empty {
            width: 0px;
            max-width: 0px;
            word-break: break-all;
        }

        td.price,
        th.price {
            width: 70px;
            max-width: 70px;
            word-break: break-all;
        }
        
        td.totalprice,
        th.totalprice {
            width: 90px;
            max-width: 90px;
            word-break: break-all;
        }
        
        td.adjust,
        th.adjust {
            width: 190px;
            max-width: 190px;
        }
        
        td.nametitle,
        th.nametitle {
            width: 70px;
            max-width: 70px;
        }
        
        .centered {
            text-align: center;
            align-content: center;
            margin-left: 10px;
        }
        
        .titleplacement {
            text-align: center;
            align-content: center;
            margin-left: 22px;
        }
        
        .alignleft {
            text-align: left;
            align-content: left;
            margin-left: 20px;           
        }
        
        .left {
            text-align: left;
            align-content: left;
        }

        .ticket {
            width: 173px;
            max-width: 173px;
        }

        img {
            max-width: inherit;
            width: inherit;
        }

        @media print {
            .hidden-print,
            .hidden-print * {
                display: none !important;
            }
        }
        </style>
        <script>
            const $btnPrint = document.querySelector("#btnPrint");
            $btnPrint.addEventListener("click", () => {
                window.print();
            });
        </script>
        <title>Invoice</title>
    </head>
    <body>
        <div class="ticket" style="margin-top:-45px;">
            <p class="titleplacement">
            <img src="images/logo/logo_black_and_white.png" alt="Logo" />                
            </p>
<!--            <p class="titleplacement">
                <b>CARRYON DXB</b>
                <br>-------------------------         
                <br><span class="left">Order No:&nbsp;{!! $invID !!}</span>
                <br><span class="alignleft">Customer Name: {!! $customername !!}</span>
            </p>-->
            <table style="border-top: none !important;margin-left:5px !important;">
                <thead style="border-top: none !important;">
                    <tr style="border-top: none !important;">
                        <th style="border-top: none !important;font-size: 13px !important;width: 110px !important;" class="nametitle">Order Id</th>
                        <th style="border-top: none !important;font-size: 13px !important;width: 110px !important;" class="price">&nbsp;&nbsp;:{!! $invID !!}</th>
                    </tr>
                </thead>
            </table>
            <table style="border-top: none !important;margin-left:5px !important;">
                <thead style="border-top: none !important;">
                    <tr style="border-top: none !important;">
                        <th style="border-top: none !important;font-size: 13px !important;width: 110px !important;" class="nametitle">Customer Name</th>
                        <th style="border-top: none !important;font-size: 13px !important;width: 110px !important;" class="price">&nbsp;&nbsp;:{!! $customername !!}</th>
                    </tr>
                </thead>
            </table>
            <table style="border-top: none !important;margin-left:5px !important;">
                <thead style="border-top: none !important;">
                    <tr style="border-top: none !important;margin-left:0px !important;">
                        <th style="border-top: none !important;font-size: 13px !important;width: 110px !important;" class="nametitle">Pickup Location</th>
                        <th style="border-top: none !important;font-size: 13px !important;width: 110px !important;" class="price">&nbsp;&nbsp;:{!! $pickup_loc !!}</th>
                    </tr>
                </thead>
            </table>
            <table style="border-top: none !important;margin-left:5px !important;">
                <thead style="border-top: none !important;">
                    <tr style="border-top: none !important;margin-left:0px !important;">
                        <th style="border-top: none !important;font-size: 13px !important;width: 110px !important;" class="nametitle">Payment Mode</th>
                        <th style="border-top: none !important;font-size: 13px !important;width: 110px !important;" class="price">&nbsp;&nbsp;:{!! $payment_mode !!}</th>
                    </tr>
                </thead>
            </table>
            <table style="border-top: none !important;margin-left:5px !important;">
                <thead style="border-top: none !important;margin-left:5px !important;">
                    <tr style="border-top: none !important;">
                        <th style="border-top: none !important;font-size: 13px !important;width: 110px !important;" class="nametitle">Flight No</th>
                        <th style="border-top: none !important;font-size: 13px !important;width: 110px !important;" class="price">&nbsp;&nbsp;:{!! $flight_no !!}</th>
                    </tr>
                </thead>
            </table>
            <table style="border-top: none !important;margin-left:5px !important;">
                <thead style="border-top: none !important;margin-left:5px !important;">
                    <tr style="border-top: none !important;">
                        <th style="border-top: none !important;font-size: 13px !important;width: 110px !important;" class="nametitle">Dispatch Time</th>
                        <th style="border-top: none !important;font-size: 13px !important;text-align:left;width: 110px !important;" class="price">&nbsp;&nbsp;:{!! $dispatch_time !!}</th>
                    </tr>
<!--                    <tr style="border-top: none !important;">
                        <th style="border-top: none !important;font-size: 10px !important;text-align: center;" >
                            <img style="width:65px;height:65px;margin-left:95px;" src="{{ $qr_code }}" />
                        </th>
                    </tr>-->
                </thead>
            </table>
            <p class="titleplacement">
            <table>
                <thead>
                    <tr>
                        <th class="description">Item</th>
                        <th class="quantity" style="text-align: center;">Qty</th>
                        <th class="quantity" style="text-align: left;">Unit Price</th>
                        <th class="price" style="text-align: left;">Price</th>
                    </tr>
                </thead>
                <tbody>
                    {!! $content !!}
                </tbody>
            </table>
            <table>
                <thead>
                    <tr>
                        <th class="empty">&nbsp;</th>
                        <th class="adjust" style="text-align: right;">Total(Incl VAT)</th>
                        <th class="totalprice" style="text-align: left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{!! $total !!} AED</th>
                    </tr>
                </thead>
            </table>
            <?php
                // if (!isset($is_lounge)) {
                //     $is_lounge = 'N';
                // }
                // if ($is_lounge != 'Y') {
            ?>
            <table style="border-top: none !important;margin-left:20px !important;">
                <thead style="border-top: none !important;margin-left:30px !important;">
                    <tr style="border-top: none !important;">
                        <th style="border-top: none !important;font-size: 10px !important;text-align: center;" >
                            <img style="width:130px;height:130px;margin-left:35px;" src="{{ $qr_code }}" />
                        </th>
                    </tr>
                </thead>
            </table>
            <?php
                // }
            ?>
            <p class="centered" style="font-size:12px !important;margin-bottom: 2px !important;">
                You are now fueled up!<br />
                Safe Journey!<br /><br />
                <span style="font-size:10px !important;">
                    Dubai International Airport <br />
                    United Arab Emirates <br />
                    +971&nbsp;(0)&nbsp;50&nbsp;298&nbsp;7966<br />
                    hello@carryondxb.com
                </span>
            </p>

        </div>
<!--        <button id="btnPrint" class="hidden-print">Print</button>-->
    </body>
</html>