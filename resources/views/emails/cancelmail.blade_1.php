<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<table bgcolor="#525659" cellpadding="0" cellspacing="0" width="100%">
	<tbody>
		<tr>
			<td>
			<table align="center" bgcolor="#ffffff" cellpadding="0" cellspacing="0" width="720">
				<tbody>
					<tr>
						<td align="center" valign="top">
						<table border="0" cellpadding="20" cellspacing="0" width="100%">
							<tbody>
								<tr>
									<td align="center" bgcolor="#f5f7ec"><a href="#" target="_blank"><img src="https://www.carryondxb.com/images/logo-email.png" style="width: 220px;" /> </a></td>
								</tr>
								<tr>
									<td align="center" bgcolor="#dc5d20"><font face="arial" style="font-size: 32px;color: #ffffff;">Order Cancelled</font></td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td align="center" valign="top">
						<table style="font-size:14px;color:#404041;" width="620">
							<tbody>
								<tr>
									<td height="40"> </td>
								</tr>
								<tr>
									<td height="20"> </td>
								</tr>
								<tr>
									<td align="center"><font face="arial" style="font-size: 18px;color: #4c7f3d;">Order {{ $order_id }} has been cancelled </font></td>
								</tr>
                                <tr>
									<td height="20"> </td>
								</tr>
								<tr>
									<td height="30"  align="center">
                                        This is to bring to your kind attention that the customer recently cancelled order {{ $order_id }}.<br>
                                        Kindly do the reimbursement earliest possible. <a style="cursor:pointer;color:#dc5d20;" href="{{ $url_link }}">View Orders</a>
                                    </td>
								</tr>
								
								<tr>
									<td height="30" style="border-bottom:1px dashed #bbca85;"> </td>
								</tr>
								<tr>
									<td height="20"> </td>
								</tr>
								<tr>
									<td height="60"> </td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<tr>
						<td height="50"> </td>
					</tr>
					<tr>
						<td height="40"> </td>
					</tr>
					<tr>
						<td bgcolor="#f5f7ec">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tbody>
								<tr>
									<td height="40"> </td>
								</tr>
								<tr>
									<td align="center">
									<table border="0" cellpadding="0" cellspacing="5" width="75%">
										<tbody>
											<tr>
												<td style="text-align:center;font-size: 13px;color:#000;"><font face="arial">451 Lounge, Airport Terminal 3, Dubai, United Arab Emirates</font></td>
											</tr>
											<tr>
												<td style="text-align:center;font-size: 13px;color:#000;"><font face="arial"><strong>Telephone:</strong> +971 4 12345678</font></td>
											</tr>
										</tbody>
									</table>
									</td>
								</tr>
								<tr>
									<td height="30"> </td>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>