<script type="text/javascript">
    $(document).ready(function() {


        var r_try_time = 10;
        var pg_interval = setInterval(() => {
            console.log('asd')
            r_try_time -= 1;
            if (r_try_time > 0) {
                $('#retry_pg').text(`Retry in ${r_try_time}  sec...`);
            } else {
                $('#retry_pg').html('<button onclick="location.reload();" class="btn button1">Retry</button>');
                clearintrval();
            }
        }, 1000);

        function clearintrval() {
            window.clearInterval(pg_interval);
        }

        $('html,body').animate({
            scrollTop: $("#online_payment").offset().top
        }, 'slow');
        $('.pay_proceed').hide();
        setTimeout(() => {
            $('#flight_popup button.close').trigger('click');
        }, 200);
        @if(Session::has('message'))
        $('#overlay').show();
        setTimeout(() => {
            $('#overlay').hide();
            alert("{{ Session::get('message') }}");
        }, 2000);
        @endif
        @if(Session::has('code'))
        if ("{{ Session::get('code') }}" == 'PAYED_OK') {
            setTimeout(() => {
                location.href = "<?php echo route('home_page') ?>";
            }, 4000);
            localStorage.setItem("flight", 'N/A');
            localStorage.setItem("flight_date", 'N/A');
            localStorage.setItem("can_order", false);
            localStorage.setItem("cart_key", '');
        }
        @endif

        cart_list_side_view();
        var flight_date = localStorage.getItem("flight_date");
        var flight = localStorage.getItem("flight");
        $('.flight_number_sidebar').html('<span>Flight number:</span>' + flight);
        $('.flight_deliver_date_sidebar').html('<span>Flight date:</span>' + flight_date);




        function modalFunc(productID, qty, addonData, type, operation) {
            $(".addon_add").click(function() {
                let addonObj = {};
                val = $("#val_" + $(this).attr("addon_id")).val();
                val = parseInt(val) + 1;
                $("#val_" + $(this).attr("addon_id")).val(val);
                let addonPrice = (sessionStorage.getItem("addonPrice") === null) ? 0 : sessionStorage.getItem("addonPrice") ;
                sessionStorage.setItem("addonPrice", parseInt(addonPrice) + parseInt($(this).attr("addon_price")) );
                    $("#addon_price").text(sessionStorage.getItem("addonPrice")+' AED');
                if (addonData.length === 0) {
                    addonObj.addonID = $(this).attr("addon_id");
                    addonObj.count = val;
                    addonData.push(addonObj);
                } else {
                    let found = 0;
                    addonData.map((data, key) => {
                        if (parseInt(data.addonID) === parseInt($(this).attr("addon_id"))) {
                            addonData[key]['count'] = val;
                            found = 1;
                        }
                    });
                    if (found === 0) {
                        addonObj.addonID = $(this).attr("addon_id");
                        addonObj.count = val;
                        addonData.push(addonObj);
                    }

                }
            });

            $(".addon_less").click(function() {
                let addonObj = {};
                let val = $("#val_" + $(this).attr("addon_id")).val();
                if (parseInt(val) !== 0) {
                    val = parseInt(val) - 1;
                    $("#val_" + $(this).attr("addon_id")).val(val);
                    let addonPrice = (sessionStorage.getItem("addonPrice") === null) ? 0 : sessionStorage.getItem("addonPrice") ;
                    sessionStorage.setItem("addonPrice", parseInt(addonPrice) - parseInt($(this).attr("addon_price")) );
                    $("#addon_price").text(sessionStorage.getItem("addonPrice")+' AED');
                    if (addonData.length === 0) {
                        addonObj.addonID = $(this).attr("addon_id");
                        addonObj.count = val;
                        addonData.push(addonObj);
                    } else {
                        let found = 0;
                        addonData.map((data, key) => {
                            if (parseInt(data.addonID) === parseInt($(this).attr("addon_id"))) {
                                addonData[key]['count'] = val;
                                found = 1;
                            }
                        });

                        if (found === 0) {
                            addonObj.addonID = $(this).attr("addon_id");
                            addonObj.count = val;
                            addonData.push(addonObj);
                        }
                    }
                }
            });

            $("#addon_save").click(function() {
                if (!isNaN(qty) && qty >= 0) {
                    addItemToCart(productID, qty + 1, addonData, type, operation, function() {
                        addonData.splice(0, addonData.length);
                        $('.product_id_' + productID).find('input.input-control').val(qty + 1);
                        $('.add-item-cart').attr('item_count', qty + 1);
                        $('.product_id_' + productID).removeClass('active');
                        if (qty + 1 > 0) {
                            $('.product_id_' + productID).addClass('active');
                        }
                        sessionStorage.removeItem("addonPrice");
                    });
                }
            });
        }

        function addItemToCart(product_id, newQty, addonData, type, operation, cartItemID, callBack) {
            $('#overlay').show();
            $.ajax({
                url: "<?php echo route('cart_add') ?>",
                method: "post",
                data: {
                    productsid: product_id,
                    item_count: newQty,
                    key: localStorage.getItem('cart_key'),
                    addons: addonData,
                    type: type,
                    operation: operation,
                    cart_item_id: cartItemID
                },
                success: function(data) {

                    data = JSON.parse(data);
                    if (data.status == true) {
                        if (typeof data.cartkey != 'undefined') {
                            localStorage.setItem("cart_key", data.cartkey);
                        }
                        alert('Your cart updated');
                        cart_list_side_view();
                        callBack();
                    } else {
                        alert(data.message);
                    }
                },
                complete: function() {
                    $('#overlay').hide();
                }
            });
        }

        function sideon_cart_list(product_id, product_name, item_count_item_total, grant_total_item_total, path, addon_cart_string, cart_item_id, addons_total) {
            grant_total_item_total = parseFloat(grant_total_item_total);
            if (addon_cart_string !== 'Nil') {
                return `<tr>
            <td class="product-image">
            <figure class="image">
            <img class="w-100" src="${path}" alt="img">
            </figure>
            
            </td>
            <td class="product-details">
            <h4>${product_name}
            <small></small>
            </h4>
            <ul class="quantity">
            <li><button product_id=${product_id} type="button" data-type="cart" data-cart-item-id="${cart_item_id}" class="less"><i class="fa fa-minus" aria-hidden="true"></i></button></li>
            <li><input product_id=${product_id} type="number" class="input-control" value="${parseInt(item_count_item_total)}"></li>
            <li><button product_id=${product_id} type="button" data-type="cart" data-cart-item-id="${cart_item_id}" class="add"><i class="fa fa-plus" aria-hidden="true"></i></button></li>
            </ul>
            </td>
            <td class="product-price">
            AED <strong>${grant_total_item_total}</strong>
            </td>
            <tr>
            <td colspan="3">
                <div class="customize">
                    <h5>Extra Items</h5>
                    <p>${addon_cart_string}</p>
                    <a href="javascript:void(0)" data-qty="${item_count_item_total}" data-product-id="${product_id}" data-cart-item-id="${cart_item_id}" class="customize btn button1">
                        <i  class="fa fa-pencil" aria-hidden="true">
                        </i>Customize
                    </a>
                </div>
            </td>
            </tr>
            </tr>`;
            } else {
                return `<tr>
            <td class="product-image">
            <figure class="image">
            <img class="w-100" src="${path}" alt="img">
            </figure>
                
            </td>
            <td class="product-details">
            <h4>${product_name}
            <small></small>
            </h4>
            <ul class="quantity">
            <li><button product_id=${product_id} type="button" data-type="cart" data-cart-item-id="${cart_item_id}" class="less"><i class="fa fa-minus" aria-hidden="true"></i></button></li>
            <li><input product_id=${product_id} type="number" class="input-control" value="${item_count_item_total}"></li>
            <li><button product_id=${product_id} type="button" data-cart-item-id="${cart_item_id}" data-type="cart" class="add"><i class="fa fa-plus" aria-hidden="true"></i></button></li>
            </ul>
            </td>
            <td class="product-price">
            AED <strong>${grant_total_item_total}</strong>
            </td>
            </tr>`;
            }

        }


    });
    window.onload = function() {
                                    sessionStorage.removeItem("addonPrice");
                      };
</script>