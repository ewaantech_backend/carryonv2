<!DOCTYPE html>
<html>

<head>
  <!-- Meta Tags -->
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width" />
  <meta name="description" content="" />
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <!-- Page Title & Favicon -->
  <title>Carry On</title>
  <!-- Stylesheets -->
  <link rel="stylesheet" href="{{ asset('css/frontend/bootstrap.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('css/frontend/font-awesome.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('css/frontend/aos-min.css') }}" />
  <link rel="stylesheet" href="{{ asset('css/frontend/jquery-ui.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('css/frontend/jquery-ui.theme.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('css/frontend/main.css') }}" />
  <link rel="stylesheet" href="{{asset('css/frontend/main2.css')}}" /> <!-- new -->
  <link rel="stylesheet" href="{{ asset('css/frontend/custom.css') }}" />
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
  <!-- Javascript -->
  <script src="{{ asset('js/vendor/head.core.js') }}"></script>
</head>

<body>
  <!--[if lt IE 10]>
        <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please upgrade your browser to improve your experience.</p>
  <![endif]-->
  <div id="container">
    <!-- header -->
    @include('frontend.layouts.headermenu')

    @include('frontend.layouts.footer')

    @include('frontend.layouts.popup')
    <!-- /header -->

    <!-- content -->
    <div id="content">
      <!-- section -->
      <div class="wrapper">
        <div class="container">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item"><a href="#">Our Menu </a></li>
              <li class="breadcrumb-item"><a href="#">Breakfast</a></li>
              <li class="breadcrumb-item active" aria-current="page">Gluten free omlet</li>
            </ol>
          </nav>
          <div class="row">
            <div class="col-lg-8 column">
              <h2>Your Information</h2>
              <div class="customer-information">
                <ul>
                  <li>
                    <span>Name:</span>{{ Session::get('user_name')}}
                  </li>
                  <li class="flight_number_sidebar">
                    <span>Flight No:</span>{{ Session::get('flight_number')}}
                  </li>
                  <li>
                    <span>Email:</span>{{ Session::get('user_email')}}
                  </li>
                  <li>
                    <span>Flight Date:</span>{{ date_format_custom(Session::get('flight_date'), '', 'Y-m-d' ) }}
                  </li>
                  <li>
                    <span>Mobile Number:</span>{{ Session::get('user_phone')}}
                  </li>
                  <li>
                    <span>Flight Time:</span>{{ date_format_custom(Session::get('flight_date'), '', 'H:i' ) }}
                  </li>
                </ul>
              </div>
              <h2>Billing Information</h2>
              <div class="billing-information">
                <div class="form">
                  <ul class="filled">
                    <li class="half">
                      <p>
                        <span>Name:</span>{{ Session::get('user_name')}}
                      </p>
                    </li>
                    <li class="half">
                      <p>
                        <span>City:</span>{{ Session::get('billing_city')}}
                      </p>
                    </li>
                    
                    <li class="half">
                      <p>
                        <span>Street:</span>{{ Session::get('billing_street')}}
                      </p>
                    </li>
                    <li class="half">
                      <p>
                        <span>Street Number:</span>{{ Session::get('billing_street_number')}}
                      </p>
                    </li>
                    <li class="half">
                      <p>
                        <span>Apartment Number:</span>{{ Session::get('billing_apartment_number')}}
                      </p>
                    </li>
                    <li class="half">
                      <p>
                        <span>ZIP:</span>{{ Session::get('billing_zip')}}
                      </p>
                    </li>
                    <li class="half">
                      <p>
                        <span>Country:</span>{{ Session::get('billing_country')}}
                      </p>
                    </li>
                  </ul>
                </div>
              </div>
              <h2 id="online_payment">Online Payment</h2>
              <div class="payment-information">
                @if( isset($status) && $status == TRUE)
                  <div id="telr-div" style="padding-top: 25px;background: #fff;">
                      <iframe frameBorder="0" id="telr" src={{ $url }} style="width: 100%;height: 500px;"></iframe>
                  </div>
                @else
                  <div>
                    <span>Payment gateway unavailable. <font id="retry_pg">Retry in 10  sec...</font></span>
                  </div>
                @endif
              </div>
            </div>
            <div class="col-lg-4 column pt-5">
              <div class="widgets">
                <div class="widget flight-block">
                  <div class="header">
                    <h3>Your CarryOn Details</h3>
                    <!-- <a href="#" class="toggle-btn"></a> -->
                  </div>
                  @include('frontend.layouts.flightinfo_order_side')
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /section -->
    </div>
    <!-- /content -->

    <!-- footer -->
    @include('frontend.layouts.footermenu')

    @include('frontend.checkout.checkout_payment_script')

    <button class="btn button1 btn-block view-basket" data-toggle="modal" data-target="#basket">view basket</button>
    @include('frontend.layouts.basket_popup')
    
    <!-- /footer -->
    <!-- Javascript -->
    <!-- <script src="assets/javascripts/vendor/jquery-3.2.1.min.js"></script>
    <script src="assets/javascripts/vendor/jquery-ui-1.9.2.custom.min.js"></script>
    <script src="assets/javascripts/vendor/jquery.matchHeight-min.js"></script>
    <script src="assets/javascripts/vendor/popper.min.js"></script>
    <script src="assets/javascripts/vendor/bootstrap.min.js"></script>
    <script src="assets/javascripts/vendor/ofi.browser.js"></script>
    <script src="assets/javascripts/vendor/jquery.cycle.all.min.js"></script>
    <script src="assets/javascripts/vendor/headroom.min.js"></script>
    <script src="assets/javascripts/vendor/jQuery.headroom.js"></script>
    <script src="assets/javascripts/vendor/aos-min.js"></script>
    <script src="assets/javascripts/main.js"></script> -->
  </div>
</body>

</html>