<script type="text/javascript">
    $(document).ready(function() {

        @if(Session::has('message'))
        setTimeout(() => {
            alert("{{ Session::get('message') }}");
        }, 2000);
        @endif
        @if(Session::has('code'))
        if ("{{ Session::get('code') }}" == 'PAYED_OK') {
            localStorage.setItem("flight", 'N/A');
            localStorage.setItem("flight_date", 'N/A');
            localStorage.setItem("can_order", false);
            localStorage.setItem("cart_key", '');
        }
        @endif

        var can_order = localStorage.getItem("can_order");
        var flight_date = localStorage.getItem("flight_date");
        var flight = localStorage.getItem("flight");
        if (can_order && !$.inArray(flight, ['N/A', 'null', null, '']) != -1) {

            var flight_updated_at = localStorage.getItem("flight_updated_at");
            if (flight_updated_at != null && new Date() - new Date(flight_updated_at) > 3600000) {
                localStorage.setItem("flight", 'N/A');
                localStorage.setItem("flight_date", 'N/A');
                localStorage.setItem("can_order", 0);
                localStorage.setItem("cart_key", "");
            }

            $('.flight_number_sidebar').html('<span>Flight number:</span>' + flight);
            $('.flight_deliver_date_sidebar').html('<span>Flight date:</span>' + flight_date);
        }

        var url = $(location).attr('href'),
            parts = url.split("/"),
            last_part = parts[parts.length - 1];

        if (last_part == "") {
            $('#menu_link').removeClass('current');
            $('#home_link').addClass('current');
            setTimeout(() => {
                $('#flight_popup').show();
            }, 50);
            setTimeout(() => {
                $('#flight_popup').show();
            }, 200);
        } else {
            $('#home_link').removeClass('current');
            $('#menu_link').addClass('current');
        }

        $('#flight_popup').on('click', 'button.close', function() {
            if (last_part == "") {
                // Display flight popup in home page even if its closed.
                $('#flight_popup').show();
            }
        });

        $('#logout').click(function() {
            $.ajax({
                url: "{{url('/customer/logout')}}",
                dataType: "json",
                data: {},
                success: function(data) {
                    alert(data.message);
                    localStorage.setItem("flight", 'N/A');
                    localStorage.setItem("flight_date", 'N/A');
                    localStorage.setItem("can_order", false);
                    localStorage.setItem("cart_key", '');
                    setTimeout(() => {
                        location.href = "<?php echo route('home_page') ?>";
                    }, 200);
                }
            });
        });


        @if(isset($categories))

        @if(count($categories) > 0)
        getProducts('{{$categories[0]->id}}', 1);
        @endif

        $(".category").click(function() {
            getProducts($(this).attr("data-category"), $(this).attr("data-key"));
        });

        function getProducts(category_id, key) {
            $.ajax({
                url: "{{url('/product/show')}}",
                dataType: "JSON",
                data: {
                    category: category_id
                },
                success: function(data) {
                    var html = `<div class="tab-pane fade show active" id="tab` + key + `" role="tabpanel" aria-labelledby="tab` + key + `-tab">
                        <ul class="menu-list">
                            `;
                    $.each(data, function() {
                        var allergy_html = '';
                        allergy_html += `<footer>
                                        <ul class="flavour-list">`;
                        $.each(this.allergies, function() {
                            allergy_html += `<li>
                                        <a data-toggle="tooltip" title="${this.description}">` +
                                `<img src="{{asset('${this.image_path}')}}" alt="img">` +
                                `</a>
                                    </li>`;
                        });
                        allergy_html += `</ul>
                <span class="price">${this.actual_price} <small>AED</small></span>
                </footer>`;
                        html +=
                            `<li>
                    <div class="food alt">
                        <div class="image">
                            <a href="{{asset('/productdetail/${this.url_key}')}}"><img style="height:315px;" class="w-100" src="${this.product_image_path}"
                                    alt="img"></a>
                        </div>
                        <div class="info">
                            <h2><a href="{{asset('/productdetail/${this.url_key}')}}">` + this.product_name + `</a></h2>` + allergy_html + `
                            <div class="btn-wrapper">
                                <a href="{{asset('/productdetail/${this.url_key}')}}" class="btn btn-outline-secondary">Add to your CarryOn</a>
                            </div>
                        </div>
                    </div>
                </li>`;
                    });
                    html += `</ul>
                    <footer class="text-center">
                       <a href="<?php echo route('our_menu') ?>" class="link">View our menu <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
                    </footer>
                </div>`;
                    $('#myTabContent').html(html);
                }
            });

        }
        var flag = true;
        setTimeout(function() {
            var categoryfromurl = window.location.hash.substr(1);

            if (categoryfromurl != "" && flag == true) {
                datakey = 'data-key=' + categoryfromurl;
                $("[" + datakey + "]").trigger("click");
                flag = false;
            }
        }, 500);

        @endif


        function flightAdd() {
            if (localStorage.getItem('can_order') === null || localStorage.getItem('can_order') === 'null') {
                $('#flight_popup').show();
                $('.trigger-order').trigger('click');
                alert('Please add flight information');
                $('#basket button').trigger('click');
                return false;
            }

            if (localStorage.getItem('can_order') == false || localStorage.getItem('can_order') == 'false') {
                $('#flight_popup').show();
                $('.trigger-order').trigger('click');
                alert('Please update flight information');
                $('#basket button.toggle-btn').trigger('click');
                return false;
            }

            return true;
        }

        $(document).on('click', '.add-item-cart', function() {
            console.log('here');
            console.log($(this).attr("class"));
            if (flightAdd() == false) {
                return false;
            }
            var qty = $('.add-item-cart').attr('item_count');
            var product_id = $(this).attr('product_id');
            if ($(this).attr("class") == 'btn button1 add-item-cart'){
                getAddons(product_id, parseInt(qty), 'update');
            }
            if ($(this).attr("class") == 'btn button1 add-item-cart-addon'){
                
                addItemToCart(productID, qty + 1, addonData, type, operation, cartItemID, function() {
                        addonData.splice(0, addonData.length);
                    });
            }
            
        });

        async function getAddons(productID, qty, type) {
            let productObj = {
                products_id: productID,
            };
            try {
                let response = await fetch("<?php echo route('addon_list') ?>", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json;charset=utf-8",
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    body: JSON.stringify(productObj)
                });
                let result = await response.json();
                if (result.data.length > 0) {
                    let modalBody = "";
                    let addonCount = 0;
                    let productName = '';
                    var product_path = "";
                    result.data.map((data) => {
                        if (result.type === 'update' && data.item_count !== null) {
                            addonCount = data.item_count;
                        }
                        product_path = data.product_path;
                        modalBody += `

                        <li>
                            <span>${data.name}
                            <strong><span>AED </span> ${data.addon_price} </strong>
                            </span>
                            <ul class="quantity">
                                <li>
                                    <button addon_price="${data.addon_price}"  addon_id="${data.addon_id}" id="addon_less_${data.addon_id}" class="addon_less lessaddon"></button>
                                </li>
                                <li>
                                    <input  addon_id="${data.addon_id}" id="val_${data.addon_id}" type="number" class="addon_value input-control" value="${addonCount}">
                                </li>
                                <li>
                                    <button addon_price="${data.addon_price}"  addon_id="${data.addon_id}" id="addon_add_${data.addon_id}" class="addon_add addaddons"></button>
                                </li>
                            </ul>
                        </li>
                       `;

                        productName = data.product_name;
                    });
                   
                    $("#addon").css({"padding-right" : "17px", "display": "block"});
                    $("#addons_product_name").html(productName);
                    $("#addon .addon-list").html(modalBody);
                    $("#popupimage").html(`<img src={{asset('${product_path}')}} alt="">`);
                    //$("#addons-extra-style").html(`<div class="modal-backdrop fade"></div>`)
                    $("#addon").modal();
                    modalFunc(productID, qty, [], type, 'insertion', ' ');
                } else {
                    if (!isNaN(qty) && qty >= 0) {
                        addItemToCart(productID, qty + 1, [], '', 'insertion', ' ', function() {

                        });
                    }
                }

            } catch (error) {
                console.log(error);
            }
        }


        function addItemToCart(product_id, newQty, addonData, type, operation, cartItemID, callBack) {
            $('#overlay').show();
            $.ajax({
                url: "<?php echo route('cart_add') ?>",
                method: "post",
                data: {
                    productsid: product_id,
                    item_count: newQty,
                    key: localStorage.getItem('cart_key'),
                    addons: addonData,
                    type: type,
                    operation: operation,
                    cart_item_id: cartItemID
                },
                success: function(data) {

                    data = JSON.parse(data);
                    if (data.status == true) {
                        if (typeof data.cartkey != 'undefined') {
                            localStorage.setItem("cart_key", data.cartkey);
                        }
                        alert('Your cart updated');
                        cart_list_side_view();
                        callBack();
                    } else {
                        alert(data.message);
                    }
                },
                complete: function() {
                    $('#overlay').hide();
                }
            });
        }

        function cart_list_side_view() {
            $('#overlay').show();
            $('.pay_proceed').addClass('disabled');
            $.ajax({
                url: "<?php echo route('cart_list') ?>",
                method: "post",
                data: {
                    key: localStorage.getItem('cart_key')
                },
                success: function(data) {
                    //data = JSON.parse(data);
                    var sideon_cart_string = '';
                    if (data.status == true) {
                        $('input.input-control').val(0);
                        $('.add-item-cart').attr('item_count', 0);
                        $('.quantity').removeClass('active');
                        for (var inc = 0; inc < data.eachdata.length; inc++) {

                            // start - addons of the product
                            let addon_cart_string = '';
                            let total_product_price = parseFloat(data.eachdata[inc].grant_total_item_total);
                            let addons_total = 0;

                            if (data.eachdata[inc].addon.length > 0) {
                                var addons_list = [];
                                data.eachdata[inc].addon.map((addon) => {
                                    total_product_price += parseFloat(addon.addon_total_price);
                                    addons_total += parseFloat(addon.addon_total_price);
                                    // addon_cart_string = addon_cart_string + addon.addon_name + ', ';
                                    addons_list.push(addon.addon_name);
                                });
                                addon_cart_string = addons_list.join();
                            } else {
                                addon_cart_string = 'Nil';
                            }
                            // end - addons of the product

                            sideon_cart_string += sideon_cart_list(
                                data.eachdata[inc].product_id,
                                data.eachdata[inc].product_name,
                                data.eachdata[inc].item_count_item_total,
                                data.eachdata[inc].grant_total_item_total,
                                data.eachdata[inc].image_path,
                                addon_cart_string,
                                data.eachdata[inc].id,
                                addons_total
                            );

                            $('.product_id_' + data.eachdata[inc].product_id).find('input.input-control').val(data.eachdata[inc].total_products_count);
                            $('.add-item-cart').attr('item_count', data.eachdata[inc].total_products_count);
                            $('.product_id_' + data.eachdata[inc].product_id).removeClass('active');
                            if (parseInt(data.eachdata[inc].total_products_count) > 0) {
                                $('.product_id_' + data.eachdata[inc].product_id).addClass('active');
                            }
                        }



                        $('.sideon-cart-list').html(sideon_cart_string);
                        $(".customize").click(function() {
                            $('#overlay').show();
                            $("#basket").modal("hide");
                            getAddonsEdit($(this).attr("data-product-id"), $(this).attr("data-cart-item-id"), parseInt($(this).attr("data-qty"))).
                            then((response) => {
                                $('#overlay').hide();
                            });
                        });

                        if (data.eachdata.length == 0) {
                            $('.pay_proceed').addClass('disabled');
                            var url = $(location).attr('href'),
                                parts = url.split("/"),
                                last_part = parts[parts.length - 1];
                            if (last_part == "checkout" || last_part == "payment" || last_part == "billingaddress") {
                                setTimeout(() => {
                                    location.href = "<?php echo route('our_menu') ?>";
                                }, 2000);
                                // if no items redirect to menu.
                                alert('No items in cart');
                            }
                            $('.sideon-cart-list').html('<tr><td class="no-items-cart">No Items added!</td></tr>');
                            $('.sub_total').text('0.00');
                            $('.tax_total').text('0.00');
                            $('.grand_total').text('AED 0');
                            $('.total_cart_item_count').text(0);
                        } else {
                            $('.pay_proceed').removeClass('disabled');
                            $('.sub_total').text(data.maindata.sub_total_total);
                            $('.tax_total').text(data.maindata.tax_total_total);
                            grant_total_item_total = parseFloat(data.maindata.grant_total_total);
                            $('.grand_total').html('<span>AED</span>' + grant_total_item_total);
                            $('.total_cart_item_count').text(data.maindata.item_count_total);
                            $('.total_cart_item_count').attr('data-total-cart_item-count', data.maindata.item_count_total);
                        }
                    } else {
                        $('.sideon-cart-list').html('<tr><td class="no-items-cart">No Items added!</td></tr>');
                        if (data.message != 'Key is not passed')
                            alert(data.message);
                    }
                },
                complete: function() {
                    $('#overlay').hide();
                }
            });
        }

        async function getAddonsEdit(productID, cartItemID, qty) {
            if (productID !== undefined && cartItemID !== undefined){
                try {
                let addonsObj = {
                    product_id: productID,
                    cart_item_id: cartItemID
                }
                let response = await fetch("<?php echo route('cart_addon') ?>", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json;charset=utf-8",
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    body: JSON.stringify(addonsObj)
                });
                let result = await response.json();
                let addonData = [];
                let total = 0;
                let modalBody = "<ul class='addon-list'>";
                let productName = "";
                var product_path = "";
                result.addons.map((data) => {
                    if (data.item_count == null) {
                        data.item_count = 0;
                    }
                    product_path = data.product_path
                    let addonObj = {};
                    total += (parseInt(data.addon_price) * parseInt(data.item_count));
                    modalBody += `

                        <li>
                            <span>${data.name}
                            <strong><span>AED </span> ${data.addon_price} </strong>
                            </span>
                            <ul class="quantity">
                                <li>
                                    <button addon_price="${data.addon_price}" addon_id="${data.addon_id}" id="addon_less_${data.addon_id}" class="addon_less lessaddon"></button>
                                </li>
                                <li>
                                    <input  addon_id="${data.addon_id}" id="val_${data.addon_id}" type="number" class="addon_value input-control" value="${parseInt(data.item_count)}">
                                </li>
                                <li>
                                    <button addon_price="${data.addon_price}"  addon_id="${data.addon_id}" id="addon_add_${data.addon_id}" class="addon_add addaddons"></button>
                                </li>
                            </ul>
                        </li>

                            `;
                    addonObj.addonID = data.addon_id;
                    addonObj.count = parseInt(data.item_count);
                    addonData.push(addonObj);
                    productName = data.product_name;
                });
                modalBody += "</ul>"
                $("#addon").css({"padding-right" : "17px", "display": "block"});
                $("#addon .addon-list").html(modalBody);
                $("#addons_product_name").html(productName)
                $("#addon_price").attr("data-price", total);
                $("#popupimage").html(`<img src={{asset('${product_path}')}} alt="">`);
                sessionStorage.setItem("addonPrice", parseInt(total));
                $("#addon_price").text(total+' AED');
                //$("#addons-extra-style").html(`<div class="modal-backdrop fade"></div>`)
                $("#addon").modal();
                modalFunc(productID, qty, addonData, '', '', cartItemID);
                return result;
            } catch (error) {
                console.log(error);
            }
            }
            
        }


        function modalFunc(productID, qty, addonData, type, operation, cartItemID) {
            $(".addon_add").click(function() {
                let addonObj = {};
                val = $("#val_" + $(this).attr("addon_id")).val();
                val = parseInt(val) + 1;
                $("#val_" + $(this).attr("addon_id")).val(val);
                let addonPrice = (sessionStorage.getItem("addonPrice") === null) ? 0 : sessionStorage.getItem("addonPrice") ;
                sessionStorage.setItem("addonPrice", parseInt(addonPrice) + parseInt($(this).attr("addon_price")) );
                $("#addon_price").text(sessionStorage.getItem("addonPrice")+' AED');
                if (addonData.length === 0) {
                    addonObj.addonID = $(this).attr("addon_id");
                    addonObj.count = val;
                    addonData.push(addonObj);
                } else {
                    let found = 0;
                    addonData.map((data, key) => {
                        if (parseInt(data.addonID) === parseInt($(this).attr("addon_id"))) {
                            addonData[key]['count'] = val;
                            found = 1;
                        }
                    });
                    if (found === 0) {
                        addonObj.addonID = $(this).attr("addon_id");
                        addonObj.count = val;
                        addonData.push(addonObj);
                    }

                }
            });

            $(".addon_less").click(function() {
                let addonObj = {};
                let val = $("#val_" + $(this).attr("addon_id")).val();
                if (parseInt(val) !== 0) {
                    let addonPrice = (sessionStorage.getItem("addonPrice") === null) ? 0 : sessionStorage.getItem("addonPrice") ;
                    sessionStorage.setItem("addonPrice", parseInt(addonPrice) - parseInt($(this).attr("addon_price")) );
                    $("#addon_price").text(sessionStorage.getItem("addonPrice")+' AED');
                    val = parseInt(val) - 1;
                    $("#val_" + $(this).attr("addon_id")).val(val);
                    
                    if (addonData.length === 0) {
                        addonObj.addonID = $(this).attr("addon_id");
                        addonObj.count = val;
                        addonData.push(addonObj);
                    } else {
                        let found = 0;
                        addonData.map((data, key) => {
                            if (parseInt(data.addonID) === parseInt($(this).attr("addon_id"))) {
                                addonData[key]['count'] = val;
                                found = 1;
                            }
                        });

                        if (found === 0) {
                            addonObj.addonID = $(this).attr("addon_id");
                            addonObj.count = val;
                            addonData.push(addonObj);
                        }
                    }
                }
            });

            $("#addon_save").click(function() {
                if (!isNaN(qty) && qty >= 0) {
                    addItemToCart(productID, qty + 1, addonData, type, operation, cartItemID, function() {
                        addonData.splice(0, addonData.length);
                        sessionStorage.removeItem("addonPrice");
                        $("#addon_price").text( "0 AED");
                        $("#addon_price").attr("data-price", 0)
                    });
                }
            });

            $("#close_addon").click(function(){
                sessionStorage.removeItem("addonPrice");
                $("#addon_price").text( "0 AED" );
                $("#addon_price").attr("data-price", 0)
            });
        }

        function sideon_cart_list(product_id, product_name, item_count_item_total, grant_total_item_total, path, addon_cart_string, cart_item_id, addons_total) {
            grant_total_item_total = parseFloat(grant_total_item_total);
            if (addon_cart_string !== 'Nil') {
                return `<tr>
            <td class="product-image">
            <figure class="image">
            <img class="w-100" src="${path}" alt="img">
            </figure>
            
            </td>
            <td class="product-details">
            <h4>${product_name}
            <small></small>
            </h4>
            <ul class="quantity">
            <li><button product_id=${product_id} type="button" data-type="cart" data-cart-item-id="${cart_item_id}" class="less"><i class="fa fa-minus" aria-hidden="true"></i></button></li>
            <li><input product_id=${product_id} type="number" class="input-control" value="${parseInt(item_count_item_total)}"></li>
            <li><button product_id=${product_id} type="button" data-type="cart" data-cart-item-id="${cart_item_id}" class="add"><i class="fa fa-plus" aria-hidden="true"></i></button></li>
            </ul>
            </td>
            <td class="product-price">
            AED <strong>${grant_total_item_total}</strong>
            </td>
            <tr>
            <td colspan="3">
                <div class="customize">
                    <h5>Extra Items</h5>
                    <p>${addon_cart_string}</p>
                    <a href="javascript:void(0)" data-qty="${item_count_item_total}" data-product-id="${product_id}" data-cart-item-id="${cart_item_id}" class="customize btn button1">
                        <i  class="fa fa-pencil" aria-hidden="true">
                        </i>Customize
                    </a>
                </div>
            </td>
            </tr>
            </tr>`;
            } else {
                return `<tr>
            <td class="product-image">
            <figure class="image">
            <img class="w-100" src="${path}" alt="img">
            </figure>
            
            </td>
            <td class="product-details">
            <h4>${product_name}
            <small></small>
            </h4>
            <ul class="quantity">
            <li><button product_id=${product_id} type="button" data-type="cart" data-cart-item-id="${cart_item_id}" class="less"><i class="fa fa-minus" aria-hidden="true"></i></button></li>
            <li><input product_id=${product_id} type="number" class="input-control" value="${item_count_item_total}"></li>
            <li><button product_id=${product_id} type="button" data-cart-item-id="${cart_item_id}" data-type="cart" class="add"><i class="fa fa-plus" aria-hidden="true"></i></button></li>
            </ul>
            </td>
            <td class="product-price">
            AED <strong>${grant_total_item_total}</strong>
            </td>
            </tr>`;
            }

        }




        $(document).on('click', '.add', function() {
            if (flightAdd() == false) {
                return false;
            }

            var $qty = $(this).parents('.quantity').find('.input-control');
            var product_id = $(this).attr('product_id');
            let type = $(this).attr("data-type");
            let cartItemID = $(this).attr('data-cart-item-id');
            if (!isNaN(parseInt($qty.val()))) {
                if (type == 'cart') {
                    addItemToCart(product_id, parseInt($qty.val()) - 1, [], ' ', 'insertion', cartItemID, function() {});
                } else {
                    getAddons(product_id, parseInt($qty.val()), 'update');
                }
            }
        });

        $(document).on('click', '.less', function() {
            if (flightAdd() == false) {
                return false;
            }

            var $qty = $(this).parents('.quantity').find('.input-control');
            var product_id = $(this).attr('product_id');
            let type = $(this).attr("data-type");
            let cartItemID = $(this).attr('data-cart-item-id');
            if (!isNaN(parseInt($qty.val())) && parseInt($qty.val()) > 0) {
                if (type == 'cart') {
                    addItemToCart(product_id, parseInt($qty.val()) - 1, [], ' ', 'removal', cartItemID, function() {});
                } else {
                    lessMenu(product_id).then((result) => {
                        if (result.data.length > 0) {
                            alert("Please update it from the cart")
                        } else {
                            addItemToCart(product_id, parseInt($qty.val()) - 1, [], ' ', 'removal', ' ', function() {});
                        }
                    });
                }
            }
        });

        async function lessMenu(product_id) {
            try {
                let productObj = {
                    products_id: product_id,
                };
                let response = await fetch("<?php echo route('addon_list') ?>", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json;charset=utf-8",
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    body: JSON.stringify(productObj)
                });
                let result = await response.json();
                return result;

            } catch (error) {
                console.log(error);
            }

        }

        $(document).on('focusout', '.quantity .input-control', function() {
            if (flightAdd() == false) {
                return false;
            }
            var $qty = $(this);
            var product_id = $(this).attr('product_id');
            var currentVal = parseInt($qty.val());
            if (!isNaN(currentVal) && currentVal > 0) {
                var newQty = currentVal;
                addItemToCart(product_id, newQty, [], 'update', ' ', ' ', function() {

                });
            }
        });

        var timeoutInMiliseconds = 3600000;
        var timeoutId;

        function startTimer() {
            timeoutId = window.setTimeout(doInactive, timeoutInMiliseconds)
        }

        function doInactive() {
            console.log('cleared');
            localStorage.setItem("can_order", 0);
            localStorage.setItem("flight", 'N/A');
            localStorage.setItem("flight_date", 'N/A');
        }

        function resetTimer() {
            window.clearTimeout(timeoutId)
            startTimer();
        }

        function setupTimers() {
            document.addEventListener("mousemove", resetTimer, false);
            document.addEventListener("mousedown", resetTimer, false);
            document.addEventListener("keypress", resetTimer, false);
            document.addEventListener("touchmove", resetTimer, false);
            startTimer();
        }
        setupTimers();
    });
    window.onload = function() {
                                    sessionStorage.removeItem("addonPrice");
                      };
</script>