@extends('frontend.layouts.app')

@section('content')
<div class="controls">
  <div class="container"></div>
</div>
<div id="content">
  <!-- section -->
  <div class="section">
    <div class="container">
      <header class="text-center">
        <h2 class="text-primary">Our food</h2>
        <p>Healthy, clean, nutrient-dense meals we absolutely love!</p>
      </header>
      @if (count($categories) > 1)
      <div class="tabs1">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
          @foreach ($categories as $key => $category)
          @if ($key == 0)
          <li class="nav-item">
            <a class="category active" id="tab" data-key="{{$key+1}}" data-category="{{$category->id}}"
              data-toggle="tab" href="#tab{{$key+1}}" role="tab" aria-controls="tab{{$key+1}}"
              aria-selected="true">{{$category->name}}</a>
          </li>
          @else
          <li class="nav-item">
            <a class="category" id="tab{{$key+2}}-tab" data-key="{{$key+1}}" data-category="{{$category->id}}"
              data-toggle="tab" href="#tab{{$key+1}}" role="tab" aria-controls="tab{{$key+1}}"
              aria-selected="false">{{$category->name}}</a>
          </li>
          @endif
          @endforeach

        </ul>
        <div class="tab-content" id="myTabContent">

        </div>
      </div>
    </div>
  </div>
  
  @endif

  
@endsection


