    <!-- /footer -->
    <!-- Javascript -->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script type="text/Javascript">
      $(document).ready(function(){
                 //disabling past date from datepicker
             var nowDate = new Date();
             var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
             //initializing datepicker
             $("#flight_date_dt, flight_date_dt_new").removeClass('hasDatepicker');
             $("#flight_date_dt").datepicker({
               minDate: today,
               dateFormat: 'dd-mm-yy',
                 onSelect: function(dateText) {
                     var res = dateText.split("-");
                     dateText = res[1]+'/'+res[0]+'/'+res[2]; 
                      checkFlight(dateText);
                     $(".flight_date").removeClass('textbox error');
                 }
             });
             $("#flight_date_dt_new").datepicker({
               minDate: today,
               dateFormat: 'dd-mm-yy',
                 onSelect: function(dateText) {
                     var res = dateText.split("-");
                     dateText = res[1]+'/'+res[0]+'/'+res[2]; 
                     checkFlightNew(dateText);
                     $(".flight_date_new").removeClass('textbox error');
                 }
             });

             $('#flight, flight_new').on('input', function() {
               $(".flight_date").val('');
               $(".flight_date_new").val('');
             });
             
         function checkFlightNew(dateText) {
           var flight = $("#flight_new").val();
           var flight_date = $(".flight_date_new").val();
           if(flight == ''){
               $("#flight_new").addClass('textbox error').focus();
               return false;
           }
           if(flight_date == ''){
               $(".flight_date_new").addClass('textbox error');
               return false;
           }
           $("#flight_new").removeClass('textbox error');
           $('#overlay').show();
           $("#timecheck_error_new").text("");
           $.ajax( {
             url: "{{url('/flight/show')}}",
             dataType: "json",
             data: {
               fl_no: flight,
               date: dateText,
               fl_rnd: Math.random()
             },
             success: function( data ) {
               if(data.status == true) {
                 $("#timecheck_error_new").text("");
                 $(".flight-info").html("Selected flight - "+ data.flight);
                   $("#flight_data_new").html(`Destination: ${data.destination}<br>
                                         Departure time: ${data.departure}\n`).
                   css('padding', '10px').css('box-shadow', ' 0 0 8px 1px #ddd').show();
               } else {
                 if(data.message == 'time elapsed') {
                   $("#timecheck_error_new").html(`We can only accept orders 6 hours before departure time. So sorry!`);
                 } else {
                   $("#timecheck_error_new").html(data.message);
                 }
                 $(".flight-info").html("No flight added.");
                 $('#flight_data_new').hide();
               }
             },
             complete: function() {
               $('#overlay').hide();
             }
           });
         }

         function checkFlight(dateText) {
           var flight = $("#flight").val();
           var flight_date = $(".flight_date").val();
           if(flight == ''){
               $("#flight").addClass('textbox error').focus();
               return false;
           }
           if(flight_date == ''){
               $(".flight_date").addClass('textbox error');
               return false;
           }
           $("#flight").removeClass('textbox error');
           $('#overlay').show();
           $("#timecheck_error").text("");
           $.ajax( {
             url: "{{url('/flight/show')}}",
             dataType: "json",
             data: {
               fl_no: flight,
               date: dateText,
               fl_rnd: Math.random()
             },
             success: function( data ) {
               if(data.status == true) {
                 $("#timecheck_error").text("");
                 $(".flight-info").html("Selected flight - "+ data.flight);
                 $('#flight_data').html(`Destination: ${data.destination}<br>
                                         Departure time: ${data.departure}\n`).
                   css('padding', '10px').css('box-shadow', ' 0 0 8px 1px #ddd').show();
               } else {
                 if(data.message == 'time elapsed') {
                   $("#timecheck_error").html(`We can only accept orders 6 hours before departure time. So sorry!`);
                 } else {
                   $("#timecheck_error").html(data.message);
                 }
                 $(".flight-info").html("No flight added.");
                 $('#flight_data').hide();
               }
             },
             complete: function() {
               $('#overlay').hide();
             }
           });
         }
         });
      </script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script src="{{asset('js/vendor/jquery-3.2.1.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
<!--    <script src="{{asset('js/vendor/jquery-ui-1.9.2.custom.min.js')}}"></script>-->
    <script src="{{asset('js/vendor/jquery.matchHeight-min.js')}}"></script>
    <script src="{{asset('js/vendor/popper.min.js')}}"></script>
    <script src="{{asset('js/vendor/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/vendor/ofi.browser.js')}}"></script>
    <script src="{{asset('js/vendor/jquery.cycle.all.min.js')}}"></script>
    <script src="{{asset('js/vendor/headroom.min.js')}}"></script>
    <script src="{{asset('js/vendor/jQuery.headroom.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    
    <script src="{{asset('js/vendor/aos-min.js')}}"></script>
    <!-- Custom JS Code for all pages -->
    <script src="{{asset('js/phone_plugin/intlTelInput.js')}}"></script>
    <script src="{{ asset('js/lib/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('js/lib/select2/select2.full.min.js') }}"></script>
    <script src="{{ asset('js/lib/jquery-ui/jquery.multi-select.min.js') }}"></script>
    
    @include('frontend.home.home_script')