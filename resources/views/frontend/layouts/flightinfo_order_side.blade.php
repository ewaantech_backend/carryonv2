<div class="content">
    <ul class="flight-details">
        <li>
            <i class="fa fa-plane icon" aria-hidden="true"></i>
            <div class="wrap">
                <p class="flight_number_sidebar"><span>Flight number:</span>
                    N/A</p>
            </div>
        </li>
        <li>
            <a style="cursor:pointer;" class="edit flight-update"><i class="fa fa-pencil" aria-hidden="true"></i></a>
            <i><img src="{{asset('images/logo-briefcase-icon.png')}}" alt="" /></i>
            <div class="wrap">
                <p class="flight_deliver_date_sidebar">
                    <span>Flight date:</span>
                    N/A
                </p>
            </div>
        </li>
    </ul>
    <hr class="divider1">
    <div class="table-responsive">
        <table class="table table1">
            <tbody class="sideon-cart-list">
                <!-- side cart list -->
            </tbody>
        </table>
    </div>
    <ul class="list1 mb-0">
        <!-- <li>
        <span class="data">Subtotal</span>
        <span class="value sub_total">AED 00.00</span>
        </li>
        <li>
        <span class="data">Tax (5%)</span>
        <span class="value tax_total">AED 00.00</span>
        </li> -->
        <li class="cart-total">
            <span class="data">Total<span style="font-size: 15px !important;padding: 3px;font-weight: 500;">(Including VAT)</span></span>
            <span class="value grand_total"><span>AED</span> 0</span>
        </li>
    </ul>
    <footer>
        <a class="pay_proceed btn button1 btn-block" style="color: #fff;cursor:pointer;">Go to Checkout</a>
    </footer>
</div>
<script>
    $(document).ready(function() {

        var can_order = localStorage.getItem("can_order");
        var flight_date = localStorage.getItem("flight_date");
        var flight = localStorage.getItem("flight");
        if (can_order && !$.inArray(flight, ['N/A', 'null', null, '']) != -1) {

            var flight_updated_at = localStorage.getItem("flight_updated_at");
            if (flight_updated_at != null && new Date() - new Date(flight_updated_at) > 3600000) {
                localStorage.setItem("flight", 'N/A');
                localStorage.setItem("flight_date", 'N/A');
                localStorage.setItem("can_order", 0);
                localStorage.setItem("cart_key", "");
            } else {
                // Update to current time
                localStorage.setItem("flight_updated_at", new Date());
            }

            $('.flight_number_sidebar').html('<span>Flight number:</span>' + flight);
            $('.flight_deliver_date_sidebar').html('<span>Flight date:</span>' + flight_date);
        }

        $(document).on('click', '.flight-update', function() {
            $('#basket').find('.toggle-btn').trigger('click');
            $('#flight_popup').show();
            $('.trigger-order').trigger('click');
        });

        $(document).on('click', '.pay_proceed', function(event) {
            event.stopImmediatePropagation();
            var flight_date = localStorage.getItem("flight_date");
            var res = flight_date.split("-");
            flight_date = res[1] + '/' + res[0] + '/' + res[2];
            var flight = localStorage.getItem("flight");
            if (flight == 'N/A' || flight == null || flight == 'null' || flight == '') {
                alert('Please add flight information');
                $('.trigger-order').trigger('click');
                return;
            }
            $.ajax({
                url: "{{url('/flight/show')}}",
                dataType: "json",
                data: {
                    fl_no: flight,
                    date: flight_date,
                    fl_rnd: Math.random()
                },
                success: function(data) {
                    if (data.status == false) {
                        $('.trigger-order').trigger('click');
                        alert('Please update flight information');
                        if (data.message == 'time elapsed') {
                            $("#timecheck_error").html(`We can only accept orders 6 hours before departure time. So sorry!`);
                        } else {
                            $("#timecheck_error").html(data.message);
                        }

                        return;
                    }
                    if (data.is_logged_in == '1') {
                        update_cart();
                    } else {
                        location.href = "<?php echo route('checkout') ?>";
                    }
                }
            });
        });


        function update_cart() {
            $('#overlay').show();
            $.ajax({
                url: "<?php echo route('update_cart_user') ?>",
                method: "post",
                data: {
                    key: localStorage.getItem('cart_key')
                },
                success: function(data) {
                    data = JSON.parse(data);
                    if (data.status == true) {
                        location.href = "<?php echo route('billingaddress') ?>";
                    }
                },
                complete: function() {
                    $('#overlay').hide();
                }
            });
        }

    });
</script>