<head>
  <!-- Meta Tags -->
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width" />
  <meta name="description" content="" />
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <!-- Page Title & Favicon -->
  <title>Carry On</title>
  <!-- Stylesheets -->

    <link rel="stylesheet" href="{{asset('css/frontend/bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/frontend/aos-min.css')}}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css">
    <link rel="stylesheet" href="{{asset('css/frontend/jquery-ui.theme.min.css')}}" />
     <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800&display=swap"
    rel="stylesheet">
    
    
    <link rel="stylesheet" href="{{asset('css/frontend/cocogoose.css')}}" /> <!-- new -->
    <link rel="stylesheet" href="{{asset('css/frontend/custom.css')}}" />
    <link rel="stylesheet" href="{{asset('css/frontend/font-awesome.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/frontend/jquery-ui.min.css')}}" />
    <link rel="stylesheet" href="{{asset('css/frontend/jquery-ui(1).css')}}" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('images/favicon/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('images/favicon/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('images/favicon/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('images/favicon/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('images/favicon/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('images/favicon/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('images/favicon/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('images/favicon/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('images/favicon/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{asset('images/favicon/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('images/favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('images/favicon/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('images/favicon/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('images/favicon/manifest.json')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{asset('images/favicon/ms-icon-144x144.png')}}">
    <meta name="theme-color" content="#ffffff">
    <script src="{{asset('js/vendor/head.core.js')}}"></script>

    <link rel="stylesheet" href="{{asset('css/phone_plugin/intlTelInput.css')}}" />
    <link rel="stylesheet" href="{{asset('css/frontend/main.css')}}" />
    <link rel="stylesheet" href="{{asset('css/frontend/main2.css')}}" /> <!-- new -->
    
<!--    <link href="{{ asset('css/lib/select2/select2.min.css') }}" rel="stylesheet">-->

</head>
