<!-- header -->
    <header id="header" >
      <div class="container-fluid">
        <div class="row align-items-center">
          <div class="col-md-4">
            <div class="logo">
              <a href="{{asset('home')}}">
                <img src="{{asset('images/frontend/logo.svg')}}" alt="logo">
              </a>
            </div>
            <a class="open-menu-btn d-md-none"><i class="fa fa-bars" aria-hidden="true"></i></a>
          </div>
          <div class="col-md-8 d-flex">
            <ul class="user-block">
              @if( ! Session::has('user_id'))
                <li class="login-signup resp-screen-login">
                  <i class="fa fa-lock" aria-hidden="true"></i>
                  <a data-toggle="modal" data-target="#login">Login</a><span>/</span>
                  <a data-toggle="modal" data-target="#register">Sign up</a>
                </li>
              @else
                  <li class="login-signup resp-screen-login">
                  <a href="<?php echo route('about'); ?>" class="user-name-display">{{ Session::get('user_name')}}</a>
                  </li>
              @endif
              <li>
                <a class="cart"><i class="fa fa-shopping-bag" aria-hidden="true"></i><span
                    class="total total_cart_item_count">0</span>
                </a>
              </li>
            </ul>
            <nav id="main-navigation">
              <div class="scrollable-content">
                <a class="close-menu-btn d-md-none"><i class="fa fa-times" aria-hidden="true"></i></a>
                <ul class="menu">
                  <li id="home_link">
                    <a href="<?php echo route('home_page')?>">Home</a>
                  </li>
                  <li id="menu_link" class="">
                    <a href="<?php echo route('our_menu')?>">Our menu</a>
                  </li>
                </ul>
                <ul class="user-block">
                   @if( ! Session::has('user_id'))
                  <li class="login-signup resp-screen-login">
                    <i class="fa fa-lock" aria-hidden="true"></i>
                    <a data-toggle="modal" data-target="#login">Login</a><span> /</span>
                    <a data-toggle="modal" data-target="#register">Sign up</a>
                  </li>
                  @else
                  <li class="login-signup resp-screen-login">
                    <a href="<?php echo route('about'); ?>" class="user-name-display">{{ Session::get('user_name')}}</a>
                    <span class="">/</span>
                    <a href="#" id="logout">Logout</a>
                  </li>
                  @endif

                  <li>
                    <a class="cart"><i class="fa fa-shopping-bag" aria-hidden="true"></i><span
                        class="total total_cart_item_count">0</span>
                    </a>
                  </li>
                </ul>
              </div>
            </nav>
          </div>
        </div>
      </div>
    </header> <!-- /header -->




<!-- <header id="header" data-headroom><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
      <div class="container-fluid">
        <div class="row align-items-center">
          <div class="col-md-4">
            <div class="logo">
              <a href="{{asset('home')}}">
                  <img src="{{asset('images/frontend/logo.svg')}}" alt="logo">
              </a>
            </div>
            <a class="open-menu-btn d-md-none"><i class="fa fa-bars" aria-hidden="true"></i></a>
          </div>
          <div class="col-md-8 d-flex">
            <ul class="user-block">
                  @if( ! Session::has('user_id'))
                    <li class="login-signup resp-screen-login">
                      <i class="fa fa-lock" aria-hidden="true"></i>
                      <a data-toggle="modal" data-target="#login">Login</a><span>/</span>
                      <a data-toggle="modal" data-target="#register">Sign up</a>
                    </li>
                  @else
                  <li class="login-signup resp-screen-login">
                  <a href="<?php echo route('about'); ?>" class="user-name-display">{{ Session::get('user_name')}}</a>
                  </li>
                  @endif
                  
                  <li>
                    <a class="cart"><i class="fa fa-cart-plus" aria-hidden="true"></i><span data-total-cart_item-count="0" class="total total_cart_item_count">0</span>
                    </a>
                  </li>
            </ul>
            <nav id="main-navigation">
              <div class="scrollable-content">
                <a class="close-menu-btn d-md-none"><i class="fa fa-times" aria-hidden="true"></i></a>
                <ul class="menu">
                  <li id="home_link">
                    <a href="<?php echo route('home_page')?>">Home</a>
                  </li>
                  <li id="menu_link" class="current">
                    <a href="<?php echo route('our_menu')?>">Our menu</a>
                  </li>
                </ul>
                <ul class="user-block">
                  @if( ! Session::has('user_id'))
                    <li class="login-signup resp-screen-login">
                      <i class="fa fa-lock" aria-hidden="true"></i>
                      <a data-toggle="modal" data-target="#login">Login</a><span> /</span>
                      <a data-toggle="modal" data-target="#register">Sign up</a>
                    </li>
                  @else
                  <li class="login-signup resp-screen-login">
                    <a href="<?php echo route('about'); ?>" class="user-name-display">{{ Session::get('user_name')}}</a>
                    <span class="">/</span>
                    <a href="#" id="logout">Logout</a>
                  </li>
                  @endif
                  
                  <li>
                    <a class="cart"><i class="fa fa-cart-plus" aria-hidden="true"></i><span class="total total_cart_item_count">0</span>
                    </a>
                  </li>
                </ul>
              </div>
            </nav>
          </div>
        </div>
      </div>
    </header> -->