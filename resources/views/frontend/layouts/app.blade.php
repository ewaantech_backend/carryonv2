<!DOCTYPE html>
<html
  class=" js no-mobile desktop no-ie chrome chrome83 root-section gradient rgba opacity textshadow multiplebgs boxshadow borderimage borderradius cssreflections csstransforms csstransitions no-touch no-retina fontface webkit chrome win js domloaded w-1920 gt-240 gt-320 gt-480 gt-640 gt-768 gt-800 gt-1024 gt-1280 gt-1440 gt-1680 no-portrait landscape"
  id="index-page">

@include('frontend.layouts.header')
@include('frontend.layouts.footer')

<style>
  .ui-autocomplete {
    z-index: 2147483647 !important;
  }

  /*      .shadow {
  -moz-box-shadow:    2px 2px 2px 1px #ccc;
  -webkit-box-shadow: 2px 2px 2px 1px #ccc;
  box-shadow:         2px 2px 2px 1px #ccc;
}*/
</style>


<body class="home">
  <!--[if lt IE 10]>
        <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please upgrade your browser to improve your experience.</p>
  <![endif]-->
  <div id="container">
    <!-- header -->
    @include('frontend.layouts.headermenu')
    <!-- /header -->
    <div class="fixed-footer">
      <a class="btn button1 trigger-order">Order now</a>
    </div>
    <!-- Order -->
    @include('frontend.layouts.popup')
    <!-- Order -->
    <!-- banner -->
    @php
      $flight_popup_top = DB::select('select html_content from cms where visibility = "YES" and short_code = "flight_popup_top" and deleted_at IS NULL');
      $flight_popup_bottom = DB::select('select html_content from cms where visibility = "YES" and short_code = "flight_popup_bottom" and deleted_at IS NULL');
  @endphp

    <div id="banner">
      <button class="close"><i class="fa fa-times" aria-hidden="true"></i> </button>

      <div class="slides">      
        @if (count($home_sliders) > 0)
        @php
        $count = 1;
        @endphp

        {{-- @foreach ($home_sliders as $home_slider) --}}
        @php
        $contains = strpos($home_sliders[0]->image_path, 'mp4');
        if($contains){
        @endphp
            <div class="slide video-slide" data-slide="slide{{$count}}">
              <figure class="figure1">
                <img class="w-100" src="{{asset($home_sliders[0]->cover_image_path)}}" alt="img">
              </figure>
              <a href="#" class="play-btn"><i class="fa fa-play" aria-hidden="true"></i></a>
              <video id="slide1" muted controls>
                <source src="{{asset($home_sliders[0]->image_path)}}" type="video/mp4">
              </video>
              <div class="description pt-5">
                <div class="container">
                  <div class="row justify-content-center">
                    <div class="col-lg-10 col-md-12">
                      <div class="wrapper mb-md-5">
                        <h2 class="display-2 text-white">{{$home_sliders[0]->description}}</h2>
                        @if($home_sliders[0]->description != '' && $home_sliders[0]->link_text != '')
                          <a href="{{ asset($home_sliders[0]->link) }}" class="link text-white">{{ $home_sliders[0]->link_text }} <i class="fa fa-long-arrow-right"
                          aria-hidden="true"></i></a>
                        @endif
                      </div>
                      <div class="order alt">
                    <div class="form">
                      <div class="d-flex">
                        <div class="heading">
                          <h3>Where to?</h3>
                        </div>
                        <div class="full-box">
                          <div class="row">
                            <div class="col-md-4">
                              <span class="flight ui-widget">
                                <input type="password" style="display:none"><!-- to override prompt -->
                                <input id="flight_new" type="text" placeholder="Enter your flight number" class="flight_new">
                              </span>
                            </div>

                            <div class="col-md-4">
                              <input type="password" style="display:none"><!-- to override prompt -->
                              <span class="date"><input id="flight_date_dt_new" type="text" placeholder="Select your flight date" class="flight_date_new1" readonly onfocus="this.removeAttribute('readonly');" /></span>
                              <span id="flight_data_new" style="font-size: 16px;border-radius: 7px;color: #4a8046;"></span>
                              <span id="timecheck_error_new" style="color:red;font-weight: bold;font-size: 18px"></span>
                            </div>
                            <div class="col-md-4">

                              <span  class="addonsubmit" style="color:white !important"><input class="flight_data_submit"  id="fligh_submit" type="button"
                                  value="Order your food now"></span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                    </div>
                  </div>
                </div>
              </div>
            </div> 
        @php
        } else {
        @endphp
                <div class="slide" data-slide="slide{{$count}}">
                <figure class="figure1">
                  <img class="w-100" src="{{asset($home_sliders[0]->image_path)}}" alt="img">
                </figure>
                <div class="description pt-5">
                  <div class="container">
                    <div class="row justify-content-center">
                      <div class="col-lg-10 col-md-12">
                        <div class="wrapper mb-md-5">
                          <h2 class="display-2 text-white">{{$home_sliders[0]->description}}</h2>
                              @if($home_sliders[0]->description != '' && $home_sliders[0]->link_text != '')
                                <a href="{{ asset($home_sliders[0]->link) }}" class="link text-white">{{ $home_sliders[0]->link_text }} <i class="fa fa-long-arrow-right"
                                aria-hidden="true"></i></a>
                              @endif
                        </div>
                        <div class="order alt">
                    <div class="form">
                      <div class="d-flex">
                        <div class="heading">
                          <h3>Where to?</h3>
                        </div>
                        <div class="full-box">
                          <div class="row">
                            <div class="col-md-4">
                              <span class="flight ui-widget">
                                <input type="password" style="display:none"><!-- to override prompt -->
                                <input id="flight_new" type="text" placeholder="Enter your flight number" class="flight_new">
                              </span>
                            </div>

                            <div class="col-md-4">
                              <input type="password" style="display:none"><!-- to override prompt -->
                              <span class="date"><input id="flight_date_dt_new" type="text" placeholder="Select your flight date" class="flight_date_new1" readonly onfocus="this.removeAttribute('readonly');" /></span>
                              <span id="flight_data_new" style="font-size: 16px;border-radius: 7px;color: #4a8046;"></span>
                              <span id="timecheck_error_new" style="color:red;font-weight: bold;font-size: 18px"></span>
                            </div>
                            <div class="col-md-4">

                              <span  class="addonsubmit" style="color:white !important"><input class="flight_data_submit"  id="fligh_submit" type="button"
                                  value="Order your food now"></span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
        @php
        } 
        $count++;
        @endphp
        {{-- @endforeach --}}
        @endif
      </div>

      {{-- <div class="slides">
        <div class="slide" data-slide="slide1">
          <figure class="figure1">
            <img class="w-100" src="{{asset('images/banner-img1.jpg')}}" alt="img">
          </figure>
          <div class="description pt-5">
            <div class="container">
              <div class="row justify-content-center">
                <div class="col-lg-10 col-md-12">
                  <div class="wrapper mb-md-5">
                    <h2 class="display-2 text-white">In flight, good for
                      you meals that taste
                      amazing!</h2>
                    <a href="#" class="link text-white">More information <i class="fa fa-long-arrow-right"
                        aria-hidden="true"></i></a>
                  </div>
                  <div class="order alt">
                    <div class="form">
                      <div class="d-flex">
                        <div class="heading">
                          <h3>Where to?</h3>
                        </div>
                        <div class="full-box">
                          <div class="row">
                            <div class="col-md-4">
                              <span class="flight ui-widget">
                                <input type="password" style="display:none"><!-- to override prompt -->
                                <input id="flight_new" type="text" placeholder="Enter your flight number" class="flight_new">
                              </span>
                            </div>

                            <div class="col-md-4">
                              <input type="password" style="display:none"><!-- to override prompt -->
                              <span class="date"><input id="flight_date_dt_new" type="text" placeholder="Select your flight date" class="flight_date_new1" readonly onfocus="this.removeAttribute('readonly');" /></span>
                              <span id="flight_data_new" style="font-size: 16px;border-radius: 7px;color: #4a8046;"></span>
                              <span id="timecheck_error_new" style="color:red;font-weight: bold;font-size: 18px"></span>
                            </div>
                            <div class="col-md-4">

                              <span  class="addonsubmit" style="color:white !important"><input class="flight_data_submit"  id="fligh_submit" type="button"
                                  value="Order your food now"></span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div> --}}
      <div class="controls">
        <div class="container"></div>
      </div>
    </div>
    <!-- banner -->
    <div id="content">
      <!-- section -->
      <div class="section">
        @yield('content')
      </div>
      <!-- /section -->
      <!-- section -->
      <!--<div class="section about">
        <div class="fuel-for-journey">
          <img src="{{asset('images/frontend/fuel-for-journey.png')}}" alt="Fuel For Journey" />
        </div>
        <div class="container">
          <header>
            {!! $home_page_content_title[0]
            ->html_content !!}
          </header>
        </div>
        <div class="container-fluid">
          <div class="row">
            @php

            $home_page_middle = DB::select('select title, description, image_path from cms_slider where visibility =
            "YES" and short_code = "homepage_middle" and deleted_at IS NULL');
            @endphp
            <div class="col-md-7 order-md-2">
              <div class="video">
                <a href="#">
                  <img class="w-100" src="{{ asset($home_page_middle[0]->image_path) }}" alt="img">
                  <span class="play-btn">
                    <i class="fa fa-play" aria-hidden="true"></i>
                  </span>
                </a>
              </div>
            </div>
            <div class="col-md-5 d-flex">
              <div class="box1 align-self-center">
                {!! $home_page_content[0]
                ->html_content !!}
                
                <footer>
                  <a href="{{asset('about-us')}}" class="btn btn-link">Learn more about CarryOn</a>
                </footer>
              </div>
            </div>
          </div>
        </div>
      </div> -->
      <!-- /section -->
      @php
        if (count($home_sliders) > 1) {
          unset($home_sliders[0]);
        }
        $count = 1;
      @endphp
      <div class="section meals py-0">
        <div class="slideshow3">
          <div class="slides">
          @foreach ($home_sliders as $home_slider)
            <div class="slide" data-slide="slide{{$count}}">
              <div class="description">
                <div class="container">
                  <div class="row justify-content-end">
                    <div class="col-md-7 pl-md-5">
                      <h2 class="display-2">
                        {{$home_slider->description}}
                      </h2>
                      @if($home_slider->description != '' && $home_slider->link_text != '')
                                <a href="{{ asset($home_slider->link) }}" class="link">{{ $home_slider->link_text }} <i class="fa fa-long-arrow-right text-primary"
                                  aria-hidden="true"></i></a>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              <figure class="figure1">
                <img class="w-100" src="{{asset($home_slider->image_path)}}" alt="img">
              </figure>
            </div>
          
          @php
          $count = $count + 1;
          @endphp
          @endforeach
        </div>
          <div class="pager">
            <div class="container">

            </div>
          </div>
        </div>
      </div> 

      @php
        $home_page_footer = DB::select('select * from cms where visibility =
        "YES" and short_code = "home_page_footer" and deleted_at IS NULL');
      @endphp
        <div class="section instagram-block">
          {!! $home_page_footer[0]
            ->html_content !!}
      </div>
    </div>
    <button style="display:none;" class="btn button1 btn-block view-basket" data-toggle="modal"
      data-target="#basket">view basket</button>
    @include('frontend.layouts.basket_popup')



    <!-- footer -->
    @include('frontend.layouts.footermenu')

    <!-- footer -->
  </div>
</body>

</html>
