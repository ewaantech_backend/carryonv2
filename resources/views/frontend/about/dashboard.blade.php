<!DOCTYPE html>
<html>

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <!-- Meta Tags -->
  
  <meta name="viewport" content="width=device-width" />
  <meta name="description" content="" />
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <!-- Page Title & Favicon -->
  <title>Carry On</title>
  <!-- Stylesheets -->
  <link rel="stylesheet" href="{{ asset('css/frontend/bootstrap.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('css/frontend/font-awesome.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('css/frontend/aos-min.css') }}" />
  <link rel="stylesheet" href="{{ asset('css/frontend/jquery-ui.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('css/frontend/jquery-ui.theme.min.css') }}" />  
    <link rel="stylesheet" href="{{ asset('css/frontend/main.css') }}" />
    <link rel="stylesheet" href="{{asset('css/frontend/main2.css')}}" /> <!-- new -->
    <link rel="stylesheet" href="{{ asset('css/frontend/custom.css') }}" />

    <link rel="stylesheet" href="{{asset('css/phone_plugin/intlTelInput.css')}}" />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('css/lib/sweetalert/sweetalert.css') }}">
    <link href="{{ asset('css/lib/multiselect/multiselect-styles.css') }}" rel="stylesheet">
<!--    <link href="{{ asset('css/lib/multiselect/demo-styles.css') }}" rel="stylesheet">-->
  <!-- Javascript -->
  <script src="{{ asset('js/vendor/head.core.js') }}"></script>

  <style>
          #errcode,#errphone
            {
            color: red !important;
             font-size: 16px !important; 
            text-align: center;
            }
</style>
</head>

<body>
  <!--[if lt IE 10]>
        <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please upgrade your browser to improve your experience.</p>
  <![endif]-->
  <div id="container">
    <!-- header -->
    @include('frontend.layouts.headermenu')

    @include('frontend.layouts.footer')
    <!-- /header -->
    
    <!-- /header -->

    <!-- content -->
    <div id="content">
      <!-- section -->
      <div class="section dashboard">
        <div class="container">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
            </ol>
          </nav>
          <div class="row">
            <div class="col-xl-3 column">
<!--               <aside id="sidebars">
                <span class="title">Profile</span>
                <ul id="side-navigations" class="mb-0">
                  <li><a href="#">Home</a></li class="current">
                  <li><a href="#">Orders</a></li>
                  <li class="current"><a href="#">Profile</a></li class="current">
                  <li><a href="#">My Preferences</a></li>
                </ul>
              </aside>               -->
              <aside id="sidebar">
                <!-- <span class="title">Home</span> -->
                <nav class="navbar dashboard-nav">


                  <button type="button" class="navbar-toggle dash-btn" data-toggle="collapse" data-target="#descriptionTab"
                  aria-expanded="false" aria-controls="descriptionTab">
                  <i class="fa fa-bars" aria-hidden="true"></i></button>
            
                <div class="navbar-collapse" id="descriptionTab">
                  <ul id="side-navigation" class="mb-0 nav nav-tabs" role="tablist" >
                    <li class="nav-item">
                      <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Orders</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="true">Profile</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="preference-tab" data-toggle="tab" href="#preference" role="tab" aria-controls="preference" aria-selected="true">My Details</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="reset-password-tab" data-toggle="tab" href="#reset-password" role="tab" aria-controls="reset-password" aria-selected="true">Reset Password</a>
                    </li>                  
                  </ul>
                </div>
          </nav>
              </aside>
            </div>
              

            <div class="tab-content col-xl-9 column">

            <div class="tab-pane active" id="home" role="tabpanel" aria-labelledby="home-tab">
              <main id="main" class="all-orders">
              </main>
            </div>

            <div class="tab-pane" id="orders" role="tabpanel" aria-labelledby="orders-tab">
            
            </div>

            <div class="tab-pane" id="profile" role="tabpanel" aria-labelledby="profile-tab">
            <main id="main">
                <h3>My Profile</h3>
                <div class="form">
                  <ul>
                    <li class="half">
                      <label>First Name:</label>
                      <input id="first_name" name="first_name" type="text" class="textbox" value="">
                    </li>
                    <li class="half">
                      <label>Last Name:</label>
                      <input id="last_name" name="last_name" type="text" class="textbox" value="">
                    </li>
                    <li class="half">
                      <label>Mobile:</label>
                      <input id="phone" name="phone" type="text" class="textbox" value="">
                      <span id="errphone"></span>
                      <span id="phone_error" style="color:red;font-size: 17px;display:none;">Invalid Mobile Number</span>
                    </li>
                    <li class="half">
                      <label>Email:</label>
                      <input id="email" name="email" type="text" class="textbox" value="">
                    </li>
                    <li class="half">
                      <label>Country:</label>
                      <span class="dropdown_custom"><select id="country" name="country" class="textbox" placeholder="Select Country"></span>
                        </select>
                    </li>
                    <li class="half">
                      <label>City:</label>
                      <input id="city" name="city" type="text" class="textbox" value="">
                    </li>
                    <li class="half">
                      <label>P.O Box:</label>
                      <input maxlength="6" id="zip" name="zip" type="text" class="textbox" value="">
                      <span id="zip_error" style="color:red;font-size: 17px;display:none;">Invalid P.O Box</span>
                    </li>
                    <li class="half">
                      <label>Street:</label>
                      <input  id="billing_street" name="billing_street" type="text" class="textbox" value="">
                    </li>
                    <li class="half">
                      <label>Street No:</label>
                      <input  id="billing_street_no" name="billing_street_number" type="text" class="textbox" value="">
                    </li>
                    <li class="half">
                      <label>Apartment No:</label>
                      <input  id="billing_apartment_no" name="billing_apartment_number" type="text" class="textbox" value="">
                    </li>                   
                    <li>
                      <input id="profilesubmit" type="submit" value="Save" class="btn button1">
                    </li>
                  </ul>
                  </ul>
                </div>
              </main>
            </div>
                
            <div class="tab-pane" id="preference" role="tabpanel" aria-labelledby="preference-tab">
            <main id="main">
                <h3>My Details</h3>
                <div class="form">
                  <ul>
                    <li class="half">
                        <label>Age:</label>
                        <span class="dropdown_custom">
                        <select id="agegroup" name="agegroup" class="textbox selectbox" placeholder="Please Choose">
                            <option value="">Please Choose</option>
                            <option value="18-24">18-24</option>
                            <option value="25-34">25-34</option>
                            <option value="35-44">35-44</option>
                            <option value="45-54">45-54</option>
                            <option value="55+">55+</option>
                        </select>
                        </span>
                    </li>
                    <li class="half">
                      <label>Gender:</label>
                      <span class="dropdown_custom">
                        <select id="gender" name="gender" class="textbox selectbox" placeholder="Please Choose">
                            <option value="">Please Choose</option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>
                      </span>
                    </li>
                    <li class="half">
                        <label>Nationality:</label>
                        <span class="dropdown_custom">
                            <select id="pref_country" name="pref_country" class="textbox selectbox" placeholder="Please Choose">
                            </select>
                        </span>
                    </li>
                    <li class="half">
                        <label>Food allergies:</label>
                        <span class="dropdown_custom">
                        <select id="intolerances" name="intolerances[]" class="select-2 textbox" multiple placeholder="Please Choose">
                            @foreach ($intolerence_data as $intolerence)
                                <option value="{{$intolerence->val}}">{{$intolerence->name}}</option>
                            @endforeach
                        </select>
                        </span>
                    </li>
                    <li class="half">
                        <label>I’m following a…. diet:</label>
                        <span class="dropdown_custom">
                        <select id="preferred_diet" name="preferred_diet[]" class="select-2 textbox" multiple placeholder="Please Choose">
                            @foreach ($diet_data as $diet)
                                <option value="{{$diet->val}}">{{$diet->name}}</option>
                            @endforeach
                        </select>
                        </span>
                    </li>
                    <li class="half">
                        <label>I exercise…:</label>
                        <span class="dropdown_custom">
                        <select id="lifestyle" name="lifestyle" class="textbox selectbox" placeholder="Please Choose">
                            <option value="">Please Choose</option>
                            <option value="4 times/week or more">4 times/week or more</option>
                            <option value="2-3 times/week">2-3 times/week</option>
                            <option value="1-2 times/week">1-2 times/week</option>
                            <option value="1-2 times/month">1-2 times/month</option>
                            <option value="I don’t exercise">I don’t exercise</option>
                        </select>
                        </span>
                    </li>
                    <li class="half">
                        <label>How often do you travel?</label>
                        <span class="dropdown_custom">
                        <select id="travel_pattern" name="travel_pattern" class="textbox selectbox" placeholder="Please Choose">
                            <option value="">Please Choose</option>
                            <option value="1/week">1/week</option>
                            <option value="1/month">1/month</option>
                            <option value="1/every few months">1/every few months</option>
                            <option value="1/year">1/year</option>
                        </select>
                        </span>
                    </li>
                    <li class="half">
                        <label>Why do you travel?</label>
                        <span class="dropdown_custom">
                        <select id="travelfor" name="travelfor" class="textbox selectbox" placeholder="Please Choose">
                            <option value="">Please Choose</option>
                            <option value="business">More for business</option>
                            <option value="leisure">More for leisure</option>
                            <option value="both">Both</option>
                        </select>
                        </span>
                    </li>                   
                    <li>
                      <input id="preferencesubmit" type="submit" value="Save" class="btn button1">
                    </li>
                  </ul>
                  </ul>
                </div>
              </main>
            </div>

                
            <div class="tab-pane" id="resetpassword" role="tabpanel" aria-labelledby="reset-password">
            <main id="main">
                <h3>Reset Password</h3>
                <div class="form">
                  <ul>
                    <li class="half">
                      <label>Current:</label>
                      <input id="current_password" name="current_password" type="password" class="textbox" value="" placeholder="Enter Current Password">
                    </li>
                    <li class="half">
                      <label>New:</label>
                      <input id="new_password" name="new_password" type="password" class="textbox" value="" placeholder="Enter New Password">
                      <span id="errphone"></span>
                    </li>
                    <li class="half">
                      <label>Confirm:</label>
                      <input id="confirm_password" name="confirm_password" type="text" class="textbox" value="" placeholder="Confirm New Password">
                    </li>                 
                    <li>
                      <input id="resetpassword_submit" type="submit" value="Reset Password" class="btn button1" >
                    </li>
                  </ul>
                  </ul>
                </div>
              </main>
            </div>                
            </div>

          </div>
        </div>
      </div>
      <!-- /section -->
    </div>
    <!-- /content -->

    <!-- footer -->
    @include('frontend.layouts.footermenu')

    <button class="btn button1 btn-block view-basket" data-toggle="modal" data-target="#basket">view basket</button>
    @include('frontend.layouts.basket_popup')

    <div class="modal" id="cancel_reason" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <h2>Order Cancel</h2>
            <div id="reason_c" class="row">
                <div class="col-md-12" style="margin-top: 5px;margin-bottom: 7px;">
                  <span>Please let us know the reason for order cancellation</span>
                </div>
                <div class="col-md-12">
                  <textarea id="cancellation_reason" class="form-control"></textarea>
                </div>
                
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn button1 order-cancellation_reason">Proceed</button>
            <button style="width:150px;" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    
  </div>
  <script>
  
  $(document).ready(function() {
    $('.view-basket').hide();
//    $('.select-2').select2({
//            placeholder: 'Please Choose'
//        });
    // $('.select-2').multiSelect();

    @if( isset($order_id) )
      var order_confirmed_id = {{ $order_id }}
    @else
      var order_confirmed_id = "";
    @endif

    $(document).on('click', '.navbar-toggle', function() {
      setTimeout(() => {
        if( $('.navbar-collapse').hasClass('show') ) {
          $('#side-navigation').css('display', 'block');
        } else {
          $('#side-navigation').css('display', 'none');
        }
      }, 500);
      
    });
    if ( $(window).width() >= 1200 ) {
      $('#side-navigation').css('display', 'block');
    } else {
      if( ! $('.navbar-collapse').hasClass('show') ) {
        $('#descriptionTab').css('height', 0);
      }
    }

    $('#zip').on('focusout', function() {
      var zipcode = $(this).val();
      if( ! validateZipCode(zipcode) ) {
        $('#zip_error').show();
        $("#zip").addClass('error');
      } else {
        $('#zip_error').hide();
        $("#zip").removeClass('error');
      }
    });

    $('#phone').on('focusout', function() {
      var phone = $(this).val();
      if( ! phonevalidation(phone) ) {
        $('#phone_error').show();
        $("#phone").addClass('error');
      } else {
        $('#phone_error').hide();
        $("#phone").removeClass('error');
      }
    });

    function phonevalidation(phone) {
      reg = /\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)-\d{5,10}$/
      return reg.test(phone);
    }

    function validateZipCode(elementValue){
      //var zipCodePattern = /^\d{6}$/;
      var zipCodePattern = /^\d{1,}$/;
      return zipCodePattern.test(elementValue);
    }

    function checkWidth() {
      console.log('width rez', $(window).width())
      if ( $(window).width() >= 1100 ) {
        $('#side-navigation').css('display', 'block');
        $('.navbar-collapse').addClass('show');
      } else {
        if( ! $('.navbar-collapse').hasClass('show') ) {
          $('#descriptionTab').css('height', 0);
        }
      }
    }

    $(window).resize(checkWidth);

   $('#resetpassword').hide();
    var order_placed = '';
    @if(Session::has('code'))
      if("{{ Session::get('code') }}" == 'PAYED_OK') {
        order_placed = 'success';
        localStorage.setItem("flight", 'N/A');
        localStorage.setItem("flight_date", 'N/A');
        localStorage.setItem("can_order", false);
        localStorage.setItem("cart_key", '');
      }
    @endif

    $("#phone").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
          $("#errcode,#errphone").html("Digits only").show().fadeOut("slow");
          return false;
        }
    });
    var input = document.querySelector("#phone");
    window.intlTelInput(input, {
    preferredCountries: ["ae"],
    initialCountry:"ae",
    utilsScript: "{{asset('js/phone_plugin/utils.js')}}",
    });
    $('.iti__country').click(function(){
      $('#phone').val($(this).children("span:nth-child(3)").html()+'-');
    })

    loadData("<?php echo route('order_history'); ?>", 'all_orders', {});
    $('#home-tab').on('click', function() {
      order_confirmed_id = '';
      hideDetails();
      $('.view-basket').hide();
      $('#resetpassword').hide();
      $('#profile').hide();
      $('#preference').hide();
      loadData("<?php echo route('order_history'); ?>", 'all_orders', {});
    });

    $('#orders-tab').on('click', function() {
      order_confirmed_id = '';
      hideDetails();
      $('.view-basket').hide();
      $('#resetpassword').hide();
      $('#profile').hide();
      $('#preference').hide();
      loadData("<?php echo route('order_history'); ?>", 'order_detail', {});
    });

    $('#profile-tab').on('click', function() {
      order_confirmed_id = '';
      hideDetails();
      $('.view-basket').hide();
      $('#resetpassword').hide();
      $('#preference').hide();
      loadData("<?php echo route('profile'); ?>", 'profile', {});
      $('#profile').show();
    });
    
    $('#preference-tab').on('click', function() {
      order_confirmed_id = '';
      hideDetails();
      $('.view-basket').hide();
      $('#home').hide();  
      $('#resetpassword').hide();
      $('#profile').hide();
      loadData("<?php echo route('preference'); ?>", 'preference', {});
      $('#preference').show();
    });
    
    $('#reset-password-tab').on('click', function() {
      order_confirmed_id = '';
      hideDetails();
      $('.view-basket').hide();
      $('#home').hide(); 
      $('#resetpassword').show();
      $('#profile').hide();
      $('#home').hide();
      $('#preference').hide();
    });
    
    $('#profilesubmit').on('click', function() {
        $(".textbox").removeClass('error');
        var first_name = $('#first_name').val().trim();
        var last_name = $('#last_name').val().trim();
        var email =  $('#email').val().trim();
        var phone =  $('#phone').val().trim();
        var country =  $('#country').val().trim();
        var city =  $('#city').val().trim();
        var zip =  $('#zip').val().trim();
        var billing_street =  $('#billing_street').val().trim();
        var billing_street_no =  $('#billing_street_no').val().trim();
        var billing_apartment_no =  $('#billing_apartment_no').val().trim();
        var flag = true; 
        if(first_name == ''){
            $("#first_name").addClass('error').focus();
            flag = false;
        }
        if(email == ''){
            $("#email").addClass('error').focus();
            flag = false;
        }
        if(phone == ''){
            $("#phone").addClass('error').focus();
            flag = false;
        } else {
          if( ! phonevalidation(phone) ) {
            $('#phone_error').show();
            $("#phone").addClass('error');
            flag = false;
          } else {
            $('#phone_error').hide();
            $("#phone").removeClass('error');
          }
        }
        if(country == ''){
            $("#country").addClass('error').focus();
            flag = false;
        }
        if(city == ''){
            $("#city").addClass('error').focus();
            flag = false;
        }
        if(zip == ''){
            $("#zip").addClass('error').focus();
            flag = false;
        } else {
          zipcode = $('#zip').val();
          if( ! validateZipCode(zipcode) ) {
            $('#zip_error').show();
            $("#zip").addClass('error').focus();
            flag = false;
          } else {
            $('#zip_error').hide();
            $("#zip").removeClass('error');
          }
        }
        if(billing_street == ''){
            $("#billing_street").addClass('error').focus();
            flag = false;
        }
        if(billing_street_no == ''){
            $("#billing_street_no").addClass('error').focus();
            flag = false;
        }
        if(billing_apartment_no == ''){
            $("#billing_apartment_no").addClass('error').focus();
            flag = false;
        }
        if (flag) {
            var dataToSubmit = {"first_name":first_name,"last_name":last_name,"email":email,
              "billing_street": billing_street, "billing_street_number": billing_street_no, 
              "billing_apartment_number": billing_apartment_no,
              "phone":phone,"country":country,"city":city,"zip":zip};    
            loadData("<?php echo route('profileupdate'); ?>", 'profileupdate', dataToSubmit);
        }
    });
    
    
    $('#preferencesubmit').on('click', function() {
        $(".textbox").removeClass('error');
        var agegroup = $('#agegroup').val().trim();
        var gender =  $('#gender').val().trim();
        var pref_country =  $('#pref_country').val().trim();
        var intolerances =  $('#intolerances').val();
        console.log(intolerances);
        var preferred_diet =  $('#preferred_diet').val();
        var lifestyle =  $('#lifestyle').val().trim();
        var travel_pattern =  $('#travel_pattern').val().trim();
        var travelfor =  $('#travelfor').val().trim();

        var flag = true; 
        if(agegroup == ''){
            $("#agegroup").addClass('error').focus();
            flag = false;
        }
        if(gender == ''){
            $("#gender").addClass('error').focus();
            flag = false;
        }
        if(pref_country == ''){
            $("#pref_country").addClass('error').focus();
            flag = false;
        }
        if(intolerances.length < 1){
            $("#intolerances").addClass('error').focus();
            flag = false;
        }
        if(preferred_diet.length < 1){
            $("#preferred_diet").addClass('error').focus();
            flag = false;
        }
        if(lifestyle == ''){
            $("#lifestyle").addClass('error').focus();
            flag = false;
        }
        if(travel_pattern == ''){
            $("#travel_pattern").addClass('error').focus();
            flag = false;
        }
        if(travelfor == ''){
            $("#travelfor").addClass('error').focus();
            flag = false;
        }

        if (flag) {
            var dataToSubmit = {"agegroup":agegroup,"gender":gender,
              "pref_country": pref_country, "intolerances": intolerances, 
              "preferred_diet": preferred_diet,
              "lifestyle":lifestyle,"travel_pattern":travel_pattern,"travelfor":travelfor};    
            loadData("<?php echo route('preferenceupdate'); ?>", 'preferenceupdate', dataToSubmit);
        }
    });
    
    
    $("#resetpassword_submit").on('click', function() {
    $(".textbox").removeClass('error');
    
        var current_password =  $('#current_password').val().trim();
        var new_password =  $('#new_password').val().trim();
        var confirm_password =  $('#confirm_password').val().trim();
        var flag = true; 
        if(current_password == ''){
            $("#current_password").addClass('error').focus();
            return false;
        }
        if(new_password == ''){
            $("#new_password").addClass('error').focus();
            return false;
        }
        if(new_password != '' && (new_password.length < 4)){
            $("#new_password").addClass('error').focus();
            alert('Enter atleast 4 characters');
            return false;
        }
        if(confirm_password != '' && (confirm_password.length < 4)){
            $("#confirm_password").addClass('error').focus();
            alert('Enter atleast 4 characters');
            return false;
        }
        
        if(new_password != confirm_password){
            alert('Mismatch with confirm password');
            return false;
        }
        
        $.ajax({
                url: "<?php echo route('resetpassword') ?>",
                type: 'post',
                data: {"current_password":current_password,"new_password":new_password,"confirm_password":confirm_password},
                success: function(data) {
                    $('#overlay').hide();
                    data = JSON.parse(data);
                    alert(data.message); 

                    return true;
            }
            
        });
    
    });
    

    function loadData(url, tab, data) {
      $('#email').removeClass('error')
      $("#overlay").show();
      var order_id = order_confirmed_id;
      $.ajax({
        url: url,
        type: 'post',
        data: data,
        success: function(data) {
          data = JSON.parse(data);
          if(data.message == 'User is not logged in') {
            alert('Please login');
            setTimeout(() => {
              location.href = "<?php echo route('home_page') ?>";
            }, 2000);

            return;
          }
          if( tab == 'all_orders') {
            all_orders(data);
            if(order_id != '' && $('a.order_details[order_id="'+ order_id +'"]').length == 0) {
              alert('Order id ' + order_id + 'not found')
            } else if( order_id != '' ) {
              $('a.order_details[order_id="'+ order_id +'"]').trigger('click');
            }
          }
          if( tab == 'order_detail') {
            order_detail(data);
          }
          if( tab == 'profile') {
            profile(data);
          }
          
          if( tab == 'preference') {
            preference(data);
          }
          
          if (tab == 'preferenceupdate') {
              alert("Successfully updated");
          }
          
          if( tab == 'profileupdate') {
            if(data.status == true) {
              $('.user-name-display').text( $('#first_name').val()+" "+$('#last_name').val() );
              alert("Successfully updated");
            } else {
              alert(data.message);
              if(data.message == 'Email already exists') {
                $('#email').addClass('textbox error');
              }
            }
            
          }
          if( tab == 'pdfreport') {
                window.open(
                  data.filepath,
                  '_blank'
                );
          }
            $("#overlay").hide();
        },
        complete: function() {
            $("#overlay").hide();
        }
      });
    }

    allData = {};

    function all_orders(data) {
      if (data.data == false) {
        var string = `<h3>All orders</h3>
          <div class="tracking" style="margin-bottom: 0px;">
            <div class="table-responsive">
              <table class="table table2">
                <thead>
                  <tr>
                    <th scope="col" class="serial-no">No orders to display</th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>`;
          $('.all-orders').html( string );
          return true;
      }
      var string = `<h3>All orders</h3>
        <div class="tracking" style="margin-bottom: 0px;">
          <div class="table-responsive">
            <table class="table table2">
              <thead>
                <tr>
                  <th scope="col" class="serial-no"></th>
                  <th scope="col" class="order-no">Order Number</th>
                  <th scope="col" class="order-date">Order date</th>
                  <th scope="col" class="status">Status</th>
                  <th></th>
                </tr>
              </thead>
            </table>
          </div>
        </div>`;
      var data = data.data;
      allData = data;
      $('#orders').empty();
      var orders = Object.keys(data).sort((a, b) => b - a);
      for( var incr=0; incr < orders.length; incr++ ) {
        if (data[orders[incr]].order_status == 'IN KITCHEN') {
            data[orders[incr]].order_status = 'ORDER CONFIRMED';
        } else if (data[orders[incr]].order_status != 'DELIVERED' && data[orders[incr]].order_status != 'CANCELLED' && ('At '+ data[orders[incr]].boarding_point != data[orders[incr]].pickup_reached_at)) {
              data[orders[incr]].order_status = 'ORDER CONFIRMED';
        }
        pickup_active = $.inArray(data[orders[incr]].order_status, ['READY FOR PICKUP', 'DELIVERED']) != -1 ? 'active' : '';
        delivered_active = $.inArray(data[orders[incr]].order_status, ['DELIVERED']) != -1 ? 'active' : '';

        status_ord = camelize(data[orders[incr]].order_status);
        cancelledItem = status_ord != 'Cancelled' ? 'View' : '';
        placed_active = status_ord != 'Cancelled' ? 'active' : '';
        orderConfirmedText = cancelledItem == '' ? 'Cancelled' : 'Order Confirmed';
        string += `<div class="tracking">
            <div class="table-responsive">
              <table class="table table2">
                <tbody>
                  <tr>
                    <td class="serial-no" data-title="serial No">${incr+1}</td>
                    <td class="order-no" data-title="Order No">${data[orders[incr]].order_encoded}</td>
                    <td class="order-date" data-title="Order date">${data[orders[incr]].created_at}</td>
                    <td class="status" id="status_item_${data[orders[incr]].order_id}" data-title="status">${status_ord}</td>
                    <td class="btn-wrapper"> 
                    <a style="color: #387635;font-size: 18px;cursor:pointer;" id="view_item_${data[orders[incr]].order_id}"
                      order_id="${data[orders[incr]].order_id}" class="btn link1 p-md-0 order_details">View</a>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <ul class="progressbar">
              <li class="${placed_active}">Order Confirmed</span></li>
              <li class="${pickup_active}">Ready for pickup</li>
              <li class="${delivered_active}">Collected</li>
            </ul>
          </div>`;
          items = data[orders[incr]].items;
          order_detail_bind(data[orders[incr]], items);
      }
      $('.all-orders').html( string );
      
    }

    function camelize(text) {
        text = text.replace(/_/g,  ' ').toLowerCase();
        return text.substr(0, 1).toUpperCase() + text.substr(1);
    }

    $(document).on('click', '.back_to_order', function() {
      $('.order-placed-message').hide();
      $('.main_items').hide();
      $('#orders').hide();
      $('#home').show();
    })

    function hideDetails() {
      $('.order-placed-message').hide();
      $('.main_items').hide();
      $('#orders').hide();
      // $('#home').show();
    }

    function order_detail_bind(data, items) {
        
      var placed_active = $.inArray(data.order_status, ['CANCELLED']) == -1 ? 'active' : '';
      var hideForCancelled = placed_active == '' ? 'display:none;' : '';
      var pickup_active = $.inArray(data.order_status, ['READY FOR PICKUP', 'DELIVERED']) != -1 ? 'active' : '';
      var delivered_active = $.inArray(data.order_status, ['DELIVERED']) != -1 ? 'active' : '';
      var hide_map = (data.gate == '' || data.gate == '-') ? 'hide-map' : '';
      var file_map = 'index';
      if( hide_map == 'hide-map' && data.pickup_reached_at == 'At '+ data.boarding_point ) {
        // Show map with "lift" as gate if gate is not set even after pickup reached.
        data.gate = 'Lift';
        hide_map = '';
        file_map = 'concourseb';
      } else {
        first_char = data.gate.substring(0, 1).toLowerCase();
        file_map = (first_char == 'b' || first_char == 'c') ? ("concourse"+first_char) : 'index';
      }
      
      if (data.order_status == 'DELIVERED') {
          hideForCancelled = 'display:none;';
      }
      if (data.order_status == 'IN KITCHEN') {
          data.order_status = 'ORDER CONFIRMED';
      } else if (data.order_status != 'DELIVERED' && data.order_status != 'CANCELLED' && ('At '+ data.boarding_point != data.pickup_reached_at)) {
            data.order_status = 'ORDER CONFIRMED';
      }  
      var success_string = order_placed == 'success' ?
                          `<p class="order-md-1 order-placed-message"> Your order ${data.order_encoded} has been placed successfully.</p>` : '';
      var string = `<main id="main_${data.order_id}" class="order_details_${data.order_id} main_items">
                <header class="d-lg-flex">
                  <a style="color: #387635;cursor:pointer;font-size: 18px;" class="btn link1 order-md-3 back_to_order"><i class="fa fa-angle-left" aria-hidden="true"></i>
                    <span>Back to order page</span></a>
                  ${success_string}
                  <a order_id=${data.order_id} class="btn button1 btn-lg order-md-2 ml-auto downloadpdf" 
                    style="background: #387635;min-width: 169px;color:#fff;cursor: pointer;">
                 <i class="fa fa-download"
                      aria-hidden="true"></i> Download Invoice PDF</a>
                </header>
                <h3><span>Order tracking</span>
                </h3>
                <div class="clearfix"></div>
                <div class="tracking">
                  <div class="table-responsive" style="overflow-x: hidden;">
                    <table class="table table2">
                      <thead>
                        <tr>
                          <th scope="col" class="order-no">Order</th>
                          <th scope="col" class="order-date">Order date</th>
                          <th scope="col" class="flight-no">Flight</th>
                          <th scope="col" class="time">Departure Date & Time</th>
                          <th scope="col" class="gate">Pickup Location</th>
                          <th scope="col" colspan="2" class="status">Status</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td class="order-no" data-title="Order No">${data.order_encoded}</td>
                          <td class="order-date" data-title="Order date">${data.created_at.split(" ")[0]}</td>
                          <td class="flight-no" data-title="Flight no">${data.flight_no.toUpperCase()}</td>
                          <td class="time" data-title="Time">${data.order_at}</td>
                          <td class="gate" data-title="Gate">${data.boarding_point}</td>
                          <td class="status" data-title="status">${data.order_status}</td>
                          <td class="btn-wrapper"> <a style="${hideForCancelled}" order_id="${data.order_id}" class="cancel_order_item btn link1 p-md-0">Cancel</a> </td>
                        </tr>
                      </tbody>
                    </table>

                  </div>
                  <ul class="progressbar">
                    <li class="${placed_active}">Order Confirmed</li>
                    <li class="${pickup_active}">Ready for pickup</li>
                    <li class="${delivered_active}">Collected</li>
                  </ul>
                </div>
                <h3 class="dash-h2-ites-head">Items</h3>
                <a target="_blank" href="/maps/${file_map}.html?from=${data.gate}&to=${data.boarding_point}" class="pull-right btn btn-green-custom ${hide_map}"><i class="fa fa-map-marker" aria-hidden="true"></i> MAP</a>
                <div class="table-responsive">
                  <table class="table table2 item-table">
                    <thead>
                      <tr>
                        <th scope="col" class="product-thumbnail">Product Name</th>
                        <th scope="col" class="price">Price</th>
                        <th scope="col" class="qty">QTY</th>
                        <th scope="col" class="total">Total</th>
                      </tr>
                    </thead>
                    <tbody id="order_item_details_${data.order_id}">
                    </tbody>
                    <tfoot class="price-total" id="order_total_details_${data.order_id}">
                      <tr style="display:none;">
                        <td class="blank"></td>
                        <td>Subtotal</td>
                        <td colspan="6"><strong id="item_subtotal">AED 0.0</strong></td>
                      </tr>
                      <tr style="display:none;">
                        <td class="blank" style="border-top: 0px;"></td>
                        <td>Vat(5%)</td>
                        <td colspan="6"><strong id="item_taxtotal">AED 0.0</strong></td>
                      </tr>
                      <tr>
                        <td class="blank" style="border-top: 1px solid #f2f1f6;"></td>
                        <td class="heading">TOTAL</td>
                        <td colspan="6">AED <span id="item_total">0.0</span></td>
                      </tr>
                  </table>

                </div>
              </main>`;
              $('#orders').append(string);
              var eachItems = '';
              var sub_total = parseFloat(data.sub_total);
              var grnd_total = parseFloat(data.grant_total);
              var tx_total = parseFloat(data.tax_total);
              for( var incr_items=0; incr_items < Object.keys(items).length; incr_items++ ) {
                items_data = items[ Object.keys(items)[incr_items] ];
                sub_item_price = parseFloat(items_data.sub_item_price);
                eachItems += `<tr>
                                  <td class="product-thumbnail" data-title="Product Name">
                                    <a href="#" class="image">
                                      <img src="${items_data.productthumbnail}" alt="img">
                                    </a>
                                    <div class="content">
                                      <h5><a href="#">${items_data.productname}</a></h5>
                                      </div>
                                  </td>
                                  <td class="price" data-title="Price">AED ${items_data.actual_price}</td>
                                  <td class="qty" data-title="QTY">${items_data.item_count}</td>
                                  <td  class="total" style="text-align:right" data-title="Total">AED ${items_data.actual_price * items_data.item_count }</td>
                                </tr>`;
                                
                                if (items_data.addons.length > 0){
                                  eachItems += `<tr>
                                                    <td colspan="4" class="product-thumbnail">Addons</td>
                                                </tr>`;
                                    items_data.addons.map((data) => {
                                      if (parseInt(data.addon_item_count) > 0){
                                            eachItems += `<tr>
                                                            <td class="product-thumbnail" data-title="Product Name">
                                                              <div class="content">
                                                                <h5><a href="#">${data.addon_name}</a></h5>
                                                              </div>
                                                            </td>
                                                              <td class="price">AED ${data.addon_price === null ? 0 : data.addon_price}</td>
                                                              <td class="qty">${data.addon_item_count === null ? 0 : data.addon_item_count}</td>
                                                              <td  class="total" style="text-align:right" data-title="Total">AED ${data.addon_price * data.addon_item_count }</td>
                                                          </tr>`;
                                      }
                                  })
                                }
                  
              }
              $('#order_item_details_' + data.order_id).empty().append(eachItems);
              $('#order_total_details_' + data.order_id + ' #item_total').text(grnd_total);
              $('#order_total_details_' + data.order_id + ' #item_taxtotal').text('AED ' + tx_total);
              $('#order_total_details_' + data.order_id + ' #item_subtotal').text('AED ' + sub_total);
    }

    $('#profile-tab').on('click', function() {
      $('.main_items').hide();
      $('#orders').hide();
      $('#home').hide();
    });

    $('#home-tab').on('click', function() {
      $('.main_items').hide();
      $('#orders').hide();
      $('#home').show();
    });

    $(document).on('click', '.cancel_order_item', function() {
      var order_id = $(this).attr('order_id');
      swal({
            title: "Cancel order",
            text: "Are you sure to cancel this order?",
            type: "info",
            showCancelButton: true,
            cancelButtonText: "Close",
            confirmButtonText: "Confirm",
            closeOnConfirm: true,
            showLoaderOnConfirm: true,
        },
        function(confirm){
            if(confirm) {
              $('#cancel_reason').modal('show');
              $('.order-cancellation_reason').unbind('click').bind('click', function(){
                if( $('#cancellation_reason').val() != '' ) {
                  $('#overlay').show();
                  $.ajax({
                    url: "<?php echo route('cancel_order') ?>",
                    type: 'post',
                    data: {
                      id: order_id,
                      reason: $('#cancellation_reason').val()
                    },
                    success: function(data) {
                      data = JSON.parse(data);
                      $('#cancel_reason').modal('hide');
                      if(data.status == true) {
                        $('#status_item_' + order_id).text('Cancelled');
                        // $('#view_item_' + order_id).text('');
                        $('.cancel_order_item[order_id="'+ order_id +'"]').hide();
                        alert('Order cancelled successfully');
                        $('.main_items').hide();
                        $('#orders').hide();
                        $('#home').show();
                        $('#home-tab').trigger('click');
                      } else {
                        show_error(data.message);
                      }
                    },
                    complete: function() {
                      $('#overlay').hide();
                    }
                  });
                } else {
                  alert('Please enter a reason for cancellation');
                }
              });
              
            }
          }
      );
      
    });

    function show_error(message) {
      setTimeout(() => {
        swal({
          title: "CAN'T BE CANCELLED",
          text: message,
          timer: 3000,
          showConfirmButton: false
      });
      }, 200);
    }

    $(document).on('click', '.downloadpdf', function() {
      var order_id = $(this).attr('order_id');
      var dataToSubmit = {"orderid":order_id};   
      //location.href = "order/pdfreport/"+order_id;
      loadData("<?php echo route('pdfreport'); ?>", 'pdfreport', dataToSubmit);
    });
    
    
    function pdfDownload(url, tab, data) {
      $("#overlay").show();
      @if( isset($order_id) )
        var order_id = {{ $order_id }}
      @else
        var order_id = "";
      @endif
      $.ajax({
        url: url,
        type: 'post',
        data: data,
        success: function(data) {
            $("#overlay").hide();
        },
        complete: function() {
            $("#overlay").hide();
        }
      });
    }
    
    $(document).on('click', '.order_details', function() {
      var order_id = $(this).attr('order_id');
      $('.main_items').hide();
      $('.order_details_'+order_id).show();
      $('#orders').show();
      $('#home').hide();
    });

    function profile(data) {
        $('#first_name').val(data.data.first_name);
        $('#last_name').val(data.data.last_name);
        $('#email').val(data.data.email);
        //var phone_number = data.data.phone_number;
        //phone_number = phone_number.split("-");
        //$('#phoneprefix').val(phone_number[0]);
        $('#phone').val(data.data.phone_number);
        $('#country').val(data.data.billing_country);
        //$("#country").val(data.data.nationality).change();
        $('#city').val(data.data.billing_city);
        $('#zip').val(data.data.billing_zip);
        $('#billing_street').val(data.data.billing_street);
        $('#billing_street_no').val(data.data.billing_street_no);
        $('#billing_apartment_no').val(data.data.billing_apartment_no);
        
    }
    
    function preference(data) {
        $('#agegroup').val(data.data.pref_age_group);
        $('#gender').val(data.data.pref_gender);
        $('#pref_country').val(data.data.pref_country);
        if (data.data.pref_intolerances !== null){
          $.each(data.data.pref_intolerances.split(","), function(i,e){
              $("#intolerances option[value='" + e + "']").prop("selected", true);
          });
        }
        
        // $('#intolerances').val(data.data.pref_intolerances);
        //$('#preferred_diet').val(data.data.pref_diet);
        if (data.data.pref_diet !== null){
          $.each(data.data.pref_diet.split(","), function(i,e){
            $("#preferred_diet option[value='" + e + "']").prop("selected", true);
          });
        }
        
        setTimeout(function(){ $('.select-2').multiSelect({placeholder: 'Please Choose'}); }, 150);
        
        $('#lifestyle').val(data.data.pref_lifestyle);
        $('#travel_pattern').val(data.data.pref_travel_pattern);
        $('#travelfor').val(data.data.pref_travel_for);
       
    }

     // populate country name in country feild //
         var countryNames = 
          ["United Arab Emirates","Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua & Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia & Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands","Central Arfrican Republic","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cuba","Curacao","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kiribati","Kosovo","Kuwait","Kyrgyzstan","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Mauritania","Mauritius","Mexico","Micronesia","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Myanmar","Namibia","Nauro","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","North Korea","Norway","Oman","Pakistan","Palau","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre & Miquelon","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Korea","South Sudan","Spain","Sri Lanka","St Kitts & Nevis","St Lucia","St Vincent","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad & Tobago","Tunisia","Turkey","Turkmenistan","Turks & Caicos","Tuvalu","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States of America","Uruguay","Uzbekistan","Vanuatu","Vatican City","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe"];    
             var country = $("#country");
            $(countryNames).each(function (index, element) {
                var option = $("<option />");

                //Set Customer Name in Text part.
                option.html(element);
 
                //Set Customer CustomerId in Value part.
                option.val(element);
 
                //Add the Option element to DropDownList.
                country.append(option);
            });

            var pref_country = $("#pref_country");
            $(countryNames).each(function (index, element) {
                var option = $("<option />");
                var pref_option = $("<option />");
 
                //Set Customer Name in Text part.
                option.html(element);
                pref_option.html(element);
 
                //Set Customer CustomerId in Value part.
                option.val(element);
                pref_option.val(element);
 
                //Add the Option element to DropDownList.
                country.append(option);
                pref_country.append(option);
            });
       // populate country name in country feild  ends //

  });
</script>
</body>

</html>