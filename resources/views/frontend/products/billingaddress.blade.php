@extends('frontend.layouts.appdetail')
@section('content')
<style>
 input[type=search] {
           
            background-color: #f1f1f1;
            border-color: #2e2e2e;
            border-style: solid;
            border-width: 2px 2px 2px 2px;
            outline: none;
            padding: 10px 20px 10px 20px;
            width: 250px;
            }
      
        ul.ui-autocomplete {
            color: #000000 !important;
            -moz-border-radius: 15px;
            border-radius: 1px;
            max-height: 180px; overflow-y: auto;
            overflow-x: hidden; 
            z-index: 99;
            font-size: 83% !important;
            font-weight: normal !important;
            }
  </style>
  <script>
  $(document).ready(function() {
    $('.pay_proceed').hide();
          
            setTimeout(() => {
                $('#flight_popup button.close').trigger('click');
            }, 200);
    //$('.order').hide();
    
        $(function () {
            var availableTags = 
              ["Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua & Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia & Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands","Central Arfrican Republic","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cuba","Curacao","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kiribati","Kosovo","Kuwait","Kyrgyzstan","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Mauritania","Mauritius","Mexico","Micronesia","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Myanmar","Namibia","Nauro","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","North Korea","Norway","Oman","Pakistan","Palau","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre & Miquelon","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Korea","South Sudan","Spain","Sri Lanka","St Kitts & Nevis","St Lucia","St Vincent","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad & Tobago","Tunisia","Turkey","Turkmenistan","Turks & Caicos","Tuvalu","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States of America","Uruguay","Uzbekistan","Vanuatu","Vatican City","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe"];
              $("#billing_country").autocomplete({
                source: availableTags
            });
        });
  });
  
  </script>
<div id="content">
       <!-- section -->
      <div class="wrapper">
        <div class="container">
          <nav aria-label="breadcrumb">
<!--            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item"><a href="#">Our Menu </a></li>
              <li class="breadcrumb-item"><a href="#">Breakfast</a></li>
              <li class="breadcrumb-item active" aria-current="page">Gluten free omlet</li>
            </ol>-->
          </nav>
          <div class="row">
            <div class="col-lg-8 column">
              <h2>Your Information</h2>
              <div class="customer-information">
                <ul>
                  <li>
                    <span>Name:</span>{{ Session::get('user_name')}}
                  </li>
                  <li>
                    <span>Flight No:</span>{{ Session::get('flight_number')}}
                  </li>
                  <li>
                    <span>Email:</span>{{ Session::get('user_email')}}
                  </li>
                  <li>
                    <span>Flight Date:</span>{{ date_format_custom(Session::get('flight_date'), '', 'd-m-Y' ) }}
                  </li>
                  <li>
                    <span>Mobile Number:</span>{{ Session::get('user_phone')}}
                  </li>
                  <li>
                    <span>Flight Time:</span>{{ date_format_custom(Session::get('flight_date'), '', 'H:i' ) }}
                  </li>
                </ul>
              </div>
              <h2>Billing Information</h2>
              <div class="billing-information">
                  <p id="validation_messages" style="color:red;font-weight: bold;"></p>
                  <h1 id="success_message" style="color: white!important;"></h1>
                <div class="form">
                  <ul>
                    <li class="half">
                    @if(Session::has('user_id'))
                    <input type="text" id="billing_name" name="billing_name" class="textbox" placeholder="Billing Name" value="{{ Session::get('billing_name')}}">
                      @else
                      <input type="text" id="billing_name" name="billing_name" class="textbox" placeholder="Billing Name" >
                     @endif 
                      
                    </li>
                    <li class="half">
                    @if(Session::has('billing_city'))
                    <input type="text" id="billing_city" name="billing_city" class="textbox" placeholder="Enter City" value="{{ Session::get('billing_city')}}">
                      @else
                      <input type="text" id="billing_city" name="billing_city" class="textbox" placeholder="Enter City" >
                     @endif 
                    </li>
                    <li  class="half">
                    @if(Session::has('billing_zip'))
                    <input maxlength="6" type="text" id="billing_zip1" name="billing_zip" class="textbox" placeholder="Enter P.O Box" value="{{ Session::get('billing_zip')}}">
                      @else
                      <input maxlength="6" type="text" id="billing_zip1" name="billing_zip" class="textbox" placeholder="Enter P.O Box">
                     @endif
                     <div id="billing-zip-error" style="display:none;position:absolute;font-size:16px;margin-top: 1px;color:red;" class="validation_messages">
                       Invalid P.O Box
                     </div>
                    </li>
                    <li  class="half">
                        <span class="dropdown_custom" style="display:block;">
                            <select id="billing_country" name="billing_country" class="textbox" placeholder="Select Country">
                            </select>
                        </span>
                    </li>

                    <li  class="half">
                    @if(Session::has('billing_street'))
                    <input type="text" id="billing_street" name="billing_street" class="textbox" placeholder="Enter Street" value="{{ Session::get('billing_street')}}">
                      @else
                      <input type="text" id="billing_street" name="billing_street" class="textbox" placeholder="Enter Street">
                     @endif 
                      
                    </li>
                    <li  class="half">
                    @if(Session::has('billing_street_number'))
                    <input type="text" id="billing_street_number" name="billing_street_number" class="textbox" placeholder="Enter Street number" value="{{ Session::get('billing_street_number')}}">
                      @else
                      <input type="text" id="billing_street_number" name="billing_street_number" class="textbox" placeholder="Enter Street number">
                     @endif 
                      
                    </li>
                    <li  class="half">
                    @if(Session::has('billing_apartment_number'))
                    <input type="text" id="billing_apartment_number" name="billing_apartment_number" class="textbox" placeholder="Enter Apartment number" value="{{ Session::get('billing_apartment_number')}}">
                      @else
                      <input type="text" id="billing_apartment_number" name="billing_apartment_number" class="textbox" placeholder="Enter Apartment number">
                     @endif 
                      
                    </li>


                    <li>
                      <input type="submit" value="Confirm and pay" class="btn button1" id="billing_submits">
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-lg-4 column pt-5">
              <div class="widgets">
                <div class="widget flight-block">
                  <div class="header">
                    <h3>Your CarryOn Details</h3>
                    <!-- <a href="#" class="toggle-btn"></a> -->
                  </div>
                  @include('frontend.layouts.flightinfo_order_side')
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /section -->
</div>
@php
   $confirm_pay = DB::select('select html_content from cms where visibility = "YES" and short_code = "confirm_pay" and deleted_at IS NULL');

@endphp
<!-- <button class="btn button1 btn-block " data-toggle="modal" data-target="#confirm">View Confirm Popup</button> -->

    <div class="modal" id="confirm" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <h2>Please Confirm</h2>
            <p>{!! $confirm_pay[0]->html_content !!}</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary order-reviewed">Yes</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
          </div>
        </div>
      </div>
    </div>
  <script>
  
  $(document).ready(function() {
      
     // populate country name in country feild //
     //alert("{{ Session::get('billing_country')}}");
         var countryNames = 
          ["United Arab Emirates","Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua & Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia & Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands","Central Arfrican Republic","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cuba","Curacao","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kiribati","Kosovo","Kuwait","Kyrgyzstan","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Mauritania","Mauritius","Mexico","Micronesia","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Myanmar","Namibia","Nauro","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","North Korea","Norway","Oman","Pakistan","Palau","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre & Miquelon","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Korea","South Sudan","Spain","Sri Lanka","St Kitts & Nevis","St Lucia","St Vincent","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad & Tobago","Tunisia","Turkey","Turkmenistan","Turks & Caicos","Tuvalu","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States of America","Uruguay","Uzbekistan","Vanuatu","Vatican City","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe"];    
             var billing_country = $("#billing_country");
            $(countryNames).each(function (index, element) {
                var option = $("<option />");
 
                //Set Customer Name in Text part.
                option.html(element);
 
                //Set Customer CustomerId in Value part.
                option.val(element);
 
                //Add the Option element to DropDownList.
                billing_country.append(option);
            });
            @if(Session::has('billing_country'))
                $("#billing_country").val("{{ Session::get('billing_country')}}").change();
            @endif
       // populate country name in country feild  ends //

  });
</script>
@endsection
