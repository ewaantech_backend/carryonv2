@extends('frontend.layouts.appdetail')
@section('content')
<div id="content">
    <!-- section -->
    <div class="wrapper">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">Our Menu </a></li>
                    <li class="breadcrumb-item"><a href="#">{{ $product_category_name }}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">
                        @php
                            echo $productdetails[0]['product_name'];
                        @endphp
                    </li>
                </ol>
            </nav>
            <div class="tabs1">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    @if(count($categories) > 0)
                        @php
                            $count = 1;
                        @endphp
                        @foreach($categories as $category)
                            @php
                                $category_selection_class = ($product_category_id == $category->id)?"active":"";
                            @endphp
                            <li class="nav-item">
                                <a href="{{ asset('product/menu#'.$category->id) }}"
                                    class="{{ $category_selection_class }}">{{ $category->name }}</a>
                            </li>
                        @endforeach
                    @else
                        <p>No Data</p>
                    @endif
                </ul>
            </div>
            <div class="row">
                <div class="col-lg-8 column dish">
                    <header class="mb-md-0">
                        <div class="row">
                            <div class="col-md-7">
                                <h2>
                                    @php
                                        echo $productdetails[0]['product_name'];
                                    @endphp
                                </h2>
                            </div>
                            <div class="col-md-5 text-md-right">
                                <a item_count={{ $productdetails[0]['item_count'] }}
                                    product_id={{ $productdetails[0]['product_id'] }}
                                    href="#" class="btn button1 add-item-cart"><i class="fa fa-cart-plus"
                                        aria-hidden="true"></i> Add for AED
                                    @php
                                        echo decimal_format_custom($productdetails[0]['actual_price']);
                                    @endphp</a>
                            </div>
                        </div>
                    </header>
                    <div class="slideshow2">
                        <div class="slides2">
                            @if(count($productdetails) > 0)
                                @php
                                    $count = 1;
                                @endphp
                                @foreach($productdetails as $productdetail)
                                    @if ($productdetail['type'] != 'THUMBNAIL')
                                    <div class="slide" data-slide="slide{{ $count }}">
                                        <figure class="figure1">
                                            <img class="w-100"
                                                src="{{ url($productdetail['product_image_path']) }}"
                                                alt="img">
                                        </figure>
                                    </div>
                                    @endif
                                        @php
                                            $count++;
                                        @endphp
                                    @endforeach
                                @endif
                        </div>
                        <div class="slide-controls">
                            <a href="#" class="prev" id="prev"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
                            <a href="#" class="next" id="next"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                        </div>
                        <div class="controls2">
                        </div>
                    </div>
                    <ul class="flavour-list">
                        @foreach($productallergies as $allergies)
                            <li>
                                <a data-toggle="tooltip" title="" data-original-title="{{ $allergies->description }}">
                                    <img src="{{ asset($allergies->image_path) }}" alt="img">
                                </a>
                            </li>
                        @endforeach
                    </ul>
                     @php
                                    echo $productdetails[0]['description'];
                    @endphp
                </div>

                <div class="col-lg-4 column pt-md-5">
                    <div class="widgets pt-md-3">
                        @php
                            $addonsAvailble = 0;    
                        @endphp
                        @if(count($addons) > 0)
                        <div class="widget addons">
                            <div class="header">
                                <h3>Add ons</h3>
                                <a href="#" class="toggle-btn"></a>
                            </div>
                            <div class="content">
                                <ul class="addon-list">
                                    @php
                                        $addonsAvailble = 1;    
                                    @endphp
                                    @foreach($addons as $addon)
                                    
                                    <li>
                                        <span>{{ $addon->name }}
                                            <strong>{{ $addon->addon_price }} <span>AED</span></strong>
                                        </span>
                                        <ul class="quantity">
                                            <li>
                                                <button addon_price="{{ $addon->addon_price }}" addon_id="{{ $addon->addon_id }}" id="addon_less_{{ $addon->addon_id }}" type="button" class="addon_less_new lessaddon"></button>
                                            {{-- <a href="#" style="color:#0e76bc"><i addon_id="{{$addon->addon_id }}" id="addon_less_{{ $addon->addon_id }}" class="addon_less fa fa-minus-circle"></i></a> --}}
                                            </li>
                                            <li>
                                                {{-- <input product_id="24" type="number" class="input-control" value="0" /> --}}
                                                <input  addon_id="{{ $addon->addon_id }}" id="val_{{ $addon->addon_id }}" type="number" class="addon_value input-addon" value="0">
                                            </li>
                                            <li>
                                                <button addon_price="{{ $addon->addon_price }}"  addon_id="{{ $addon->addon_id }}" id="{{ $addon->addon_id }}" class="addon_add_new addaddons"></button>
                                                {{-- <a href="#" style="color:#0e76bc"><i addon_id="{{$addon->addon_id }}" id="{{ $addon->addon_id }}" class="addon_add fa fa-plus-circle"></i></a> --}}
                                            </li>
                                        </ul>
                                    </li>
                                    @endforeach
                                </ul>
                                <footer class="clearfix">
                                    Addons Total <span id="addon_price_detail" class="price">0 <span>AED</span></span>
                                        <a href="#" product_id="{{ $productdetails[0]['product_id'] }}"  class="btn button1 add-item-cart-addon">Add for <span style="font-weight:bold;"> AED                                 
                                    @php
                                        echo decimal_format_custom($productdetails[0]['actual_price']);
                                    @endphp
                                        </span></a>
                                </footer>
                            </div>
                        </div>
                    @endif
                    

                        <div class="widget ingredients">
                            <div class="header">
                                <h3>Ingredients</h3>
                                <a href="#" class="toggle-btn"></a>
                            </div>
                            <div class="content">
                                <ul class="ingredients-list">
@if(count($ingredients) > 0)
@php
                                    $count = 1;
@endphp
@foreach($ingredients as $ingredient)
                                    <li>
                                        <div class="image">
                                            <img src="{{ asset($ingredient->image_path) }}" alt="img">
                                        </div>
                                        <p>{{ $ingredient->name }}</p>
                                    </li>
@endforeach
@else
                                    <p>No data to show</p>
@endif
                                </ul>
                            </div>
                        </div>
                        {{-- <div style="padding-top:15px;" class="detail-product">
@php
                            echo $productdetails[0]['description'];
@endphp
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /section -->
                    <!-- section -->
                    <div class="section bg-gray-light also-order">
                        <div class="container">
                            <h3>Also order</h3>
                            <ul class="menu-list">
                                @if(count($relatedproducts) > 0)
                                    @php
                                        $count = 1;
                                    @endphp
                                    @foreach($relatedproducts as $relatedproduct)
                                        <li>
                                            <div class="food">
                                                <div class="image">
                                                    <a
                                                        href="{{ asset($relatedproduct['url_key']) }}"><img
                                                            class="w-100"
                                                            src="{{ asset($relatedproduct['path']) }}"
                                                            alt="img"></a>
                                                </div>
                                                <div class="info">
                                                    <h2><a
                                                            href="#">{{ $relatedproduct['name'] }}</a>
                                                    </h2>
                                                    <footer>
                                                        <ul class="flavour-list">
                                                            @if ( isset($relatedproduct['allergies']) && count($relatedproduct['allergies']) > 0)
                                                            @foreach($relatedproduct['allergies'] as $allergies)
                                                                    <li>
                                                                        <a href="#" data-toggle="tooltip"
                                                                            title="{{ $allergies->description }}"><img
                                                                                src="{{ asset($allergies->image_path) }}"></a>
                                                                    </li>
                                                                @endforeach
                                                            @else
                                                                <p></p>
                                                            @endif
                                                        </ul>
                                                    </footer>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                @else
                                    <p>No data to show</p>
                                @endif
                            </ul>
                        </div>
                    </div>
                    <!-- /section -->
                </div>
                <script>
                    $(document).ready(function () {
                        var addonsAvailble = "{{$addonsAvailble}}";
                        if (addonsAvailble == 1){
                                        $(".add-item-cart").hide();
                                    }
                                setTimeout(() => {
                                    $('#flight_popup button.close').trigger('click');
                                }, 200);
                                //                        $.ajaxSetup({
                                //                            headers: {
                                //                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                //                            }
                                //                        });
                                function flightAdd() {
                                    if (localStorage.getItem('can_order') === null || localStorage.getItem('can_order') === "0" || localStorage.getItem(
                                        'can_order') === 'null') {
                                        $('#flight_popup').show();
                                        $('.trigger-order').trigger('click');
                                        alert('Please add flight information');
                                        $('#basket button').trigger('click');
                                        return false;
                                    }

                                    if (localStorage.getItem('can_order') === false || localStorage.getItem(
                                        'can_order') == 'false') {
                                        $('#flight_popup').show();
                                        $('.trigger-order').trigger('click');
                                        alert('Please update flight information');
                                        $('#basket button.toggle-btn').trigger('click');
                                        return false;
                                    }

                                    return true;
                                }


                                function cart_list_side_view() {
                                    $('#overlay').show();
                                    $('.pay_proceed').addClass('disabled');
                                    $.ajax({
                                        url: "<?php echo route('cart_list') ?>",
                                        method: "post",
                                        data: {
                                            key: localStorage.getItem('cart_key')
                                        },
                                        success: function (data) {
                                            //data = JSON.parse(data);
                                            var sideon_cart_string = '';
                                            if (data.status == true) {
                                                $('input.input-control').val(0);
                                                $('.add-item-cart').attr('item_count', 0);
                                                $('.quantity').removeClass('active');
                                                for (var inc = 0; inc < data.eachdata.length; inc++) {

                                                    // start - addons of the product
                                                    let addon_cart_string = '';
                                                    let total_product_price = parseFloat(data.eachdata[inc]
                                                        .grant_total_item_total);
                                                    let addons_total = 0;

                                                    if (data.eachdata[inc].addon.length > 0) { 
                                                        var addons_list = [];
                                                        data.eachdata[inc].addon.map((addon) => {
                                                            total_product_price += parseFloat(addon
                                                                .addon_total_price);
                                                            addons_total += parseFloat(addon
                                                                .addon_total_price);
                                                            //addon_cart_string = addon_cart_string +
                                                            //    addon.addon_name + ', ';
                                                            addons_list.push(addon.addon_name);
                                                        });
                                                        addon_cart_string = addons_list.join();
                                                    } else {
                                                        addon_cart_string = 'Nil';
                                                    }
                                                    // end - addons of the product

                                                    sideon_cart_string += sideon_cart_list(
                                                        data.eachdata[inc].product_id,
                                                        data.eachdata[inc].product_name,
                                                        data.eachdata[inc].item_count_item_total,
                                                        data.eachdata[inc].grant_total_item_total,
                                                        data.eachdata[inc].image_path,
                                                        addon_cart_string,
                                                        data.eachdata[inc].id,
                                                        addons_total
                                                    );

                                                    $('.product_id_' + data.eachdata[inc].product_id).find(
                                                        'input.input-control').val(data.eachdata[inc]
                                                        .total_products_count);
                                                    $('.add-item-cart').attr('item_count', data.eachdata[
                                                        inc].total_products_count);
                                                    $('.product_id_' + data.eachdata[inc].product_id)
                                                        .removeClass('active');
                                                    if (parseInt(data.eachdata[inc].total_products_count) >
                                                        0) {
                                                        $('.product_id_' + data.eachdata[inc].product_id)
                                                            .addClass('active');
                                                    }
                                                }



                                                $('.sideon-cart-list').html(sideon_cart_string);
                                                $(".customize").click(function () {
                                                    $('#overlay').show();
                                                    $("#basket").modal("hide");
                                                    getAddonsEdit($(this).attr("data-product-id"),
                                                        $(this).attr("data-cart-item-id"),
                                                        parseInt($(this).attr("data-qty"))).
                                                        then((response) => {
                                                        $('#overlay').hide();
                                                    });
                                                });

                                                if (data.eachdata.length == 0) {
                                                    $('.pay_proceed').addClass('disabled');
                                                    var url = $(location).attr('href'),
                                                        parts = url.split("/"),
                                                        last_part = parts[parts.length - 1];
                                                    if (last_part == "checkout" || last_part == "payment" ||
                                                        last_part == "billingaddress") {
                                                        setTimeout(() => {
                                                            location.href =
                                                                "<?php echo route('our_menu') ?>";
                                                        }, 2000);
                                                        // if no items redirect to menu.
                                                        alert('No items in cart');
                                                    }
                                                    $('.sideon-cart-list').html(
                                                        '<tr><td class="no-items-cart">No Items added!</td></tr>'
                                                        );
                                                    $('.sub_total').text('0.00');
                                                    $('.tax_total').text('0.00');
                                                    $('.grand_total').text('AED 0');
                                                    $('.total_cart_item_count').text(0);
                                                } else {
                                                    $('.pay_proceed').removeClass('disabled');
                                                    $('.sub_total').text(data.maindata.sub_total_total);
                                                    $('.tax_total').text(data.maindata.tax_total_total);
                                                    grant_total_item_total = parseFloat(data.maindata
                                                        .grant_total_total);
                                                    $('.grand_total').html('<span>AED</span>' +
                                                        grant_total_item_total);
                                                    $('.total_cart_item_count').text(data.maindata
                                                        .item_count_total);
                                                    $('.total_cart_item_count').attr(
                                                        'data-total-cart_item-count', data.maindata
                                                        .item_count_total);
                                                }
                                            } else {
                                                $('.sideon-cart-list').html(
                                                    '<tr><td class="no-items-cart">No Items added!</td></tr>'
                                                    );
                                                if (data.message != 'Key is not passed')
                                                    alert(data.message);
                                            }
                                        },
                                        complete: function () {
                                            $('#overlay').hide();
                                        }
                                    });
                                }

                                async function getAddonsEdit(productID, cartItemID, qty) {
                                    if (productID !== undefined && cartItemID !== undefined){
                                        try {
                                        let addonsObj = {
                                            product_id: productID,
                                            cart_item_id: cartItemID
                                        }
                                        let response = await fetch("<?php echo route('cart_addon') ?>", {
                                            method: "POST",
                                            headers: {
                                                "Content-Type": "application/json;charset=utf-8",
                                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                            },
                                            body: JSON.stringify(addonsObj)
                                        });
                                        let result = await response.json();
                                        let addonData = [];
                                        let total = 0;
                                        let modalBody = "<ul class='addon-list'>";
                                        let productName = "";
                                        var product_path = "";
                                        result.addons.map((data) => {
                                            if (data.item_count == null) {
                                                data.item_count = 0;
                                            }
                                            let addonObj = {};
                                            total += (parseInt(data.addon_price) * parseInt(data.item_count));
                                            product_path = data.product_path;
                                            modalBody += `

                                                        <li>
                                                            <span>${data.name}
                                                            <strong><span>AED </span> ${data.addon_price} </strong>
                                                            </span>
                                                            <ul class="quantity">
                                                                <li>
                                                                    <button addon_price="${data.addon_price}" addon_id="${data.addon_id}" id="addon_less_${data.addon_id}" class="addon_less lessaddon"></button>
                                                                </li>
                                                                <li>
                                                                    <input  addon_id="${data.addon_id}" id="val_${data.addon_id}" type="number" class="addon_value input-control" value="${parseInt(data.item_count)}">
                                                                </li>
                                                                <li>
                                                                    <button addon_price="${data.addon_price}" addon_id="${data.addon_id}" id="addon_add_${data.addon_id}" class="addon_add addaddons"></button>
                                                                </li>
                                                            </ul>
                                                        </li>

                                                            `;
                                            addonObj.addonID = data.addon_id;
                                            addonObj.count = parseInt(data.item_count);
                                            addonData.push(addonObj);
                                            productName = data.product_name;
                                        });
                                        modalBody += "</ul>"
                                        $("#addon").css({"padding-right" : "17px", "display": "block"});
                                        $("#addon .addon-list").html(modalBody);
                                        $("#addons_product_name").html(productName);
                                        $("#popupimage").html(`<img src={{asset('${product_path}')}} alt="">`);
                                        $("#addon_price").attr("data-price", total);
                                        sessionStorage.setItem("addonPrice", parseInt(total));
                                        $("#addon_price").text(total+' AED');
                                       // $("#addons-extra-style").html(`<div class="modal-backdrop fade"></div>`)
                                        $("#addon").modal();
                                        modalFunc(productID, qty, addonData, '', '', cartItemID);
                                        return result;
                                    } catch (error) {
                                        console.log(error);
                                    }
                                    }
                   
                                }


                                function modalFunc(productID, qty, addonData, type, operation, cartItemID) {
                                    $(".addon_add").click(function () {
                                        let addonObj = {};
                                        val = $("#val_" + $(this).attr("addon_id")).val();
                                        val = parseInt(val) + 1;
                                        $("#val_" + $(this).attr("addon_id")).val(val);
                                        let addonPrice = (sessionStorage.getItem("addonPrice") === null) ? 0 : sessionStorage.getItem("addonPrice") ;
                                        sessionStorage.setItem("addonPrice", parseInt(addonPrice) + parseInt($(this).attr("addon_price")) );
                                        $("#addon_price").text(sessionStorage.getItem("addonPrice")+' AED');
                                        if (addonData.length === 0) {
                                            addonObj.addonID = $(this).attr("addon_id");
                                            addonObj.count = val;
                                            addonData.push(addonObj);
                                        } else {
                                            let found = 0;
                                            addonData.map((data, key) => {
                                                if (parseInt(data.addonID) === parseInt($(this).attr(
                                                        "addon_id"))) {
                                                    addonData[key]['count'] = val;
                                                    found = 1;
                                                }
                                            });
                                            if (found === 0) {
                                                addonObj.addonID = $(this).attr("addon_id");
                                                addonObj.count = val;
                                                addonData.push(addonObj);
                                            }

                                        }
                                    });

                                    $(".addon_less").click(function () {
                                        let addonObj = {};
                                        let val = $("#val_" + $(this).attr("addon_id")).val();
                                        if (parseInt(val) !== 0) {
                                            let addonPrice = (sessionStorage.getItem("addonPrice") === null) ? 0 : sessionStorage.getItem("addonPrice") ;
                                            sessionStorage.setItem("addonPrice", parseInt(addonPrice) - parseInt($(this).attr("addon_price")) );
                                            $("#addon_price").text(sessionStorage.getItem("addonPrice")+' AED');
                                            val = parseInt(val) - 1;
                                            $("#val_" + $(this).attr("addon_id")).val(val);
                                            
                                            if (addonData.length === 0) {
                                                addonObj.addonID = $(this).attr("addon_id");
                                                addonObj.count = val;
                                                addonData.push(addonObj);
                                            } else {
                                                let found = 0;
                                                addonData.map((data, key) => {
                                                    if (parseInt(data.addonID) === parseInt($(this)
                                                            .attr("addon_id"))) {
                                                        addonData[key]['count'] = val;
                                                        found = 1;
                                                    }
                                                });

                                                if (found === 0) {
                                                    addonObj.addonID = $(this).attr("addon_id");
                                                    addonObj.count = val;
                                                    addonData.push(addonObj);
                                                }
                                            }
                                        }
                                    });

                                    $("#addon_save").click(function () {
                                        if (!isNaN(qty) && qty >= 0) {
                                            addItemToCart(productID, qty + 1, addonData, type, operation, cartItemID,
                                                function () {
                                                    addonData.splice(0, addonData.length);
                                                    $('.product_id_' + productID).find(
                                                        'input.input-control').val(qty + 1);
                                                    $('.add-item-cart').attr('item_count', qty + 1);
                                                    $('.product_id_' + productID).removeClass('active');
                                                    if (qty + 1 > 0) {
                                                        $('.product_id_' + productID).addClass('active');
                                                    }
                                                    sessionStorage.removeItem("addonPrice");
                                                });
                                        }
                                    });
                                }


                                function addItemToCart(product_id, newQty, addonData, type, operation, cartItemID,
                                    callBack) {
                                    $('#overlay').show();
                                    $.ajax({
                                        url: "<?php echo route('cart_add') ?>",
                                        method: "post",
                                        data: {
                                            productsid: product_id,
                                            item_count: newQty,
                                            key: localStorage.getItem('cart_key'),
                                            addons: addonData,
                                            type: type,
                                            operation: operation,
                                            cart_item_id: cartItemID
                                        },
                                        success: function (data) {

                                            data = JSON.parse(data);
                                            if (data.status == true) {
                                                if (typeof data.cartkey != 'undefined') {
                                                    localStorage.setItem("cart_key", data.cartkey);
                                                }
                                                alert('Your cart updated');
                                                cart_list_side_view();
                                                callBack();
                                            } else {
                                                alert(data.message);
                                            }
                                        },
                                        complete: function () {
                                            $('#overlay').hide();
                                        }
                                    });
                                    }

                                    function sideon_cart_list(product_id, product_name, item_count_item_total, grant_total_item_total, path, addon_cart_string, cart_item_id, addons_total) {
            grant_total_item_total = parseFloat(grant_total_item_total);
            if (addon_cart_string !== 'Nil') {
                return `<tr>
            <td class="product-image">
            <figure class="image">
            <img class="w-100" src="${path}" alt="img">
            </figure>
            
            </td>
            <td class="product-details">
            <h4>${product_name}
            <small></small>
            </h4>
            <ul class="quantity">
            <li><button product_id=${product_id} type="button" data-type="cart" data-cart-item-id="${cart_item_id}" class="less"><i class="fa fa-minus" aria-hidden="true"></i></button></li>
            <li><input product_id=${product_id} type="number" class="input-control" value="${parseInt(item_count_item_total)}"></li>
            <li><button product_id=${product_id} type="button" data-type="cart" data-cart-item-id="${cart_item_id}" class="add"><i class="fa fa-plus" aria-hidden="true"></i></button></li>
            </ul>
            </td>
            <td class="product-price">
            AED <strong>${grant_total_item_total}</strong>
            </td>
            <tr>
            <td colspan="3">
                <div class="customize">
                    <h5>Extra Items</h5>
                    <p>${addon_cart_string}</p>
                    <a href="javascript:void(0)" data-qty="${item_count_item_total}" data-product-id="${product_id}" data-cart-item-id="${cart_item_id}" class="customize btn button1">
                        <i  class="fa fa-pencil" aria-hidden="true">
                        </i>Customize
                    </a>
                </div>
            </td>
            </tr>
            </tr>`;
            } else {
                return `<tr>
            <td class="product-image">
            <figure class="image">
            <img class="w-100" src="${path}" alt="img">
            </figure>
                
            </td>
            <td class="product-details">
            <h4>${product_name}
            <small></small>
            </h4>
            <ul class="quantity">
            <li><button product_id=${product_id} type="button" data-type="cart" data-cart-item-id="${cart_item_id}" class="less"><i class="fa fa-minus" aria-hidden="true"></i></button></li>
            <li><input product_id=${product_id} type="number" class="input-control" value="${item_count_item_total}"></li>
            <li><button product_id=${product_id} type="button" data-cart-item-id="${cart_item_id}" data-type="cart" class="add"><i class="fa fa-plus" aria-hidden="true"></i></button></li>
            </ul>
            </td>
            <td class="product-price">
            AED <strong>${grant_total_item_total}</strong>
            </td>
            </tr>`;
            }

        }


                                    $(".addon_add_new").click(function () {
                                        if (flightAdd() == false) {
                                            return false;
                                        }
                                        let addonObj = {};
                                        val = $("#val_" + $(this).attr("addon_id")).val();
                                        val = parseInt(val) + 1;
                                        $("#val_" + $(this).attr("addon_id")).val(val);
                                        let addonData = (sessionStorage.getItem("addonData") === null) ? [] : sessionStorage.getItem("addonData") ;
                                        let addonPrice = (sessionStorage.getItem("addonPrice") === null) ? 0 : sessionStorage.getItem("addonPrice") ;
                                        sessionStorage.setItem("addonPrice", parseInt(addonPrice) + parseInt($(this).attr("addon_price")));
                                        $("#addon_price_detail").text(sessionStorage.getItem("addonPrice")+' AED');
                                        if (addonData.length === 0) {
                                            addonObj.addonID = $(this).attr("addon_id");
                                            addonObj.count = $("#val_" + $(this).attr("addon_id")).val();
                                            addonData.push(addonObj);
                                            sessionStorage.setItem("addonData", JSON.stringify(addonData));
                                        } else {
                                            addonData = JSON.parse(sessionStorage.getItem("addonData"));//no brackets
                                            let found = 0;
                                            addonData.map((data, key) => {
                                                if (parseInt(data.addonID) === parseInt($(this).attr(
                                                        "addon_id"))) {
                                                    addonData[key]['count'] = $("#val_" + $(this).attr("addon_id")).val();
                                                    found = 1;
                                                    sessionStorage.setItem("addonData", JSON.stringify(addonData));
                                                }
                                            });
                                            if (found === 0) {
                                                addonObj.addonID = $(this).attr("addon_id");
                                                addonObj.count = $("#val_" + $(this).attr("addon_id")).val();
                                                addonData.push(addonObj);
                                                sessionStorage.setItem("addonData", JSON.stringify(addonData));
                                            }

                                        }

                                    });

                                    $(".addon_less_new").click(function () {
                                        if (flightAdd() == false) {
                                            return false;
                                        }
                                        let addonObj = {};
                                        let val = $("#val_" + $(this).attr("addon_id")).val();
                                        if (parseInt(val) !== 0) {
                                            val = parseInt(val) - 1;
                                            $("#val_" + $(this).attr("addon_id")).val(val);
                                            let addonData = (sessionStorage.getItem("addonData") === null) ? [] : sessionStorage.getItem("addonData") ;
                                            let addonPrice = (sessionStorage.getItem("addonPrice") === null) ? 0 : sessionStorage.getItem("addonPrice") ;
                                            sessionStorage.setItem("addonPrice", parseInt(addonPrice) - parseInt($(this).attr("addon_price")));
                                            $("#addon_price_detail").text(sessionStorage.getItem("addonPrice")+' AED');
                                            if (addonData.length === 0) {
                                                addonObj.addonID = $(this).attr("addon_id");
                                                addonObj.count = $("#val_" + $(this).attr("addon_id")).val();
                                                addonData.push(addonObj);
                                                sessionStorage.setItem("addonData", JSON.stringify(addonData));
                                            } else {
                                                addonData = JSON.parse(sessionStorage.getItem("addonData"));//no brackets
                                                let found = 0;
                                                addonData.map((data, key) => {
                                                    if (parseInt(data.addonID) === parseInt($(this)
                                                            .attr("addon_id"))) {
                                                        addonData[key]['count'] = $("#val_" + $(this).attr("addon_id")).val();
                                                        found = 1;
                                                        sessionStorage.setItem("addonData", JSON.stringify(addonData));
                                                    }
                                                });

                                                if (found === 0) {
                                                    addonObj.addonID = $(this).attr("addon_id");
                                                    addonObj.count = $("#val_" + $(this).attr("addon_id")).val();
                                                    addonData.push(addonObj);
                                                    sessionStorage.setItem("addonData", JSON.stringify(addonData));
                                                }
                                            }
                                        }
                                    });

                                    $(".add-item-cart-addon").click(function () {
                                        if (flightAdd() == false) {
                                            return false;
                                        }
                                        // if (!isNaN(qty) && qty >= 0) {
                                            addItemToCart($(this).attr("product_id"), 1, JSON.parse(sessionStorage.getItem("addonData")),'', 'insertion', '', function(){
                                                sessionStorage.removeItem("addonData");
                                                sessionStorage.removeItem("addonPrice");
                                                $(".input-addon").val(0);
                                                $("#addon_price_detail").text(0+' AED');
                                                $('#basket').modal('show')
                                            });
                                        // }
                                    });


                                });

                                window.onload = function() {
                                    sessionStorage.removeItem("addonData");
                                    sessionStorage.removeItem("addonPrice");
                                    
                                    };
                                    
                </script>
                @endsection