@extends('frontend.layouts.appdetail')
@section('content')
<style>
#errcode,#errphone
{
color: white;
/* font-size: 8; */
text-align: center;
}
</style>
            <div id="content">
                <!-- section -->
      <div class="wrapper">
        <div class="container">
<!--          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item"><a href="#">Our Menu </a></li>
              <li class="breadcrumb-item"><a href="#">Breakfast</a></li>
              <li class="breadcrumb-item active" aria-current="page">Checkout</li>
            </ol>
          </nav>-->
          <div class="row">
            <div class="col-lg-8 column">
              <h2>Checkout</h2>
              <div class="checkout" id="checkout_image_footer">
                <ul>
                  <li class="active">
                      <a href="#register-form">Register</a>
                  </li>
                  <li>
                      <a href="#login-form">Login</a>
                  </li>
                </ul>
                <div class="content">
                  <div id="register-form" class="active">
                  <p id="validation_messages"></p>
                  <h1 id="success_message" style="color: white!important;"></h1>
                  <div class="form">
                      <ul>
                        <li class="half">
                            <input id="first_name" name="first_name" type="text" class="textbox" placeholder="Enter your first name">
                        </li>
                        <li class="half">
                          <input id="last_name" name="last_name"  type="text" class="textbox" placeholder="Enter your last name">
                        </li>
                        <li class="half">
                          <input  id="email" name="email" type="text" class="textbox" placeholder="Enter your email address">
                        </li>
                        <li class="half">
                          <!-- <input id="code" name="code" type="text" class="small textbox" placeholder="Code"> -->
                          <input id="phone_checkout" name="phone" type="text" class="textbox" placeholder="Enter your phone">
                          <span id="errphone"></span>
                        </li>
                        <li class="half">
                            <span class="dropdown_custom">
                                <select id="country" name="country" class="textbox search_input" placeholder="Select Country">
    <!--                                <option value="United Arab Emirates">United Arab Emirates</option>-->
                                </select>
                            </span>
<!--                          <input id="country" name="country" type="text" class="textbox search_input" placeholder="Enter country of residence">-->
                        </li>
                        <li class="half">
                            <input name="terms" type="checkbox" id="terms" /> <label for="terms"><a style="color:white !important;" href="{{asset('terms-and-conditions')}}" target="_blank">* I read and accepted the terms and
                                    conditions.</a></label>
                          <input type="submit" value="Register" class="btn button1" id="register_submit">
                        </li>
                      </ul>
                      </ul>
                    </div>
                  </div>
                    <div id="login-form" class="design-adjust">
                      <p id="login_validation_messages"></p>
                      <h1 id="login_success_message" style="color: white!important;"></h1>
                      <div class="form">
                        <ul>
                          <li>
                            <input id="login_email" name="login_email" type="text" class="textbox" placeholder="Email">
                          </li>
                          <li>
                            <input id="login_password" name="login_password" type="Password" class="textbox" placeholder="Password">
                          </li>
                          <li>
                            <a href="#" data-toggle="modal" data-target="#forgot_password_top_menu" id="forgot_password_link_checkout">Forgot password?</a>
                          </li>
                          <li>
                            <input type="submit" value="Login" class="btn button1" id="login_submit">
                          </li>
                        </ul>
                        </ul>
                      </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="col-lg-4 column pt-5">
              <div class="widgets">
                <div class="widget flight-block">
                  <div class="header">
                    <h3>Your CarryOn Details</h3>
                    <!-- <a href="#" class="toggle-btn"></a> -->
                  </div>
                  @include('frontend.layouts.flightinfo_order_side')
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /section -->
                </div>

<script type="text/Javascript">
$(document).ready(function(){
    
  $("#phone_checkout,#code").keypress(function (e) {
         if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
         $("#errcode,#errphone").html("Numbers Only").show().fadeOut("slow");
         return false;
                  }
          });
 
    var input = document.querySelector("#phone_checkout");
    window.intlTelInput(input, {
    preferredCountries: ["ae"],
    initialCountry:"ae",
    utilsScript: "{{asset('js/phone_plugin/utils.js')}}",
    });
    $('.iti__country').click(function(){
      $('#phone_checkout').val($(this).children("span:nth-child(3)").html()+'-');
    })
       


$('.order').hide();
$('.pay_proceed').hide();
$('#flight_popup').hide();
$('#register_submit').click(function(){
    $("#validation_messages").text('');
    var first_name = $("#first_name").val();
    var last_name = $("#last_name").val();
    var email = $("#email").val();
    var code = $("#code").val();
    var phone = $("#phone_checkout").val();
    var country = $("#country").val();
    var terms = $("#terms").val();
    var commonmessage = 'Provide data to all the feilds';
    if(first_name == ''){
        $("#validation_messages").text(commonmessage).focus();
        $("#first_name").addClass('textbox error').focus();
        return false;
    } else {
        $("#first_name").removeClass('error');
    }
    if(last_name == ''){
        $("#validation_messages").text(commonmessage).focus();
        $("#last_name").addClass('textbox error').focus();
        return false;
    } else {
        $("#last_name").removeClass('error');
    }
    if(email == ''){
        $("#validation_messages").text(commonmessage).focus();
        $("#email").addClass('textbox error').focus();
        return false;
    } else {
        $("#email").removeClass('error');
    }
    if(!validateEmail(email)){
        $("#email").addClass('textbox error');
        $("#validation_messages").text('Enter valid Email Address').focus();
        return false;
    } else {
        $("#email").removeClass('error');
    }
    // if(code == ''){
    //     $("#validation_messages").text(commonmessage).focus();
    //     $("#code").addClass('textbox error').focus();
    //     return false;
    // } else {
    //     $("#code").removeClass('error');
    // }
    if(phone == ''){
        $("#validation_messages").text(commonmessage).focus();
        $("#phone_checkout").addClass('textbox error').focus();
        return false;
    } else {
        $("#phone_checkout").removeClass('error');
    } 
    // if(isNaN(phone)){
    //     $("#validation_messages").text('Phone number should be numerals').focus();
    //     $("#phone").addClass('textbox error');
    //     return false;
    // } else {
    //     $("#phone").removeClass('error');
    // }   
    
    
    if(country == ''){
        $("#validation_messages").text(commonmessage);
        $("#country").addClass('textbox error').focus();
        return false;
    } else {
        $("#country").removeClass('error');
    }
    if($('#terms').prop('checked') == false){
        $("#validation_messages").text('Please Accept Terms and Conditions').focus();
        return false;
    } else {
        $("#terms").removeClass('error');
    }
    $('#overlay').show();   
    $.ajax( {
      url: "{{url('/customer/registration')}}",
      dataType: "json",
      data: {
        first_name: first_name,
        last_name: last_name,
        email: email,
        code: code,
        phone: phone,
        country: country,
      },
      success: function( data ) {
          if(data.error == '1'){
              $("#validation_messages").text(data.message).focus();
              $('#overlay').hide(); 
          } 
          if(data.error == '0'){
              update_cart();
              $('#overlay').hide();
              alert(data.message);
              
//              $(".form").html("");
//              $("#success_message").text(data.message);
              //$('.btn-secondary').trigger('click');
//                function close_popup(){
//                     //$('#register').slideUp();
//                     location.reload();
//                  };
//               window.setTimeout( close_popup, 2000 );
          }
      }
    } );
    
    function validateEmail($email) {
      var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
      return emailReg.test( $email );
    }
});

$('#login_submit').click(function(){
    $("#login_validation_messages").text('');
    var email = $("#login_email").val();
    var password = $("#login_password").val();
    var commonmessage = 'Provide data to all the feilds';
    if(email == ''){
        $("#login_validation_messages").text(commonmessage);
        $("#login_email").addClass('textbox error').focus();
        return false;
    } else {
        $("#login_email").removeClass('error');
    }
    if(!validateEmail(email)){
        $("#login_email").addClass('textbox error');
        $("#login_validation_messages").text('Enter valid Email Address').focus();
        return false;
    } else {
        $("#login_email").removeClass('error');
    }    
    if(password == ''){
        $("#login_validation_messages").text(commonmessage);
        $("#login_password").addClass('textbox error').focus();
        return false;
    } else {
        $("#login_password").removeClass('error');
    }
    
    $('#overlay').show();
    $.ajax( {
      type:"POST",
      url: "{{url('/customer/login')}}",
      dataType: "json",
      data: {
        email: email,
        password: password,
      },
      success: function( data ) {
          if(data.error == '1'){
              $("#login_validation_messages").text(data.message).focus();
              $('#overlay').hide();
          } 
          if(data.error == '0'){
              //$(".form").html("");
              //$("#login_success_message").text(data.message);
              update_cart();
              $('#overlay').hide();
              alert(data.message)
              
              //$('.btn-secondary').trigger('click');
//                function close_popup(){
//                     //$('#login').slideUp();
//                     location.reload();
//                  };
//               window.setTimeout( close_popup, 2000 );
          }
      }
    } );
    
    function validateEmail($email) {
      var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
      return emailReg.test( $email );
    }
});
    function update_cart() {
        $('#overlay').show();
        $.ajax({
            url: "<?php echo route('update_cart_user')?>",
            method: "post",
            data: {
                key: localStorage.getItem('cart_key')
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                data = JSON.parse(data);
                if(data.status == true) {
                    location.href = "<?php echo route('billingaddress') ?>";
                }
            },
            complete: function() {
                $('#overlay').hide();
            }
        });
    }
    
    
         // populate country name in country feild //
         var countryNames = 
          ["United Arab Emirates","Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua & Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas","Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia & Herzegovina","Botswana","Brazil","British Virgin Islands","Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands","Central Arfrican Republic","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica","Cote D Ivoire","Croatia","Cuba","Curacao","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea","Eritrea","Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana","Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India","Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kiribati","Kosovo","Kuwait","Kyrgyzstan","Laos","Latvia","Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Marshall Islands","Mauritania","Mauritius","Mexico","Micronesia","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Myanmar","Namibia","Nauro","Nepal","Netherlands","Netherlands Antilles","New Caledonia","New Zealand","Nicaragua","Niger","Nigeria","North Korea","Norway","Oman","Pakistan","Palau","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal","Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre & Miquelon","Samoa","San Marino","Sao Tome and Principe","Saudi Arabia","Senegal","Serbia","Seychelles","Sierra Leone","Singapore","Slovakia","Slovenia","Solomon Islands","Somalia","South Africa","South Korea","South Sudan","Spain","Sri Lanka","St Kitts & Nevis","St Lucia","St Vincent","Sudan","Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad & Tobago","Tunisia","Turkey","Turkmenistan","Turks & Caicos","Tuvalu","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States of America","Uruguay","Uzbekistan","Vanuatu","Vatican City","Venezuela","Vietnam","Virgin Islands (US)","Yemen","Zambia","Zimbabwe"];    
             var country = $("#country");
            $(countryNames).each(function (index, element) {
                var option = $("<option />");
 
                //Set Customer Name in Text part.
                option.html(element);
 
                //Set Customer CustomerId in Value part.
                option.val(element);
 
                //Add the Option element to DropDownList.
                country.append(option);
            });
       // populate country name in country feild  ends //

  });

</script>
@endsection