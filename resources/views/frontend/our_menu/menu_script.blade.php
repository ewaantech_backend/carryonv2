<script type="text/javascript">
    $(document).ready(function() {

        var url = $(location).attr('href'),
            parts = url.split("/"),
            last_part = parts[parts.length - 1];
        if (last_part == "") {
            $('#menu_link').removeClass('current');
            $('#home_link').addClass('current');

        } else {
            $('#home_link').removeClass('current');
            $('#menu_link').addClass('current');
        }

        var clickflag = true;
        var datakey;
        setTimeout(function() {
            var categoryfromurl = window.location.hash.substr(1);
            if (categoryfromurl != "" && clickflag == true) {
                datakey = 'tab_' + categoryfromurl + '-tab';
                $("#" + datakey).trigger("click");
                clickflag = false;
            }
        }, 1000);

        $('#logout').click(function() {
            $.ajax({
                url: "{{url('/customer/logout')}}",
                dataType: "json",
                data: {},
                success: function(data) {
                    alert(data.message);
                    localStorage.setItem("flight", 'N/A');
                    localStorage.setItem("flight_date", 'N/A');
                    localStorage.setItem("can_order", false);
                    localStorage.setItem("cart_key", '');
                    setTimeout(() => {
                        location.href = "<?php echo route('home_page') ?>";
                    }, 200);
                }
            });
        });

        $('#overlay').show();
        $.ajax({
            url: "<?php echo route('menu_dishes') ?>",
            method: "post",
            data: {
                key: localStorage.getItem('cart_key')
            },
            success: function(data) {
                data = JSON.parse(data);
                if (data.status == true) {
                    var tab_header = '';
                    var cat_tab_dets = [];
                    setIngredientsFilter(data.allergies_filter);
                    for (var inc = 0; inc < data.category_order.length; inc++) {
                        catid = data.category_order[inc];
                        tab_header += tabHeader(data.products[catid].category_id, data.products[catid].category_name, inc);
                        cat_tab_dets.push(itemRender(catid, name, data.products[catid].items, inc, data.cart_id));
                    }
                    $('#menu_tab').html(tab_header);
                    //$('.place-holder').remove();
                    for (var incr = 0; incr < cat_tab_dets.length; incr++) {
                        $('#render_content').before(cat_tab_dets[incr]);
                    }
                } else {
                    //alert(data.message);
                }
                //$('.place-holder').remove();
                cart_list_side_view();
            },
            complete: function() {
                $('#overlay').hide();
            }
        });

        function cart_list_side_view() {
            $('#overlay').show();
            $('.pay_proceed').addClass('disabled');
            $.ajax({
                url: "<?php echo route('cart_list') ?>",
                method: "post",
                data: {
                    key: localStorage.getItem('cart_key')
                },
                success: function(data) {
                    //data = JSON.parse(data);
                    var sideon_cart_string = '';
                    if (data.status == true) {
                        $('input.input-control').val(0);
                        $('.add-item-cart').attr('item_count', 0);
                        $('.quantity').removeClass('active');
                        for (var inc = 0; inc < data.eachdata.length; inc++) {

                            // start - addons of the product
                            let addon_cart_string = '';
                            let total_product_price = parseFloat(data.eachdata[inc].grant_total_item_total);
                            let addons_total = 0;

                            if (data.eachdata[inc].addon.length > 0) {
                                data.eachdata[inc].addon.map((addon) => {
                                    total_product_price += parseFloat(addon.addon_total_price);
                                    addons_total += parseFloat(addon.addon_total_price);
                                    addon_cart_string = addon_cart_string + addon.addon_name + ', ';
                                });
                            } else {
                                addon_cart_string = 'Nil';
                            }
                            // end - addons of the product

                            sideon_cart_string += sideon_cart_list(
                                data.eachdata[inc].product_id,
                                data.eachdata[inc].product_name,
                                data.eachdata[inc].item_count_item_total,
                                data.eachdata[inc].grant_total_item_total,
                                data.eachdata[inc].image_path,
                                addon_cart_string,
                                data.eachdata[inc].id,
                                addons_total
                            );

                            $('.product_id_' + data.eachdata[inc].product_id).find('input.input-control').val(data.eachdata[inc].total_products_count);
                            $('.add-item-cart').attr('item_count', data.eachdata[inc].total_products_count);
                            $('.product_id_' + data.eachdata[inc].product_id).removeClass('active');
                            if (parseInt(data.eachdata[inc].total_products_count) > 0) {
                                $('.product_id_' + data.eachdata[inc].product_id).addClass('active');
                            }
                        }



                        $('.sideon-cart-list').html(sideon_cart_string);
                        $(".customize").click(function() {
                            $('#overlay').show();
                            $("#basket").modal("hide");
                            getAddonsEdit($(this).attr("data-product-id"), $(this).attr("data-cart-item-id"), parseInt($(this).attr("data-qty"))).
                            then((response) => {
                                $('#overlay').hide();
                            });

                        });

                        if (data.eachdata.length == 0) {
                            $('.pay_proceed').addClass('disabled');
                            var url = $(location).attr('href'),
                                parts = url.split("/"),
                                last_part = parts[parts.length - 1];
                            if (last_part == "checkout" || last_part == "payment" || last_part == "billingaddress") {
                                setTimeout(() => {
                                    location.href = "<?php echo route('our_menu') ?>";
                                }, 2000);
                                // if no items redirect to menu.
                                alert('No items in cart');
                            }
                            $('.sideon-cart-list').html('<tr><td class="no-items-cart">No Items added!</td></tr>');
                            $('.sub_total').text('0.00');
                            $('.tax_total').text('0.00');
                            $('.grand_total').text('AED 0');
                            $('.total_cart_item_count').text(0);
                        } else {
                            $('.pay_proceed').removeClass('disabled');
                            $('.sub_total').text(data.maindata.sub_total_total);
                            $('.tax_total').text(data.maindata.tax_total_total);
                            grant_total_item_total = parseFloat(data.maindata.grant_total_total);
                            $('.grand_total').html('<span>AED</span>' + grant_total_item_total);
                            $('.total_cart_item_count').text(data.maindata.item_count_total);
                            $('.total_cart_item_count').attr('data-total-cart_item-count', data.maindata.item_count_total);
                        }
                    } else {
                        $('.sideon-cart-list').html('<tr><td class="no-items-cart">No Items added!</td></tr>');
                        // if (data.message != 'Key is not passed')
                        //     alert('here');
                        //     alert(data.message);
                    }
                },
                complete: function() {
                    $('#overlay').hide();
                }
            });
        }

        async function getAddonsEdit(productID, cartItemID, qty) {
            if (productID !== undefined && cartItemID !== undefined){
                try {
                let addonsObj = {
                    product_id: productID,
                    cart_item_id: cartItemID
                }
                
                let response = await fetch("<?php echo route('cart_addon') ?>", {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json;charset=utf-8",
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    body: JSON.stringify(addonsObj)
                });
                let result = await response.json();
                let total = 0;
                let addonData = [];
                let modalBody = "";
                let productName = "";
                var product_path = "";
                result.addons.map((data) => {
                    if (data.item_count == null) {
                        data.item_count = 0;
                    }
                    let addonObj = {};
                    total += (parseInt(data.addon_price) * parseInt(data.item_count));
                    product_path = data.product_path;
                    modalBody += `

                        <li>
                            <span>${data.name}
                            <strong><span>AED </span> ${data.addon_price} </strong>
                            </span>
                            <ul class="quantity">
                                <li>
                                    <button addon_price="${data.addon_price}" addon_id="${data.addon_id}" id="addon_less_${data.addon_id}" class="addon_less lessaddon"></button>
                                </li>
                                <li>
                                    <input  addon_id="${data.addon_id}" id="val_${data.addon_id}" type="number" class="addon_value input-control" value="${parseInt(data.item_count)}">
                                </li>
                                <li>
                                    <button addon_price="${data.addon_price}" addon_id="${data.addon_id}" id="addon_add_${data.addon_id}" class="addon_add addaddons"></button>
                                </li>
                            </ul>
                        </li>

                            `;
                    addonObj.addonID = data.addon_id;
                    addonObj.count = parseInt(data.item_count);
                    addonData.push(addonObj);
                    productName = data.product_name;
                });
                
                $("#addon").css({"padding-right" : "17px", "display": "block"});
                $("#addon .addon-list").html(modalBody);
                $("#addons_product_name").html(productName);
                $("#popupimage").html(`<img src={{asset('${product_path}')}} alt="">`);
                $("#addon_price").attr("data-price", total);
                sessionStorage.setItem("addonPrice", parseInt(total));
                $("#addon_price").text(total+' AED');
               // $("#addons-extra-style").html(`<div class="modal-backdrop fade"></div>`)
                $("#addon").modal();
                modalFunc(productID, qty, addonData, '', '', cartItemID);

            } catch (error) {
                console.log(error);
            }
            }
            
        }


        function modalFunc(productID, qty, addonData, type, operation, cartItemID) {
            $(".addon_add").click(function() {
                let addonObj = {};
                val = $("#val_" + $(this).attr("addon_id")).val();
                val = parseInt(val) + 1;
                $("#val_" + $(this).attr("addon_id")).val(val);
                let addonPrice = (sessionStorage.getItem("addonPrice") === null) ? 0 : sessionStorage.getItem("addonPrice") ;
                sessionStorage.setItem("addonPrice", parseInt(addonPrice) + parseInt($(this).attr("addon_price")) );
                $("#addon_price").text(sessionStorage.getItem("addonPrice")+' AED');
                if (addonData.length === 0) {
                    addonObj.addonID = $(this).attr("addon_id");
                    addonObj.count = val;
                    addonData.push(addonObj);
                } else {
                    let found = 0;
                    addonData.map((data, key) => {
                        if (parseInt(data.addonID) === parseInt($(this).attr("addon_id"))) {
                            addonData[key]['count'] = val;
                            found = 1;
                        }
                    });
                    if (found === 0) {
                        addonObj.addonID = $(this).attr("addon_id");
                        addonObj.count = val;
                        addonData.push(addonObj);
                    }

                }
            });

            $(".addon_less").click(function() {
                let addonObj = {};
                let val = $("#val_" + $(this).attr("addon_id")).val();
                if (parseInt(val) !== 0) {
                    let addonPrice = (sessionStorage.getItem("addonPrice") === null) ? 0 : sessionStorage.getItem("addonPrice") ;
                    sessionStorage.setItem("addonPrice", parseInt(addonPrice) - parseInt($(this).attr("addon_price")) );
                    $("#addon_price").text(sessionStorage.getItem("addonPrice")+' AED');
                    val = parseInt(val) - 1;
                    $("#val_" + $(this).attr("addon_id")).val(val);
                    
                    if (addonData.length === 0) {
                        addonObj.addonID = $(this).attr("addon_id");
                        addonObj.count = val;
                        addonData.push(addonObj);
                    } else {
                        let found = 0;
                        addonData.map((data, key) => {
                            if (parseInt(data.addonID) === parseInt($(this).attr("addon_id"))) {
                                addonData[key]['count'] = val;
                                found = 1;
                            }
                        });

                        if (found === 0) {
                            addonObj.addonID = $(this).attr("addon_id");
                            addonObj.count = val;
                            addonData.push(addonObj);
                        }
                    }
                }
            });

            $("#addon_save").click(function() {
                if (!isNaN(qty) && qty >= 0) {
                    addItemToCart(productID, qty + 1, addonData, type, operation, cartItemID, function() {
                        addonData.splice(0, addonData.length);
                        sessionStorage.removeItem("addonPrice");
                        $("#addon_price").text( "0 AED");
                        $("#addon_price").attr("data-price", 0)
                    });
                }
            });

            $("#close_addon").click(function(){
                sessionStorage.removeItem("addonPrice");
                $("#addon_price").text( "0 AED" );
                $("#addon_price").attr("data-price", 0)
            });
        }

        function addItemToCart(product_id, newQty, addonData, type, operation, cartItemID, callBack) {
            $('#overlay').show();
            $.ajax({
                url: "<?php echo route('cart_add') ?>",
                method: "post",
                data: {
                    productsid: product_id,
                    item_count: newQty,
                    key: localStorage.getItem('cart_key'),
                    addons: addonData,
                    type: type,
                    operation: operation,
                    cart_item_id: cartItemID
                },
                success: function(data) {

                    data = JSON.parse(data);
                    if (data.status == true) {
                        if (typeof data.cartkey != 'undefined') {
                            localStorage.setItem("cart_key", data.cartkey);
                        }
                        alert('Your cart updated');
                        cart_list_side_view();
                        callBack();
                    } else {
                        alert(data.message);
                    }
                },
                complete: function() {
                    $('#overlay').hide();
                }
            });
        }

        function sideon_cart_list(product_id, product_name, item_count_item_total, grant_total_item_total, path, addon_cart_string, cart_item_id, addons_total) {
            grant_total_item_total = parseFloat(grant_total_item_total);
            if (addon_cart_string !== 'Nil') {
                return `<tr>
            <td class="product-image">
            <figure class="image">
            <img class="w-100" src="${path}" alt="img">
            </figure>
            
            </td>
            <td class="product-details">
            <h4>${product_name}
            <small></small>
            </h4>
            <ul class="quantity">
            <li><button product_id=${product_id} type="button" data-type="cart" data-cart-item-id="${cart_item_id}" class="less"><i class="fa fa-minus" aria-hidden="true"></i></button></li>
            <li><input product_id=${product_id} type="number" class="input-control" value="${parseInt(item_count_item_total)}"></li>
            <li><button product_id=${product_id} type="button" data-type="cart" data-cart-item-id="${cart_item_id}" class="add"><i class="fa fa-plus" aria-hidden="true"></i></button></li>
            </ul>
            </td>
            <td class="product-price">
            AED <strong>${grant_total_item_total}</strong>
            </td>
            <tr>
            <td colspan="3">
                <div class="customize">
                    <h5>Extra Items</h5>
                    <p>${addon_cart_string}</p>
                    <a href="javascript:void(0)" data-qty="${item_count_item_total}" data-product-id="${product_id}" data-cart-item-id="${cart_item_id}" class="customize btn button1">
                        <i  class="fa fa-pencil" aria-hidden="true">
                        </i>Customize
                    </a>
                </div>
            </td>
            </tr>
            </tr>`;
            } else {
                return `<tr>
            <td class="product-image">
            <figure class="image">
            <img class="w-100" src="${path}" alt="img">
            </figure>
            <ul class="quantity">
            <li><button product_id=${product_id} type="button" data-type="cart" data-cart-item-id="${cart_item_id}" class="less"><i class="fa fa-minus" aria-hidden="true"></i></button></li>
            <li><input product_id=${product_id} type="number" class="input-control" value="${item_count_item_total}"></li>
            <li><button product_id=${product_id} type="button" data-cart-item-id="${cart_item_id}" data-type="cart" class="add"><i class="fa fa-plus" aria-hidden="true"></i></button></li>
            </ul>
            </td>
            <td class="product-details">
            <h4>${product_name}
            <small></small>
            </h4>
            </td>
            <td class="product-price">
            AED <strong>${grant_total_item_total}</strong>
            </td>
            </tr>`;
            }

        }

        function tabHeader(cat_id, name, count) {
            var classs = count == 0 ? 'active' : '';
            return `<li class="nav-item">
                    <a class="category nav-link ${classs}" id="tab_${cat_id}-tab" data-toggle="tab" href="#tab_${cat_id}" 
                        role="tab" aria-controls="home" aria-selected="true">${name}</a>
                </li>`;
        }

        function setIngredientsFilter(incredients) {
            var string = '';
            for (var inc = 0; inc < incredients.length; inc++) {
                string += `<li>
                                <a style="cursor:pointer;" ingredient_id=${incredients[inc].id} class="ingredient_filter">
                                    ${incredients[inc].name}
                                </a>
                            </li>`;
            }

            $('.filter-list').html(string);
        }

        $(document).on('click', '.ingredient_filter', function() {
            $('#overlay').show();
            var ingredient_id = $(this).attr('ingredient_id');
            $(".menu-list li").map((index, element) => {
                let ingredients =  ($(element).attr("ingredients"));
                if (ingredients){
                if ($.inArray(ingredient_id, ingredients.split(',')) == -1) {
                        $(element).hide();
                    } else {
                        $(element).show();
                    }
                } else {
                 $(element).hide();
                }
            })
            $(".info li").show();
            $('.filter-list li').removeClass('active');
            $(this).closest('li').addClass('active');
            $('.clear-filter-btn').removeClass('d-none');
            setTimeout(() => {
                $('#overlay').hide();
            }, 500);
        });
        
        $(document).on('click', '.clear-filter-btn', function() {
            $('#overlay').show();
            var ingredient_id = $(this).attr('ingredient_id');
            $(".menu-list li").map((index, element) => {
                $(element).show();
            })
            $(".info li").show();
            $('.filter-list li').removeClass('active');
            //location.reload();
            $('.clear-filter-btn').addClass('d-none');
            setTimeout(() => {
                $('#overlay').hide();
            }, 500);
        });

        function itemRender(cat_id, name, items, inc, cart_id) {
            var string = '';
            var classs = inc == 0 ? 'show active' : '';
            string += `<div class="tab-pane fade ${classs} column" id="tab_${cat_id}" role="tabpanel" aria-labelledby="tab_${cat_id}-tab">
              <ul class="menu-list">`;
            var count = 0;
            var active_or_not = '';
            for (var inc = 0; inc < items.length; inc++) {
                count = items[inc].item_count == null ? 0 : items[inc].item_count;
                active_or_not = count > 0 ? 'active' : '';
                pdt_price = parseFloat(items[inc].actual_price);
                string += `<li  ingredients=${items[inc].allergy_menu_filters}>
                        <div class="food alt">
                            <div class="image">
                            <a href="${items[inc].url_key}"><img style="height: 280px;" class="w-100" src="${items[inc].thumnail}" alt="img"></a>
                            </div>
                            <div class="info">
                                <footer>
                                    <ul class="flavour-list">`;
                                    for (var allr_inc = 0; allr_inc < items[inc].allergies.length; allr_inc++) {
                                        string += `<li>
                                                                        <a data-toggle="tooltip" title="${items[inc].allergies[allr_inc].name}"><img src="${items[inc].allergies[allr_inc].image_path}"
                                                                            alt="img">
                                                                        </a>
                                                                    </li>`;
                                    }
                                    string += `</ul>
                                    <span class="price">${pdt_price} <small>AED</small></span>
                                </footer>
                                <h2><a href="#">${items[inc].product_name}</a></h2>
                                <ul class="quantity ${active_or_not} product_id_${items[inc].product_id}">
                                    <li><button product_id=${items[inc].product_id} type="button"  class="less"></button></li>
                                    <li><input  product_id=${items[inc].product_id} type="number" class="input-control" value="${count}"></li>
                                    <li><button product_id=${items[inc].product_id} type="button" class="add"></button></li>
                                </ul>
                            </div>
                        </div>
                    </li>`;
            }
            string += `</ul>
            </div>`;

            return string;
        }

        $('#flight_popup').hide();
        if (localStorage.getItem("can_order") == true) {
            $('#flight_popup button.close').trigger('click');
            setTimeout(() => {
                $('#flight_popup button.close').trigger('click');
            }, 200);
        }

    });
    window.onload = function() {
                                    sessionStorage.removeItem("addonPrice");
                                    $("#addon_price").text( "0 AED" );
                                    $("#addon_price").attr("data-price", 0)
                      };
</script>